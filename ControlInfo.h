#pragma once

#include <QtQml>

namespace aw {

struct ControlInfo
{
	QString text_;
	QString name_;
	int type_;
	static QObject *model(QList<ControlInfo> const &, QString const &);
};

}
