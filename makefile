target := i386-linux-gnu
config := debug
tmp := /tmp

# --------------------------------------------------------------------------------
# Pour make

tmpdir := $(abspath $(tmp)/$(target)-$(config))
rollback = { status=$$?; $(RM) $@; exit $$status; }
add_rules = $(foreach *,$2,$(call rules.$1,$(patsubst $(CURDIR)/%,$(tmpdir)/%,$*),$*))
assert = $(if $2,$(error $1: $2))
recipe = @ $$(call recipe*,$1,$2,$3)
.DEFAULT_GOAL = all
stack :=
push = $(foreach *,$1,$(call push_one,$*))
add_files = files += $1
add_prerequisites = prerequisites += $1
simplify = $(patsubst $(CURDIR)/%,%,$(patsubst $(tmpdir)/%,%,$1))
select = $($1) $($1@$(target)) $($1@$(config)) $($1@$(target)@$(config))

ifeq ($(filter-out i386-linux-gnu,$(filter-out amd64-linux-gnu,$(target))),)
find := find
application_suffix :=
endif

ifeq ($(target),windows-32-linux)
find := find
application_suffix := .exe
override cpp_defines += _USE_MATH_DEFINES
endif

ifeq ($(target),i386-windows-gnu)
find := "C:/MinGW/msys/1.0/bin/find.exe"
application_suffix := .exe
override cpp_defines += M_PI=3.141592654
endif

define recipe*
echo '#' $(call simplify,$@)
mkdir -p $(@D)
$(call recipe.$1,$2,$3)
endef

define recipe.contents.mk
$(find) -L $(CURDIR) ! -path '*/.*' -regextype posix-basic ! -regex '.*[[:space:]].*' '(' -type d -printf '$@: %p\n%p:\n' -o -printf 'all += %p\n' ')' >$@ || $(rollback)
endef

define push_one
#
$(stack)$1 := $($1)
variables += $1
endef

define pop_one
#
$1 := $($(stack)$1)
undefine $(stack)$1
endef

define begin
$(stack)*stack := $(stack)
$(stack)*variables := $(variables)
$(stack)*environment := $(environment)
stack := $(stack)*
variables := environment variables
environment := $1
$(call begin.$1,$2)
endef

define end
$(call end.$(environment))
$(foreach *,$(variables),$(call pop_one,$*))
$(call pop_one,stack)
endef

define rules.contents.mk
all :=
$(tmpdir)/contents.mk:; $(call recipe,contents.mk)
-include $(tmpdir)/contents.mk
endef

$(eval $(rules.contents.mk))

# --------------------------------------------------------------------------------
# Pour GCC

all.c := $(filter %.c,$(all))
all.cc := $(filter %.cc %.C %.cxx %.cpp,$(all))
all.h := $(filter %.h %.hh %.H %.hxx %.hpp,$(all))
all.f := $(filter %.f %.F,$(all))
all.s := $(filter %.s %.S,$(all))

add_cxx_files = $(call add_rules,cc,$(all.cc))

CPPFLAGS += $(call select,cpp_flags) $(patsubst %,-I"%",$(call select,cpp_includes)) $(patsubst %,-isystem"%",$(call select,cpp_includes_system)) $(addprefix -D,$(call select,cpp_defines))
CXXFLAGS += $(call select,cxx_flags)
LDFLAGS += $(call select,ld_flags)

cpp_defines@release += NDEBUG
cpp_defines@profile += NDEBUG
override cpp_includes += $(CURDIR)

define recipe.cc.o
$(COMPILE.cc) -o $@ $< -MD -MP -MF $@.d
sed -i 's, $< , ,' $@.d
endef

define recipe.elf.aux
#
echo $1 >>$@.d
endef
define recipe.elf
>$@.d
$(foreach *,$^,$(call recipe.elf.aux,$*))
$(LINK.cc) @$@.d $(LDLIBS) -o $@
endef

define rules.cc
# compile a .cc
# $1 is the prefix of the target .o file
# $2 is the source .cc file
$1.o: $2; $(call recipe,cc.o)
all.o += $1.o
-include $1.o.d
endef

define begin.application
# link several .o
# 1 = the name of the target application
# all.o = a list of source .o files
$(call push,application all.o)
application := $(tmpdir)/$1$(application_suffix)
all.o :=
endef
define end.application
all: $(application)
$(application): $(all.o); $(call recipe,elf)
application :=
endef

# --------------------------------------------------------------------------------
# Pour Qt

override cpp_defines += QT_NO_KEYWORDS
cpp_defines@debug += QT_QML_DEBUG QT_DECLARATIVE_DEBUG
cpp_defines@release += QT_NO_DEBUG QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT
cpp_defines@profile += QT_NO_DEBUG QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT

all.ui := $(filter %.ui,$(all))
all.qrc := $(filter %.qrc,$(all))
all.ts := $(filter %.ts,$(all))

ifeq ($(filter-out i386-linux-gnu,$(filter-out amd64-linux-gnu,$(filter-out windows-32-linux,$(target)))),)
qt5_moc := moc
qt5_rcc := rcc
qt5_uic := uic
qt5_lupdate := lupdate
qt5_lrelease := lrelease
endif

ifeq ($(target),i386-windows-gnu)
qt5_path := $(dir $(wildcard C:/Qt/Qt5.*/5.*/*/bin))
qt5_moc := "$(qt5_path)/bin/moc.exe"
qt5_rcc := "$(qt5_path)/bin/rcc.exe"
qt5_uic := "$(qt5_path)/bin/uic.exe"
qt5_lupdate := "$(qt5_path)/bin/lupdate.exe"
qt5_lrelease := "$(qt5_path)/bin/lrelease.exe"
override cpp_includes_system += $(qt5_path)/include
endif

add_qt_objects = $(call add_rules,qt.moc,$(all.h))
add_qt_resources = $(call add_rules,qt.qrc,$(all.qrc))
add_qt_forms = $(call add_rules,qt.ui,$(all.ui))
add_qt_translations = $(call add_rules,qt.ts,$(all.ts))

with_qt_private_includes = $(dir $(wildcard $(addsuffix /*/Qt*/private,$1))) $1

define recipe.qt.moc.cc
$(qt5_moc) $(patsubst %,-I"%",$(call select,cpp_includes) $(call select,cpp_includes_system)) $(addprefix -D,$(call select,cpp_defines)) -f$< $< -o $@
endef

define recipe.qt.qrc.aux
#
echo '<file alias="$(call simplify,$1)">$1</file>' >>$@.new
endef
define recipe.qt.qrc
echo '<RCC><qresource>' >$@.new
$(foreach *,$1,$(call recipe.qt.qrc.aux,$*))
echo '</qresource></RCC>' >>$@.new
cmp -s $@.new $@ || mv $@.new $@
endef

define recipe.qt.qrc.cc
$(qt5_rcc) -name $(call simplify,$<) $< -o $@
endef

define recipe.qt.qrc.rcc
$(qt5_rcc) -binary -name $(call simplify,$<) $< -o $@
endef

define recipe.qt.ui.h
$(qt5_uic) $< -o $@
endef

define recipe.qt.ts.qm
$(qt5_lrelease) $< -qm $@
endef

define recipe.qt.ts.lupdate
cp $1 $@ || $(rollback)
$(qt5_lupdate) -locations none -recursive $(CURDIR) -ts $@ || $(rollback)
cmp -s $@ $1 || mv $@ $1
rm -f $@
endef

define rules.qt.moc
# run moc on a .cc or .h
# $1 is the prefix of the target .cc file
# $2 is the source .cc or .h file
$1.moc.cc: $2; $(call recipe,qt.moc.cc)
$1.moc.cc.o: override cpp_flags += -MQ $1.moc.cc
$(call rules.cc,$1.moc.cc,$1.moc.cc)
endef

define rules.qt.qrc
# run rcc on a .qrc
# $1 is the prefix of the target .cc file
# $2 is the source .qrc file
$1.cc.o: override CXXFLAGS += $(call select,qrc_cxx_flags)
$1.cc: $2; $(call recipe,qt.qrc.cc)
$1.rcc: $2; $(call recipe,qt.qrc.rcc)
$(if $(application),$(call rules.cc,$1.cc,$1.cc),all: $1.rcc)
endef

define rules.qt.ui
# run uic on a .ui
# $1 is the prefix of the target .h file
# $2 is the source .ui file
$1.h: $2; $(call recipe,qt.ui.h)
all.ui.h += $1.h
$(all.o): | $1.h
endef

define rules.qt.ts
#
$1.qm: $2 | $(dir $2)update.$(notdir $2); $(call recipe,qt.ts.qm)
$(dir $2)update.$(notdir $2): $(MAKEFILE_LIST) $(all); $(call recipe,qt.ts.lupdate,$2)
$(call add_files,$1.qm)
endef

define begin.qt.qrc
$(call push,qrc files prerequisites)
qrc := $(tmpdir)/$1.qrc
files :=
prerequisites :=
endef
define end.qt.qrc
$(qrc): $(prerequisites); $(call recipe,qt.qrc,$(files))
$(qrc).cc $(qrc).rcc: $(files)
$(files):
$(call rules.qt.qrc,$(qrc),$(qrc))
endef

# --------------------------------------------------------------------------------
# Pour git

define recipe.version.h
git rev-list --count HEAD >$@.new
cmp -s $@.new $@ || mv $@.new $@
endef

define rules.version.h
$(all.cc): | $(tmpdir)/version.h
$(tmpdir)/version.h: $(shell git rev-parse --git-dir)/index; $(call recipe,version.h)
override cpp_includes += $(tmpdir)
endef

$(eval $(rules.version.h))

# --------------------------------------------------------------------------------
# Spécifique au projet

override cpp_flags += -Wall -Wextra -Werror -Wconversion -Winit-self -Wno-error=unused-function
override cpp_defines@release += CONFIG_RELEASE
override cxx_flags += -std=c++11
cxx_flags@debug += -O0 -ggdb3
cxx_flags@release += -Ofast -flto -s -Wno-odr
cxx_flags@profile += -Ofast -g
cxx_flags@amd64-linux-gnu += -m64
ld_flags@release += -flto
ld_flags@i386-windows-gnu += -Wl,--large-address-aware
ld_flags@windows-32-linux += -Wl,--large-address-aware
qml := $(filter %.qml %.js %/qmldir,$(all))
data := $(filter $(CURDIR)/game/%,$(filter %.png %.wav %.ogg,$(all)))

ifeq ($(filter-out i386-linux-gnu,$(filter-out amd64-linux-gnu,$(filter-out windows-32-linux,$(target)))),)
pc_packages := Qt5Quick Qt5Concurrent box2d sdl2 openal vorbisfile vorbis theoradec
override cpp_includes_system += $(call with_qt_private_includes,$(patsubst -I%,%,$(shell pkg-config --cflags-only-I $(pc_packages))))
override LDLIBS += -Wl,-rpath=. -llycop
endif

ifneq ($(steam_path),)
override cpp_includes_system += $(steam_path)/public
override cpp_defines += CONFIG_STEAM
override LDLIBS += -lsteam_api
cxx_flags@amd64-linux-gnu += -ldl
cxx_flags@i386-linux-gnu += -ldl -no-pie
ld_flags@i386-windows-gnu += -lsteam_hack
ld_flags@windows-32-linux += -lsteam_hack
endif

ifeq ($(target),i386-windows-gnu)
override cpp_includes_system += $(call with_qt_private_includes,$(addprefix $(qt5_path)/include/,QtCore QtQml QtGui QtQuick QtConcurrent))
override LDLIBS += $(patsubst %,"$(qt5_path)/lib/lib%.a",Qt5Core Qt5Qml Qt5Gui Qt5Quick Qt5Concurrent qtmain)
endif

ifneq ($(demo),)
filter_demo = $(filter-out $(foreach *,3 4 5 6 7 8 9,$(CURDIR)/game/chapter-$*%),$1)
qml := $(call filter_demo,$(qml))
data := $(call filter_demo,$(data))
override cpp_defines += CONFIG_DEMO
endif

$(eval	$(call begin,application,lycop))
$(eval		$(call add_cxx_files))
$(eval		$(call add_qt_objects))
$(eval		$(call add_qt_resources))
$(eval		$(call begin,qt.qrc,qml))
$(eval			$(call add_files,$(qml)))
$(eval			$(call add_prerequisites,$(MAKEFILE_LIST)))
$(eval		$(call end))
$(eval		$(call begin,qt.qrc,ui))
$(eval			$(call add_files,$(filter %.sci %.otf %.vsh %.fsh,$(all))))
$(eval			$(call add_files,$(filter-out $(CURDIR)/game/%,$(filter %.png %.wav %.ogg,$(all)))))
$(eval			$(call add_prerequisites,$(MAKEFILE_LIST)))
$(eval		$(call end))
$(eval	$(call end))
$(eval	$(call begin,qt.qrc,data))
$(eval		$(call add_qt_translations))
$(eval		$(call add_files,$(data)))
$(eval		$(call add_prerequisites,$(MAKEFILE_LIST)))
$(eval	$(call end))
