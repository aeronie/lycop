# Images

\code
[A] (De)select All
[X] Delete

Add Monkey
Shade Smooth

Add Plane
[N] Properties → Transform
Toggle Editmode
Area → Properties → Material → Transparency [Alpha = 0]
Area → UV/Image Editor → New
Toggle Editmode

Area → Properties → World → Ambient Occlusion
Area → Properties → World → Environment Lightning
Area → Properties → Render → Bake [Selected to Active, Full Render, Bake, Normals, Bake]
\endcode

\see
- http://matrep.parastudios.de/index.php?p=7
