#include <QtQuick>
#include <SDL.h>
#include <memory>

#include "meta.private.h"

namespace aw {
namespace {

struct Joystick
{
	std::unique_ptr<SDL_Joystick, decltype(&SDL_JoystickClose)> ptr_;
	int index_;

	Joystick(SDL_Joystick *joystick, int index)
	: ptr_(joystick, SDL_JoystickClose)
	, index_(index)
	{
	}
};

struct Events
: QObject
{
	Events **previous_ = 0;
	Events *next_ = 0;

	~Events()
	{
		if(previous_) *previous_ = next_;
		if(next_) next_->previous_ = previous_;
	}
};

template<class _type>
struct EventsList
{
	_type *begin_ = 0;
	typename _type::value_type front_, back_;

	~EventsList()
	{
		if(begin_) begin_->previous_ = 0;
	}

	_type *acquire()
	{
		_type *o = new _type;
		o->previous_ = reinterpret_cast<Events **>(&begin_);
		o->next_ = *o->previous_;
		*o->previous_ = o;
		if(o->next_) o->next_->previous_ = &o->next_;
		return begin_;
	}

	void set(typename _type::value_type value, bool enabled)
	{
		if(enabled)
		{
			if(front_ != value)
			{
				front_ = value;
				back_ = value;
				if(begin_) begin_->set(value);
			}
		}
		else
		{
			back_ = value;
		}
	}

	void set()
	{
		set(back_, true);
	}
};

#define _(_class, _signal, _type) \
	struct _class \
	: Events \
	{ \
		Q_OBJECT \
	public: \
		using value_type = _type; \
		void set(_type const &x) { _signal(x); } \
		Q_SIGNAL void _signal(_type); \
	};

_(JoystickAxisEvents, moved, qreal)
_(JoystickHatEvents, moved, QPointF)
_(JoystickBallEvents, moved, QPointF)
_(JoystickButtonEvents, pressed, bool)
_(KeyEvents, pressed, bool)
_(MouseEvents, moved, QPointF)
_(MouseButtonEvents, pressed, bool)

#undef _

struct InputArea
: QQuickItem
{
	AW_DECLARE_OBJECT_STUB(InputArea)
	AW_DECLARE_PROPERTY_STORED(float, zoom) = 100; ///< px m⁻¹
	AW_DECLARE_PROPERTY_STORED(float, angle) = 0; ///< rad
	AW_DECLARE_PROPERTY_STORED(bool, edit) = false;
	AW_DECLARE_PROPERTY_READONLY(QPointF, mouse_position)
	std::vector<SDL_JoystickGUID> joystick_guids_;
	std::map<int, Joystick> joysticks_;
	std::map<int, EventsList<KeyEvents>> keys_;
	std::map<int, EventsList<MouseEvents>> mouses_;
	std::map<int, EventsList<MouseButtonEvents>> mouse_buttons_;
	std::map<std::pair<int, int>, EventsList<JoystickAxisEvents>> joystick_axes_;
	std::map<std::pair<int, int>, EventsList<JoystickHatEvents>> joystick_hats_;
	std::map<std::pair<int, int>, EventsList<JoystickBallEvents>> joystick_balls_;
	std::map<std::pair<int, int>, EventsList<JoystickButtonEvents>> joystick_buttons_;

	QPointF transform(QPointF const &p) const
	{
		return QTransform().rotateRadians(angle()).scale(1 / zoom(), 1 / zoom()).translate(float(width()) / -2, float(height()) / -2).map(p); // fonctionne parce que le vaisseau est toujours au milieu de l'écran
	}

	void timerEvent(QTimerEvent *) override
	{
		SDL_Event event;

		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_JOYAXISMOTION:
				{
					int joystick = joysticks_.find(event.jaxis.which)->second.index_;

					if(edit())
					{
						if(std::abs(event.jaxis.value) > 28672) joystick_axis_event(joystick, event.jaxis.axis);
					}
					else
					{
						auto axis = joystick_axes_.find(std::make_pair(joystick, event.jaxis.axis));
						if(axis != joystick_axes_.end()) axis->second.set(event.jaxis.value / 32768., hasActiveFocus());
					}
				}
				break;

				case SDL_JOYBALLMOTION:
				{
					int joystick = joysticks_.find(event.jball.which)->second.index_;

					if(edit())
					{
						joystick_ball_event(joystick, event.jball.ball);
					}
					else
					{
						auto ball = joystick_balls_.find(std::make_pair(joystick, event.jball.ball));
						if(ball != joystick_balls_.end()) ball->second.set(ball->second.back_ + QPointF(event.jball.xrel, event.jball.yrel), hasActiveFocus());
					}
				}
				break;

				case SDL_JOYHATMOTION:
				{
					int joystick = joysticks_.find(event.jhat.which)->second.index_;

					if(edit())
					{
						joystick_hat_event(joystick, event.jhat.hat);
					}
					else
					{
						if(!hasActiveFocus())
						{
							if(event.jhat.value == SDL_HAT_RIGHT) simulate_key(Qt::Key_Tab);
							if(event.jhat.value == SDL_HAT_LEFT) simulate_key(Qt::Key_Backtab);
							if(event.jhat.value == SDL_HAT_DOWN) simulate_key(Qt::Key_Down), simulate_key(Qt::Key_Left);
							if(event.jhat.value == SDL_HAT_UP) simulate_key(Qt::Key_Up), simulate_key(Qt::Key_Right);
						}

						auto hat = joystick_hats_.find(std::make_pair(joystick, event.jhat.hat));
						if(hat != joystick_hats_.end()) hat->second.set(QPointF((event.jhat.value & SDL_HAT_RIGHT ? 1 : 0) - (event.jhat.value & SDL_HAT_LEFT ? 1 : 0), (event.jhat.value & SDL_HAT_DOWN ? 1 : 0) - (event.jhat.value & SDL_HAT_UP ? 1 : 0)), hasActiveFocus());
					}
				}
				break;

				case SDL_JOYBUTTONDOWN:
				case SDL_JOYBUTTONUP:
				{
					int joystick = joysticks_.find(event.jbutton.which)->second.index_;

					if(edit())
					{
						joystick_button_event(joystick, event.jbutton.button);
					}
					else
					{
						if(!hasActiveFocus())
						{
							if(event.jbutton.state == SDL_RELEASED) simulate_key(event.jbutton.button ? Qt::Key_Space : Qt::Key_Escape);
						}

						auto button = joystick_buttons_.find(std::make_pair(joystick, event.jbutton.button));
						if(button != joystick_buttons_.end()) button->second.set(event.jbutton.state != SDL_RELEASED, hasActiveFocus());
					}
				}
				break;

				case SDL_JOYDEVICEADDED:
				{
					SDL_Joystick *joystick = SDL_JoystickOpen(event.jdevice.which);
					SDL_JoystickGUID guid = SDL_JoystickGetGUID(joystick);
					size_t index = 0;

					for(index = 0; index < joystick_guids_.size(); ++index)
					{
						if(std::equal(guid.data, guid.data + sizeof(guid.data) / sizeof(guid.data[0]), joystick_guids_[index].data))
						{
							break;
						}
					}
					if(index >= joystick_guids_.size())
					{
						joystick_guids_.push_back(guid);
					}

					joysticks_.emplace(SDL_JoystickInstanceID(joystick), Joystick(joystick, int(index)));
				}
				break;

				case SDL_JOYDEVICEREMOVED:
				{
					joysticks_.erase(event.jdevice.which);
				}
				break;
			}
		}
	}

	void itemChange(ItemChange change, const ItemChangeData &value) override
	{
		if(change == ItemSceneChange && value.window)
		{
			value.window->removeEventFilter(this);
			value.window->installEventFilter(this);
		}
	}

	bool eventFilter(QObject *object, QEvent *e) override
	{
		switch(e->type())
		{
			case QEvent::KeyPress:
			{
				Q_ASSERT(dynamic_cast<QKeyEvent *>(e));
				auto event = static_cast<QKeyEvent *>(e);
				auto key = keys_.find(event->key());

				if(key != keys_.end() && !event->isAutoRepeat())
				{
					key->second.set(true, hasActiveFocus());
				}
			}
			break;

			case QEvent::KeyRelease:
			{
				Q_ASSERT(dynamic_cast<QKeyEvent *>(e));
				auto event = static_cast<QKeyEvent *>(e);
				auto key = keys_.find(event->key());

				if(key != keys_.end() && !event->isAutoRepeat())
				{
					key->second.set(false, hasActiveFocus());
				}
			}
			break;

			case QEvent::HoverMove:
			{
				Q_ASSERT(dynamic_cast<QHoverEvent *>(e));
				auto event = static_cast<QHoverEvent *>(e);
				auto mouse = mouses_.begin();

				if(mouse != mouses_.end())
				{
					mouse->second.set(transform(event->pos()), hasActiveFocus());
				}
			}
			break;

			case QEvent::MouseMove:
			{
				Q_ASSERT(dynamic_cast<QMouseEvent *>(e));
				auto event = static_cast<QMouseEvent *>(e);
				auto mouse = mouses_.begin();

				if(mouse != mouses_.end())
				{
					mouse->second.set(transform(event->pos()), hasActiveFocus());
				}
			}
			break;

			case QEvent::MouseButtonPress:
			{
				Q_ASSERT(dynamic_cast<QMouseEvent *>(e));
				auto event = static_cast<QMouseEvent *>(e);
				auto button = mouse_buttons_.find(event->button());

				if(button != mouse_buttons_.end())
				{
					button->second.set(true, hasActiveFocus());
				}
			}
			break;

			case QEvent::MouseButtonRelease:
			{
				Q_ASSERT(dynamic_cast<QMouseEvent *>(e));
				auto event = static_cast<QMouseEvent *>(e);
				auto button = mouse_buttons_.find(event->button());

				if(button != mouse_buttons_.end())
				{
					button->second.set(false, hasActiveFocus());
				}
			}
			break;

			default:
			break;
		}

		return QQuickItem::eventFilter(object, e);
	}

	void focusInEvent(QFocusEvent *event) override
	{
		for(auto &&p: keys_) p.second.set();
		for(auto &&p: mouses_) p.second.set();
		for(auto &&p: mouse_buttons_) p.second.set();
		for(auto &&p: joystick_axes_) p.second.set();
		for(auto &&p: joystick_hats_) p.second.set();
		for(auto &&p: joystick_balls_) p.second.set();
		for(auto &&p: joystick_buttons_) p.second.set();
		return QQuickItem::focusInEvent(event);
	}

	static void simulate_key(Qt::Key key)
	{
		Q_ASSERT(qApp->focusWindow());
		qApp->postEvent(qApp->focusWindow(), new QKeyEvent(QEvent::KeyPress, key, Qt::NoModifier));
		qApp->postEvent(qApp->focusWindow(), new QKeyEvent(QEvent::KeyRelease, key, Qt::NoModifier));
	}

protected:

	InputArea()
	{
		setAcceptHoverEvents(true);
		setAcceptedMouseButtons(Qt::AllButtons);
		setCursor(QPixmap(":/controls/cursors/reticle.16.16.png"));

		if(SDL_Init(SDL_INIT_JOYSTICK) < 0) Q_UNREACHABLE();
		SDL_JoystickEventState(SDL_ENABLE);

		if(!startTimer(10)) Q_UNREACHABLE();
	}

	Q_SLOT aw::KeyEvents *key(int key_id)
	{
		return keys_[key_id].acquire();
	}

	Q_SLOT aw::MouseEvents *mouse()
	{
		return mouses_[0].acquire();
	}

	Q_SLOT aw::MouseButtonEvents *mouse_button(int button_id)
	{
		return mouse_buttons_[button_id].acquire();
	}

	Q_SLOT aw::JoystickHatEvents *joystick_hat(int joystick_id, int hat_id)
	{
		return joystick_hats_[std::make_pair(joystick_id, hat_id)].acquire();
	}

	Q_SLOT aw::JoystickBallEvents *joystick_ball(int joystick_id, int ball_id)
	{
		return joystick_balls_[std::make_pair(joystick_id, ball_id)].acquire();
	}

	Q_SLOT aw::JoystickAxisEvents *joystick_axis(int joystick_id, int axis_id)
	{
		return joystick_axes_[std::make_pair(joystick_id, axis_id)].acquire();
	}

	Q_SLOT aw::JoystickButtonEvents *joystick_button(int joystick_id, int button_id)
	{
		return joystick_buttons_[std::make_pair(joystick_id, button_id)].acquire();
	}

	Q_SLOT static QString key_name(int id)
	{
		switch(id)
		{
			#define _(x) case Qt::Key_##x: return tr(#x);
			_(Shift)
			_(Control)
			_(Meta)
			_(Alt)
			#undef _
			default: return QKeySequence(id).toString(QKeySequence::NativeText).toHtmlEscaped();
		}
	}

	Q_SIGNAL void joystick_axis_event(int joystick, int axis);
	Q_SIGNAL void joystick_ball_event(int joystick, int ball);
	Q_SIGNAL void joystick_hat_event(int joystick, int hat);
	Q_SIGNAL void joystick_button_event(int joystick, int button);
};

QPointF InputArea::mouse_position() const
{
	Q_ASSERT(window());
	return transform(mapFromScene(window()->mapFromGlobal(QCursor::pos())));
}

AW_DEFINE_OBJECT_STUB(InputArea)
AW_DEFINE_PROPERTY_STORED(InputArea, zoom)
AW_DEFINE_PROPERTY_STORED(InputArea, angle)
AW_DEFINE_PROPERTY_STORED(InputArea, edit)
AW_REGISTER_ABSTRACT_TYPE(JoystickAxisEvents)
AW_REGISTER_ABSTRACT_TYPE(JoystickHatEvents)
AW_REGISTER_ABSTRACT_TYPE(JoystickBallEvents)
AW_REGISTER_ABSTRACT_TYPE(JoystickButtonEvents)
AW_REGISTER_ABSTRACT_TYPE(KeyEvents)
AW_REGISTER_ABSTRACT_TYPE(MouseButtonEvents)
AW_REGISTER_ABSTRACT_TYPE(MouseEvents)

}
}
