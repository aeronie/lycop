#pragma once

#include <QSGNode>

namespace aw {

template<class _node_type>
struct NodePool
: private QSGNode
{
	_node_type *acquire()
	{
		if(QSGNode *node = firstChild())
		{
			Q_ASSERT(dynamic_cast<_node_type *>(node));
			removeChildNode(node);
			return static_cast<_node_type *>(node);
		}
		else
		{
			return new _node_type;
		}
	}

	void release(QSGNode *node)
	{
		Q_ASSERT(dynamic_cast<_node_type *>(node));
		node->parent()->removeChildNode(node);
		appendChildNode(node);
	}
};

}
