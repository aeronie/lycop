#pragma once

namespace aw {

template<class _parent_type>
struct CheatPtr
{
	CheatPtr(_parent_type *parent): parent_(*parent), ptr_(parent_.get()) {}
	~CheatPtr() { if(ptr_ != parent_.get()) parent_.reset(ptr_); }
	operator typename _parent_type::pointer *() { return &ptr_; }
private:
	_parent_type &parent_;
	typename _parent_type::pointer ptr_;
};

template<class _ptr_type>
CheatPtr<_ptr_type> cheat(_ptr_type *ptr)
{
	return CheatPtr<_ptr_type>(ptr);
}

}
