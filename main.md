# Les aventures du capitaine Lycop : L’invasion des Heters {#mainpage}

\todo
- effet visuel à l'impact + explosion
- vaisseau avec PV répartis dans plusieurs modules
- outil pour placer les modules
- effets sonnores
- réseau

\see
- http://qt-project.org/doc/qt-5/index.html
- http://box2d.org/manual.pdf
- http://wiki.libsdl.org/APIByCategory
