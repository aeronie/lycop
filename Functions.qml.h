#include <QtGui>

#include "meta.private.h"

namespace aw {
namespace {

struct Functions
: QObject
{
	AW_DECLARE_OBJECT_STUB(Functions)

public Q_SLOTS:

	static int version()
	{
		return
		#if defined CONFIG_DEMO
		-
		#endif
		#include "version.h"
		;
	}

	static QPointF get_mouse_position(QWindow *window)
	{
		return window->mapFromGlobal(QCursor::pos());
	}
};

AW_DEFINE_OBJECT_STUB(Functions)

}
}
