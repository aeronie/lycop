#include "ControlInfo.h"
#include "meta.private.h"

namespace aw {
namespace {

struct Info
: QObject
{
	Q_OBJECT
public:
	static QList<ControlInfo> const &info()
	{
		static const QList<ControlInfo> info
		{
			{tr("enable"), "enable", 0},
			{tr("disable"), "disable", 0},
		};
		return info;
	}
};

struct BooleanControl
: QObject
{
	AW_DECLARE_OBJECT_STUB(BooleanControl)
	AW_DECLARE_PROPERTY_STORED(QString, text);
	AW_DECLARE_PROPERTY_STORED(bool, checked) = false;
	AW_DECLARE_PROPERTY_STORED_READONLY(bool, value) = false;

public Q_SLOTS:

	QObject *info() const
	{
		return ControlInfo::model(Info::info(), objectName());
	}

	void enable(bool value)
	{
		value_ = value;
		Q_EMIT value_changed(value_);
	}

	void disable(bool value)
	{
		enable(!value);
	}
};

AW_DEFINE_OBJECT_STUB(BooleanControl)
AW_DEFINE_PROPERTY_STORED(BooleanControl, text)
AW_DEFINE_PROPERTY_STORED(BooleanControl, checked)

}
}
