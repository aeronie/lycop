#pragma once

#include <qqml.h>

#include "meta.h"

#define AW_DEFINE_OBJECT_STUB(_class) \
\
	static const register_type registered_type_##_class = qmlRegisterType<_class>(AW_REGISTER_TYPE_(_class)); \

#define AW_REGISTER_ABSTRACT_TYPE(_class) \
\
	static const register_type registered_type_##_class = qmlRegisterUncreatableType<_class>(AW_REGISTER_TYPE_(_class), QString()); \

#define AW_REGISTER_TYPE_(_class) QString(_class::staticMetaObject.className()).replace("::", ".").section(".", 0, -2).toUtf8(), 0, 0, QString(_class::staticMetaObject.className()).section("::", -1).toUtf8()

#define AW_DEFINE_PROPERTY_STORED(_class, _name) \
\
	void _class::set_##_name(decltype(((_class *)(0))->_name##_) const &_name) \
	{ \
		if(_name##_ == _name) return; \
		_name##_ = _name; \
		Q_EMIT _name##_changed(_name##_); \
	} \

