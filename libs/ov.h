#pragma once

#include <vorbis/vorbisfile.h>

#include "common.h"

#define AW_CHECK_OV(_expression) AW_CHECK_(::aw::check_ov, _expression)

#if !defined NDEBUG

namespace aw {

template<class T>
T check_ov(T &&x, void (*throw_)(char const *))
{
	if(x < 0) switch(x)
	{
		#define _(x) case x: throw_(#x);
		_(OV_FALSE)
		_(OV_HOLE)
		_(OV_EREAD)
		_(OV_EFAULT)
		_(OV_EIMPL)
		_(OV_EINVAL)
		_(OV_ENOTVORBIS)
		_(OV_EBADHEADER)
		_(OV_EVERSION)
		_(OV_EBADLINK)
		_(OV_ENOSEEK)
		#undef _
		default: throw_(0);
	}
	return x;
}

}

#endif
