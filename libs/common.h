#pragma once

#include <QtGlobal>

#if defined NDEBUG
#define AW_CHECK_(_function, _expression) (_expression)
#else
#define AW_CHECK_(_function, _expression) _function(_expression, [] (char const *error) { qt_assert_x(#_expression, error ? error : "error", __FILE__, __LINE__); })
#endif
