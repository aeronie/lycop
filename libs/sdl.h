#pragma once

#include <SDL.h>

#include "common.h"

#define AW_CHECK_SDL(_expression) AW_CHECK_(::aw::check_sdl, _expression)
#define AW_CHECK_SDL_PTR(_expression) AW_CHECK_(::aw::check_sdl_ptr, _expression)

#if !defined NDEBUG

namespace aw {

template<class T>
T check_sdl(T &&x, void (*throw_)(char const *))
{
	if(x < 0) throw_(SDL_GetError());
	return x;
}

template<class T>
T check_sdl_ptr(T &&x, void (*throw_)(char const *))
{
	if(!x) throw_(SDL_GetError());
	return x;
}

}

#endif
