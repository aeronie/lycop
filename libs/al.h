#pragma once

#include <alext.h>

#include "common.h"

#define AW_CHECK_AL(_expression) AW_CHECK_(::aw::check_al, (_expression, 0))

#if !defined NDEBUG

namespace aw {

inline void check_al(int, void (*throw_)(char const *))
{
	ALenum error = alGetError();
	if(error != AL_NO_ERROR) throw_(alGetString(error));
}

}

#endif
