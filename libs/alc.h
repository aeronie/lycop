#pragma once

#include <memory>

#include "Mutex.h"
#include "common.h"

namespace aw {

struct UseAL
{
	UseAL(): lock_(mutex()) {}
	UseAL(std::try_to_lock_t const &tag): lock_(mutex(), tag) {}
	operator bool() const { return lock_.owns_lock(); }
private:
	std::unique_lock<Mutex> lock_;
	static Mutex &mutex();
};

}
