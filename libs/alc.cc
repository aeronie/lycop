#include <alc.h>

#include "alc.h"
#include "al.h"

#define _CHECK(_expression) AW_CHECK_(check, _expression)

namespace aw {
namespace {

#if !defined NDEBUG

template<class T>
static T check(T &&x, void (*throw_)(char const *))
{
	ALCdevice *device = alcGetContextsDevice(alcGetCurrentContext());
	ALCenum error = alcGetError(device);
	if(!x) throw_(alcGetString(device, error));
	return x;
}

#endif

struct Deleter
{
	void operator()(ALCdevice *p) const { _CHECK(alcCloseDevice(p)); }
	void operator()(ALCcontext *p) const { _CHECK((alcDestroyContext(p), true)); }
};

struct Context
{
	std::unique_ptr<ALCdevice, Deleter> device_;
	std::unique_ptr<ALCcontext, Deleter> context_;

	Context()
	: device_(_CHECK(alcOpenDevice(0)))
	, context_(_CHECK(alcCreateContext(device_.get(), 0)))
	{
		static const float orientation[] {0, 1, 0, 0, 0, 1};
		_CHECK(alcMakeContextCurrent(context_.get()));
		AW_CHECK_AL(alDistanceModel(AL_NONE));
		AW_CHECK_AL(alListenerfv(AL_ORIENTATION, orientation));
	}
};

}

Mutex &UseAL::mutex()
{
	static Context context_;
	static Mutex mutex;
	return mutex;
}

}
