uniform sampler2D color_map;
uniform sampler2D normal_map;
uniform bool has_normal_map;
uniform float qt_Opacity;
varying vec2 fragment_in_color_map_;
varying vec2 fragment_in_normal_map_;
varying vec2 direction_;
varying vec4 mask_;

void main()
{
	vec4 raw_color = texture2D(color_map, fragment_in_color_map_);
	if(raw_color.a == 0.) discard;
	gl_FragColor = raw_color * mask_ * qt_Opacity;

	#ifdef DIFFUSE

	if(!has_normal_map) return;
	vec4 raw_normal = texture2D(normal_map, fragment_in_normal_map_);
	vec3 light_direction = vec3(-direction_.y * .8, direction_.x * .8, .6); // normalisé
	vec3 normal = 2. * raw_normal.rgb - vec3(1, 1, 1); // normalisé
	float diffuse = dot(normal, light_direction);
	gl_FragColor.rgb *= diffuse;

	#ifdef SPECULAR

	if(diffuse > 0.)
	{
		float specular = reflect(light_direction, normal).z;

		if(specular > 0.)
		{
			gl_FragColor.rgb += .5 * pow(specular, 5) * gl_FragColor.aaa;
		}
	}

	#endif
	#endif
}
