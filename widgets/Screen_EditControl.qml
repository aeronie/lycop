import QtQuick 2.4
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"
import "qrc:///controls/styles" as S

ColumnLayout
{
	property var form
	signal triggered_back()
	Keys.onEscapePressed: triggered_back()
	Keys.onPressed: area.keyPressed(event)
	Component.onCompleted: inputs.edit = true
	Component.onDestruction: inputs.edit = false
	Label
	{
		text: qsTr("%1: %2").arg(form.data1("text")).arg(form.data2("text"))
	}
	MouseArea
	{
		id: area
		Layout.fillWidth: true
		Layout.fillHeight: true
		acceptedButtons: Qt.AllButtons
		S.ScrollView_frame {anchors.fill: parent; readonly property alias control: area}
		signal keyPressed(var event)
		function apply(type, value, event)
		{
			if(event) event.accepted = true

			if(!enabled) return
			enabled = false

			value = type + "," + value

			for(var i1 = 0; i1 < controls.all.length; ++i1)
			{
				var x1 = controls.all[i1]
				var info = x1.info()
				for(var i2 = 0; i2 < info.count(); ++i2)
				{
					var x = info.get(i2, "key")
					if(settings.get(x) == value) return form.Stack.view.push({item: screen_confirm_control, replace: true, properties: {x1: qsTr("%1: %2").arg(form.data1("text")).arg(form.data2("text")), x2: qsTr("%1: %2").arg(x1.text).arg(info.get(i2, "text")), y1: form.objectName, y2: x}})
				}
			}

			settings.set(form.objectName, value)
			triggered_back()
			controls.update()
		}
		Label
		{
			anchors.fill: parent
			anchors.margins: font.pixelSize
			horizontalAlignment: Text.AlignHCenter
			verticalAlignment: Text.AlignVCenter
			wrapMode: Text.WordWrap
			text: [qsTr("help 0"), qsTr("help 1"), qsTr("help 2")][form.data2("type")]
		}
		Connections
		{
			target: form.data2("type") == 0 ? area : null
			onClicked: area.apply(1, mouse.button, mouse)
			onWheel: area.apply(2, wheel.angleDelta.x < 0 ? 2 : wheel.angleDelta.x > 0 ? 0 : wheel.angleDelta.y < 0 ? 3 : 1, wheel)
			onKeyPressed: area.apply(3, event.key, event)
		}
		Connections
		{
			target: form.data2("type") == 2 ? area : null
			onClicked: area.apply(4, 0, mouse)
		}
		Connections
		{
			target: area.enabled && form.data2("type") == 0 ? inputs : null
			onJoystick_button_event: area.apply(5, joystick + "," + button)
		}
		Connections
		{
			target: area.enabled && form.data2("type") == 1 ? inputs : null
			onJoystick_axis_event: area.apply(6, joystick + "," + axis)
		}
		Connections
		{
			target: area.enabled && form.data2("type") == 2 ? inputs : null
			onJoystick_hat_event: area.apply(7, joystick + "," + hat)
			onJoystick_ball_event: area.apply(8, joystick + "," + ball)
		}
	}
	Buttons
	{
		model: [action_back]
	}
	Action
	{
		id: action_back
		text: qsTr("Back")
		onTriggered: triggered_back()
	}
}
