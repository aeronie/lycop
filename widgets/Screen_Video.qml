import QtQuick 2.0
import QtQuick.Controls 1.0
import aw 0.0

Rectangle
{
	id: screen
	signal triggered_open()
	signal triggered_close()
	readonly property alias video: video
	color: "black"
	Timer
	{
		interval: 20
		repeat: true
		running: video.playing
		onTriggered: video.update()
	}
	VideoPlayer
	{
		id: video
		anchors.fill: parent
		playing: file && screen.Stack.status == Stack.Active
		gain: playing ? settings.video_gain : 0
		onFileChanged:
		{
			if(file) triggered_open()
			else triggered_close()
		}
	}
	MouseArea
	{
		anchors.fill: parent
		enabled: video.playing
		onClicked: video.file = ""
		cursor: "arrow-e"
	}
}
