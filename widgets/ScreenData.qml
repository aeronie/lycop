import QtQuick 2.0

QtObject
{
	property QtObject target
	property string text
	property string icon
	property bool blink: no
	readonly property bool no: false
}
