import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"
import "qrc:///controls/styles" as S

Rectangle
{
	id: root
	signal triggered_retry()
	signal triggered_load()
	color: "black"
	property bool change_int: false
	property int ind_tab_hint: 0
	property var tab_hint:[
		qsTr("Pensez à utiliser le celeritas accretio pour vous déplacer plus vite"),
		qsTr("Le bouclier à percussion désactive le bouclier Zoubidou Classix mais pas le bouclier auxiliaire Zoubidou Companion"),
		qsTr("L'accélerateur linéaire vous permet passivement de vous déplacer plus vite"),
		qsTr("Le générateur de replis de l’espace permet de vous téléporter. Si vous vous téléportez sur un petit ennemi, vous échangerez de position"),
		qsTr("Vous manquez de DPS ? Utilisez le Modus accretio")
	]
	property int tab_hint_max: 5 // je ne sais pas pourquoi mais ici tab_hint.length est undefined
	opacity: 0

	onChange_intChanged:
	{
		root.ind_tab_hint = random_index(root.tab_hint_max)
	}
	SequentialAnimation
	{
		id: animation
		running: true
		PauseAnimation {duration: 1000}
		NumberAnimation {target: root; property: "opacity"; to: 1; duration: 2000}
		NumberAnimation {target: buttons.children[0]; property: "opacity"; to: 1; duration: 2000}
		NumberAnimation {target: buttons.children[1]; property: "opacity"; to: 1; duration: 2000}
		NumberAnimation {target: buttons.children[3]; property: "opacity"; to: 1; duration: 2000} // dafuq?
	}
	MouseArea
	{
		anchors.fill: parent
		onClicked: animation.complete()
	}
	Component.onCompleted: 
	{
		for(var i = 0; i < buttons.children.length; ++i)
		{
			buttons.children[i].opacity = 0
		}
	}
	ColumnLayout
	{
		anchors.centerIn: parent
		Label
		{
			Layout.fillWidth: true
			horizontalAlignment: Text.AlignHCenter
			text: qsTr("Game over")
			font.pointSize: S.Private.font.pointSize * 3
		}
		Label
		{
			Layout.fillWidth: true
			horizontalAlignment: Text.AlignHCenter
			text: ""
			font.pointSize: S.Private.font.pointSize * 0.5
		}
		Label
		{
			Layout.fillWidth: true
			horizontalAlignment: Text.AlignHCenter
			text: qsTr("Conseil : %1").arg(root.tab_hint[root.ind_tab_hint])
			font.pointSize: S.Private.font.pointSize
		}
		Label
		{
			Layout.fillWidth: true
			horizontalAlignment: Text.AlignHCenter
			text: ""
			font.pointSize: S.Private.font.pointSize * 0.5
		}
		Buttons
		{
			id: buttons
			Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
			flow: GridLayout.TopToBottom
			style: S.MenuButton {}
			model: [
				Action {text: qsTr("Retry"); onTriggered: triggered_retry()},
				Action {text: qsTr("Load"); onTriggered: triggered_load()},
				Action {text: qsTr("Quit"); onTriggered: window.back_to_main_menu()}]
		}
	}
}
