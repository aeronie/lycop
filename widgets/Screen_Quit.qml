import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"
import "qrc:///controls/styles" as S

FocusScope
{
	id: root
	signal triggered_back()
	readonly property bool is_screen_quit: true
	Keys.onEscapePressed: root.triggered_back()
	Rectangle
	{
		anchors.fill: parent
		color: "black"
	}
	ColumnLayout
	{
		anchors.fill: parent
		anchors.margins: spacing
		spacing: 20
		Item
		{
			Layout.fillHeight: true
			Layout.preferredHeight: 1
		}
		Label
		{
			Layout.fillWidth: true
			text: qsTr("title")
			horizontalAlignment: Text.AlignHCenter
			font.pointSize: S.Private.font.pointSize * 2
		}
		Label
		{
			Layout.fillWidth: true
			text: qsTr("prompt")
			wrapMode: Text.Wrap
		}
		Buttons
		{
			model: [action_quit, action_back]
		}
		Item
		{
			Layout.fillHeight: true
			Layout.preferredHeight: 3
		}
	}
	Action
	{
		id: action_back
		text: qsTr("Cancel")
		onTriggered: triggered_back()
	}
	Action
	{
		id: action_quit
		text: qsTr("Quit")
		onTriggered: Qt.quit()
	}
}
