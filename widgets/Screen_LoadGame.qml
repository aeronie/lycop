import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"

GridLayout
{
	id: root
	columns: 2
	objectName: table_1.currentRow < 0 ? "" : "games/" + table_1.model[table_1.currentRow]
	readonly property string target: table_2.currentRow < 0 ? "" : objectName + "/" + table_2.model[table_2.currentRow]
	signal triggered_back()
	signal triggered_load(string target)
	signal triggered_edit(string name)
	signal triggered_remove_1(string name)
	signal triggered_remove_2(string name)
	Keys.onEscapePressed: triggered_back()
	TableView
	{
		id: table_1
		Layout.fillWidth: true
		Layout.fillHeight: true
		Layout.preferredWidth: 1
		headerVisible: false
		TableViewColumn {readonly property var value: function(x) {return settings.name_of("games/" + x.value)}}
		model: settings.self.children_of("games")
		onPressAndHold: buttons_1.show_menu()
	}
	TableView
	{
		id: table_2
		Layout.fillWidth: true
		Layout.fillHeight: true
		Layout.preferredWidth: 2
		TableViewColumn {title: qsTr("Date"); function value(x) {return settings.date_of(x.value).toLocaleString() || ""}}
		TableViewColumn {title: qsTr("Summary"); function value(x) {var a = /([^\/]*)\.\w*$/.exec(settings.get(root.objectName + "/" + x.value + "/file")); return a ? (qsTranslate)(a[1], "title") : ""}}
		model: table_1.currentRow < 0 ? null : settings.self.children_of(root.objectName)
		onPressAndHold: buttons_2.show_menu()
		onDoubleClicked: action_load.trigger()
	}
	Buttons
	{
		id: buttons_1
		Layout.alignment: Qt.AlignLeft
		model: [action_edit, action_remove_1]
	}
	Buttons
	{
		id: buttons_2
		model: [action_load, action_remove_2, action_back]
	}
	Action
	{
		id: action_edit
		text: qsTr("Edit")
		enabled: table_1.currentRow >= 0
		onTriggered: triggered_edit(root.objectName)
	}
	Action
	{
		id: action_remove_1
		text: qsTr("Remove")
		enabled: table_1.currentRow >= 0
		onTriggered: triggered_remove_1(root.objectName)
	}
	Action
	{
		id: action_load
		text: qsTr("Load")
		enabled: table_2.currentRow >= 0
		onTriggered: triggered_load(root.target)
	}
	Action
	{
		id: action_remove_2
		text: qsTr("Remove")
		enabled: table_2.currentRow >= 0
		onTriggered: triggered_remove_2(root.target)
	}
	Action
	{
		id: action_back
		text: qsTr("Back")
		onTriggered: triggered_back()
	}
}
