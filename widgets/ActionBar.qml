import QtQuick 2.0
import QtQml.Models 2.1
import aw 0.0

ListView
{
	id: root
	property alias actions: model.model
	displaced: Transition {NumberAnimation {property: "x"; easing.type: Easing.OutQuad}}
	orientation: ListView.Horizontal
	interactive: false
	QtObject
	{
		id: that
		property QtObject button: null
		property QtObject key: null
		property var indexes: []
		function grow_indexes() {for(var i = indexes.length; i < model.items.count; ++i) indexes.push(i)}
		function real_index_of(index) {for(var i = 0; i < model.items.count; ++i) if(model.items.get(i).model.index == indexes[index]) return i}
		onButtonChanged:
		{
			if(button)
			{
				function callback(x)
				{
					button.control.enable(x)
					if(!x) button.checked = false
				}
				key = inputs.mouse_button(Qt.LeftButton)
				key.pressed.connect(callback)
			}
			else if(key)
			{
				key.destroy()
				key = null
			}
		}
	}
	model: DelegateModel
	{
		id: model
		delegate: MouseArea
		{
			id: container
			width: root.height
			height: root.height
			acceptedButtons: Qt.AllButtons // le drag'n'drop avec un bouton pas accepté fait n'importe quoi
			drag.target: button
			drag.axis: Drag.XAxis
			drag.filterChildren: true
			drag.smoothed: false
			onPressed:
			{
				button.Drag.hotSpot.x = mouseX
				button.Drag.hotSpot.y = mouseY
			}
			EquipmentButton
			{
				id: button
				visible: !!modelData
				health: modelData && modelData.equipment && modelData.equipment.health ? modelData.equipment.health / modelData.equipment.max_health : 0
				shield: modelData && modelData.equipment && modelData.equipment.shield && modelData.equipment.body.max_secondary_shield ? modelData.equipment.shield / modelData.equipment.body.max_secondary_shield : 0
				cooldown: modelData && modelData.equipment && modelData.equipment.time && modelData.equipment.period ? modelData.equipment.time / modelData.equipment.period : 0
				property QtObject control: modelData ? modelData.control : null
				signal set_checked(bool x)
				width: container.width
				height: container.height
				anchors.horizontalCenter: container.horizontalCenter
				anchors.verticalCenter: container.verticalCenter
				onSet_checked: checked = x
				iconName: modelData ? "%1,%2,%3".arg(modelData.name).arg(0).arg(!modelData.consumable || modelData.consumable.available ? 0 : 1) : ""
				Drag.active: container.drag.active
				Drag.source: container
				onIsDefaultChanged: modelData.control.checked = isDefault
				tooltip:
				{
					if(!modelData) return ""
					var a = []
					controls.get_bindings(a, modelData.control)
					controls.get_bindings(a, controls.buttons[container.DelegateModel.itemsIndex])
					return "<b>%1</b><br>%2".arg(modelData.text).arg(a.join(', '))
				}
				Component.onCompleted: if(modelData)
				{
					isDefault = modelData.control.checked
					switch(modelData.type)
					{
						case 1:
						clicked.connect(function(){modelData.control.enable(true)})
						cooldownChanged.connect(function(){modelData.control.enable(false)})
						break

						case 2:
						checkable = true
						checked = modelData.control.value
						modelData.control.value_changed.connect(set_checked)
						break

						case 3:
						checkable = true
						checked = that.button === this
						break
					}
				}
				onCheckedChanged: if(modelData)
				{
					switch(modelData.type)
					{
						case 2:
						modelData.control.enable(checked)
						break

						case 3:
						if(checked)
						{
							if(that.button) that.button.checked = false
							that.button = this
						}
						else
						{
							if(that.button === this) that.button = null
						}
						break
					}
				}
				Connections
				{
					target: controls.buttons[container.DelegateModel.itemsIndex]
					onValueChanged: if(modelData) modelData.control.enable(target.value)
				}
				Connections
				{
					target: button.isDefault ? controls.secondary_weapons : null
					onValueChanged: if(modelData) modelData.control.enable(target.value)
				}
				states: State
				{
					when: button.Drag.active
					ParentChange {target: button; parent: root}
					AnchorChanges {target: button; anchors.horizontalCenter: undefined; anchors.verticalCenter: undefined}
				}
			}
			DropArea
			{
				anchors.fill: parent
				onEntered:
				{
					that.grow_indexes()
					var i = drag.source.DelegateModel.itemsIndex
					var j = container.DelegateModel.itemsIndex
					that.indexes.splice(j, 0, that.indexes.splice(i, 1)[0])
					root.model.items.move(i, j)
				}
			}
		}
		items.onChanged:
		{
			that.grow_indexes()
			for(var i = 0; i < items.count - 1; ++i)
			{
				var j = that.real_index_of(i)
				if(i == j) continue
				root.model.items.move(j, i)
				root.model.items.move(i + 1, j)
			}
		}
	}
}
