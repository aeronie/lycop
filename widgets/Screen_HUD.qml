import QtQuick 2.0
import QtQuick.Layouts 1.0
import aw 0.0
import "qrc:///controls"
import "qrc:///controls/styles" as S

InputArea
{
	id: screen
	signal triggered_show(int index)
	property alias screens: repeater.model
	property alias scene: view_scene.scene
	property alias center: view_scene.center
	property bool last_level: false
	property bool credit: false
	property QtObject ship
	GridLayout
	{
		anchors.left: parent.left
		anchors.right: parent.right
		anchors.bottom: parent.bottom
		anchors.margins: rowSpacing
		RowLayout
		{
			Layout.row: 0
			Layout.column: 0
			Repeater
			{
				id: repeater
				Image
				{
					id: root
					source: "show-" + modelData.icon + ".png"
					opacity: (!screen.last_level || (!credit && index == 3) || index == 4) ? (modelData.blink || area.containsMouse ? 1 : 0.5) : 0.3
					SequentialAnimation
					{
						running: modelData.blink
						loops: Animation.Infinite
						NumberAnimation {target: root; property: "opacity"; from: 0; to: 1; easing.type: Easing.InOutExpo}
						NumberAnimation {target: root; property: "opacity"; from: 1; to: 0; easing.type: Easing.InOutExpo}
					}
					ToolTip
					{
						id: area
						acceptedButtons: Qt.AllButtons
						onClicked: 
						{
							if(!screen.last_level || (!credit && index == 3) || index == 4)
							{
								screen.triggered_show(index)
							}
						}
						text: modelData.text
						cursor: (!screen.last_level || (!credit && index == 3) || index == 4)  ? "hand-pointing" : ""
					}
				}
			}
		}
		S.ScrollView_frame
		{
			Layout.row: 1
			Layout.column: 0
			Layout.rowSpan: 3
			Layout.preferredWidth: 120
			Layout.preferredHeight: 100
			readonly property alias control: view_scene
			clip: true
			RadarView
			{
				id: view_scene
				anchors.fill: parent
				zoom: 1
				opacity: !screen.last_level ? 1 : 0
			}
			Connections
			{
				target: view_scene.scene
				onImages_changed: view_scene.update()
			}
		}
		StatusBar
		{
			Layout.row: 2
			Layout.column: 1
			energy: window.energy
			matter: saved_game.matter
			science: saved_game.science
			show_only_health: screen.last_level
			show_nothing: screen.credit
			shield: ship ? ship.shield : 0
			health:
			{
				if(!ship) return 0
				var x = 0
				for(var i = 0; i < ship.slots.length; ++i)
				{
					var a = ship.slots[i].equipment
					if(a) x += a.health
				}
				return x + 1
			}
		}
		ActionBar
		{
			Layout.row: 3
			Layout.column: 1
			Layout.fillWidth: true
			height: S.Private.lineSpacing * 3
			spacing: parent.rowSpacing
			actions:
			{
				var a = []
				if(ship)
				{
					if(!screen.last_level)
					{
						var b = [controls.speed_boost, controls.weapons_boost, controls.shield_boost]
						for(var i in b) a.push({name: b[i].objectName, control: b[i], type: 2, consumable: ship ? ship.consumable : null, text: b[i].text})
					}
					if(!credit)
					{
						for(var i in ship.slots) a.push(ship.slots[i].equipment ? {name: ship.slots[i].data.objectName, control: controls.slots[i], type: ship.slots[i].data.control, equipment: ship.slots[i].equipment, consumable: ship.slots[i].equipment.consumable, text: ship.slots[i].data.title} : undefined)
					}
				}
				return a
			}
		}
	}
}
