import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"
import "qrc:///controls/styles" as S

GridLayout
{
	id: root
	signal triggered_back()
	signal triggered_edit()
	signal triggered_reset()
	Keys.onEscapePressed: triggered_back()
	function data1(x) {return table_1.model[table_1.currentRow][x]}
	function data2(x) {return table_2.currentRow < 0 ? "0" : table_2.model.get(table_2.currentRow, x)}
	objectName: data2("key")
	TableView
	{
		id: table_1
		Layout.row: 0
		Layout.column: 0
		Layout.fillWidth: true
		Layout.fillHeight: true
		headerVisible: false
		model: controls.all
		TableViewColumn {function value(x) {return x.value.text}}
	}
	TableView
	{
		id: table_2
		Layout.row: 0
		Layout.column: 1
		Layout.fillWidth: true
		Layout.fillHeight: true
		model: table_1.currentRow < 0 ? null : data1("info")(settings.self)
		section.property: "type"
		section.delegate: Rectangle
		{
			color: Qt.rgba(0.5, 0, 0, 0.5)
			width: Math.max(parent.width, table_2.contentItem.width)
			height: S.Private.lineSpacing * 1.5
			Label
			{
				anchors.fill: parent
				anchors.leftMargin: 3
				text: [qsTr("Buttons"), qsTr("Axes"), qsTr("Mouses / hats")][section]
				verticalAlignment: Text.AlignVCenter
			}
		}
		TableViewColumn
		{
			role: "text"
			title: qsTr("Slot")
		}
		TableViewColumn
		{
			role: "key"
			title: qsTr("Binding")
			function value(x) {return controls.to_string(x.value)}
		}
		onPressAndHold: buttons.show_menu()
		onDoubleClicked: action_edit.trigger()
	}
	Buttons
	{
		Layout.row: 1
		Layout.column: 0
		Layout.alignment: Qt.AlignLeft
		model: [action_reset]
	}
	Buttons
	{
		id: buttons
		Layout.row: 1
		Layout.column: 1
		model: [action_edit, action_remove, action_back]
	}
	Action
	{
		id: action_reset
		text: qsTr("Reset")
		onTriggered: triggered_reset()
	}
	Action
	{
		id: action_edit
		text: qsTr("Edit")
		enabled: table_2.currentRow >= 0
		onTriggered: triggered_edit()
	}
	Action
	{
		id: action_remove
		text: qsTr("Remove")
		enabled: table_2.currentRow >= 0
		onTriggered:
		{
			settings.remove(root.objectName)
			controls.update()
		}
	}
	Action
	{
		id: action_back
		text: qsTr("Back")
		onTriggered: triggered_back()
	}
}
