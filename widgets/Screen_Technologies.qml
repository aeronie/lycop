import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"

Image
{
	source: technologies.qui_poutre.enabled ? "technologies.3.png" : technologies.vortex_gun.enabled ? "technologies.2.png" : technologies.plasma_spray.enabled ? "technologies.1.png" : "technologies.png"
	fillMode: Image.PreserveAspectFit
	verticalAlignment: Image.AlignTop
	Repeater
	{
		id: view
		property int currentIndex: 0
		model: technologies.data
		Image
		{
			visible: modelData.enabled || modelData.y < 4
			source: modelData.icon()
			x: (modelData.x + 0.5) * parent.paintedWidth / 9 + (parent.width - parent.paintedWidth - width) / 2
			y: (modelData.y + 0.5) * parent.paintedHeight / 7 - height / 2
			width: 0.7 * parent.paintedWidth / 9
			height: 0.7 * parent.paintedHeight / 7
			Rectangle
			{
				anchors.fill: parent
				anchors.margins: -1
				color: "transparent"
				border.color: "white"
				border.width: 2
				radius: 2
			}
			BorderImage
			{
				anchors.fill: parent
				anchors.margins: -6
				visible: index == view.currentIndex
				source: modelData.x < 4 ? "area-F60.png" : modelData.x < 7 ? "area-0F0.png" : "area-06F.png"
				border.left: 20
				border.right: 20
				border.top: 20
				border.bottom: 20
			}
			ToolTip
			{
				acceptedButtons: Qt.LeftButton | Qt.RightButton
				onPressed: view.currentIndex = index
				onPressAndHold: buttons.show_menu()
				onClicked: if(mouse.button == Qt.RightButton) pressAndHold(mouse)
				onDoubleClicked: action_research.trigger()
				text: modelData.title + "<br>" + (modelData.enabled ? modelData.level ? qsTr("Level %1").arg(modelData.level) : qsTr("Enabled") : qsTr("Disabled"))
			}
		}
	}
	TextArea
	{
		id: text
		anchors.left: parent.left
		anchors.bottom: parent.bottom
		height: parent.height - parent.paintedHeight * 4 / 7
		width: (parent.width - (technologies.plasma_spray.enabled ? parent.paintedWidth * 1 / 9 : 5)) / 2
		text: view.model[view.currentIndex].description
	}
	ColumnLayout
	{
		anchors.bottom: parent.bottom
		anchors.right: parent.right
		width: text.width
		Label {text: qsTr("Science: %1 points").arg(saved_game.science)}
		Buttons
		{
			id: buttons
			model: [action_research]
			flow: GridLayout.TopToBottom
			Layout.fillWidth: true
		}
	}
	Action
	{
		id: action_research
		text: qsTr("Research")
		enabled: saved_game.science && view.model[view.currentIndex].enabled && view.model[view.currentIndex].level < 2
		onTriggered:
		{
			view.model[view.currentIndex].add_level(1)
			saved_game.add_science(-1)
		}
	}
}
