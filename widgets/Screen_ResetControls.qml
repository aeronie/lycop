import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"

ColumnLayout
{
	signal triggered_back()
	Keys.onEscapePressed: triggered_back()
	Label {text: qsTr("prompt"); Layout.fillWidth: true; wrapMode: Text.Wrap}
	Buttons {model: [action_reset, action_back]}
	Item {Layout.fillHeight: true}
	Action
	{
		id: action_back
		text: qsTr("Cancel")
		onTriggered: triggered_back()
	}
	Action
	{
		id: action_reset
		text: qsTr("Reset")
		onTriggered:
		{
			controls.reset_all()
			triggered_back()
		}
	}
}
