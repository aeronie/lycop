import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"

ColumnLayout
{
	property alias x1: a1.text
	property alias x2: a2.text
	property string y1
	property string y2
	signal triggered_back()
	Keys.onEscapePressed: triggered_back()
	spacing: 20
	Item
	{
		Layout.fillHeight: true
		Layout.preferredHeight: 1
	}
	Label
	{
		Layout.fillWidth: true
		text: qsTr("prompt %1 %2").arg(controls.to_string(y2)).arg(x2)
		wrapMode: Text.Wrap
	}
	Buttons
	{
		Layout.alignment: Qt.AlignCenter
		flow: GridLayout.TopToBottom
		model: [a1, a2]
	}
	Item
	{
		Layout.fillHeight: true
		Layout.preferredHeight: 3
	}
	Action
	{
		id: a1
		text: x1
		onTriggered:
		{
			settings.set(y1, settings.get(y2))
			settings.remove(y2)
			triggered_back()
			controls.update()
		}
	}
	Action
	{
		id: a2
		text: x2
		onTriggered: triggered_back()
	}
}
