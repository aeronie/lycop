import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"

ColumnLayout
{
	id: root
	objectName: table.currentRow < 0 ? "" : "scores/" + table.model[table.currentRow]
	signal triggered_back()
	signal triggered_edit(string name)
	signal triggered_remove(string name)
	Keys.onEscapePressed: triggered_back()
	function get(row, column, value) {return settings.get("scores/" + row + "/" + column, value)}
	TableView
	{
		id: table
		Layout.fillWidth: true
		Layout.fillHeight: true
		TableViewColumn {title: qsTr("Name"); readonly property var value: function(x) {return settings.name_of("scores/" + x.value)}} // "value" défini comme "property var" pour que la colonne soit actualisée après l'écran "annoter"
		TableViewColumn {title: qsTr("Wave"); function value(x) {var a = get(x.value, "wave", 0); return a < 0 ? qsTr("end") : qsTr("wave %1").arg(a)}}
		TableViewColumn {title: qsTr("Score"); function value(x) {return qsTr("score %1").arg(get(x.value, "score", 0))}}
		model: settings.self.children_of("/scores").sort(function(a, b) {return get(b, "score", 0) - get(a, "score", 0)})
		onPressAndHold: buttons.show_menu()
	}
	Buttons
	{
		id: buttons
		model: [action_edit, action_remove, action_back]
	}
	Action
	{
		id: action_edit
		text: qsTr("Edit")
		enabled: table.currentRow >= 0
		onTriggered: triggered_edit(root.objectName)
	}
	Action
	{
		id: action_remove
		text: qsTr("Remove")
		enabled: table.currentRow >= 0
		onTriggered: triggered_remove(root.objectName)
	}
	Action
	{
		id: action_back
		text: qsTr("Back")
		onTriggered: triggered_back()
	}
}
