import QtQuick 2.0
import QtQuick.Layouts 1.1
import "qrc:///controls"

RowLayout
{
	property int health
	property int shield
	property int energy
	property int energy_: energy // pour convertir en int avant d'animer (sinon on affiche 1 ou 2 au lieu de 0)
	property int matter
	property int science
	property bool show_only_health: false
	property bool show_nothing: false
	Behavior on health {NumberAnimation {}}
	Behavior on shield {NumberAnimation {}}
	Behavior on energy_ {NumberAnimation {}}
	Behavior on matter {NumberAnimation {}}

	Image
	{
		visible: !show_nothing
		source: "health.png"
	}
	Label
	{
		visible: !show_nothing
		text: qsTr("%1 HP").arg(health)
		color: "#F00"
	}
	Item
	{
		width: spacing
	}
	Image
	{
		visible: !show_only_health && !show_nothing
		source: "shield.png"
	}
	Label
	{
		visible: !show_only_health && !show_nothing
		text: qsTr("%1 HP").arg(shield)
		color: "#06F"
	}
	Item
	{
		width: spacing
	}
	Image
	{
		visible: !show_only_health && !show_nothing
		source: "energy.png"
	}
	Label
	{
		visible: !show_only_health && !show_nothing
		text: "%1 J".arg(energy)
		color: "#0F0"
	}
	Item
	{
		width: spacing
	}
	Image
	{
		visible: !show_only_health && !show_nothing
		source: "matter.png"
	}
	Label
	{
		visible: !show_only_health && !show_nothing
		text: "%1 kg".arg(matter)
		color: "#F60"
	}
	Item
	{
		width: spacing
	}
	Image
	{
		visible: science && !show_only_health && !show_nothing
		source: "science.png"
	}
	Label
	{
		visible: science && !show_only_health && !show_nothing
		text: "%1".arg(science)
		color: "#FF6"
	}
}
