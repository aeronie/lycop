import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"

ColumnLayout
{
	id: root
	signal triggered_back()
	Keys.onEscapePressed: triggered_back()
	Label {text: qsTr("Annotate game “%1”:").arg(new_name.placeholderText); Layout.fillWidth: true; wrapMode: Text.Wrap}
	TextField
	{
		id: new_name
		placeholderText: settings.name_of(root.objectName)
		Layout.fillWidth: true
	}
	Buttons {model: [action_apply, action_back]}
	Item {Layout.fillHeight: true}
	Action
	{
		id: action_back
		text: qsTr("Cancel")
		onTriggered: triggered_back()
	}
	Action
	{
		id: action_apply
		text: qsTr("Apply")
		onTriggered:
		{
			settings.set(root.objectName + "/name", settings.encode(new_name.text))
			triggered_back()
		}
	}
}
