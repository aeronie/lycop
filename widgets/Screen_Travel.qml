import QtQuick 2.0
import QtQuick.Controls 1.2
import "qrc:///controls"

Image
{
	id: root
	property alias model: repeater.model
	signal triggered_travel(int index)
	fillMode: Image.PreserveAspectFit
	Repeater
	{
		id: repeater
		property int current_index: -1
		readonly property real rx: parent.sourceSize.width ? parent.paintedWidth / parent.sourceSize.width : 0
		readonly property real ry: parent.sourceSize.height ? parent.paintedHeight / parent.sourceSize.height : 0
		MouseArea
		{
			visible: saved_game.self.get("travel/" + modelData[5], false)
			x: modelData[0] * repeater.rx + (parent.width - parent.paintedWidth - width) / 2
			y: modelData[1] * repeater.ry + (parent.height - parent.paintedHeight - height) / 2
			width: Math.max(modelData[2] * repeater.rx + 20, 40)
			height: Math.max(modelData[3] * repeater.ry + 20, 40)
			acceptedButtons: Qt.LeftButton | Qt.RightButton
			onPressed: repeater.current_index = index
			onPressAndHold: buttons.show_menu()
			onClicked: if(mouse.button == Qt.RightButton) pressAndHold(mouse)
			onDoubleClicked: action_travel.trigger()
			BorderImage
			{
				id: border
				anchors.fill: parent
				source: index == repeater.current_index ? "area-selected.png" : "area.png"
				border.left: 20
				border.right: 20
				border.top: 20
				border.bottom: 20
			}
			Label
			{
				visible: index == repeater.current_index
				anchors.top: parent.bottom
				text: modelData[4]
				color: "red"
			}
			SequentialAnimation
			{
				running: border.visible
				loops: Animation.Infinite
				NumberAnimation {target: border; property: "anchors.margins"; from: 0; to: 1; duration: 1000; easing.type: Easing.InOutSine}
				NumberAnimation {target: border; property: "anchors.margins"; from: 1; to: 0; duration: 1000; easing.type: Easing.InOutSine}
			}
		}
	}
	Buttons
	{
		id: buttons
		anchors.bottom: parent.bottom
		anchors.right: parent.right
		model: [action_travel]
	}
	Action
	{
		id: action_travel
		text: qsTr("Travel")
		enabled: repeater.current_index >= 0
		onTriggered: root.triggered_travel(repeater.current_index)
	}
}
