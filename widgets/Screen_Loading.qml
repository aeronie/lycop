import QtQuick 2.0
import QtQuick.Controls 1.0
import aw 0.0
import "qrc:///controls"

Rectangle
{
	id: root
	color: "black"
	Loader
	{
		id: loader
		anchors.top: root.top
		anchors.bottom: progress.top
		anchors.left: root.left
		anchors.right: root.right
		sourceComponent: scene_loader.item ? scene_loader.item.splash : null
		onLoaded: if(item) item.forceActiveFocus()
	}
	ProgressBar
	{
		id: progress
		anchors.bottom: root.bottom
		anchors.left: root.left
		anchors.right: root.right
		maximumValue: 1
		readonly property int count: jobs.count
		onCountChanged:
		{
			if(maximumValue < count) maximumValue = count
			value = maximumValue - count
		}
	}
	QueuedBinding
	{
		direct: root.Stack.status == root.Stack.Active && !window.loading && (!loader.item || !loader.item.enabled)
		onQueuedChanged: if(queued) root.Stack.view.pop()
	}
}
