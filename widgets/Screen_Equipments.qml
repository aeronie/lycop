import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import aw 0.0
import "qrc:///controls"
import "qrc:///controls/styles" as S

GridLayout
{
	property QtObject ship
	property var trash: []
	readonly property Action action_install_: view_technologies.selection.count ? action_install_new : action_install_from_trash
	columns: 2
	onVisibleChanged: if(!visible)
	{
		if(action_sell.enabled) action_sell.trigger()
		if(ship) for(var i in ship.slots) if(!ship.slots[i].equipment) ship.slots[i].data = null
	}
	readonly property int max_health:
	{
		view_ship.update() // actualise la vue quand un équipement change
		var x = 0
		if(ship) for(var i in ship.slots)
		{
			var a = ship.slots[i].equipment
			if(a) x += a.max_health
		}
		return x + 1
	}
	ColumnLayout
	{
		Layout.preferredWidth: 1
		Layout.preferredHeight: 3
		TableView
		{
			id: view_technologies
			Layout.fillWidth: true
			Layout.fillHeight: true
			Layout.preferredHeight: 2
			headerDelegate: null
			TableViewColumn {function value(x) {return x.value.icon()} delegate: S.TableView_itemDelegate_icon {}}
			TableViewColumn {function value(x) {return x.value.title}}
			onPressAndHold: buttons_1.show_menu()
			onDoubleClicked: action_install.trigger()
			model:
			{
				var selected = []
				var all = technologies.data
				for(var i = 0; i < all.length; ++i) if(all[i].level && all[i].factory) selected.push(all[i])
				return selected
			}
		}
		TableView
		{
			id: view_trash
			Layout.fillWidth: true
			Layout.fillHeight: true
			Layout.preferredHeight: 1
			headerDelegate: null
			TableViewColumn {function value(x) {return x.value[0].icon()} delegate: S.TableView_itemDelegate_icon {}}
			TableViewColumn {function value(x) {return x.value[0].title}}
			TableViewColumn {function value(x) {return "(%1/%2)".arg(x.value[1].health).arg(x.value[1].max_health)}}
			onPressAndHold: buttons_1.show_menu()
			onDoubleClicked: action_install.trigger()
			model: trash
			visible: action_sell.enabled
		}
		Button
		{
			Layout.fillWidth: true
			action: action_sell
			visible: action_sell.enabled
		}
		Connections
		{
			target: view_technologies.selection
			onCountChanged: if(target.count) view_trash.selection.clear()
		}
		Connections
		{
			target: view_trash.selection
			onCountChanged: if(target.count) view_technologies.selection.clear()
		}
	}
	RowLayout
	{
		Layout.preferredWidth: 1
		Layout.preferredHeight: 3
		ColumnLayout
		{
			GridLayout
			{
				columns: 2
				Image
				{
					source: "matter.png"
				}
				Label
				{
					Layout.maximumWidth: 0
					Layout.alignment: Qt.AlignLeft
					text: "%1 kg".arg(Math.floor(saved_game.matter))
					color: "#F60"
				}
				Image
				{
					source: "health.png"
				}
				Label
				{
					Layout.maximumWidth: 0
					Layout.alignment: Qt.AlignLeft
					text: qsTr("%1 HP").arg(max_health)
					color: "#F00"
				}
			}
			Item
			{
				Layout.fillHeight: true
			}
			Buttons
			{
				id: buttons
				model: [action_install, action_remove, action_rotate_left, action_rotate_right, action_repair]
				flow: GridLayout.TopToBottom
			}
		}
		ImageView
		{
			id: view_ship
			Layout.fillWidth: true
			Layout.fillHeight: true
			model: ship ? ship.images : undefined
			center: ship ? ship.position : Qt.point(0, 0)
			angle: ship ? ship.angle : 0
			zoom: ship ? Math.min(width, height) / 2 / ship.scale : Math.min(width, height) / 2
			visible: ship
			Repeater
			{
				id: view_slots
				property int currentIndex: 0
				model: ship ? ship.slots : []
				MouseArea
				{
					acceptedButtons: Qt.LeftButton | Qt.RightButton
					x: ship ? view_ship.zoom * ship.scale * modelData.position.x + (parent.width - width) / 2 : 0
					y: ship ? view_ship.zoom * ship.scale * modelData.position.y + (parent.height - height) / 2 : 0
					width: modelData.scale * view_ship.zoom * ship.scale
					height: width
					onPressed: view_slots.currentIndex = index
					onPressAndHold: buttons.show_menu()
					onClicked: if(mouse.button == Qt.RightButton) pressAndHold(mouse)
					Rectangle
					{
						anchors.fill: parent
						color: "transparent"
						border.color: index == view_slots.currentIndex ? "red" : "white"
						border.width: 2
						radius: width
						visible: !modelData.equipment || index == view_slots.currentIndex
						Label
						{
							anchors.centerIn: parent
							text: index + 1
							color: parent.border.color
							style: modelData.equipment ? Text.Outline : Text.Normal
							styleColor: "white"
						}
					}
					ToolTip
					{
						text: controls.slots[index].text + (modelData.data ? "<br>" + modelData.data.title : "")
					}
				}
			}
		}
	}
	TextArea
	{
		Layout.fillWidth: true
		Layout.fillHeight: true
		Layout.preferredHeight: 2
		text:
		{
			if(view_technologies.selection.count) return view_technologies.model[view_technologies.currentRow].description
			if(view_trash.selection.count) return view_trash.model[view_trash.currentRow][0].description
			return qsTr("no-equipment")
		}
	}
	ColumnLayout
	{
		Layout.preferredHeight: 2
		Button
		{
			Layout.fillWidth: true
			action: action_repair_all
			visible: action_repair_all.enabled
		}
		TextArea
		{
			Layout.fillWidth: true
			Layout.fillHeight: true
			text:
			{
				var a = view_slots.model[view_slots.currentIndex]
				return a && a.data ? a.data.description : qsTr("no-slot")
			}
		}
	}
	Buttons
	{
		id: buttons_1
		model: [action_install]
		visible: false
	}
	Action
	{
		id: action_install
		text: action_install_.text
		iconSource: "install.png"
		enabled: action_install_.enabled
		onTriggered: action_install_.trigger()
	}

	readonly property real install_cost:
	{
		var a = view_technologies.model[view_technologies.currentRow]
		return a && ship ? a.cost : 0
	}
	Action
	{
		id: action_install_new
		text: qsTr("Equip (%1 kg)").arg(install_cost)
		enabled: install_cost > 0 && install_cost <= saved_game.matter
		onTriggered:
		{
			var a = view_technologies.model[view_technologies.currentRow]
			if(action_remove.enabled) action_remove.trigger()
			saved_game.add_matter(-a.cost)
			ship.set_equipment(view_slots.currentIndex, a)
		}
	}

	Action
	{
		id: action_install_from_trash
		text: qsTr("Equip")
		enabled: !!view_trash.model[view_trash.currentRow]
		onTriggered:
		{
			var a = trash.splice(view_trash.currentRow, 1)[0]
			var b = view_slots.model[view_slots.currentIndex]
			if(action_remove.enabled) action_remove.trigger()
			b.data = a[0]
			b.equipment = a[1]
			ship.connect_shooting(view_slots.currentIndex)
			trashChanged()
		}
	}
	Action
	{
		id: action_remove
		text: qsTr("Remove")
		iconSource: "remove.png"
		enabled:
		{
			var a = view_slots.model[view_slots.currentIndex]
			return !!a && !!a.equipment
		}
		onTriggered:
		{
			var a = view_slots.model[view_slots.currentIndex]
			trash.push([a.data, a.equipment])
			if(a.equipment.set_shooting) controls.slots[view_slots.currentIndex].value_changed.disconnect(a.equipment.set_shooting)
			a.equipment = null
			a.data = null
			trashChanged()
		}
	}

	readonly property real trash_value:
	{
		var x = 0;
		for(var i = 0; i < trash.length; ++i) x += trash[i][0].cost * trash[i][1].health / trash[i][1].max_health
		return x * saved_game.get("tax", 0.5)
	}
	Action
	{
		id: action_sell
		text: qsTr("Recycle (%1 kg)").arg(trash_value)
		enabled: trash.length > 0
		onTriggered:
		{
			saved_game.add_matter(trash_value)
			for(var i = 0; i < trash.length; ++i) trash[i][1].deleteLater()
			trash = []
		}
	}

	function rotate(i, x)
	{
		var a = view_slots.model[i]
		a.angle = ((Math.round(a.angle * 6 / Math.PI) + x + 12) % 12) * Math.PI / 6
		saved_game.set("slots/" + i + "/angle", a.angle)
	}
	Action
	{
		id: action_rotate_left
		text: qsTr("Rotate left")
		iconSource: "rotate-left.png"
		enabled: !!view_slots.model[view_slots.currentIndex]
		onTriggered: rotate(view_slots.currentIndex, -1)
	}
	Action
	{
		id: action_rotate_right
		text: qsTr("Rotate right")
		iconSource: "rotate-right.png"
		enabled: !!view_slots.model[view_slots.currentIndex]
		onTriggered: rotate(view_slots.currentIndex, 1)
	}

	readonly property real repair_cost: ship ? ship.repair_cost(view_slots.currentIndex) : 0
	readonly property real repair_all_cost: ship ? ship.repair_all_cost() : 0
	Action
	{
		id: action_repair
		text: qsTr("Repair (%1 kg)").arg(repair_cost)
		iconSource: "repair.png"
		enabled: ship && ship.can_repair(repair_cost)
		onTriggered: saved_game.add_matter(-repair_cost), ship.repair(view_slots.currentIndex)
	}
	Action
	{
		id: action_repair_all
		text: qsTr("Repair all (%1 kg)").arg(repair_all_cost)
		enabled: ship && ship.can_repair(repair_all_cost)
		onTriggered: saved_game.add_matter(-repair_all_cost), ship.repair_all()
	}
}
