import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"
import "qrc:///controls/styles" as S

HybridTabView
{
	id: root
	property bool last_level: false
	property bool travel_map_2: false
	onLast_levelChanged:
	{
		if(last_level)
		{
			show(screen_start)
		}
	}
	signal triggered_load(string group, var data)
	signal triggered_quit()
	signal triggered_close()
	property QtObject ship
	readonly property ScreenData about_screen_equipments: ScreenData {target: screen_equipments; text: qsTr("Equipments"); icon: "equipments"}
	readonly property ScreenData about_screen_technologies: ScreenData {target: screen_technologies; text: qsTr("Technologies"); icon: "technologies"}
	readonly property ScreenData about_screen_travel: ScreenData {target: screen_travel; text: qsTr("Travel"); icon: "travel"}
	readonly property ScreenData about_screen_start: ScreenData {target: screen_start; text: qsTr("Menu"); icon: "options"}
	Keys.onEscapePressed: root.triggered_close()
	onTriggered_load: stack.pop(null)

	stack.initialItem: screen_start
	Button
	{
		text: qsTr("Back")
		onClicked: root.triggered_close()
		style: tab_style
	}
	Repeater
	{
		model: [about_screen_equipments, about_screen_technologies, about_screen_travel, about_screen_start]
		Button
		{
			enabled: !root.last_level || modelData.target == screen_start
			text: modelData.text
			opacity : (!root.last_level || modelData.target == screen_start) ? 1 : 0.5
			checked: stack.initialItem == modelData.target
			onClicked: show(modelData.target)
			style: tab_style
		}
	}
	readonly property Component screen_equipments: Screen_Equipments
	{
		z: 4
		ship: root.ship
	}
	readonly property Component screen_technologies: Screen_Technologies
	{
		z: 3
	}
	readonly property Component screen_travel: Screen_Travel
	{
		z: 2
		onTriggered_travel:
		{
			saved_game.set("file", "game/" + model[index][5] + ".qml")
			saved_game.save()
			triggered_load(settings.get_last_saved_game(), {})
		}
		source: "///data/game/" + (root.travel_map_2 ? "travel2" : "travel") + ".png"
		model:
		[
			[900, 370, 35, 35, qsTr("Mercury"), "chapter-2/chapter-2"],
			[900, 370, 35, 35, qsTr("Mercury"), "chapter-2/chapter-2c"],
			[565, 250, 275, 275, qsTr("Sun"), "chapter-3/chapter-3"],
			[635, 715, 100, 100, qsTr("Earth"), "chapter-4/chapter-4"],
			[635, 715, 100, 100, qsTr("Earth"), "chapter-4/chapter-4b"],
			[1600, 360, 375, 375, qsTr("Planète Capitis"), "chapter-5/chapter-5"],
			[1620, 890, 300, 187, qsTr("Vaisseau-monde Heter"), "chapter-6/chapter-6"],
		]
	}
	readonly property Component screen_start: ColumnLayout
	{
		z: 1
		Buttons
		{
			Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
			flow: GridLayout.TopToBottom
			style: S.MenuButton {}
			model: [
				Action {text: qsTr("Back"); onTriggered: root.triggered_close(); enabled: scene_loader.item && !scene_loader.item.main_menu},
				Action {text: qsTr("Auto load"); onTriggered: triggered_load(settings.get_last_saved_game(), {}); enabled: !!settings.self.get_last_saved_game()},
				Action {text: qsTr("New game"); onTriggered: stack.push(screen_new_game)},
				Action {text: qsTr("Load"); onTriggered: stack.push(screen_load_game); enabled: !!settings.self.get_last_saved_game()},
				Action {text: qsTr("Scores"); onTriggered: stack.push(screen_scores); enabled: !!settings.self.children_of("/scores").length && scene_loader.item ? scene_loader.item.main_menu : false},
				Action {text: qsTr("Credits"); onTriggered: root.triggered_close(); enabled: scene_loader.item && scene_loader.item.main_menu},
				Action {text: qsTr("Options"); onTriggered: stack.push(screen_options)},
				Action {text: qsTr("Title screen"); onTriggered: window.back_to_main_menu(); enabled: scene_loader.item ? !scene_loader.item.main_menu : false},
				Action {text: qsTr("Quit"); onTriggered: root.triggered_quit()}]
		}
		Label
		{
			anchors.right: parent.right
			anchors.bottom: parent.bottom
			text: global.version() < 0 ? qsTr("demo version %1").arg(-global.version()) : qsTr("version %1").arg(global.version())
		}
	}
	readonly property Component screen_new_game: ColumnLayout
	{
		Keys.onEscapePressed: stack.pop()
		Buttons
		{
			Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
			flow: GridLayout.TopToBottom
			style: S.MenuButton {}
			model: [
				Action {text: qsTr("Easy"); onTriggered: stack.push(screen_story_mod)},
				Action {text: qsTr("Survival mode"); onTriggered: triggered_load(undefined, {"file": "game/survival/survival.qml"})},
				Action {text: qsTr("Training mode"); onTriggered: triggered_load(undefined, {"file": "game/chapter-0/chapter-0-1.qml"})},
				Action {text: qsTr("Back"); onTriggered: stack.pop()}]
		}
	}
	
	readonly property Component screen_story_mod: ColumnLayout
	{
		Keys.onEscapePressed: stack.pop()
		Buttons
		{
			Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
			flow: GridLayout.TopToBottom
			style: S.MenuButton {}
			model: [
				Action {text: qsTr("Faible"); onTriggered: triggered_load(undefined, {"file": "game/chapter-1/chapter-1.qml", "slot-count": 7, "equipments/gun/level": 1, "equipments/0": "gun", "mod_easy" : true })},
				Action {text: qsTr("Vaillant"); onTriggered: triggered_load(undefined, {"file": "game/chapter-1/chapter-1.qml", "slot-count": 7, "equipments/gun/level": 1, "equipments/0": "gun"})}
			]
		}
	}
	readonly property Component screen_load_game: Screen_LoadGame
	{
		onTriggered_back: show(screen_start)
		onTriggered_load: root.triggered_load(target, {})
		onTriggered_edit: stack.push({item: screen_edit_game, properties: {objectName: name}})
		onTriggered_remove_1: stack.push({item: screen_remove_game_1, properties: {objectName: name}})
		onTriggered_remove_2: stack.push({item: screen_remove_game_2, properties: {objectName: name}})
	}
	readonly property Component screen_remove_game_1: Screen_RemoveGame1
	{
		onTriggered_back: stack.pop()
	}
	readonly property Component screen_remove_game_2: Screen_RemoveGame2
	{
		onTriggered_back: stack.pop()
	}
	readonly property Component screen_edit_game: Screen_EditGame
	{
		onTriggered_back: stack.pop()
	}
	readonly property Component screen_scores: Screen_Scores
	{
		onTriggered_back: show(screen_start)
		onTriggered_edit: stack.push({item: screen_edit_score, properties: {objectName: name}})
		onTriggered_remove: stack.push({item: screen_remove_score, properties: {objectName: name}})
	}
	readonly property Component screen_edit_score: Screen_EditScore
	{
		onTriggered_back: stack.pop()
	}
	readonly property Component screen_remove_score: Screen_RemoveScore
	{
		onTriggered_back: stack.pop()
	}
	readonly property Component screen_options: Screen_Options
	{
		onTriggered_back: stack.pop()
		onTriggered_edit_controls: stack.push(screen_controls)
	}
	readonly property Component screen_controls: Screen_Controls
	{
		onTriggered_back: stack.pop()
		onTriggered_edit: stack.push({item: screen_edit_control, properties: {form: this}})
		onTriggered_reset: stack.push(screen_reset_controls)
	}
	readonly property Component screen_edit_control: Screen_EditControl
	{
		onTriggered_back: stack.pop()
	}
	readonly property Component screen_confirm_control: Screen_ConfirmControl
	{
		onTriggered_back: stack.pop()
	}
	readonly property Component screen_reset_controls: Screen_ResetControls
	{
		onTriggered_back: stack.pop()
	}
	readonly property Component tab_style: S.Button
	{
		background: Rectangle
		{
			visible: control.checked
			color: "white"
		}
		label: Label
		{
			text: control.text
			color: control.activeFocus ? "red" : control.checked ? "black" : "white"
		}
	}
}
