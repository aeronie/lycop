import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"

ColumnLayout
{
	id: root
	signal triggered_back()
	signal triggered_edit_controls()
	Keys.onEscapePressed: triggered_back()
	ScrollView
	{
		id: form
		horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
		Layout.fillHeight: true
		Layout.fillWidth: true
		GridLayout
		{
			width: form.viewport.width - (form.__scroller.verticalScrollBar.visible ? 5 : 0)
			columns: 2
			Repeater
			{
				model:
				[
					{type: "section", text: qsTr("Display preferences")},
					{type: "label", text: qsTr("Full screen")},{type: "bool", key: "full_screen"},
					{type: "label", text: qsTr("Lighting quality")},{type: "enum", key: "shader_quality", model: [qsTr("Flat"), qsTr("Diffuse"), qsTr("Diffuse and specular")]},
					{type: "section", text: qsTr("Sound preferences")},
					{type: "label", text: qsTr("Volume: sound effects")},{type: "real", key: "audio_gain"},
					{type: "label", text: qsTr("Volume: music")},{type: "real", key: "music_gain"},
					{type: "label", text: qsTr("Volume: cutscenes")},{type: "real", key: "video_gain"},
					{type: "section", text: qsTr("Input preferences")},
					{type: "label", text: qsTr("Absolute velocity")},{type: "bool", key: "velocity_absolute"},
					{type: "label", text: qsTr("Bindings")},{type: "button", function: "triggered_edit_controls", text: qsTr("Configure...")},
				]
				Repeater
				{
					model: [modelData]
					delegate: form["delegate_" + modelData.type]
				}
			}
		}
		readonly property Component delegate_section: RowLayout
		{
			Layout.columnSpan: 2
			Layout.preferredHeight: implicitHeight * 2
			Rectangle
			{
				Layout.fillWidth: true
				height: 2
			}
			Label
			{
				text: modelData.text
			}
			Rectangle
			{
				Layout.fillWidth: true
				height: 2
			}
		}
		readonly property Component delegate_label: Label
		{
			text: modelData.text
		}
		readonly property Component delegate_bool: Switch
		{
			Layout.alignment: Qt.AlignRight
			checked: settings[modelData.key]
			onCheckedChanged: settings.set(modelData.key, checked)
		}
		readonly property Component delegate_real: RowLayout
		{
			Layout.alignment: Qt.AlignRight
			Layout.maximumWidth: 500
			Slider
			{
				id: slider
				Layout.fillWidth: true
				stepSize: 0.01
				value: settings[modelData.key]
				onValueChanged:
				{
					spinbox.value = value
					settings.set(modelData.key, value)
				}
			}
			SpinBox
			{
				id: spinbox
				stepSize: slider.stepSize
				maximumValue: slider.maximumValue
				decimals: 2
				onValueChanged: slider.value = value
			}
		}
		readonly property Component delegate_enum: RowLayout
		{
			Label
			{
				Layout.preferredWidth: 1
				Layout.fillWidth: true
				text: modelData.model[slider.value]
				horizontalAlignment: Text.AlignRight
				elide: Text.ElideRight
			}
			Slider
			{
				id: slider
				Layout.maximumWidth: 200
				Layout.preferredWidth: 1
				Layout.fillWidth: true
				maximumValue: modelData.model.length - 1
				stepSize: 1
				value: settings[modelData.key]
				onValueChanged: settings.set(modelData.key, value)
			}
		}
		readonly property Component delegate_button: Button
		{
			Layout.alignment: Qt.AlignRight
			text: modelData.text
			onClicked: root[modelData.function]()
		}
	}
	Buttons
	{
		model: [action_back]
	}
	Action
	{
		id: action_back
		text: qsTr("Back")
		onTriggered: triggered_back()
	}
}
