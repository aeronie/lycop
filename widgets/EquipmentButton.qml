import QtQuick 2.0
import aw 0.0
import "qrc:///controls"

Button
{
	id: root
	property real health: 0
	property real shield: 0
	property real cooldown: 0
	iconSource: "check.png"
	activeFocusOnPress: false // pour pouvoir utiliser la touche [espace] en jeu
	Image
	{
		anchors.fill: parent
		anchors.margins: 2
		anchors.topMargin: root.checked ? 2 : root.pressed ? 2 : 0
		anchors.bottomMargin: root.checked ? 2 : root.pressed ? 2 : 4
		source: root.iconName ? "image://EquipmentIcon/%1,%2,%3".arg(root.iconName).arg(!root.cooldown).arg(root.checked) : ""
		Rectangle
		{
			anchors.bottom: parent.bottom
			anchors.left: parent.left
			anchors.right: parent.right
			color: Qt.rgba(0, 0, 0, 0.5)
			visible: height
			height: parent.height * root.cooldown
		}
		Column
		{
			anchors.bottom: parent.bottom
			anchors.left: parent.left
			anchors.right: parent.right
			Image
			{
				source: "check.png"
				visible: root.isDefault
				anchors.right: parent.right
			}
			Rectangle
			{
				color: "#06F"
				visible: width
				width: parent.width * root.shield
				height: 2
			}
			Rectangle
			{
				color: "#F00"
				visible: width
				width: parent.width * root.health
				height: 2
			}
		}
		Label
		{
			anchors.bottom: parent.bottom
			anchors.right: parent.right
			text: root.text
		}
	}
	MouseArea
	{
		anchors.fill: parent
		acceptedButtons: Qt.RightButton
		onClicked: isDefault = !isDefault
	}
}
