import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"

ColumnLayout
{
	id: root
	signal triggered_back()
	Keys.onEscapePressed: triggered_back()
	Label
	{
		text:
		{
			var n = root.objectName.lastIndexOf("/")
			return qsTr("Do you want to remove data saved on %2 about game “%1”?").arg(settings.name_of(root.objectName.substring(0, n))).arg(settings.date_of(root.objectName.substring(n + 1)).toLocaleString())
		}
		Layout.fillWidth: true
		wrapMode: Text.Wrap
	}
	Buttons {model: [action_remove, action_back]}
	Item {Layout.fillHeight: true}
	Action
	{
		id: action_back
		text: qsTr("Cancel")
		onTriggered: triggered_back()
	}
	Action
	{
		id: action_remove
		text: qsTr("Remove")
		onTriggered:
		{
			settings.remove(root.objectName)
			triggered_back()
		}
	}
}
