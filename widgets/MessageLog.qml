import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import aw 0.0
import "qrc:///controls"
import "qrc:///controls/styles" as S

FocusScope
{
	signal triggered_open()
	signal triggered_close()
	readonly property bool has_unread: that.first_unread < that.messages.length
	onHas_unreadChanged: if(has_unread) triggered_open()
	Keys.onSpacePressed: action_go_last.trigger()
	Keys.onUpPressed: if(!event.isAutoRepeat) that.go_previous_next(0)
	Keys.onDownPressed: if(!event.isAutoRepeat) that.go_previous_next(1)
	function add(author, message)
	{
		that.messages.push([author, message])
		that.messagesChanged()
		that.jump(that.last)
	}
	function clear()
	{
		that.messages = []
		that.first_unread = 0
	}
	QtObject
	{
		id: that
		property var messages: []
		property int first_unread: 0
		property int last: Math.min(first_unread, repeater.count - 1)
		property Item first_unread_label: first_unread_item && first_unread_item.children.length && first_unread_item.children[1].children[1].children[0] || null
		property Item first_unread_item:
		{
			var i = 0
			for(var n = that.first_unread + 1; n; ++i) if(layout.children[i] != repeater) --n
			return layout.children[i - 1] || null
		}
		function jump(index)
		{
			flickable.contentY = Qt.binding(function(){return index < layout.children.length ? layout.children[index].y : 0})
		}
		function go_previous_next(previous_or_next)
		{
			var begin = 0
			var end = last
			while(begin <= end)
			{
				var i = Math.floor((begin + end) / 2)
				var y = layout.children[i].y
				if(y <= flickable.contentY) begin = i + 1
				if(y >= flickable.contentY) end = i - 1
			}
			jump(Math.max(0, Math.min(last, [end, begin][previous_or_next])))
		}
		function step()
		{
			if(has_unread && flickable.contentY == layout.children[that.first_unread].y) ++that.first_unread
			if(!has_unread) triggered_close()
		}
	}
	Flickable
	{
		id: flickable
		anchors.fill: parent
		contentWidth: width
		contentHeight: height + (that.last < layout.children.length ? layout.height - layout.children[that.last].height : 0)
		MouseArea
		{
			anchors.fill: parent
			cursor: "arrow-e"
			onClicked: action_go_last.trigger()
		}
		ColumnLayout
		{
			id: layout
			anchors.left: parent.left
			anchors.right: parent.right
			anchors.top: parent.top
			Repeater
			{
				id: repeater
				model: that.messages.length ? that.messages : [["", qsTr("No new messages.")]]
				delegate: Item
				{
					visible: index <= that.first_unread
					Layout.fillWidth: true
					implicitHeight: layout.implicitHeight + layout.spacing * 2
					BorderImage
					{
						anchors.fill: parent
						source: "MessageLog.png"
						verticalTileMode: BorderImage.Repeat
					}
					RowLayout
					{
						id: layout
						anchors.left: parent.left
						anchors.right: parent.right
						anchors.top: parent.top
						anchors.margins: spacing
						Item
						{
							width: 100
							height: 100
							Layout.alignment: Qt.AlignTop
							Image
							{
								visible: modelData[0]
								source: visible ? "qrc:///data/game/folks/%1.png".arg(modelData[0]) : ""
								anchors.fill: parent
								smooth: false
							}
						}
						ColumnLayout
						{
							Label
							{
								text: index < that.first_unread ? modelData[1] : ""
								font.weight: Font.Normal
								font.capitalization: Font.MixedCase
								font.pointSize: S.Private.font.pointSize * 1.3
								color: "black"
								wrapMode: Text.WordWrap
								Layout.fillWidth: true
								Layout.fillHeight: true
							}
							RowLayout
							{
								visible: !timer.running
								Layout.alignment: Qt.AlignRight
								Image
								{
									source: "go-previous.png"
									MouseArea
									{
										anchors.fill: parent
										cursor: "hand-pointing"
										onClicked: that.go_previous_next(0)
									}
								}
								Image
								{
									source: "go-next.png"
									MouseArea
									{
										anchors.fill: parent
										cursor: "hand-pointing"
										onClicked:
										{
											that.step()
											that.go_previous_next(1)
										}
									}
								}
								Image
								{
									source: "go-last.png"
									MouseArea
									{
										anchors.fill: parent
										cursor: "hand-pointing"
										onClicked: action_go_last.trigger()
									}
								}
							}
						}
					}
				}
			}
		}
	}
	Timer
	{
		id: timer
		interval: 20
		repeat: true
		running: has_unread && that.first_unread_label && that.first_unread_label.text != that.messages[that.first_unread][1]
		onTriggered: that.first_unread_label.text += that.messages[that.first_unread][1][that.first_unread_label.text.length]
	}
	Action
	{
		id: action_go_last
		onTriggered:
		{
			if(timer.running)
			{
				that.first_unread_label.text = that.messages[that.first_unread][1]
			}
			else
			{
				that.step()
				that.jump(that.last)
			}
		}
	}
}
