#include <QtQml>

#include "Platform.h"
#include "meta.private.h"

namespace aw {
namespace {

struct AbstractPlatform
: QObject
{
	Q_OBJECT
public Q_SLOTS:
	virtual int32_t get_int(QString const &) const = 0;
	virtual float get_float(QString const &) const = 0;
	virtual void set_bool(QString const &) const = 0;
	virtual void set_int(QString const &, int) const = 0;
	virtual void set_float(QString const &, float) const = 0;
	virtual void update_average(QString const &, float, float) const = 0;
public:
	virtual QLocale get_locale() const = 0;
	QTranslator translator_;
protected:
	std::map<QString, bool> bool_ = {
		{"end", false}, // jeu terminé (progresse à chaque niveau)
		{"good-end", false}, // bonne fin (caché)
		{"bad-end", false}, // mauvaise fin (caché)
		{"survival-end", false}, // survival terminé
		{"secret-base-heter", false}, // secret de la base heter (caché)
		{"secret-foreuse", false}, // secret de la foreuse (caché)
		{"secret-cathedrale", false}, // secret de la cathedrale (caché)
		{"secret-tomate", false}, // secret tomate (caché)
		{"secret-apis", false}, // épargner l'apis(caché)
		{"secret-commandement", false}, // secret du vaisseau de commandement heter (caché)
		{"chapter-3-end", false}, // chapitre 3 fini (caché)
		{"course-fini", false}, // terminer la course (caché)
		{"chapter-6-end", false}, // chapitre 6 fini (caché)
		{"true-last-boss-end", false}, // cerebus vaincu (caché)
		{"qui-poutre-unlock", false}, // debloquer le qui poutre (caché)
		{"kamikaze", false}, // suicide pendant les credits (caché)
		{"epic", false}, // jeu fini en mode vaillant (caché)
	};
	std::map<QString, int32_t> int_ = {
		{"level", 0}, // niveau atteint
		{"survival-score", 0}, // meilleur score au survival
		{"survival-level", 0}, // meilleure vague au survival
		{"mercury-score", 0}, // meilleur loot sur mercure
	};
	std::map<QString, float> float_ = {
		{"time", 0}, // temps de jeu
	};
};

}
}

#if defined CONFIG_STEAM

#include <steam/steam_api.h>

#define _FOR_EACH_EVENT(_) _(UserStatsReceived) _(UserStatsStored) _(UserAchievementStored)

#if defined Q_OS_UNIX

#include <dlfcn.h>

namespace aw {
namespace {
static void __attribute__((used)) dummy() // drmwrap a besoin que le symbole dlsym soit importé dans l'exécutable
{
	dlsym(0, "");
}
}
}

#endif

#if defined Q_OS_WIN

#define _DECLARE_CALLBACK(x) void x(x##_t *);
#define _INIT_CALLBACK(x) , [] (void *this_, void *o) { static_cast<Platform *>(this_)->x(static_cast<x##_t *>(o)); }
#define _INIT_CALLBACKS steam_init_callbacks(this _FOR_EACH_EVENT(_INIT_CALLBACK));

#define _(x) , void (*)(void *, void *)
extern "C" __declspec(dllimport) void steam_init_callbacks(void * _FOR_EACH_EVENT(_));
#undef _

#else

#define _DECLARE_CALLBACK(x) STEAM_CALLBACK_MANUAL(Platform, x, x##_t, x##_);
#define _INIT_CALLBACK(x) x##_.Register(this, &Platform::x);
#define _INIT_CALLBACKS _FOR_EACH_EVENT(_INIT_CALLBACK)

#endif

namespace aw {
namespace {

static char const *to_string(EResult x)
{
	switch(x)
	{
		#define _(_) case k_EResult##_: return #_;
		_(OK)
		_(Fail)
		_(NoConnection)
		_(InvalidPassword)
		_(LoggedInElsewhere)
		_(InvalidProtocolVer)
		_(InvalidParam)
		_(FileNotFound)
		_(Busy)
		_(InvalidState)
		_(InvalidName)
		_(InvalidEmail)
		_(DuplicateName)
		_(AccessDenied)
		_(Timeout)
		_(Banned)
		_(AccountNotFound)
		_(InvalidSteamID)
		_(ServiceUnavailable)
		_(NotLoggedOn)
		_(Pending)
		_(EncryptionFailure)
		_(InsufficientPrivilege)
		_(LimitExceeded)
		_(Revoked)
		_(Expired)
		_(AlreadyRedeemed)
		_(DuplicateRequest)
		_(AlreadyOwned)
		_(IPNotFound)
		_(PersistFailed)
		_(LockingFailed)
		_(LogonSessionReplaced)
		_(ConnectFailed)
		_(HandshakeFailed)
		_(IOFailure)
		_(RemoteDisconnect)
		_(ShoppingCartNotFound)
		_(Blocked)
		_(Ignored)
		_(NoMatch)
		_(AccountDisabled)
		_(ServiceReadOnly)
		_(AccountNotFeatured)
		_(AdministratorOK)
		_(ContentVersion)
		_(TryAnotherCM)
		_(PasswordRequiredToKickSession)
		_(AlreadyLoggedInElsewhere)
		_(Suspended)
		_(Cancelled)
		_(DataCorruption)
		_(DiskFull)
		_(RemoteCallFailed)
		_(PasswordUnset)
		_(ExternalAccountUnlinked)
		_(PSNTicketInvalid)
		_(ExternalAccountAlreadyLinked)
		_(RemoteFileConflict)
		_(IllegalPassword)
		_(SameAsPreviousValue)
		_(AccountLogonDenied)
		_(CannotUseOldPassword)
		_(InvalidLoginAuthCode)
		_(AccountLogonDeniedNoMail)
		_(HardwareNotCapableOfIPT)
		_(IPTInitError)
		_(ParentalControlRestricted)
		_(FacebookQueryError)
		_(ExpiredLoginAuthCode)
		_(IPLoginRestrictionFailed)
		_(AccountLockedDown)
		_(AccountLogonDeniedVerifiedEmailRequired)
		_(NoMatchingURL)
		_(BadResponse)
		_(RequirePasswordReEntry)
		_(ValueOutOfRange)
		_(UnexpectedError)
		_(Disabled)
		_(InvalidCEGSubmission)
		_(RestrictedDevice)
		_(RegionLocked)
		_(RateLimitExceeded)
		_(AccountLoginDeniedNeedTwoFactor)
		_(ItemDeleted)
		_(AccountLoginDeniedThrottle)
		_(TwoFactorCodeMismatch)
		_(TwoFactorActivationCodeMismatch)
		_(AccountAssociatedToMultiplePartners)
		_(NotModified)
		_(NoMobileDevice)
		_(TimeNotSynced)
		_(SmsCodeFailed)
		_(AccountLimitExceeded)
		_(AccountActivityLimitExceeded)
		_(PhoneActivityLimitExceeded)
		_(RefundToWallet)
		_(EmailSendFailure)
		_(NotSettled)
		_(NeedCaptcha)
		_(GSLTDenied)
		_(GSOwnerDenied)
		_(InvalidItemType)
		_(IPBanned)
		_(GSLTExpired)
		#undef _
	}
	return "?";
}

template<class T>
bool check(T const &x1, T const &x2, char const *function)
{
	if(x1 != x2)
	{
		qWarning() << function << "failed:" << to_string(x1);
		return false;
	}
	return true;
}

struct Platform
: AbstractPlatform
{
	AW_DECLARE_OBJECT_STUB(Platform)
	_FOR_EACH_EVENT(_DECLARE_CALLBACK)
	bool valid_ = false;
	uint32_t id_ = 0;

	void timerEvent(QTimerEvent *) override
	{
		SteamAPI_RunCallbacks();
	}

	int32_t get_int(QString const &key) const override
	{
		auto p = int_.find(key);
		Q_ASSERT(p != int_.end());
		return p->second;
	}

	float get_float(QString const &key) const override
	{
		auto p = float_.find(key);
		Q_ASSERT(p != float_.end());
		return p->second;
	}

	void set_bool(QString const &key) const override
	{
		if(!valid_) return;
		SteamUserStats()->SetAchievement(key.toUtf8());
		SteamUserStats()->StoreStats();
	}

	void set_int(QString const &key, int value) const override
	{
		if(!valid_) return;
		SteamUserStats()->SetStat(key.toUtf8(), value);
		SteamUserStats()->StoreStats();
	}

	void set_float(QString const &key, float value) const override
	{
		if(!valid_) return;
		SteamUserStats()->SetStat(key.toUtf8(), value);
		SteamUserStats()->StoreStats();
	}

	void update_average(QString const &key, float num, float den) const override
	{
		if(!valid_) return;
		SteamUserStats()->UpdateAvgRateStat(key.toUtf8(), num, den);
		SteamUserStats()->StoreStats();
	}

	/*

	Il faut faire correspondre la chaîne retournée par GetCurrentGameLanguage à une locale standard.
	Liste des chaînes : https://partner.steamgames.com/documentation/languages#supported

	*/
	QLocale get_locale() const override
	{
		char const *x = SteamApps()->GetCurrentGameLanguage();
		#define _(_, ...) if(!strcmp(x, #_)) return {QLocale::__VA_ARGS__};
		_(brazilian, Portuguese, QLocale::Brazil)
		_(bulgarian, Bulgarian)
		_(czech, Czech)
		_(danish, Danish)
		_(dutch, Dutch)
		_(english, English)
		_(finnish, Finnish)
		_(french, French)
		_(german, German)
		_(greek, Greek)
		_(hungarian, Hungarian)
		_(italian, Italian)
		_(japanese, Japanese)
		_(koreana, Korean) // ?
		_(norwegian, Norwegian)
		_(polish, Polish)
		_(portuguese, Portuguese)
		_(romanian, Romanian)
		_(russian, Russian)
		_(schinese, Chinese, QLocale::SimplifiedHanScript, QLocale::AnyCountry)
		_(spanish, Spanish)
		_(swedish, Swedish)
		_(tchinese, Chinese, QLocale::TraditionalHanScript, QLocale::AnyCountry)
		_(thai, Thai)
		_(turkish, Turkish)
		_(arabic, Arabic)
		_(ukrainian, Ukrainian)
		#undef _
		return {};
	}

public:

	Platform()
	{
		if(!SteamAPI_Init())
		{
			qWarning() << "failed to initialize Steam API";
			return;
		}
		_INIT_CALLBACKS
		id_ = SteamUtils()->GetAppID();
		SteamUserStats()->RequestCurrentStats();
		int timer = startTimer(50);
		Q_ASSERT(timer);
		(void) timer;
	}

	~Platform()
	{
		SteamAPI_Shutdown();
	}
};

void Platform::UserStatsReceived(UserStatsReceived_t *o)
{
	if(o->m_nGameID != id_) return;
	if(!check(o->m_eResult, k_EResultOK, __func__)) return;
	for(auto &p: bool_) SteamUserStats()->GetAchievement(p.first.toUtf8(), &p.second);
	for(auto &p: int_) SteamUserStats()->GetStat(p.first.toUtf8(), &p.second);
	valid_ = true;
	setObjectName("STEAM");
}

void Platform::UserStatsStored(UserStatsStored_t *o)
{
	if(o->m_nGameID != id_) return;
	if(!check(o->m_eResult, k_EResultOK, __func__)) return;
}

void Platform::UserAchievementStored(UserAchievementStored_t *o)
{
	if(o->m_nGameID != id_) return;
}

}
}

#else

namespace aw {
namespace {

struct Platform
: AbstractPlatform
{
	AW_DECLARE_OBJECT_STUB(Platform)
	int32_t get_int(QString const &) const override { return 0; }
	float get_float(QString const &) const override { return 0; }
	void set_bool(QString const &) const override {}
	void set_int(QString const &, int) const override {}
	void set_float(QString const &, float) const override {}
	void update_average(QString const &, float, float) const override {}
	QLocale get_locale() const override { return {}; }
};

}
}

#endif

namespace aw {
namespace {

AW_DEFINE_OBJECT_STUB(Platform)

}

void load_platform(QQmlEngine *engine)
{
	static Platform this_;
	AbstractPlatform &that = this_;

	Q_ASSERT(engine);
	Q_ASSERT(engine->rootContext());
	engine->rootContext()->setContextProperty("platform", &that);

	if(!that.translator_.load(that.get_locale(), "", "", ":/data/translations", ".ts.qm") && !that.translator_.load(":/data/translations/en.ts.qm")) qWarning() << "failed to load translation";
	qApp->installTranslator(&that.translator_);
}

}
