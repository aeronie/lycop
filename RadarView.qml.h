#include <QtQuick>

#include "game/Body.h"
#include "game/Scene.h"
#include "meta.private.h"

namespace aw {
namespace {

struct Attributes
{
	float vertex_in_scene_[2];
	float vertex_in_texture_[2];
	uint8 color_[4];

	struct Data
	{
		b2Vec2 position_;
		QColor color_;
		unsigned icon_;
		unsigned angle_;
	};

	static const QSGGeometry::AttributeSet &definition()
	{
		static const QSGGeometry::Attribute definitions[]
		{
			QSGGeometry::Attribute::create(0, 2, GL_FLOAT, true),
			QSGGeometry::Attribute::create(1, 2, GL_FLOAT),
			QSGGeometry::Attribute::create(2, 4, GL_UNSIGNED_BYTE),
		};

		static const QSGGeometry::AttributeSet definition {3, sizeof(Attributes), definitions};
		return definition;
	}
};

struct Uniforms
{
	std::unique_ptr<QSGTexture> texture_;
	float texture_ratio_;
	QMatrix4x4 transform_;

	struct Data
	{
		QMatrix4x4 transform_;
		float icon_size_;
	};

	int compare(const Uniforms *other) const
	{
		return
		texture_->textureId() < other->texture_->textureId() ? -1 :
		texture_->textureId() > other->texture_->textureId() ? 1 :
		0;
	}
};

struct Shader
: QSGSimpleMaterialShader<Uniforms>
{
	QSG_DECLARE_SIMPLE_COMPARABLE_SHADER(Shader, Uniforms)

	int texture_;
	int texture_ratio_;
	int transform_;

	Shader()
	{
		setShaderSourceFile(QOpenGLShader::Vertex, ":/RadarView.vsh");
		setShaderSourceFile(QOpenGLShader::Fragment, ":/RadarView.fsh");
	}

	QList<QByteArray> attributes() const override
	{
		return {"vertex_in_scene", "vertex_in_texture", "color"};
	}

	void resolveUniforms() override
	{
		texture_ = program()->uniformLocation("texture");
		texture_ratio_ = program()->uniformLocation("texture_ratio");
		transform_ = program()->uniformLocation("transform");
	}

	void updateState(const Uniforms *uniforms, const Uniforms *) override
	{
		uniforms->texture_->bind();
		program()->setUniformValue(texture_, 0);
		program()->setUniformValue(texture_ratio_, uniforms->texture_ratio_);
		program()->setUniformValue(transform_, uniforms->transform_);
	}
};

struct Node
: QSGGeometryNode
{
	Uniforms::Data global_;
	std::vector<Attributes::Data> data_;
	QSGGeometry geometry_ {Attributes::definition(), 0, 0};

	Node(QQuickWindow *window)
	{
		setFlags(UsePreprocess | OwnedByParent | OwnsMaterial);
		setGeometry(&geometry_);
		QImage image {":/RadarView.png"};
		QSGSimpleMaterial<Uniforms> *material = Shader::createMaterial();
		material->setFlag(QSGMaterial::Blending | QSGMaterial::RequiresFullMatrix);
		material->state()->texture_.reset(window->createTextureFromImage(image));
		material->state()->texture_ratio_ = float(image.width()) / float(image.height());
		setMaterial(material);
	}

	void preprocess() override
	{
		static_cast<QSGSimpleMaterial<Uniforms> *>(this->material())->state()->transform_ = global_.transform_;

		geometry()->allocate(int(data_.size() * 4), int(data_.size() * 6));
		markDirty(DirtyGeometry);
		uint16 *index = geometry()->indexDataAsUShort();
		Attributes *attributes = static_cast<Attributes *>(geometry()->vertexData());

		for(size_t i = 0; i < data_.size(); ++i)
		{
			for(int j = 0; j < 6; ++j)
			{
				static const size_t indices[] = {0, 0, 1, 2, 3, 3};
				*index++ = uint16(i * 4 + indices[j]);
			}
			for(int j = 0; j < 4; ++j)
			{
				static const unsigned indices[][4] {{0, 1, 2, 3}, {3, 2, 1, 0}, {2, 0, 3, 1}, {1, 3, 0, 2}};
				static const b2Vec2 vertices_in_object[] {{0, 0}, {0, 1}, {1, 0}, {1, 1}};
				*attributes++ =
				{ {
					data_[i].position_.x + (vertices_in_object[j].x - .5f) * global_.icon_size_,
					data_[i].position_.y + (vertices_in_object[j].y - .5f) * global_.icon_size_,
				},{
					vertices_in_object[indices[data_[i].angle_][j]].x,
					vertices_in_object[indices[data_[i].angle_][j]].y + float(data_[i].icon_),
				},{
					uint8(data_[i].color_.red()),
					uint8(data_[i].color_.green()),
					uint8(data_[i].color_.blue()),
					uint8(data_[i].color_.alpha()),
				} };
			}
		}
	}
};

struct RadarView
: QQuickItem
{
	AW_DECLARE_OBJECT_STUB(RadarView)
	AW_DECLARE_PROPERTY_STORED(aw::game::Scene *, scene) = 0;
	AW_DECLARE_PROPERTY_STORED(float, zoom) = 1; ///< px m⁻¹
	AW_DECLARE_PROPERTY_STORED(b2Vec2, center) = b2Vec2_zero; ///< m
	AW_DECLARE_PROPERTY_STORED(float, icon_size) = 16; ///< px
	AW_DECLARE_PROPERTY_STORED(bool, show_inside) = true;
	Uniforms::Data global_;
	std::vector<Attributes::Data> data_;

	void updatePolish() override
	{
		data_.clear();

		if(scene())
		{
			QRectF box = global_.transform_.inverted().mapRect(QRectF(QPointF(), QSizeF(width(), height())));

			for(b2Body const *child = scene()->world_->GetBodyList(); child; child = child->GetNext())
			{
				game::Body const *body = static_cast<game::Body const *>(child->GetUserData());

				if(body->icon())
				{
					if(box.contains(body->position().x, body->position().y))
					{
						if(show_inside_)
						{
							Attributes::Data data {body->position(), body->icon_color(), body->icon(), 0};
							data_.push_back(data);
						}
					}
					else
					{
						b2Vec2 direction = body->position() - center_;
						b2Vec2 distance = b2Abs(direction);
						unsigned angle = 0;
						float scale = .5f / zoom_;

						if(distance.x * height() < distance.y * width())
						{
							angle = direction.y > 0 ? 0 : 1;
							scale *= float(height() - icon_size_) / distance.y;
						}
						else
						{
							angle = direction.x > 0 ? 2 : 3;
							scale *= float(width() - icon_size_) / distance.x;
						}

						QColor color = body->icon_color();
						if(show_inside_) color.setAlphaF(std::max(.1, color.alphaF() * std::min(width(), height()) / 2 / zoom_ / direction.Length()));
						Attributes::Data data {scale * direction + center_, color, 0, angle};
						data_.push_back(data);
					}
				}
			}
		}
	}

	QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *) override
	{
		if(!node)
		{
			node = new Node(window());
		}

		Q_ASSERT(dynamic_cast<Node *>(node));
		static_cast<Node *>(node)->data_.swap(data_);
		static_cast<Node *>(node)->global_ = global_;
		return node;
	}

	void geometryChanged(const QRectF &new_geometry, const QRectF &old_geometry) override
	{
		update();
		update_transform();
		return QQuickItem::geometryChanged(new_geometry, old_geometry);
	}

	Q_SLOT void update_transform()
	{
		global_.transform_.setToIdentity();
		global_.transform_.translate(float(width()) / 2, float(height()) / 2);
		global_.transform_.scale(zoom(), zoom(), 1);
		global_.transform_.translate(-center().x, -center().y);
		global_.icon_size_ = icon_size() / zoom();
	}

protected:

	RadarView()
	{
		setFlags(ItemHasContents);
	}

	Q_SLOT void update()
	{
		QQuickItem::update();
		QQuickItem::polish();
	}
};

void RadarView::set_scene(game::Scene *const &scene)
{
	if(scene_ == scene) return;
	scene_ = scene;
	Q_EMIT scene_changed(scene_);
	update();
}

void RadarView::set_zoom(float const &zoom)
{
	if(zoom_ == zoom) return;
	zoom_ = zoom;
	Q_EMIT zoom_changed(zoom_);
	update_transform();
}

void RadarView::set_center(b2Vec2 const &center)
{
	if(center_ == center) return;
	center_ = center;
	Q_EMIT center_changed(center_);
	update_transform();
}

void RadarView::set_icon_size(float const &size)
{
	if(icon_size_ == size) return;
	icon_size_ = size;
	Q_EMIT icon_size_changed(icon_size_);
	update();
}

AW_DEFINE_OBJECT_STUB(RadarView)
AW_DEFINE_PROPERTY_STORED(RadarView, show_inside)

}
}
