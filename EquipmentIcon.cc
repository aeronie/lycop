#include <QtQuick>
#include <cmath>

#include "qt/ImageProvider.h"

namespace aw {
namespace {

struct EquipmentIcon
: QQuickImageProvider
{
	std::vector<int> centers; // centres des paraboles de l'enveloppe
	std::vector<float> intersections; // points d'intersection des paraboles de l'enveloppe
	std::vector<float> halo_data;
	std::vector<float> shadow_data;

	EquipmentIcon()
	: QQuickImageProvider(Image)
	{
		QImage shadow_image = QImage(":/fonts/shadow.png");
		Q_ASSERT(shadow_image.format() == QImage::Format_Indexed8);
		shadow_data.resize(shadow_image.width() * shadow_image.height());

		for(size_t i = 0; i < shadow_data.size(); ++i)
		{
			shadow_data[i] = float(shadow_image.bits()[i]) / 0XFF;
		}
	}

	static float square(int x)
	{
		return float(x * x);
	}

	void distance_transform(float const *input, int input_step, int input_size, float *output, int output_step)
	{
		centers.resize(input_size);
		centers[0] = 0;
		intersections.resize(input_size + 1);
		intersections[0] = -INFINITY;
		intersections[1] = +INFINITY;

		for(int k = 0, i = 1; i < input_size; ++i)
		{
			while((intersections[k + 1] = ( input[input_step * i] + square(i) - input[input_step * centers[k]] - square(centers[k]) ) / float(i - centers[k]) / 2) <= intersections[k]) --k; // si le nouveau point d'intersection est à gauche du précédent, alors la parabole précédente n'est finalement pas dans l'enveloppe, donc on la supprime et on réessaie
			++k;
			centers[k] = i;
			intersections[k + 1] = +INFINITY;
		}
		for(int k = 0, i = 0; i < input_size; ++i)
		{
			while(intersections[k + 1] < float(i)) ++k;
			output[output_step * i] = square(i - centers[k]) + input[input_step * centers[k]];
		}
	}

	void distance_transform(float *buffer_1, float *buffer_2, int row_count, int column_count, int row_step)
	{
		for(int column = 0; column < column_count; ++column) distance_transform(buffer_1 + column, row_step, row_count, buffer_2 + column, row_step);
		for(int row = 0; row < row_count; ++row) distance_transform(buffer_2 + row * row_step, 1, column_count, buffer_1 + row * row_step, 1);
	}

	QImage requestImage(QString const &id, QSize *size, QSize const &) override
	{
		QVector<QStringRef> data = id.splitRef(',');
		Q_ASSERT(data.count() == 5);

		QImage fg_image = QImage(":/data/game/equipments/" + data[0].toString() + "/icon.png");
		QImage silhouette = QImage(":/data/game/equipments/" + data[0].toString() + "/silhouette.png");
		Q_ASSERT(fg_image.size() == silhouette.size());
		Q_ASSERT(fg_image.format() == QImage::Format_RGB32); // en fait, BGRA
		Q_ASSERT(silhouette.format() == QImage::Format_Indexed8);
		size_t pixel_count = fg_image.width() * fg_image.height();
		Q_ASSERT(pixel_count == shadow_data.size());

		bool has_halo = data[1].toInt();
		uchar halo[] {0X66, 0XFF, 0XFF};
		if(has_halo)
		{
			halo_data.resize(pixel_count * 2);
			for(size_t i = 0; i < pixel_count; ++i) halo_data[i] = silhouette.bits()[i] > 0X66 ? 0 : +FLT_MAX;
			distance_transform(halo_data.data(), halo_data.data() + pixel_count, fg_image.height(), fg_image.width(), fg_image.width());
		}

		int mask = data[2].toInt();
		float masks[][3] = {{1, 1, 1}, {.5, .5, 1}, {.5, .5, .5}};

		bool is_enabled = data[3].toInt();
		bool is_pressed = data[4].toInt();

		for(size_t i = 0; i < pixel_count; ++i)
		{
			uchar *fg = &fg_image.bits()[i * 4];
			float halo_opacity = has_halo ? (1 - float(silhouette.bits()[i]) / 0XFF) * std::max(1 - std::pow(std::sqrt(halo_data[i]) / 12, .5f), 0.f) : 0;

			for(int j = 0; j < 3; ++j)
			{
				float c = fg[j];
				c += halo_opacity * halo[j];
				c *= masks[mask][j];
				if(is_pressed) c *= 1 - shadow_data[i];
				fg[j] = uchar(std::min(255.f, std::max(0.f, c)));
			}

			if(!is_enabled) fg[0] = fg[1] = fg[2] = uchar(qGray(fg[2], fg[1], fg[0]));
		}

		if(size) *size = fg_image.size();
		return fg_image;
	}
};

AW_REGISTER_IMAGE_PROVIDER(EquipmentIcon)

}
}
