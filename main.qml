import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
import aw 0.0
import aw.game 0.0 as A
import "controls"
import "game"
import "widgets"

ApplicationWindow
{
	id: window
	signal signal_game_start
	property real energy: 0
	readonly property bool animation_running: scene_loader.item && !scene_loader.item.scene.running
	readonly property bool game_running: !!queued_game_running.queued
	QueuedBinding {id: queued_game_running; direct: stack.depth == 1 && !stack.busy && !jobs.count && scene_loader.status == Loader.Ready}
	width: 640
	height: 480
	color: "black"
	title: qsTr("title")
	visible: true
	visibility: settings.full_screen ? Window.FullScreen : Window.Windowed
	readonly property Component screen_loading: Screen_Loading {}
	readonly property bool loading: jobs.count || scene_loader.status == Loader.Loading
	onLoadingChanged: if(loading) stack.push({item: screen_loading, immediate: true})
	Settings
	{
		id: settings
		function date_of(x) {return new Date(x.split("/").pop().toUpperCase())}
		function name_of(x) {var a = get(x + "/name"); return a ? decode(a) : date_of(x).toLocaleDateString()}
		function encode(x) {return encodeURI(x).replace(/[^A-Z0-9%]/g, function(x) {return "%" + x.charCodeAt(0).toString(16)})}
		function decode(x) {return decodeURI(x.toUpperCase())}
		readonly property real audio_gain: self.get("audio_gain", 1)
		readonly property real music_gain: self.get("music_gain", 1)
		readonly property real video_gain: self.get("video_gain", 1)
		readonly property bool full_screen: self.get("full_screen", false)
		readonly property bool velocity_absolute: self.get("velocity_absolute", false)
		readonly property int shader_quality: self.get("shader_quality", 1)
	}
	Settings
	{
		id: saved_game
		readonly property real matter: self.get("matter", 0)
		readonly property int science: self.get("science", 0)
		function add_matter(x) {set("matter", matter + x)}
		function add_science(x) {set("science", science + x)}
	}
	Controls {id: controls}
	Groups {id: groups}
	Altitudes {id: altitudes}
	Technologies {id: technologies}
	ScreenData {id: about_screen_messages; target: screen_messages; text: qsTr("Messages"); icon: "messages"}
	ThreadPool {id: jobs}
	Functions {id: global}
	Component.onCompleted: scene_loader.load(undefined, {file: "game/title/title.qml"})
	Connections
	{
		target: screen_hud.scene
		onImages_changed:
		{
			main_radar_view.update()
			//debug_view.update()
		}
	}
	Timer
	{
		interval: 10
		repeat: true
		running: scene_loader.item && animation_running
		onTriggered: scene_loader.item.scene.update()
	}
	Loader
	{
		id: scene_loader
		property var retry: undefined
		readonly property QtObject ship: item ? item.ship : null
		asynchronous: true
		property bool travel_map_2: (item && item.travel_map_2) ? item.travel_map_2 : false
		property bool last_level: (item && item.last_level) ? item.last_level : false
		property bool credit: (item && item.last_level && item.credit) ? item.credit : false
		Component.onDestruction: finalize()
		onLoaded: if(item)
		{
			controls.init(item.ship)
			item.ship.restore_equipments()
			item.scene.update()
		}
		function load(group, data)
		{
			finalize()
			active = false
			messages.clear()
			saved_game.load(group)
			for(var key in data) saved_game.set(key, data[key])
			source = saved_game.get("file")
			active = true
			retry = function() {load(group, data)}
		}
		function save()
		{
			saved_game.save()
			var group = settings.get_last_saved_game()
			retry = function() {load(group)}
		}
		function finalize()
		{
			if(item && item.finalize)
			{
				item.finalize()
				item.finalize = undefined
			}
			scene_view.unload()
			music.unload()
		}
	}
	ImageView
	{
		id: scene_view
		anchors.fill: parent
		model: scene_loader.item ? scene_loader.item.scene.images : undefined
		center: scene_loader.ship.position// il ne faut pas déplacer la caméra quand ship devient null (cas du game over)
		zoom: Math.max(width, height) / 50
		quality: settings.shader_quality
	}
	/*DebugView
	{
		id: debug_view
		anchors.fill: scene_view
		scene: scene_loader.item ? scene_loader.item.scene : null
		center: scene_view.center
		zoom: scene_view.zoom
		opacity: 0.5
		visible: true
	}*/
	SoundView
	{
		id: scene_audio
		model: scene_loader.item ? scene_loader.item.scene.sounds : undefined
		center: scene_view.center
		gain: game_running ? settings.audio_gain : 0
		Behavior on gain {SmoothedAnimation {velocity: animation_running ? 1e6 : 1}}
	}
	SoundView
	{
		id: music
		model: sounds.data
		gain: settings.music_gain
		Behavior on gain {NumberAnimation {}}
		property QtObject sounds: A.SoundList {}
		property Component delegate: A.Sound
		{
			looping: true
			Behavior on scale {NumberAnimation {}}
			onScaleChanged: if(!scale) music.sounds.remove(this)
		}
		onObjectNameChanged:
		{
			for(var i = 0; i < sounds.qml_data.length; ++i) sounds.qml_data[i].scale = 0
			var x = delegate.createObject(null, {objectName: objectName, scale: 0})
			sounds.insert(x)
			x.scale = 1
		}
	}
	StackView
	{
		id: stack
		anchors.fill: parent
		focus: true
		initialItem: [screen_hud, screen_menu]
		onCurrentItemChanged: if(currentItem) currentItem.focus = true
		Keys.onEscapePressed: push(screen_menu)
		delegate: StackViewDelegate
		{
			pushTransition: StackViewTransition
			{
				PropertyAnimation {target: enterItem; property: "y"; from: -enterItem.height; to: 0}
				PropertyAnimation {target: exitItem; property: "y"; from: 0; to: exitItem.height}
			}
			popTransition: StackViewTransition
			{
				PropertyAnimation {target: enterItem; property: "y"; from: enterItem.height; to: 0}
				PropertyAnimation {target: exitItem; property: "y"; from: 0; to: -exitItem.height}
			}
		}
	}
	RadarView
	{
		id: main_radar_view
		anchors.fill: scene_view
		zoom: scene_view.zoom
		center: scene_view.center
		scene: screen_hud.scene
		show_inside: false
		Behavior on opacity {NumberAnimation {}}
		opacity: game_running ? 1 : 0
	}
	
	readonly property Item screen_hud: Screen_HUD
	{
		id: inputs
		enabled: Stack.status == Stack.Active
		focus: enabled
		last_level: scene_loader.last_level
		credit: scene_loader.credit
		scene: scene_loader.item ? scene_loader.item.scene : null
		zoom: scene_view.zoom
		angle: scene_view.angle
		center: scene_view.center
		ship: scene_loader.ship
		screens: [screen_menu.about_screen_equipments, screen_menu.about_screen_technologies, screen_menu.about_screen_travel, about_screen_messages, screen_menu.about_screen_start]
		onTriggered_show:
		{
			if(~[0, 1, 2, 4].indexOf(index))
			{
				screen_menu.show(screens[index].target)
				stack.push(screen_menu)
			}
			else
			{
				stack.push(screens[index].target)
			}
		}
	}
	readonly property Item screen_messages: FocusScope
	{
		clip: true
		MessageLog
		{
			id: messages
			onTriggered_open: stack.push(parent)
			onTriggered_close: stack.pop()
			anchors.fill: parent
			anchors.margins: 6
			focus: true
		}
	}
	readonly property Item screen_video: Screen_Video
	{
		onTriggered_open: stack.push(this)
		onTriggered_close: stack.pop()
	}
	readonly property Item screen_menu: Screen_Menu
	{
		ship: scene_loader.ship
		last_level: scene_loader.last_level
		travel_map_2 : scene_loader.travel_map_2
		onTriggered_close: stack.pop()
		onTriggered_quit: stack.push(screen_quit)
		onTriggered_load:
		{
			stack.pop(null)
			scene_loader.load(group, data)
		}
	}

	// -- Quand on a perdu

	Connections
	{
		target: scene_loader.item
		onGame_overChanged: 
		{
			if(target.game_over) 
			{
				stack.push(screen_game_over)
			}
		}
	}
	readonly property Component screen_game_over: Screen_GameOver
	{
		change_int: scene_loader ? (scene_loader.item ? scene_loader.item.game_over : false) : false
		onTriggered_retry:
		{
			stack.pop(null)
			scene_loader.retry()
		}
		onTriggered_load:
		{
			stack.pop(null)
			screen_menu.show(screen_menu.screen_load_game)
			stack.push(screen_menu)
			scene_loader.load(undefined, {file: "game/title/title.qml", immediate: true})
		}
	}
	function back_to_main_menu()
	{
		stack.pop(null)
		screen_menu.show(screen_menu.screen_start)
		stack.push(screen_menu)
		scene_loader.load(undefined, {file: "game/title/title.qml", immediate: true})
	}

	// -- Quand on veut fermer le jeu

	readonly property Component screen_quit: Screen_Quit
	{
		onTriggered_back: stack.pop()
	}
	onClosing:
	{
		if(stack.currentItem.is_screen_quit) return
		close.accepted = false
		stack.push(screen_quit)
	}

	// -- Fonctions pour faciliter la création des vaisseaux

	function mask_equipment(equipment)
	{
		if(!equipment) return "red"
		var x = equipment.health / equipment.max_health
		return Qt.rgba(1, x, x, 1)
	}
	function mask_shield(ship)
	{
		return	ship.aggressive_shield_active_damages ? "#F00" :
			ship.absorbtion_damages_multiplier ? "#FFF" :
			(ship.shield >= 1) && ship.max_shield ? Qt.rgba(0, 0.4, 1, ship.shield / ship.max_shield * 0.7 + 0.3) :
			"transparent"
	}

	// -- Autres fonctions

	function random_index(n) {return Math.min(Math.floor(Math.random() * n), n - 1)} // bug soupçonné dans Math.random() : peut retourner 1
}
