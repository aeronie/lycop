uniform sampler2D opacity_map;
uniform float qt_Opacity;
varying vec2 fragment_in_texture_;
varying vec4 color_;

void main()
{
	gl_FragColor = color_ * texture2D(opacity_map, fragment_in_texture_).r * qt_Opacity;
}
