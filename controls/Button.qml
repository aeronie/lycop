import QtQuick 2.0
import QtQuick.Controls 1.2
import aw 0.0
import "styles" as S

Button
{
	id: root
	activeFocusOnPress: true
	style: S.Button {}
	Repeater
	{
		model: root.iconSource == "" ? 0 : 1
		ToolTip
		{
			text: tooltip
			vertical: root.transformOrigin == Item.Bottom
		}
	}
	MouseArea
	{
		anchors.fill: root
		acceptedButtons: Qt.NoButton
		cursor: "hand-pointing"
	}
}
