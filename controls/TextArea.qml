import QtQuick 2.0
import QtQuick.Controls 1.2
import "styles" as S

TextArea
{
	textFormat: TextEdit.RichText
	readOnly: true
	menu: null
	style: S.TextArea {}
}
