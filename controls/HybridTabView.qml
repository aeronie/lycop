import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

FocusScope
{
	id: root
	default property alias children: layout.children
	readonly property alias stack: stack
	BorderImage
	{
		anchors.fill: parent
		source: "styles/TabView_frame.png"
		verticalTileMode: BorderImage.Repeat
	}
	ColumnLayout
	{
		anchors.fill: parent
		anchors.margins: spacing
		anchors.topMargin: 0
		RowLayout
		{
			id: layout
			spacing:
			{
				var n = -1
				var sum = 0
				for(var i = 0; i < children.length; ++i)
				{
					sum += children[i].implicitWidth
					if(children[i].visible && children[i].implicitWidth) ++n
				}
				return n > 0 ? (parent.width - sum) / n : 0
			}
		}
		StackView
		{
			id: stack
			Layout.fillWidth: true
			Layout.fillHeight: true
			onCurrentItemChanged: if(currentItem) currentItem.focus = true
			delegate: StackViewDelegate
			{
				function getTransition(x)
				{
					if(x.name == "pushTransition") return pushTransition
					if(x.name == "popTransition") return popTransition
					if(x.enterItem.z > x.exitItem.z) return popTransition
					return pushTransition
				}
				pushTransition: StackViewTransition
				{
					PropertyAnimation {target: enterItem; property: "x"; from: root.width; to: 0}
					PropertyAnimation {target: exitItem; property: "x"; from: 0; to: -root.width}
				}
				popTransition: StackViewTransition
				{
					PropertyAnimation {target: enterItem; property: "x"; from: -root.width; to: 0}
					PropertyAnimation {target: exitItem; property: "x"; from: 0; to: root.width}
				}
			}
		}
	}
	function show(item, properties)
	{
		stack.pop(null)
		if(item == stack.initialItem) return
		stack.initialItem = item
		stack.push({item: item, properties: properties, replace: true, immediate: !root.visible})
	}
}
