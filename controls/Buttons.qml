import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import "styles" as S
import "."

GridLayout
{
	id: root
	property list<Action> model
	property Component style: S.Button {}
	property Item menu: null
	Layout.fillWidth: false
	Layout.fillHeight: false
	Layout.alignment: Qt.AlignRight | Qt.AlignBottom
	Component.onDestruction: hide_menu()
	Repeater
	{
		model: root.model
		Button
		{
			action: modelData
			Layout.fillWidth: true
			Layout.fillHeight: true
			style: root.style
			transformOrigin: root.flow == GridLayout.TopToBottom ? Item.Bottom : Item.Right
		}
	}
	Component
	{
		id: menu_factory
		Item
		{
			id: menu
			implicitWidth: column.width + column.x * 2
			implicitHeight: column.height + column.y * 2
			property bool exited: false
			readonly property bool hovered:
			{
				if(!exited) return true
				if(area.containsMouse) return true
				for(var i in column.children)
				{
					var a = column.children[i].children[0]
					if(a && a.containsMouse) return true
				}
				return false
			}
			Component.onCompleted:
			{
				var p = global.get_mouse_position(window)
				x = p.x + width < window.width ? p.x - 5 : p.x - width + 5
				y = p.y + height < window.height ? p.y - 5 : p.y - height + 5
			}
			MouseArea
			{
				id: area
				anchors.fill: parent
				anchors.margins: -1
				hoverEnabled: true
				onExited: menu.exited = true
				BorderImage
				{
					anchors.fill: parent
					anchors.margins: -1
					source: "styles/button-pressed.sci"
				}
			}
			ColumnLayout
			{
				id: column
				spacing: 0
				x: 0
				y: 3
				Repeater
				{
					model: root.model
					Item
					{
						Layout.fillWidth: true
						enabled: modelData.enabled
						implicitWidth: row.width + row.x * 2
						implicitHeight: row.height + row.y * 2
						MouseArea
						{
							anchors.fill: parent
							hoverEnabled: true
							onExited: menu.exited = true
							onClicked:
							{
								modelData.trigger()
								hide_menu()
							}
							Rectangle
							{
								anchors.fill: parent
								visible: enabled && parent.containsMouse
								color: "white"
							}
						}
						RowLayout
						{
							id: row
							opacity: enabled ? 1 : 0.5
							x: 5
							y: 2
							Image
							{
								source: modelData.iconSource
							}
							Label
							{
								text: modelData.text
								color: "black"
							}
						}
					}
				}
			}
		}
	}
	Timer
	{
		interval: 100
		running: menu && !menu.hovered
		onTriggered: hide_menu()
	}
	function show_menu()
	{
		hide_menu()
		menu = menu_factory.createObject(window)
	}
	function hide_menu()
	{
		if(menu)
		{
			menu.destroy()
			menu = null
		}
	}
}
