import QtQuick 2.0
import ".."

Box
{
	source: "TableView_headerDelegate%1".arg(styleData.column < 0 ? "_background%1.png" : "%1.sci").arg(control.activeFocus ? "-focus" : "")
	Label
	{
		anchors.fill: parent
		anchors.leftMargin: 3
		anchors.rightMargin: 3
		text: styleData.value
		color: "black"
		elide: Text.ElideRight
		horizontalAlignment: styleData.textAlignment
		verticalAlignment: Text.AlignVCenter
	}
}
