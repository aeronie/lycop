import QtQuick 2.0
import ".."
import "."

Label
{
	text: control.text
	color: control.activeFocus ? "red" : "white"
	verticalAlignment: Text.AlignVCenter
	horizontalAlignment: Text.AlignHCenter
	font.pointSize: Private.font.pointSize * 1.5
	visible: control.enabled
}
