import QtQuick 2.0
import "."

Rectangle
{
	visible: color.a
	color: Qt.rgba(1, 1, 1, styleData.selected ? 1 : styleData.alternate ? 0.1 : 0)
	height: Private.lineSpacing * 1.5
}
