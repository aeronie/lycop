import QtQuick.Controls.Styles 1.2

SwitchStyle
{
	groove: Switch_groove {implicitWidth: implicitHeight * 2.5}
	handle: Slider_handle {}
}
