import QtQuick.Controls.Styles 1.2

TableViewStyle
{
	frame: ScrollView_frame {}
	handle: ScrollView_handle {}
	scrollBarBackground: ScrollView_scrollBarBackground {}
	incrementControl: null
	decrementControl: null
	corner: null
	handleOverlap: 0
	padding.left: 2
	padding.right: 2
	padding.top: 2
	padding.bottom: 2

	textColor: "white"
	highlightedTextColor: "black"
	backgroundColor: "transparent"
	rowDelegate: TableView_rowDelegate {}
	itemDelegate: TableView_itemDelegate_label {}
	headerDelegate: TableView_headerDelegate {}
}
