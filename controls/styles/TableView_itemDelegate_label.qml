import QtQuick 2.0
import ".."

TableView_itemDelegate
{
	implicitWidth: label.implicitWidth + 7
	Label
	{
		id: label
		anchors.fill: parent
		anchors.leftMargin: 3
		anchors.rightMargin: 3
		text: column && column.value ? column.value(styleData) : styleData.value.toString()
		color: hasActiveFocus ? "red" : styleData.textColor
		elide: styleData.elideMode
		horizontalAlignment: styleData.textAlignment
		verticalAlignment: Text.AlignVCenter
	}
}
