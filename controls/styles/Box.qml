import QtQuick 2.0
import "."

Item
{
	property alias source: image.source
	property alias margins: image.anchors
	implicitHeight: Private.lineSpacing * 1.5
	implicitWidth: implicitHeight
	BorderImage
	{
		id: image
		anchors.fill: parent
	}
}
