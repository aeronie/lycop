#!/usr/bin/fontforge

import fontforge
import sys

def generate(file, weight, weight_name, style, style_name):
	font = fontforge.open(file)
	font.selection.all()

	font.unlinkReferences()
	font.removeOverlap()
	font.simplify()
	font.correctDirection()
	font.canonicalContours()
	font.canonicalStart()

	font.em = 1000
	for glyph in font.glyphs(): glyph.width += 2.5 * 1000. / 22.

	if weight: font.changeWeight(weight, "custom", 0, 0, "squish")
	if style: font.transform((1, 0, 0.25, 1, 0, 0))

	font.round()
	font.autoHint()
	font.autoInstr()
	font.weight = weight_name
	font.fontname = "{0}{1}{2}".format(font.familyname, weight_name, style_name)
	font.fullname = "{0} {1} {2}".format(font.familyname, weight_name, style_name)
	font.generate("{0}.otf".format(font.fontname))
	font.close()

for file in sys.argv[1:]:
	for weight, weight_name in ((0, ""), (25, "Bold"), (50, "Black"), (-25, "Light")):
		for style, style_name in ((False, ""), (True, "Oblique")):
			generate(file, weight, weight_name, style, style_name)
