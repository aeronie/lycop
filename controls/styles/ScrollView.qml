import QtQuick 2.0
import QtQuick.Controls.Styles 1.0

ScrollViewStyle
{
	readonly property Component _frame: ScrollView_frame {}
	frame: control.frameVisible ? _frame : null
	handle: ScrollView_handle {}
	scrollBarBackground: ScrollView_scrollBarBackground {}
	incrementControl: null
	decrementControl: null
	corner: null
	handleOverlap: 0
	padding.left: 2
	padding.right: 2
	padding.top: 2
	padding.bottom: 2
}
