import QtQuick.Controls.Styles 1.2
import "."

SpinBoxStyle
{
	textColor: Qt.rgba(1, 1, 1, control.enabled ? 1 : 0.5)
	selectionColor: "white"
	selectedTextColor: "black"
	font: Private.font
	padding.left: 2+4
	padding.right: 2+4+5+4
	background: TextField_background {}
	incrementControl: SpinBox_incrementControl {}
	decrementControl: SpinBox_decrementControl {}
}
