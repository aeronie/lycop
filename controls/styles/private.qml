pragma Singleton
import QtQuick 2.4

FontMetrics
{
	readonly property FontLoader loader: FontLoader {source: "font.otf"}
	font.family: loader.name
	font.pixelSize: 16
}
