import QtQuick 2.0
import QtQuick.Controls.Styles 1.2
import "."

TextAreaStyle
{
	frame: ScrollView_frame {}
	handle: ScrollView_handle {}
	scrollBarBackground: ScrollView_scrollBarBackground {}
	incrementControl: null
	decrementControl: null
	corner: null
	handleOverlap: 0
	padding.left: 2
	padding.right: 2
	padding.top: 2
	padding.bottom: 2

	backgroundColor: "transparent"
	renderType: Text.NativeRendering
	font: Private.font
	textColor: "white"
	selectedTextColor: "black"
	selectionColor: "white"
}
