import QtQuick 2.0
import "."

Item
{
	implicitHeight: 10
	implicitWidth: 100
	BorderImage
	{
		height: parent.height
		width: styleData.handlePosition
		source: "liquid.sci"
	}
	BorderImage
	{
		x: styleData.handlePosition
		height: parent.height
		width: parent.width - x
		source: "groove.sci"
	}
}
