import QtQuick 2.0

MouseArea
{
	acceptedButtons: Qt.RightButton
	onImplicitWidthChanged: if(table) table.queue.direct = true
	readonly property QtObject column: table ? table.getColumn(styleData.column) : null
	readonly property QtObject row: parent.parent.parent.parent
	readonly property bool hasActiveFocus: row && row.activeFocus // nécessaire parce que styleData.hasActiveFocus est toujours faux
	readonly property QtObject table:
	{
		var x = row
		while(x && !x.getColumn) x = x.parent
		return x
	}
	onClicked:
	{
		table.selection.clear()
		table.selection.select(styleData.row)
		table.currentRow = styleData.row
		table.pressAndHold(styleData.row)
	}
}
