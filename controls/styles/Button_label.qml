import QtQuick 2.0
import QtQuick.Controls.Private 1.0
import ".."

Column
{
	y: control.checked ? 2 : control.pressed ? 0 : -2
	opacity: control.enabled ? 1 : 0.5
	Row
	{
		anchors.horizontalCenter: parent.horizontalCenter
		Image
		{
			source: control.iconSource
		}
		Label
		{
			visible: control.iconSource == ""
			text: StyleHelpers.stylizeMnemonics(control.text)
			color: "black"
		}
	}
}
