import QtQuick 2.0

Item
{
	implicitWidth: padding.right
	opacity: control.enabled ? styleData.downPressed ? 1 : 0.75 : 0.5
	Image
	{
		source: "SpinBox_decrement.png"
		anchors.centerIn: parent
		anchors.verticalCenterOffset: ((height + 2) - control.height / 2) / 2
	}
}
