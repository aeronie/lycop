import QtQuick 2.0

Item
{
	implicitWidth: padding.right
	opacity: control.enabled ? styleData.upPressed ? 1 : 0.75 : 0.5
	Image
	{
		source: "SpinBox_increment.png"
		anchors.centerIn: parent
		anchors.verticalCenterOffset: (control.height / 2 - (height + 2)) / 2
	}
}
