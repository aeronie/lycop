import QtQuick 2.0

TableView_itemDelegate
{
	implicitWidth: height
	Image
	{
		anchors.fill: parent
		source: column ? column.value ? column.value(styleData) : styleData.value.toString() : ""
	}
}
