import QtQuick 2.0
import QtQuick.Controls.Styles 1.0
import "."

TextFieldStyle
{
	background: TextField_background {}
	font: Private.font
	textColor: "white"
	placeholderTextColor: Qt.rgba(1, 1, 1, 0.5)
	selectedTextColor: "black"
	selectionColor: "white"
}
