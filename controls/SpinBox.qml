import QtQuick.Controls 1.0
import "styles" as S

SpinBox
{
	implicitWidth: __styleData.contentWidth + __style.padding.left + __style.padding.right
	style: S.SpinBox {}
}
