import QtQuick 2.0
import QtQuick.Controls 1.2
import "styles" as S

Label
{
	color: "white"
	font.family: S.Private.font.family
	font.pointSize: S.Private.font.pointSize
	font.capitalization: Font.AllUppercase
}
