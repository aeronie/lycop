import QtQuick 2.0
import aw 0.0
import "styles" as S

MouseArea
{
	id: area
	property string text
	property bool vertical: false
	property Rectangle balloon: null
	anchors.fill: parent
	acceptedButtons: Qt.NoButton
	hoverEnabled: true
	Component.onDestruction: if(balloon) balloon.destroy()
	Component
	{
		id: balloon_factory
		Rectangle
		{
			readonly property var dummy:
			{
				area.parent.x
				area.parent.y
				var c = area.mapToItem(null, 0, 0)
				if(area.vertical)
				{
					x = c.x + (c.x < window.width / 2 ? area.width + 5 : -5 - width)
					y = Math.max(0, Math.min(window.height - height, c.y + (area.height - height) / 2))
				}
				else
				{
					x = Math.max(0, Math.min(window.width - width, c.x + (area.width - width) / 2))
					y = c.y + (c.y < window.height / 2 ? area.height + 5 : -5 - height)
				}
			}
			width: label.contentWidth + label.x * 2
			height: label.contentHeight + label.x * 2
			color: "black"
			Label
			{
				id: label
				text: area.text
				x: font.pixelSize * 0.25
				y: x
				width: 200
				wrapMode: Text.Wrap
				font.pointSize: S.Private.font.pointSize * 0.9
				font.capitalization: Font.MixedCase
			}
		}
	}
	Timer
	{
		interval: 1000
		running: area.containsMouse && !balloon
		onTriggered:
		{
			balloon = balloon_factory.createObject(window)
		}
	}
	Timer
	{
		interval: 100
		running: !area.containsMouse && balloon
		onTriggered:
		{
			balloon.destroy()
			balloon = null
		}
	}
}
