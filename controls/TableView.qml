import QtQuick 2.0
import QtQuick.Controls 1.0
import aw 0.0
import "styles" as S

TableView
{
	id: root
	style: S.TableView {}
	readonly property Connections connections: Connections
	{
		target: root
		onModelChanged:
		{
			for(var i = 0; i < columnCount; ++i)
			{
				var a = getColumn(i).valueChanged
				if(a) a()
			}
		}
	}
	readonly property Connections connections_scroller: Connections
	{
		target: root.__scroller
		onAvailableHeightChanged: root.__scroller.availableHeight = root.contentItem.height
		// sinon il ne tient pas compte de l'en-tête pour l'affichage de la barre de défilement
	}
	readonly property QtObject queue: QueuedBinding
	{
		direct: false
		onQueuedChanged: if(queued)
		{
			direct = false
			root.resizeColumnsToContents()
		}
	}
}
