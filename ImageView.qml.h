#include <QtQuick>
#include <cmath>
#include <memory>

#include "game/Image.h"
#include "ArrayView.h"
#include "Mutex.h"
#include "NodePool.h"
#include "Profiler.h"
#include "meta.private.h"

Q_DECLARE_METATYPE(aw::ArrayView<aw::game::Image *>)
static const aw::register_type registered_QList = qRegisterMetaType<aw::ArrayView<aw::game::Image *>>();

namespace aw {
namespace {

struct Attributes
{
	float vertex_in_scene_[3];
	uint8 mask_[4];
	float vertex_in_color_map_[2];
	float vertex_in_normal_map_[2];
	float direction_[2];

	struct Data
	{
		QString material_;
		QColor mask_;
		QMatrix4x4 transform_;
		float angle_ = 0;

		bool operator<(const Data &other) const
		{
			return transform_(3, 3) > other.transform_(3, 3);
		}

		void transform(float x, float y, float z, float a, float s)
		{
			angle_ += a;
			transform_.translate(x, y, z);
			transform_.rotate(a * float(180 / M_PI), 0, 0, 1);
			transform_.scale(s, s, 1);
		}
	};

	static const QSGGeometry::AttributeSet &definition()
	{
		static const QSGGeometry::Attribute definitions[]
		{
			QSGGeometry::Attribute::create(0, 3, GL_FLOAT, true),
			QSGGeometry::Attribute::create(1, 4, GL_UNSIGNED_BYTE),
			QSGGeometry::Attribute::create(2, 2, GL_FLOAT),
			QSGGeometry::Attribute::create(3, 2, GL_FLOAT),
			QSGGeometry::Attribute::create(4, 2, GL_FLOAT),
		};

		static const QSGGeometry::AttributeSet definition {5, sizeof(Attributes), definitions};
		return definition;
	}
};

struct Uniforms
{
	std::unique_ptr<QSGTexture> color_map_;
	std::unique_ptr<QSGTexture> normal_map_;

	int compare(const Uniforms *other) const
	{
		return
		color_map_->textureId() < other->color_map_->textureId() ? -1 :
		color_map_->textureId() > other->color_map_->textureId() ? 1 :
		normal_map_ == other->normal_map_ ? 0 :
		!normal_map_ ? -1 :
		!other->normal_map_ ? 1 :
		normal_map_->textureId() < other->normal_map_->textureId() ? -1 :
		normal_map_->textureId() > other->normal_map_->textureId() ? 1 :
		0;
	}
};

template<class _type>
struct Shader
: QSGSimpleMaterialShader<_type>
, QOpenGLFunctions
{
	QSG_DECLARE_SIMPLE_COMPARABLE_SHADER(Shader, _type)

	int color_map_;
	int normal_map_;
	int has_normal_map_;

	Shader()
	{
		this->setShaderSourceFile(QOpenGLShader::Vertex, ":/ImageView.vsh");
		this->setShaderSourceFiles(QOpenGLShader::Fragment, _type::shaders());
		initializeOpenGLFunctions();
	}

	QList<QByteArray> attributes() const override
	{
		return {"vertex_in_scene", "mask", "vertex_in_color_map", "vertex_in_normal_map", "direction"};
	}

	void resolveUniforms() override
	{
		color_map_ = this->program()->uniformLocation("color_map");
		normal_map_ = this->program()->uniformLocation("normal_map");
		has_normal_map_ = this->program()->uniformLocation("has_normal_map");
	}

	void updateState(const _type *uniforms, const _type *) override
	{
		int active_texture;
		glGetIntegerv(GL_ACTIVE_TEXTURE, &active_texture);

		glActiveTexture(GL_TEXTURE0);
		uniforms->color_map_->bind();
		this->program()->setUniformValue(color_map_, 0);

		this->program()->setUniformValue(has_normal_map_, !!uniforms->normal_map_);
		if(uniforms->normal_map_)
		{
			glActiveTexture(GL_TEXTURE1);
			uniforms->normal_map_->bind();
			this->program()->setUniformValue(normal_map_, 1);
		}

		glActiveTexture(active_texture);
	}
};

struct Mesh
: QSGGeometryNode
{
	QSGGeometry geometry_ {Attributes::definition(), 4};

	Mesh()
	{
		setGeometry(&geometry_);
	}
};

struct TextureLoader
{
	QImage image;
	std::unique_ptr<QSGTexture> *texture;
};

/*

Qt identifie les shaders par leur type, donc il faut définir un type différent pour chaque configuration possible.
On les stocke uniformément comme des QSGMaterial, et on les reinterpret_cast() pour accéder à state().

*/
struct Uniforms_flat: Uniforms {static const QStringList &shaders() { static const QStringList shaders {":/ImageView.fsh"}; return shaders; }};
struct Uniforms_diffuse: Uniforms {static const QStringList &shaders() { static const QStringList shaders {":/option-diffuse.fsh", ":/ImageView.fsh"}; return shaders; }};
struct Uniforms_specular: Uniforms {static const QStringList &shaders() { static const QStringList shaders {":/option-diffuse.fsh", ":/option-specular.fsh", ":/ImageView.fsh"}; return shaders; }};
typedef std::map<QString, std::unique_ptr<QSGMaterial>> MaterialList;
static MaterialList materials_;
static Mutex mutex_;
static std::vector<std::unique_ptr<QSGMaterial>> removed_materials_;
static std::vector<TextureLoader> texture_loaders_;
static QAtomicInt texture_loader_count_ = 0;
static int quality_ = 2;
static bool dirty_ = false;

static QSGMaterial *get_material()
{
	switch(quality_)
	{
		case 2: return Shader<Uniforms_specular>::createMaterial();
		case 1: return Shader<Uniforms_diffuse>::createMaterial();
		default: return Shader<Uniforms_flat>::createMaterial();
	}
}

static Uniforms *get_uniforms(MaterialList::mapped_type const &material)
{
	return reinterpret_cast<QSGSimpleMaterial<Uniforms> *>(material.get())->state();
}

struct ImageView
: QQuickItem
{
	AW_DECLARE_OBJECT_STUB(ImageView)
	AW_DECLARE_PROPERTY_STORED(QVariant, model);
	AW_DECLARE_PROPERTY_STORED(float, zoom) = 100; ///< px m⁻¹
	AW_DECLARE_PROPERTY_STORED(float, angle) = 0; ///< rad
	AW_DECLARE_PROPERTY_STORED(b2Vec2, center) = b2Vec2_zero; ///< m
	AW_DECLARE_PROPERTY_READONLY(QMatrix4x4, transform)
	AW_DECLARE_PROPERTY(int, quality)
	QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *) override;
	std::vector<Attributes::Data> data_;

	void load(QString const &file, std::unique_ptr<QSGTexture> *texture, bool required)
	{
		QImage image {file};
		if(!required && image.isNull()) return;
		Q_ASSERT(!image.isNull());
		image = image.convertToFormat(QImage::Format_ARGB32_Premultiplied);
		Q_ASSERT(!image.isNull());
		static Mutex mutex;
		std::lock_guard<Mutex> lock {mutex};
		texture_loaders_.push_back({image, texture});
	}

	void updatePolish() override
	{
		data_.clear();
		QRectF box(QPointF(), QSizeF(width(), height()));
		Attributes::Data data0;
		data0.transform_ = transform();
		static const QRectF query {QPointF(-1, -1), QPointF(1, 1)};

		if(!model().isNull())
		{
			Q_ASSERT(model_.canConvert<ArrayView<game::Image *>>());

			for(auto &image: model_.value<ArrayView<game::Image *>>())
			{
				if(!image) continue;
				QVector3D position = image->transform().map(QVector3D());
				QVector3D direction = image->transform().map(QVector3D(1, 0, 0)) - position;
				Attributes::Data data1 = data0;
				data1.transform(position.x(), position.y(), image->z(), std::atan2(direction.y(), direction.x()), direction.length());

				if(box.intersects(data1.transform_.mapRect(query)))
				{
					data1.material_ = image->material();
					data1.mask_ = image->mask();
					data_.push_back(data1);
				}
			}

			stable_sort(data_.begin(), data_.end());
		}
	}

	void geometryChanged(const QRectF &new_geometry, const QRectF &old_geometry) override
	{
		Q_EMIT transform_changed(transform());
		update();
		return QQuickItem::geometryChanged(new_geometry, old_geometry);
	}

protected:

	ImageView()
	{
		setFlags(ItemHasContents);
	}

	Q_SLOT void update()
	{
		QQuickItem::update();
		QQuickItem::polish();
	}

	Q_SLOT void preload(QString const &name)
	{
		{
			std::unique_lock<Mutex> lock {mutex_};
			MaterialList::const_iterator material = materials_.find(name);

			if(material == materials_.end())
			{
				AW_TIME("ImageView", time, name)
				material = new_material(name);
				++texture_loader_count_;
				lock.unlock();
				load(material);
				--texture_loader_count_;
			}
		}
		if(!QMetaObject::invokeMethod(this, "update")) Q_UNREACHABLE();
	}

	Q_SLOT void unload()
	{
		std::unique_lock<Mutex> lock {mutex_};

		for(auto p = materials_.begin(); p != materials_.end();)
		{
			if(!p->first.startsWith("e"))
			{
				removed_materials_.emplace_back(move(p->second));
				materials_.erase(p++);
			}
			else
			{
				++p;
			}
		}

		materials_.clear();
	}

public:

	MaterialList::const_iterator new_material(QString const &name)
	{
		return materials_.insert(MaterialList::value_type(name, MaterialList::mapped_type(get_material()))).first;
	}

	void load(MaterialList::const_iterator const &material)
	{
		material->second->setFlag(QSGMaterial::Blending);
		Uniforms *uniforms = get_uniforms(material->second);
		load(":/data/game/" + material->first + "/colors.png", &uniforms->color_map_, true);
		load(":/data/game/" + material->first + "/normals.png", &uniforms->normal_map_, false);
	}
};

struct Node
: QSGNode
{
	std::vector<Attributes::Data> data_;
	ImageView *window_ = 0;
	NodePool<Mesh> pool_;

	Node(ImageView *window)
	: window_(window)
	{
		Q_ASSERT(window);
		setFlags(UsePreprocess | OwnedByParent);
	}

	void preprocess() override
	{
		while(QSGNode *child = firstChild())
		{
			pool_.release(child);
		}

		if(!mutex_.try_lock()) return;
		std::lock_guard<Mutex> lock {mutex_, std::adopt_lock};
		if(texture_loader_count_) return;
		removed_materials_.clear();

		for(const Attributes::Data &data: data_)
		{
			MaterialList::const_iterator material = materials_.find(data.material_);

			if(material == materials_.end())
			{
				AW_TIME("ImageView", time, data.material_)
				material = window_->new_material(data.material_);
				window_->load(material);
			}
			for(TextureLoader &loader: texture_loaders_)
			{
				static const QQuickWindow::CreateTextureOptions options {QQuickWindow::TextureHasAlphaChannel | QQuickWindow::TextureOwnsGLTexture | QQuickWindow::TextureCanUseAtlas};
				std::unique_ptr<QSGTexture> &p = *loader.texture;
				p.reset(window_->window()->createTextureFromImage(loader.image, options));
				Q_ASSERT(p);
				Q_ASSERT(!p->textureSize().isEmpty());
				p->setFiltering(QSGTexture::Linear);
				p->setMipmapFiltering(QSGTexture::None);
				p->setHorizontalWrapMode(QSGTexture::ClampToEdge);
				p->setVerticalWrapMode(QSGTexture::ClampToEdge);
			}
			if(dirty_)
			{
				for(auto &material: materials_)
				{
					MaterialList::mapped_type new_material {get_material()};
					new_material->setFlag(material.second->flags());
					*get_uniforms(new_material) = std::move(*get_uniforms(material.second));
					material.second = std::move(new_material);
				}

				dirty_ = false;
			}

			texture_loaders_.clear();
			Uniforms const *uniforms = get_uniforms(material->second);

			QRectF color_map = uniforms->color_map_->normalizedTextureSubRect();
			QVector2D vertices_in_color_map[] {QVector2D(color_map.topLeft()), QVector2D(color_map.bottomLeft()), QVector2D(color_map.topRight()), QVector2D(color_map.bottomRight())};

			QRectF normal_map = uniforms->normal_map_ ? uniforms->normal_map_->normalizedTextureSubRect() : QRectF();
			QVector2D vertices_in_normal_map[] {QVector2D(normal_map.topLeft()), QVector2D(normal_map.bottomLeft()), QVector2D(normal_map.topRight()), QVector2D(normal_map.bottomRight())};

			Mesh *mesh = pool_.acquire();
			mesh->setMaterial(material->second.get());
			appendChildNode(mesh);

			for(int i = 0; i < 4; ++i)
			{
				static const QVector3D vertices_in_object[] {{-1, -1, 0}, {-1, 1, 0}, {1, -1, 0}, {1, 1, 0}};
				QVector3D vertex_in_scene = data.transform_ * vertices_in_object[i];

				static_cast<Attributes *>(mesh->geometry_.vertexData())[i] =
				{ {
					vertex_in_scene.x(),
					vertex_in_scene.y(),
					vertex_in_scene.z(),
				},{
					uint8(data.mask_.red()),
					uint8(data.mask_.green()),
					uint8(data.mask_.blue()),
					uint8(data.mask_.alpha()),
				},{
					vertices_in_color_map[i].x(),
					vertices_in_color_map[i].y(),
				},{
					vertices_in_normal_map[i].x(),
					vertices_in_normal_map[i].y(),
				},{
					std::cos(data.angle_),
					std::sin(data.angle_),
				} };
			}
		}
	}
};

QSGNode *ImageView::updatePaintNode(QSGNode *node, UpdatePaintNodeData *)
{
	if(!node)
	{
		node = new Node(this);
	}

	Q_ASSERT(dynamic_cast<Node *>(node));
	static_cast<Node *>(node)->data_.swap(data_);
	return node;
}

void ImageView::set_model(QVariant const &model)
{
	model_ = model;
	Q_EMIT model_changed(model_);
	update();
}

void ImageView::set_zoom(float const &zoom)
{
	if(zoom_ == zoom) return;
	zoom_ = zoom;
	Q_EMIT zoom_changed(zoom_);
	Q_EMIT transform_changed(transform());
	update();
}

void ImageView::set_angle(float const &angle)
{
	if(angle_ == angle) return;
	angle_ = angle;
	Q_EMIT angle_changed(angle_);
	Q_EMIT transform_changed(transform());
	update();
}

void ImageView::set_center(b2Vec2 const &center)
{
	if(center_ == center) return;
	center_ = center;
	Q_EMIT center_changed(center_);
	Q_EMIT transform_changed(transform());
	update();
}

int ImageView::quality() const
{
	return quality_;
}

void ImageView::set_quality(int const &quality)
{
	if(quality_ == quality) return;
	quality_ = quality;
	dirty_ = true;
	quality_changed(quality_);
}

QMatrix4x4 ImageView::transform() const
{
	QMatrix4x4 m;
	m.translate(float(width()) / 2, float(height()) / 2);
	m.perspective(90, 1, -1, 1);
	m.scale(zoom(), zoom(), 1);
	m.rotate(-angle() * float(180 / M_PI), 0, 0, 1);
	m.translate(-center().x, -center().y, -1);
	return m;
}

AW_DEFINE_OBJECT_STUB(ImageView)

}
}
