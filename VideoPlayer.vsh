attribute vec2 vertex_in_scene;
attribute vec2 vertex_in_texture;
uniform mat4 qt_Matrix;
varying vec2 fragment_in_texture_;

void main()
{
	gl_Position = qt_Matrix * vec4(vertex_in_scene, 0, 1);
	fragment_in_texture_ = vertex_in_texture;
}
