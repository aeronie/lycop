attribute vec2 vertex_in_scene;
attribute vec2 vertex_in_texture;
attribute vec4 color;
uniform mat4 qt_Matrix;
uniform mat4 transform;
uniform float texture_ratio;
varying vec2 fragment_in_texture_;
varying vec4 color_;

void main()
{
	fragment_in_texture_ = vec2(vertex_in_texture.x, vertex_in_texture.y * texture_ratio);
	color_ = color;
	color_.rgb *= color.a;
	gl_Position = qt_Matrix * transform * vec4(vertex_in_scene, 0, 1);
}
