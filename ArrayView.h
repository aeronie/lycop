#pragma once

#include <vector>

namespace aw {

/*

Utilise un pointeur sur std::vector et deux indices plutôt que simplement deux pointeurs, parce qu'on veut que les ArrayView restent valides quand le std::vector sous-jacent redimensionne sa mémoire.

*/
template<class T>
struct ArrayView
{
	ArrayView() = default;
	ArrayView(std::vector<T> const *array, std::size_t begin, std::size_t end): array_(array), begin_(begin), end_(end) {}
	T const *begin() const { return array_->data() + begin_; }
	T const *end() const { return array_->data() + end_; }
private:
	std::vector<T> const *array_ = 0;
	std::size_t begin_ = 0;
	std::size_t end_ = 0;
};

}
