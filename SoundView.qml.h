#include <QtQml>
#include <cmath>

#include "game/Sound.h"
#include "ArrayView.h"
#include "CheatPtr.h"
#include "Profiler.h"
#include "meta.private.h"
#include "libs/alc.h"
#include "libs/al.h"
#include "libs/ov.h"
#include "libs/sdl.h"

Q_DECLARE_METATYPE(aw::ArrayView<aw::game::Sound *>)
static const aw::register_type registered_QList = qRegisterMetaType<aw::ArrayView<aw::game::Sound *>>();

namespace aw {
namespace {

/*

On peut jouer un son en boucle après une intro en codant un « loop point » dans le nom du fichier : « path/name.15000.ogg ».
La position du loop point est donnée en nombre d'échantillons.

*/
struct Buffer
{
	ALuint id_;
	Buffer(Buffer const &) = delete;

	struct Deleter
	{
		void operator()(SDL_RWops *p) const { AW_CHECK_SDL(SDL_RWclose(p)); }
		void operator()(Uint8 *p) const { SDL_FreeWAV(p); }
	};

	static QFileInfo glob(QString const &name, std::initializer_list<QString> const &formats)
	{
		for(QString const &format: formats)
		{
			QFileInfo query = format.arg(name);
			QFileInfoList result = query.dir().entryInfoList(QStringList(query.fileName()));
			if(!result.isEmpty()) return result.first();
		}

		Q_UNREACHABLE();
	}

	void load(QString const &name, void const *bytes, size_t byte_count, size_t format, size_t rate)
	{
		Q_ASSERT(byte_count <= size_t(INT32_MAX));
		Q_ASSERT(rate <= size_t(INT32_MAX));
		AW_CHECK_AL(alGenBuffers(1, &id_));
		AW_CHECK_AL(alBufferData(id_, format == 1 ? AL_FORMAT_MONO8 : AL_FORMAT_MONO16, bytes, int32_t(byte_count), int32_t(rate)));

		QStringList strings = name.split('.');
		if(strings.count() > 1)
		{
			Q_ASSERT(strings.count() < 3);
			int offsets[2] {0, int(byte_count / format)};
			for(int i = 1; i < strings.count(); ++i)
			{
				bool ok;
				offsets[i - 1] = strings[i].toInt(&ok);
				Q_ASSERT(ok);
			}
			AW_CHECK_AL(alBufferiv(id_, AL_LOOP_POINTS_SOFT, offsets));
		}
	}

	Buffer(QString const &name)
	{
		QFileInfo file_info = glob(name, {":/data/game/%1.*", "%1.*"});
		QFile file(file_info.filePath());
		if(!file.open(QFile::ReadOnly)) Q_UNREACHABLE();
		AW_TIME("SoundView", time, file_info.filePath())

		if(file_info.suffix() == "ogg")
		{
			OggVorbis_File ov;
			ov_callbacks callbacks;
			callbacks.read_func = [] (void *buffer, size_t size, size_t count, void *that) { return size_t(reinterpret_cast<QFile *>(that)->read(reinterpret_cast<char *>(buffer), size * count)); };
			callbacks.seek_func = 0;
			callbacks.close_func = 0;
			callbacks.tell_func = 0;
			AW_CHECK_OV(ov_open_callbacks(&file, &ov, 0, 0, callbacks));
			vorbis_info *info = ov_info(&ov, -1);
			Q_ASSERT(info);
			Q_ASSERT(info->channels == 1);
			std::vector<char> buffer;
			int count = 0;
			do
			{
				size_t size = buffer.size();
				count = 4096;
				int stream_id;
				buffer.resize(size + count);
				count = int(AW_CHECK_OV(ov_read(&ov, &buffer[size], count, 0, 2, 1, &stream_id)));
				buffer.resize(size + count);
			}
			while(count > 0);
			load(file_info.completeBaseName(), buffer.data(), buffer.size(), 2, info->rate);
			AW_CHECK_OV(ov_clear(&ov));
			return;
		}
		if(file_info.suffix() == "wav")
		{
			QByteArray data = file.readAll();
			SDL_AudioSpec wav;
			Uint32 size = 0;
			std::unique_ptr<SDL_RWops, Deleter> rw {AW_CHECK_SDL_PTR(SDL_RWFromConstMem(data.data(), data.count()))};
			std::unique_ptr<Uint8, Deleter> buffer;
			AW_CHECK_SDL_PTR(SDL_LoadWAV_RW(rw.get(), 0, &wav, cheat(&buffer), &size));
			Q_ASSERT(wav.channels == 1);
			Q_ASSERT(wav.format == AUDIO_U8 || wav.format == AUDIO_S16SYS);
			load(file_info.completeBaseName(), buffer.get(), size, wav.format == AUDIO_U8 ? 1 : 2, wav.freq);
			return;
		}

		Q_UNREACHABLE();
	}

	~Buffer()
	{
		AW_CHECK_AL(alDeleteBuffers(1, &id_));
	}
};

struct Source
{
	ALuint id_;
	std::shared_ptr<Buffer> buffer_;
	bool expired_ = true;
	bool looping_ = false;
	Source(Source const &) = delete;

	Source(std::shared_ptr<Buffer> const &buffer, bool looping)
	: buffer_(buffer)
	, looping_(looping)
	{
		AW_CHECK_AL(alGenSources(1, &id_));
		AW_CHECK_AL(alSourcei(id_, AL_BUFFER, buffer_->id_));
		AW_CHECK_AL(alSourcei(id_, AL_SOURCE_RELATIVE, true));
		if(looping_)
		{
			AW_CHECK_AL(alSourcei(id_, AL_LOOPING, true));
			play();
		}
	}

	~Source()
	{
		AW_CHECK_AL(alDeleteSources(1, &id_));
	}

	void play() const
	{
		AW_CHECK_AL(alSourcePlay(id_));
	}

	bool playing() const
	{
		int value;
		AW_CHECK_AL(alGetSourcei(id_, AL_SOURCE_STATE, &value));
		return value == AL_PLAYING;
	}
};

struct Sound
{
	game::Sound const *data;
	QVector3D position;
	float distance;

	bool operator<(Sound const &other) const
	{
		return distance < other.distance;
	}
};

struct SoundView
: QObject
{
	AW_DECLARE_OBJECT_STUB(SoundView)
	AW_DECLARE_PROPERTY_STORED(QVariant, model);
	AW_DECLARE_PROPERTY_STORED(b2Vec2, center) = b2Vec2_zero; ///< m
	AW_DECLARE_PROPERTY_STORED(float, gain) = 1; // volume sonore perçu (psycho-acoustique)
	typedef std::map<QString, std::shared_ptr<Buffer>> BufferList;
	typedef std::map<game::Sound const *, std::unique_ptr<Source>> SourceList;
	BufferList buffers_;
	SourceList sources_;
	std::vector<SourceList::key_type> expired_;
	std::vector<Sound> sounds_;
	float real_gain_ = 1;

	BufferList::const_iterator get(QString const &name)
	{
		BufferList::const_iterator buffer = buffers_.find(name);

		if(buffer == buffers_.end())
		{
			buffer = buffers_.insert(BufferList::value_type(name, std::make_shared<Buffer>(name))).first;
		}

		return buffer;
	}

	void update(SourceList::iterator const &source, Sound const &sound) const
	{
		AW_CHECK_AL(alSource3f(source->second->id_, AL_POSITION, sound.position.x(), sound.position.y(), 0));
		float gain = std::max(0.f, std::min(1.f, 1 - sound.distance));
		AW_CHECK_AL(alSourcef(source->second->id_, AL_GAIN, gain * gain * real_gain_));

		if(!source->second->looping_ && sound.data->playing())
		{
			source->second->play();
		}
	}

protected:

	~SoundView()
	{
		UseAL use_al;
		sources_.clear();
		buffers_.clear();
	}

	Q_SLOT void preload(QString const &name)
	{
		{
			UseAL use_al;
			get(name);
		}
		if(!QMetaObject::invokeMethod(this, "update")) Q_UNREACHABLE();
	}

	Q_SLOT void unload()
	{
		UseAL use_al;

		for(auto p = buffers_.begin(); p != buffers_.end();)
		{
			if(!p->first.startsWith("e"))
			{
				buffers_.erase(p++);
			}
			else
			{
				++p;
			}
		}
	}

	Q_SLOT void update()
	{
		UseAL use_al {std::try_to_lock};
		if(!use_al) return;
		sounds_.clear();
		expired_.clear();

		if(!model().isNull())
		{
			Q_ASSERT(model_.canConvert<ArrayView<game::Sound *>>());
			float max_distance = 10;
			static const size_t max_count = 255;

			// model_ contient tous les sons de la scène
			// on évacue ceux qui ont été supprimés, ceux qui sont silencieux et ceux qui sont trop loin
			for(auto &sound: model_.value<ArrayView<game::Sound *>>())
			{
				if(sound && sound->scale())
				{
					QVector3D position = sound->transform().map(QVector3D()) - QVector3D(center_.x, center_.y, 0);
					float distance = position.length() - sound->transform().mapVector(QVector3D(1, 0, 0)).length(); // distance au centre − rayon = distance à la limite d'audibilité du son
					if(distance < max_distance) sounds_.push_back({sound, position, distance});
				}
			}
			// sounds_ contient tous les sons qu'on peut entendre
			// on évacue les plus éloignés pour qu'il n'en reste que max_count
			if(sounds_.size() > max_count)
			{
				auto end = sounds_.begin() + max_count;
				nth_element(sounds_.begin(), end, sounds_.end());
				max_distance = end->distance;
				sounds_.erase(end, sounds_.end());
			}
			// sounds_ contient au maximum max_count sons qu'on doit entendre
			// pour les sons déjà associés à une source, on passe expired_ à false et on les évacue de la liste
			// pour les sons qui n'ont pas encore de source, on les garde pour plus tard
			auto function = [&] (Sound &sound)
			{
				sound.distance /= max_distance;
				SourceList::iterator source = sources_.find(sound.data);
				if(source == sources_.end()) return false; // on ajoutera une source plus tard, après avoir retiré les anciennes
				update(source, sound);
				source->second->expired_ = false;
				return true;
			};
			sounds_.erase(remove_if(sounds_.begin(), sounds_.end(), function), sounds_.end());
		}
		// sounds_ contient les sons pour lesquels il faut créer une nouvelle source
		// sources_ contient les sources active à l'itération précédente
		// seules les sources marquées comme pas expired_ ci-dessus ne sont pas expired_ ; il y en a au maximum (max_count - sounds_.size())
		// on supprime les sources expired_
		// on marque toutes les sources comme expired_ pour la prochaine itération
		for(SourceList::reference pair: sources_)
		{
			if(pair.second->expired_)
			{
				expired_.push_back(pair.first);
			}

			pair.second->expired_ = true;
		}
		for(SourceList::key_type key: expired_)
		{
			sources_.erase(key);
		}
		// sources_ contient les sources encore actives à l'itération courante ; il y en a au maximum (max_count - sounds_.size())
		// toutes les sources sont expired_
		// on crée des nouvelles sources pour les sons qui n'en ont pas encore ; il y en a (sounds_.size())
		for(Sound const &sound: sounds_)
		{
			update(sources_.insert(SourceList::value_type(sound.data, SourceList::mapped_type(new Source(get(sound.data->objectName())->second, sound.data->looping())))).first, sound);
		}
		if(!model().isNull())
		{
			for(auto &sound: model_.value<ArrayView<game::Sound *>>())
			{
				if(sound)
				{
					sound->set_playing(false);
				}
			}
		}
	}
};

void SoundView::set_model(QVariant const &model)
{
	model_ = model;
	Q_EMIT model_changed(model_);
	update();
}

void SoundView::set_center(b2Vec2 const &center)
{
	if(center_ == center) return;
	center_ = center;
	Q_EMIT center_changed(center_);
	update();
}

void SoundView::set_gain(float const &gain)
{
	if(gain_ == gain) return;
	gain_ = gain;
	real_gain_ = float(std::pow(10, gain_) - 1) / (10 - 1); // fonction exponentielle pour compenser la perception logarithmique. La constante 10 a été trouvée empiriquement.
	Q_EMIT gain_changed(gain_);
	update();
}

AW_DEFINE_OBJECT_STUB(SoundView)

}
}
