#!/bin/sh
set -e
cd "$0/.."
export PATH="$PWD:$PATH"
export LD_LIBRARY_PATH="$PWD:$LD_LIBRARY_PATH"
export QML2_IMPORT_PATH="$PWD:$QML2_IMPORT_PATH"
cwd=$(pwd | sed 's,["`$\\],\\\0,g;s,\\,\\\\,g')
desktop_entry=aeronie-lycop.desktop

[ "$*" ] || cat <<eof

Usage:

$0 install -- Add a launcher in the menu.
$0 remove  -- Remove the launcher.
$0 start   -- Start the game.

eof

install() {
cat >"$desktop_entry" <<eof
[Desktop Entry]
Type=Application
Name=The adventures of captain Lycop: Invasion of the Heters
Name[fr]=Les aventures du capitaine Lycop : l’invasion des Heters
Icon=$PWD/icon.png
Exec=sh "$cwd/run.sh" start
Categories=Game;ActionGame
eof
chmod +x lycop run.sh "$desktop_entry"
xdg-desktop-menu install "$desktop_entry"
}

remove() {
xdg-desktop-menu uninstall "$desktop_entry"
}

start() {
exec lycop >log.txt 2>&1
}

check_libs() {
ldd lycop
find -name '*.so' -exec ldd {} +
}

"$@"
