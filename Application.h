#pragma once

#include <QGuiApplication>

namespace aw {

struct Application
: QGuiApplication
{
	Q_OBJECT
public:
	Application(int *, char **);
};

Application *application();

}
