#include "ControlInfo.h"
#include "meta.private.h"

namespace aw {
namespace {

struct Info
: QObject
{
	Q_OBJECT
public:
	static QList<ControlInfo> const &info()
	{
		static const QList<ControlInfo> info
		{
			{tr("plus"), "plus", 1},
			{tr("minus"), "minus", 1},
			{tr("plus"), "plus", 0},
			{tr("minus"), "minus", 0},
		};
		return info;
	}
};

struct MagnitudeControl
: QObject
{
	AW_DECLARE_OBJECT_STUB(MagnitudeControl)
	AW_DECLARE_PROPERTY_STORED(QString, text);
	AW_DECLARE_PROPERTY_READONLY(qreal, value)
	qreal plus_ = 0, minus_ = 0;

public Q_SLOTS:

	QObject *info() const
	{
		return ControlInfo::model(Info::info(), objectName());
	}

	void plus(qreal z)
	{
		plus_ = z;
		minus_ = 0;
		Q_EMIT value_changed(value());
	}

	void minus(qreal z)
	{
		plus(-z);
	}

	void plus(bool z)
	{
		plus_ = z;
		Q_EMIT value_changed(value());
	}

	void minus(bool z)
	{
		minus_ = z;
		Q_EMIT value_changed(value());
	}
};

qreal MagnitudeControl::value() const
{
	return plus_ - minus_;
}

AW_DEFINE_OBJECT_STUB(MagnitudeControl)
AW_DEFINE_PROPERTY_STORED(MagnitudeControl, text)

}
}
