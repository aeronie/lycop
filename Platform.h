#pragma once

#include <QQmlEngine>

namespace aw {

void load_platform(QQmlEngine *);

}
