#include <QtCore>

#include "Application.h"

namespace aw {

Application::Application(int *ac, char **av)
: QGuiApplication(*ac, av)
{
	for(QFileInfo file: QDir(applicationDirPath()).entryInfoList({"*.rcc"}))
	{
		if(!QResource::registerResource(file.absoluteFilePath(), "/data")) qWarning() << "failed to load" << file.absoluteFilePath();
	}
}

Application *application()
{
	Q_ASSERT(dynamic_cast<Application *>(Application::instance()));
	return static_cast<Application *>(Application::instance());
}

}
