#include "meta.private.h"

namespace aw {
namespace {

struct QueuedBinding
: QObject
{
	AW_DECLARE_OBJECT_STUB(QueuedBinding)
	AW_DECLARE_PROPERTY_READONLY(QVariant, queued)
	AW_DECLARE_PROPERTY_STORED(QVariant, direct);
};

QVariant QueuedBinding::queued() const
{
	return direct_;
}

void QueuedBinding::set_direct(QVariant const &value)
{
	if(direct_ == value) return;
	direct_ = value;
	Q_EMIT direct_changed(direct_);
	QMetaObject::invokeMethod(this, "queued_changed", Qt::QueuedConnection, Q_ARG(QVariant, direct_));
}

AW_DEFINE_OBJECT_STUB(QueuedBinding)

}
}
