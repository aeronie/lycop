#pragma once

#include <QQmlIncubationController>

namespace aw {

struct IncubationController
: QObject
, QQmlIncubationController
{
	IncubationController()
	{
		startTimer(20);
	}

	void timerEvent(QTimerEvent *) override
	{
		bool flag = true;
		incubateWhile(&flag);
	}
};

}
