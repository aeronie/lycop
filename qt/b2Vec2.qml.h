#pragma once

#include <QtGui>
#include <Box2D.h>

#include "ValueType.h"
#include "meta.h"

Q_DECLARE_METATYPE(b2Vec2);
Q_DECLARE_TYPEINFO(b2Vec2, Q_PRIMITIVE_TYPE);

namespace aw {
namespace {

struct qml_b2Vec2
: QQmlValueTypeBase<b2Vec2>
{
	Q_OBJECT
	Q_PROPERTY(float x READ x WRITE x FINAL)
	Q_PROPERTY(float y READ y WRITE y FINAL)

	QString toString() const override
	{
		return QString("b2Vec2(x=%1, y=%2)").arg(v.x).arg(v.y);
	}

	float x() const
	{
		return v.x;
	}

	float y() const
	{
		return v.y;
	}

	void x(float x)
	{
		v.x = x;
	}

	void y(float y)
	{
		v.y = y;
	}

public:

	qml_b2Vec2()
	: QQmlValueTypeBase<b2Vec2>(qMetaTypeId<b2Vec2>(), 0)
	{
	}

	Q_SLOT QPointF apply(const QMatrix4x4 &m) const
	{
		return m * QPointF(v.x, v.y);
	}

	Q_SLOT float norm() const
	{
		return v.Length();
	}
};

static b2Vec2 convert_QPointF_b2Vec2(QPointF v)
{
	return b2Vec2(float(v.x()), float(v.y()));
}

static QPointF convert_b2Vec2_QPointF(b2Vec2 v)
{
	return QPointF(v.x, v.y);
}

static QVariant interpolate(const b2Vec2 &from, const b2Vec2 &to, qreal progress)
{
	return QVariant::fromValue(float(1 - progress) * from + float(progress) * to);
}

static void delete_b2Vec2(void *v)
{
	delete static_cast<b2Vec2 *>(v);
}

static void *new_b2Vec2(void const *other)
{
	return other ? new b2Vec2(*static_cast<b2Vec2 const *>(other)) : new b2Vec2(0, 0);
}

static void finalize_b2Vec2(void *v)
{
	static_cast<b2Vec2 *>(v)->~b2Vec2();
}

static void *initialize_b2Vec2(void *v, void const *other)
{
	return other ? new(v) b2Vec2(*static_cast<b2Vec2 const *>(other)) : new(v) b2Vec2(0, 0);
}

static const register_type plugin_ = []
{
	register_value_type(QMetaType::registerType("b2Vec2", delete_b2Vec2, new_b2Vec2, finalize_b2Vec2, initialize_b2Vec2, sizeof(b2Vec2), QMetaType::MovableType, 0), {ValueType::create<qml_b2Vec2>});
	QMetaType::registerConverter<b2Vec2, QPointF>(convert_b2Vec2_QPointF);
	QMetaType::registerConverter<QPointF, b2Vec2>(convert_QPointF_b2Vec2);
	qRegisterAnimationInterpolator(interpolate);
	return true;
}();

}
}
