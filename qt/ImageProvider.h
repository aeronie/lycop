#pragma once

#include <QQmlEngine>

#include "meta.h"

#define AW_REGISTER_IMAGE_PROVIDER(_class) \
	static void plugin(QQmlEngine *engine) { engine->addImageProvider(#_class, new _class); } \
	static const register_type registered_plugin_ = register_image_provider(plugin); \

namespace aw {

int register_image_provider(void (*)(QQmlEngine *));
void load_image_providers(QQmlEngine *);

}
