#include "ImageProvider.h"

namespace aw {
namespace {

typedef void Plugin(QQmlEngine *);

static QList<Plugin *> &plugins()
{
	static QList<Plugin *> x;
	return x;
}

}

int register_image_provider(Plugin *plugin)
{
	plugins().append(plugin);
	return 0;
}

void load_image_providers(QQmlEngine *engine)
{
	for(Plugin *plugin: plugins())
	{
		plugin(engine);
	}
}

}
