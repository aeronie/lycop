#include <QtGui>

#include <private/qquickmousearea_p.h>

#include "meta.private.h"

namespace aw {
namespace {

struct MouseArea
: QQuickMouseArea
{
	AW_DECLARE_OBJECT_STUB(MouseArea)
	AW_DECLARE_PROPERTY_STORED(QString, cursor);

protected:

	MouseArea(QQuickItem *parent = 0)
	: QQuickMouseArea(parent)
	{
		unsetCursor(); // annule setCursor(Qt::ArrowCursor) dans QQuickMouseArea
	}
};

void MouseArea::set_cursor(QString const &name)
{
	if(name == cursor_) return;
	cursor_ = name;
	cursor_changed(cursor_);

	if(name.isEmpty())
	{
		unsetCursor();
	}
	else
	{
		QFileInfoList result = QDir(":/controls/cursors").entryInfoList(QStringList(name + ".*"));
		Q_ASSERT(!result.isEmpty());
		QStringList strings = result[0].completeSuffix().split('.');
		Q_ASSERT(strings.count() == 3);
		bool ok;
		int x = strings[0].toInt(&ok);
		Q_ASSERT(ok);
		int y = strings[1].toInt(&ok);
		Q_ASSERT(ok);
		setCursor(QCursor(QPixmap(result[0].absoluteFilePath()), x, y));
	}
}

AW_DEFINE_OBJECT_STUB(MouseArea)

}
}
