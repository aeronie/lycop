#pragma once

#include <private/qqmlvaluetype_p.h>

namespace aw {

struct ValueType
{
	#define _(_function, _args) \
	typedef bool (*_##_function) _args; \
	_##_function _function##_ = 0; \
	template<class _type> \
	static bool _function _args

	_(create, (QQmlValueType *&v))
	{
		v = new _type;
		return true;
	}

	_(init, (void *data, size_t))
	{
		new (reinterpret_cast<_type *>(data)) _type;
		return true;
	}

	_(destroy, (void *data, size_t))
	{
		reinterpret_cast<_type *>(data)->~_type();
		return true;
	}

	_(copy, (const void *other, void *data, size_t))
	{
		new (reinterpret_cast<_type *>(data)) _type(*reinterpret_cast<const _type *>(other));
		return true;
	}

	#undef _

	ValueType(_create create)
	: create_(create)
	{
	}
};

void register_value_type(int, const ValueType &);

}
