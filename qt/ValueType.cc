#include <private/qqmlglobal_p.h>

#include "ValueType.h"

namespace aw {
namespace {

struct ValueTypeProvider
: QQmlValueTypeProvider
, std::map<int, ValueType>
{
	ValueTypeProvider()
	{
		QQml_addValueTypeProvider(this);
	}

	#define _(_function, _args_1, _args_2) \
	bool _function _args_1 override \
	{ \
		auto p = find(type); \
		return p == end() ? false : (p->second._function##_args_2); \
	}

	_(create, (int type, QQmlValueType *&v), _(v))
	_(init, (int type, void *data, size_t size), _(data, size))
	_(destroy, (int type, void *data, size_t size), _(data, size))
	_(copy, (int type, const void *other, void *data, size_t size), _(other, data, size))

	#undef _
};

}

void register_value_type(int type, const ValueType &data)
{
	static ValueTypeProvider instance;
	instance.emplace(type, data);
}

}
