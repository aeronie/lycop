#include <QtQml>

#include "Application.h"
#include "Platform.h"
#include "qt/ImageProvider.h"
#include "qt/IncubationController.h"

int main(int ac, char **av)
{
	aw::Application app(&ac, av);
	QQmlApplicationEngine qml;
	aw::load_image_providers(&qml);
	aw::load_platform(&qml);
	aw::IncubationController incubation_controller;
	qml.setIncubationController(&incubation_controller);
	qml.load(QUrl("qrc:///main.qml"));
	return app.exec();
}
