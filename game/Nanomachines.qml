import aw.game 0.0

Nanomachines
{
	property int level: 1
	health: max_health
	max_health: 200 +~~body.health_boost
	regeneration: [0, 100, 200][level]
	image: Image
	{
		material: "equipments/nanomachines"
		scale: 0.5
		mask: mask_equipment(parent)
	}
}
