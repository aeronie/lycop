#include "Body.h"
#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct Bullet
: Body
, Observer<1> // timer
{
	AW_DECLARE_OBJECT_STUB(Bullet)
	AW_DECLARE_PROPERTY_STORED(int, range) = 0;
	AW_DECLARE_PROPERTY_STORED(float, damages) = 0;

	void update(const Observer<-1>::Tag &tag) override
	{
		Body::update(tag);

		if(body_)
		{
			Observer<1>::connect(scene()->signal_frame_begin());
		}
	}

	void update(const Observer<1>::Tag &) override
	{
		if(range_ && damages_ > 0)
		{
			set_range(range()-1);
		}
		else
		{
			deleteLater();
		}
	}
};

AW_DEFINE_OBJECT_STUB(Bullet)
AW_DEFINE_PROPERTY_STORED(Bullet, range)
AW_DEFINE_PROPERTY_STORED(Bullet, damages)

}
}
}
