import QtQuick 2.0 as Q
import aw.game 0.0

ActiveEquipment
{
	id: maser_emitter
	property int level: 1
	property int bullet_group: groups.enemy_dot
	property real power: 1
	health: max_health
	max_health: 200 +~~body.health_boost
	image: Image
	{
		material: "equipments/maser-emitter"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	Sound
	{
		objectName: "equipments/maser-emitter/shooting"
		scale: shooting ? slot.sound_scale : 0
		looping: true
	}
	Q.Component
	{
		id: spray_factory
		Body
		{
			id: spray
			readonly property real damages: [0,200,230][maser_emitter.level] * scene.time_step
			scene: maser_emitter.body.scene
			scale: 3 * maser_emitter.power
			Image
			{
				material: "equipments/maser-emitter/" + (scene.time % 4)
				mask: maser_emitter.shooting ? Qt.rgba(1, 1, 1, 0.6) : "transparent"
				position.y: 70 / 512 * scale - scale
				z: altitudes.bullet - 0.000001
				scale: 1.5
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0,0),Qt.point(-0.2,-0.1),Qt.point(-0.35,-0.3),Qt.point(-0.35,-2.7),Qt.point(0.35,-2.7),Qt.point(0.35,-0.3),Qt.point(0.2,-0.1)]
				group: maser_emitter.shooting ? bullet_group : 0
				sensor: true
				density: 1e-10
			}
		}
	}
	WeldJoint
	{
		id: joint
		body_1: maser_emitter.body
		Q.Component.onDestruction: body_2.destroy()
		Q.Component.onCompleted:
		{
			body_2 = spray_factory.createObject(null, {position: slot.point_to_scene(Qt.point(0, -1.4)), angle: slot.angle_to_scene(0)})
			slot.angle_changed(slot.angle)
		}
	}
	Q.Connections
	{
		target: slot
		onAngleChanged:
		{
			joint.position = slot.point_to_body(Qt.point(0, -1.4))
			joint.angle = slot.angle_to_body(0)
			joint.initialize()
		}
	}
}
