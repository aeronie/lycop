import aw.game 0.0

EquipmentSlot
{
	readonly property real sound_scale: 50
	Image
	{
		material: "equipments/shield"
		mask: Qt.rgba(0, 0.4, 1, equipment && equipment.body && (equipment.body.shield < 1) && equipment.body.max_secondary_shield ? equipment.shield / equipment.body.max_secondary_shield : 0)
	}
}
