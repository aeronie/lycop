#pragma once

#include "Consumable.h"
#include "Equipment.h"

namespace aw {
namespace game {

struct ActiveEquipment
: Equipment
, Observer<0> // ready
, Observer<1> // slot
, Observer<2> // body
, Observer<3> // scene
, Observer<4> // timer
{
	AW_DECLARE_OBJECT_STUB(ActiveEquipment)
	AW_DECLARE_PROPERTY_STORED(aw::game::Consumable *, consumable) = 0;
	AW_DECLARE_PROPERTY_STORED(bool, shooting) = false;
protected:
	void update(Observer<0>::Tag const &) override;
	void update(Observer<1>::Tag const &) override;
	void update(Observer<2>::Tag const &) override;
	void update(Observer<3>::Tag const &) override;
	void update(Observer<4>::Tag const &) override;
	ActiveEquipment();
	~ActiveEquipment() = default;
	Q_SIGNAL virtual void activate();
};

}
}
