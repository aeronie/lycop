#include <QtCore>

#include "Object.h"

namespace aw {
namespace game {
namespace {

static int qml_children_count(QQmlListProperty<QObject> *list)
{
	return list->object->children().count();
}

static QObject *qml_children_at(QQmlListProperty<QObject> *list, int index)
{
	return list->object->children().at(index);
}

static void qml_children_append(QQmlListProperty<QObject> *list, QObject *object)
{
	Q_ASSERT(list);
	Q_ASSERT(dynamic_cast<Object *>(list->object));
	Q_ASSERT(object);
	object->setParent(0);
	object->setParent(list->object);
}

}

void Object::classBegin()
{
}

void Object::componentComplete()
{
	qml_initialized_ = true;
	ready_.notify();
}

void Object::childEvent(QChildEvent *event)
{
	QObject::childEvent(event);

	if(event->added())
	{
		if(Object *child = dynamic_cast<Object *>(event->child()))
		{
			child->parent_changed_.notify();
			child->parent_changed();
		}
	}

	Q_EMIT children_changed();
}

QQmlListProperty<QObject> Object::qml_children()
{
	return QQmlListProperty<QObject>(this, 0, qml_children_append, qml_children_count, qml_children_at, 0);
}

}
}
