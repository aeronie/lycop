#include <Box2D.h>

#include "Body.h"
#include "Joint.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct RopeJoint
: Joint
{
	AW_DECLARE_OBJECT_STUB(RopeJoint)
	AW_DECLARE_PROPERTY_STORED(b2Vec2, position_1) = b2Vec2_zero;
	AW_DECLARE_PROPERTY_STORED(b2Vec2, position_2) = b2Vec2_zero;
	AW_DECLARE_PROPERTY_STORED(float, length) = -1;

	void initialize() override
	{
		b2RopeJointDef def;
		def.localAnchorA = point_to_body_1(position_1());
		def.localAnchorB = point_to_body_1(position_2());
		def.maxLength = length() < 0 ? ( body_1()->body_->GetWorldPoint(def.localAnchorA) - body_2()->body_->GetWorldPoint(def.localAnchorB) ).Length() : length();
		Joint::initialize(&def);
	}
};

AW_DEFINE_OBJECT_STUB(RopeJoint)
AW_DEFINE_PROPERTY_STORED(RopeJoint, position_1)
AW_DEFINE_PROPERTY_STORED(RopeJoint, position_2)
AW_DEFINE_PROPERTY_STORED(RopeJoint, length)

}
}
}
