#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {

struct Animation
: Object
, Observer<0> // parent = scene
, Observer<1> // timer
{
	AW_DECLARE_OBJECT_STUB(Animation)
	AW_DECLARE_PROPERTY_STORED(float, speed) = 0;
	AW_DECLARE_PROPERTY_STORED(float, time) = 0;

	void update(const Observer<0>::Tag &) override
	{
		Observer<1>::connect(scene() && speed() ? scene()->signal_frame_begin() : 0);
	}

	void update(const Observer<1>::Tag &) override
	{
		set_time(time() + speed() * scene()->time_step());
	}

	Scene *scene() const
	{
		Q_ASSERT(!parent() || dynamic_cast<Scene *>(parent()));
		return static_cast<Scene *>(parent());
	}

protected:

	Animation()
	{
		Observer<0>::connect(signal_parent_changed());
	}
};

void Animation::set_speed(float const &speed)
{
	if(speed_ == speed) return;
	speed_ = speed;
	speed_changed(speed_);
	update(Observer<0>::Tag());
}

AW_DEFINE_OBJECT_STUB(Animation)
AW_DEFINE_PROPERTY_STORED(Animation, time)

}
}
