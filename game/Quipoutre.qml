import QtQuick 2.0 as Q
import aw.game 0.0

Weapon
{
	property int level: 1
	property int bullet_group: groups.enemy_bullet
	property int bullet_group_aoe: groups.enemy_dot_qui_poutre
	property real bullet_velocity: 5 //utile pour cheater des enemy
	period: scene.seconds_to_frames([0 , 7 , 4.55] [level])
	range_bullet: scene.seconds_to_frames(6)
	health: max_health
	max_health: 400 +~~body.health_boost
	image: Image
	{
		material: "equipments/qui-poutre"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/qui-poutre/shooting"
		scale: slot.sound_scale
	}
	bullet_factory: Bullet
	{
		readonly property int max_range: weapon.range_bullet
		scale: 3 * Math.min(1, (max_range - range) / scene.seconds_to_frames(0.5))
		onScaleChanged: collider.update_transform(), collider_aoe.update_transform()
		range: max_range
		damages: 3000
		property real damages_aoe: 300* scene.time_step
		angular_velocity: 1
		Image
		{
			material: "equipments/qui-poutre/" + (Math.floor(scene.time / 5) % 5)
			mask: Qt.rgba(1, 1, 1, Math.min(1, range / scene.seconds_to_frames(0.1)))
			z: altitudes.bullet
		}
		CircleCollider
		{
			id: collider
			group: weapon.bullet_group
			radius: 0.4
			sensor: true
		}
		CircleCollider
		{
			id: collider_aoe
			group: weapon.bullet_group_aoe
			sensor: true
		}
		Q.Component.onCompleted:
		{
			position = weapon.slot.point_to_scene(Qt.point(0, -1.4))
			angle = weapon.slot.angle_to_scene(0)
			weapon.sound_shooting.play()
			weapon.set_bullet_velocity(this, weapon.bullet_velocity)
		}
	}
}
