import QtQuick 2.0
import aw.game 0.0

Weapon
{
	property int level: 1
	property int enemy: 1 // 0 = joueur , 1 = enemy
	property int type: next_type()
	function next_type() {return random_index(factories.length)}
	function next_angle() {return slot.angle_to_scene((Math.random() - .5) * Math.PI / 4)}
	range_bullet: scene.seconds_to_frames(2)
	period: scene.seconds_to_frames([0 , 1 / 5 , 1 / 7] [level])
	health: max_health
	max_health: 200 +~~body.health_boost
	bullet_factory: factories[type]

	function update(bullet)
	{
		bullet.position = slot.point_to_scene(Qt.point(0, -1.4))
		bullet.angle = next_angle()
		sounds[type].play()
		set_bullet_velocity(bullet, 20)
		type = next_type()
	}
	image: Image
	{
		material: "equipments/random-spray"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	property list<Component> factories: [
	Component
	{
		Bullet
		{
			Component.onCompleted: weapon.update(this)
			scale: 0.15
			range: weapon.range_bullet
			damages: 50
			Image
			{
				material: "equipments/gun/bullet"
				z: altitudes.bullet
			}
			CircleCollider
			{
				group: [groups.bullet,groups.enemy_bullet][weapon.enemy]
				sensor: true
			}
		}
	},
	Component
	{
		Bullet
		{
			Component.onCompleted: weapon.update(this)
			scale: 0.15
			range: weapon.range_bullet
			damages: 100
			Image
			{
				material: "equipments/coil-gun/bullet"
				z: altitudes.bullet
			}
			CircleCollider
			{
				group: [groups.bullet,groups.enemy_bullet][weapon.enemy]
				sensor: true
			}
		}
	},
	Component
	{
		Bullet
		{
			Component.onCompleted: weapon.update(this)
			scale: 0.15
			range: weapon.range_bullet
			damages: 200
			Image
			{
				material: "equipments/electric-gun/bullet"
				z: altitudes.bullet
			}
			CircleCollider
			{
				group: [groups.bullet_only_shield,groups.enemy_bullet_only_shield][weapon.enemy]
				sensor: true
			}
		}
	},
	Component
	{
		Bullet
		{
			Component.onCompleted: weapon.update(this)
			scale: 0.15
			range: weapon.range_bullet
			damages: 1
			Image
			{
				material: "equipments/acid-gun/bullet"
				z: altitudes.bullet
			}
			CircleCollider
			{
				group: [groups.bullet_acid,groups.enemy_bullet_acid][weapon.enemy]
				sensor: true
			}
		}
	}]
	property list<Sound> sounds: [
	Sound
	{
		objectName:"equipments/gun/shooting"
		scale: slot.sound_scale
	},
	Sound
	{
		objectName: "equipments/coil-gun/shooting"
		scale: slot.sound_scale
	},
	Sound
	{
		objectName: "equipments/electric-gun/shooting"
		scale: slot.sound_scale
	},
	Sound
	{
		objectName: "equipments/acid-gun/shooting"
		scale: slot.sound_scale
	}]
}
