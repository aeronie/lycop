#pragma once

#include "Behaviour.h"

namespace aw {
namespace game {

struct Behaviour_attack
: Behaviour
{
	Q_OBJECT
	AW_DECLARE_PROPERTY_STORED(QList<int>, target_groups);
	AW_DECLARE_PROPERTY_STORED(float, max_distance) = 1e33f;
	AW_DECLARE_PROPERTY_STORED_READONLY(bool, shooting) = false;
	virtual void attack(b2Vec2 const &) = 0;
	virtual void wait();
	void update(const Observer<-1>::Tag &) override final;
};

}
}
