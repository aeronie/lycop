#include "Body.h"
#include "Equipment.h"
#include "EquipmentSlot.h"
#include "meta.private.h"

namespace aw {
namespace game {

EquipmentSlot::~EquipmentSlot()
{
	if(equipment_)
	{
		equipment_->set_slot(0);
	}
}

void EquipmentSlot::set_equipment(Equipment *const &equipment)
{
	if(equipment_ == equipment) return;
	equipment_ = equipment;
	Q_EMIT equipment_changed(equipment_);

	if(equipment_)
	{
		equipment_->set_slot(this);
		equipment_->setParent(this);
	}
}

Body *EquipmentSlot::body() const
{
	Q_ASSERT(!parent() || dynamic_cast<Body *>(parent()));
	return static_cast<Body *>(parent());
}

Scene *EquipmentSlot::scene() const
{
	return body() ? body()->scene() : 0;
}

float EquipmentSlot::angle_to_body(float a) const
{
	return a + angle();
}

float EquipmentSlot::angle_to_scene(float a) const
{
	Q_ASSERT(body());
	return angle_to_body(a) + body()->angle();
}

b2Vec2 EquipmentSlot::point_to_body(b2Vec2 a) const
{
	return b2Mul(b2Transform(position(), b2Rot(angle())), scale() * a);
}

b2Vec2 EquipmentSlot::point_to_scene(b2Vec2 a) const
{
	Q_ASSERT(body());
	return b2Mul(b2Transform(body()->position(), b2Rot(body()->angle())), body()->scale() * point_to_body(a));
}

AW_DEFINE_OBJECT_STUB(EquipmentSlot)
AW_DEFINE_PROPERTY_STORED(EquipmentSlot, data)

}
}
