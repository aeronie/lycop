#include "Behaviour.h"

namespace aw {
namespace game {

Behaviour::Behaviour()
{
	Observer<0>::connect(signal_parent_changed());
}

void Behaviour::set_enabled(bool const &enabled)
{
	if(enabled_ == enabled) return;
	enabled_ = enabled;
	Q_EMIT enabled_changed(enabled_);
	Observer<1>::update();
}

void Behaviour::update(const Observer<0>::Tag &)
{
	Observer<1>::connect(body() ? body()->signal_scene_changed() : 0);
	Observer<2>::connect(body() ? body()->signal_ready() : 0);
}

void Behaviour::update(const Observer<1>::Tag &)
{
	Observer<-1>::connect(scene() && enabled() && body()->body_ ? scene()->signal_frame_begin() : 0);
}

void Behaviour::update(const Observer<2>::Tag &)
{
	update(Observer<1>::Tag());
}

}
}
