import QtQuick 2.0 as Q
import aw.game 0.0

TriggeredEquipment
{
	id: equipment
	property int level: 1
	property real radius: 1
	period: scene.seconds_to_frames([5,5,4][level])
	health: max_health
	max_health: 200 +~~body.health_boost
	image: Image
	{
		material: "equipments/teleporter"
		scale: 0.5
		mask: mask_equipment(parent)
	}
	Sound
	{
		id: sound_shooting
		objectName: "equipments/teleporter/shooting"
		scale: slot.sound_scale
	}
	onActivate:
	{
		var p = body.position
		var q = inputs.mouse_position
		if(q.x * q.x + q.y * q.y < 676)
		{
			disc_factory.createObject(null, {position: Qt.point(p.x + q.x, p.y + q.y)}).deleteLater()
		}
		sound_shooting.play()
	}
	Q.Component
	{
		id: disc_factory
		Body
		{
			property var targets: []
			function begin(target) {targets.push(target)}
			scene: body.scene
			CircleCollider
			{
				radius: equipment.radius
				group: groups.teleporter
				sensor: true
				density: 1e-10
			}
			Q.Component.onDestruction:
			{
				for(var i in targets)
				{
					if(targets[i].type != Body.DYNAMIC) return
				}
				for(var i in targets)
				{
					targets[i].position.x += body.position.x - position.x
					targets[i].position.y += body.position.y - position.y
				}
				body.position = position
			}
		}
	}
}
