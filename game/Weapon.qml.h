#include <QtQml>

#include "Body.h"
#include "TriggeredEquipment.h"
#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct Weapon
: TriggeredEquipment
{
	AW_DECLARE_OBJECT_STUB(Weapon)
	AW_DECLARE_PROPERTY_STORED(QQmlComponent *, bullet_factory) = 0;
	AW_DECLARE_PROPERTY_STORED(int, range_bullet) = 0;

	void activate() override
	{
		TriggeredEquipment::activate();

		QQmlContext *context = new QQmlContext(qmlContext(scene()));
		Q_ASSERT(context);
		Q_ASSERT(context->isValid());
		context->setContextProperty("weapon", this);

		Q_ASSERT(bullet_factory_);
		QObject *const bullet = bullet_factory_->beginCreate(context);
		Q_ASSERT(bullet);
		bullet->setParent(scene());
		bullet_factory_->completeCreate();

		context->setParent(bullet);
	}

public:

	Q_SLOT void set_bullet_velocity(Body *bullet, float bullet_speed) const
	{
		b2Vec2 axis = bullet->body_->GetWorldVector(b2Vec2(0, -1));
		float vehicle_speed = b2Dot(body()->velocity(), axis);
		float influence = std::max(0.f, vehicle_speed);
		float s = body()->velocity().Length();
		if(s > 1e-3) influence /= s;
		bullet->set_velocity((influence * std::max(bullet_speed, vehicle_speed * 1.05f) + (1 - influence) * bullet_speed) * axis);
	}
};

AW_DEFINE_OBJECT_STUB(Weapon)
AW_DEFINE_PROPERTY_STORED(Weapon, bullet_factory)
AW_DEFINE_PROPERTY_STORED(Weapon, range_bullet)

}
}
}
