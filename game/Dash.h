#include "Equipment.h"

namespace aw {
namespace game {

struct Dash
: Equipment
{
	AW_DECLARE_OBJECT_STUB(Dash)
	AW_DECLARE_PROPERTY_STORED(float, speed_amplification) = 0; ///< m s⁻¹
protected:
	Dash() = default;
	~Dash() = default;
};

}
}
