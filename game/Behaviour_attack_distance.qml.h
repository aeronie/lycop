#include "Behaviour_attack.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct Behaviour_attack_distance
: Behaviour_attack
{
	AW_DECLARE_OBJECT_STUB(Behaviour_attack_distance)
	AW_DECLARE_PROPERTY_STORED(float, target_distance) = 0;

	void attack(b2Vec2 const &target) override
	{
		b2Vec2 direction = target - body()->position();
		body()->set_target_direction(direction);
		body()->set_relative_velocity(b2Vec2(0, std::min(1.f, std::max(-1.f, ( target_distance() - direction.Length() ) / body()->max_speed()))));
	}
};

AW_DEFINE_OBJECT_STUB(Behaviour_attack_distance)
AW_DEFINE_PROPERTY_STORED(Behaviour_attack_distance, target_distance)

}
}
}
