#pragma once

#include <QColor>
#include <QQmlComponent>
#include <Dynamics/b2Body.h>

#include "ArrayView.h"
#include "Object.h"
#include "meta.h"

namespace aw {
namespace game {

struct Image;
struct Scene;

struct Body
: Object
, Observer<0> // parent = scene
, Observer<-1> // body ready
, Observer<-2> // scene ready
{
	enum Type {STATIC = b2_staticBody, KINEMATIC = b2_kinematicBody, DYNAMIC = b2_dynamicBody};
	Q_ENUMS(Type)
	AW_DECLARE_OBJECT_STUB(Body)
	AW_DECLARE_PROPERTY(aw::game::Scene *, scene)
	AW_DECLARE_PROPERTY(b2Vec2, position) ///< m
	AW_DECLARE_PROPERTY(b2Vec2, velocity) ///< m s⁻¹
	AW_DECLARE_PROPERTY(float, angle) ///< rad
	AW_DECLARE_PROPERTY(float, angular_velocity) ///< rad s⁻¹
	AW_DECLARE_PROPERTY_STORED(Type, type) = DYNAMIC;
	AW_DECLARE_PROPERTY_STORED(float, damping) = 0; ///< s⁻¹
	AW_DECLARE_PROPERTY_STORED(float, angular_damping) = 0; ///< s⁻¹
	AW_DECLARE_PROPERTY_STORED(float, scale) = 1;
	AW_DECLARE_PROPERTY_STORED(unsigned, icon) = 0;
	AW_DECLARE_PROPERTY_STORED(float, masse) = 1;
	AW_DECLARE_PROPERTY_STORED(QColor, icon_color) = Qt::red;
	AW_DECLARE_PROPERTY_STORED(QQmlComponent *, loot_factory) = 0;
	AW_DECLARE_PROPERTY_STORED(int, weapon_regeneration) = 1;
	AW_DECLARE_PROPERTY_STORED(aw::ArrayView<aw::game::Image *>, images);
	b2Vec2 position_ = b2Vec2_zero; ///< m
	b2Vec2 velocity_ = b2Vec2_zero; ///< m s⁻¹
	float angle_ = 0; ///< rad
	float angular_velocity_ = 0; ///< rad s⁻¹
protected:
	Body();
	~Body() = default;
	bool event(QEvent *) override;
	void update(const Observer<0>::Tag &) override;
	void update(const Observer<-2>::Tag &) override;
	void update(const Observer<-1>::Tag &) override;
public:
	Q_SLOT b2Vec2 point_from_scene(b2Vec2) const;
	Q_SLOT b2Vec2 point_to_scene(b2Vec2) const;
	Q_SLOT b2Vec2 direction_from_scene(b2Vec2) const;
	Q_SLOT b2Vec2 direction_to_scene(b2Vec2) const;
	std::unique_ptr<b2Body, void(*)(b2Body *)> body_;

	ObserverList *signal_scene_changed()
	{
		return signal_parent_changed();
	}
};

}
}
