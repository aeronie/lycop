#pragma once

#include <QQmlListProperty>
#include <QQmlParserStatus>

#include "meta.h"

namespace aw {
namespace game {

struct Object
: QObject
, QQmlParserStatus
{
	Q_OBJECT
	Q_INTERFACES(QQmlParserStatus)
	Q_CLASSINFO("DefaultProperty", "children")
	Q_PROPERTY(QQmlListProperty<QObject> children READ qml_children NOTIFY children_changed)
	Q_PROPERTY(QObject *parent READ parent WRITE setParent NOTIFY parent_changed)
	AW_DECLARE_SIGNAL(parent_changed)
	AW_DECLARE_SIGNAL(ready)
	QQmlListProperty<QObject> qml_children();
	Q_SIGNAL void children_changed();
	Q_SIGNAL void parent_changed();
protected:
	void classBegin() override;
	void componentComplete() override;
	void childEvent(QChildEvent *) override;
	bool qml_initialized_ = false;
public:

	Q_SLOT void deleteLater()
	{
		return QObject::deleteLater();
	}
};

}
}
