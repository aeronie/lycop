import QtQuick 2.0 as Q
import aw.game 0.0

Weapon
{
	property int level: 1
	property int bullet_group: groups.enemy_laser
	period: scene.seconds_to_frames([0 , 1 / 2 , 1 / 2.4] [level])
	health: max_health
	max_health: 250 +~~body.health_boost
	range_bullet: scene.seconds_to_frames(2)
	image: Image
	{
		material: "equipments/heavy-laser-gun"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/heavy-laser-gun/shooting"
		scale: slot.sound_scale
	}
	bullet_factory: Bullet
	{
		scale: 0.45
		range: weapon.range_bullet
		damages: 200
		Image
		{
			material: "equipments/laser-gun/bullet"
			z: altitudes.bullet
		}
		PolygonCollider
		{
			vertexes: [Qt.point(-0.125, -1), Qt.point(-0.125, 1), Qt.point(0.125, 1), Qt.point(0.125, -1)]
			group: weapon.bullet_group
			sensor: true
		}
		Q.Component.onCompleted:
		{
			position = weapon.slot.point_to_scene(Qt.point(0, -1.4))
			angle = weapon.slot.angle_to_scene(0)
			weapon.sound_shooting.play()
			weapon.set_bullet_velocity(this, 20)
		}
	}
}
