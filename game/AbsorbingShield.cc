#include "AbsorbingShield.h"
#include "meta.private.h"

namespace aw {
namespace game {

AW_DEFINE_OBJECT_STUB(AbsorbingShield)
AW_DEFINE_PROPERTY_STORED(AbsorbingShield, damages_multiplier)
AW_DEFINE_PROPERTY_STORED(AbsorbingShield, shield_multiplier)

}
}
