import QtQuick 2.0
QtObject
{
	property bool credit_not_title: false
	property var tab_final: credit_not_title ? tab_credit_begin.concat(tab_credit,tab_credit_end): tab_credit
	property var tab_credit: [
		[qsTr("<h1>Aeronie's Dream Team</h1><br>") +
				"<br><h3>Benoit Bernay-Angeletti</h3>" +
				"<h3>Rémi Jeaugey</h3>" +
				"<h3>Alexis Wilhelm</h3>"  , "title/team", 0, -50],
		[qsTr("<h1>Translations</h1><br>") +
				"Florian Angeletti<br>" + 
				"André Bernay<br>" + 
				"Robin Dehu<br>" + 
				"Quentin Duverger<br>"+
				"M. Gauvent<br>"+
				"Simon Lapersonne<br>"+
				"Pepys<br>"+
				"Sylvain Rebeyrol<br>"+
				"Damien Teyssier<br>"+
				"William Tottey<br>"+
				"Ian Wegener", "title/translations", 40, -40],
		[qsTr("<h1>Background music</h1>") + "<br>Licensed under Creative Commons: By Attribution 3.0 License<br>http://creativecommons.org/licenses/by/3.0/<br>" +
				qsTr("<h2>Title screen</h2>") +
				"<ul><li><b>Ouroboros</b> by Kevin MacLeod, http://incompetech.com</li>" +
				"</ul>" +
				qsTr("<h2>Chapter 1</h2>") +
				"<ul><li><b>Comet Halley</b> by Edgaras Zakevicius 'Stellardrone', http://stellardrone.bandcamp.com</li>" +
				"</ul>" +
				qsTr("<h2>Chapter 2</h2>") +
				"<ul><li><b>Sci-Fi Close</b> by Eric Matyas, http://Soundimage.org</li>" + // surface
				"<li><b>Sky Game Menu</b> by Eric Matyas, http://Soundimage.org</li>" + // souterrain
				"<li><b>Exhilarate</b> by Kevin MacLeod, http://incompetech.com</li>" + // autodestruction
				"</ul>", "title/music", -40, -40],
		[qsTr("<h1>Background music</h1>") + "<br>Licensed under Creative Commons: By Attribution 3.0 License<br>http://creativecommons.org/licenses/by/3.0/<br>" +
				qsTr("<h2>Chapter 3</h2>") +
				"<ul><li><b>Take The Lead</b> by Kevin MacLeod, http://incompetech.com</li>" +
				"</ul>" +
				qsTr("<h2>Chapter 4</h2>") +
				"<ul><li><b>Just Among Us</b> by Android128, http://opengameart.org/content/just-among-us
</li>" +
				"</ul>" +
				qsTr("<h2>Chapter 5</h2>") +
				"<ul><li><b>Neoliths</b> by Kevin MacLeod, http://incompetech.com</li>" + // surface
				"<li><b>Ethereal Space (cdk Mix)</b> by cdk Ft: snowflake, http://dig.ccmixter.org/files/cdk/34151 
</li>" + 
				"<li><b>Reckless</b> by Dysfunction_AL, http://dig.ccmixter.org/files/destinazione_altrove/54482 
</li>" +
				"<li><b>Twisted</b> by Kevin MacLeod, http://incompetech.com</li>" +
				"</ul>", "title/music", -40, -20],
		[qsTr("<h1>Background music</h1>") + "<br>Licensed under Creative Commons: By Attribution 3.0 License<br>http://creativecommons.org/licenses/by/3.0/<br>" +
				qsTr("<h2>Chapter 6</h2>") +
				"<ul><li><b>Eighties Action</b> by Kevin MacLeod, http://incompetech.com</li>" +
				"<li><b>Sized Up</b> by cdk, http://ccmixter.org/files/cdk/45071</li>" + 
				"<li><b>Exhilarate</b> by Kevin MacLeod, http://incompetech.com</li>" +
				"</ul>" +
				qsTr("<h2>Fin ?</h2>") +
				"<ul><li><b>Comet Halley</b> by Edgaras Zakevicius 'Stellardrone', http://stellardrone.bandcamp.com</li>" +
				"<li><b>I Feel You</b> by Kevin MacLeod, http://incompetech.com</li>" +
				"<li><b>Rave Try (Blueeskies)</b> by Android128, http://opengameart.org/content/rave-try-blueeskies
</li>" +
				"</ul>" +
				qsTr("<h2>Chapter 7</h2>") +
				"<ul><li><b>Eighties Action</b> by Kevin MacLeod, http://incompetech.com</li>" +
				"<li><b>Sized Up</b> by cdk, http://ccmixter.org/files/cdk/45071</li>" + 
				"<li><b>Exhilarate</b> by Kevin MacLeod, http://incompetech.com</li>" +
				"</ul>", "title/music", -40, 0],
		[qsTr("<h1>Background music</h1>") + "<br>Licensed under Creative Commons: By Attribution 3.0 License<br>http://creativecommons.org/licenses/by/3.0/<br>" +
				qsTr("<h2>Epilogue</h2>") +
				"<ul><li><b>I Feel You</b> by Kevin MacLeod, http://incompetech.com</li>" +
				"<li><b>Demons</b> by cdk, http://ccmixter.org/files/cdk/46920</li>" + 
				"</ul>" +
				qsTr("<h2>Boss themes</h2>") +
				"<ul><li><b>Rock Theme</b> by Gichco, http://opengameart.org/users/oddroom</li>" +
				"<li><b>Drive</b> by Alex, http://ccmixter.org/people/AlexBeroza/profile</li>" +
				"<li><b>Our Music - RumbleStep Mix</b> by cdk, http://ccmixter.org/people/cdk/profile</li>" +
				"<li><b>The Game Has Changed - RumbleStep Mix</b> by cdk, http://ccmixter.org/files/cdk/41830</li>" +
				"</ul>", "title/music", -60, -20],
		[qsTr("<h1>Sound Effects</h1>") + "<br>Licensed under Creative Commons: By Attribution 3.0 License<br>http://creativecommons.org/licenses/by/3.0/<br>" +
				"<h2></h2><ul>" +
				"<li><b>41 Random Sound Effects</b> by HorrorPen, http://opengameart.org/users/horrorpen</li>" +
				"<li><b>63 Digital sound effects</b> by Kenney, http://Kenney.nl</li>" +
				"<li><b>Fire Loop</b> by Iwan Gabovitch 'qubodup', http://opengameart.org/users/qubodup</li>" +
				"<li><b>Magic Smite</b> by spookymodem, http://opengameart.org/users/spookymodem</li>" +
				"<li><b>Laser Fire</b> By Devin Watson 'dklon', http://opengameart.org/users/dklon</li>" +
				"</ul>", "title/sound", -10, 50],
		[qsTr("<h1>Libraries</h1><br>") +
				"<h2>Qt</h2><p>The Qt Toolkit is Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).<br>Contact: http://www.qt-project.org/legal</p><br>" +
				"<h2>Simple DirectMedia Layer</h2><p>Copyright (C) 1997-2014 Sam Lantinga.<br>slouken@libsdl.org<br>http://www.libsdl.org/</p><br>" +
				"<h2>Box2D</h2><p>Copyright (C) 2006-2013 Erin Catto.<br>http://www.box2d.org/</p>", "title/libs", 50, 10],
		[qsTr("<h1>Libraries</h1><br>") +
				"<h2>OpenAL Soft</h2><p>OpenAL cross platform audio library.<br>Copyright (C) 1999-2008 by authors.<br>http://www.openal-soft.org/</p><br>" +
				"<h2>OggVorbis</h2><p>The OggVorbis source code is copyright (C) 1994-2007 by the Xiph.Org Foundation.<br>http://www.xiph.org/</p><br>" +
				"<h2>Theora</h2><p>The Theora source code is copyright (C) 2002-2009 by the Xiph.Org Foundation.<br>http://www.xiph.org/</p>", "title/libs", 70, -10],
		[qsTr("<h1>Outils Graphiques</h1><br>") +
				"<h2>Blender</h2><p>Copyright Blender Foundation <br>https://www.blender.org/</p><br>" +
				"<h2>Gimp</h2><p>Copyright 2001-2016 The GIMP Team <br>https://www.gimp.org/</p><br>", "title/libs", 30, 30]
	]
	
	property var tab_credit_end: [
	[qsTr("Remerciement")]
	]
	property var tab_credit_begin: [
	[qsTr("<h1> Capitaine Lycop : L'invasion des Heters </h1>")]
	]
	
}
