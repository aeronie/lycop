#include "Behaviour_attack.h"
#include "Collider.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct QueryCallback
: b2QueryCallback
{
	b2Vec2 const &center;
	b2Vec2 nearest_position = b2Vec2_zero;
	float min_distance;
	QList<int> const &target_groups;
	Body const *nearest_body = 0;

	QueryCallback(b2Vec2 const &center, float max_distance, QList<int> const &target_groups)
	: center(center)
	, min_distance(max_distance * max_distance)
	, target_groups(target_groups)
	{
	}

	bool ReportFixture(b2Fixture *fixture) override
	{
		Collider const *collider = static_cast<Collider const *>(fixture->GetUserData());
		if(!target_groups.contains(collider->group())) return true;
		b2Vec2 position = fixture->GetAABB(0).GetCenter();
		float distance = b2DistanceSquared(position, center);
		if(distance >= min_distance) return true;
		nearest_body = collider->body();
		nearest_position = position;
		min_distance = distance;
		return true;
	}
};

}

void Behaviour_attack::update(const Observer<-1>::Tag &)
{
	b2Vec2 p = body()->position();
	QueryCallback query {p, max_distance_, target_groups_};
	scene()->world_->QueryAABB(&query, {{p.x - max_distance_, p.y - max_distance_}, {p.x + max_distance_, p.y + max_distance_}});

	if(query.nearest_body)
	{
		attack(query.nearest_position);
	}
	else
	{
		wait();
	}
	if(shooting_ != bool(query.nearest_body))
	{
		shooting_ = query.nearest_body;
		Q_EMIT shooting_changed(shooting_);
	}
}

void Behaviour_attack::wait()
{
	body()->set_target_direction(b2Vec2_zero);
	body()->set_relative_velocity(b2Vec2_zero);
}

AW_DEFINE_PROPERTY_STORED(Behaviour_attack, target_groups)
AW_DEFINE_PROPERTY_STORED(Behaviour_attack, max_distance)

}
}
