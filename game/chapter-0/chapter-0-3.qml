import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-0/chapter-0.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	property var camera_position:undefined
	property var tab_spawn_enemy:[
	[-5,-5],
	[0,-5],
	[6,-5],
	[6,0],
	[6,6],
	[0,6],
	[-5,6],
	[-5,0],
	]
	property var tab_spawn_angle:[
	Math.PI*3/4,
	Math.PI*1,
	-Math.PI*3/4,
	-Math.PI*1/2,
	-Math.PI*1/4,
	Math.PI*0,
	Math.PI*1/4,
	Math.PI*1/2,
	]
	property int nb_ship: 0
	property int nb_generated_ship: 0
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")

	scene: Scene
	{
		running: false
		Background
		{
			x: 25
			y: 25
			z: altitudes.background_near
			scale: 2
			material: "chapter-0/floor"
		}
		Body
		{
			Repeater
			{
				count: tab_spawn_enemy.length
				Q.Component
				{
					Image
					{
						material: "chapter-0/spawner"
						z: altitudes.background_near
						scale : 2
						position.x: tab_spawn_enemy[index][0]*4
						position.y: tab_spawn_enemy[index][1]*4
					}
				}
			}
		}
		Body
		{
			id: walls
			position.x : -7*4
			position.y : -7*4
			scale: 2
			type: Body.STATIC
			Repeater
			{
				count: 15
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: (index)*2
						position.y: 0*2
					}
				}
			}
			Repeater
			{
				count: 15
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: (index+1)*2
						position.y: 15*2
					}
				}
			}
			Repeater
			{
				count: 15
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: 0*2
						position.y: (index+1)*2
					}
				}
			}
			Repeater
			{
				count: 15
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: 15*2
						position.y: (index)*2
					}
				}
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-1, -1), Qt.point(14*2+1,-1), Qt.point(14*2+1, 1), Qt.point(-1, 1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-1, 1), Qt.point(1,1), Qt.point(1, 15*2+1), Qt.point(-1, 15*2+1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(1*2-1, 15*2-1), Qt.point(15*2+1,15*2-1), Qt.point(15*2+1, 15*2+1), Qt.point(1*2-1, 15*2+1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(15*2-1, -1), Qt.point(15*2+1,-1), Qt.point(15*2+1, 14*2+1), Qt.point(15*2-1, 14*2+1)]
				group: groups.wall
			}
		}
		
		Ship
		{
			id: ship
			Q.Component.onDestruction: game_over = true
			position.y : -2
			position.x : -2
			angle: Math.PI/2
		}
	}
	Q.Component
	{
		id: drone_gun_factory
		Vehicle
		{
			id: drone
			max_speed: 5
			max_angular_speed: 1
			property bool on: false
			icon: 2
			scale: 1.2
			icon_color: "blue"
			property int indice: 0
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						scale: 2
						sound_scale: 10
					}
				}
			}
			Q.Component.onCompleted:
			{
				root.nb_ship++
			}
			Q.Component.onDestruction:
			{
				root.nb_ship--
			}
			Image
			{
				material: "chapter-0/drone"
				z: altitudes.boss + 0.00000001
			}
			EquipmentSlot
			{
				scale: 0.5
				equipment: Gun
				{
					shooting: true
				}
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 5
				//max_distance: 20
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: big_drone_gun_factory
		Vehicle
		{
			id: drone
			max_speed: 5
			max_angular_speed: 1
			property bool on: false
			scale: 2.4
			icon: 2
			icon_color: "blue"
			property int indice: 0
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						scale: 2
						sound_scale: 10
					}
				}
			}
			Q.Component.onCompleted:
			{
				root.nb_ship++
			}
			Q.Component.onDestruction:
			{
				root.nb_ship--
			}
			Image
			{
				material: "chapter-0/big-drone"
				z: altitudes.boss + 0.00000001
			}
			EquipmentSlot
			{
				position.x: 0.5
				scale: 0.25
				equipment: Gun
				{
					shooting: true
				}
			}
			EquipmentSlot
			{
				position.x: -0.5
				scale: 0.25
				equipment: Gun
				{
					shooting: true
				}
			}
			EquipmentSlot
			{
				scale: 0.25
				equipment: TripleGun
				{
					shooting: true
				}
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 5
				//max_distance: 20
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	StateMachine
	{
		begin: state_story_0
		active: game_running
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					saved_game.set("tax", 1)
					music.objectName = "chapter-1/boss"
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_after_story_1
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					camera_position = undefined
					messages.add("nectaire/normal", qsTr("0_beginning"))
				}
			}
		}
		State 
		{
			id: state_after_story_1
			SignalTransition 
			{
				targetState: state_spawn_1
				signal: state_after_story_1.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					var spawn_nb = random_index(tab_spawn_enemy.length)
					drone_gun_factory.createObject(null, {scene: scene, position: Qt.point(root.tab_spawn_enemy[spawn_nb][0]*4, root.tab_spawn_enemy[spawn_nb][1]*4), angle: root.tab_spawn_angle[spawn_nb]})
					root.nb_generated_ship++
				}
			}
		}
		State
		{
			id: state_spawn_1
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_spawn_1
				signal: nb_shipChanged
				guard: nb_ship < 1 && root.nb_generated_ship == 1
				onTriggered: 
				{
					messages.add("nectaire/normal", qsTr("1_first_wave_finished"))
				}
			}
		}
		State 
		{
			id: state_after_spawn_1
			SignalTransition 
			{
				targetState: state_spawn_2
				signal: state_after_spawn_1.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					root.nb_generated_ship = 0
					var spawn_nb1 = random_index(tab_spawn_enemy.length)
					var spawn_nb2tmp = random_index(tab_spawn_enemy.length-1)
					var spawn_nb2 = (spawn_nb2tmp < spawn_nb1 ? spawn_nb2tmp : spawn_nb2tmp + 1)
					drone_gun_factory.createObject(null, {scene: scene, position: Qt.point(root.tab_spawn_enemy[spawn_nb1][0]*4, root.tab_spawn_enemy[spawn_nb1][1]*4), angle: root.tab_spawn_angle[spawn_nb1]})
					root.nb_generated_ship++
					drone_gun_factory.createObject(null, {scene: scene, position: Qt.point(root.tab_spawn_enemy[spawn_nb2][0]*4, root.tab_spawn_enemy[spawn_nb2][1]*4), angle: root.tab_spawn_angle[spawn_nb2]})
					root.nb_generated_ship++
				}
			}
		}
		State
		{
			id: state_spawn_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_spawn_2
				signal: nb_shipChanged
				guard: nb_ship < 1 && root.nb_generated_ship == 2
				onTriggered: 
				{
					messages.add("nectaire/normal", qsTr("2_second_wave_finished"))
				}
			}
		}
		State 
		{
			id: state_after_spawn_2
			SignalTransition 
			{
				targetState: state_spawn_3
				signal: state_after_spawn_2.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					root.nb_generated_ship = 0
					var spawn_nb1 = random_index(tab_spawn_enemy.length)
					var spawn_nb2tmp = random_index(tab_spawn_enemy.length-1)
					var spawn_nb2 = (spawn_nb2tmp < spawn_nb1 ? spawn_nb2tmp : spawn_nb2tmp + 1)
					var spawn_nb3tmp = random_index(tab_spawn_enemy.length-2)
					var spawn_nb3 = 0
					var first = (spawn_nb1 < spawn_nb2 ? spawn_nb1 : spawn_nb2)
					var second = (spawn_nb1 < spawn_nb2 ? spawn_nb2 : spawn_nb1)
					var third = 0
					if (spawn_nb3tmp < first)
					{
						spawn_nb3 = spawn_nb3tmp
						third = second
						second = first
						first = spawn_nb3
					}
					else if (spawn_nb3tmp + 1 < second)
					{
						spawn_nb3 = spawn_nb3tmp+1
						third = second
						second = spawn_nb3
					}
					else 
					{
						spawn_nb3 = spawn_nb3tmp+2
						third = spawn_nb3
					}
					var spawn_nb4tmp = random_index(tab_spawn_enemy.length-3)
					var spawn_nb4 = 0
					if (spawn_nb4tmp < first)
					{
						spawn_nb4 = spawn_nb4tmp
					}
					else if (spawn_nb4tmp + 1 < second)
					{
						spawn_nb4 = spawn_nb4tmp+1
					}
					else if (spawn_nb4tmp + 2 < third)
					{
						spawn_nb4 = spawn_nb4tmp+2
					}
					else 
					{
						spawn_nb4 = spawn_nb4tmp+3
					}
					drone_gun_factory.createObject(null, {scene: scene, position: Qt.point(root.tab_spawn_enemy[spawn_nb1][0]*4, root.tab_spawn_enemy[spawn_nb1][1]*4), angle: root.tab_spawn_angle[spawn_nb1]})
					root.nb_generated_ship++
					drone_gun_factory.createObject(null, {scene: scene, position: Qt.point(root.tab_spawn_enemy[spawn_nb2][0]*4, root.tab_spawn_enemy[spawn_nb2][1]*4), angle: root.tab_spawn_angle[spawn_nb2]})
					root.nb_generated_ship++
					drone_gun_factory.createObject(null, {scene: scene, position: Qt.point(root.tab_spawn_enemy[spawn_nb3][0]*4, root.tab_spawn_enemy[spawn_nb3][1]*4), angle: root.tab_spawn_angle[spawn_nb3]})
					root.nb_generated_ship++
					drone_gun_factory.createObject(null, {scene: scene, position: Qt.point(root.tab_spawn_enemy[spawn_nb4][0]*4, root.tab_spawn_enemy[spawn_nb4][1]*4), angle: root.tab_spawn_angle[spawn_nb4]})
					root.nb_generated_ship++
				}
			}
		}
		State
		{
			id: state_spawn_3
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_spawn_3
				signal: nb_shipChanged
				guard: nb_ship < 1 && root.nb_generated_ship == 4
				onTriggered: 
				{
					messages.add("nectaire/normal", qsTr("3_third_wave_finished"))
				}
			}
		}
		State 
		{
			id: state_after_spawn_3
			SignalTransition 
			{
				targetState: state_spawn_4
				signal: state_after_spawn_3.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					root.nb_generated_ship = 0
					var spawn_nb = random_index(tab_spawn_enemy.length)
					big_drone_gun_factory.createObject(null, {scene: scene, position: Qt.point(root.tab_spawn_enemy[spawn_nb][0]*4, root.tab_spawn_enemy[spawn_nb][1]*4), angle: root.tab_spawn_angle[spawn_nb]})
					root.nb_generated_ship++
				}
			}
		}
		State
		{
			id: state_spawn_4
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_spawn_4
				signal: nb_shipChanged
				guard: nb_ship < 1 && root.nb_generated_ship == 1
				onTriggered: 
				{
					messages.add("nectaire/normal", qsTr("4_fourth_wave_finished"))
				}
			}
		}
		State 
		{
			id: state_after_spawn_4
			SignalTransition 
			{
				targetState: state_end
				signal: state_after_spawn_4.propertiesAssigned
				guard: !messages.has_unread 
				onTriggered:
				{
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					window.back_to_main_menu()
				}
			}
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["chapter-0/floor"])
		jobs.run(scene_view, "preload", ["chapter-0/wall"])
		jobs.run(scene_view, "preload", ["chapter-0/door"])
		jobs.run(scene_view, "preload", ["chapter-0/spawner"])
		jobs.run(scene_view, "preload", ["chapter-0/drone"])
		jobs.run(scene_view, "preload", ["chapter-0/big-drone"])
		jobs.run(music, "preload", ["chapter-1/boss"])

		groups.init(scene)
	}
}
