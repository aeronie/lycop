import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-0/chapter-0.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal timer_end
	property var camera_position:undefined
	property real ti: 0
	property real ti_deb: 0
	property var tab_checkpoint_enemy:[
	[-3,-1],
	[-1,-3],
	[2,-3],
	[4,-1],
	[4,2],
	[2,4],
	[-1,4],
	[-3,2]
	]
	property var tab_spawn_enemy:[
	[-5,0],
	[6,0],
	[0,-5],
	[0,6]
	]
	property int nb_ship: 0
	property int indice_global: 0
	property real t0: 0
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")

	scene: Scene
	{
		running: false
		Animation
		{
			id: timer
			time: 0
			speed: 1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		Background
		{
			x: 25
			y: 25
			z: altitudes.background_near
			scale: 2
			material: "chapter-0/floor"
		}
		Body
		{
			Repeater
			{
				count: tab_spawn_enemy.length
				Q.Component
				{
					Image
					{
						material: "chapter-0/spawner"
						z: altitudes.background_near
						scale : 2
						position.x: tab_spawn_enemy[index][0]*4
						position.y: tab_spawn_enemy[index][1]*4
					}
				}
			}
		}
		Body
		{
			id: walls
			position.x : -7*4
			position.y : -7*4
			scale: 2
			type: Body.STATIC
			Repeater
			{
				count: 15
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: (index)*2
						position.y: 0*2
					}
				}
			}
			Repeater
			{
				count: 15
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: (index+1)*2
						position.y: 15*2
					}
				}
			}
			Repeater
			{
				count: 15
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: 0*2
						position.y: (index+1)*2
					}
				}
			}
			Repeater
			{
				count: 15
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: 15*2
						position.y: (index)*2
					}
				}
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-1, -1), Qt.point(14*2+1,-1), Qt.point(14*2+1, 1), Qt.point(-1, 1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-1, 1), Qt.point(1,1), Qt.point(1, 15*2+1), Qt.point(-1, 15*2+1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(1*2-1, 15*2-1), Qt.point(15*2+1,15*2-1), Qt.point(15*2+1, 15*2+1), Qt.point(1*2-1, 15*2+1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(15*2-1, -1), Qt.point(15*2+1,-1), Qt.point(15*2+1, 14*2+1), Qt.point(15*2-1, 14*2+1)]
				group: groups.wall
			}
		}
		
		Ship
		{
			id: ship
			Q.Component.onDestruction: game_over = true
			position.y : -2
			position.x : -2
			angle: Math.PI/2
		}
	}
	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			id: drone
			max_speed: 10
			max_angular_speed: 5
			property bool on: false
			icon: 2
			scale: 1.2
			icon_color: "blue"
			property int indice: 0
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						scale: 2
						sound_scale: 10
					}
				}
			}
			Q.Component.onCompleted:
			{
				root.nb_ship++
			}
			Q.Component.onDestruction:
			{
				root.nb_ship--
			}
			Image
			{
				material: "chapter-1/drone"
				z: altitudes.boss + 0.00000001
			}
			Behaviour_attack_missile
			{
				enabled: !drone.attack
				target_groups: [groups.enemy_checkpoint - drone.indice]
				range: 10000000
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: checkpoint_factory
		Vehicle
		{
			id: check
			property int indice: 0
			property var tab: root.tab_checkpoint_enemy
			property real x: tab[tab_indice][0] * 4
			property real y: tab[tab_indice][1] * 4
			property bool random_check: false
			property bool random_point: false
			property int tab_indice: 0
			property bool on: false
			property var drone
			property real ti_local: root.ti

			onTi_localChanged:
			{
				check.position.x = check.x
				check.position.y = check.y
				if (!drone)
				{
					destroy()
				}
			}
			onOnChanged:
			{
				if(on)
				{
					if (random_check)
					{
						tab_indice = random_index(tab.length)
					}
					else if (random_point)
					{
						var tmp_x = Math.random()
						var tmp_y = Math.random()
						x = (tmp_x*12-5.5)*4
						y = (tmp_y*12-5.5)*4
					}
					else
					{
						tab_indice = (tab_indice + 1)%tab.length
					}
					on = false
				}
			}

			Q.Component.onCompleted:
			{
				check.position = Qt.point(x,y)
				tab_indice = (check.random_check ? random_index(tab.length) : 0)
				if ( check.random_point )
				{
					var tmp_x = Math.random()
					var tmp_y = Math.random()
					x = (tmp_x*13-6)*4
					y = (tmp_y*13-6)*4
				}
			}

			CircleCollider
			{
				group: groups.enemy_checkpoint - check.indice
				sensor: true
			}
		}
	}
	StateMachine
	{
		begin: state_story_0
		active: game_running
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					saved_game.set("tax", 1)
					music.objectName = "chapter-0/mafia"
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_after_story_1
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					camera_position = undefined
					var a1 = []
					controls.get_bindings(a1, controls.weapons_boost)
					var a2 = []
					controls.get_bindings(a2, controls.weapons)
					messages.add("nectaire/normal", qsTr("0_beginning").arg(a2[0]).arg(a1[0]))
				}
			}
		}
		State 
		{
			id: state_after_story_1
			SignalTransition 
			{
				targetState: state_spawn_1
				signal: state_after_story_1.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
				}
			}
		}
		State
		{
			id: state_spawn_1
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_spawn_1
				signal: tiChanged
				guard: (root.ti - root.ti_deb >  1.1) && root.indice_global < 8
				onTriggered: 
				{
					var d1 = drone_factory.createObject(null, {scene: scene, position: Qt.point( tab_spawn_enemy[0][0]*4, tab_spawn_enemy[0][1]*4), angle: Math.PI/2, indice : root.indice_global})
					//checkpoint
					checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 1})
					root.indice_global++
					root.ti_deb = root.ti
				}
			}
			SignalTransition
			{
				targetState: state_after_spawn_1_2
				signal: nb_shipChanged
				guard: nb_ship < 1 && root.indice_global == 8
				onTriggered: 
				{
					root.indice_global = 0
					messages.add("nectaire/normal", qsTr("1_first_wave_finished"))
				}
			}
		}
		State {id: state_after_spawn_1; SignalTransition {targetState: state_spawn_1; signal: state_after_spawn_1.propertiesAssigned; guard: !messages.has_unread}}
		State {
			id: state_after_spawn_1_2;
			SignalTransition
			{
				targetState: state_spawn_2;
				signal: state_after_spawn_1_2.propertiesAssigned;
				guard: !messages.has_unread
				onTriggered: 
				{
				}
			}
		}
		State
		{
			id: state_spawn_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_spawn_2
				signal: tiChanged
				guard: (root.ti - root.ti_deb >  1.1) && root.indice_global < 16
				onTriggered: 
				{
					var d1 = drone_factory.createObject(null, {scene: scene, position: Qt.point( tab_spawn_enemy[0][0]*4, tab_spawn_enemy[0][1]*4), angle: Math.PI/2, indice : root.indice_global})
					//checkpoint
					checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 1, random_check : true})
					root.indice_global++
					var d2 = drone_factory.createObject(null, {scene: scene, position: Qt.point( tab_spawn_enemy[1][0]*4, tab_spawn_enemy[1][1]*4), angle: -Math.PI/2, indice : root.indice_global})
					//checkpoint
					checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d2, tab_indice: 1, random_check : true})
					root.indice_global++
					root.ti_deb = root.ti
				}
			}
			SignalTransition
			{
				targetState: state_after_spawn_2_3
				signal: nb_shipChanged
				guard: nb_ship < 1 && root.indice_global == 16
				onTriggered: 
				{
					root.indice_global = 0
					messages.add("nectaire/normal", qsTr("2_second_wave_finished"))
				}
			}
		}
		State 
		{
			id: state_after_spawn_2
			SignalTransition 
			{
				targetState: state_spawn_2
				signal: state_after_spawn_2.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
				}
			}
		}
		State {
			id: state_after_spawn_2_3;
			SignalTransition
			{
				targetState: state_spawn_3;
				signal: state_after_spawn_2_3.propertiesAssigned;
				guard: !messages.has_unread
				onTriggered: 
				{
				}
			}
		}
		State
		{
			id: state_spawn_3
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_spawn_3
				signal: tiChanged
				guard: (root.ti - root.ti_deb >  1.1) && root.indice_global < 20
				onTriggered: 
				{
					var d1 = drone_factory.createObject(null, {scene: scene, position: Qt.point(tab_spawn_enemy[0][0]*4, tab_spawn_enemy[0][1]*4), angle: Math.PI/2, indice : root.indice_global})
					//checkpoint
					checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 1, random_point : true})
					root.indice_global++
					var d2 = drone_factory.createObject(null, {scene: scene, position: Qt.point(tab_spawn_enemy[1][0]*4, tab_spawn_enemy[1][1]*4), angle: -Math.PI/2, indice : root.indice_global})
					//checkpoint
					checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d2, tab_indice: 1, random_point : true})
					root.indice_global++
					var d3 = drone_factory.createObject(null, {scene: scene, position: Qt.point(tab_spawn_enemy[2][0]*4, tab_spawn_enemy[2][1]*4), angle: -Math.PI/2, indice : root.indice_global})
					//checkpoint
					checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d3, tab_indice: 1, random_point : true})
					root.indice_global++
					var d4 = drone_factory.createObject(null, {scene: scene, position: Qt.point(tab_spawn_enemy[3][0]*4, tab_spawn_enemy[3][1]*4), angle: -Math.PI/2, indice : root.indice_global})
					//checkpoint
					checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d4, tab_indice: 1, random_point : true})
					root.indice_global++
					root.ti_deb = root.ti
				}
			}
			SignalTransition
			{
				targetState: state_after_spawn_3_end
				signal: nb_shipChanged
				guard: nb_ship < 1 && root.indice_global == 20
				onTriggered: 
				{
					root.indice_global = 0
					messages.add("nectaire/normal", qsTr("3_third_wave_finished"))
				}
			}
		}
		State 
		{
			id: state_after_spawn_3
			SignalTransition 
			{
				targetState: state_spawn_3
				signal: state_after_spawn_3.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
				}
			}
		}
		State {
			id: state_after_spawn_3_end;
			SignalTransition
			{
				targetState: state_end;
				signal: state_after_spawn_3_end.propertiesAssigned;
				guard: !messages.has_unread
				onTriggered: 
				{
					saved_game.set("file", "game/chapter-0/chapter-0-3.qml")
					saved_game.set("equipments/gun/level", 2)
					saved_game.set("equipments/0", "gun")
					saved_game.set("equipments/1", "gun")
					saved_game.set("equipments/2", "gun")
					saved_game.set("equipments/3", "gun")
					saved_game.set("equipments/4", "gun")
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["chapter-0/floor"])
		jobs.run(scene_view, "preload", ["chapter-0/wall"])
		jobs.run(scene_view, "preload", ["chapter-0/door"])
		jobs.run(scene_view, "preload", ["chapter-0/spawner"])
		jobs.run(scene_view, "preload", ["chapter-0/drone"])
		jobs.run(music, "preload", ["chapter-0/mafia"])

		groups.init(scene)
	}
}
