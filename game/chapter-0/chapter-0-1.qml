import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-0/chapter-0.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias door_timer: door_timer
	property alias ship: ship
	signal timer_end
	property var camera_position:undefined
	property real ti: 0
	property var doors: []
	property var doors_coord: [
	[2,-0.5],
	[8,-0.5],
	[14,-0.5],
	[17,2.5],
	[20,5.5],
	[23,2.5],
	[26,-0.5],
	[38,-0.5]]
	property real t0: 0
	property int id_active: 0
	property int id_target: 1
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")

	scene: Scene
	{
		running: false
		Animation
		{
			id: timer
			time: 0
			speed: 1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		Animation
		{
			id: door_timer
			property real time_default_2_3 : 1.6
			property real time_default_4_6 : 2.1
			time: 2
			speed: 0
			onTimeChanged:
			{
				if (time < 0)
				{
					root.timer_end()
				}
			}
		}
		Background
		{
			x: 25
			y: 25
			z: altitudes.background_near
			scale: 2
			material: "chapter-0/floor"
		}
		Body
		{
			id: walls
			position.x : -8
			position.y : -8
			scale: 2
			type: Body.STATIC
			Repeater
			{
				count: 42
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: (index)*2
						position.y: 0*2
					}
				}
			}
			Repeater
			{
				count: 3
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: 0*2
						position.y: (index+1)*2
					}
				}
			}
			Repeater
			{
				count: 16
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: (index+1)*2
						position.y: 3*2
					}
				}
			}
			Repeater
			{
				count: 7
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: 16*2
						position.y: (index+4)*2
					}
				}
			}
			Repeater
			{
				count: 12
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: (index+17)*2
						position.y: 10*2
					}
				}
			}
			Repeater
			{
				count: 7
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: 28*2
						position.y: (index+3)*2
					}
				}
			}
			Repeater
			{
				count: 13
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: (index+29)*2
						position.y: 3*2
					}
				}
			}
			Repeater
			{
				count: 2
				Q.Component
				{
					Image
					{
						material: "chapter-0/wall"
						z: altitudes.background_near
						position.x: 41*2
						position.y: (index+1)*2
					}
				}
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-1, -1), Qt.point(41*2+1,-1), Qt.point(41*2+1, 1), Qt.point(-1, 1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-1, 1), Qt.point(1,1), Qt.point(1, 3*2+1), Qt.point(-1, 3*2+1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(1*2-1, 3*2-1), Qt.point(16*2+1,3*2-1), Qt.point(16*2+1, 3*2+1), Qt.point(1*2-1, 3*2+1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(16*2-1, 4*2-1), Qt.point(16*2+1,4*2-1), Qt.point(16*2+1, 10*2+1), Qt.point(16*2-1, 10*2+1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(17*2-1, 10*2-1), Qt.point(28*2+1,10*2-1), Qt.point(28*2+1, 10*2+1), Qt.point(17*2-1, 10*2+1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(28*2-1, 3*2-1), Qt.point(28*2+1,3*2-1), Qt.point(28*2+1, 9*2+1), Qt.point(28*2-1, 9*2+1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(29*2-1, 3*2-1), Qt.point(41*2+1,3*2-1), Qt.point(41*2+1, 3*2+1), Qt.point(29*2-1, 3*2+1)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [Qt.point(41*2-1, 1*2-1), Qt.point(41*2+1,1*2-1), Qt.point(41*2+1, 2*2+1), Qt.point(41*2-1, 2*2+1)]
				group: groups.wall
			}
		}
		Repeater
		{
			count: root.doors_coord.length
			Q.Component
			{
				Sensor
				{
					id : door
					property int doorId: index+1
					type: Body.STATIC
					position.x: root.doors_coord[index][0]*4
					position.y: root.doors_coord[index][1]*4
					icon: 1
					icon_color: "cyan"
					scale: 4
					onValueChanged: 
					{
						if (value == 1)
						{
							root.id_active = doorId
						}
					}
					Image
					{
						material: "chapter-0/door"
						z: altitudes.background_near
						mask: (root.id_target == doorId ? Qt.rgba(1, 223/255, 0, 1) : Qt.rgba(100/255, 149/255, 237/255, 1))
					}
					PolygonCollider
					{
						vertexes: [Qt.point(-0.1, -1), Qt.point(0.1,-1), Qt.point(0.1, 1), Qt.point(-0.1, 1)]
						group: groups.sensor
						sensor: true
					}
					Q.Component.onCompleted:
					{
						doors.push(door)
					}
				}
			}
		}
		// 15*2 canons
		Repeater
		{
			count: 15
			Q.Component
			{
				Vehicle
				{
					id: turret
					property bool shooting: false
					type: Body.KINEMATIC
					max_speed: 0
					icon: 3
					icon_color: "red"
					property bool alive: true
					onAliveChanged: if(!alive) destroy()
					property real ti_local: root.ti
					property real time_deb: root.ti
					property real time_to_wait: 2
					property real time_to_fire: 1
					position.x : (30+index/3)*4
					position.y : 1.5
					onTi_localChanged:
					{
						if (shooting)
						{
							if(time_to_wait < ti_local - time_deb)
							{
								time_deb = ti_local
								shooting = false
							}
						}
						else
						{
							if(time_to_wait < ti_local - time_deb)
							{
								time_deb = ti_local
								shooting = true
							}
						}
					}
					Image
					{
						material: "chapter-0/wall"
						scale: 0.7
						position.y : -0.2
						z: altitudes.background_near
					}
					CircleCollider
					{
						id: collider
						radius: 0.7
						group: groups.wall
					}
					EquipmentSlot
					{
						scale: 0.7
						angle: 0
						equipment: Gun
						{
							level: 2
							shooting: turret.shooting
						}
					}
					Q.Component.onCompleted:
					{
						time_deb = root.ti+index/30
					}
				}
			}
			Q.Component
			{
				Vehicle
				{
					id: turret
					property bool shooting: false
					type: Body.KINEMATIC
					max_speed: 0
					icon: 3
					icon_color: "red"
					property bool alive: true
					onAliveChanged: if(!alive) destroy()
					property real ti_local: root.ti
					property real time_deb: root.ti
					property real time_to_wait: 2
					property real time_to_fire: 1
					position.x : (30+index/3)*4
					position.y : -5.5
					onTi_localChanged:
					{
						if (shooting)
						{
							if(time_to_wait < ti_local - time_deb)
							{
								time_deb = ti_local
								shooting = false
							}
						}
						else
						{
							if(time_to_wait < ti_local - time_deb)
							{
								time_deb = ti_local
								shooting = true
							}
						}
					}
					Image
					{
						material: "chapter-0/wall"
						scale: 0.7
						position.y : +0.2
						z: altitudes.background_near
					}
					CircleCollider
					{
						id: collider
						radius: 0.7
						group: groups.wall
					}
					EquipmentSlot
					{
						scale: 0.7
						angle: Math.PI
						equipment: Gun
						{
							level: 2
							shooting: turret.shooting
						}
					}
					Q.Component.onCompleted:
					{
						time_deb = root.ti+index/30
					}
				}
			}
		}
		Ship
		{
			id: ship
			Q.Component.onDestruction: game_over = true
			position.y : -2
			position.x : -2
			angle: Math.PI/2
		}
	}
	
	StateMachine
	{
		begin: state_story_0
		active: game_running
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					saved_game.set("tax", 1)
					music.objectName = "chapter-1/main"
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_after_story_1
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					camera_position = undefined
					messages.add("nectaire/normal", qsTr("0_beginning"))
				}
			}
		}
		State 
		{
			id: state_after_story_1
			SignalTransition 
			{
				targetState: state_door_1
				signal: state_after_story_1.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
				}
			}
		}
		State
		{
			id: state_door_1
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_door_1
				signal: id_activeChanged
				guard: root.id_active == 1 && root.id_target == 1
				onTriggered: 
				{
					var a = []
					controls.get_bindings(a, controls.velocity)
					messages.add("nectaire/normal", qsTr("1_first_door").arg(a[0]))
					root.id_target = 2
				}
			}
		}
		State {id: state_after_door_1; SignalTransition {targetState: state_door_2; signal: state_after_door_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_door_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_door_2
				signal: id_activeChanged
				guard: root.id_active == 2 && root.id_target == 2
				onTriggered:
				{
					var a = []
					controls.get_bindings(a, controls.speed_boost)
					messages.add("nectaire/normal", qsTr("2_second_door").arg(a[0]))
					root.id_target = 3
					door_timer.speed = -1
				}
			}
			SignalTransition
			{
				targetState: state_after_door_2
				signal: timer_end
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("3_1_too_late"))
					root.id_target = 2
					root.id_active = 0
					door_timer.speed = 0
					door_timer.time = door_timer.time_default_2_3
				}
			}
			SignalTransition
			{
				targetState: state_after_door_2_3
				signal: id_activeChanged
				guard: root.id_active == 3 && root.id_target == 3
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("3_2_third_door"))
					root.id_target = 4
					door_timer.speed = 0
					door_timer.time = door_timer.time_default_4_6
				}
			}
		}
		State 
		{
			id: state_after_door_2
			SignalTransition 
			{
				targetState: state_door_2
				signal: state_after_door_2.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
				}
			}
		}
		State 
		{
			id: state_after_door_2_3
			
			SignalTransition 
			{
				targetState: state_door_4
				signal: state_after_door_2_3.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
				}
			}
			
		}
		State
		{
			id: state_door_4
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_door_4
				signal: id_activeChanged
				guard: root.id_active == 4 && root.id_target == 4
				onTriggered: 
				{
					messages.add("nectaire/normal", qsTr("4_1_fourth_door"))
					root.id_target = 5
					door_timer.speed = -1
				}
			}
			SignalTransition
			{
				targetState: state_after_door_4
				signal: timer_end
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("4_2_too_late"))
					root.id_target = 4
					root.id_active = 0
					door_timer.speed = 0
					door_timer.time = door_timer.time_default_4_6
				}
			}
			SignalTransition
			{
				targetState: state_after_door_4
				signal: id_activeChanged
				guard: root.id_active == 5 && root.id_target == 5
				onTriggered: 
				{
					root.id_target = 6
				}
			}
			SignalTransition
			{
				targetState: state_after_door_4_6
				signal: id_activeChanged
				guard: root.id_active == 6 && root.id_target == 6
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("6_sixth_door"))
					root.id_target = 7
				}
			}
		}
		State 
		{
			id: state_after_door_4
			
			SignalTransition 
			{
				targetState: state_door_4
				signal: state_after_door_4.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
				}
			}
			
		}
		State 
		{
			id: state_after_door_4_6
			
			SignalTransition 
			{
				targetState: state_door_7
				signal: state_after_door_4_6.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
				}
			}
			
		}
		State
		{
			id: state_door_7
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_door_7
				signal: id_activeChanged
				guard: root.id_active == 7 && root.id_target == 7
				onTriggered: 
				{
					messages.add("nectaire/normal", qsTr("7_seventh_door"))
					root.id_target = 8
				}
			}
		}
		State {id: state_after_door_7; SignalTransition {targetState: state_door_8; signal: state_after_door_7.propertiesAssigned; guard: !messages.has_unread}}
		
		State
		{
			id: state_door_8
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_door_8
				signal: id_activeChanged
				guard: root.id_active == 8 && root.id_target == 8
				onTriggered: 
				{
					messages.add("nectaire/normal", qsTr("8_final_door"))
					root.id_target = 1
				}
			}
		}
		State
		{
			id: state_after_door_8;
			SignalTransition
			{
				targetState: state_end;
				signal: state_after_door_8.propertiesAssigned;
				guard: !messages.has_unread
				onTriggered:
				{
					saved_game.set("file", "game/chapter-0/chapter-0-2.qml")
					saved_game.set("equipments/gun/level", 2)
					saved_game.set("equipments/2", "gun")
					saved_game.set("equipments/3", "gun")
					saved_game.set("equipments/4", "gun")
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: scene_loader.load(settings.get_last_saved_game()) 
			}
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["chapter-0/floor"])
		jobs.run(scene_view, "preload", ["chapter-0/wall"])
		jobs.run(scene_view, "preload", ["chapter-0/door"])
		jobs.run(music, "preload", ["chapter-1/main"])

		groups.init(scene)
	}
}
