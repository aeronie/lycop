import QtQuick 2.0 as Q
import aw.game 0.0

Weapon
{
	property int level: 1
	property int bullet_group: groups.enemy_bullet
	period: scene.seconds_to_frames([0 , 1 / 2 , 1 / 2.4] [level])
	health: max_health
	max_health: 150 +~~body.health_boost
	range_bullet: scene.seconds_to_frames(2)
	image: Image
	{
		material: "equipments/triple-gun"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/gun/shooting"
		scale: slot.sound_scale
	}
	bullet_factory: Repeater
	{
		count: 3
		Q.Component
		{
			Bullet
			{
				scale: 0.15
				range: weapon.range_bullet
				damages: 50
				Image
				{
					material: "equipments/gun/bullet"
					z: altitudes.bullet
				}
				CircleCollider
				{
					group: weapon.bullet_group
					sensor: true
				}
				Q.Component.onCompleted:
				{
					var a = weapon.slot.point_to_scene(Qt.point(0, -1.4))
					var b = (index - 1) * scale * 2
					angle = weapon.slot.angle_to_scene(0)
					position.x = a.x + b * Math.cos(angle)
					position.y = a.y + b * Math.sin(angle)
					weapon.sound_shooting.play()
					weapon.set_bullet_velocity(this, 20)
				}
			}
		}
	}
}
