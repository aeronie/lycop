#pragma once

#include "Frame.h"

namespace aw {
namespace game {

struct Body;
struct Equipment;
struct Scene;

struct EquipmentSlot
: Frame
{
	AW_DECLARE_OBJECT_STUB(EquipmentSlot)
	AW_DECLARE_PROPERTY_STORED(aw::game::Equipment *, equipment) = 0;
	AW_DECLARE_PROPERTY_STORED(QObject *, data) = 0;
protected:
	EquipmentSlot() = default;
	~EquipmentSlot();
public:
	Body *body() const;
	Scene *scene() const;
	Q_SLOT float angle_to_body(float) const;
	Q_SLOT float angle_to_scene(float) const;
	Q_SLOT b2Vec2 point_to_body(b2Vec2) const;
	Q_SLOT b2Vec2 point_to_scene(b2Vec2) const;

	ObserverList *signal_body_changed()
	{
		return signal_parent_changed();
	}
};

}
}
