#pragma once

#include "Equipment.h"

namespace aw {
namespace game {

struct Generator
: Equipment
{
	AW_DECLARE_OBJECT_STUB(Generator)
	AW_DECLARE_PROPERTY_STORED(float, energy_regeneration) = 10; ///< J s⁻¹
protected:
	Generator() = default;
	~Generator() = default;
};

}
}
