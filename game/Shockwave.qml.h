#include "Body.h"
#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct Shockwave
: Body
, Observer<1> // timer
{
	AW_DECLARE_OBJECT_STUB(Shockwave)
	AW_DECLARE_PROPERTY_STORED(float, target_scale) = 1;
	AW_DECLARE_PROPERTY_STORED(float, damages) = 0; //[damages per frame]
	AW_DECLARE_PROPERTY_STORED(int, duration) = 0; //[frame]

	void update(const Observer<-1>::Tag &tag) override
	{
		Body::update(tag);

		if(body_)
		{
			body_->SetSleepingAllowed(false);
			Observer<1>::connect(scene()->signal_frame_begin());
		}
	}

	void update(const Observer<1>::Tag &) override
	{
		if(duration_)
		{
			set_scale(scale() + (target_scale() - scale()) / float(duration_));
			--duration_;
		}
		else
		{
			deleteLater();
		}
	}
};

AW_DEFINE_OBJECT_STUB(Shockwave)
AW_DEFINE_PROPERTY_STORED(Shockwave, target_scale)
AW_DEFINE_PROPERTY_STORED(Shockwave, damages)
AW_DEFINE_PROPERTY_STORED(Shockwave, duration)

}
}
}
