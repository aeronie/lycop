import QtQuick 2.0 as Q
import aw.game 0.0

ActiveEquipment
{
	id: plasmaspray
	property int level: 1
	property int bullet_group: groups.enemy_plasmaspray
	health: max_health
	max_health: 250 +~~body.health_boost
	image: Image
	{
		material: "equipments/plasma-spray"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	Sound
	{
		objectName: "equipments/plasma-spray/shooting"
		scale: shooting ? slot.sound_scale : 0
		looping: true
	}
	Q.Component
	{
		id: spray_factory
		Body
		{
			readonly property real damages: 25 * scene.time_step
			scene: plasmaspray.body.scene
			scale: 3
			Image
			{
				material: "equipments/plasma-spray/" + (scene.time % 7)
				mask: plasmaspray.shooting ? "white" : "transparent"
				position.y: -0.3
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0,0),Qt.point(-1,-1),Qt.point(1,-1)]
				group: plasmaspray.shooting ? bullet_group : 0
				sensor: true
				density: 1e-10
			}
		}
	}
	WeldJoint
	{
		id: joint
		body_1: plasmaspray.body
		Q.Component.onDestruction: body_2.destroy()
		Q.Component.onCompleted:
		{
			body_2 = spray_factory.createObject(null, {position: slot.point_to_scene(Qt.point(0, -1.4)), angle: slot.angle_to_scene(0)})
			slot.angle_changed(slot.angle)
		}
	}
	Q.Connections
	{
		target: slot
		onAngleChanged:
		{
			joint.position = slot.point_to_body(Qt.point(0, -1.4))
			joint.angle = slot.angle_to_body(0)
			joint.initialize()
		}
	}
}
