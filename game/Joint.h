#pragma once

#include <Box2D/Dynamics/Joints/b2Joint.h>
#include <memory>

#include "Object.h"
#include "meta.h"

namespace aw {
namespace game {

struct Body;

struct Joint
: Object
, Observer<0> // ready
, Observer<-1> // body 1 ready
, Observer<-2> // body 2 ready
{
	Q_OBJECT
	AW_DECLARE_PROPERTY_STORED(aw::game::Body *, body_1) = 0;
	AW_DECLARE_PROPERTY_STORED(aw::game::Body *, body_2) = 0;
	AW_DECLARE_PROPERTY_STORED(bool, collide) = false;
	AW_DECLARE_PROPERTY_READONLY(bool, ready)
	void update(const Observer<0>::Tag &) override;
	void update(const Observer<-1>::Tag &) override;
	void update(const Observer<-2>::Tag &) override;
protected:
	Joint();
	void initialize(b2JointDef *);
	b2Vec2 point_to_body_1(b2Vec2) const;
	b2Vec2 point_to_body_2(b2Vec2) const;
public:
	Q_SLOT virtual void initialize() = 0;
	std::unique_ptr<b2Joint, void(*)(b2Joint *)> joint_;
};

}
}
