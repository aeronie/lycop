import QtQuick 2.0

QtObject
{
	property int miner: 4
	property int enemy_miner: 5
	property int drone_nectaire: 7
	property int lycop_human: 8
	property int enemy_invincible: 9
	property int hull: 10
	property int enemy_hull: 11
	property int hull_absorbing_shield: 12
	property int enemy_hull_absorbing_shield: 13
	property int miner_killer: 14
	property int enemy_miner_killer: 15
	property int hull_naked: 20
	property int enemy_hull_naked: 21
	property int hull_naked_aggressive_shield: 22
	property int enemy_hull_naked_aggressive_shield: 23
	property int item_destructible: 24
	property int item_ignore_enemy: 25
	property int shield: 30
	property int enemy_shield: 31
	property int shield_absorbtion: 32
	property int enemy_shield_absorbtion: 33
	property int shield_ignore_wall: 34
	property int enemy_shield_ignore_wall: 35
	property int bullet: 40
	property int enemy_bullet: 41
	property int bullet_only_shield: 42
	property int enemy_bullet_only_shield: 43
	property int bullet_acid: 44
	property int enemy_bullet_acid: 45
	property int laser: 50
	property int enemy_laser: 51
	property int shield_canceler_1: 60
	property int shield_canceler_2: 61
	property int enemy_shield_canceler_2: 62
	property int mine_shockwave: 70
	property int shield_aggressive: 80
	property int enemy_shield_aggressive: 81
	property int plasmaspray: 90
	property int enemy_plasmaspray: 91
	property int dot: 100
	property int enemy_dot: 101
	property int dot_qui_poutre: 102
	property int enemy_dot_qui_poutre: 103
	property int roquette: 110
	property int enemy_roquette: 111
	property int missile: 120
	property int enemy_missile: 121
	property int laser_beam: 130
	property int enemy_laser_beam: 131

	property int wall: -1
	property int sensor: -2
	property int loot: -3
	property int mine: -4
	property int mine_aoe: -5
	property int teleporter: -6
	property int harpoon: -7
	property int reflector_1: -8
	property int reflector_2: -9
	property int reflector_only_player: -10
	property int reflector_only_enemy: -11
	property int no_blink: -12
	property int transportable: -13
	property int clear_screen: -15
	property int destroy_all: -16
	property int checkpoint: -20
	property int enemy_checkpoint: -21
	property int enemy_checkpoint_end: -200
	property int rock: -201
	property int paroi: -202
	property int lycop_dont_move: -203
	//apres -21 ce sont les checkpoints enemy personnalisés

	property var targets: [enemy_hull, enemy_hull_naked, enemy_hull_absorbing_shield, enemy_hull_naked_aggressive_shield,item_destructible,item_ignore_enemy]
	property var enemy_targets: [hull, hull_naked, hull_absorbing_shield, hull_naked_aggressive_shield]

	function bullet_vs_shield(ship, bullet) {bullet.damages = ship.damage_shield(bullet.damages)}
	function bullet_vs_shield_absorbtion(ship, bullet) {ship.absorbtion_shield(bullet.damages); bullet.damages = 0}
	function bullet_vs_hull(ship, bullet, point) {bullet.damages = ship.damage_hull(bullet.damages, point, 0)}
	function bullet_acid_vs_hull(ship, bullet, point) {bullet.damages = ship.damage_hull(bullet.damages, point, 2)}
	function bullet_only_shield_vs_hull(ship, bullet, point) {bullet.damages = ship.damage_hull(bullet.damages, point, 1)}
	function cancel_shield_1(ship) {ship.negate_shield(); ship.negate_secondary_shield()}
	function cancel_shield_2(ship) {ship.negate_shield()}

	function missile_vs_hull(ship, bullet, point) {ship.damage_hull(bullet.damages, point, 0);bullet.deleteLater()}
	function missile_vs_shield(ship, bullet) {ship.damage_shield(bullet.damages);bullet.deleteLater()}

	function begin_dot(ship,shockwave) {if(ship) ship.add_dot(shockwave.damages)}
	function end_dot(ship,shockwave) {if(ship) ship.add_dot(-shockwave.damages)}
	function begin_dot_without_shield(ship,shockwave) {if(ship) ship.add_dot_without_shield(shockwave.damages)}
	function end_dot_without_shield(ship,shockwave) {if(ship) ship.add_dot_without_shield(-shockwave.damages)}

	function begin_dot_qui_poutre(ship,bullet_aoe) {if(ship) ship.add_dot(bullet_aoe.damages_aoe)}
	function end_dot_qui_poutre(ship,bullet_aoe) {if(ship) ship.add_dot(-bullet_aoe.damages_aoe)}
	function begin_dot_qui_poutre_without_shield(ship,bullet_aoe) {if(ship) ship.add_dot_without_shield(bullet_aoe.damages_aoe)}
	function end_dot_qui_poutre_without_shield(ship,bullet_aoe) {if(ship) ship.add_dot_without_shield(-bullet_aoe.damages_aoe)}

	function plasmaspray_vs_ship(ship,spray) {if(ship) ship.add_dot_timed(spray.damages,2)}
	function plasmaspray_vs_ship_end(ship,spray) {if(ship) ship.add_dot_timed(-spray.damages,-2)}

	function switch_sensor_on(sensor, ship)
	{
		if(ship && ship.player)
		{
			sensor.target_value = true
		}
	}
	function switch_sensor_off(sensor, ship)
	{
		if(ship && ship.player)
		{
			sensor.target_value = false
		}
	}
	function pick_loot(loot) {saved_game.add_matter(loot.damages); loot.deleteLater()}

	function collision_ship_enemy_end_dot(ship,enemy)
	{
		if(ship)
		{
			ship.add_dot(-50 * ship.scene.time_step)
			enemy.add_dot(-50 * enemy.scene.time_step)
		}
	}
	function collision_ship_wall_end_dot(wall,ship)
	{
		if(ship)
		{
			ship.add_dot(-50 * ship.scene.time_step)
		}
	}
	function collision_ship_ship_aggressive_shield_end_dot(ship)
	{
		ship.add_dot(-50 * ship.scene.time_step)
	}

	function collision_hull_vs_hull(ship,enemy,point)
	{
		ship.collision_ship_hull(enemy.velocity,enemy.masse,point,0)
		ship.add_dot(50 * ship.scene.time_step)
		enemy.collision_ship_hull(ship.velocity,ship.masse,point,0)
		enemy.add_dot(50 * enemy.scene.time_step)
	}
	function collision_hull_vs_wall(wall,ship,point)
	{
		ship.collision_ship_hull(wall.velocity,wall.masse,point,0)
		ship.add_dot(50 * ship.scene.time_step)
	}
	function collision_shield_vs_wall(wall,ship,point)
	{
		ship.collision_ship_shield(wall.velocity,wall.masse,point,0)
		ship.add_dot(50 * ship.scene.time_step)
	}
	function collision_shield_vs_shield(ship,enemy,point)
	{
		ship.collision_ship_shield(enemy.velocity,enemy.masse,point,0)
		ship.add_dot(50 * ship.scene.time_step)
		enemy.collision_ship_shield(ship.velocity,ship.masse,point,0)
		enemy.add_dot(50 * enemy.scene.time_step)
	}

	function collision_hull_vs_shield(ship,ship_with_shield,point)
	{
		ship.collision_ship_hull(ship_with_shield.velocity,ship_with_shield.masse,point,0)
		ship.add_dot(50 * ship.scene.time_step)
		ship_with_shield.collision_ship_shield(ship.velocity,ship.masse,point,0)
		ship_with_shield.add_dot(50 * ship_with_shield.scene.time_step)
	}

	function collision_hull_vs_shield_aggressive(ship,ship_with_shield_aggressive,point)
	{
		ship.collision_ship_hull(ship_with_shield_aggressive.velocity,ship_with_shield_aggressive.masse,point,ship_with_shield_aggressive.aggressive_shield_active_damages)
		ship.add_dot(50 * ship.scene.time_step)
	}
	function collision_shield_vs_shield_aggressive(ship,ship_with_shield_aggressive,point)
	{
		ship.collision_ship_shield(ship_with_shield_aggressive.velocity,ship_with_shield_aggressive.masse,point,ship_with_shield_aggressive.aggressive_shield_active_damages)
		ship.add_dot(50 * ship.scene.time_step)
	}

	function enemy_checkpoint_on(a, b) {if(b.indice == a.indice){a.on = true ; b.on = true}}
	function checkpoint_on(a, b) {a.on = true ; if(b && b.player) {b.on = true}}
	function destroy_first(a) {a.deleteLater()}
	function destroy_second(a, b) {b.deleteLater()}
	function destroy_both(a, b) {a.deleteLater(); b.deleteLater()}
	function begin_first(a, b, c) {a.begin(b, c)}
	function begin_second(a, b, c) {b.begin(a, c)}
	function end_second(a, b) {if(b){b.end(a)}}

	function init(scene, god)
	{
		for(var a = 0; a < 2; ++a)
		{
			var b = !a
			var z = !god || !a

			scene.contact(a+hull_naked, b+hull_naked).begin.connect(collision_hull_vs_hull)
			scene.contact(a+hull_naked, b+hull_naked).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+hull_naked, b+hull_absorbing_shield).begin.connect(collision_hull_vs_hull)
			scene.contact(a+hull_naked, b+hull_absorbing_shield).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+hull_naked, b+shield).begin.connect(collision_hull_vs_shield)
			scene.contact(a+hull_naked, b+shield).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+hull_naked, b+shield_ignore_wall).begin.connect(collision_hull_vs_shield)
			scene.contact(a+hull_naked, b+shield_ignore_wall).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+hull_naked, wall).begin.connect(collision_hull_vs_wall)
			scene.contact(a+hull_naked, wall).end.connect(collision_ship_wall_end_dot)
			scene.contact(a+hull_naked, item_destructible).begin.connect(collision_hull_vs_hull)
			scene.contact(a+hull_naked, item_destructible).end.connect(collision_ship_enemy_end_dot)

			scene.contact(a+hull_naked, mine).begin.connect(destroy_first)
			scene.contact(a+hull_naked, destroy_all).begin.connect(destroy_second)

			scene.contact(a+hull_absorbing_shield, b+hull_absorbing_shield).begin.connect(collision_hull_vs_hull)
			scene.contact(a+hull_absorbing_shield, b+hull_absorbing_shield).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+hull_absorbing_shield, b+shield).begin.connect(collision_hull_vs_shield)
			scene.contact(a+hull_absorbing_shield, b+shield).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+hull_absorbing_shield, b+shield_ignore_wall).begin.connect(collision_hull_vs_shield)
			scene.contact(a+hull_absorbing_shield, b+shield_ignore_wall).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+hull_absorbing_shield, wall).begin.connect(collision_hull_vs_wall)
			scene.contact(a+hull_absorbing_shield, wall).end.connect(collision_ship_wall_end_dot)
			scene.contact(a+hull_absorbing_shield, item_destructible).begin.connect(collision_hull_vs_hull)
			scene.contact(a+hull_absorbing_shield, item_destructible).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+hull_absorbing_shield, mine).begin.connect(destroy_first)
			scene.contact(a+hull_absorbing_shield, destroy_all).begin.connect(destroy_second)

			scene.contact(a+shield, b+shield).begin.connect(collision_shield_vs_shield)
			scene.contact(a+shield, b+shield).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+shield, b+shield_ignore_wall).begin.connect(collision_shield_vs_shield)
			scene.contact(a+shield, b+shield_ignore_wall).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+shield, wall).begin.connect(collision_shield_vs_wall)
			scene.contact(a+shield, wall).end.connect(collision_ship_wall_end_dot)
			scene.contact(a+shield, item_destructible).begin.connect(collision_hull_vs_shield)
			scene.contact(a+shield, item_destructible).end.connect(collision_ship_enemy_end_dot)
			scene.contact(a+shield, mine).begin.connect(destroy_first)
			scene.contact(a+shield, destroy_all).begin.connect(destroy_second)

			scene.contact(a+shield_ignore_wall, mine).begin.connect(destroy_first)

			scene.contact(a+shield_aggressive, b+hull_naked).begin.connect(collision_hull_vs_shield_aggressive)
			scene.contact(a+shield_aggressive, b+hull_naked).end.connect(collision_ship_ship_aggressive_shield_end_dot)
			scene.contact(a+shield_aggressive, b+hull_absorbing_shield).begin.connect(collision_hull_vs_shield_aggressive)
			scene.contact(a+shield_aggressive, b+hull_absorbing_shield).end.connect(collision_ship_ship_aggressive_shield_end_dot)
			scene.contact(a+shield_aggressive, b+shield).begin.connect(collision_shield_vs_shield_aggressive)
			scene.contact(a+shield_aggressive, b+shield).end.connect(collision_ship_ship_aggressive_shield_end_dot)
			scene.contact(a+shield_aggressive, b+shield_ignore_wall).begin.connect(collision_shield_vs_shield_aggressive)
			scene.contact(a+shield_aggressive, b+shield_ignore_wall).end.connect(collision_ship_ship_aggressive_shield_end_dot)
			scene.contact(a+shield_aggressive, b+shield_aggressive)
			scene.contact(a+shield_aggressive, mine).begin.connect(destroy_first)
			scene.contact(a+shield_aggressive, wall)
			scene.contact(a+shield_aggressive, destroy_all).begin.connect(destroy_second)

			if(z) scene.contact(a+bullet, b+hull_naked).begin.connect(bullet_vs_hull)
			if(z) scene.contact(a+bullet, b+hull_naked_aggressive_shield).begin.connect(bullet_vs_hull)
			scene.contact(a+bullet, b+shield).begin.connect(bullet_vs_shield)
			scene.contact(a+bullet, b+shield_ignore_wall).begin.connect(bullet_vs_shield)
			scene.contact(a+bullet, b+shield_absorbtion).begin.connect(bullet_vs_shield_absorbtion)
			scene.contact(a+bullet, wall).begin.connect(destroy_second)
			scene.contact(a+bullet, mine).begin.connect(destroy_first)
			scene.contact(a+bullet, clear_screen).begin.connect(destroy_second)
			scene.contact(a+bullet, destroy_all).begin.connect(destroy_second)

			if(z) scene.contact(a+bullet_only_shield, b+hull_naked).begin.connect(bullet_only_shield_vs_hull)
			if(z) scene.contact(a+bullet_only_shield, b+hull_naked_aggressive_shield).begin.connect(bullet_only_shield_vs_hull)
			scene.contact(a+bullet_only_shield, b+shield).begin.connect(bullet_vs_shield)
			scene.contact(a+bullet_only_shield, b+shield_ignore_wall).begin.connect(bullet_vs_shield)
			scene.contact(a+bullet_only_shield, b+shield_absorbtion).begin.connect(bullet_vs_shield_absorbtion)
			scene.contact(a+bullet_only_shield, wall).begin.connect(destroy_second)
			scene.contact(a+bullet_only_shield, item_destructible).begin.connect(destroy_second)
			scene.contact(a+bullet_only_shield, mine).begin.connect(destroy_second)
			scene.contact(a+bullet_only_shield, clear_screen).begin.connect(destroy_second)
			scene.contact(a+bullet_only_shield, destroy_all).begin.connect(destroy_second)

			if(z) scene.contact(a+bullet_acid, b+hull_naked).begin.connect(bullet_acid_vs_hull)
			if(z) scene.contact(a+bullet_acid, b+hull_naked_aggressive_shield).begin.connect(bullet_acid_vs_hull)
			scene.contact(a+bullet_acid, b+shield).begin.connect(destroy_second)
			scene.contact(a+bullet_acid, b+shield_ignore_wall).begin.connect(destroy_second)
			scene.contact(a+bullet_acid, b+shield_absorbtion).begin.connect(destroy_second)
			scene.contact(a+bullet_acid, wall).begin.connect(destroy_second)
			scene.contact(a+bullet_acid, item_destructible).begin.connect(destroy_second)
			scene.contact(a+bullet_acid, mine).begin.connect(destroy_first)
			scene.contact(a+bullet_acid, clear_screen).begin.connect(destroy_second)
			scene.contact(a+bullet_acid, destroy_all).begin.connect(destroy_second)

			if(z) scene.contact(a+laser, b+hull_naked).begin.connect(bullet_vs_hull)
			if(z) scene.contact(a+laser, b+hull_naked_aggressive_shield).begin.connect(bullet_vs_hull)
			scene.contact(a+laser, b+shield).begin.connect(bullet_vs_shield)
			scene.contact(a+laser, b+shield_ignore_wall).begin.connect(bullet_vs_shield)
			scene.contact(a+laser, b+shield_absorbtion).begin.connect(bullet_vs_shield_absorbtion)
			scene.contact(a+laser, wall).begin.connect(destroy_second)
			scene.contact(a+laser, mine).begin.connect(destroy_first)
			scene.contact(a+laser, clear_screen).begin.connect(destroy_second)
			scene.contact(a+laser, destroy_all).begin.connect(destroy_second)

			if(z) scene.contact(a+plasmaspray, b+hull_naked).begin.connect(plasmaspray_vs_ship)
			if(z) scene.contact(a+plasmaspray, b+hull_naked).end.connect(plasmaspray_vs_ship_end)
			if(z) scene.contact(a+plasmaspray, b+hull_naked_aggressive_shield).begin.connect(plasmaspray_vs_ship)
			if(z) scene.contact(a+plasmaspray, b+hull_naked_aggressive_shield).end.connect(plasmaspray_vs_ship_end)
			scene.contact(a+plasmaspray, b+shield).begin.connect(plasmaspray_vs_ship)
			scene.contact(a+plasmaspray, b+shield).end.connect(plasmaspray_vs_ship_end)
			scene.contact(a+plasmaspray, b+shield_ignore_wall).begin.connect(plasmaspray_vs_ship)
			scene.contact(a+plasmaspray, b+shield_ignore_wall).end.connect(plasmaspray_vs_ship_end)
			scene.contact(a+plasmaspray, b+shield_absorbtion).begin.connect(plasmaspray_vs_ship)
			scene.contact(a+plasmaspray, b+shield_absorbtion).end.connect(plasmaspray_vs_ship_end)
			scene.contact(a+plasmaspray, mine).begin.connect(destroy_first)

			scene.contact(shield_canceler_1, a+hull_naked).begin.connect(cancel_shield_1)
			scene.contact(shield_canceler_1, a+hull_naked_aggressive_shield).begin.connect(cancel_shield_1)
			scene.contact(shield_canceler_1, a+hull).begin.connect(cancel_shield_1)
			scene.contact(shield_canceler_1, a+shield).begin.connect(cancel_shield_1)
			scene.contact(shield_canceler_1, a+hull_absorbing_shield).begin.connect(cancel_shield_1)

			scene.contact(a+shield_canceler_2, b+hull_naked).begin.connect(cancel_shield_1)
			scene.contact(a+shield_canceler_2, b+shield).begin.connect(cancel_shield_1)
			scene.contact(a+shield_canceler_2, b+hull_naked_aggressive_shield).begin.connect(cancel_shield_1)
			scene.contact(a+shield_canceler_2, b+hull).begin.connect(cancel_shield_1)
			scene.contact(a+shield_canceler_2, b+hull_absorbing_shield).begin.connect(cancel_shield_1)
			scene.contact(a+shield_canceler_2, a+hull_naked).begin.connect(cancel_shield_2)
			scene.contact(a+shield_canceler_2, a+hull_naked_aggressive_shield).begin.connect(cancel_shield_2)
			scene.contact(a+shield_canceler_2, a+hull).begin.connect(cancel_shield_2)
			scene.contact(a+shield_canceler_2, a+hull_absorbing_shield).begin.connect(cancel_shield_2)

			scene.contact(mine_aoe, a+hull_naked).begin.connect(destroy_first)
			scene.contact(mine_aoe, a+hull).begin.connect(destroy_first)
			scene.contact(mine_aoe, a+hull_absorbing_shield).begin.connect(destroy_first)
			scene.contact(mine_aoe, a+hull_naked_aggressive_shield).begin.connect(destroy_first)

			if(z) scene.contact(mine_shockwave, b+hull_naked).begin.connect(begin_dot)
			if(z) scene.contact(mine_shockwave, b+hull_naked).end.connect(end_dot)
			if(z) scene.contact(mine_shockwave, b+hull_naked_aggressive_shield).begin.connect(begin_dot)
			if(z) scene.contact(mine_shockwave, b+hull_naked_aggressive_shield).end.connect(end_dot)
			scene.contact(mine_shockwave, b+hull_absorbing_shield).begin.connect(begin_dot_without_shield)
			scene.contact(mine_shockwave, b+hull_absorbing_shield).end.connect(end_dot_without_shield)
			scene.contact(mine_shockwave, b+shield).begin.connect(begin_dot)
			scene.contact(mine_shockwave, b+shield).end.connect(end_dot)
			scene.contact(mine_shockwave, b+shield_ignore_wall).begin.connect(begin_dot)
			scene.contact(mine_shockwave, b+shield_ignore_wall).end.connect(end_dot)

			if(z) scene.contact(a+dot, b+hull_naked).begin.connect(begin_dot)
			if(z) scene.contact(a+dot, b+hull_naked).end.connect(end_dot)
			if(z) scene.contact(a+dot, b+hull_naked_aggressive_shield).begin.connect(begin_dot)
			if(z) scene.contact(a+dot, b+hull_naked_aggressive_shield).end.connect(end_dot)
			scene.contact(a+dot, b+hull_absorbing_shield).begin.connect(begin_dot_without_shield)
			scene.contact(a+dot, b+hull_absorbing_shield).end.connect(end_dot_without_shield)
			scene.contact(a+dot, b+shield).begin.connect(begin_dot)
			scene.contact(a+dot, b+shield).end.connect(end_dot)
			scene.contact(a+dot, b+shield_ignore_wall).begin.connect(begin_dot)
			scene.contact(a+dot, b+shield_ignore_wall).end.connect(end_dot)
			scene.contact(a+dot, mine).begin.connect(destroy_first)

			if(z) scene.contact(a+dot_qui_poutre, b+hull_naked).begin.connect(begin_dot_qui_poutre)
			if(z) scene.contact(a+dot_qui_poutre, b+hull_naked).end.connect(end_dot_qui_poutre)
			if(z) scene.contact(a+dot_qui_poutre, b+hull_naked_aggressive_shield).begin.connect(begin_dot_qui_poutre)
			if(z) scene.contact(a+dot_qui_poutre, b+hull_naked_aggressive_shield).end.connect(end_dot_qui_poutre)
			scene.contact(a+dot_qui_poutre, b+hull_absorbing_shield).begin.connect(begin_dot_qui_poutre_without_shield)
			scene.contact(a+dot_qui_poutre, b+hull_absorbing_shield).end.connect(end_dot_qui_poutre_without_shield)
			scene.contact(a+dot_qui_poutre, b+shield).begin.connect(begin_dot_qui_poutre)
			scene.contact(a+dot_qui_poutre, b+shield).end.connect(end_dot_qui_poutre)
			scene.contact(a+dot_qui_poutre, b+shield_ignore_wall).begin.connect(begin_dot_qui_poutre)
			scene.contact(a+dot_qui_poutre, b+shield_ignore_wall).end.connect(end_dot_qui_poutre)
			scene.contact(a+dot_qui_poutre, mine).begin.connect(destroy_first)

			if(z) scene.contact(a+roquette, b+hull_naked).begin.connect(missile_vs_hull)
			if(z) scene.contact(a+roquette, b+hull_naked_aggressive_shield).begin.connect(missile_vs_hull)
			scene.contact(a+roquette, b+hull_absorbing_shield).begin.connect(missile_vs_hull)
			scene.contact(a+roquette, b+shield).begin.connect(missile_vs_shield)
			scene.contact(a+roquette, b+shield_ignore_wall).begin.connect(missile_vs_shield)
			scene.contact(a+roquette, b+bullet).begin.connect(destroy_both)
			scene.contact(a+roquette, b+bullet_acid).begin.connect(destroy_both)
			scene.contact(a+roquette, b+bullet_only_shield).begin.connect(destroy_both)
			scene.contact(a+roquette, b+laser).begin.connect(destroy_both)
			scene.contact(a+roquette, b+dot).begin.connect(destroy_second)
			scene.contact(a+roquette, b+dot_qui_poutre).begin.connect(destroy_second)
			scene.contact(a+roquette, b+plasmaspray).begin.connect(destroy_second)
			scene.contact(a+roquette, b+missile).begin.connect(destroy_both)
			scene.contact(a+roquette, b+roquette).begin.connect(destroy_both)
			scene.contact(a+roquette, wall).begin.connect(destroy_second)
			scene.contact(a+roquette, mine).begin.connect(destroy_first)
			scene.contact(a+roquette, clear_screen).begin.connect(destroy_second)
			scene.contact(a+roquette, destroy_all).begin.connect(destroy_second)

			if(z) scene.contact(a+missile, b+hull_naked).begin.connect(missile_vs_hull)
			if(z) scene.contact(a+missile, b+hull_naked_aggressive_shield).begin.connect(missile_vs_hull)
			scene.contact(a+missile, b+hull_absorbing_shield).begin.connect(missile_vs_hull)
			scene.contact(a+missile, b+shield).begin.connect(missile_vs_shield)
			scene.contact(a+missile, b+shield_ignore_wall).begin.connect(missile_vs_shield)
			scene.contact(a+missile, b+laser).begin.connect(destroy_both)
			scene.contact(a+missile, b+dot).begin.connect(destroy_second)
			scene.contact(a+missile, b+dot_qui_poutre).begin.connect(destroy_second)
			scene.contact(a+missile, b+plasmaspray).begin.connect(destroy_second)
			scene.contact(a+missile, b+missile).begin.connect(destroy_both)
			scene.contact(a+missile, wall).begin.connect(destroy_second)
			scene.contact(a+missile, mine).begin.connect(destroy_first)
			scene.contact(a+missile, clear_screen).begin.connect(destroy_second)
			scene.contact(a+missile, destroy_all).begin.connect(destroy_second)

			if(z) scene.contact(a+laser_beam, b+hull_naked).begin.connect(begin_second)
			if(z) scene.contact(a+laser_beam, b+hull_naked).end.connect(end_second)
			scene.contact(a+laser_beam, b+shield).begin.connect(begin_second)
			scene.contact(a+laser_beam, b+shield).end.connect(end_second)
			scene.contact(a+laser_beam, b+hull_absorbing_shield).begin.connect(begin_second)
			scene.contact(a+laser_beam, b+hull_absorbing_shield).end.connect(end_second)
			scene.contact(a+laser_beam, b+hull_naked_aggressive_shield).begin.connect(begin_second)
			scene.contact(a+laser_beam, b+hull_naked_aggressive_shield).end.connect(end_second)
			scene.contact(a+laser_beam, clear_screen).begin.connect(destroy_second)
			scene.contact(a+laser_beam, destroy_all).begin.connect(destroy_second)

			scene.contact(teleporter, a+hull_naked).begin.connect(begin_first)
			scene.contact(teleporter, a+hull_absorbing_shield).begin.connect(begin_first)
			scene.contact(teleporter, a+hull_naked_aggressive_shield).begin.connect(begin_first)
			scene.contact(teleporter, a+hull).begin.connect(begin_first)

			scene.contact(harpoon, a+hull_naked).begin.connect(begin_first)
			scene.contact(harpoon, a+hull_naked_aggressive_shield).begin.connect(begin_first)
			scene.contact(harpoon, a+hull_absorbing_shield).begin.connect(begin_first)
			scene.contact(harpoon, a+hull).begin.connect(begin_first)

			scene.contact(reflector_1, a+bullet).begin.connect(begin_first)
			scene.contact(reflector_1, a+bullet_only_shield).begin.connect(begin_first)
			scene.contact(reflector_1, a+bullet_acid).begin.connect(begin_first)
			scene.contact(reflector_1, a+laser).begin.connect(begin_first)
			scene.contact(reflector_1, a+shield_aggressive).begin.connect(destroy_first)

			scene.contact(reflector_2, a+bullet).begin.connect(begin_first)
			scene.contact(reflector_2, a+bullet_only_shield).begin.connect(begin_first)
			scene.contact(reflector_2, a+bullet_acid).begin.connect(begin_first)
			scene.contact(reflector_2, a+laser).begin.connect(begin_first)
			scene.contact(reflector_2, a+roquette).begin.connect(begin_first)
			scene.contact(reflector_2, a+missile).begin.connect(begin_first)
			scene.contact(reflector_2, a+shield_aggressive).begin.connect(destroy_first)

			scene.contact(a+miner, b+miner_killer).begin.connect(destroy_first)

			scene.contact(rock, a+bullet).begin.connect(begin_first)
			scene.contact(rock, a+bullet_only_shield).begin.connect(destroy_second)
			scene.contact(rock, a+bullet_acid).begin.connect(destroy_second)
			scene.contact(rock, a+laser).begin.connect(begin_first)
			scene.contact(rock, a+roquette).begin.connect(begin_first)
			scene.contact(rock, a+missile).begin.connect(begin_first)
			scene.contact(rock, a+shield_aggressive)
			scene.contact(rock, a+hull_naked).begin.connect(collision_hull_vs_wall)
			scene.contact(rock, a+hull_naked).end.connect(collision_ship_wall_end_dot)
			scene.contact(rock, a+hull_absorbing_shield).begin.connect(collision_hull_vs_wall)
			scene.contact(rock, a+hull_absorbing_shield).end.connect(collision_ship_wall_end_dot)
			scene.contact(rock, a+shield).begin.connect(collision_shield_vs_wall)
			scene.contact(rock, a+shield).end.connect(collision_ship_wall_end_dot)
		}

		scene.contact(lycop_human, wall)
		scene.contact(lycop_human, enemy_invincible)
		scene.contact(lycop_human, enemy_hull_naked)
		scene.contact(lycop_human, lycop_dont_move)
		scene.contact(drone_nectaire, lycop_human).begin.connect(checkpoint_on)
		scene.contact(lycop_human, enemy_shield)
		scene.contact(sensor, lycop_human).begin.connect(switch_sensor_on)
		scene.contact(sensor, lycop_human).end.connect(switch_sensor_off)
		scene.contact(drone_nectaire, wall)
		scene.contact(bullet, drone_nectaire).begin.connect(bullet_vs_hull)
		
		scene.contact(rock, mine).begin.connect(destroy_second)
		scene.contact(rock, rock)
		scene.contact(rock, harpoon).begin.connect(begin_second)
		scene.contact(rock, reflector_1).begin.connect(destroy_second)
		scene.contact(rock, reflector_2).begin.connect(destroy_second)
		scene.contact(rock, reflector_only_player).begin.connect(destroy_second)
		scene.contact(rock, teleporter).begin.connect(begin_second)
		scene.contact(rock, paroi).begin.connect(begin_first)

		scene.contact(reflector_only_player, bullet).begin.connect(begin_first)
		scene.contact(reflector_only_player, bullet_only_shield).begin.connect(begin_first)
		scene.contact(reflector_only_player, bullet_acid).begin.connect(begin_first)
		scene.contact(reflector_only_player, laser).begin.connect(begin_first)
		scene.contact(reflector_only_player, roquette).begin.connect(begin_first)
		scene.contact(reflector_only_player, missile).begin.connect(begin_first)
		scene.contact(reflector_only_player, shield_aggressive).begin.connect(collision_hull_vs_shield_aggressive)
		scene.contact(reflector_only_player, hull_naked).begin.connect(collision_hull_vs_hull)
		scene.contact(reflector_only_player, hull_absorbing_shield).begin.connect(collision_hull_vs_hull)
		scene.contact(reflector_only_player, shield).begin.connect(collision_hull_vs_shield)
		scene.contact(reflector_only_player, shield_ignore_wall).begin.connect(collision_hull_vs_shield)
		scene.contact(reflector_only_player, shield_aggressive).end.connect(collision_ship_ship_aggressive_shield_end_dot)
		scene.contact(reflector_only_player, hull_naked).end.connect(collision_ship_enemy_end_dot)
		scene.contact(reflector_only_player, hull_absorbing_shield).end.connect(collision_ship_enemy_end_dot)
		scene.contact(reflector_only_player, shield).end.connect(collision_ship_enemy_end_dot)
		scene.contact(reflector_only_player, shield_ignore_wall).end.connect(collision_ship_enemy_end_dot)
		scene.contact(reflector_only_player, clear_screen).begin.connect(destroy_second)
		scene.contact(reflector_only_player, harpoon).begin.connect(destroy_second)
		scene.contact(reflector_only_player, reflector_2).begin.connect(destroy_second)
		scene.contact(reflector_only_player, reflector_1).begin.connect(destroy_second)

		scene.contact(reflector_only_enemy, enemy_bullet).begin.connect(begin_first)
		scene.contact(reflector_only_enemy, enemy_bullet_only_shield).begin.connect(begin_first)
		scene.contact(reflector_only_enemy, enemy_bullet_acid).begin.connect(begin_first)
		scene.contact(reflector_only_enemy, enemy_laser).begin.connect(begin_first)
		scene.contact(reflector_only_enemy, enemy_roquette).begin.connect(begin_first)
		scene.contact(reflector_only_enemy, enemy_missile).begin.connect(begin_first)
		scene.contact(reflector_only_enemy, enemy_shield_aggressive).begin.connect(collision_hull_vs_shield_aggressive)
		scene.contact(reflector_only_enemy, enemy_hull_naked).begin.connect(collision_hull_vs_hull)
		scene.contact(reflector_only_enemy, enemy_hull_absorbing_shield).begin.connect(collision_hull_vs_hull)
		scene.contact(reflector_only_enemy, enemy_shield).begin.connect(collision_hull_vs_shield)
		scene.contact(reflector_only_enemy, enemy_shield_ignore_wall).begin.connect(collision_hull_vs_shield)
		scene.contact(reflector_only_enemy, enemy_shield_aggressive).end.connect(collision_ship_ship_aggressive_shield_end_dot)
		scene.contact(reflector_only_enemy, enemy_hull_naked).end.connect(collision_ship_enemy_end_dot)
		scene.contact(reflector_only_enemy, enemy_hull_absorbing_shield).end.connect(collision_ship_enemy_end_dot)
		scene.contact(reflector_only_enemy, enemy_shield).end.connect(collision_ship_enemy_end_dot)
		scene.contact(reflector_only_enemy, enemy_shield_ignore_wall).end.connect(collision_ship_enemy_end_dot)
		scene.contact(reflector_only_enemy, clear_screen).begin.connect(destroy_second)
		scene.contact(reflector_only_enemy, harpoon).begin.connect(destroy_second)

		scene.contact(reflector_1, clear_screen).begin.connect(destroy_second)
		scene.contact(reflector_2, clear_screen).begin.connect(destroy_second)
		scene.contact(reflector_2, harpoon).begin.connect(destroy_second)

		scene.contact(teleporter, wall).begin.connect(begin_first)
		scene.contact(teleporter, item_destructible).begin.connect(begin_first)
		scene.contact(teleporter, item_ignore_enemy).begin.connect(begin_first)
		scene.contact(teleporter, no_blink).begin.connect(begin_second)
		scene.contact(teleporter, clear_screen).begin.connect(begin_second)

		scene.contact(harpoon, item_ignore_enemy).begin.connect(destroy_first)
		scene.contact(hull_naked, item_ignore_enemy).begin.connect(collision_hull_vs_hull)
		scene.contact(hull_naked, item_ignore_enemy).end.connect(collision_ship_enemy_end_dot)
		scene.contact(hull_absorbing_shield, item_ignore_enemy).begin.connect(collision_hull_vs_hull)
		scene.contact(hull_absorbing_shield, item_ignore_enemy).end.connect(collision_ship_enemy_end_dot)
		scene.contact(shield, item_ignore_enemy).begin.connect(collision_hull_vs_shield)
		scene.contact(shield, item_ignore_enemy).end.connect(collision_ship_enemy_end_dot)
		scene.contact(item_ignore_enemy, bullet).begin.connect(bullet_vs_hull)
		scene.contact(item_ignore_enemy, laser).begin.connect(bullet_vs_hull)
		scene.contact(item_ignore_enemy, roquette).begin.connect(missile_vs_hull)
		scene.contact(item_ignore_enemy, missile).begin.connect(missile_vs_hull)
		scene.contact(item_ignore_enemy, plasmaspray).begin.connect(plasmaspray_vs_ship)
		scene.contact(item_ignore_enemy, plasmaspray).end.connect(plasmaspray_vs_ship_end)
		scene.contact(bullet_only_shield, item_ignore_enemy).begin.connect(destroy_second)
		scene.contact(item_ignore_enemy, dot).begin.connect(begin_dot)
		scene.contact(item_ignore_enemy, dot).end.connect(end_dot)
		scene.contact(item_ignore_enemy, dot_qui_poutre).begin.connect(begin_dot_qui_poutre)
		scene.contact(item_ignore_enemy, dot_qui_poutre).end.connect(end_dot_qui_poutre)
		scene.contact(item_ignore_enemy, bullet_acid).begin.connect(destroy_second)
		scene.contact(shield_aggressive, item_ignore_enemy).begin.connect(collision_hull_vs_shield_aggressive)
		scene.contact(shield_aggressive, item_ignore_enemy).end.connect(collision_ship_ship_aggressive_shield_end_dot)
		scene.contact(item_ignore_enemy, destroy_all).begin.connect(destroy_second)
		scene.contact(shield_canceler_2, item_ignore_enemy).begin.connect(cancel_shield_1)
		scene.contact(shield_canceler_1, item_ignore_enemy).begin.connect(cancel_shield_1)

		scene.contact(item_destructible, bullet).begin.connect(bullet_vs_hull)
		scene.contact(item_destructible, laser).begin.connect(bullet_vs_hull)
		scene.contact(item_destructible, roquette).begin.connect(missile_vs_hull)
		scene.contact(item_destructible, missile).begin.connect(missile_vs_hull)
		scene.contact(item_destructible, plasmaspray).begin.connect(plasmaspray_vs_ship)
		scene.contact(item_destructible, plasmaspray).end.connect(plasmaspray_vs_ship_end)
		scene.contact(item_destructible, dot).begin.connect(begin_dot)
		scene.contact(item_destructible, dot).end.connect(end_dot)
		scene.contact(item_destructible, dot_qui_poutre).begin.connect(begin_dot_qui_poutre)
		scene.contact(item_destructible, dot_qui_poutre).end.connect(end_dot_qui_poutre)
		scene.contact(shield_aggressive, item_destructible).begin.connect(collision_hull_vs_shield_aggressive)
		scene.contact(shield_aggressive, item_destructible).end.connect(collision_ship_ship_aggressive_shield_end_dot)
		scene.contact(item_destructible, enemy_bullet).begin.connect(destroy_second)
		scene.contact(item_destructible, enemy_laser).begin.connect(destroy_second)
		scene.contact(item_destructible, enemy_roquette).begin.connect(destroy_second)
		scene.contact(item_destructible, enemy_missile).begin.connect(destroy_second)
		scene.contact(item_destructible, destroy_all).begin.connect(destroy_second)
		scene.contact(shield_canceler_2, item_destructible).begin.connect(cancel_shield_1)
		scene.contact(shield_canceler_1, item_destructible).begin.connect(cancel_shield_1)

		scene.contact(enemy_invincible,hull_naked).begin.connect(collision_hull_vs_wall)
		scene.contact(enemy_invincible,hull_naked).end.connect(collision_ship_wall_end_dot)
		scene.contact(enemy_invincible,hull_absorbing_shield).begin.connect(collision_hull_vs_wall)
		scene.contact(enemy_invincible,hull_absorbing_shield).end.connect(collision_ship_wall_end_dot)
		scene.contact(enemy_invincible,shield).begin.connect(collision_shield_vs_wall)
		scene.contact(enemy_invincible,shield).end.connect(collision_ship_wall_end_dot)
		scene.contact(enemy_invincible,shield_aggressive)
		scene.contact(enemy_invincible,mine_aoe).begin.connect(destroy_first)
		scene.contact(enemy_invincible,mine ).begin.connect(destroy_first)
		scene.contact(enemy_invincible, bullet).begin.connect(destroy_second)
		scene.contact(enemy_invincible,bullet_acid).begin.connect(destroy_second)
		scene.contact(enemy_invincible,bullet_only_shield).begin.connect(destroy_second)
		scene.contact(enemy_invincible, laser).begin.connect(destroy_second)
		scene.contact(enemy_invincible, roquette).begin.connect(destroy_second)
		scene.contact(enemy_invincible, missile).begin.connect(destroy_second)
		//scene.contact(enemy_invincible, harpoon).begin.connect(destroy_first)
		scene.contact(enemy_invincible, destroy_all).begin.connect(destroy_second)
		scene.contact(enemy_invincible, teleporter).begin.connect(begin_first)

		scene.contact(harpoon, item_destructible).begin.connect(destroy_first)
		scene.contact(harpoon, wall).begin.connect(destroy_first)
		scene.contact(harpoon, clear_screen).begin.connect(destroy_second)
		scene.contact(harpoon, destroy_all).begin.connect(destroy_second)

		scene.contact(mine, wall).begin.connect(destroy_first)
		scene.contact(mine, item_destructible).begin.connect(destroy_first)
		scene.contact(mine, item_ignore_enemy).begin.connect(destroy_first)
		scene.contact(mine, mine).begin.connect(destroy_first)
		scene.contact(mine, clear_screen).begin.connect(destroy_second)
		scene.contact(mine, destroy_all).begin.connect(destroy_second)

		scene.contact(mine_aoe, item_destructible).begin.connect(destroy_first)
		scene.contact(mine_aoe, item_ignore_enemy).begin.connect(destroy_first)
		//scene.contact(mine_aoe, mine).begin.connect(destroy_first)

		scene.contact(mine_shockwave, item_destructible).begin.connect(begin_dot)
		scene.contact(mine_shockwave, item_destructible).end.connect(end_dot)
		scene.contact(mine_shockwave, item_ignore_enemy).begin.connect(begin_dot)
		scene.contact(mine_shockwave, item_ignore_enemy).end.connect(end_dot)
		scene.contact(mine_shockwave, mine).begin.connect(destroy_first)

		scene.contact(reflector_1, reflector_1).begin.connect(destroy_both)
		scene.contact(reflector_1, reflector_2).begin.connect(destroy_both)
		scene.contact(reflector_2, reflector_2).begin.connect(destroy_both)
		scene.contact(reflector_1, destroy_all).begin.connect(destroy_second)
		scene.contact(reflector_2, destroy_all).begin.connect(destroy_second)
		scene.contact(reflector_1, clear_screen).begin.connect(destroy_second)
		scene.contact(reflector_2, clear_screen).begin.connect(destroy_second)

		scene.contact(sensor, hull_naked).begin.connect(switch_sensor_on)
		scene.contact(sensor, hull_naked).end.connect(switch_sensor_off)
		scene.contact(sensor, hull).begin.connect(switch_sensor_on)
		scene.contact(sensor, hull).end.connect(switch_sensor_off)
		scene.contact(sensor, hull_naked_aggressive_shield).begin.connect(switch_sensor_on)
		scene.contact(sensor, hull_naked_aggressive_shield).end.connect(switch_sensor_off)
		scene.contact(sensor, hull_absorbing_shield).begin.connect(switch_sensor_on)
		scene.contact(sensor, hull_absorbing_shield).end.connect(switch_sensor_off)

		scene.contact(loot, hull).begin.connect(pick_loot)
		scene.contact(loot, hull_naked_aggressive_shield).begin.connect(pick_loot)
		scene.contact(loot, hull_naked).begin.connect(pick_loot)
		scene.contact(loot, hull_absorbing_shield).begin.connect(pick_loot)
		scene.contact(loot, clear_screen).begin.connect(destroy_second)
		scene.contact(loot, destroy_all).begin.connect(destroy_second)

		scene.contact(checkpoint, hull_naked).begin.connect(checkpoint_on)
		scene.contact(checkpoint, hull).begin.connect(checkpoint_on)
		scene.contact(checkpoint, hull_naked_aggressive_shield).begin.connect(checkpoint_on)
		scene.contact(checkpoint, hull_absorbing_shield).begin.connect(checkpoint_on)
		scene.contact(checkpoint, rock).begin.connect(checkpoint_on)

		scene.contact(enemy_hull_naked, transportable).begin.connect(begin_second)
		scene.contact(enemy_hull, transportable).begin.connect(begin_second)
		scene.contact(enemy_hull_absorbing_shield, transportable).begin.connect(begin_second)
		scene.contact(enemy_hull_naked_aggressive_shield, transportable).begin.connect(begin_second)

		for(var a = enemy_checkpoint_end; a <= enemy_checkpoint; ++a)
		{
			scene.contact(enemy_hull_naked, a).begin.connect(enemy_checkpoint_on)
			scene.contact(enemy_hull, a).begin.connect(enemy_checkpoint_on)
			scene.contact(enemy_hull_absorbing_shield, a).begin.connect(enemy_checkpoint_on)
			scene.contact(enemy_hull_naked_aggressive_shield, a).begin.connect(enemy_checkpoint_on)
		}
	}
	function auto_hull(ship, player)
	{
		var x = player ? 0 : 1
		return	ship.aggressive_shield_active_damages ? x+groups.hull_naked_aggressive_shield :
			ship.absorbtion_damages_multiplier ? x+groups.hull_absorbing_shield :
			(ship.shield >= 1) ? x+groups.hull :
			x+groups.hull_naked
	}
	function auto_shield(ship, player)
	{
		var x = player ? 0 : 1
		return	ship.aggressive_shield_active_damages ? x+groups.shield_aggressive :
			ship.absorbtion_damages_multiplier ? x+groups.shield_absorbtion :
			(ship.shield >= 1) ? x+groups.shield :
			0
	}
}
