#include "Generator.h"
#include "meta.private.h"

namespace aw {
namespace game {

AW_DEFINE_OBJECT_STUB(Generator)
AW_DEFINE_PROPERTY_STORED(Generator, energy_regeneration)

}
}
