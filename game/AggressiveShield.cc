#include "AggressiveShield.h"
#include "meta.private.h"

namespace aw {
namespace game {

AW_DEFINE_OBJECT_STUB(AggressiveShield)
AW_DEFINE_PROPERTY_STORED(AggressiveShield, damages)

}
}
