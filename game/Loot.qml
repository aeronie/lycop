import QtQuick 2.0 as Q
import aw.game 0.0

Bullet
{
	position: body.position
	angle: Math.random() * Math.PI * 2
	Q.Component.onCompleted: velocity = direction_to_scene(Qt.point(0, Math.random() * 10 * body.scale))
	range: scene.seconds_to_frames(40)
	damages: 50
	scale: 0.5
	angular_velocity: Math.random() * 20 - 10
	damping: 10
	angular_damping: 10
	Image
	{
		material: "loot"
	}
	CircleCollider
	{
		group: groups.loot
	}
}
