import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-3/chapter-3.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property var root_tab_weapons:[aggressive_shield,missile_launcher_factory,coil_gun_factory,vortex_gun_factory,quipoutre_factory]
	property alias ship: ship
	property bool is_sun: true
	property var info_print
	signal one_boss_destroyed
	signal concentration_done
	signal screen_cleared
	property bool less_400: true
	property var camera_position: Qt.point(0, 0)
	property real hideleo_x: 600
	property var hideleo_phase_animation
	property bool save_boss : saved_game.self.get("save_boss_3", false)
	property bool mod_easy : saved_game.self.get("mod_easy", false)
	property real proximity_hideleo: ship ? hideleo_x - ship.position.x : hideleo_x
	property bool request_patrols: false
	property var boss
	property var boss_animation
	property var animation_tp
	property var sound
	property int spawn: 0

	//j'ai rajouté le ship.position sinon le binding ne fonctionnait pas
	property real spawn_enemy: ship ? ship.position ? Math.max(30,3 * ship.get_norm_velocity()) : 30 : 30

	Q.Component
	{
		id: aggressive_shield
		AggressiveShield
		{
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: missile_launcher_factory
		MissileLauncher
		{
			level: 2
		}
	}
	Q.Component
	{
		id: coil_gun_factory
		CoilGun
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: vortex_gun_factory
		VortexGun
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}

	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	function next_checkpoint_position() {return Qt.point(ship.position.x + (Math.random()+3)*(root.random_sign())*(10), ship.position.y + (Math.random()+3)*(root.random_sign())*(10))}

	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false
		Vehicle
		{
			id: fake_hideleo
			property bool alive: root.is_sun
			onAliveChanged: if(!alive) destroy()

			position.x: ship ? ship.position.x + 100 : 100
			position.y: 20
			icon: 5
			icon_color: "Red"
		}
		Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Sensor
		{
			id: timer_patrols
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(6)
			target_value: !value && request_patrols
		}
		Sensor
		{
			id: timer_eruption
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(8)
			target_value: !value && request_patrols
		}
		Ship
		{
			id: ship

			//distance du vaisseau par rapport au soleil (entre 0 et 1 par rapport à la scale du soleil)
			property real distance_sun: root.is_sun ? (ship.position.y - sun.position.y - sun.scale * sun.limit) / sun.scale : 1
			angle: Math.PI / 2
			speed_boost_mul: distance_sun < 1 ? (1 + 2 * (1 - distance_sun)) * (1 + 2 * (1 - distance_sun)) : 1

			Q.Component.onDestruction: game_over = true
		}
		Background
		{
			id: sun
			y: 1
			z: -0.0001
			scale: 30
			position.y: -50
			property real limit: 0.19
			property bool alive: root.is_sun
			onAliveChanged: if(!alive) destroy()
			shift_x: 0
			material: "chapter-3/sun"
			PolygonCollider
			{
				vertexes: [Qt.point(5,sun.limit), Qt.point(-5,sun.limit), Qt.point(-5,-5), Qt.point(5,-5)]
				group: groups.destroy_all
			}
		}
		Animation
		{
			id: hideleo_move
			time: 0.1
			speed: -1
			property bool alive: root.is_sun
			onAliveChanged: if(!alive) destroy()

			onTimeChanged:
			{
				if(ship)
				{
					root.hideleo_x += 50 * ship.scene.time_step
				}
			}
		}

		Animation
		{
			id: sun_dmg
			time: 0.1
			speed: -1
			property bool alive: root.is_sun
			onAliveChanged: if(!alive) destroy()

			onTimeChanged:
			{
				var dmg_global = 0
				if(ship)
				{
					if(ship.distance_sun < 1 )
					{
						dmg_global = 100 * (1 - ship.distance_sun) * ship.scene.time_step
					}
					if(dmg_global > 0)
					{
						dmg_global = ship.damage_shield(dmg_global)
						if(dmg_global > 0)
						{
							ship.damage_global(dmg_global,false)
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: clear_screen_timer
		Animation
		{
			id: clear_screen
			time: 0.5
			speed: -1
			property bool alive: true
			onAliveChanged: if(!alive) destroy()

			onTimeChanged:
			{
				if(time < 0)
				{
					root.screen_cleared()
					clear_screen.alive = false
				}
			}
		}
	}
	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("Hideleo: %1 km").arg(Math.max(Math.floor(root.proximity_hideleo),0))
			style: Q.Text.Outline
			styleColor: "#000"
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}
	Q.Component
	{
		id: eruption_factory
		Body
		{
			id: eruption
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)}
			position.y: -43.5+15-1 /* limite du soleil + scale de l'eruption - ajustement pour la texture*/
			position.x: 0
			scale: 15
			type: Body.STATIC
			angle:Math.PI
			icon: 2
			icon_color: "Black"
			property real damages: 10000 * scene.time_step
			property bool alive: root.is_sun
			property int indice_0: 0
			property real variation_0: 0
			property int indice: (Math.floor(scene.time/2) - indice_0)%45
			property real variation: ((scene.time/2) - variation_0)%45
			property real utility: ship ? ship.position.x - eruption.position.x : 0
			onAliveChanged: if(!alive) destroy()
			onUtilityChanged:
			{
				if(utility > 60)
				{
					eruption.destroy()
				}
			}
			Q.Component.onCompleted:
			{
				indice_0 = Math.floor(root.scene.time/2) % 45
				variation_0 = root.scene.time/2 % 45
			}
			Image
			{
				id: image
				material: "chapter-3/eruption/" + indice
				mask: Qt.rgba(1, 1, 1, (variation) > 29 ? 1-((variation-30)/14) : 1)
				z: 0.003
			}
			CircleCollider
			{
				group: groups.enemy_dot
				position: coords(512,860+582*(1-variation/45))
				radius: (512-100)/512
				sensor: true
			}
		}
	}
	Q.Component
	{
		id: space_door_factory
		Body
		{
			id: space_door
			scale: 15
			type: Body.STATIC
			property int indice: 0
			property color mask: "white"
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real z: 0
			Image
			{
				id: image
				material: "chapter-3/door/" + indice
				z: space_door.z
				mask: space_door.mask
			}
		}
	}
	Q.Component
	{
		id: space_door_swap_factory
		Body
		{
			id: space_door
			scale: player ? 6 : 15
			type: Body.STATIC
			property bool player: true
			position: player ? (ship ? ship.position : Qt.point(0, 0)) : (root.boss ? root.boss.position : Qt.point(0, 0))
			angle: root.boss ? root.boss.angle - Math.PI / 2 : 0
			property int indice: 0
			property color mask: "white"
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real z: 0
			Image
			{
				id: image
				material: "chapter-3/door/" + indice
				z: space_door.z
				mask: space_door.mask
			}
		}
	}

	Q.Component
	{
		id: tp_ship_factory
		Animation
		{
			id: timer
			property var hideleo: root.boss
			property var sound_hideleo: root.sound
			property var door
			property var animation_apparition
			property real value:0
			property real sens: 1
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				var step = 0.05;

				if(timer.hideleo)
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door=space_door_factory.createObject(null, {scene: ship.scene, position: Qt.point(ship.position.x + 30, ship.position.y ), z: altitudes.boss - 0.00001, angle: 0 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 24)
						{
							reapp_beg = false
							animation_apparition = space_door_factory.createObject(null, {scene: door.scene, position: Qt.point(door.position.x, door.position.y), z: altitudes.boss + 0.001, angle : door.angle, indice: 24 })
							hideleo.position = door.position
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						var a = animation_apparition.mask.a
						a = a - 0.05
						if(a <= 0)
						{
							a = 0
							reapp_end = true
						}
						animation_apparition.mask.a = a
						if(reapp_end)
						{
							animation_apparition.alive = false
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							hideleo.start = true
							door.alive = false
							timer.alive = false
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: hideleo_turret_animation_1
		Animation
		{
			id: timer
			property var hideleo: root.boss
			property var sound_hideleo: root.sound
			property var vortex_angle_ini: Math.PI
			property var door_ship
			property var door_disp_ship
			property var door_disp_hideleo
			property var door_hideleo
			property real value:0
			property real sens: 1
			property real swap_time: 5 // avec l'animation rajoute environ 1 sec
			property bool on_swaping: false
			property bool swap_ini: true
			property bool spaw_anim_beg: false
			property bool spaw_anim_mid: false
			property bool spaw_anim_mid_swap: false
			property bool reapp: false
			property bool spaw_anim_end: false

			property real aggressive: 7
			property real last_swap: 7
			time: 5
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					if(door_ship)
					{
						door_ship.alive = false
					}
					if(door_disp_ship)
					{
						door_disp_ship.alive = false
					}
					if(door_hideleo)
					{
						door_hideleo.alive = false
					}
					if(door_disp_hideleo)
					{
						door_disp_hideleo.alive = false
					}
					destroy()
				}
			}

			onTimeChanged:
			{
				var step = 0.05;

				if(timer.hideleo)
				{
					//spam
					timer.value = timer.value + timer.sens*step
					if(timer.value > 1 || timer.value < -1)
					{
						timer.sens= -1 * timer.sens
					}
					hideleo.angle_turret_vortex = timer.vortex_angle_ini + Math.PI/4 * timer.value
					hideleo.angle_turret_vortex_2 = timer.vortex_angle_ini - Math.PI/4 * timer.value
					hideleo.angle_front_quipoutre = timer.vortex_angle_ini + Math.PI/3 * timer.value

					//swap
					if(last_swap - time > swap_time - 0.2)
					{
						sound_hideleo.play = true
					}
					if(last_swap - time > swap_time)
					{
						if(swap_ini)
						{
							door_ship = space_door_swap_factory.createObject(null, {scene: ship.scene, z: altitudes.boss - 0.000000001})
							door_hideleo = space_door_swap_factory.createObject(null, {scene: hideleo.scene, player : false, z: altitudes.boss - 0.000000001})
							door_disp_ship = space_door_swap_factory.createObject(null, {scene: door_ship.scene, z: altitudes.boss + 0.01, indice: 10, mask: Qt.rgba(1, 1, 1, 0) })
							door_disp_hideleo = space_door_swap_factory.createObject(null, {scene: door_hideleo.scene, player : false, z: altitudes.boss + 0.01, indice: 24, mask: Qt.rgba(1, 1, 1, 0) })
							spaw_anim_beg = true
							swap_ini = false
						}

						//ouverture des portes
						if(spaw_anim_beg)
						{
							var indice = door_hideleo.indice
							indice++
							if(indice < 11)
							{
								door_ship.indice = indice
							}
							if(indice < 25)
							{
								door_hideleo.indice = indice
							}
							else
							{
								spaw_anim_mid = true
								spaw_anim_beg = false
							}
						}

						//disparition partielle et swap et reapparition
						if(spaw_anim_mid)
						{
							if(!spaw_anim_mid_swap && !reapp)
							{
								var a = door_disp_ship.mask.a
								a = a + 0.05
								if(a > 0.5)
								{
									a = 0.5
									spaw_anim_mid_swap = true
								}
								door_disp_ship.mask.a = a
								door_disp_hideleo.mask.a = a
							}
							if(!spaw_anim_mid_swap && reapp)
							{
								var a = door_disp_ship.mask.a
								a = a - 0.05
								if(a <= 0)
								{
									a = 0
									spaw_anim_mid = false
									reapp = false
									spaw_anim_end = true
								}
								door_disp_ship.mask.a = a
								door_disp_hideleo.mask.a = a
							}
							if(spaw_anim_mid_swap)
							{
								//echange de position + deplacer de 60 en x
								var hpositionx = hideleo.position.x
								var hpositiony = hideleo.position.y
								hideleo.position = Qt.point(ship.position.x, ship.position.y)
								ship.position.x = hpositionx + 0.05 * (hideleo.position.x - hpositionx)
								ship.position.y = hpositiony + 0.05 * (hideleo.position.y - hpositiony)
								spaw_anim_mid_swap = false
								reapp = true
								on_swaping = true
								aggressive = time
							}
						}

						//fermeture des portes
						if(spaw_anim_end)
						{
							var indice = door_hideleo.indice
							indice--
							if(indice < 11)
							{
								door_ship.indice = indice
							}
							if(indice < 25)
							{
								door_hideleo.indice = indice
							}
							if(indice == 0)
							{
								last_swap = time
								door_disp_ship.alive = false
								door_disp_hideleo.alive = false
								door_ship.alive = false
								door_hideleo.alive = false
								spaw_anim_end= false
								swap_ini = true
							}
						}
					}

					if(on_swaping)
					{
						if(aggressive - time > 1)
						{
							on_swaping = false
							sound_hideleo.play = false
						}
						hideleo.swap_shoot = on_swaping
						hideleo.max_angular_speed = 0
					}
					else
					{
						hideleo.max_angular_speed = 5
					}
				}
				else
				{
					destroy()
				}
			}
		}
	}
	Q.Component
	{
		id: explosion_factory
		Explosion
		{
			scale: 4
		}
	}
	Q.Component
	{
		id: hideleo_phase_animation_factory
		Animation
		{
			id: timer
			function next_boom_position()
			{
				var a = 2 * Math.PI * Math.random()
				var b = Math.random() * 7
				return Qt.point(b * Math.cos(a), b * Math.sin(a))
			}
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var hideleo: root.boss
			property var door
			property var animation_apparition
			property bool swap: true
			property bool earthquake: true
			property bool door_ini: true
			property bool door_beg: false
			property bool door_end: false
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false

			property bool flee: true
			property real last_boom: 7
			time: 7
			speed: -1

			onTimeChanged:
			{
				var explosion_step = 0.1
				if(earthquake)
				{
					var hideleo_x = timer.hideleo.position.x
					var hideleo_y = timer.hideleo.position.y
					hideleo_x += swap ? -0.1 : 0.1
					hideleo_y += swap ? -0.1 : 0.1
					swap= !swap
					hideleo.position = Qt.point(hideleo_x, hideleo_y)
					if(last_boom - time > explosion_step)
					{
						last_boom = time
						explosion_factory.createObject(null, {scene: hideleo.scene, position: Qt.point(hideleo_x + timer.next_boom_position().x, hideleo_y + timer.next_boom_position().y)})
					}
				}
				if(time < 3 && flee)
				{
					earthquake = false

					//ouverture porte
					if(door_beg)
					{
						door.indice++
						if(door.indice == 24)
						{
							door_beg = false
						}
					}

					//disparition
					if(!door_beg && !door_end && !door_ini)
					{
						var a = hideleo.mask.a
						a = a - 0.05
						if(a <= 0)
						{
							a = 0
							door_end = true
						}
						hideleo.mask.a = a
					}

					//fermeture porte
					if(door_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							door_end = false
							door.alive = false
							hideleo.alive = false
							flee = false
						}
					}

					if(door_ini)
					{
						door_ini = false
						door = space_door_factory.createObject(null, {scene: hideleo.scene, position: Qt.point(hideleo.position.x, hideleo.position.y), z: altitudes.boss - 0.000000001, angle: hideleo.angle - Math.PI / 2 })
						door_beg = true
					}
				}
				if(!flee)
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door=space_door_factory.createObject(null, {scene: ship.scene, position: Qt.point(ship.position.x, ship.position.y - 10), z: altitudes.boss - 0.00001, angle: 0 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 24)
						{
							reapp_beg = false
							animation_apparition = space_door_factory.createObject(null, {scene: door.scene, position: Qt.point(door.position.x, door.position.y), z: altitudes.boss + 0.01, angle : door.angle, indice: 24 })
							root.boss = hideleo_factory_2.createObject(null, {scene: door.scene, position: Qt.point(door.position.x, door.position.y), angle: Math.PI / 2, z: altitudes.boss })
							hideleo = root.boss
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						var a = animation_apparition.mask.a
						a = a - 0.05
						if(a <= 0)
						{
							a = 0
							reapp_end = true
						}
						animation_apparition.mask.a = a
						if(reapp_end)
						{
							animation_apparition.alive = false
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							hideleo.begin = true
							hideleo.activate = true
							root.boss_animation = hideleo_behaviour_2.createObject(null, {parent: root.scene})
							door.alive = false
							timer.alive = false
						}
					}
				}
			}
		}
	}
	Q.Component
	{
		id: armagedon_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: "transparent"
			scale: 0.01
			property real z: 0.01
			Q.Component.onCompleted: sound.play()
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 5000
			}
		}
	}
	Q.Component
	{
		id: hideleo_phase_animation_2_factory
		Animation
		{
			id: timer
			function next_boom_position()
			{
				var a = 2 * Math.PI * Math.random()
				var b = Math.random() * 7
				return Qt.point(b * Math.cos(a), b * Math.sin(a))
			}
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var hideleo: root.boss
			property bool swap: true
			property bool earthquake: true
			property bool armagedon_ini: true
			property bool hideleo_destroyed: true
			property var armagedon

			property real last_boom: 7
			time: 7
			speed: -1

			onTimeChanged:
			{
				var explosion_step = 0.1
				if(earthquake)
				{
					var hideleo_x = timer.hideleo.position.x
					var hideleo_y = timer.hideleo.position.y
					hideleo_x += swap ? -0.1 : 0.1
					hideleo_y += swap ? -0.1 : 0.1
					swap= !swap
					hideleo.position = Qt.point(hideleo_x, hideleo_y)
					if(last_boom - time > explosion_step)
					{
						last_boom = time
						explosion_factory.createObject(null, {scene: hideleo.scene, position: Qt.point(hideleo_x + timer.next_boom_position().x, hideleo_y + timer.next_boom_position().y), scale: 8})
					}
				}
				if(time < 4.65)
				{
					earthquake = false
					if(armagedon_ini)
					{
						armagedon = armagedon_factory.createObject(null, {scene: ship.scene, position: Qt.point(timer.hideleo.position.x, timer.hideleo.position.y), mask_c: "white"})
						armagedon_ini = false
					}
					if(armagedon.scale < 80)
					{
						armagedon.scale += 30 * hideleo.scene.time_step
					}
					else
					{
						if(hideleo_destroyed)
						{
							hideleo.alive = false
							hideleo_destroyed = false
							root.boss = hideleo_sun_destroyer_factory.createObject(null, {scene: ship.scene, position: Qt.point(timer.hideleo.position.x, timer.hideleo.position.y)})
							root.spawn = 0
						}
						var a = armagedon.mask_c.a - 1 * ship.scene.time_step
						if(a<0)
						{
							a=0
							root.boss_animation = hideleo_behaviour_3.createObject(null, {parent: root.scene})
							timer.alive=false
							//fin
						}
						armagedon.mask_c.a = a
					}
				}
			}
		}
	}

	Q.Component
	{
		id: hideleo_behaviour_2
		Animation
		{
			id: timer
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var hideleo: root.boss
			property bool dashing: false

			//obligé de tester hideleo.change_phase sinon je prends une erreur à la destruction du hideleo
			property bool change_phase: hideleo ? (hideleo.change_phase ? hideleo.change_phase : false) : false

			property bool first: false
			property real dist_dash: 20
			property real sens: 1
			property real dist_dash_current: 20
			property real time_run: 2
			property real time_dash: 5
			property real last_dash: 7
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(!change_phase)
				{
					if(!dashing)
					{
						//rotation
						hideleo.angle += Math.PI * 2 / hideleo.scene.seconds_to_frames(8)
						if(last_dash - time > time_dash)
						{
							dashing = true
							dist_dash_current = dist_dash
							first = true
						}
					}
					else
					{
						var x_varia = Math.cos(hideleo.angle) * 0.5
						var y_varia = Math.sin(hideleo.angle) * 0.5
						if(first)
						{
							sens = 1
							//produit scalaire entre le vecteur normal et le vecteur position du vaisseau relatif au hideleo
							if(x_varia * (ship.position.x - hideleo.position.x) + y_varia * (ship.position.y - hideleo.position.y) < 0)
							{
								sens = -1
							}
							first = false
						}
						hideleo.position.x += sens * x_varia
						hideleo.position.y += sens * y_varia
						dist_dash_current -= Math.sqrt(x_varia * x_varia + y_varia * y_varia)
						if(dist_dash_current < 0)
						{
							dashing = false
							last_dash = time
						}
					}
				}
				else
				{
					if(!dashing)
					{
						//rotation
						if(last_dash - time > time_dash)
						{
							dashing = true
							dist_dash_current = time
							hideleo.max_speed = 0
						}
					}
					else
					{
						if(ship)
						{
							hideleo.position.x += 2 * (ship.position.x - hideleo.position.x) * hideleo.scene.time_step
							hideleo.position.y += 2 * (ship.position.y - hideleo.position.y) * hideleo.scene.time_step
							if(dist_dash_current - time > time_run)
							{
								dashing = false
								last_dash = time
								hideleo.max_speed = 10
							}
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: tp_enemy_factory
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property real value: 0
			property real enemy_count: 3
			property var position_enemy
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property real last_spawn: 7
			property real time_spawn: 1

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: 4 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						if(new_enemy)
						{
							var ind_weap = 1 + random_index(4)
							enemy = drone_factory.createObject(null, {scene: scene, position: Qt.point(door.position.x, door.position.y), angle: Math.atan2(ship.position.y - door.position.y, ship.position.x - door.position.x ), indice_weapon : ind_weap, utility: 0})
							new_enemy = false
							last_spawn = time
							enemy_count--
						}
						else
						{
							if(enemy_count == 0)
							{
								reapp_end = true
							}
							else
							{
								if(last_spawn - time > time_spawn)
								{
									new_enemy = true
								}
							}
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							timer.alive = false
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: hideleo_behaviour_3
		Animation
		{
			id: timer
			function door_pos()
			{
				var res = Qt.point(0,0)
				if(door)
				{
					res = door.position
				}
				return res
			}

			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var hideleo: root.boss
			property var concentration
			property var boom
			property var door
			property var door_2
			property var door_anim
			property var door_anim_2
			property bool boom_available: true
			property bool concentration_available: true
			property bool spawn: true
			property bool tp: false
			property bool first: true
			property bool swap: true
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property bool arene_ini: true
			property real time_spawn: 2
			property real last_spawn: 7

			property real time_boom: 2 * time_arene / 3
			property real time_arene: 10
			property real deb_arene: 7
			time: 7
			speed: -1

			Q.Component.onDestruction:
			{
				if(concentration)
				{
					concentration.alive= false
				}
				if(boom)
				{
					boom.alive=false
				}
				if(door)
				{
					door.alive=false
				}
				if(door_2)
				{
					door_2.alive=false
				}
				if(door_anim)
				{
					door_anim.alive= false
				}
				if(door_anim_2)
				{
					door_anim.alive= false
				}
			}

			onTimeChanged:
			{
				if(spawn)
				{
					if(first)
					{
						tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(ship.position.x + 10, ship.position.y + 5)})
						tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(ship.position.x - 10, ship.position.y + 5)})
						tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(ship.position.x + 10, ship.position.y - 5)})
						tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(ship.position.x - 10, ship.position.y - 5)})
						tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(ship.position.x, ship.position.y + 15)})
						tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(ship.position.x , ship.position.y - 15)})
						first = false
						last_spawn = time
					}
					else
					{
						if(last_spawn - time > time_spawn)
						{
							if(root.spawn == 0)
							{
								spawn = false
								first = true
								tp = true
								swap = true
							}
						}
					}
				}
				else
				{
					if(tp)
					{
						//initialisation
						if(reapp_ini)
						{
							reapp_ini = false
							door = space_door_swap_factory.createObject(null, {scene: ship.scene, z: altitudes.boss - 0.000001, angle: 0})

							var res_x = hideleo.position.x
							var res_y = hideleo.position.y
							var angle = 2 * Math.random() * Math.PI
							res_x += Math.cos(angle) * 15
							res_y += Math.sin(angle) * 15
							door_2 = space_door_factory.createObject(null, {scene: root.scene, position: Qt.point(res_x,res_y),z: altitudes.boss - 0.000001, angle : 0, scale: 6 })

							reapp_beg=true
						}

						//ouverture des portes
						if(reapp_beg)
						{
							door.indice++
							door_2.indice++
							if(door.indice == 10)
							{
								door_anim = space_door_swap_factory.createObject(null, {scene: root.scene, z: altitudes.boss - 0.000001, angle : 0, indice: 10 })
								door_anim.mask.a = 0

								door_anim_2 = space_door_swap_factory.createObject(null, {scene: ship.scene, position: door_2.position, z: altitudes.boss - 0.000001, angle : 0, scale : 6, indice: 10 })
								door_anim_2.mask.a = 1

								reapp_beg = false
							}
						}

						//disparition
						if(!reapp_beg && !reapp_end && !reapp_ini)
						{
							var a = door_anim.mask.a
							a = a + 0.05
							if(a >= 0.5 && swap)
							{
								door.position = ship.position
								door_anim.position = ship.position
								ship.position = door_2.position
								swap = false
								hideleo.invincible = false
								hideleo.mask_barriere_a = 1
								boom_available = true
								concentration_available = true
								deb_arene = time
							}
							if(a>=1)
							{
								a = 1
								reapp_end = true
							}

							door_anim.mask.a = a
							door_anim_2.mask.a = 1 - a
							if(reapp_end)
							{
								door_anim_2.alive = false
								door_anim.alive = false
							}
						}

						//fermeture porte
						if(reapp_end)
						{
							door.indice--
							door_2.indice--
							if(door.indice == 0)
							{
								reapp_end = false
								door.alive = false
								door_2.alive = false
								tp = false
								reapp_ini = true
								arene_ini = true
							}
						}
					}
					else
					{
						if(arene_ini && deb_arene - time > 1)
						{
							arene_ini = false
							hideleo.shooting = true
						}

						var a = 0
						if(time < deb_arene) a = 1 - (deb_arene - time) / time_arene
						if(a < 0)
						{
							a = 0
						}
						
						hideleo.mask_barriere_a = a

						if(deb_arene - time > time_boom - 72/root.scene.seconds_to_frames(1))
						{
							if(concentration_available)
							{
								concentration_available = false
								concentration=concentration_factory.createObject(null, {scene: root.scene, position: hideleo.position})
							}
						}

						if(deb_arene - time > time_boom)
						{
							if(concentration)
							{
								concentration.alive = false
							}
							if(boom_available)
							{
								boom_available = false
								boom=hideleo_aoe_factory.createObject(null, {scene: root.scene, position: hideleo.position ,z: altitudes.boss})
							}
							else
							{
								boom.scale += 9/(time_arene-time_boom) * root.scene.time_step
								boom.angle += 0.01
							}
						}

						if(deb_arene - time > time_arene)
						{
							boom.alive= false
							hideleo.invincible = true
							hideleo.shooting = false
							hideleo.mask_barriere_a = 0
							spawn = true
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: hideleo_phase_animation_last_factory
		Animation
		{
			id: timer
			function next_boom_position()
			{
				var a = 2 * Math.PI * Math.random()
				var b = Math.random() * 7
				return Qt.point(b * Math.cos(a), b * Math.sin(a))
			}
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var hideleo: root.boss
			property bool swap: true
			property bool earthquake: true
			property bool dialogue_ini: true
			property bool concentration: false

			function calcul_hideleo_angle()
			{
				var result = hideleo.angle
				while(result < -Math.PI)
				{
					result += 2 * Math.PI
				}
				while(result > Math.PI)
				{
					result -= 2 * Math.PI
				}
				return result
			}

			Q.Component.onDestruction:
			{
				if(concentration_1)
				{
					concentration_1.alive = false
				}
				if(concentration_2)
				{
					concentration_2.alive = false
				}
			}

			property var concentration_1
			property var concentration_2
			property int nb_concentration: 4
			property real time_concentration: 36/root.scene.seconds_to_frames(1)
			property real last_concentration: 7

			property real last_boom: 7
			time: 7
			speed: -1

			onTimeChanged:
			{
				var explosion_step = 0.1
				if(earthquake)
				{
					var hideleo_x = timer.hideleo.position.x
					var hideleo_y = timer.hideleo.position.y
					hideleo_x += swap ? -0.1 : 0.1
					hideleo_y += swap ? -0.1 : 0.1
					swap= !swap
					hideleo.position = Qt.point(hideleo_x, hideleo_y)
					if(last_boom - time > explosion_step)
					{
						last_boom = time
						explosion_factory.createObject(null, {scene: hideleo.scene, position: Qt.point(hideleo_x + timer.next_boom_position().x, hideleo_y + timer.next_boom_position().y), scale: 4})
					}
				}
				if(time < 4.65 && !concentration)
				{
					earthquake = false
					if(dialogue_ini)
					{
						one_boss_destroyed()
						dialogue_ini = false
						hideleo.angle = calcul_hideleo_angle()
					}
					if(hideleo.angle > 0)
					{
						hideleo.angle -= 0.05
					}
					else
					{
						hideleo.angle += 0.05
					}
					if(hideleo.angle <= 0.05 && hideleo.angle >= -0.05 )
					{
						hideleo.angle = 0
						concentration = true
					}
				}
				if(concentration)
				{
					if(last_concentration - time > time_concentration)
					{
						if(concentration_1)
						{
							concentration_1.alive= false
						}
						if(concentration_2)
						{
							concentration_2.alive= false
						}
						last_concentration = time
						nb_concentration--
						concentration_1 = concentration_factory.createObject(null, {scene: root.scene, position: hideleo.coords_concentration_1, scale : 1, z : altitudes.boss + 0.0000001, mask: Qt.rgba(58, 140, 204, 1)})
						concentration_2 = concentration_factory.createObject(null, {scene: root.scene, position: hideleo.coords_concentration_2, scale : 1, z : altitudes.boss + 0.0000001, mask: Qt.rgba(58, 140, 204, 1)})
					}
					if(nb_concentration == 0)
					{
						concentration_1.indice = 3
						concentration_2.indice = 3
						concentration_done()
					}
				}
			}
		}
	}

	Q.Component
	{
		id: hideleo_transition_factory
		Vehicle
		{
			id: hideleo
			scale: 12
			angle: Math.PI
			icon: 4
			icon_color: "red"
			property color mask: "white"
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			Image
			{
				id: image
				material: "chapter-3/hideleo"
				z: altitudes.boss
				mask: hideleo.mask
			}
		}
	}

	Q.Component
	{
		id: hideleo_sun_destroyer_transition_factory
		Vehicle
		{
			id: hideleo
			scale: 3
			angle: 0
			icon: 4
			icon_color: "red"
			property color mask: Qt.rgba(1, 0, 0, 1)
			property color mask_shield: Qt.rgba(0.58, 0, 0.82, 1)
			property bool alive: true
			type: Body.KINEMATIC
			property var coords_concentration_1: Qt.point(hideleo.position.x + hideleo.scale * (235 / 256 - 1), hideleo.position.y + hideleo.scale * -1)
			property var coords_concentration_2: Qt.point(hideleo.position.x + hideleo.scale * (275 / 256 - 1), hideleo.position.y + hideleo.scale * -1)

			onAliveChanged: if(!alive) destroy()
			Image
			{
				id: image
				material: "chapter-3/sun-destroyer"
				z: altitudes.boss
				mask: hideleo.mask
			}

			Image
			{
				material: "equipments/shield"
				mask: hideleo.mask_shield
				scale: 1.1
				z: altitudes.shield
			}

			CircleCollider
			{
				group: groups.enemy_invincible
				radius: 1.1
			}
		}
	}

	Q.Component
	{
		id: concentration_factory
		Body
		{
			id: concentration
			scale: 16
			property bool alive: true
			property int indice_0: 0
			property int indice: (Math.floor(scene.time/2) - indice_0)
			onAliveChanged: if(!alive) destroy()
			property real z: altitudes.boss - 0.0000001
			property color mask: "white"

			Sound
			{
				id: sound_shooting
				objectName: "chapter-3/power_charging"
				scale: 10
			}

			Q.Component.onCompleted:
			{
				indice_0 = Math.floor(root.scene.time/2)
				sound_shooting.play()
			}
			Image
			{
				id: image
				material: indice > 35 ? "chapter-3/concentration/35" : "chapter-3/concentration/" + indice
				z: concentration.z
				mask: concentration.mask
			}
		}
	}

	Q.Component
	{
		id: hideleo_aoe_factory
		Body
		{
			id: boom
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real damages: 10000 * scene.time_step
			property real radius: 1.9
			scale: 1
			property color mask: "white"

			property var interieur
			Q.Component.onCompleted:
			{
				interieur = hideleo_int_factory.createObject(null, {scene: root.scene,position: boom.position ,scale: scale, angle: 0})

				sound_shooting.play()
			}

			onScaleChanged:
			{
				interieur.scale = scale
				collider.update_transform()
			}
			onAngleChanged: interieur.angle = -angle

			Q.Component.onDestruction:
			{
				interieur.alive = false
			}

			Sound
			{
				id: sound_shooting
				objectName: "chapter-3/aoe_of_death"
				scale: 10
			}
			Image
			{
				material: "chapter-3/explosion-hideleo/ext"
				mask: boom.mask
				position: Qt.point(-1,-1)
				z: altitudes.boss - 0.000002
			}
			Image
			{
				material: "chapter-3/explosion-hideleo/ext"
				mask: boom.mask
				angle: Math.PI/2
				position: Qt.point(1,-1)
				z: altitudes.boss - 0.000002
			}
			Image
			{
				material: "chapter-3/explosion-hideleo/ext"
				mask: boom.mask
				angle: Math.PI
				position: Qt.point(1,1)
				z: altitudes.boss - 0.000002
			}
			Image
			{
				material: "chapter-3/explosion-hideleo/ext"
				mask: boom.mask
				angle: -Math.PI/2
				position: Qt.point(-1,1)
				z: altitudes.boss - 0.000002
			}

			//colision du bouclier etendu
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				radius: boom.radius
				sensor: true
			}
		}
	}
	Q.Component
	{
		id: hideleo_int_factory
		Body
		{
			id: boom
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			scale: 1
			property color mask: "white"

			Image
			{
				material: "chapter-3/explosion-hideleo/int"
				mask: boom.mask
				position: Qt.point(-1,-1)
				z: altitudes.boss - 0.000001
			}
			Image
			{
				material: "chapter-3/explosion-hideleo/int"
				mask: boom.mask
				angle: Math.PI/2
				position: Qt.point(1,-1)
				z: altitudes.boss - 0.000001
			}
			Image
			{
				material: "chapter-3/explosion-hideleo/int"
				mask: boom.mask
				angle: Math.PI
				position: Qt.point(1,1)
				z: altitudes.boss - 0.000001
			}
			Image
			{
				material: "chapter-3/explosion-hideleo/int"
				mask: boom.mask
				angle: -Math.PI/2
				position: Qt.point(-1,1)
				z: altitudes.boss - 0.000001
			}
		}
	}

	Q.Component
	{
		id: barrier_factory
		Body
		{
			id: quater_barrier
			function coords(x, y) {return Qt.point((x / 512 - 1), (y / 512 - 1))} // plink fait des images de boss en 1024*1024
			property bool alive: true
			type: Body.KINEMATIC
			onAliveChanged: if(!alive) destroy()
			property bool active: root.boss ? !root.boss.invincible : false
			property int group: active? groups.enemy_invincible : 0
			property color mask: Qt.rgba(0.58, 0, 0.82, 0)

			Image
			{
				material: "chapter-3/circle-barrier"
				mask: quater_barrier.mask
				z: altitudes.shield
			}
			//colision du bouclier etendu
			PolygonCollider
			{
				vertexes: [coords(131,1024),coords(22,1024),coords(37,864),coords(142,864)]
				group: quater_barrier.group
			}
			PolygonCollider
			{
				vertexes: [coords(142,864),coords(37,864),coords(77,700),coords(189,700)]
				group: quater_barrier.group
			}
			PolygonCollider
			{
				vertexes: [coords(189,700),coords(77,700),coords(140,550),coords(263,550)]
				group: quater_barrier.group
			}
			PolygonCollider
			{
				vertexes: [coords(263,550),coords(140,550),coords(215,440),coords(344,440)]
				group: quater_barrier.group
			}
			PolygonCollider
			{
				vertexes: [coords(344,440),coords(215,440),coords(272,365),coords(421,365)]
				group: quater_barrier.group
			}
			PolygonCollider
			{
				vertexes: [coords(421,365),coords(272,365),coords(376,265),coords(551,265)]
				group: quater_barrier.group
			}
			PolygonCollider
			{
				vertexes: [coords(643,215),coords(551,265),coords(376,265),coords(444,215)]
				group: quater_barrier.group
			}
			PolygonCollider
			{
				vertexes: [coords(643,215),coords(444,215),coords(512,165),coords(777,165)]
				group: quater_barrier.group
			}
			PolygonCollider
			{
				vertexes: [coords(905,140),coords(777,165),coords(512,165),coords(641,102),coords(785,55),coords(905,32)]
				group: quater_barrier.group
			}
			PolygonCollider
			{
				vertexes: [coords(905,32),coords(1024,22),coords(1024,130),coords(905,140)]
				group: quater_barrier.group
			}
		}
	}

	Q.Component
	{
		id: hideleo_sun_destroyer_factory
		Vehicle
		{
			id: hideleo
			type: Body.KINEMATIC
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de boss en 512*512
			scale: 3
			angle: Math.PI
			icon: 4
			icon_color: "red"
			max_speed: 0
			max_angular_speed: 1
			property color mask_shield: invincible ? Qt.rgba(0.58, 0, 0.82, 1) : Qt.rgba(0.58, 0, 0.82, 0)
			property real mask_barriere_a: 0
			property bool invincible: true
			property bool alive: true
			property bool shooting: false
			property var barrier_1
			property var barrier_2
			property var barrier_3
			property var barrier_4
			property real scale_for_shield: 4.2 * scale
			onAliveChanged: if(!alive) destroy()

			onMask_barriere_aChanged:
			{
				barrier_1.mask.a = mask_barriere_a
				barrier_2.mask.a = mask_barriere_a
				barrier_3.mask.a = mask_barriere_a
				barrier_4.mask.a = mask_barriere_a
			}

			function coords_barrier(x_sign, y_sign) {return Qt.point(hideleo.position.x + x_sign * scale_for_shield, hideleo.position.y + y_sign * scale_for_shield)} // plink fait des images de boss en 512*512

			function barrier_1b()
			{
				return coords_barrier(-1,-1)
			}
			function barrier_2b()
			{
				return coords_barrier(1,1)
			}
			function barrier_3b()
			{
				return coords_barrier(1,-1)
			}
			function barrier_4b()
			{
				return coords_barrier(-1,1)
			}

			Q.Component.onCompleted:
			{
				barrier_1 = barrier_factory.createObject(null, {scene: root.scene ,scale: scale_for_shield,z: altitudes.boss, angle: 0})
				barrier_1.position = Qt.binding(barrier_1b)
				barrier_2 = barrier_factory.createObject(null, {scene: root.scene ,scale: scale_for_shield ,z: altitudes.boss, angle: Math.PI})
				barrier_2.position = Qt.binding(barrier_2b)
				barrier_3 = barrier_factory.createObject(null, {scene: root.scene,scale: scale_for_shield ,z: altitudes.boss, angle: Math.PI/2})
				barrier_3.position = Qt.binding(barrier_3b)
				barrier_4 = barrier_factory.createObject(null, {scene: root.scene ,scale: scale_for_shield ,z: altitudes.boss, angle: -Math.PI/2})
				barrier_4.position = Qt.binding(barrier_4b)
			}
			Q.Component.onDestruction:
			{
				root.boss = hideleo_sun_destroyer_transition_factory.createObject(null, {scene: root.scene, position: hideleo.position, angle: hideleo.angle})
				barrier_1.alive= false
				barrier_2.alive= false
				barrier_3.alive= false
				barrier_4.alive= false
				root.boss_animation.alive = false
				root.hideleo_phase_animation = hideleo_phase_animation_last_factory.createObject(null, {parent: root.scene})
			}
			Image
			{
				id: image
				material: "chapter-3/sun-destroyer"
				z: altitudes.boss
				mask:
				{
					var x = 0
					if(weapon)
					{
						x = weapon.health / weapon.max_health
					}
					return Qt.rgba(1, x, x, 1)
				}
			}

			Image
			{
				material: "equipments/shield"
				mask: hideleo.mask_shield
				scale: 1.1
				z: altitudes.shield
			}

			CircleCollider
			{
				group: hideleo.invincible ? groups.enemy_invincible : 0
				radius: 1.1
			}

			PolygonCollider
			{
				vertexes: [coords(210,506),coords(177,470),coords(166,257),coords(229,6),coords(282,6),coords(346,257),coords(336,470),coords(305,506)]
				group: hideleo.invincible ? 0 : groups.item_destructible
			}

			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 10
			}

			EquipmentSlot
			{
				position: coords(255, 23)
				equipment: Weapon_of_death
				{
					id: weapon
					shooting: hideleo.shooting
				}
				onEquipmentChanged: hideleo.alive = false
			}
		}
	}

	Q.Component
	{
		id: sound_tp_factory
		Body
		{
			position: ship ? ship.position : Qt.point(0,0)
			property bool play: false
			property bool alive: true
			onPlayChanged:
			{
				if(play)
				{
					sound_shooting.play()
				}
			}
			onAliveChanged: if(!alive) destroy()
			Sound
			{
				id: sound_shooting
				objectName: "equipments/teleporter/shooting"
				scale: 4
			}
		}
	}
	Q.Component
	{
		id: hideleo_factory_2
		Vehicle
		{
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			function angle_shoot()
			{
				var res = 1/2
				if(left_equip < right_equip)
				{
					res = -1/2
				}
				if(left_equip < 2 && right_equip < 2)
				{
					res = -1
					if(poupe_equip < 2)
					{
						res = 1
					}
				}
				return res
			}
			id: hideleo
			scale: 12
			angle: Math.PI/2
			max_speed: 10
			max_angular_speed: 5
			type: Body.KINEMATIC
			property real angle_turret_vortex: Math.PI
			property real angle_turret_vortex_2: Math.PI
			property real angle_front_quipoutre: Math.PI
			property bool activate: false
			property bool begin: false
			icon: 4
			icon_color: "red"
			property real z: altitudes.boss
			property color mask: "white"
			property int left_equip: 0
			property int right_equip: 0
			property int poupe_equip: 0
			property int proue_equip: 0
			property bool change_phase: false

			Q.Component.onDestruction:
			{
				if(root.boss_animation)
				{
					root.boss_animation.alive = false
				}
				root.boss = hideleo_transition_factory.createObject(null, {scene: root.scene, position: hideleo.position, angle: hideleo.angle})
				root.hideleo_phase_animation = hideleo_phase_animation_2_factory.createObject(null, {parent: root.scene})
			}

			onRight_equipChanged:
			{
				if(activate)
				{
					if(right_equip < 2)
					{
						hideleo.change_phase = true
					}
				}
			}
			onLeft_equipChanged:
			{
				if(activate)
				{
					if(left_equip < 2)
					{
						hideleo.change_phase = true
					}
				}
			}

			Behaviour_attack_circle
			{
				enabled: hideleo.change_phase
				target_groups: groups.enemy_targets
				target_angle: angle_shoot() * Math.PI
				target_distance: 10
			}

			Image
			{
				id: image
				material: "chapter-3/hideleo"
				z: hideleo.z
				mask: hideleo.mask
			}
			PolygonCollider
			{
				vertexes: [coords(444,1003),coords(351,563),coords(351,307),coords(448,8),coords(575,8),coords(670,306),coords(670,560),coords(578,1003)]
				group: hideleo.begin ? groups.enemy_hull_naked : groups.wall
			}

			//aile droite
			EquipmentSlot
			{
				position: coords(402, 251)
				scale: 0.15/2
				angle: Math.PI * -1/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.right_equip++
					Q.Component.onDestruction: hideleo.right_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(402, 348)
				scale: 0.15/2
				angle: Math.PI * -1/2
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.right_equip++
					Q.Component.onDestruction: hideleo.right_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(402, 445)
				scale: 0.15/2
				angle: Math.PI * -1/2
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.right_equip++
					Q.Component.onDestruction: hideleo.right_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(402, 542)
				scale: 0.15/2
				angle: Math.PI * -1/2
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.right_equip++
					Q.Component.onDestruction: hideleo.right_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(402, 640)
				scale: 0.15/2
				angle: Math.PI * -3/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.right_equip++
					Q.Component.onDestruction: hideleo.right_equip--
				}
			}

			//aile gauche
			EquipmentSlot
			{
				position: coords(616, 251)
				scale: 0.15/2
				angle: Math.PI * 1/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.left_equip++
					Q.Component.onDestruction: hideleo.left_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(616, 348)
				scale: 0.15/2
				angle: Math.PI * 1/2
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.left_equip++
					Q.Component.onDestruction: hideleo.left_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(616, 445)
				scale: 0.15/2
				angle: Math.PI * 1/2
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.left_equip++
					Q.Component.onDestruction: hideleo.left_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(616, 542)
				scale: 0.15/2
				angle: Math.PI * 1/2
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.left_equip++
					Q.Component.onDestruction: hideleo.left_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(616, 640)
				scale: 0.15/2
				angle: Math.PI * 3/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.left_equip++
					Q.Component.onDestruction: hideleo.left_equip--
				}
			}

			//centre
			EquipmentSlot
			{
				position: coords(512, 806)
				scale: 0.15/2
				angle: 0
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(512, 650)
				scale: 0.15/2
				angle: 0
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(512, 465)
				scale: 0.15/2
				angle: 0
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(512, 327)
				scale: 0.20/2
				angle: 0
				equipment: ShieldCanceler
				{
					range_bullet: 40
					level: 0
					shooting: hideleo.activate
					period: hideleo.scene.seconds_to_frames(6)
				}
			}
			//poupe
			EquipmentSlot
			{
				position: coords(475, 745)
				scale: 0.15/2
				angle: Math.PI * -3/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.poupe_equip++
					Q.Component.onDestruction: hideleo.poupe_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(545, 745)
				scale: 0.15/2
				angle: Math.PI * 3/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.poupe_equip++
					Q.Component.onDestruction: hideleo.poupe_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(475, 880)
				scale: 0.15/2
				angle: Math.PI * -3/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.poupe_equip++
					Q.Component.onDestruction: hideleo.poupe_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(545, 880)
				scale: 0.15/2
				angle: Math.PI * 3/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.poupe_equip++
					Q.Component.onDestruction: hideleo.poupe_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(510, 940)
				scale: 0.15/2
				angle: Math.PI
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.poupe_equip++
					Q.Component.onDestruction: hideleo.poupe_equip--
				}
			}

			//proue

			EquipmentSlot
			{
				position: coords(475, 180)
				scale: 0.15/2
				angle: Math.PI * -1/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.proue_equip++
					Q.Component.onDestruction: hideleo.proue_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(545, 180)
				scale: 0.15/2
				angle: Math.PI * 1/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.proue_equip++
					Q.Component.onDestruction: hideleo.proue_equip--
				}
			}

			EquipmentSlot
			{
				position: coords(475, 100)
				scale: 0.15/2
				angle: Math.PI * -1/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.proue_equip++
					Q.Component.onDestruction: hideleo.proue_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(545, 100)
				scale: 0.15/2
				angle: Math.PI * 1/4
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.proue_equip++
					Q.Component.onDestruction: hideleo.proue_equip--
				}
			}
			EquipmentSlot
			{
				position: coords(510, 50)
				scale: 0.15/2
				angle: 0
				equipment: CoilGun
				{
					shooting: hideleo.activate
					level: 2
					period: hideleo.scene.seconds_to_frames(1/8)
					range_bullet: hideleo.scene.seconds_to_frames(3)
					Q.Component.onCompleted: hideleo.proue_equip++
					Q.Component.onDestruction: hideleo.proue_equip--
				}
			}
		}
	}

	Q.Component
	{
		id: hideleo_factory_1
		Vehicle
		{
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			id: hideleo
			scale: 12
			angle: Math.PI/2
			max_speed: 10
			type: Body.KINEMATIC
			max_angular_speed: 5
			icon: 4
			icon_color: "red"
			property real angle_turret_vortex: Math.PI
			property real angle_turret_vortex_2: Math.PI
			property real angle_front_quipoutre: Math.PI
			property bool swap_shoot: false
			property bool start: false
			Q.Component.onDestruction:
			{
				root.boss_animation.alive = false
				root.boss = hideleo_transition_factory.createObject(null, {scene: root.scene, position: hideleo.position, angle: hideleo.angle})
				root.hideleo_phase_animation = hideleo_phase_animation_factory.createObject(null, {parent: root.scene})
			}

			Image
			{
				id: image
				material: "chapter-3/hideleo"
				z: altitudes.boss
			}
			PolygonCollider
			{
				vertexes: [coords(444,1003),coords(351,563),coords(351,307),coords(448,8),coords(575,8),coords(670,306),coords(670,560),coords(578,1003)]
				group: hideleo.start ? groups.enemy_hull_naked : groups.wall
			}

			Behaviour_attack_circle
			{
				id: behaviour
				enabled: hideleo.start
				target_groups: groups.enemy_targets
				target_angle: Math.PI
				target_distance: 20
			}

			EquipmentSlot
			{
				position: coords(460, 761)
				scale: 0.15/2
				angle: Math.PI * -7/8
				equipment: TripleGun
				{
					shooting: behaviour.shooting && !swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(564, 761)
				scale: 0.15/2
				angle: Math.PI * 7/8
				equipment: TripleGun
				{
					shooting: behaviour.shooting && !swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(512, 806)
				scale: 0.15/2
				angle: 0
				equipment: SecondaryShield
				{
					level: 2
					max_shield: 900
				}
			}
			EquipmentSlot
			{
				position: coords(460, 867)
				scale: 0.15/2
				angle: Math.PI
				equipment: HeavyLaserGun
				{
					shooting: behaviour.shooting && !swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(564, 867)
				scale: 0.15/2
				angle: Math.PI
				equipment: HeavyLaserGun
				{
					shooting: behaviour.shooting && !swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(512, 903)
				scale: 0.15/2
				angle: angle_front_quipoutre
				equipment: Quipoutre
				{
					shooting: behaviour.shooting && !swap_shoot
					period: scene.seconds_to_frames(1)
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(407, 585)
				scale: 0.15/2
				angle: hideleo.angle_turret_vortex
				equipment: VortexGun
				{
					shooting: behaviour.shooting && !swap_shoot
					period: scene.seconds_to_frames(1.5)
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(617, 585)
				scale: 0.15/2
				angle: hideleo.angle_turret_vortex_2
				equipment: VortexGun
				{
					shooting: behaviour.shooting && !swap_shoot
					period: scene.seconds_to_frames(1.5)
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(460, 585)
				scale: 0.15/2
				angle: Math.PI * -7/8
				equipment: LaserGun
				{
					shooting: behaviour.shooting && !swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(564, 585)
				scale: 0.15/2
				angle: Math.PI * 7/8
				equipment: LaserGun
				{
					shooting: behaviour.shooting && !swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(407, 418)
				scale: 0.15/2
				angle: Math.PI
				equipment: MissileLauncher
				{
					shooting: behaviour.shooting && !swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(617, 418)
				scale: 0.15/2
				angle: Math.PI
				equipment: MissileLauncher
				{
					shooting: behaviour.shooting && !swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(512, 330)
				scale: 0.15/2
				angle: 0
				equipment: Teleporter
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(407, 295)
				scale: 0.15/2
				angle: 0
				equipment: Quipoutre
				{
					shooting: swap_shoot
					period: scene.seconds_to_frames(1/3)
					bullet_velocity: 15
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(617, 295)
				scale: 0.15/2
				angle: 0
				equipment: Quipoutre
				{
					shooting: swap_shoot
					period: scene.seconds_to_frames(1/3)
					bullet_velocity: 15
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(464, 250)
				scale: 0.15/2
				angle: 0
				equipment: LaserGun
				{
					shooting: swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(560, 250)
				scale: 0.15/2
				angle: 0
				equipment: LaserGun
				{
					shooting: swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(476, 40)
				scale: 0.15/2
				angle: 0
				equipment: PlasmaSpray
				{
					shooting: swap_shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(558, 40)
				scale: 0.15/2
				angle: 0
				equipment: PlasmaSpray
				{
					shooting: swap_shoot
					level: 2
				}
			}
		}
	}
	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			id: drone
			property var tab_weapons: root.root_tab_weapons
			property int indice_weapon: random_index(tab_weapons.length)
			max_speed: indice_weapon ? 5 : 10
			max_angular_speed: 6
			property bool behaviour: !indice_weapon ? true : false
			icon: 2
			icon_color: "White"
			property real utility: ship ? ship.position.x - drone.position.x : 0
			property bool alive: root.is_sun
			onAliveChanged: if(!alive) destroy()
			Q.Component.onDestruction: root.spawn--
			Q.Component.onCompleted: root.spawn++
			onUtilityChanged:
			{
				if(utility > 60)
				{
					drone.destroy()
				}
			}

			loot_factory: Repeater
			{
				Q.Component
				{
					Loot
					{
						property bool alive: !root.is_sun
						onAliveChanged: if(!alive) destroy()
					}
				}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				material: "folks/heter/1"
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
			}
			CircleCollider
			{
				group: (!indice_weapon && drone.aggressive_shield_active_damages) ? groups.enemy_hull_naked_aggressive_shield : groups.enemy_hull_naked
			}
			CircleCollider
			{
				group: (!indice_weapon && drone.aggressive_shield_active_damages) ? groups.enemy_shield_aggressive : 0
				radius: 1.1
			}
			EquipmentSlot
			{
				position.y: 0.3
				scale: 0.5
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon].createObject(null, {slot: this, shooting: true})
			}
			Behaviour_attack_circle
			{
				enabled: behaviour
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 0
			}
			Behaviour_attack_harass
			{
				enabled: !behaviour
				target_groups: groups.enemy_targets
				relaxation_time: 1
				target_distance: ship ? ship.get_norm_velocity() : 0
			}
		}
	}

	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_story_0 // state_test_1
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					ship.position.y = 0
					camera_position = undefined
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					//root.hideleo_x = -600
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: state_story_0.propertiesAssigned
				guard: !root.save_boss
				onTriggered:
				{
					camera_position = undefined
					music.objectName = "chapter-3/Take_the_Lead"
					messages.add("nectaire/normal", qsTr("story begin 1"))
					messages.add("lycop/normal", qsTr("story begin 2"))
					messages.add("nectaire/normal", qsTr("story begin 3"))
					messages.add("lycop/normal", qsTr("story begin 4"))
					messages.add("nectaire/normal", qsTr("story begin 5"))
					messages.add("nectaire/normal", qsTr("story begin 6"))
					messages.add("lycop/normal", qsTr("story begin 7"))
					messages.add("nectaire/normal", qsTr("story begin 8"))
					saved_game.set("travel/chapter-3/chapter-3", false)
				}
			}
			
			SignalTransition
			{
				targetState: state_after_video_opening
				signal: state_story_0.propertiesAssigned
				guard: root.save_boss
				onTriggered:
				{
					camera_position = undefined
					music.objectName = "chapter-3/Take_the_Lead"
					root.is_sun = false
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_1; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "request_patrols"; value: true}

			//en vue du boss
			SignalTransition
			{
				targetState: state_after_video_opening
				signal: root.proximity_hideleoChanged
				guard: root.proximity_hideleo < 50
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-3/arrivee_boss_soleil.ogg"
					function f ()
					{
						music.objectName = "survival/main"
						root.is_sun = false
						if(!root.save_boss && mod_easy)
						{
							saved_game.set("save_boss_3", true)
							scene_loader.save()
						}
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
			}
			//moins de 400 km du hideleo plus de mechant
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: root.proximity_hideleoChanged
				guard: root.proximity_hideleo < 400 && root.less_400
				onTriggered:
				{
					root.less_400 = false
					var z = [
						[0, -1/2],
						[-5, -1/2],
						[-10, -1/2]
						]
					for(var i = 0; i < z.length; ++i)
					{
						drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + root.spawn_enemy, ship.position.y - z[i][0]), angle: z[i][1] * Math.PI, indice_weapon: 0})

						drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 20 + root.spawn_enemy, ship.position.y - z[i][0]), angle: z[i][1] * Math.PI, indice_weapon: 1 + random_index(root.root_tab_weapons.length-1)})

						drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 40 + root.spawn_enemy, ship.position.y - z[i][0]), angle: z[i][1] * Math.PI, indice_weapon: 1 + random_index(root.root_tab_weapons.length-1)})
					}
				}
			}

			//patrouilles
			SignalTransition
			{
				signal: timer_patrols.value_changed
				guard: timer_patrols.value
				onTriggered:
				{
					var z = [
						[0, -1/2],
						[-5, -1/2],
						[-10, -1/2]
						]
					for(var i = 0; i < z.length; ++i) drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + root.spawn_enemy, ship.position.y - z[i][0]), angle: z[i][1] * Math.PI})
				}
			}

			//autres eruption
			SignalTransition
			{
				signal: timer_eruption.value_changed
				guard: timer_eruption.value
				onTriggered:
				{
					//declencher eruption
					eruption_factory.createObject(null, {scene: root.scene, position:Qt.point(ship.position.x + 40 + root.spawn_enemy, -43.5+15-1)})
				}
			}
		}
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_1; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_after_video_opening;
			SignalTransition
			{
				targetState:state_hideleo_dialogue
				signal: state_after_video_opening.propertiesAssigned
				guard:!screen_video.video.file
				// guard:
				onTriggered:
				{
					root.hideleo_x = ship.position.x + 20
					//dialogue
					clear_screen_timer.createObject(null, {parent: scene})
					messages.add("nectaire/normal", qsTr("story boss 1"))
					messages.add("lycop/normal", qsTr("story boss 2"))
					messages.add("nectaire/normal", qsTr("story boss 3"))
					messages.add("lycop/normal", qsTr("story boss 4"))
					messages.add("nectaire/normal", qsTr("story boss 5"))
					saved_game.add_science(1)
				}
			}
		}
		State {id: state_hideleo_dialogue; SignalTransition {targetState: state_hideleo_before_wait; signal: state_hideleo_dialogue.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_hideleo_before_wait
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: scene; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_hideleo_wait
				signal: screen_cleared
			}
		}
		State
		{
			id: state_hideleo_wait
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_hideleo
				signal: root.proximity_hideleoChanged
				guard: root.proximity_hideleo < 5
				// guard:
				onTriggered:
				{
					root.info_print.destroy()
					//ajouter boss

					root.boss = hideleo_factory_1.createObject(null, {scene: root.scene, position:Qt.point(ship.position.x + 200, ship.position.y)})
					root.boss_animation = hideleo_turret_animation_1.createObject(null, {parent: root.scene})
					root.sound = sound_tp_factory.createObject(null, {parent: root.scene})
					animation_tp = tp_ship_factory.createObject(null, {parent: root.scene})

					//boss phase 3
					/*root.boss = hideleo_transition_factory.createObject(null, {scene: root.scene, position: Qt.point(ship.position.x + 20, ship.position.y), angle: 3* Math.PI})
					root.hideleo_phase_animation = hideleo_phase_animation_2_factory.createObject(null, {parent: root.scene})*/

					//dernier animation
					/*root.boss = hideleo_sun_destroyer_transition_factory.createObject(null, {scene: root.scene, position: Qt.point(ship.position.x + 20, ship.position.y), angle: 3* Math.PI})
					root.hideleo_phase_animation = hideleo_phase_animation_last_factory.createObject(null, {parent: root.scene})*/
				}
			}
		}
		State
		{
			id: state_hideleo
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_concentration
				signal: one_boss_destroyed
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("story concentration 1"))
					messages.add("lycop/normal", qsTr("story concentration 2"))
					messages.add("heter/normal", qsTr("story concentration 3"))
					messages.add("nectaire/normal", qsTr("story concentration 4"))
					messages.add("lycop/normal", qsTr("story concentration 5"))
				}
			}
		}
		State {id: state_dialogue_concentration; SignalTransition {targetState: state_concentration_wait; signal: state_dialogue_concentration.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_concentration_wait
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_after_concentration
				signal: concentration_done
				onTriggered:
				{
					messages.add("heter/normal", qsTr("story concentration fin 1"))
					messages.add("heter/normal", qsTr("story concentration fin 2"))
				}
			}
		}
		State
		{
			id: state_dialogue_after_concentration
			SignalTransition
			{
				targetState: state_after_video_freeze_opening
				signal: state_dialogue_after_concentration.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-3/gel_soleil.ogg"
					function f ()
					{
						music.objectName = "survival/main"
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
					root.boss.alive = false
					hideleo_phase_animation.alive = false
				}
			}
		}
		State
		{
			id: state_after_video_freeze_opening;
			SignalTransition
			{
				targetState:state_sun_freeze
				signal: state_after_video_freeze_opening.propertiesAssigned
				guard:!screen_video.video.file
			}
		}

		State
		{
			id: state_sun_freeze
			SignalTransition
			{
				targetState: state_dialogue_after_sun_freeze
				signal: state_sun_freeze.propertiesAssigned
				onTriggered:
				{
					messages.add("lycop/surprised", qsTr("story freeze 1"))
					messages.add("nectaire/normal", qsTr("story freeze 2"))
					messages.add("lycop/surprised", qsTr("story freeze 3"))
					messages.add("nectaire/normal", qsTr("story freeze 4"))
					messages.add("lycop/angry", qsTr("story freeze 5"))
					messages.add("nectaire/normal", qsTr("story freeze 6"))
					messages.add("lycop/angry", qsTr("story freeze 7"))
				}
			}
		}
		State
		{
			id: state_dialogue_after_sun_freeze
			SignalTransition
			{
				targetState: state_end
				signal: state_dialogue_after_sun_freeze.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("story freeze 8"))
					messages.add("cnes/sad", qsTr("story freeze 9"))
					messages.add("lycop/normal", qsTr("story freeze 10"))
					messages.add("nectaire/normal", qsTr("story freeze 11"))
					//gestion sauvegarde + debloquage map
					saved_game.set("travel/chapter-4/chapter-4", true)
					saved_game.set("travel/chapter-2/chapter-2c", true)
					saved_game.add_science(3)
					saved_game.add_matter(3000)
					platform.set_bool("chapter-3-end")
					saved_game.set("file", "game/chapter-transition/chapter-transition.qml")
					root.finalize = function() {scene_loader.save()}
				}
			}
		}
		State {id: state_dialogue_after_sun_freeze_2; SignalTransition {targetState: state_end; signal: state_dialogue_after_sun_freeze_2.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_end
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: screen_menu.about_screen_travel; property: "blink"; value: true}
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["chapter-3/hideleo"])
		jobs.run(scene_view, "preload", ["chapter-3/sun"])
		jobs.run(scene_view, "preload", ["chapter-3/sun-destroyer"])
		jobs.run(scene_view, "preload", ["chapter-3/circle-barrier"])
		jobs.run(scene_view, "preload", ["chapter-3/explosion-hideleo/ext"])
		jobs.run(scene_view, "preload", ["chapter-3/explosion-hideleo/int"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		jobs.run(scene_view, "preload", ["folks/heter/1"])
		jobs.run(scene_view, "preload", ["explosion"])
		jobs.run(scene_view, "preload", ["vide"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-3/door/" + i])
		for(var i = 0; i < 45; ++i) jobs.run(scene_view, "preload", ["chapter-3/eruption/" + i])
		for(var i = 0; i < 36; ++i) jobs.run(scene_view, "preload", ["chapter-3/concentration/" + i])
		for(var i = 0; i < 30; ++i)
		{
			for(var j = 0; j < 5; ++j)
			{
				jobs.run(scene_view, "preload", ["chapter-3/freezing-beam/" + i + "/" + j])
			}
		}
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		jobs.run(music, "preload", ["chapter-3/Take_the_Lead"])
		jobs.run(music, "preload", ["survival/main"])
		groups.init(scene)
		root.info_print = screen_info_factory.createObject(screen_hud)
		root.finalize = function() {if(info_print) info_print.destroy()}
	}
}
