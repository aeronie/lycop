import QtQuick 2.0 as Q
import aw.game 0.0

ActiveEquipment
{
	id: death
	property int level: 1
	property int bullet_group: groups.enemy_dot
	property real power: 1.1
	health: max_health
	max_health: 50000
	Sound
	{
		objectName: "equipments/maser-emitter/shooting"
		scale: shooting ? slot.sound_scale : 0
		looping: true
	}
	Q.Component
	{
		id: spray_factory
		Body
		{
			id: spray
			property int nb_repete: 5
			readonly property real damages: 10000 * scene.time_step
			scene: death.body.scene
			scale: death.power

			Image
			{
				id: beam0
				position.y: -scale
				position.x: 0
				mask: death.shooting ? "white" : "transparent"
				z: altitudes.bullet - 0.000001
				material: "chapter-3/freezing-beam/"+(Math.floor(scene.time))%29+"/0"
			}
			Image
			{
				id: beam1
				position.y: -3*scale
				position.x: 0
				mask: death.shooting ? "white" : "transparent"
				z: altitudes.bullet - 0.000001
				material: "chapter-3/freezing-beam/"+(Math.floor(scene.time))%29+"/1"
			}
			Repeater
			{
				count: nb_repete
				Q.Component
				{
					Image
					{
						position.y: -(5+(index*2))*scale
						position.x: 0
						mask: death.shooting ? "white" : "transparent"
						z: altitudes.bullet - 0.000001
						material: "chapter-3/freezing-beam/"+(Math.floor(scene.time))%29+"/2"
					}
				}
			}
			Image
			{
				id: beam3
				position.y: -(5+(nb_repete*2))*scale
				position.x: 0
				mask: death.shooting ? "white" : "transparent"
				z: altitudes.bullet - 0.000001
				material: "chapter-3/freezing-beam/"+(Math.floor(scene.time))%29+"/3"
			}
			Image
			{
				id: beam4
				position.y: -(7+(nb_repete*2))*scale
				position.x: 0
				mask: death.shooting ? "white" : "transparent"
				z: altitudes.bullet - 0.000001
				material: "chapter-3/freezing-beam/"+(Math.floor(scene.time))%29+"/4"
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-0.45,0),Qt.point(-0.45, - (7.5 + 2 * nb_repete)),Qt.point(0.45,-(7.5 + 2 * nb_repete)),Qt.point(0.45,0)]
				group: death.shooting ? bullet_group : 0
				sensor: true
			}
		}
	}
	WeldJoint
	{
		id: joint
		body_1: death.body
		Q.Component.onDestruction: body_2.destroy()
		Q.Component.onCompleted:
		{
			body_2 = spray_factory.createObject(null, {position: slot.point_to_scene(Qt.point(0, -1.4)), angle: slot.angle_to_scene(0)})
			slot.angle_changed(slot.angle)
		}
	}
	Q.Connections
	{
		target: slot
		onAngleChanged:
		{
			joint.position = slot.point_to_body(Qt.point(0, 0))
			joint.angle = slot.angle_to_body(0)
			joint.initialize()
		}
	}
}
