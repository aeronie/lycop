import QtQuick 2.0 as Q
import aw.game 0.0

Body
{
	id: root
	property int group: 0
	property real radius: 1
	property real width: 0
	function p(i, a)
	{
		a = (a + i * 2) * Math.PI / 20
		return Qt.point(Math.cos(a) * radius, Math.sin(a) * radius)
	}
	Repeater
	{
		count: 20
		Q.Component
		{
			PolygonCollider
			{
				vertexes: [p(index, 0), p(index, 1), p(index, 2)]
				radius: root.width
				group: root.group
			}
		}
	}
}
