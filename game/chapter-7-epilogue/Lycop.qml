import QtQuick 2.0 as Q
import aw.game 0.0
import "."
import "qrc:///game"

Vehicle
{
	id: lycop
	property bool player: true
	max_angular_speed: 3
	Q.Component.onDestruction: undefined
	max_speed: 10
	readonly property CircleCollider collider: collider3
	property var last_pos: position
	property var tab_lycop:["chapter-7-epilogue/lycop/no-armor/","chapter-7-epilogue/lycop/armor/","chapter-7-epilogue/lycop/armor-gun/"]
	property int ind_im_lycop: 0
	property var tab_move:["foot-front/","foot-strafe/"]
	property var tab_cap_ind:[10,9]
	property var tab_scale:[1,1,1.25]
	property int phase: 0 // 0 marche, 1 strafe
	property int indice: 0
	property bool equiped: false
	property bool suit: false
	CircleCollider
	{
		id: collider3
		density: 0
		radius: 0
	}
	Image
	{
		material:  lycop.tab_lycop[lycop.ind_im_lycop] + lycop.tab_move[lycop.phase] + lycop.indice
		scale: tab_scale[lycop.ind_im_lycop]
	}
	
	CircleCollider
	{
		id: collider1
		group: groups.lycop_human
		radius: 0.9
	}
	
	
	function calcul_ship_angle(a)
	{
		var result = a
		while(result <= -Math.PI)
		{
			result += 2 * Math.PI
		}
		while(result > Math.PI)
		{
			result -= 2 * Math.PI
		}
		return result
	}
	
	onEquipedChanged:
	{
		if(equiped)
		{
			lycop.ind_im_lycop++
			lycop.slots[0].data = lycop_gun
			lycop.slots[0].equipment = shotgun_factory.createObject(null, {slot: slots[0]})
			restore_equipments()
		}
	}
	onSuitChanged:
	{
		if(suit)
		{
			lycop.ind_im_lycop++
		}
	}
	
	Q.Component
	{
		id: shotgun_factory
		Weapon
		{
			property int level: 1
			property int bullet_group: groups.bullet
			period: scene.seconds_to_frames(1.6)
			health: max_health
			max_health: 1
			range_bullet: scene.seconds_to_frames(2)
			readonly property Sound sound_shooting: Sound
			{
				objectName: "equipments/lycop-gun/shooting"
				scale: 50
			}
			bullet_factory: Repeater
			{
				count: 9
				Q.Component
				{
					Bullet
					{
						scale: 0.08
						range: weapon.range_bullet
						damages: 12
						Image
						{
							material: "equipments/lycop-gun/bullet"
							z: altitudes.bullet
						}
						CircleCollider
						{
							group: weapon.bullet_group
							sensor: true
						}
						Q.Component.onCompleted:
						{
							var a = weapon.slot.point_to_scene(Qt.point(0, 0))
							var b = 0
							angle = weapon.slot.angle_to_scene(-( 4 - index) * Math.PI / 96)
							position.x = a.x + b * Math.cos(angle)
							position.y = a.y + b * Math.sin(angle)
							weapon.sound_shooting.play()
							weapon.set_bullet_velocity(this, 20)
						}
					}
				}
			}
		}
	}
	
	Technology
	{
		id: lycop_gun
		objectName: "lycop-gun"
	}
	readonly property var slots:
	{
		var selected = []
		var all = children
		for(var i = 0; i < all.length; ++i) if(all[i].equipment !== undefined) selected.push(all[i])
		return selected
	}
	readonly property bool shooting:
	{
		for(var i = 0; i < slots.length; ++i) if(slots[i].equipment && slots[i].equipment.shooting) return true
		return false
	}
	
	
	EquipmentSlot
	{
		scale: 1
		position.x: 31/128
		position.y: -1
	}
	
	
	function restore_equipments()
	{
		if(slots[0].equipment)
		{
			connect_shooting(0)
		}
		update_equipments()
		shield = 0
		window.energy = 0
	}
	function connect_shooting(slot_id)
	{
		var a = slots[slot_id].equipment.set_shooting
		if(a) controls.slots[slot_id].value_changed.connect(a)
		if(a && !slots[slot_id].equipment.consumable) controls.weapons.value_changed.connect(a)
		if(slots[slot_id].equipment.slot_id !== undefined) slots[slot_id].equipment.slot_id = slot_id
	}
	
	
	function repair_cost(slot_id)
	{
		return 1
	}
	function repair_all_cost()
	{
		return 1
	}
	function can_repair(cost)
	{
		return false
	}
	function repair(slot_id)
	{
	}
	
	
}