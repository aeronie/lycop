 import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-7-epilogue/epilogue.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: lycop
	signal sortie
	signal fin
	signal ship_ok
	signal asphyxie
	property var camera_position: Qt.point(17.5 * 4, 14.5 * 4)
	property var last_ship_position: Qt.point(0, 0)
	property bool mod_easy : saved_game.self.get("mod_easy", false)
	property var barriere_bas
	property real ti: 0
	property var info_print
	property real chapter_scale: 2
	property bool can_open: true
	property bool end_back: false
	property real fuzzy_comp: 0
	property bool shotgun_taken: false
	property bool suit_taken: false
	property bool open_capsule: false
	property bool open_room_nectaire: false
	property bool open_room_life: false
	property bool open_room_stock: false
	property int cons_room_life: 2
	property int cons_room_stock: 3
	property bool last_level: true
	property bool level_finish: false
	property bool vener: false
	property bool start_spawner: false
	property bool start_nectaire: false
	property real oxygene: 100 + ti / 200
	property real timer_oxy: clock ? clock.time : 12
	property var clock
	property bool apis_come: false
	property var capsule
	property var lycop_ship
	property var tab_spawner:[
	[17.5,29.5, 0],
	[10.5,26.5, 1/2],
	[10.5,23.5, 1/2]
	]
	
	onOxygeneChanged:
	{
		if(root.oxygene <= 0)
		{
			info_print.color = "red"
		}
	}
	
	onApis_comeChanged:
	{
		//creer anim apis
		if(root.apis_come)
		{
			tp_apis_factory.createObject(null, {parent: ship.scene, position_enemy: Qt.point(root.capsule.position.x - 20, root.capsule.position.y)})
		}
	}
	
	onTiChanged:
	{
		if(root.oxygene < 90 && !root.start_nectaire)
		{
			root.oxygene = 90
		}
	
		if(!ship)
		{
			game_over = true
		}
		else
		{
			last_ship_position = Qt.point(ship.position.x,ship.position.y)
		}
	}
	
	onCons_room_lifeChanged:
	{	
		if(!start_spawner)
		{
			start_spawner = true
		}
		if(cons_room_life == 0)
		{
			open_room_stock = true
		}
	}
	onCons_room_stockChanged:
	{
		if(cons_room_stock == 0)
		{
			open_room_nectaire = true
		}
	}
	
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-7-epilogue/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-7-epilogue/"+mat+"/c"])
			}
		}
		return fill_tiles
	}
	
	function fill_2(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y, mat])
			}
		}
		return fill_tiles
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}
	
	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: !root.suit_taken ? qsTr("Asphyxie : %1 s").arg(Math.floor(root.timer_oxy)) : qsTr(" Oxygène : %1%").arg( Math.max(Math.floor(root.oxygene * 100) / 100, 0)) 
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}
	
	Q.Component
	{
		id: clock_time
		Animation
		{
			id: timer
			time: 12
			speed: -1
			onTimeChanged:
			{
				if(!root.suit_taken && timer.time < 0)
				{
					root.asphyxie()
				}
				else
				{
					if(root.suit_taken)
					{
						destroy()
					}
				}
			}
		}
	}
	
	scene: Scene
	{
		running: false
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		
		Background
		{
			property bool alive: !root.end_back
			onAliveChanged: if(!alive) destroy()
		
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		
		Body
		{
			id: fond_vaisseau
			position.x: 17.5 * 2 * chapter_scale
			position.y: 20.5 * 2 * chapter_scale 
			scale : 70
			Image
			{
				id: image
				material: "folks/lycop/HR"
				z: altitudes.background_near
			}
		}
		
		Tiles
		{
			id: corridor_1
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[19,13,'n','background'],
				[19,16,'e','background'],
				[18,16,'s','background'],
				[18,17,'e','background'],
				[22,17,'n','background'],
				[22,21,'e','background'],
				[18,21,'s','background'],
				[18,22,'e','background'],
				[28,22,'n','background'],
				[29,22,'n',''],
				[29,24,'e',''],
				[28,24,'s','background'],
				[28,28,'e','background'],
				[24,28,'s','background'],
				[24,23,'w','background'],
				[23,23,'s','background'],
				[23,28,'e','background'],
				[21,28,'s','background'],
				[21,31,'e','background'],
				[14,31,'s','background'],
				[14,28,'w','background'],
				[7,28,'s','background'],
				[7,22,'w','background'],
				[17,22,'n','background'],
				[17,21,'w','background'],
				[13,21,'s','background'],
				[13,17,'w','background'],
				[17,17,'n','background'],
				[17,16,'w','background'],
				[16,16,'s','background'],
				[16,13,'w','background']
				]).concat(
				fill(17,14,18,15,'background'),
				fill(28,23,28,23,'background'),
				fill(14,18,21,20,'background'),
				fill(8,23,22,27,'background'),
				fill(15,28,20,30,'background'),
				fill(25,23,27,27,'background')
				)
		}
		
		Tiles
		{
			id: capsule_room
			z: altitudes.background_near
			mask: root.open_capsule ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[30,22,'n','background'],
				[30,24,'e','background'],
				[29,24,'s','background'],
				[28,24,'s',''],
				[28,22,'w','']
				]).concat(
				fill(29,23,29,23,'background'),
				[[29,24,'chapter-7-epilogue/background/s']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(28.5 * 2, 21.5 * 2), Qt.point(30.5 * 2, 21.5 * 2), Qt.point(30.5 * 2, 24.5 * 2), Qt.point(28.5 * 2, 24.5 * 2)]
				group: root.open_capsule ? 0 : groups.wall
				sensor: true
			}
		}
		Repeater
		{
			count: tab_spawner.length
			Q.Component
			{
				Body 
				{
					type:  Body.STATIC
					scale: 3
					position.x: tab_spawner[index][0]*4
					position.y: tab_spawner[index][1]*4
					Image
					{
						material: "chapter-7-epilogue/nectaire/drone-spawner/destroyed"
						z: altitudes.boss
					}
					PolygonCollider
					{
						vertexes: [Qt.point(-0.9,-0.9), Qt.point(-0.9,0.9), Qt.point(0.9, 0.9), Qt.point(0.9,-0.9)]
						group: groups.lycop_dont_move
					}
				}
			}
		}
		Body 
		{
			type:  Body.STATIC
			Image
			{
				material: "chapter-7-epilogue/capsule-launcher"
				position.x: 29.5 * 4
				position.y: 22.5 * 4
				scale: 2*chapter_scale
				z: altitudes.background_near + 0.0000001
				mask: root.open_capsule ? "white" : "transparent"
			}
		}
		Body 
		{
			type:  Body.STATIC
			Image
			{
				material: "chapter-7-epilogue/control-panel"
				position.x: 17.5 * 4
				position.y: 13.25 * 4
				scale: 2
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(17 * 4, 12.75*4), Qt.point(18 * 4, 12.75*4), Qt.point(18 * 4, 13.55*4), Qt.point(17 * 4, 13.55*4)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			Image
			{
				material: "chapter-7-epilogue/furniture/kitchen"
				position.x: 13.375 * 4
				position.y: 17.375 * 4
				scale: 2.5
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(12.75 * 4, 16.75*4), Qt.point(13.125 * 4, 16.75*4), Qt.point(13.125 * 4, 18*4), Qt.point(12.75 * 4, 18*4)]
				group: groups.lycop_dont_move
			}
			PolygonCollider
			{
				vertexes: [Qt.point(13.125 * 4, 16.75*4), Qt.point(13.875 * 4, 16.75*4), Qt.point(13.875 * 4, 17.15*4), Qt.point(13.125 * 4, 17.15*4)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			Image
			{
				material: "chapter-7-epilogue/furniture/shelf"
				position.x: 13.375 * 4
				position.y: 20 * 4
				scale: 2.5
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(12.75 * 4, 19.375*4), Qt.point(13.125 * 4, 19.375*4), Qt.point(13.125 * 4, 20.625*4), Qt.point(12.75 * 4, 20.625*4)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			Image
			{
				material: "chapter-7-epilogue/furniture/table"
				position.x: 15 * 4
				position.y: 19 * 4
				scale: 3
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(14.30 * 4, 18.30*4), Qt.point(15.125 * 4, 18.30*4), Qt.point(15.125 * 4, 19.70*4), Qt.point(14.30 * 4, 19.70*4)]
				group: groups.lycop_dont_move
			}
			PolygonCollider
			{
				vertexes: [Qt.point(15.125 * 4, 18.9*4), Qt.point(15.65 * 4, 18.9*4), Qt.point(15.65 * 4, 19.70*4), Qt.point(15.125 * 4, 19.70*4)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			Image
			{
				material: "chapter-7-epilogue/furniture/gym"
				position.x: 20.5 * 4
				position.y: 20.25 * 4
				scale: 2.5
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(20 * 4, 19.75*4), Qt.point(21 * 4, 19.75*4), Qt.point(21 * 4, 20.75*4), Qt.point(20 * 4, 20.75*4)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			Image
			{
				material: "chapter-7-epilogue/furniture/bed"
				position.x: 21.75 * 4
				position.y: 17.5 * 4
				scale: 2.5
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(21.5 * 4, 16.75*4), Qt.point(22 * 4, 16.75*4), Qt.point(22 * 4, 18*4), Qt.point(21.5 * 4, 18*4)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			Image
			{
				material: "chapter-7-epilogue/furniture/tv"
				position.x: 19.5 * 4
				position.y: 17.5 * 4
				scale: 3
				z: altitudes.background_near + 0.0000001
			}
			/* canape */
			PolygonCollider
			{
				vertexes: [Qt.point(19* 4, 17.9*4), Qt.point(20 * 4, 17.9*4), Qt.point(20 * 4, 18.25*4), Qt.point(19 * 4, 18.25*4)]
				group: groups.lycop_dont_move
			}
			/* tv + table */
			PolygonCollider
			{
				vertexes: [Qt.point(19* 4, 16.75*4), Qt.point(20 * 4, 16.75*4), Qt.point(20 * 4, 17.375*4), Qt.point(19 * 4, 17.375*4)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			position.x: 22.5 * 4
			position.y: 27.5 * 4
			scale: 3
			Image
			{
				material: "chapter-7-epilogue/filling/guns"
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0.75,0.78), Qt.point(-0.75,0.78), Qt.point(-0.75,-0.78), Qt.point(0.75,-0.78)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			position.x: 14.5 * 4
			position.y: 30.5 * 4
			scale: 3
			Image
			{
				material: "chapter-7-epilogue/filling/laser-guns"
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0.75,0.65), Qt.point(-0.75,0.65), Qt.point(-0.75,-0.65), Qt.point(0.75,-0.65)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			position.x: 7.5 * 4
			position.y: 24 * 4
			scale: 3
			Image
			{
				material: "chapter-7-epilogue/filling/materials"
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0.75,0.8), Qt.point(-0.75,0.8), Qt.point(-0.75,-0.8), Qt.point(0.75,-0.8)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			position.x: 14.5 * 4
			position.y: 29 * 4
			scale: 3
			Image
			{
				material: "chapter-7-epilogue/filling/materials"
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0.75,0.8), Qt.point(-0.75,0.8), Qt.point(-0.75,-0.8), Qt.point(0.75,-0.8)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			position.x: 7.5 * 4
			position.y: 22.5 * 4
			scale: 3
			Image
			{
				material: "chapter-7-epilogue/filling/missiles"
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0.8,0.8), Qt.point(-0.8,0.8), Qt.point(-0.8,-0.8), Qt.point(0.8,-0.8)]
				group: groups.lycop_dont_move
			}
		}
		Body 
		{
			type:  Body.STATIC
			position.x: 7.5 * 4
			position.y: 27.5 * 4
			scale: 3
			Image
			{
				material: "chapter-7-epilogue/filling/rockets"
				z: altitudes.background_near + 0.0000001
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0.8,0.8), Qt.point(-0.8,0.8), Qt.point(-0.8,-0.8), Qt.point(0.8,-0.8)]
				group: groups.lycop_dont_move
			}
		}
		Body
		{
			id: all_wall
			position.y: 0
			position.x: 0
			scale: chapter_scale*2
			
			type: Body.STATIC
			
			function point(x,y)
			{
				return Qt.point(x - 0.25 , y + 0.25)
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(16, 10), all_wall.point(19.5,10), all_wall.point(19.5, 12.5), all_wall.point(16, 12.5)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(19.5,10), all_wall.point(23,10), all_wall.point(23,16.5), all_wall.point(19.5,16.5)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(22.5,10), all_wall.point(30.5, 10), all_wall.point(30.5,21.5), all_wall.point(22.5, 21.5)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(30.5,10), all_wall.point(33, 10), all_wall.point(33 ,28), all_wall.point(30.5,28)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(28.5,24), all_wall.point(30.5, 24), all_wall.point(30.5 ,28), all_wall.point(28.5,28)]
				group: groups.wall
			}
			
			
			PolygonCollider
			{
				vertexes: [all_wall.point(21.5, 28), all_wall.point(30,28), all_wall.point(30,31), all_wall.point(21.5, 31)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(14, 31), all_wall.point(30,31), all_wall.point(30,33), all_wall.point(14,33)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(7, 28), all_wall.point(14, 28), all_wall.point(14,33), all_wall.point(7,33)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(3, 21.5), all_wall.point(7, 21.5), all_wall.point(7 ,33), all_wall.point(3,33)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(3, 16.5), all_wall.point(13,16.5), all_wall.point(13, 21.5), all_wall.point(3, 21.5)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(3, 10), all_wall.point(16, 10), all_wall.point(16, 16.5), all_wall.point(3,16.5)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(16, 16), all_wall.point(17,16), all_wall.point(17,16.5), all_wall.point(16,16.5)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(18.5, 16), all_wall.point(19.5, 16), all_wall.point(19.5 , 16.5), all_wall.point(18.5, 16.5)]
				group: groups.wall
			}
			
			
			PolygonCollider
			{
				vertexes: [all_wall.point(13, 21), all_wall.point(17,21), all_wall.point(17,21.5), all_wall.point(13,21.5)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(18.5, 21), all_wall.point(22.5,21), all_wall.point(22.5,21.5), all_wall.point(18.5,21.5)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(23.5, 23), all_wall.point(24,23), all_wall.point(24,28), all_wall.point(23.5,28)]
				group: groups.wall
			}
		}
		
		
		Body
		{
			id: barriere_1
			type: Body.STATIC
			property real x:29
			property real y:23
			property int nb_repe: 10
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_1.x - 0.625)*2*chapter_scale
						position.y: (barriere_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: root.open_capsule ? Qt.rgba(0.58, 0, 0.82, 0) : Qt.rgba(0.58, 0, 0.82, 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_1.coords(barriere_1.x - 0.75, barriere_1.y + 1.25 - ( barriere_1.nb_repe + 1 ) / 4),
				barriere_1.coords(barriere_1.x - 0.25, barriere_1.y + 1.25 - ( barriere_1.nb_repe + 1 ) / 4), 
				barriere_1.coords(barriere_1.x - 0.25, barriere_1.y + 1.25), 
				barriere_1.coords(barriere_1.x - 0.75, barriere_1.y + 1.25)
				]
				group:  root.open_capsule ? 0 : groups.enemy_invincible
			}
		}
		
		
		
		Body
		{
			id: barriere_2
			type: Body.STATIC
			property real x:24
			property real y:22
			property int nb_repe: 6
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_2.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_2.x - 0.625)*2*chapter_scale
						position.y: (barriere_2.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, root.open_room_nectaire ? (root.start_nectaire ? 1 : 0) : 1)
					}
				}
			}
			Repeater
			{
				count: barriere_2.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_2.x - 0.375)*2*chapter_scale
						position.y: (barriere_2.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, root.open_room_nectaire ? (root.start_nectaire ? 1 : 0) : 1)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [
				barriere_2.coords(barriere_2.x - 0.75, barriere_2.y + 1.25 - ( barriere_2.nb_repe + 1 ) / 4),
				barriere_2.coords(barriere_2.x - 0.25, barriere_2.y + 1.25 - ( barriere_2.nb_repe + 1 ) / 4), 
				barriere_2.coords(barriere_2.x - 0.25, barriere_2.y + 1.25), 
				barriere_2.coords(barriere_2.x - 0.75, barriere_2.y + 1.25)
				]
				group:  root.open_room_nectaire  ? (root.start_nectaire ? groups.enemy_invincible : 0) : groups.enemy_invincible
			}
		}
		
		
		Body
		{
			id: barriere_3
			type: Body.STATIC
			property real x: 17
			property real y: 16
			property int nb_repe: 6
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_3.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_3.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_3.y+0.375)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_room_life ? 0 : 1) 
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_3.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_3.y + 0.625)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_room_life ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_3.coords(barriere_3.x + 1.25 - barriere_3.nb_repe / 4, barriere_3.y + 0.25), 
				barriere_3.coords(barriere_3.x + 1.25, barriere_3.y + 0.25), 
				barriere_3.coords(barriere_3.x + 1.25, barriere_3.y + 0.75), 
				barriere_3.coords(barriere_3.x + 1.25 - barriere_3.nb_repe / 4, barriere_3.y + 0.75)
				]
				group: root.open_room_life ? 0 : groups.enemy_invincible
			}
		}
		
		Body
		{
			id: barriere_4
			type: Body.STATIC
			property real x: 17
			property real y: 21
			property int nb_repe: 6
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_4.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_4.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_4.y+0.375)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_room_stock ? 0 : 1) 
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_4.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_4.y + 0.625)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_room_stock ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_4.coords(barriere_4.x + 1.25 - barriere_4.nb_repe / 4, barriere_4.y + 0.25), 
				barriere_4.coords(barriere_4.x + 1.25, barriere_4.y + 0.25), 
				barriere_4.coords(barriere_4.x + 1.25, barriere_4.y + 0.75), 
				barriere_4.coords(barriere_4.x + 1.25 - barriere_4.nb_repe / 4, barriere_4.y + 0.75)
				]
				group: root.open_room_stock ? 0 : groups.enemy_invincible
			}
		}
		
		Sensor
		{
			id: console_1
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(16.5 *  chapter_scale*2, 15.95 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{	
				if(fuzzy_value == 1)
				{
					root.open_room_life = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-7-epilogue/console/" + console_1.indice
				z: altitudes.boss
				scale :1.2
				angle: Math.PI
			}
			
			CircleCollider
			{
				group: root.open_room_life ?  0 : groups.sensor
				sensor: true
				radius: 0.5
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-0.9, 0.9), Qt.point(0.9, 0.9), Qt.point(0.9, 0.3), Qt.point(-0.9, 0.3)]
				group: groups.wall
			}
		}
		
		Sensor
		{
			id: console_2
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(21.95 *  chapter_scale*2, 19 *  chapter_scale*2)
			property bool first: true
			
			onFuzzy_valueChanged:
			{	
				if(fuzzy_value == 1)
				{
					if(first)
					{
						root.cons_room_life--
						first = false
					}
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-7-epilogue/console/" + console_2.indice
				z: altitudes.boss
				scale :1.2
				angle: Math.PI/2
			}
			
			CircleCollider
			{
				group: console_2.first ?  groups.sensor : 0
				sensor: true
				radius: 0.5
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0.3, 0.9), Qt.point(0.9, 0.9), Qt.point(0.9,-0.9), Qt.point(0.3, -0.9)]
				group: groups.wall
			}
		}
		Sensor
		{
			id: console_3
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(15 * chapter_scale * 2 , 20.95 *  chapter_scale*2)
			property bool first: true
			
			onFuzzy_valueChanged:
			{	
				if(fuzzy_value == 1)
				{
					if(first)
					{
						root.cons_room_life--
						first = false
					}
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-7-epilogue/console/" + console_3.indice
				z: altitudes.boss
				scale :1.2
				angle: Math.PI
			}
			
			CircleCollider
			{
				group: console_3.first ?  groups.sensor : 0
				sensor: true
				radius: 0.5
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-0.9, 0.9), Qt.point(0.9, 0.9), Qt.point(0.9, 0.3), Qt.point(-0.9, 0.3)]
				group: groups.wall
			}
		}
		
		Sensor
		{
			id: console_4
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(20.95 * chapter_scale * 2 , 30 *  chapter_scale*2)
			property bool first: true
			
			onFuzzy_valueChanged:
			{	
				if(fuzzy_value == 1)
				{
					if(first)
					{
						root.cons_room_stock--
						first = false
					}
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-7-epilogue/console/" + console_4.indice
				z: altitudes.boss
				scale :1.2
				angle: Math.PI/2
			}
			
			CircleCollider
			{
				group: console_4.first ?  groups.sensor : 0
				sensor: true
				radius: 0.5
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0.3, 0.9), Qt.point(0.9, 0.9), Qt.point(0.9,-0.9), Qt.point(0.3, -0.9)]
				group: groups.wall
			}
		}
		Sensor
		{
			id: console_5
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(22.95 * chapter_scale * 2 , 24 *  chapter_scale*2)
			property bool first: true
			
			onFuzzy_valueChanged:
			{	
				if(fuzzy_value == 1)
				{
					if(first)
					{
						root.cons_room_stock--
						first = false
					}
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-7-epilogue/console/" + console_5.indice
				z: altitudes.boss
				scale :1.2
				angle: Math.PI/2
			}
			
			CircleCollider
			{
				group: console_5.first ?  groups.sensor : 0
				sensor: true
				radius: 0.5
			}
			PolygonCollider
			{
				vertexes: [Qt.point(0.3, 0.9), Qt.point(0.9, 0.9), Qt.point(0.9,-0.9), Qt.point(0.3, -0.9)]
				group: groups.wall
			}
		}
		Sensor
		{
			id: console_6
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(10 * chapter_scale * 2 , 27.95 *  chapter_scale * 2)
			property bool first: true
			
			onFuzzy_valueChanged:
			{	
				if(fuzzy_value == 1)
				{
					if(first)
					{
						root.cons_room_stock--
						first = false
					}
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-7-epilogue/console/" + console_6.indice
				z: altitudes.boss
				scale :1.2
				angle: Math.PI
			}
			
			CircleCollider
			{
				group: console_6.first ?  groups.sensor : 0
				sensor: true
				radius: 0.5
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-0.9, 0.9), Qt.point(0.9, 0.9), Qt.point(0.9, 0.3), Qt.point(-0.9, 0.3)]
				group: groups.wall
			}
		}
		
		Sensor
		{
			id: suit_dress
			type:  Body.STATIC
			position: Qt.point(19 * chapter_scale * 2 , 14 *  chapter_scale * 2)
			property bool first: true
			
			onFuzzy_valueChanged:
			{
				root.suit_taken = true
				first = false
			}
			Image
			{
				material: "chapter-7-epilogue/armor-holder/" + (root.suit_taken ? "without" : "with")
				z: altitudes.boss
				scale : 0.65
				
			}
			
			CircleCollider
			{
				position.x : 0.75 
				group: suit_dress.first ?  groups.sensor : 0
				sensor: true
				radius: 0.5
			}
		}
		Sensor
		{
			id: shotgun_sensor
			type:  Body.STATIC
			position: Qt.point(19 * chapter_scale * 2 , 15 *  chapter_scale * 2)
			property bool first: true
			
			onFuzzy_valueChanged:
			{
				root.shotgun_taken = true
				first = false
			}
			Image
			{
				material: "chapter-7-epilogue/shotgun-holder/" + (root.shotgun_taken ? "without" : "with")
				z: altitudes.boss
				scale : 0.8
			}
			
			CircleCollider
			{
				position.x : 0.75 
				group: root.suit_taken ? (shotgun_sensor.first ?  groups.sensor : 0) : 0
				sensor: true
				radius: 0.5
			}
		}
		
		Sensor
		{
			id: nectaire_sensor
			duration_in: root.scene.seconds_to_frames(0.5)
			duration_out: 30
			type:  Body.STATIC
			
			onFuzzy_valueChanged:
			{	
				if(fuzzy_value == 1)
				{
					root.start_nectaire = true
				}
			}
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			PolygonCollider
			{
				vertexes: [
				nectaire_sensor.coords(23.75,23.5), 
				nectaire_sensor.coords(28.25,23.5), 
				nectaire_sensor.coords(28.25,25.5), 
				nectaire_sensor.coords(23.75,25.5)
				]
				group: root.start_nectaire ? 0 : groups.sensor
				sensor: true
			}
		}
		
		Lycop
		{
			id: lycop
			position.x: 17.5 * 4
			position.y: 13.875 * 4
			angle: Math.PI
			property real ti_local: root.ti
			equiped: root.shotgun_taken
			suit: root.suit_taken
			property bool on: false
			property bool change_ind: true
			onTi_localChanged:
			{
				if(on)
				{
					if(root.vener)
					{
						root.level_finish = true
						root.open_capsule = true
						root.fin()
					}
					else
					{
						destroy()
					}
				}
			
				//animation des pieds
				var x = position.x - last_pos.x
				var y = position.y - last_pos.y
				if( x!=0 || y!=0)
				{
					var angle_norm = calcul_ship_angle(lycop.angle)
					var angle_direc = calcul_ship_angle(Math.atan2(y, x) + Math.PI / 2 - angle_norm)
					if( (angle_direc < Math.PI / 2 + Math.PI / 12 && angle_direc > Math.PI / 2 - Math.PI / 12) || (angle_direc < -Math.PI / 2 + Math.PI / 12 && angle_direc > -Math.PI / 2 - Math.PI / 12))
					{
						if(phase == 0)
						{
							phase = 1
							indice = 0
						}
						else
						{
							if(change_ind)
							{
								indice = (indice + 1)%tab_cap_ind[phase] 
							}
							change_ind = !change_ind
						}
					}
					else
					{
						if(phase == 1)
						{
							phase = 0
							indice = 0
						}
						else
						{
							if(change_ind)
							{
								indice = (indice + 1)%tab_cap_ind[phase] 
							}
							change_ind = !change_ind
						}
					}
				}
				last_pos = Qt.point(position.x,position.y)
			}
		}
	}
	
	Q.Component
	{
		id: nectaire_factory
		Vehicle
		{
			id: nectaire
			type: Body.STATIC
			property real x:27
			property real y:25
			property int nb_repe: 18
			property color mask:  Qt.rgba(0, 0.4, 1, nectaire.shield ? Math.max(0.2, nectaire.shield / nectaire.max_shield) : 0)
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool active: root.start_nectaire
			property real ti_local: root.ti
			property real deb_spawn: root.ti
			property real time_to_spawn: root.vener ? 3 : 7
			property real deb_anim: root.ti
			property real time_to_anim: 0.1
			property int ind_im_nect: 0
			property int indice_max_nect: 18
			property bool spawn: root.vener
			
			onSpawnChanged:
			{
				drone_factory.createObject(null, {scene: ship.scene, position: nectaire.coords(nectaire.x - 0.5, nectaire.y + 2), angle: nectaire.angle})
				drone_factory.createObject(null, {scene: ship.scene, position: nectaire.coords(nectaire.x - 1.5,nectaire.y + 2), angle: nectaire.angle})
			}
			
			onTi_localChanged:
			{
				if(nectaire.shield == 0)
				{
					if(!root.vener)
					{
						root.vener = true
						nectaire.shield = nectaire.max_shield
					}
				}
				if(active)
				{
					if(deb_spawn - root.ti > time_to_spawn)
					{
						deb_spawn = root.ti
						if(!root.level_finish)
						{
							drone_factory.createObject(null, {scene: ship.scene, position: nectaire.coords(nectaire.x, nectaire.y + 2), angle: nectaire.angle})
							drone_factory.createObject(null, {scene: ship.scene, position: nectaire.coords(nectaire.x - 2,nectaire.y + 2), angle: nectaire.angle})
						}
					}
					if(deb_anim - root.ti > time_to_anim)
					{
						deb_anim = root.ti
						ind_im_nect = (ind_im_nect + 1)%indice_max_nect
					}
				}
			}
			Image
			{
				position : nectaire.coords(nectaire.x, nectaire.y + 2)
				material: "chapter-7-epilogue/nectaire/mini-drone-spawner/" + ind_im_nect
				z: altitudes.boss
			}
			Image
			{
				position : nectaire.coords(nectaire.x - 2,nectaire.y + 2)
				material: "chapter-7-epilogue/nectaire/mini-drone-spawner/" + ind_im_nect
				z: altitudes.boss
			}
			
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			
			Q.Component.onCompleted:
			{
				deb_spawn = root.ti
				deb_anim = root.ti
				nectaire.shield = max_shield
			}
			
			Repeater
			{
				count: nectaire.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (nectaire.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (nectaire.y+0.375)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI / 2
						mask: nectaire.mask
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (nectaire.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (nectaire.y + 0.625)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: nectaire.mask
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				nectaire.coords(nectaire.x + 1.25 - nectaire.nb_repe / 4, nectaire.y + 0.25), 
				nectaire.coords(nectaire.x + 1.25, nectaire.y + 0.25), 
				nectaire.coords(nectaire.x + 1.25, nectaire.y + 0.75), 
				nectaire.coords(nectaire.x + 1.25 - nectaire.nb_repe / 4, nectaire.y + 0.75)
				]
				group: root.vener ? groups.enemy_invincible : (nectaire.active ? (nectaire.shield ? groups.enemy_shield : 0) : groups.enemy_invincible) 
			}
			Image
			{
				position: nectaire.coords(nectaire.x - 1,nectaire.y + 2)
				material: "chapter-7-epilogue/nectaire"
				scale: 5
				z: altitudes.boss-0.00001
			}
			
			EquipmentSlot
			{
				equipment: Shield
				{
					max_health: 200000
					max_shield: 700	
					material_image: "chapter-2/shield-generator"
					Q.Component.onCompleted:
					{
						image.mask.a = 0
					}
				}
			}
		}
	}
	
	Q.Component
	{
		id: drone_spawner
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 3
			angle: 0
			property bool active: root.start_spawner
			property real ti_local: root.ti
			property real deb_spawn: root.ti
			property real deb_anim: root.ti
			property real time_to_spawn: 6
			property real time_to_anim: 0.1
			property int ind_im: 0
			property int indice_max: 18
			onTi_localChanged:
			{
				if(active)
				{
					if(deb_spawn - root.ti > time_to_spawn)
					{
						deb_spawn = root.ti
						if(!root.level_finish)
						{
							drone_factory.createObject(null, {scene: ship.scene, position: Qt.point(boss.position.x + boss.scale* Math.sin(boss.angle), boss.position.y - boss.scale * Math.cos(boss.angle)), angle: boss.angle})
						}
					}
					if(deb_anim - root.ti > time_to_anim)
					{
						deb_anim = root.ti
						ind_im = (ind_im + 1)%indice_max
					}
				}
			}
			
			
			Q.Component.onCompleted:
			{	
				deb_spawn = root.ti
				deb_anim = root.ti
			}
			
			Image
			{
				material: "chapter-7-epilogue/nectaire/drone-spawner/" + ind_im
				mask: health_boss ? Qt.rgba(1, health_boss.health / health_boss.max_health, health_boss.health / health_boss.max_health, 1) : Qt.rgba(1,0,0, 1)
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-0.95,-0.95), Qt.point(-0.95,0.95), Qt.point(0.95, 0.95), Qt.point(0.95,-0.95)]
				group: groups.enemy_hull_naked
			}
			
			EquipmentSlot
			{
				position.y: 0.3
				scale: 0.5
				equipment: Equipment
				{
					id: health_boss
					health: max_health
					max_health: 200
				}
			}
			
			
		}
	}
	
	Q.Component
	{
		id: ship_factory
		Body
		{
			Image
			{
				material: "folks/lycop"
			}
		}	
	}
	
	Q.Component
	{
		id: capsule_factory
		Body
		{
			property bool alive: true
			scale : 0.10
			onAliveChanged: if(!alive) destroy()
			Image
			{
				material: "chapter-7-epilogue/capsule"
				z: -0.000002
			}
		}	
	}
	Q.Component
	{
		id: space_door_factory
		Body
		{
			id: space_door
			scale: 15
			type: Body.STATIC
			property int indice: 0
			property color mask: "white"
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real z: 0
			Image
			{
				id: image
				material: "chapter-3/door/" + indice
				z: space_door.z
				mask: space_door.mask
			}
		}
	}
	Q.Component
	{
		id: apis_factory
		Body
		{
			id: apis
			property real ti_local: root.ti
			property real step: 4 / root.scene.seconds_to_frames(1)
			property color mask: Qt.rgba(1,1,1,0)
			property bool activate: false
			property var door
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property bool scan_done: false
			property bool scan_deb: true
			property bool scan_fin: false
			property real deb_scan: root.ti
			property int scan_ind: 0
			onTi_localChanged:
			{
				if(activate)
				{
					if(root.capsule)
					{
						var x = position.x - root.capsule.position.x
						var y = position.y - root.capsule.position.y
						if(Math.sqrt(x*x + y*y) < step)
						{
							position = Qt.point(root.capsule.position.x, root.capsule.position.y)
							root.capsule.alive = false
						}
						else
						{
							if(Math.abs(x) < 6 && !scan_done)
							{
								if(scan_deb)
								{
									if(scan_ind < 9)
									{
										scan_ind++
									}
									else
									{
										scan_deb = false
										deb_scan = root.ti
									}
								}
								if(!scan_deb && !scan_fin)
								{
									if(deb_scan - root.ti > 2.5)
									{
										scan_fin = true
									}
								}
								if(scan_fin)
								{
									if(scan_ind > 0)
									{
										scan_ind--
									}
									else
									{
										scan_done = true
									}
								}
								
								
							}
							else
							{
								position.x += step
							}
						}
					}
					else
					{
						if(reapp_ini)
						{
							reapp_ini = false
							door = space_door_factory.createObject(null, {scene: ship.scene, position: apis.position, z: altitudes.boss - 0.000001, angle : 0, scale: 8 })
							reapp_beg=true
						}

						//ouverture de la porte
						if(reapp_beg)
						{
							door.indice++
							if(door.indice == 10)
							{
								reapp_beg = false
							}
						}

						//disparition
						if(!reapp_beg && !reapp_end && !reapp_ini)
						{
							var a = mask.a - 0.01
							if(a <= 0)
							{
								a = 0
								reapp_end = true
							}
							mask.a = a
						}

						//fermeture porte
						if(reapp_end && door)
						{
							door.indice--
							if(door.indice == 0)
							{
								reapp_end = false
								door.alive = false
								root.sortie()
							}
						}
					}
				}
			}
		
			Image
			{
				material: "chapter-5/scan/" + scan_ind
				z: altitudes.boss - 0.000001
				mask: "orange"
				scale: 5
				position:Qt.point(0,-4.9)
			}
			Image
			{
				material: "folks/apis/4"
				mask: apis.mask
			}
		}	
	}
	
	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			id: drone
			max_speed: root.vener ? 10 : 5
			max_angular_speed: 6
			property bool lycop_throw: false
			property bool on: false
			property real ti_local: root.ti
			property real deb_anim: root.ti
			property real time_to_anim: 0.1
			property int ind_im: 0
			property int indice_max: 8
			scale: 0.5
			onTi_localChanged:
			{
				if(deb_anim - root.ti > time_to_anim)
				{
					deb_anim = root.ti
					ind_im = (ind_im + 1)%indice_max
				}
			}
			
			Q.Component.onCompleted:
			{	
				deb_anim = root.ti
			}
			
			Image
			{
				material: "chapter-7-epilogue/nectaire/drone/" + ind_im
				mask: health_boss ? Qt.rgba(1, health_boss.health / health_boss.max_health, health_boss.health / health_boss.max_health, 1) : Qt.rgba(1,0,0, 1)
			}
			CircleCollider
			{
				group: groups.drone_nectaire
			}
			EquipmentSlot
			{
				position.y: 0.3
				scale: 0.5
				equipment: Equipment
				{
					id: health_boss
					health: max_health
					max_health: 50
				}
			}
			Behaviour_attack_missile
			{
				enabled: !lycop_throw
				target_groups: [groups.lycop_human]
				range: 10000000
			}
		}
	}
	
	Q.Component
	{
		id: move_ship_to
		Animation
		{
			id: timer

			//ship_angle entre -pi et pi
			function calcul_ship_angle(angle)
			{
				var result = angle
				while(result <= -Math.PI)
				{
					result += 2 * Math.PI
				}
				while(result > Math.PI)
				{
					result -= 2 * Math.PI
				}
				return result
			}

			property real epsilon: 0.5
			property real x
			property real long_x
			property real y
			property real long_y
			property real angle
			property real long_angle
			property real time_to_move: 3
			property bool first: true
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(time < 6.9)
				{
					if(first)
					{
						ship.angle = calcul_ship_angle(ship.angle)
						long_x = ship.position.x - timer.x
						long_y = ship.position.y - timer.y
						long_angle = calcul_ship_angle(ship.angle - timer.angle)
						first = false
					}
					else
					{
						if(ship)
						{
							if(root.scene.seconds_to_frames(6.9) - root.scene.seconds_to_frames(time) < root.scene.seconds_to_frames(time_to_move) + 2)
							{
								ship.position.x -= long_x / root.scene.seconds_to_frames(time_to_move)
								ship.position.y -= long_y / root.scene.seconds_to_frames(time_to_move)
								ship.angle -= long_angle / root.scene.seconds_to_frames(time_to_move)
							}
							else
							{
								root.ship_ok()
								alive = false
							}
						}
					}
				}
			}
		}
	}
	
	
	Q.Component
	{
		id: animation_fin
		Animation
		{
			id: timer
			time: 7
			speed: -1
			property bool beg: true
			property real beg_go: time
			property real beg_cli: time
			property real time_to_cli: 0.8
			property bool emit: false
			onTimeChanged:
			{
				if(root.capsule && root.lycop_ship)
				{
					if(beg)
					{
						beg = false
						beg_go = time
					}
					if(beg_go - time < 5)
					{
						root.capsule.position.x += 5 / (1 + beg_go - time) / scene.seconds_to_frames(1)
					}
					else
					{
						root.camera_position = Qt.point(root.capsule.position.x,root.capsule.position.y)
					}
					if(root.oxygene < 20)
					{
						if(beg_cli - time > time_to_cli)
						{
							var c = (root.info_print.color.g == 1) ? "red" : "white" 
							root.info_print.color = c
							music.objectName = "chapter-7-epilogue/good_ending"
						}	
						root.oxygene -= 4 / scene.seconds_to_frames(1)
						if(!emit && root.oxygene < 10)
						{
							emit = true
							root.apis_come = true
						}
						if(root.oxygene < 0)
						{
							timer.destroy()
						}
					}
					else
					{
						root.oxygene -= 10 / scene.seconds_to_frames(1)
						beg_cli = time
					}
					if(root.lycop_ship.position.y > 150)
					{
						root.lycop_ship.position.y -= 15 / scene.seconds_to_frames(1)
					}
				}
			}
		}
	}
	
	Q.Component
	{
		id: tp_apis_factory
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property real value: 0
			property real enemy_count: 1
			property var position_enemy
			property var position_central: root.capsule ? Qt.point(root.capsule.position.x,root.capsule.position.y) : Qt.point(0,0)
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property real last_spawn: 7
			property real time_spawn: 1.5
			property var tab_drone:[apis_factory]

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: 8 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						if(new_enemy)
						{
							var i = 0
							enemy = tab_drone[i].createObject(null, {scene: scene, position: Qt.point(door.position.x, door.position.y), angle: Math.atan2(position_central.y - door.position.y, position_central.x - door.position.x )  + Math.PI / 2})
							enemy.activate = true
							new_enemy = false
							last_spawn = time
							enemy_count--
						}
						else
						{
							if(enemy.mask.a < 1)
							{
								var a = enemy.mask.a + 0.05
								if(a > 1)
								{
									a = 1
								}
								enemy.mask.a = a
							}
							else
							{
								reapp_end = true
							}
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							timer.alive = false
						}
					}
				}
			}
		}
	}
	
	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState:  state_story_3 //state_test_1 // state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					move_ship_to.createObject(null, {parent: scene, x: ship.position.x, y: ship.position.y, angle: ship.angle, time_to_move: 1})
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_dialogue_0  //state_test_1 // 
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					music.objectName = "chapter-6-conclusion/bad_ending"
					camera_position = undefined
					for(var i = 0; i < tab_spawner.length; i++)
					{
						drone_spawner.createObject(null, {scene: ship.scene, position: Qt.point(tab_spawner[i][0] * 2 * root.chapter_scale, tab_spawner[i][1] * 2 * root.chapter_scale), angle : tab_spawner[i][2] * Math.PI })
					}
					nectaire_factory.createObject(null, {scene: ship.scene})
					messages.add("lycop/normal", qsTr("begin 1"))
					messages.add("lycop/normal", qsTr("begin 2"))
					messages.add("lycop/normal", qsTr("begin 3"))
					messages.add("nectaire/normal", qsTr("begin 4"))
					messages.add("lycop/normal", qsTr("begin 5"))
					messages.add("nectaire/normal", qsTr("begin 6"))
					messages.add("lycop/normal", qsTr("begin 7"))
					messages.add("nectaire/normal", qsTr("begin 8"))
					
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_1; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State 
		{
			id: state_dialogue_game_over
			SignalTransition 
			{
				targetState: state_story_1
				signal: state_dialogue_game_over.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					game_over = true
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
			
			
			SignalTransition
			{
				targetState: state_dialogue_game_over
				signal: asphyxie
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("asphyxie 1"))
				}
			}
			
			SignalTransition
			{
				targetState: state_dialogue_0 
				signal: root.suit_takenChanged
				guard: root.suit_taken
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("suit 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0 
				signal: root.shotgun_takenChanged
				guard: root.shotgun_taken
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("shotgun 1"))
					messages.add("nectaire/normal", qsTr("shotgun 2"))
				}
			}
			//console 1
			SignalTransition
			{
				targetState: state_dialogue_0 
				signal: root.start_spawnerChanged
				guard: root.start_spawner
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("console 1 1"))
					messages.add("lycop/normal", qsTr("console 1 2"))
				}
			}
			
			//console 2
			SignalTransition
			{
				targetState: state_dialogue_0 
				signal: root.open_room_stockChanged
				guard: root.open_room_stock
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("console 2 1"))
					messages.add("lycop/normal", qsTr("console 2 2"))
					messages.add("nectaire/normal", qsTr("console 2 3"))
					messages.add("lycop/normal", qsTr("console 2 4"))
					messages.add("nectaire/normal", qsTr("console 2 5"))
				}
			}
			
			//console 3
			SignalTransition
			{
				targetState: state_dialogue_0 
				signal: root.cons_room_stockChanged
				guard: root.cons_room_stock == 2
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("console 3 1"))
					messages.add("lycop/normal", qsTr("console 3 2"))
					messages.add("nectaire/normal", qsTr("console 3 3"))
					messages.add("nectaire/normal", qsTr("console 3 4"))
				}
			}
			
			//console 4
			SignalTransition
			{
				targetState: state_dialogue_0 
				signal: root.cons_room_stockChanged
				guard: root.cons_room_stock == 1
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("console 4 1"))
				}
			}
			
			//console 5
			SignalTransition
			{
				targetState: state_dialogue_0 
				signal: root.open_room_nectaireChanged
				guard: root.open_room_nectaire
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("console 5 1"))
					messages.add("lycop/normal", qsTr("console 5 2"))
					messages.add("nectaire/normal", qsTr("console 5 3"))
				}
			}
			
			//console 5
			SignalTransition
			{
				targetState: state_dialogue_1 
				signal: root.start_nectaireChanged
				guard: root.start_nectaire
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("face a face 1"))
					messages.add("nectaire/normal", qsTr("face a face 2"))
				}
			}
		}
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_2; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}
		
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_2 
				signal: fin
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("fin 1"))
					messages.add("nectaire/normal", qsTr("fin 2"))
					messages.add("nectaire/normal", qsTr("fin 3"))
					messages.add("nectaire/normal", qsTr("fin 4"))
					//camera_position = Qt.point(ship.position.x,ship.position.y)
					move_ship_to.createObject(null, {parent: scene, x: 27.5 * 4, y: 23 * 4, angle: ship.angle, time_to_move: 1.5})
				}
			}
		}
		State {id: state_dialogue_2; SignalTransition {targetState: state_story_2_1; signal: state_dialogue_2.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2_1
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_story_3 
				signal: ship_ok
				onTriggered:
				{
					move_ship_to.createObject(null, {parent: scene, x: 29.5 * 4, y: 23 * 4, angle: ship.angle, time_to_move: 1})
				}
			}
		}
		
		State
		{
			id: state_story_3
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_3 
				signal: ship_ok
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("fin 5"))
					messages.add("lycop/normal", qsTr("fin 6"))
					messages.add("nectaire/normal", qsTr("fin 7"))
					lycop_ship = ship_factory.createObject(null, {scene: ship.scene, position: Qt.point(250,250)})
					capsule = capsule_factory.createObject(null, {scene: ship.scene, position: Qt.point(250,250)})
					animation_fin.createObject(null, {parent: ship.scene})
				}	
			}	
		}
		State 
		{
			id: state_dialogue_3
			SignalTransition 
			{
				targetState: state_story_4 
				signal: state_dialogue_3.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					camera_position = Qt.binding(function(){return Qt.point(root.capsule.position.x, root.capsule.position.y)})
				}
			}
		}
		State
		{
			id: state_story_4
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition 
			{
				targetState: state_video_end 
				signal: sortie
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-7-epilogue/end.ogg"
					function f ()
					{
						root.end_back = true
						saved_game.set("file", "game/chapter-7-credit/credit-good.qml")
						platform.set_bool("good-end")
						if(!mod_easy)
						{
							platform.set_bool("epic")
						}
						saved_game.save()
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
			}	
		}
		State
		{
			id: state_video_end
			SignalTransition
			{
				targetState: state_end
				signal: state_video_end.propertiesAssigned
				guard:!screen_video.video.file
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	
	Q.Component.onCompleted:
	{
		var a2 = ["c", "e", "n", "ne", "nei", "nw", "nwi", "s", "se", "sei", "sw", "swi", "w"]; for(var i2 in a2) jobs.run(scene_view, "preload", ["chapter-7-epilogue/background/" + a2[i2]])
		clock = clock_time.createObject(null, {parent: scene})
		jobs.run(music, "preload", ["chapter-6-conclusion/bad_ending"])
		jobs.run(music, "preload", ["chapter-7-epilogue/good_ending"])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["chapter-5/scan/" + i])
		
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/armor-holder/with"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/armor-holder/without"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/capsule"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/capsule-launcher"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-7-epilogue/console/" + i])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/control-panel"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/filling/guns"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/filling/laser-guns"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/filling/materials"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/filling/missiles"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/filling/rockets"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/furniture/bed"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/furniture/gym"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/furniture/kitchen"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/furniture/shelf"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/furniture/table"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/furniture/tv"])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["chapter-7-epilogue/lycop/armor/foot-front/" + i])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["chapter-7-epilogue/lycop/armor-gun/foot-front/" + i])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["chapter-7-epilogue/lycop/no-armor/foot-front/" + i])
		for(var i = 0; i <9; ++i) jobs.run(scene_view, "preload", ["chapter-7-epilogue/lycop/armor/foot-strafe/" + i])
		for(var i = 0; i <9; ++i) jobs.run(scene_view, "preload", ["chapter-7-epilogue/lycop/armor-gun/foot-strafe/" + i])
		for(var i = 0; i <9; ++i) jobs.run(scene_view, "preload", ["chapter-7-epilogue/lycop/no-armor/foot-strafe/" + i])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/nectaire"])
		for(var i = 0; i < 8; ++i) jobs.run(scene_view, "preload", ["chapter-7-epilogue/nectaire/drone/" + i])
		for(var i = 0; i < 18; ++i) jobs.run(scene_view, "preload", ["chapter-7-epilogue/nectaire/drone-spawner/" + i])
		for(var i = 0; i < 18; ++i) jobs.run(scene_view, "preload", ["chapter-7-epilogue/nectaire/mini-drone-spawner/" + i])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/nectaire/drone-spawner/destroyed"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/shotgun-holder/with"])
		jobs.run(scene_view, "preload", ["chapter-7-epilogue/shotgun-holder/without"])
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["folks/apis/4"])
		jobs.run(scene_view, "preload", ["chapter-2/barrier-wall"])
		jobs.run(scene_view, "preload", ["folks/lycop"])
		jobs.run(scene_view, "preload", ["folks/lycop/HR"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-3/door/" + i])
		
		groups.init(scene)
		root.info_print = screen_info_factory.createObject(screen_hud)
		root.finalize = function() {if(info_print) info_print.destroy()}
	}
}