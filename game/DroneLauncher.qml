import QtQuick 2.0 as Q
import aw.game 0.0
import "."

Weapon
{
	id: drone_launcher
	property int level: 1
	property var target_groups: groups.enemy_targets
	property int naked_hull_group: groups.enemy_hull_naked
	property var tab_group: [groups.enemy_bullet, groups.enemy_bullet, groups.enemy_missile, groups.enemy_laser]
	property int bullet_group: tab_group[level]
	property int drone_count: 0
	signal drone_destroyed()
	onDrone_destroyed: --drone_count
	period: scene.seconds_to_frames(6)
	health: max_health
	max_health: 200 +~~body.health_boost
	consumable: Consumable {available: drone_count < 5}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/drone-launcher/shooting"
		scale: slot.sound_scale
	}
	image: Image
	{
		material: "equipments/drone-launcher"
		scale: 0.5
		mask: mask_equipment(parent)
	}
	bullet_factory: Vehicle
	{
		property bool player: false
		Q.Component.onCompleted:
		{
			++weapon.drone_count
			Q.Component.destruction.connect(weapon.drone_destroyed)
			position = weapon.slot.point_to_scene(Qt.point(0, 0))
			angle = weapon.slot.angle_to_scene(0)
			weapon.sound_shooting.play()
			weapon.set_bullet_velocity(this, max_speed)
		}
		Q.Component
		{
			id: laser_gun_factory
			LaserGun
			{
				shooting: behaviour.shooting
				bullet_group: weapon.bullet_group
				level: 2
			}
		}
		Q.Component
		{
			id: missile_factory
			MissileLauncher
			{
				shooting: behaviour.shooting
				target_groups: weapon.target_groups
				bullet_group: weapon.bullet_group
				level: 2
			}
		}
		Q.Component
		{
			id: gun_factory
			Gun
			{
				shooting: behaviour.shooting
				bullet_group: weapon.bullet_group
				level: 2
			}
		}
		scale: [0.7, 0.7, 0.7, 1][weapon.level]
		max_speed: 10
		icon: 2
		icon_color: (weapon.naked_hull_group & 1) ? "#F00" : "#0F0"
		Image
		{
			material: "folks/heter/2"
		}
		CircleCollider
		{
			group: weapon.naked_hull_group
		}
		EquipmentSlot
		{
			position.y: 0.5
			scale: 0.8
			Q.Component.onCompleted: equipment = [gun_factory, gun_factory, missile_factory, laser_gun_factory][weapon.level].createObject(null, {slot: this})
		}
		Behaviour_attack_distance
		{
			id: behaviour
			target_groups: weapon.target_groups
			max_distance: 100
			target_distance: 5
		}
	}
}
