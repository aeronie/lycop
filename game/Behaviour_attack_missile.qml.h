#include "Behaviour_attack.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct Behaviour_attack_missile
: Behaviour_attack
{
	AW_DECLARE_OBJECT_STUB(Behaviour_attack_missile)
	AW_DECLARE_PROPERTY_STORED(int, range) = 0;

	void attack(b2Vec2 const &target) override
	{
		body()->set_target_direction(target - body()->position());
		wait();
	}

	void wait() override
	{
		if(range_)
		{
			--range_;
			body()->set_relative_velocity(b2Vec2(0, -1));
		}
		else
		{
			body()->deleteLater();
		}
	}
};

AW_DEFINE_OBJECT_STUB(Behaviour_attack_missile)
AW_DEFINE_PROPERTY_STORED(Behaviour_attack_missile, range)

}
}
}
