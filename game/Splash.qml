import QtQuick 2.0
import QtQuick.Layouts 1.0
import aw 0.0

Item
{
	id: root
	property int current_index: -1
	default property alias children: row.children
	Keys.onEscapePressed: step(1)
	function step(step)
	{
		current_index += step
		if(current_index < 0) current_index = 0
		if(current_index > row.visibleChildren.length - 1)
		{
			current_index = row.visibleChildren.length - 1
			enabled = false
			return
		}
		timer.interval = row.visibleChildren[current_index].z
		timer.restart()
	}
	Timer
	{
		id: timer
		running: root.enabled
		interval: 1
		repeat: true
		onTriggered: step(1)
	}
	RowLayout
	{
		anchors.fill: parent
		spacing: 0
		MouseArea
		{
			Layout.fillHeight: true
			Layout.fillWidth: true
			Layout.preferredWidth: 1
			onClicked: step(-1)
			cursor: "arrow-w"
		}
		MouseArea
		{
			Layout.fillHeight: true
			Layout.fillWidth: true
			Layout.preferredWidth: 2
			onClicked: step(1)
			cursor: "arrow-e"
		}
	}
	Row
	{
		id: row
		x: -current_index * root.width
		Behavior on x {NumberAnimation {}}
	}
	Component.onCompleted:
	{
		for(var i = 0; i < row.visibleChildren.length; ++i)
		{
			row.visibleChildren[i].width = Qt.binding(function(){return root.width})
			row.visibleChildren[i].height = Qt.binding(function(){return root.height})
		}
	}
}
