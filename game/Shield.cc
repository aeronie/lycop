#include "Shield.h"
#include "meta.private.h"

namespace aw {
namespace game {

AW_DEFINE_OBJECT_STUB(Shield)
AW_DEFINE_PROPERTY_STORED(Shield, max_shield)
AW_DEFINE_PROPERTY_STORED(Shield, shield_regeneration)

}
}
