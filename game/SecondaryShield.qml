import aw.game 0.0

SecondaryShield
{
	property int level: 1
	readonly property int time: body.time_before_secondary_shield_regeneration
	readonly property int period: body.max_time_before_secondary_shield_regeneration
	health: max_health
	max_health: 200 +~~body.health_boost
	max_shield: [0, 200, 300][level]
	image: Image
	{
		material: "equipments/secondary-shield-generator"
		scale: 0.5
		mask: mask_equipment(parent)
	}
}
