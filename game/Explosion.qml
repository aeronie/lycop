import QtQuick 2.4 as Q
import aw.game 0.0

Body
{
	id: component
	type: Body.STATIC
	position.x: body.position.x + (Math.random() - 0.5) * body.scale
	position.y: body.position.y + (Math.random() - 0.5) * body.scale
	scale: body.scale
	property real sound_scale: 10
	property color mask: "white"
	property var material: "explosion"
	Image
	{
		id: image
		material: component.material
		mask: component.mask
	}
	Sound
	{
		id: sound
		objectName: "explosion/sound"
		scale: component.sound_scale
	}
	Q.NumberAnimation
	{
		target: image
		property: "scale"
		from: 0
		to: 1
		running: true
		easing.type: Q.Easing.OutCubic
		onStarted:
		{
			sound.play()
			image.scale_changed.connect(update)
		}
	}
	function update()
	{
		image.mask.a = 1 - image.scale
		if(!image.mask.a) component.deleteLater()
	}
}
