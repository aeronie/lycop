import QtQuick 2.0 as Q
import aw.game 0.0
import "."

Vehicle
{
	id: ship
	property bool player: true
	health_regen: [0,25,50][technologies.nanomachines.level] * scene.time_step
	readonly property var positions: [[-0.36, -0.37], [0.36, -0.37], [0, 0], [-0.63, 0.35], [0.63, 0.35], [-0.23, 0.58], [0.23, 0.58]]
	readonly property Consumable consumable: Consumable {energy: 50 * scene.time_step}
	readonly property int health_boost: [0, 50, 100][technologies.hull.level]
	readonly property CircleCollider collider: collider3
	max_angular_speed: 3
	icon: 3
	icon_color: "lime"
	Q.Component.onDestruction: undefined // Ça a l'air inutile, mais sans ça je ne peux pas utiliser ship.Component.destruction.connect(). Je soupçonne que les signaux ne sont attachés par Component que si on les utilise.

	onScaleChanged:
	{
		collider1.update_transform()
		collider2.update_transform()
	}
	Image
	{
		material: "folks/lycop"
	}
	CircleCollider
	{
		id: collider1
		group: groups.auto_hull(parent, true)
		radius: 0.75
	}
	CircleCollider
	{
		id: collider2
		group: groups.auto_shield(parent, true)
	}
	CircleCollider
	{
		id: collider3
		density: 0
		radius: 0
	}
	Repeater
	{
		count: positions.length
		Q.Component
		{
			EquipmentSlot
			{
				position.x: positions[index][0]
				position.y: positions[index][1]
				scale: 0.4
				onEquipmentChanged:
				{
					if(equipment) saved_game.set("equipments/" + index, data.objectName)
					else saved_game.remove("equipments/" + index)
				}
			}
		}
	}
	Image
	{
		material: "equipments/shield"
		mask: mask_shield(parent)
	}
	Sound
	{
		id: sound_built
		objectName: "equipments/builder/destroyed"
	}
	readonly property var slots:
	{
		var selected = []
		var all = children
		for(var i = 0; i < all.length; ++i) if(all[i].equipment !== undefined) selected.push(all[i])
		return selected
	}
	readonly property bool shooting:
	{
		for(var i = 0; i < slots.length; ++i) if(slots[i].equipment && slots[i].equipment.shooting) return true
		return false
	}
	Q.Component
	{
		id: builder_factory
		EquipmentBuilder
		{
			factory: slot && slot.data ? slot.data.factory : null
			period: scene.seconds_to_frames(5)
			health: max_health
			max_health: 1
			Q.Component.onDestruction:
			{
				connect_shooting(slot_id)
				sound_built.play()
			}
			image: Image
			{
				material: "equipments/builder"
				scale: 0.5
				mask: Qt.rgba(1, 1, 1, .6 + .8 * Math.atan(100 * Math.cos(100 * Math.pow(1 - time / period, 2))) / Math.PI)
			}
		}
	}
	Q.Connections
	{
		target: scene
		onFrame_end:
		{
			function use(control)
			{
				if(control.value) control.enable(consumable.available)
				if(control.value) consumable.consumed()
				return control.value
			}
			window.energy = Math.min(window.energy + energy_regeneration * scene.time_step, max_energy)
			max_speed = use(controls.speed_boost) ? 15 : 7.5
			weapon_regeneration = use(controls.weapons_boost) ? 2 : 1
			shield_regeneration_boost = use(controls.shield_boost) ? 2 : 1
		}
	}
	function set_equipment(slot, data)
	{
		slots[slot].data = data
		slots[slot].equipment = builder_factory.createObject(null, {slot: slots[slot], slot_id: slot})
	}
	function restore_equipments()
	{
		for(var i = 0; i < slots.length; ++i)
		{
			var a = saved_game.get("equipments/" + i)
			if(!a) continue
			slots[i].angle = saved_game.get("slots/" + i + "/angle", 0)
			slots[i].data = technologies.get(a)
			slots[i].equipment = slots[i].data.factory.createObject(null, {slot: slots[i]})
			connect_shooting(i)
		}
		update_equipments()
		shield = max_shield
		window.energy = max_energy
	}
	function connect_shooting(slot_id)
	{
		var a = slots[slot_id].equipment.set_shooting
		if(a) controls.slots[slot_id].value_changed.connect(a)
		if(a && !slots[slot_id].equipment.consumable) controls.weapons.value_changed.connect(a)
		if(slots[slot_id].equipment.slot_id !== undefined) slots[slot_id].equipment.slot_id = slot_id
	}

	function repair_cost(slot_id)
	{
		var s = slots[slot_id]
		if(!s || !s.data) return 0
		var e = s.equipment
		return s.data.cost * (e && e.max_health ? 1 - e.health / e.max_health : 1)
	}
	function repair_all_cost()
	{
		var x = 0
		for(var i in slots) x += repair_cost(i)
		return x
	}
	function can_repair(cost)
	{
		return cost > 0 && cost <= saved_game.matter
	}
	function repair(slot_id)
	{
		var s = slots[slot_id]
		var e = s.equipment
		if(e)
		{
			if(e.health < e.max_health)
			{
				e.deleteLater()
				set_equipment(slot_id, s.data)
			}
		}
		else
		{
			set_equipment(slot_id, s.data)
		}
	}
	function repair_all()
	{
		for(var i in slots) if(slots[i].data) repair(i)
	}
	Q.Connections
	{
		target: controls.repair
		onValue_changed: if(controls.repair.value)
		{
			var cost = repair_all_cost()
			if(!can_repair(cost)) return
			saved_game.add_matter(-cost)
			repair_all(cost)
		}
	}
}
