import aw.game 0.0

Sensor
{
	property string text: ""
	type: Body.STATIC
	icon: 1
	icon_color: "cyan"
	scale: 10
	property bool active: true
	CircleCollider
	{
		group: active ? groups.sensor : 0
		sensor: true
	}
	onValueChanged:
	{
		if(value) label.text = text
		else if(label.text == text) label.text = ""
	}
}
