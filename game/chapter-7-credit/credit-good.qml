import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	property alias ship: ship
	property var camera_position: Qt.point(0, 0)
	property var last_ship_position: Qt.point(0, 0)
	property var time
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	property var tab: tab_cre.tab_final
	property string text_affiche : 
	{
		return tab[indice_tab][0]
	}
	signal fin
	property int indice_tab: 0
	property real affiche : 0
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	property real ti: 0
	property real deb_allume: 0
	property real time_to_allume: 1
	property real deb_affiche: 0
	property real time_to_affiche: 5
	property real deb_eteint: 0
	property real time_to_eteint: 1
	property real deb_not_affiche: 0
	property real time_to_not_affiche: 0.5
	property real deb_last: ti
	property real time_to_last: 6
	property bool is_allume : true
	property bool is_affiche : false
	property bool is_eteint : false
	property bool is_not_affiche : false
	property bool active: true
	property bool stop: false
	property bool travel_map_2: true
	property var label
	property int nb_drone: 0
	property int door_open: 0
	property var wave_spawner
	property bool new_wave: false
	property bool first: true
	property var tab_weapons: [coil_gun_factory,laser_gun_factory,laser_bouncer_factory,qui_poutre_factory,vortex_factory]
	onTiChanged:
	{
		if(first)
		{
			if(root.ti < -7)
			{
				first = false
				new_wave = true
			}
		}
		else
		{
			if(!new_wave && door_open == 0 && nb_drone == 0)
			{
				new_wave = true
			}
		}
		if(active)
		{
			if(is_allume)
			{
				if(deb_allume - root.ti < time_to_allume)
				{
					var a = (deb_allume - root.ti) / time_to_allume
					if(a > 1)
					{
						a = 1
					}
					affiche = a
				}
				else
				{
					is_allume = false
					is_affiche = true
					deb_affiche = root.ti
				}
			}
			if(is_affiche)
			{
				if(deb_affiche - root.ti > time_to_affiche)
				{
					if(indice_tab == tab.length - 1)
					{
						active = false
						deb_last = root.ti
					}
					is_affiche = false
					is_eteint = true
					deb_eteint = root.ti
				}
			}
			if(is_eteint)
			{
				if(deb_eteint - root.ti < time_to_eteint)
				{
					var a = 1 - (deb_eteint - root.ti) / time_to_eteint
					if(a < 0)
					{
						a = 0
					}
					affiche = a
				}
				else
				{
					is_eteint = false
					is_not_affiche = true
					deb_not_affiche = root.ti
				}
			}
			if(is_not_affiche)
			{
				
				if(deb_not_affiche - root.ti > time_to_not_affiche)
				{
					var ind = indice_tab + 1
					if(ind < tab.length)
					{
						indice_tab = ind
					}
					is_not_affiche = false
					is_allume = true
					deb_allume = root.ti
				}
			}
		}
	}
	Q.Component
	{
		id: laser_gun_factory
		LaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: laser_bouncer_factory
		LaserBouncer
		{
			range_bullet: scene.seconds_to_frames(2)
			level: 1
			shooting: true
		}
	}
	Q.Component
	{
		id: qui_poutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: vortex_factory
		VortexGun
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: coil_gun_factory
		CoilGun
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			level: 2
		}
	}
	scene: Scene
	{
		running: false
		
		Tab_credit
		{
			id: tab_cre
			credit_not_title: true
		}
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Ship
		{
			id: ship
			icon_color: "transparent"
			function health()
			{
				if(!ship) return 0
				var x = 0
				for(var i = 0; i < ship.slots.length; ++i)
				{
					var a = ship.slots[i].equipment
					if(a) x += a.health
				}
				return x
			}
			
			Q.Component.onDestruction:
			{
				if(health() < 1)
				{
					platform.set_bool("kamikaze")
				}
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}
	}
	
	Q.Component
	{
		id: label_factory
		Q.Label
		{
			anchors.top : screen_hud.top
			anchors.horizontalCenter : screen_hud.horizontalCenter
			font.capitalization: Q.Font.MixedCase
			font.pointSize : 6 * (Math.sqrt(window.width * window.height / 640 / 480)) 
			color: Qt.rgba(1,1,1,root.affiche)
			text: root.text_affiche
		}
	}
	
	Q.Component
	{
		id: space_door_factory
		Body
		{
			id: space_door
			scale: 15
			type: Body.STATIC
			property int indice: 0
			property color mask: "white"
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real z: 0
			Image
			{
				id: image
				material: "chapter-3/door/" + indice
				z: space_door.z
				mask: space_door.mask
			}
		}
	}
	Q.Component
	{
		id: drone_police_2
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 128 - 1, y / 128 - 1)} // plink fait des images de drone en 256*256
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 2
			icon: 2
			icon_color: "red"
			scale: 1.5
			max_speed: 9
			loot_factory: Repeater
			{
				count: 1
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/2"
			}
			Q.Component.onCompleted:
			{
				root.nb_drone++
			}
			Q.Component.onDestruction:
			{
				root.nb_drone--
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(74,128)
				equipment: ShieldCanceler
				{
					range_bullet: 30
					level: 0
					shooting: true
					period: root.scene.seconds_to_frames(8)
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(182,128)
				Q.Component.onCompleted: equipment = root.tab_weapons[2].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_police_3
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 128 - 1, y / 128 - 1)} // plink fait des images de drone en 256*256
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 2
			icon: 2
			icon_color: "red"
			scale: 1.5
			max_speed: 9
			loot_factory: Repeater
			{
				count: 1
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/3"
			}
			Q.Component.onCompleted:
			{
				root.nb_drone++
			}
			Q.Component.onDestruction:
			{
				root.nb_drone--
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(128,176)
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(74,100)
				Q.Component.onCompleted: equipment = root.tab_weapons[4].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(182,100)
				Q.Component.onCompleted: equipment = root.tab_weapons[4].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_police_5
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 2
			icon: 2
			icon_color: "red"
			scale: 2
			max_speed: 9
			loot_factory: Repeater
			{
				count: 2
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/5"
			}

			Q.Component.onCompleted:
			{
				root.nb_drone++
			}
			Q.Component.onDestruction:
			{
				root.nb_drone--
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(406,269)
				Q.Component.onCompleted: equipment = root.tab_weapons[3].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(106,269)
				Q.Component.onCompleted: equipment = root.tab_weapons[3].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(200,310)
				Q.Component.onCompleted: equipment = root.tab_weapons[4].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(312,310)
				Q.Component.onCompleted: equipment = root.tab_weapons[4].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(256, 411)
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: wave_assault_factory
		Animation
		{
			id: timer
			property var tab_pos: [
					[-20,10],
					[-20,-10],
					[20,-10],
					[20,10],
					[0,-30],
					[0,30],
					]

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(root.new_wave)
				{
					for(var i = 0 ; i < tab_pos.length ; ++i)
					{
						root.door_open++
						tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(ship.position.x + tab_pos[i][0], ship.position.y + tab_pos[i][1]), position_central: Qt.point(ship.position.x,ship.position.y)})
					}
					root.new_wave = false
				}
			}
		}
	}
	Q.Component
	{
		id: tp_enemy_factory
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property real value: 0
			property real enemy_count: 1
			property var position_enemy
			property var position_central
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property real last_spawn: 7
			property real time_spawn: 2
			property var tab_drone:[drone_police_2,drone_police_3,drone_police_5]

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: 8 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						if(new_enemy)
						{
							var i = random_index(tab_drone.length)
							enemy = tab_drone[i].createObject(null, {scene: scene, position: Qt.point(door.position.x, door.position.y), angle: Math.atan2(position_central.y - door.position.y, position_central.x - door.position.x )  + Math.PI / 2})
							new_enemy = false
							last_spawn = time
							enemy_count--
						}
						else
						{
							if(enemy_count == 0)
							{
								reapp_end = true
							}
							else
							{
								if(last_spawn - time > time_spawn)
								{
									new_enemy = true
								}
							}
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							timer.alive = false
							root.door_open--
						}
					}
				}
			}
		}
	}
	
	StateMachine
	{
		begin: state_story_0 // state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_test_1 //state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					//camera_position = undefined
					saved_game.add_science(30)
					saved_game.add_matter(5000)
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					wave_spawner = wave_assault_factory.createObject(null, {parent: scene})
					saved_game.add_matter(5000)
					music.objectName = "chapter-7-epilogue/good_ending"
					camera_position = undefined
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
	}

	Q.Component.onCompleted:
	{
		label = label_factory.createObject(screen_hud)
		groups.init(scene, true)
		tab.push([qsTr("vrai fin")])
		finalize = function() {if(label) label.destroy()}
		jobs.run(music, "preload", ["chapter-7-epilogue/good_ending"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-3/door/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		jobs.run(scene_view, "preload", ["folks/insec/1"])
		jobs.run(scene_view, "preload", ["folks/insec/2"])
		jobs.run(scene_view, "preload", ["folks/insec/3"])
		jobs.run(scene_view, "preload", ["folks/insec/5"])
	}
}
