import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	property alias ship: ship
	property var camera_position: Qt.point(0, 0)
	property var last_ship_position: Qt.point(0, 0)
	property var time
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	property var tab: tab_cre.tab_final
	readonly property bool secret_base_heter: saved_game.self.get("secret_base_heter", false)
	readonly property bool secret_cathedrale: saved_game.self.get("secret_cathedrale", false)
	readonly property bool secret_foreuse: saved_game.self.get("secret_foreuse", false)
	readonly property bool secret_tomate: saved_game.self.get("secret_tomate", false)
	readonly property bool secret_commandement: saved_game.self.get("secret_commandement", false)
	readonly property bool secret_apis: saved_game.self.get("secret_apis", false)
	property var tab_secret: [secret_base_heter, secret_foreuse, secret_cathedrale, secret_tomate, secret_apis, secret_commandement]
	property string text_affiche : 
	{
		return tab[indice_tab][0]
	}
	signal fin
	property int indice_tab: 0
	property bool last_level: true
	property bool credit: true
	property real affiche : 0
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	property real ti: 0
	property real deb_allume: 0
	property real time_to_allume: 1
	property real deb_affiche: 0
	property real time_to_affiche: 3
	property real deb_eteint: 0
	property real time_to_eteint: 1
	property real deb_not_affiche: 0
	property real time_to_not_affiche: 0.5
	property real deb_last: ti
	property real time_to_last: 6
	property bool is_allume : true
	property bool is_affiche : false
	property bool is_eteint : false
	property bool is_not_affiche : false
	property bool active: true
	property bool stop: false
	property var label
	onTiChanged:
	{
		if(!stop)
		{
			if(active)
			{
				if(is_allume)
				{
					if(deb_allume - root.ti < time_to_allume)
					{
						var a = (deb_allume - root.ti) / time_to_allume
						if(a > 1)
						{
							a = 1
						}
						affiche = a
					}
					else
					{
						is_allume = false
						is_affiche = true
						deb_affiche = root.ti
					}
				}
				if(is_affiche)
				{
					if(deb_affiche - root.ti > time_to_affiche)
					{
						if(indice_tab == tab.length - 1)
						{
							active = false
							deb_last = root.ti
						}
						is_affiche = false
						is_eteint = true
						deb_eteint = root.ti
					}
				}
				if(is_eteint)
				{
					if(deb_eteint - root.ti < time_to_eteint)
					{
						var a = 1 - (deb_eteint - root.ti) / time_to_eteint
						if(a < 0)
						{
							a = 0
						}
						affiche = a
					}
					else
					{
						is_eteint = false
						is_not_affiche = true
						deb_not_affiche = root.ti
					}
				}
				if(is_not_affiche)
				{
					
					if(deb_not_affiche - root.ti > time_to_not_affiche)
					{
						var ind = indice_tab + 1
						if(ind < tab.length)
						{
							indice_tab = ind
						}
						is_not_affiche = false
						is_allume = true
						deb_allume = root.ti
					}
				}
			}
			else
			{
				if(deb_last - root.ti > time_to_last)
				{
					if(deb_eteint - root.ti < time_to_eteint)
					{
						var a = 1 - (deb_eteint - root.ti) / time_to_eteint
						if(a < 0)
						{
							a = 0
						}
						affiche = a
					}
					else
					{
						stop = true
						fin()
					}
				}
				else
				{
					deb_eteint = root.ti
				}
			}
		}
	}
	
	scene: Scene
	{
		running: false
		
		Tab_credit
		{
			id: tab_cre
			credit_not_title: true
		}
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Ship
		{
			id: ship
			position.x : 100
			icon_color: "transparent"
			Q.Component.onDestruction:
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}
	}
	
	Q.Component
	{
		id: label_factory
		Q.Label
		{
			anchors.centerIn : screen_hud
			font.capitalization: Q.Font.MixedCase
			font.pointSize : 8 * (Math.sqrt(window.width * window.height / 640 / 480)) 
			color: Qt.rgba(1,1,1,root.affiche)
			text: root.text_affiche
		}
	}
	
	Q.Component
	{
		id: info_factory
		Q.Label
		{
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
			font.pointSize : 12
			font.capitalization: Q.Font.MixedCase
			enabled: false
		}
	}
	Q.Component
	{
		id: text_factory
		Text
		{
			id: info
			property string mat: "chapter-7-credit/image_secret/0"
			property int indice: 0
			scale: 12
			Image
			{
				material: info.mat
				mask: Qt.rgba(1, 1, 1, root.tab_secret[indice] ? 0.25 : 1)
				z: -0.00011
			}
		}
	}
	Q.Component
	{
		id: valid_factory
		Body
		{
			id: info
			property string mat: "chapter-7-credit/validation"
			property int indice: 0
			scale: 10
			Image
			{
				material: info.mat
				mask: Qt.rgba(1, 1, 1, root.tab_secret[indice] ? 1 : 0)
				z: -0.0001
			}
		}
	}
	
	StateMachine
	{
		begin: state_story_0 // state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_test_1 //state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					//camera_position = undefined
					saved_game.add_science(30)
					saved_game.add_matter(5000)
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				onTriggered: music.objectName = "chapter-6-conclusion/bad_ending"
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: scene; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_story_2
				signal: fin
				onTriggered:
				{
					camera_position = undefined
					label.destroy()
					label = info_factory.createObject(screen_hud)
					var tab_pos = [
					[-40,-20],
					[0,-20],
					[40,-20],
					[-40,20],
					[0,20],
					[40,20]
					]
					var tab_text = [
					qsTr("info secret_base_heter"),
					qsTr("info secret_foreuse"),
					qsTr("info secret_cathedrale"),
					qsTr("info secret_tomate"),
					qsTr("info secret_apis"),
					qsTr("info secret_commandement"),
					]
					
					for(var i = 0 ; i < root.tab_secret.length; i++)
					{
						valid_factory.createObject(null, {scene: ship.scene, position: Qt.point(ship.position.x + tab_pos[i][0], ship.position.y + tab_pos[i][1]), indice: i})
						text_factory.createObject(null, {scene: ship.scene, position: Qt.point(ship.position.x + tab_pos[i][0], ship.position.y + tab_pos[i][1]), text: tab_text[i], indice: i, mat: "chapter-7-credit/image_secret/" + i})
					}
					
				}
			}
		}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
		}
	}

	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["chapter-7-credit/validation"])
		for(var i = 0; i < 6; ++i) jobs.run(scene_view, "preload", ["chapter-7-credit/image_secret/" + i])
		label = label_factory.createObject(screen_hud)
		groups.init(scene)
		tab.push([qsTr("fausse fin")])
		finalize = function() {if(label) label.destroy()}
		jobs.run(music, "preload", ["chapter-6-conclusion/bad_ending"])
	}
}
