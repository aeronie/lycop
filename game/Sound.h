#pragma once

#include <QMatrix4x4>

#include "Frame.h"

namespace aw {
namespace game {

struct Sound
: Frame
{
	AW_DECLARE_OBJECT_STUB(Sound)
	AW_DECLARE_PROPERTY_STORED(bool, looping) = false;
	AW_DECLARE_PROPERTY_STORED(bool, playing) = false;
	AW_DECLARE_PROPERTY_STORED(QMatrix4x4, transform);
protected:
	Sound() = default;
	~Sound();
public:
	Q_SLOT void play();
	Sound **pointer_ = 0;
};

}
}
