#include "Mine.h"
#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {

void Mine::update(const Observer<-1>::Tag &tag)
{
	Body::update(tag);
	if(body_)
	{
		Observer<1>::connect(scene()->signal_frame_begin());
	}
}

void Mine::update(const Observer<1>::Tag &)
{
	if(launch_time())
	{
		set_launch_time(launch_time() - 1);
	}
	else if(activation_time())
	{
		set_activation_time(activation_time() - 1);
	}
	else
	{
		Observer<1>::connect(0);
	}
}

AW_DEFINE_OBJECT_STUB(Mine)
AW_DEFINE_PROPERTY_STORED(Mine, launch_time)
AW_DEFINE_PROPERTY_STORED(Mine, activation_time)

}
}
