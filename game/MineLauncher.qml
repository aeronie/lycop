import QtQuick 2.0 as Q
import aw.game 0.0

Weapon
{
	property int level: 1
	period: scene.seconds_to_frames([0 , 1 / 2 , 1 / 2.3] [level])
	health: max_health
	max_health: 250 +~~body.health_boost
	image: Image
	{
		material: "equipments/mine-launcher"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/mine-launcher/shooting"
		scale: slot.sound_scale
	}
	bullet_factory: Mine
	{
		Q.Component.onCompleted:
		{
			position = weapon.slot.point_to_scene(Qt.point(0, -3))
			angle = weapon.slot.angle_to_scene(0)
			weapon.sound_shooting.play()
			weapon.set_bullet_velocity(this, 8)

			var launch_group = groups.mine
			launch_time_changed.connect(function(x){if(!x){
				launch_collider.group = launch_group
				}})

			var activation_group = groups.mine_aoe
			activation_time_changed.connect(function(x){if(!x){
				activation_collider.group = activation_group
				image.mask = "white"
				}})
		}
		scale: 0.5
		angular_velocity: Math.random() * 20 - 10
		damping: 2
		angular_damping: 8
		launch_time: scene.seconds_to_frames(0.4)
		activation_time: scene.seconds_to_frames(3)
		Image
		{
			id: image
			material: "equipments/mine"
			mask: "grey"
		}
		CircleCollider
		{
			id: launch_collider
			radius: 0.5
		}
		CircleCollider
		{
			id: activation_collider
			radius: 3 / body.scale
			sensor: true
		}
		loot_factory: Shockwave
		{
			position: body.position
			scale: 0.1
			target_scale: 2.4
			damages: 80
			duration: scene.seconds_to_frames(0.2)
			Q.Component.onCompleted:
			{
				scale_changed.connect(collider.update_transform)
				sound_explosion.play()
			}
			Sound
			{
				id: sound_explosion
				objectName: "equipments/mine-launcher/explosion"
				scale: 10
			}
			Image
			{
				material: "explosion"
			}
			CircleCollider
			{
				id: collider
				group: groups.mine_shockwave
				sensor: true
			}
		}
	}
}
