#include "ActiveEquipment.h"
#include "Body.h"
#include "EquipmentSlot.h"
#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct Nanomachines
: ActiveEquipment
{
	AW_DECLARE_OBJECT_STUB(Nanomachines)
	AW_DECLARE_PROPERTY_STORED(float, regeneration) = 0;

	void activate() override
	{
		std::vector<Equipment *> equipments;

		for(QObject *child: body()->children())
		{
			if(EquipmentSlot *slot = static_qobject_cast<EquipmentSlot>(child))
			{
				if(slot && slot->equipment() && slot->equipment()->health() < slot->equipment()->max_health())
				{
					equipments.push_back(slot->equipment());
				}
			}
		}
		for(Equipment *e: equipments)
		{
			e->set_health(std::min(e->health() + regeneration() * scene()->time_step() / float(equipments.size()), e->max_health()));
		}
	}
};

AW_DEFINE_OBJECT_STUB(Nanomachines)
AW_DEFINE_PROPERTY_STORED(Nanomachines, regeneration)

}
}
}
