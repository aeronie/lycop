import QtQuick 2.0

QtObject
{
	property real shield: 0.0002
	property real bullet: 0.0001
	property real boss: -0.00001
	property real background_near: -0.001
	property real background_away: -0.5
}
