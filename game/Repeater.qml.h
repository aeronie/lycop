#include <QtQml>

#include "meta.private.h"

namespace aw {
namespace game {
namespace {

static int qml_children_count(QQmlListProperty<QQmlComponent> *)
{
	Q_UNREACHABLE();
}

static QQmlComponent *qml_children_at(QQmlListProperty<QQmlComponent> *, int)
{
	Q_UNREACHABLE();
}

static void qml_children_append(QQmlListProperty<QQmlComponent> *list, QQmlComponent *object)
{
	object->setParent(list->object);
}

struct Repeater
: QObject
, QQmlParserStatus
{
	AW_DECLARE_OBJECT_STUB(Repeater)
	Q_INTERFACES(QQmlParserStatus)
	Q_CLASSINFO("DefaultProperty", "children")
	Q_PROPERTY(QQmlListProperty<QQmlComponent> children READ qml_children)
	AW_DECLARE_PROPERTY_STORED(int, count) = 1;

	void classBegin() override
	{
	}

	void componentComplete() override
	{
		for(QObject *child: children())
		{
			if(QQmlComponent *factory = dynamic_cast<QQmlComponent *>(child))
			{
				for(int i = 0; i < count_; ++i)
				{
					QQmlContext *context = new QQmlContext(factory->creationContext());
					context->setContextProperty("index", i);
					QObject *object = factory->beginCreate(context);
					Q_ASSERT(object);
					Q_ASSERT(parent());
					object->setParent(parent());
					context->setParent(object);
					factory->completeCreate();
				}
			}
		}

		deleteLater();
	}

	QQmlListProperty<QQmlComponent> qml_children()
	{
		return QQmlListProperty<QQmlComponent>(this, 0, qml_children_append, qml_children_count, qml_children_at, 0);
	}
};

AW_DEFINE_OBJECT_STUB(Repeater)
AW_DEFINE_PROPERTY_STORED(Repeater, count)

}
}
}
