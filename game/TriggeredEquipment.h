#pragma once

#include "ActiveEquipment.h"

namespace aw {
namespace game {

struct TriggeredEquipment
: ActiveEquipment
{
	AW_DECLARE_OBJECT_STUB(TriggeredEquipment)
	AW_DECLARE_PROPERTY_STORED(int, period) = 0;
	AW_DECLARE_PROPERTY_STORED_READONLY(int, time) = 0;
protected:
	TriggeredEquipment() = default;
	~TriggeredEquipment() = default;
	void update(Observer<4>::Tag const &) override;
	void activate() override;
};

}
}
