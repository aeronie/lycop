#pragma once

#include <Dynamics/b2World.h>
#include <memory>

#include "ArrayView.h"
#include "Object.h"
#include "meta.h"

namespace aw {
namespace game {

struct Body;
struct Image;
struct Sound;

struct ContactEvents
: QObject
{
	Q_OBJECT
public:
	ContactEvents();
	Q_SIGNAL void begin(aw::game::Body *, aw::game::Body *, b2Vec2) const;
	Q_SIGNAL void end(aw::game::Body *, aw::game::Body *) const;
};

struct Scene
: Object
, b2DestructionListener
, b2ContactFilter
, b2ContactListener
{
	AW_DECLARE_OBJECT_STUB(Scene)
	AW_DECLARE_PROPERTY(bool, running)
	AW_DECLARE_PROPERTY_READONLY(float, time_step) ///< s
	AW_DECLARE_PROPERTY_STORED_READONLY(unsigned, time) = 0;
	AW_DECLARE_PROPERTY_READONLY(aw::ArrayView<aw::game::Sound *>, sounds)
	AW_DECLARE_PROPERTY_READONLY(aw::ArrayView<aw::game::Image *>, images)
	AW_DECLARE_SIGNAL(frame_begin)
	AW_DECLARE_SIGNAL(frame_end)
	void update(QObject *, QMatrix4x4 const &);
	std::map<std::pair<int, int>, ContactEvents> contacts_;
	std::vector<Sound *> sounds_;
	std::vector<Image *> images_;
	int timer_ = 0;
protected:
	Scene();
	~Scene();
	void timerEvent(QTimerEvent *) override;
	bool ShouldCollide(b2Fixture *, b2Fixture *) override;
	void PreSolve(b2Contact *, const b2Manifold *) override;
	void BeginContact(b2Contact *) override;
	void EndContact(b2Contact *) override;
	void SayGoodbye(b2Joint *) override;
	void SayGoodbye(b2Fixture *) override;
	Q_SIGNAL void frame_begin() const;
	Q_SIGNAL void frame_end() const;
	Q_SLOT aw::game::ContactEvents *contact(int, int);
	Q_SLOT void update();
public:
	Q_SIGNAL void sound_event();
	Q_SLOT int seconds_to_frames(float) const;
	std::unique_ptr<b2World> world_;
};

}
}
