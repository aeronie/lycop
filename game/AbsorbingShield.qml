import aw.game 0.0

AbsorbingShield
{
	property int level: 1
	health: max_health
	max_health: 200 +~~body.health_boost
	damages_multiplier: [1.5, 1.5, 0.75][level]
	shield_multiplier: [0.4, 0.4, 0.2][level]
	image: Image
	{
		material: "equipments/absorbing-shield-generator"
		scale: 0.5
		mask: mask_equipment(parent)
	}
}
