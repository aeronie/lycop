#include <Box2D.h>

#include "Body.h"
#include "Joint.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

static void destroy_joint(b2Joint *joint)
{
	Q_ASSERT(joint);
	Q_ASSERT(joint->GetBodyA());
	Q_ASSERT(joint->GetBodyA()->GetWorld());
	joint->GetBodyA()->GetWorld()->DestroyJoint(joint);
}

}

Joint::Joint()
: joint_(0, destroy_joint)
{
	Observer<0>::connect(signal_ready());
}

void Joint::update(const Observer<0>::Tag &)
{
	if(body_1() && body_1()->body_ && body_2() && body_2()->body_ && qml_initialized_ && !joint_)
	{
		initialize();
	}
}

void Joint::update(const Observer<-1>::Tag &)
{
	update(Observer<0>::Tag());
}

void Joint::update(const Observer<-2>::Tag &)
{
	update(Observer<0>::Tag());
}

void Joint::initialize(b2JointDef *def)
{
	Q_ASSERT(def);
	Q_ASSERT(body_1());
	Q_ASSERT(body_2());
	Q_ASSERT(body_1()->body_);
	Q_ASSERT(body_2()->body_);
	def->bodyA = body_1()->body_.get();
	def->bodyB = body_2()->body_.get();
	def->collideConnected = collide();
	def->userData = this;
	joint_.reset(def->bodyA->GetWorld()->CreateJoint(def));
	Q_ASSERT(joint_);
}

b2Vec2 Joint::point_to_body_1(b2Vec2 p) const
{
	Q_ASSERT(body_1());
	p *= body_1()->scale();
	return p;
}

b2Vec2 Joint::point_to_body_2(b2Vec2 p) const
{
	Q_ASSERT(body_2());
	p *= body_2()->scale();
	return p;
}

void Joint::set_body_1(Body *const &body)
{
	if(body_1_ == body) return;
	body_1_ = body;
	Observer<-1>::connect(body ? body->signal_ready() : 0);
	body_1_changed(body_1_);
}

void Joint::set_body_2(Body *const &body)
{
	if(body_2_ == body) return;
	body_2_ = body;
	Observer<-2>::connect(body ? body->signal_ready() : 0);
	body_2_changed(body_2_);
}

bool Joint::ready() const
{
	return !!joint_;
}

AW_DEFINE_PROPERTY_STORED(Joint, collide)

}
}
