#include "Dash.h"
#include "meta.private.h"

namespace aw {
namespace game {

AW_DEFINE_OBJECT_STUB(Dash)
AW_DEFINE_PROPERTY_STORED(Dash, speed_amplification)

}
}
