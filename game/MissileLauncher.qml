import QtQuick 2.0 as Q
import aw.game 0.0

Weapon
{
	property int level: 1
	property int bullet_group: groups.enemy_missile
	property var target_groups: groups.enemy_targets
	period: scene.seconds_to_frames([0 , 1 / 2 , 1 / 2.3] [level])
	range_bullet: scene.seconds_to_frames(2)
	health: max_health
	max_health: 250 +~~body.health_boost
	image: Image
	{
		material: "equipments/missile-launcher"
		mask: mask_equipment(parent)
	}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/missile-launcher/shooting"
		scale: slot.sound_scale
	}
	bullet_factory: Vehicle
	{
		property real damages: 200
		scale: 0.4
		max_speed: 15
		max_angular_speed: 3
		loot_factory: Explosion {scale: 1}
		Image
		{
			material: "equipments/missile"
			z: altitudes.bullet
		}
		PolygonCollider
		{
			vertexes: [Qt.point(-0.1, -0.95), Qt.point(-0.1, 0.95), Qt.point(0.1, 0.95), Qt.point(0.1, -0.95)]
			group: weapon.bullet_group
			sensor: true
		}
		Behaviour_attack_missile
		{
			target_groups: weapon.target_groups
			range: weapon.range_bullet
		}
		Q.Component.onCompleted:
		{
			position = weapon.slot.point_to_scene(Qt.point(0, 0))
			angle = weapon.slot.angle_to_scene(0)
			weapon.sound_shooting.play()
			weapon.set_bullet_velocity(this, max_speed)
		}
	}
}
