import QtQuick 2.0 as Q
import aw.game 0.0

Weapon
{
	property int level: 1
	property int bullet_group: groups.enemy_laser
	period: scene.seconds_to_frames([0 , 1 / 2 , 1 / 2.4] [level])
	health: max_health
	max_health: 400 +~~body.health_boost
	range_bullet: scene.seconds_to_frames(1.5)
	image: Image
	{
		material: "equipments/laser-bouncer"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/heavy-laser-gun/shooting"
		scale: slot.sound_scale
	}
	bullet_factory: Bullet
	{
		readonly property int bullet_range: weapon.range_bullet
		readonly property int group: weapon.bullet_group
		scale: 0.6
		range: weapon.range_bullet
		damages: 400
		Image
		{
			material: "equipments/laser-gun/bullet"
			z: altitudes.bullet
		}
		PolygonCollider
		{
			vertexes: [Qt.point(-0.125, -1), Qt.point(-0.125, 1), Qt.point(0.125, 1), Qt.point(0.125, -1)]
			group: body.group
			sensor: true
		}
		Q.Component.onCompleted:
		{
			angle = weapon.slot.angle_to_scene(0)
			position = weapon.slot.point_to_scene(Qt.point(0, -1.4))
			weapon.sound_shooting.play()
			weapon.set_bullet_velocity(this, 20)
		}
		loot_factory: Repeater
		{
			count: 12
			Q.Component
			{
				Bullet
				{
					readonly property int group: body.group
					scale: 0.3
					range: body.bullet_range
					damages: 100
					Image
					{
						material: "equipments/laser-gun/bullet"
						z: altitudes.bullet
					}
					PolygonCollider
					{
						vertexes: [Qt.point(-0.125, -1), Qt.point(-0.125, 1), Qt.point(0.125, 1), Qt.point(0.125, -1)]
						group: body.group
						sensor: true
					}
					Q.Component.onCompleted:
					{
						angle = body.angle + 2 * Math.PI * (index + Math.random()) / count
						position.x = body.position.x + Math.sin(angle) * scale * 3
						position.y = body.position.y - Math.cos(angle) * scale * 3
						set_velocity(direction_to_scene(Qt.point(0, -20)))
					}
				}
			}
		}
	}
}
