#include <Box2D.h>

#include "Joint.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct WeldJoint
: Joint
{
	AW_DECLARE_OBJECT_STUB(WeldJoint)
	AW_DECLARE_PROPERTY_STORED(b2Vec2, position) = b2Vec2_zero;
	AW_DECLARE_PROPERTY_STORED(float, angle) = 0;

	void initialize() override
	{
		b2WeldJointDef def;
		def.localAnchorA = point_to_body_1(position());
		def.referenceAngle = angle();
		Joint::initialize(&def);
	}
};

AW_DEFINE_OBJECT_STUB(WeldJoint)
AW_DEFINE_PROPERTY_STORED(WeldJoint, position)
AW_DEFINE_PROPERTY_STORED(WeldJoint, angle)

}
}
}
