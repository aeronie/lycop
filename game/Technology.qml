import QtQuick 2.0

QtObject
{
	property string title
	property string description
	property bool enabled: true
	readonly property int level: saved_game.self.get(key("level"), 0)
	property int control: 3 // 0: passif, 1: activé, 2: coché, 3: ciblé
	property int x
	property int y
	property int cost
	property Component factory

	function key(x) {return "equipments/" + objectName + "/" + x}
	function add_level(x) {saved_game.set(key("level"), level + x)}

	function _title() {return "<h1>%1</h1><p>%2</p>".arg(title).arg(factory ? ["Passif", "Activé", "Passif activé", ""][control] : "Recherche")}
	function _level(level, description){return qsTr("<h2>Level %1</h2><ul>%2</ul>").arg(level).arg(description)}
	function _cost() {return qsTr("<li>Required materials: %1 kg</li>").arg(cost)}
	function _time(value) {return qsTr("<li>Effect duration: %1 s</li>").arg(value)}
	function _time_activation(value) {return qsTr("<li>Activation time: %1 s</li>").arg(value)}
	function _frequency(value) {return qsTr("<li>Fire rate: %1 Hz</li>").arg(value)}
	function _damages_(value, unit) {return qsTr("<li>Firepower: %1 damages / %2</li>").arg(value).arg(unit)}
	function _damages_received_overload(value) {return qsTr("<li>Damage dealt during overload: %1 of base damages</li>").arg(value)}
	function _damages(value) {return qsTr("<li>Firepower: %1 damages</li>").arg(value)}
	function _damages_per_second(value) {return _damages_(value, qsTr("s"))}
	function _damages_per_impact(value) {return _damages_(value, qsTr("impact"))}
	function _health(value) {return qsTr("<li>Resistance: %1 HP</li>").arg(value)}
	function _health_restoration(value) {return qsTr("<li>Equipments regeneration: %1 HP / s</li>").arg(value)}
	function _health_boost(value) {return qsTr("<li>Resistance: +%1 HP per equipment</li>").arg(value)}
	function _shield(value) {return qsTr("<li>Shield resistance: %1 HP</li>").arg(value)}
	function _shield_restoration_(value, unit) {return qsTr("<li>Shield regeneration: %1 HP / %2</li>").arg(value).arg(unit)}
	function _shield_restoration(value) {return _shield_restoration_(value, qsTr("s"))}
	function _shield_restoration_per_impact(value) {return _shield_restoration_(value, qsTr("impact"))}
	function _shield_restoration_time(value) {return qsTr("<li>Shield downtime: %1 s</li>").arg(value)}
	function _shield_restoration_absorbing(value) {return qsTr("<li>Shield regeneration: %1 of base damages</li>").arg(value)}
	function _energy(value) {return qsTr("<li>Supplementary energy: %1 J</li>").arg(value)}
	function _energy_restoration(value) {return qsTr("<li>Energy regeneration: %1 J / s</li>").arg(value)}
	function _energy_used_(value, unit) {return qsTr("<li>Energy consumed: %1 J / %2</li>").arg(value).arg(unit)}
	function _energy_used(value) {return _energy_used_(value, qsTr("s"))}
	function _energy_used_per_use(value) {return _energy_used_(value, qsTr("use"))}
	function _matter_used_(value, unit) {return qsTr("<li>Materials consumed: %1 kg / %2</li>").arg(value).arg(unit)}
	function _matter_used(value) {return _matter_used_(value, qsTr("use"))}

	function icon() {return "image://EquipmentIcon/%1,%2,%3,%4,%5".arg(objectName).arg(level > 1).arg(enabled ? 0 : 2).arg(level).arg(0)}
}
