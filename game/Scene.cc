#include <QtQml>
#include <Box2D.h>

#include "Body.h"
#include "Collider.h"
#include "Equipment.h"
#include "EquipmentSlot.h"
#include "Image.h"
#include "Joint.h"
#include "Scene.h"
#include "Sound.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct ContactDispatch
{
	Body *body_1_ = 0;
	Body *body_2_ = 0;
	ContactEvents *signal_ = 0;

	ContactDispatch(std::map<std::pair<int, int>, ContactEvents> *signals, b2Fixture *fixture_1, b2Fixture *fixture_2)
	{
		Q_ASSERT(signals);
		Q_ASSERT(fixture_1);
		Q_ASSERT(fixture_2);

		if(fixture_1->GetFilterData().groupIndex > fixture_2->GetFilterData().groupIndex)
		{
			std::swap(fixture_1, fixture_2);
		}

		auto signal = signals->find(std::make_pair(fixture_1->GetFilterData().groupIndex, fixture_2->GetFilterData().groupIndex));

		if(signal != signals->end())
		{
			Q_ASSERT(fixture_1->GetBody());
			Q_ASSERT(fixture_2->GetBody());
			body_1_ = static_cast<Body *>(fixture_1->GetBody()->GetUserData());
			body_2_ = static_cast<Body *>(fixture_2->GetBody()->GetUserData());
			Q_ASSERT(body_1_);
			Q_ASSERT(body_2_);
			signal_ = &signal->second;
		}
	}
};

template<class T>
static void transform(QMatrix4x4 *m, T const &frame)
{
	m->translate(frame.position().x, frame.position().y, 0);
	m->rotate(frame.angle() * float(180 / M_PI), 0, 0, 1);
	m->scale(frame.scale(), frame.scale(), 1);
}

}

Scene::Scene()
: world_(new b2World(b2Vec2_zero))
{
	world_->SetDestructionListener(this);
	world_->SetContactFilter(this);
	world_->SetContactListener(this);
}

Scene::~Scene()
{
	for(Image *&image: images_) if(image) image->pointer_ = 0;
	for(Sound *&sound: sounds_) if(sound) sound->pointer_ = 0;
	for(b2Body *body = world_->GetBodyList(), *next = 0; body; body = next)
	{
		next = body->GetNext();
		static_cast<Body *>(body->GetUserData())->body_.reset();
	}
}

void Scene::update()
{
	for(Image *&image: images_) if(image) image->pointer_ = 0;
	for(Sound *&sound: sounds_) if(sound) sound->pointer_ = 0;
	images_.clear();
	sounds_.clear();

	for(b2Body *child = world_->GetBodyList(); child; child = child->GetNext())
	{
		Body *body = static_cast<Body *>(child->GetUserData());
		QMatrix4x4 m;
		transform(&m, *body);
		auto begin = images_.size();
		update(body, m);
		body->set_images({&images_, begin, images_.size()});
	}

	for(Image *&image: images_) image->pointer_ = &image;
	for(Sound *&sound: sounds_) sound->pointer_ = &sound;
	images_changed(images());
	sounds_changed(sounds());
}

void Scene::update(QObject *object, QMatrix4x4 const &m1)
{
	for(QObject *child: object->children())
	{
		QMatrix4x4 m2 = m1;

		if(Image *image = static_qobject_cast<Image>(child))
		{
			if(!image->mask().alpha()) continue;
			transform(&m2, *image);
			image->set_transform(m2);
			images_.push_back(image);
		}
		else if(Sound *sound = static_qobject_cast<Sound>(child))
		{
			transform(&m2, *sound);
			sound->set_transform(m2);
			sounds_.push_back(sound);
		}
		else if(EquipmentSlot *slot = static_qobject_cast<EquipmentSlot>(child))
		{
			transform(&m2, *slot);
			update(slot, m2);

			if(Equipment *equipment = slot->equipment())
			{
				update(equipment, m2);
			}
		}
	}
}

void Scene::timerEvent(QTimerEvent *)
{
	frame_begin_.notify();
	Q_EMIT frame_begin();
	world_->Step(time_step(), 8, 3);
	++time_;
	Q_EMIT time_changed(time_);
	Q_EMIT frame_end();
	frame_end_.notify();
	update();
}

bool Scene::ShouldCollide(b2Fixture *fixture_1, b2Fixture *fixture_2)
{
	return ContactDispatch(&contacts_, fixture_1, fixture_2).signal_;
}

void Scene::PreSolve(b2Contact *contact, const b2Manifold *)
{
	Q_ASSERT(contact);
	Q_ASSERT(contact->GetFixtureA());
	Q_ASSERT(contact->GetFixtureB());
	Collider *collider_1 = static_cast<Collider *>(contact->GetFixtureA()->GetUserData());
	Collider *collider_2 = static_cast<Collider *>(contact->GetFixtureB()->GetUserData());
	Q_ASSERT(collider_1);
	Q_ASSERT(collider_2);

	if(collider_1->sensor() || collider_2->sensor())
	{
		contact->SetEnabled(false);
	}
}

void Scene::BeginContact(b2Contact *contact)
{
	ContactDispatch dispatch(&contacts_, contact->GetFixtureA(), contact->GetFixtureB());

	if(dispatch.signal_)
	{
		int point_count = contact->GetManifold()->pointCount;
		b2WorldManifold manifold;
		contact->GetWorldManifold(&manifold);
		b2Vec2 point = b2Vec2_zero;

		for(int i = 0; i < point_count; ++i)
		{
			point += manifold.points[i];
		}

		point *= 1 / float(point_count);
		Q_EMIT dispatch.signal_->begin(dispatch.body_1_, dispatch.body_2_, point);
	}
}

void Scene::EndContact(b2Contact *contact)
{
	ContactDispatch dispatch(&contacts_, contact->GetFixtureA(), contact->GetFixtureB());

	if(dispatch.signal_)
	{
		Q_EMIT dispatch.signal_->end(dispatch.body_1_, dispatch.body_2_);
	}
}

void Scene::SayGoodbye(b2Joint *joint)
{
	static_cast<Joint *>(joint->GetUserData())->joint_.release();
}

void Scene::SayGoodbye(b2Fixture *fixture)
{
	static_cast<Collider *>(fixture->GetUserData())->fixture_.release();
}

bool Scene::running() const
{
	return timer_;
}

void Scene::set_running(const bool &value)
{
	if(running() != value)
	{
		if(value)
		{
			timer_ = startTimer(int(time_step() * 1000), Qt::PreciseTimer);
			Q_ASSERT(timer_);
		}
		else
		{
			killTimer(timer_);
			timer_ = 0;
		}

		Q_EMIT running_changed(value);
	}
}

int Scene::seconds_to_frames(float x) const
{
	return int(x / time_step());
}

float Scene::time_step() const
{
	return 20e-3f;
}

ArrayView<Sound *> Scene::sounds() const
{
	return {&sounds_, 0, sounds_.size()};
}

ArrayView<Image *> Scene::images() const
{
	return {&images_, 0, images_.size()};
}

ContactEvents *Scene::contact(int _1, int _2)
{
	return &contacts_[std::minmax(_1, _2)];
}

ContactEvents::ContactEvents()
{
	QQmlEngine::setObjectOwnership(this, QQmlEngine::CppOwnership);
}

AW_DEFINE_OBJECT_STUB(Scene)
AW_REGISTER_ABSTRACT_TYPE(ContactEvents)

}
}
