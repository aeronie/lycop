#pragma once

#include <QObject>

#include "meta.h"

namespace aw {
namespace game {

struct Consumable
: QObject
{
	AW_DECLARE_OBJECT_STUB(Consumable)
	AW_DECLARE_PROPERTY_STORED(bool, available) = false;
protected:
	Consumable() = default;
	~Consumable() = default;
public:
	Q_SIGNAL void consumed();
};

}
}
