#include "ActiveEquipment.h"
#include "Body.h"
#include "EquipmentSlot.h"
#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {

ActiveEquipment::ActiveEquipment()
{
	Observer<0>::connect(signal_ready());
	Observer<1>::connect(signal_slot_changed());
}

void ActiveEquipment::update(Observer<0>::Tag const &)
{
	update(Observer<3>::Tag());
}

void ActiveEquipment::update(Observer<1>::Tag const &)
{
	Observer<2>::connect(slot() ? slot()->signal_body_changed() : 0);
}

void ActiveEquipment::update(Observer<2>::Tag const &)
{
	Observer<3>::connect(body() ? body()->signal_scene_changed() : 0);
}

void ActiveEquipment::update(Observer<3>::Tag const &)
{
	Observer<4>::connect(qml_initialized_ && scene() ? scene()->signal_frame_begin() : 0);
}

void ActiveEquipment::update(Observer<4>::Tag const &)
{
	if(shooting_ && (!consumable_ || consumable_->available()))
	{
		if(consumable_)
		{
			consumable_->consumed();
		}
		activate();
	}
	else
	{
		set_shooting(false);
	}
}

AW_DEFINE_OBJECT_STUB(ActiveEquipment)
AW_DEFINE_PROPERTY_STORED(ActiveEquipment, consumable)
AW_DEFINE_PROPERTY_STORED(ActiveEquipment, shooting)

}
}
