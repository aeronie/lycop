#include "TriggeredEquipment.h"
#include "Body.h"
#include "meta.private.h"

namespace aw {
namespace game {

void TriggeredEquipment::update(Observer<4>::Tag const &tag)
{
	if(time_ > 0)
	{
		time_ = std::max(time_ - body()->weapon_regeneration(), 0);
		Q_EMIT time_changed(time_);
	}
	else
	{
		ActiveEquipment::update(tag);
	}
}

void TriggeredEquipment::activate()
{
	ActiveEquipment::activate();
	time_ = period_;
	Q_EMIT time_changed(time_);
}

AW_DEFINE_OBJECT_STUB(TriggeredEquipment)
AW_DEFINE_PROPERTY_STORED(TriggeredEquipment, period)

}
}
