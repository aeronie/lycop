import aw.game 0.0

Dash
{
	property int level: 1
	health: max_health
	max_health: 200 +~~body.health_boost
	speed_amplification: [0,0.5,1][level]
	image: Image
	{
		material: "equipments/dash"
		scale: 0.5
		mask: mask_equipment(parent)
	}
}
