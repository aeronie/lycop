#include "ActiveEquipment.h"

namespace aw {
namespace game {

struct AbsorbingShield
: ActiveEquipment
{
	AW_DECLARE_OBJECT_STUB(AbsorbingShield)
	AW_DECLARE_PROPERTY_STORED(float, damages_multiplier) = 1.5;
	AW_DECLARE_PROPERTY_STORED(float, shield_multiplier) = 0.75;
protected:
	AbsorbingShield() = default;
	~AbsorbingShield() = default;
};

}
}
