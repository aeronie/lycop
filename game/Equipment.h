#pragma once

#include "Object.h"
#include "meta.h"

namespace aw {
namespace game {

struct Body;
struct EquipmentSlot;
struct Image;
struct Scene;
struct Sound;

struct Equipment
: Object
{
	AW_DECLARE_OBJECT_STUB(Equipment)
	AW_DECLARE_PROPERTY_STORED(aw::game::EquipmentSlot *, slot) = 0;
	AW_DECLARE_PROPERTY_STORED(aw::game::Image *, image) = 0;
	AW_DECLARE_PROPERTY_STORED(float, health) = 0;
	AW_DECLARE_PROPERTY_STORED(float, max_health) = 0;
	AW_DECLARE_PROPERTY_STORED(float, shield) = 0;
	AW_DECLARE_PROPERTY_READONLY(aw::game::Body *, body)
	AW_DECLARE_PROPERTY_READONLY(aw::game::Scene *, scene)
	AW_DECLARE_SIGNAL(slot_changed)
protected:
	Equipment() = default;
	~Equipment();
};

}
}
