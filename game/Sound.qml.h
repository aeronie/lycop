#include "Scene.h"
#include "Sound.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

static int qml_count(QQmlListProperty<Sound> *list)
{
	return int(static_cast<std::vector<Sound *> *>(list->data)->size());
}

static Sound *qml_at(QQmlListProperty<Sound> *list, int index)
{
	return (*static_cast<std::vector<Sound *> *>(list->data))[index];
}

struct SoundList
: QObject
{
	AW_DECLARE_OBJECT_STUB(SoundList)
	AW_DECLARE_PROPERTY_READONLY(aw::ArrayView<aw::game::Sound *>, data)
	Q_PROPERTY(QQmlListProperty<aw::game::Sound> qml_data READ qml_data NOTIFY data_changed)
	Q_CLASSINFO("DefaultProperty", "qml_data")
	std::vector<Sound *> data_;

	QQmlListProperty<Sound> qml_data()
	{
		return {this, &data_, qml_count, qml_at};
	}

protected:

	Q_SLOT void insert(Sound *sound)
	{
		Q_ASSERT(sound);
		Q_ASSERT(find(data_.begin(), data_.end(), sound) == data_.end());
		if(!sound->parent()) sound->setParent(this);
		data_.push_back(sound);
		data_changed(data());
	}

	Q_SLOT void remove(Sound *sound)
	{
		Q_ASSERT(sound);
		if(sound->parent() == this) sound->deleteLater();
		data_.erase(std::remove(data_.begin(), data_.end(), sound), data_.end());
		data_changed(data());
	}
};

ArrayView<Sound *> SoundList::data() const
{
	return {&data_, 0, data_.size()};
}

AW_DEFINE_OBJECT_STUB(SoundList)

}

Sound::~Sound()
{
	if(pointer_) *pointer_ = 0; // il faut se désinscrire de la liste Scene::sounds_
}

void Sound::play()
{
	playing_ = true;

	for(QObject *object = this; object; object = object->parent())
	{
		if(Scene *scene = static_qobject_cast<Scene>(object))
		{
			Q_EMIT scene->sound_event();
			break;
		}
	}
}

AW_DEFINE_OBJECT_STUB(Sound)
AW_DEFINE_PROPERTY_STORED(Sound, looping)
AW_DEFINE_PROPERTY_STORED(Sound, playing)
AW_DEFINE_PROPERTY_STORED(Sound, transform)

}
}
