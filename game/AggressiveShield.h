#include "ActiveEquipment.h"

namespace aw {
namespace game {

struct AggressiveShield
: ActiveEquipment
{
	AW_DECLARE_OBJECT_STUB(AggressiveShield)
	AW_DECLARE_PROPERTY_STORED(float, damages) = 0;
protected:
	AggressiveShield() = default;
	~AggressiveShield() = default;
};

}
}
