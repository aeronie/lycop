import QtQuick 2.0 as Q
import aw.game 0.0
import "qrc:///game"

Chapter
{
	readonly property alias ship: ship
	Q.Binding {target: screen_menu.about_screen_travel; property: "blink"; value: true; when: parent}
	Q.Component.onCompleted: groups.init(scene)
	title: qsTr("title")
	scene: Scene
	{
		running: game_running
		Background
		{
			z: -0.5
			scale: 10
			material: "vide"
		}
		Ship {id: ship}
	}
}
