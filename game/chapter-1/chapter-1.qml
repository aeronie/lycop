import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-1/chapter-1.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal one_enemy_destroyed
	property bool request_pause: false
	signal signal_lycop_near_rabbit
	signal screen_cleared
	property var camera_position: Qt.point(0, 0)
	property var info_print
	property real t0: 0
	property bool tuto_done : saved_game.self.get("tuto_done", false)
	property bool mod_easy : saved_game.self.get("mod_easy", false)
	property Body lapin
	property Body boulette
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")

	scene: Scene
	{
		running: false
		Background
		{
			z: -0.5
			scale: 10
			material: "vide"
		}
		Sensor
		{
			id: checkpoint
			type: Body.STATIC
			position.x: -10
			position.y: -30
			duration_in: 50
			duration_out: 10
			icon: visible ? 1 : 0
			icon_color: "cyan"
			property real visible : root.tuto_done ? 0 : 1
			scale: 2
			Q.Behavior on scale {Q.NumberAnimation {}}
			onScaleChanged: if(!scale) visible = 0
			Image
			{
				material: "chapter-1/satellite"
				z: altitudes.background_near
				scale: 4
				mask: Qt.rgba(1, 1, 1, checkpoint.visible)
			}
			Image
			{
				material: "chapter-1/satellite-lamps"
				z: altitudes.background_near
				scale: 4
				mask: Qt.hsla(checkpoint.fuzzy_value / 3, 1, 0.5, checkpoint.visible)
			}
			CircleCollider
			{
				group: groups.sensor
				sensor: true
				radius: 0.7
				position.x : -0.4
				position.y : 0.9
				
			}
		}
		Sensor
		{
			id: lapin_timer_1
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(10)
			target_value: state_lapin_1.active
		}
		Sensor
		{
			id: lapin_timer_2
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(10)
			target_value: state_lapin_2.active
		}
		Sensor
		{
			id: short_pause
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(1)
			target_value: request_pause
		}
		Sensor
		{
			id: long_pause
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(5)
			target_value: request_pause
		}
		Ship
		{
			id: ship
			position.y: root.tuto_done ? 0 : 10 * (1 - t0)
			Q.Component.onDestruction: game_over = true
		}
	}
	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			property int ind_tab : 0
			property var tab: [
				qsTr("Recyclez le satellite"),
				qsTr("Construisez de nouveaux canons"),
				qsTr("Détruisez le drone"),
				qsTr("Utilisez votre point de technologie")
				]
			text: qsTr("Objectif : %1").arg(tab[ind_tab])
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
			visible: false
		}
	}
	
	Q.Component
	{
		id: clear_screen_factory
		Vehicle
		{
			scale: 50
			type: Body.KINEMATIC
			CircleCollider
			{
				group: groups.clear_screen
			}
		}
	}
	Q.Component
	{
		id: clear_screen_animation
		Animation
		{
			id: timer
			property bool first_time: true
			property var clear_screen
			time: 0.1
			speed: -1
			Q.Component.onDestruction: screen_cleared()

			onTimeChanged:
			{
				if(first_time)
				{
					clear_screen=clear_screen_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x,ship.position.y)})
					first_time= false
				}
				if(time<0)
				{
					clear_screen.deleteLater()
					timer.deleteLater()
				}
			}
		}
	}
	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			max_speed: 5
			icon: 2
			icon_color: "red"
			Q.Component.onDestruction: one_enemy_destroyed()
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				material: "chapter-1/drone"
				scale: 2.5
				position.y: 1.5
				mask: mask_equipment(equipment)
			}
			CircleCollider
			{
				group: groups.auto_hull(parent)
			}
			EquipmentSlot
			{
				equipment: Equipment
				{
					id: equipment
					health: max_health
					max_health: 500
				}
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: -Math.PI / 2
				target_distance: 10
			}
		}
	}
	Q.Component
	{
		id: boulette_factory
		Body
		{
			type: Body.STATIC
			position.x: (1 - t0) * ship.position.x + t0 * lapin.position.x
			position.y: (1 - t0) * ship.position.y + t0 * lapin.position.y
			scale: 0.15
			Image
			{
				material: "equipments/gun/bullet"
				z: altitudes.boss - 0.000001
			}
			Sound
			{
				Q.Component.onCompleted: play()
				objectName: "equipments/gun/shooting"
				scale: 10
			}
		}
	}
	Q.Component
	{
		id: checkpoint_lapin_factory
		Sensor
		{
			function camera_position() {return Qt.point((1 - t0) * ship.position.x + t0 * position.x, (1 - t0) * ship.position.y + t0 * position.y)}
			type: Body.STATIC
			icon: 1
			icon_color: "cyan"
			scale: 6
			angle: Math.PI
			onValueChanged: if(value) signal_lycop_near_rabbit()
			Image
			{
				scale: 1.5
				material: "chapter-1/rabbit"
			}
			CircleCollider
			{
				group: groups.sensor
				sensor: true
				radius: 3
			}
		}
	}
	Q.Component
	{
		id: lapin_factory
		Vehicle
		{
			id: component
			property int destroyed_weapon_count: 0
			icon: 4
			icon_color: "red"
			max_speed: 10
			max_angular_speed: state_lapin_1.active ? 4 : 2
			shield: max_shield
			scale: 6
			Q.Component.onDestruction: one_enemy_destroyed()
			loot_factory: Repeater
			{
				count: 17
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "chapter-1/boss"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				z: altitudes.shield
				scale: 1.1
			}
			Behaviour_attack_circle
			{
				enabled: state_lapin_1.active
				target_groups: groups.enemy_targets
				target_angle: component.destroyed_weapon_count < 5 ? Math.PI / 2 : -Math.PI / 2
				target_distance: 10
			}
			Behaviour_attack_rotate
			{
				enabled: state_lapin_2.active
				target_groups: groups.enemy_targets
				target_distance: 10
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-0.29,-0.88), Qt.point(-0.47,-0.57), Qt.point(-0.40,0.82), Qt.point(-0.12,0.94), Qt.point(0.12,0.94), Qt.point(0.40,0.82), Qt.point(0.47,-0.57), Qt.point(0.29,-0.88)]
				group: groups.auto_hull(parent)
			}
			CircleCollider
			{
				group: groups.auto_shield(parent)
				radius: 1.1
			}
			EquipmentSlot
			{
				scale: 0.15
				equipment: Shield
				{
				}
			}
			EquipmentSlot
			{
				position.y: 0.9
				scale: 0.15
				angle: Math.PI
				equipment: TripleGun
				{
					shooting: true
				}
			}
			EquipmentSlot
			{
				position.y: -0.85
				scale: 0.15
				equipment: TripleGun
				{
					shooting: true
				}
			}
			EquipmentSlot
			{
				position.x: 0.25
				position.y: -0.7
				scale: 0.15
				angle: Math.PI / 4
				equipment: LaserGun
				{
					shooting: true
				}
			}
			Repeater
			{
				count: 4
				Q.Component
				{
					EquipmentSlot
					{
						position.x: 0.4
						position.y: index * 0.2 - 0.43
						scale: 0.15
						angle: Math.PI / 2
						equipment: LaserGun
						{
							shooting: true
							Q.Component.onDestruction: ++component.destroyed_weapon_count
						}
					}
				}
			}
			EquipmentSlot
			{
				position.x: 0.35
				position.y: 0.57
				scale: 0.15
				angle: Math.PI / 2
				equipment: LaserGun
				{
					shooting: true
					Q.Component.onDestruction: ++component.destroyed_weapon_count
				}
			}
			EquipmentSlot
			{
				position.x: 0.25
				position.y: 0.8
				scale: 0.15
				angle: Math.PI * 3 / 4
				equipment: LaserGun
				{
					shooting: true
				}
			}
			EquipmentSlot
			{
				position.x: -0.25
				position.y: -0.7
				scale: 0.15
				angle: -Math.PI / 4
				equipment: LaserGun
				{
					shooting: true
				}
			}
			Repeater
			{
				count: 4
				Q.Component
				{
					EquipmentSlot
					{
						position.x: -0.4
						position.y: index * 0.2 - 0.43
						scale: 0.15
						angle: -Math.PI / 2
						equipment: LaserGun
						{
							shooting: true
						}
					}
				}
			}
			EquipmentSlot
			{
				position.x: -0.35
				position.y: 0.57
				scale: 0.15
				angle: -Math.PI / 2
				equipment: LaserGun
				{
					shooting: true
				}
			}
			EquipmentSlot
			{
				position.x: -0.25
				position.y: 0.8
				scale: 0.15
				angle: -Math.PI * 3 / 4
				equipment: LaserGun
				{
					shooting: true
				}
			}
		}
	}
	StateMachine
	{
		begin: state_story_0
		active: game_running
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				guard: !tuto_done
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 2000}
				onTriggered:
				{
					saved_game.set("tax", 1)
					music.objectName = "chapter-1/main"
				}
			}
			SignalTransition
			{
				targetState: state_story_10
				signal: state_story_0.propertiesAssigned
				guard: tuto_done
				onTriggered:
				{
					saved_game.set("tax", 0.5)
					music.objectName = "chapter-1/main"
					camera_position = undefined
					ship.position.y = 0
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_after_story_1
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					ship.position.y = 0
					camera_position = undefined
					var a = []
					controls.get_bindings(a, controls.speed_boost)
					messages.add("cnes/happy", qsTr("story 1 1"))
					messages.add("cnes/normal", qsTr("story 1 2"))
					messages.add("cnes/normal", qsTr("story 1 3"))
					messages.add("nectaire/normal", qsTr("story 1 4"))
					messages.add("nectaire/normal", qsTr("story 1 5"))
					messages.add("nectaire/normal", qsTr("story 1 6"))
					messages.add("nectaire/normal", qsTr("story 1 7 %1").arg(a[0]))
				}
			}
		}
		State 
		{
			id: state_after_story_1
			SignalTransition 
			{
				targetState: state_checkpoint_1
				signal: state_after_story_1.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					root.info_print.visible = true
					root.info_print.ind_tab = 0
				}
			}
		}
		State
		{
			id: state_checkpoint_1
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_checkpoint_1
				signal: checkpoint.value_changed
				onTriggered: 
				{
					messages.add("nectaire/normal", qsTr("story 2 1"))
					root.info_print.visible = false
				}
			}
		}
		State {id: state_after_checkpoint_1; SignalTransition {targetState: state_checkpoint_2; signal: state_after_checkpoint_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_checkpoint_2
			SignalTransition
			{
				targetState: state_after_checkpoint_2
				signal: state_checkpoint_2.propertiesAssigned
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("story 2 2"))
					messages.add("nectaire/normal", qsTr("story 2 3"))
					messages.add("nectaire/normal", qsTr("story 2 4"))
					messages.add("nectaire/normal", qsTr("story 2 5"))
					saved_game.add_matter(400)
					checkpoint.scale = 0
				}
			}
		}
		State 
		{
			id: state_after_checkpoint_2
			SignalTransition 
			{
				targetState: state_weapons
				signal: state_after_checkpoint_2.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
					root.info_print.ind_tab++
					root.info_print.visible = true
				}
			}
		}
		State
		{
			id: state_weapons
			property int health: // HACK
			{
				var x = 0
				if(ship)
				{
					for(var i = 0; i < ship.slots.length; ++i)
					{
						var a = ship.slots[i].equipment
						if(a) x += a.health
					}
				}
				return x
			}
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: screen_menu.about_screen_equipments; property: "blink"; value: true}
			SignalTransition
			{
				targetState: state_after_weapons
				signal: state_weapons.healthChanged
				guard: saved_game.matter < 100
				onTriggered:
				{
					var a = []
					controls.get_bindings(a, controls.weapons)
					var b = []
					controls.get_bindings(b, controls.weapons_boost)
					root.info_print.visible = false
					messages.add("nectaire/normal", qsTr("story 3 1"))
					messages.add("nectaire/normal", qsTr("story 3 2 %1 %2").arg(a[0]).arg(b[0]))
					messages.add("nectaire/normal", qsTr("story 3 3"))
					drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y + 30), angle: Math.PI / 2})
					saved_game.set("tax", 0.5)
				}
			}
		}
		State 
		{
			id: state_after_weapons
			SignalTransition 
			{
				targetState: state_drone
				signal: state_after_weapons.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
					root.info_print.ind_tab++
					root.info_print.visible = true
				}
			}
		}
		State
		{
			AssignProperty {target: scene; property: "running"; value: true}
			State
			{
				id: state_drone
				SignalTransition {targetState: state_drone_0; signal: one_enemy_destroyed}
			}
			State
			{
				id: state_drone_0
				AssignProperty {target: root; property: "request_pause"; value: true}
				SignalTransition
				{
					targetState: state_after_drone
					signal: short_pause.value_changed
					guard: short_pause.value
					onTriggered: 
					{
						root.info_print.visible = false
						messages.add("nectaire/normal", qsTr("story 4"))
					}
				}
			}
		}
		State 
		{
			id: state_after_drone
			SignalTransition 
			{
				targetState: state_story_10
				signal: state_after_drone.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					tuto_done = true
					if(mod_easy)
					{
						saved_game.set("tuto_done", true)
						scene_loader.save()
					}
				}
			}
		}
		State
		{
			id: state_story_10
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "request_pause"; value: true}
			SignalTransition
			{
				targetState: state_after_story_10
				signal: long_pause.value_changed
				guard: long_pause.value
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("story 5"))
					lapin = checkpoint_lapin_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 40, ship.position.y - 5), angle: Math.PI / 2})
				}
			}
		}
		State {id: state_after_story_10; SignalTransition {targetState: state_after_video_incoming; signal: state_after_story_10.propertiesAssigned; guard: !messages.has_unread; onTriggered: screen_video.video.file = ":/data/game/chapter-1/video_arrivee_lapin.ogg"}}
		State
		{
			id: state_after_video_incoming;
			SignalTransition
			{
				targetState:state_story_11
				signal: state_after_video_incoming.propertiesAssigned
				guard:!screen_video.video.file
			}
		}
		State
		{
			id: state_story_11
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_clean_screen
				signal: signal_lycop_near_rabbit
				onTriggered:
				{
					clear_screen_animation.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_clean_screen
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_story_12
				signal: screen_cleared
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 1000}
				onTriggered: 
				{
					camera_position = Qt.binding(lapin.camera_position)
				}
			}
		}
		State
		{
			id: state_story_12
			AssignProperty {target: root; property: "t0"; value: 0.5}
			SignalTransition
			{
				targetState: state_after_story_12
				signal: state_story_12.propertiesAssigned
				onTriggered:
				{
					camera_position = lapin.camera_position()
					messages.add("nectaire/normal", qsTr("story 6 1"))
					messages.add("lycop/normal", qsTr("story 6 2"))
					messages.add("lycop/normal", qsTr("story 6 3"))
					messages.add("lycop/angry", qsTr("story 6 4"))
					music.objectName = "chapter-1/boss"
				}
			}
		}
		State {id: state_after_story_12; SignalTransition {targetState: state_story_13; signal: state_after_story_12.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_13
			SignalTransition
			{
				targetState: state_story_14
				signal: state_story_13.propertiesAssigned
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 1000}
				onTriggered: boulette = boulette_factory.createObject(null, {scene: scene})
			}
		}
		State
		{
			id: state_story_14
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_after_story_14
				signal: state_story_14.propertiesAssigned
				onTriggered:
				{
					boulette.destroy()
					messages.add("lycop/surprised", qsTr("story 6 5"))
					messages.add("nectaire/normal", qsTr("story 6 6"))
					messages.add("heter/normal", qsTr("story 6 7"))
				}
			}
		}
		State
		{
			id: state_after_story_14
			SignalTransition
			{
				targetState: state_after_video_opening;
				signal: state_after_story_14.propertiesAssigned;
				guard: !messages.has_unread;
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-1/video_ouverture_lapin.ogg"
					function f ()
					{
						camera_position = undefined
						lapin.destroy()
						ship.position = Qt.point(0, 0)
						lapin_factory.createObject(null, {scene: scene, position: Qt.point(30, 0)})
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
			}
		}
		State
		{
			id: state_after_video_opening;
			SignalTransition
			{
				targetState:state_lapin
				signal: state_after_video_opening.propertiesAssigned
				guard:!screen_video.video.file
			}
		}
		State
		{
			AssignProperty {target: scene; property: "running"; value: true}
			State
			{
				id: state_lapin
				SignalTransition
				{
					targetState: state_lapin_1
					signal: state_lapin.propertiesAssigned
				}
			}
			State
			{
				State
				{
					id: state_lapin_1
					SignalTransition {targetState: state_lapin_2; signal: lapin_timer_1.value_changed; guard: lapin_timer_1.value}
				}
				State
				{
					id: state_lapin_2
					SignalTransition {targetState: state_lapin_1; signal: lapin_timer_2.value_changed; guard: lapin_timer_2.value}
				}
				SignalTransition
				{
					targetState: state_lapin_0
					signal: one_enemy_destroyed
				}
			}
			State
			{
				id: state_lapin_0
				AssignProperty {target: root; property: "request_pause"; value: true}
				SignalTransition
				{
					targetState: state_after_lapin
					signal: short_pause.value_changed
					guard: short_pause.value
					onTriggered:
					{
						music.objectName = "chapter-1/main"
						messages.add("lycop/neutral", qsTr("story 7 1"))
						messages.add("nectaire/normal", qsTr("story 7 2"))
						messages.add("nectaire/normal", qsTr("story 7 3"))
						technologies.shield_generator.add_level(1)
						saved_game.add_science(1)
					}
				}
			}
		}
		State 
		{
			id: state_after_lapin
			SignalTransition 
			{
				targetState: state_story_20
				signal: state_after_lapin.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					root.info_print.ind_tab = 3
					root.info_print.visible = true
				}
			}
		}
		State
		{
			id: state_story_20
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: screen_menu.about_screen_technologies; property: "blink"; value: true}
			SignalTransition
			{
				targetState: state_after_story_20
				signal: state_story_20.propertiesAssigned
				guard: !saved_game.science
				onTriggered:
				{
					var a = []
					controls.get_bindings(a, controls.secondary_weapons)
					var b = []
					controls.get_bindings(b, controls.shield_boost)
					root.info_print.visible = false
					messages.add("nectaire/normal", qsTr("story 8 1"))
					messages.add("nectaire/normal", qsTr("story 8 2 %1").arg(b[0]))
					messages.add("nectaire/normal", qsTr("story 8 3"))
					messages.add("nectaire/normal", qsTr("story 8 4"))
					messages.add("nectaire/normal", qsTr("story 8 5"))
					messages.add("nectaire/normal", qsTr("story 8 6 %1").arg(a[0]))
					messages.add("nectaire/normal", qsTr("story 8 7"))
				}
			}
		}
		State {id: state_after_story_20; SignalTransition {targetState: state_story_21; signal: state_after_story_20.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_21
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "request_pause"; value: true}
			SignalTransition
			{
				targetState: state_after_story_21
				signal: long_pause.value_changed
				guard: long_pause.value
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("story 9 1"))
					messages.add("lycop/normal", qsTr("story 9 2"))
					messages.add("nectaire/normal", qsTr("story 9 3"))
					messages.add("lycop/neutral", qsTr("story 9 4"))
					messages.add("nectaire/neutral", qsTr("story 9 5"))
					saved_game.set("file", "game/chapter-1/chapter-1b.qml")
					saved_game.set("travel/chapter-2/chapter-2", true)
					root.finalize = function() {scene_loader.save()}
				}
			}
		}
		State {id: state_after_story_21; SignalTransition {targetState: state_end; signal: state_after_story_21.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_end
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: screen_menu.about_screen_travel; property: "blink"; value: true}
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["chapter-1/satellite"])
		jobs.run(scene_view, "preload", ["chapter-1/satellite-lamps"])
		jobs.run(scene_view, "preload", ["chapter-1/drone"])
		jobs.run(scene_view, "preload", ["chapter-1/rabbit"])
		jobs.run(scene_view, "preload", ["chapter-1/boss"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(music, "preload", ["chapter-1/main"])
		jobs.run(music, "preload", ["chapter-1/boss"])

		groups.init(scene)
		root.info_print = screen_info_factory.createObject(screen_hud)
		root.finalize = function() {if(info_print) info_print.destroy()}
	}
}
