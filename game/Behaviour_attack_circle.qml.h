#include <cmath>

#include "Behaviour_attack.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct Behaviour_attack_circle
: Behaviour_attack
{
	AW_DECLARE_OBJECT_STUB(Behaviour_attack_circle)
	AW_DECLARE_PROPERTY_STORED(float, target_distance) = 0; ///< m
	AW_DECLARE_PROPERTY_STORED(float, target_angle) = 0; ///< rad
	AW_DECLARE_PROPERTY_STORED(float, angular_speed) = 1; ///< ∈ ]−∞,−0.1] ∪ [+0.1,+∞[

	void attack(b2Vec2 const &target) override
	{
		b2Vec2 direction = target - body()->position();
		float distance = direction.Normalize();
		b2Rot rot {target_angle()};
		body()->set_target_direction(b2MulT(rot, direction));
		direction = b2Mul(rot, b2Vec2(angular_speed(), target_distance() - distance));
		distance = direction.LengthSquared();
		if(distance > 1) direction *= 1 / std::sqrt(distance);
		body()->set_relative_velocity(direction);
	}
};

AW_DEFINE_OBJECT_STUB(Behaviour_attack_circle)
AW_DEFINE_PROPERTY_STORED(Behaviour_attack_circle, target_distance)
AW_DEFINE_PROPERTY_STORED(Behaviour_attack_circle, target_angle)
AW_DEFINE_PROPERTY_STORED(Behaviour_attack_circle, angular_speed)

}
}
}
