import QtQuick 2.0 as Q
import aw.game 0.0

Weapon
{
	property int level: 1
	property var joint
	property var data
	property var bullet
	signal bullet_destroyed(Bullet that)
	property real dot_dmg: [0,0,200][level] * scene.time_step
	period: scene.seconds_to_frames([0 , 1 / 3 , 1 / 3] [level])
	health: max_health +~~body.health_boost
	range_bullet: scene.seconds_to_frames(2)
	max_health: 200
	image: Image
	{
		material: "equipments/harpoon-launcher"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/harpoon-launcher/shooting"
		scale: slot.sound_scale
	}
	readonly property Sound sound_activate: Sound
	{
		objectName: "equipments/harpoon-launcher/activation"
		scale: slot.sound_scale
	}
	bullet_factory: Bullet
	{
		signal bullet_destroyed(Bullet that)
		Q.Component.onCompleted: weapon.on_completed(this)
		Q.Component.onDestruction: bullet_destroyed(this)
		scale: 0.4
		range: weapon.range_bullet
		damages: 1
		Image
		{
			material: "equipments/missile"
			z: altitudes.bullet
		}
		PolygonCollider
		{
			vertexes: [Qt.point(-0.1, -0.95), Qt.point(-0.1, 0.95), Qt.point(0.1, 0.95), Qt.point(0.1, -0.95)]
			group: groups.harpoon
			sensor: true
		}
		function begin(target, point)
		{
			if(target == weapon.body) return
			weapon.data = {body_2: target, position_2: target.point_from_scene(point)}
			deleteLater()
		}
	}
	Q.Component
	{
		id: joint_factory
		RopeJoint
		{
			body_1: body
			position_1: slot.point_to_body(Qt.point(0, 0))
			collide: true
			Q.Component.onCompleted: if(body_2 && typeof body_2.add_dot !== "undefined") body_2.add_dot(dot_dmg)
			Q.Component.onDestruction: if(body_2 && typeof body_2.add_dot !== "undefined") body_2.add_dot(-dot_dmg)
		}
	}
	Q.Connections
	{
		target: joint ? body.scene : null
		onFrame_end:
		{
			if(!joint.ready) return end()
			var p1 = joint.body_1.point_to_scene(joint.position_1)
			var p2 = joint.body_2.point_to_scene(joint.position_2)
			var x = p2.x - p1.x
			var y = p2.y - p1.y
			images.position = p1
			images.angle = Math.atan2(x, -y)
			var length = Math.sqrt(x * x + y * y)
			var n = Math.min(images.children.length, Math.ceil(length / 1.75))
			length = (length - 0.5) / (n - 0.5)
			for(var i = 0; i < n; ++i)
			{
				images.children[i].mask = "#CC9"
				images.children[i].position.y = (i + 0.5) * -length
			}
			for(var i = n; i < images.children.length; ++i)
			{
				images.children[i].mask = "transparent"
			}
		}
	}
	Q.Component.onDestruction:
	{
		timer.destroy()
		images.destroy()
		if(joint) joint.destroy()
		if(bullet) bullet.deleteLater()
	}
	readonly property var timer: Animation
	{
		parent: body.scene
		onTimeChanged: if(time <= 0) end()
	}
	readonly property Body images: Body
	{
		scene: body.scene
		Repeater
		{
			count: 40
			Q.Component
			{
				Image
				{
					material: "equipments/harpoon-launcher/rope"
					mask: "transparent"
				}
			}
		}
	}
	function on_completed(that)
	{
		if(bullet) bullet.deleteLater()
		bullet = that
		bullet.angle = slot.angle_to_scene(0)
		bullet.position = slot.point_to_scene(Qt.point(0, -1.4))
		bullet.bullet_destroyed.connect(bullet_destroyed)
		set_bullet_velocity(bullet, 20)
		sound_shooting.play()
		if(joint) end()
		data = null
	}
	onBullet_destroyed:
	{
		if(bullet == that) bullet = null
		if(data && !joint)
		{
			sound_activate.play()
			joint = joint_factory.createObject(null, data)
			timer.time = 10
			timer.speed = -1
		}
	}
	function end()
	{
		joint.destroy()
		joint = null
		timer.speed = 0
		for(var i in images.children) images.children[i].mask = "transparent"
	}
}
