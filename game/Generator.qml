import aw.game 0.0

Generator
{
	property int level: 1
	health: max_health
	max_health: 200 +~~body.health_boost
	energy_regeneration: [0, 30, 60][secondary_generator.level]
	image: Image
	{
		material: "equipments/secondary-generator"
		scale: 0.5
		mask: mask_equipment(parent)
	}
}
