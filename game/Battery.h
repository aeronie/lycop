#pragma once

#include "Equipment.h"

namespace aw {
namespace game {

struct Battery
: Equipment
{
	AW_DECLARE_OBJECT_STUB(Battery)
	AW_DECLARE_PROPERTY_STORED(float, max_energy) = 0; ///< J
protected:
	Battery() = default;
	~Battery() = default;
};

}
}
