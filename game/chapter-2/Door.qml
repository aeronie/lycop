import QtQuick 2.0 as Q
import aw.game 0.0

Vehicle
{
	id: root
	property bool vertical: false
	property var data
	property var door_scale
	readonly property int step: equipment ? 4 - Math.ceil(equipment.health * 4 / 1000) : 4
	readonly property string material: "chapter-2/%4/door/%1%2%3".arg(vertical ? "v" : "h").arg(step)
	type: Body.STATIC
	angle: vertical ? Math.PI / 2 : 0
	EquipmentSlot
	{
		equipment: Equipment
		{
			id: equipment
			health: 1000
		}
		onEquipmentChanged: root.deleteLater()
	}
	PolygonCollider
	{
		vertexes: [Qt.point(-2.4*door_scale, -0.3*door_scale), Qt.point(-2.4*door_scale, 0.3*door_scale), Qt.point(2.4*door_scale, 0.3*door_scale), Qt.point(2.4*door_scale, -0.3*door_scale)]
		group: groups.item_destructible
	}
	Repeater
	{
		count: 6
		Q.Component
		{
			Image
			{
				position.x: (Math.floor(index / 2) * 2 - 2)*door_scale
				position.y: ((index % 2) * 2 - 1)*door_scale
				material: root.material.arg((vertical ? ["ne", "nw", "e", "w", "se", "sw"] : ["nw", "sw", "n", "s", "ne", "se"])[index]).arg(data[index % 2][0])
				mask: data[index % 2][1].mask
				scale: door_scale
				angle: -root.angle
				z: altitudes.background_near
			}
		}
	}
	Q.Component.onDestruction:
	{
		for(var i in data) data[i][1].mask = "white"
	}
}
