import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-2/chapter-2c.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}

	property bool secret_decouvert: saved_game.self.get("secret_foreuse", false)
	property alias ship: ship
	signal screen_cleared
	property bool request_patrols: false
	property var camera_position: Qt.point(0, 0)
	property int enemy_count: 0
	property real t0: 0
	property real materiaux: 0
	property real wave: 1
	property var info_print
	property var harvester
	property var harvester_animation
	property bool harverster_alive: true
	property bool harverster_taken: false
	property bool spawn: false
	property var tab_enemy:[
	[1,1,1],
	[1,1,2],
	[2,2,1],
	[2,2,2],
	[2,2,3],
	[3,3,1],
	[3,3,2],
	[3,3,3],
	[3,3,4],
	[3,3,5]
	]
	Q.Component
	{
		id: laser_gun_factory
		LaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: aggressive_shield
		AggressiveShield
		{
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: shield_weapon
		Shield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: secondary_shield_weapon
		SecondaryShield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: heavy_laser_gun_factory
		HeavyLaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: acid_gun_factory
		AcidGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: gun_factory
		Gun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: missile_launcher_factory
		MissileLauncher
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: random_spray_factory
		RandomSpray
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			enemy: 1
			level: 2
		}
	}
	Q.Component
	{
		id: coil_gun_factory
		CoilGun
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: triple_gun_factory
		TripleGun
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: vortex_gun_factory
		VortexGun
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}

	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("Collected materials: %1 kg").arg(Math.floor(root.materiaux))
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}

	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false
		Background
		{
			z: altitudes.background_near
			scale: 10
			material: "chapter-2/surface"
		}
		Body
		{
			id: cratere
			scale: 20
			Image
			{
				material: "chapter-2/surface/crater"
				z: altitudes.background_near
			}
		}

		Ship
		{
			id: ship
			angle: Math.PI / 4
			position.y: 10 * (1 - t0)
			position.x: -10 * (1 - t0)
			Q.Component.onDestruction:
			{
				game_over = true
			}
		}
		Animation
		{
			id: harvest_material
			time: 0.1
			speed: -1
			property bool alive: true
			onAliveChanged: if(!alive) destroy()

			onTimeChanged:
			{
				if(root.scene)
				{
					root.materiaux += 20 * wave * root.scene.time_step
				}
			}
		}
		Animation
		{
			id: spawner
			time: -10
			speed: -1
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property int last_spawn: 0
			onTimeChanged:
			{
				if(time - last_spawn < -15)
				{
					root.spawn = true
					last_spawn = time
				}
			}
		}
	}
	Q.Component
	{
		id: drone_miner_factory
		Vehicle
		{
			id: drone
			function coords(x, y) {return Qt.point((x / 128 - 1), ((y-86) / 128 - 1))}
			max_speed: root.wave <= 6 ? 2 : 3
			max_angular_speed: 6
			scale: 2
			icon: 2
			icon_color: "blue"
			Q.Component.onDestruction: --root.enemy_count
			Q.Component.onCompleted: ++root.enemy_count
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				position: coords(128, 86+42)
				material: "folks/pirates/2"
				mask: Qt.rgba(0, 1, 1, 1)
			}
			Image
			{
				position.y: -0.2
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
				z: altitudes.shield
			}
			CircleCollider
			{
				position.y: -0.2
				group: shield ? groups.enemy_shield : 0
				radius: 1.1
			}
			PolygonCollider
			{
				vertexes: [coords(109,255),coords(12,211),coords(0,131),coords(75,84),coords(180,84),coords(254,132),coords(242,213),coords(145,255)]
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
			}
			CircleCollider
			{
				position.y: -0.2
				group: groups.enemy_miner_killer
				radius: 0.33
			}
			EquipmentSlot
			{
				scale: 0.33
				equipment: Shield
				{
					level: 2
					max_shield: 700 + 100 * (root.wave - 1)
				}
			}
			Behaviour_attack_circle
			{
				target_groups: groups.miner
				target_angle: 0
				target_distance: 0
			}
		}
	}
	Q.Component
	{
		id: drone_2_factory
		Vehicle
		{
			id: drone
			function coords(x, y) {return Qt.point((x / 128 - 1), ((y-86) / 128 - 1))}
			property var tab_weapons: [coil_gun_factory,triple_gun_factory,heavy_laser_gun_factory,missile_launcher_factory,laser_gun_factory]
			property int indice_weapon: random_index(tab_weapons.length)
			max_speed: 5
			max_angular_speed: 6
			scale: 2
			icon: 2
			icon_color: "red"
			Q.Component.onDestruction: --root.enemy_count
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				position: coords(128, 86+42)
				material: "folks/pirates/2"
			}
			PolygonCollider
			{
				vertexes: [coords(109,255),coords(12,211),coords(0,131),coords(75,84),coords(180,84),coords(254,132),coords(242,213),coords(145,255)]
				group: groups.enemy_hull_naked
			}
			EquipmentSlot
			{
				scale: 0.33
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
		}
	}
	Q.Component
	{
		id: dualdrone_factory
		Vehicle
		{
			id: dualdrone
			function coords(x, y, diff_x, diff_y) {return Qt.point(((x-diff_x) / 128 - 1), ((y-diff_y) / 128 - 1))}
			max_speed: behaviour ? 5 : 7
			max_angular_speed: behaviour ? 4 : 6
			scale: 2
			icon: 2
			icon_color: "red"
			property int behaviour: indice_weapon_1 && indice_weapon_2 ? random_index(2) : 1
			property var tab_weapons: [shield_weapon,coil_gun_factory,triple_gun_factory,heavy_laser_gun_factory,missile_launcher_factory,laser_gun_factory]
			property int indice_weapon_1: 0 //random_index(tab_weapons.length)
			property int indice_weapon_2: indice_weapon_1 ? random_index(tab_weapons.length) : random_index(tab_weapons.length - 1) + 1 //pas deux boucliers
			Q.Component.onCompleted: ++root.enemy_count, shield = max_shield
			Q.Component.onDestruction:
			{
				root.enemy_count++
				drone_2_factory.createObject(null, {scene: scene, position: Qt.point(dualdrone.position.x - 86/128 * Math.sin(dualdrone.angle), dualdrone.position.y + 86/128 * Math.cos(dualdrone.angle)), angle: dualdrone.angle + Math.PI})
				drone_2_factory.createObject(null, {scene: scene, position: Qt.point(dualdrone.position.x + 86/128 * Math.sin(dualdrone.angle), dualdrone.position.y - 86/128 * Math.cos(dualdrone.angle)), angle: dualdrone.angle})
			}

			loot_factory: Explosion
			{
				scale: 3
			}

			Image
			{
				material: "folks/pirates/2"
				position: Qt.point(0,-1)
			}
			Image
			{
				material: "folks/pirates/2"
				position: Qt.point(0,1)
				angle: Math.PI
			}

			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.45
				z: altitudes.shield
			}
			PolygonCollider
			{
				vertexes: [coords(75,255,0,-42),coords(0,211,0,-42),coords(0,131,0,128),coords(75,84,0,128),coords(180,84,0,128),coords(255,132,0,128),coords(254,211,0,-42),coords(180,255,0,-42)]
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
			}

			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				radius: 1.45
			}

			EquipmentSlot
			{
				position: coords(128, 168,0,0)
				angle: Math.PI
				scale: 0.33
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon_1].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position: coords(128, 88,0,0)
				scale: 0.33
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon_2].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				enabled: behaviour
				target_groups: groups.enemy_targets
				target_angle: indice_weapon_1 ? Math.PI : 0
				target_distance: 6
			}
			Behaviour_attack_harass
			{
				enabled: !behaviour
				target_groups: groups.enemy_targets
				relaxation_time: 1
				target_distance: (Math.random()- 0.5) * 20
			}
		}
	}

	Q.Component
	{
		id: drone_4_factory
		Vehicle
		{
			id: drone
			function coords(x, y) {return Qt.point((x / 128 - 1), (y / 128 - 1))}
			property var tab_weapons: [coil_gun_factory,missile_launcher_factory,laser_gun_factory]
			property int indice_weapon: random_index(tab_weapons.length)
			max_speed: 5
			max_angular_speed: 6
			scale: 2
			icon: 2
			icon_color: "red"
			Q.Component.onDestruction: --root.enemy_count
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				position: coords(128, 20)
				angle: - Math.PI / 4
				material: "folks/pirates/4"
			}
			PolygonCollider
			{
				vertexes: [coords(128,207),coords(-32,7),coords(32,-25),coords(224,-25),coords(288,7)]
				group: groups.enemy_hull_naked
			}
			EquipmentSlot
			{
				scale: 0.33
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
		}
	}
	Q.Component
	{
		id: quad_drone_factory
		Vehicle
		{
			id: quad_drone
			function coords(x, y) {return Qt.point((x / 256 - 1), (y / 256 - 1))}
			max_speed: 7
			max_angular_speed: 6
			scale: 2
			icon: 2
			icon_color: "red"
			property var tab_weapons: [coil_gun_factory,missile_launcher_factory,laser_gun_factory]

			property int indice_weapon_1: random_index(tab_weapons.length)

			loot_factory: Explosion
			{
				scale: 3
			}
			Q.Component.onCompleted: ++root.enemy_count, shield = max_shield
			Q.Component.onDestruction:
			{
				root.enemy_count+=3
				drone_4_factory.createObject(null, {scene: scene, position: Qt.point(quad_drone.position.x + 148/128 * Math.sin(quad_drone.angle + Math.PI / 4), quad_drone.position.y - 148/128 * Math.cos(quad_drone.angle + Math.PI / 4)), angle: quad_drone.angle + Math.PI / 4})

				drone_4_factory.createObject(null, {scene: scene, position: Qt.point(quad_drone.position.x + 148/128 * Math.sin(quad_drone.angle + 3 * Math.PI / 4), quad_drone.position.y - 148/128 * Math.cos(quad_drone.angle + 3 * Math.PI / 4)), angle: quad_drone.angle + 3 * Math.PI / 4})
				drone_4_factory.createObject(null, {scene: scene, position: Qt.point(quad_drone.position.x + 148/128 * Math.sin(quad_drone.angle - Math.PI / 4 ), quad_drone.position.y - 148/128 * Math.cos(quad_drone.angle - Math.PI / 4)), angle: quad_drone.angle - Math.PI / 4})
				drone_4_factory.createObject(null, {scene: scene, position: Qt.point(quad_drone.position.x + 148/128 * Math.sin(quad_drone.angle - 3 * Math.PI / 4), quad_drone.position.y - 148/128 * Math.cos(quad_drone.angle - 3 * Math.PI / 4)), angle: quad_drone.angle - 3 * Math.PI / 4})
			}

			Image
			{
				material: "folks/pirates/4"
				position: Qt.point(1,-1)
			}
			Image
			{
				material: "folks/pirates/4"
				position: Qt.point(-1,-1)
				angle: -Math.PI / 2
			}
			Image
			{
				material: "folks/pirates/4"
				position: Qt.point(-1,1)
				angle: Math.PI
			}
			Image
			{
				material: "folks/pirates/4"
				position: Qt.point(1,1)
				angle: Math.PI / 2
			}

			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
				z: altitudes.shield
			}
			EquipmentSlot
			{
				scale: 0.33
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(159, 156)
				angle: -Math.PI /4
				scale: 0.33
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon_1].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position: coords(353, 156)
				angle: Math.PI /4
				scale: 0.33
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon_1].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position: coords(353, 353)
				angle: 3 / 4 * Math.PI
				scale: 0.33
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon_1].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position: coords(159, 353)
				angle: - 3 / 4 * Math.PI
				scale: 0.33
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon_1].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}

			CircleCollider
			{
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
			}

			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				radius: 1.1
			}
		}
	}
	Q.Component
	{
		id: barre_factory
		Body
		{
			property real fuzzy_value: 0

			property bool alive: true
			onAliveChanged: if(!alive) destroy()

			Image
			{
				material: "progress-bar/" + Math.floor(fuzzy_value * 9)
				mask: fuzzy_value ? Qt.rgba(1, 1, 1, 1) : Qt.rgba(1, 1, 1, 0)
				z: altitudes.boss
				scale: 1
			}
		}
	}
	Q.Component
	{
		id: harvester_factory
		Sensor
		{
			id: harvester
			property var barre
			property real angle_drill: 0
			function coords(x, y) {return Qt.point((x / 128 - 1), (y / 128 - 1))}
			icon: 2
			icon_color: "yellow"
			duration_in: root.scene.seconds_to_frames(2)
			duration_out: 30
			onFuzzy_valueChanged:
			{
				barre.fuzzy_value = harvester.fuzzy_value
				if(fuzzy_value == 1)
				{
					root.harverster_taken = true
					harvester.destroy()
				}
			}

			Q.Component.onCompleted:
			{
				root.harvester_animation = harvester_anim.createObject(null, {parent: root.scene})
				barre = barre_factory.createObject(null, {scene: scene, position: Qt.point(harvester.position.x, harvester.position.y - harvester.scale - 0.2)})
			}
			Q.Component.onDestruction:
			{
				root.harverster_alive = false
				barre.alive = false
			}
			loot_factory: Explosion
			{
				scale: 1
			}
			Image
			{
				material: "chapter-2/drill/rotating"
				z: altitudes.boss
				angle: angle_drill
			}
			Image
			{
				material: "chapter-2/drill/still"
				z: altitudes.boss - 0.000001
			}
			CircleCollider
			{
				group: groups.miner
			}
			CircleCollider
			{
				group: groups.sensor
				sensor: true
			}
		}
	}

	Q.Component
	{
		id: harvester_anim
		Animation
		{
			id: timer
			property bool alive: root.harverster_alive
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(root.harvester)
				{
					root.harvester.angle_drill += 2 * Math.PI / 5 * root.scene.time_step
				}
			}
		}
	}

	StateMachine
	{
		begin: state_story_0// state_test
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_story_0
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					/*ship.position.y = 0
					camera_position = undefined*/
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					//drone_miner_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 20, ship.position.y), angle: 0})
					//quad_drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 20, ship.position.y), angle: 0})
					//dualdrone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 20, ship.position.y), angle: 0})
					//drone_2_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 40, ship.position.y), angle: 0})
					//drone_4_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 20, ship.position.y), angle: 0})
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 2000}
				onTriggered:
				{
					music.objectName = "chapter-2/main"
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					ship.position = Qt.point(0,0)
					camera_position = undefined
					messages.add("nectaire/normal", qsTr("story begin 1"))
					messages.add("lycop/normal", qsTr("story begin 2"))
					messages.add("nectaire/normal", qsTr("story begin 3"))
					messages.add("lycop/normal", qsTr("story begin 4"))
					messages.add("nectaire/normal", qsTr("story begin 5"))
					messages.add("lycop/normal", qsTr("story begin 6"))
					//creer la mine sous le vaisseau + animation
					harvester = harvester_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y), angle: 0})
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_2; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_story_2
				signal: root.spawnChanged
				guard: root.spawn && (root.wave <= tab_enemy.length)
				onTriggered:
				{
					root.spawn = false
					for(var i = 0; i < root.tab_enemy[root.wave-1][0]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[wave - 1][0]
						quad_drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + Math.cos(angle)*40, ship.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
					for(var i = 0; i < root.tab_enemy[root.wave-1][1]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[wave - 1][1] - Math.PI * 1 / 16
						dualdrone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + Math.cos(angle)*40, ship.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
					for(var i = 0; i < root.tab_enemy[root.wave-1][2]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[wave - 1][2] - Math.PI * 2 / 16
						drone_miner_factory.createObject(null, {scene: scene, position: Qt.point(harvester.position.x + Math.cos(angle)*40, harvester.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
					root.wave++
				}
			}
			SignalTransition
			{
				targetState: state_story_2_dialogue
				signal: root.waveChanged
				guard: root.wave > tab_enemy.length && !secret_decouvert
				onTriggered:
				{
					secret_decouvert = true
					messages.add("nectaire/normal", qsTr("story secret 1"))
					messages.add("lycop/normal", qsTr("story secret 2"))
					messages.add("nectaire/normal", qsTr("story secret 3"))
				}
			}
			SignalTransition
			{
				targetState: state_story_2
				signal: root.spawnChanged
				guard: root.spawn && (root.wave > tab_enemy.length) && secret_decouvert
				onTriggered:
				{
					root.spawn = false
					for(var i = 0; i < root.tab_enemy[tab_enemy.length-1][0]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[tab_enemy.length - 1][0]
						quad_drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + Math.cos(angle)*40, ship.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
					for(var i = 0; i < root.tab_enemy[tab_enemy.length-1][1]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[tab_enemy.length - 1][1] - Math.PI * 1 / 16
						dualdrone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + Math.cos(angle)*40, ship.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
					for(var i = 0; i < root.tab_enemy[tab_enemy.length-1][2]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[tab_enemy.length - 1][2] - Math.PI * 2 / 16
						drone_miner_factory.createObject(null, {scene: scene, position: Qt.point(harvester.position.x + Math.cos(angle)*40, harvester.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
				}
			}
			SignalTransition
			{
				targetState: state_end
				signal: root.harverster_aliveChanged
				guard: !root.harverster_alive && !root.harverster_taken
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("story loose 1"))
					messages.add("lycop/normal", qsTr("story loose 2"))
					saved_game.set("file", "game/chapter-transition/chapter-transition.qml")
					saved_game.save()
				}
			}
			SignalTransition
			{
				targetState: state_end
				signal: root.harverster_aliveChanged
				guard: !root.harverster_alive && root.harverster_taken
				onTriggered:
				{
					saved_game.set("secret_foreuse", secret_decouvert)
					if(secret_decouvert)
					{
						platform.set_bool("secret-foreuse")
					}
					messages.add("nectaire/normal", qsTr("story win 1 %1").arg(Math.floor(root.materiaux)))
					messages.add("lycop/normal", qsTr("story win 2"))
					saved_game.add_matter(root.materiaux)
					saved_game.set("file", "game/chapter-transition/chapter-transition.qml")
					saved_game.save()
				}
			}
		}
		State {id: state_story_2_dialogue; SignalTransition {targetState: state_story_2; signal: state_story_2_dialogue.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["chapter-2/surface"])
		jobs.run(scene_view, "preload", ["chapter-2/surface/crater"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		jobs.run(scene_view, "preload", ["folks/pirates/2"])
		jobs.run(scene_view, "preload", ["folks/pirates/4"])
		jobs.run(scene_view, "preload", ["chapter-2/drill/rotating"])
		jobs.run(scene_view, "preload", ["chapter-2/drill/still"])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["progress-bar/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		jobs.run(music, "preload", ["chapter-2/main"])
		groups.init(scene)
		root.info_print = screen_info_factory.createObject(screen_hud)
		root.finalize = function() {if(info_print) info_print.destroy()}
	}
}
