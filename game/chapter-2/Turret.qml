import QtQuick 2.0
import aw.game 0.0
import "qrc:///game"

Vehicle
{
	id: turret
	property int activate: 0
	property int weapon_choice: 0
	type: Body.KINEMATIC
	max_speed: 0
	icon: 3
	icon_color: activate ? "red" : "transparent"
	property bool alive: true
	onAliveChanged: if(!alive) destroy()
	loot_factory: Repeater
	{
		Component {Loot {}}
		Component {Explosion {scale: 2}}
	}
	Image
	{
		material: "chapter-2/square-turret"
		mask: Qt.rgba(1, 1, 1, turret.activate)
	}
	CircleCollider
	{
		id: collider
		group: turret.activate ? groups.enemy_hull_naked : 0
		density: 1e5
	}

	property list<Component> weapons: [
	Component
	{
		LaserGun
		{
			level: 2
			image.mask.a: turret.activate
			shooting: behaviour.shooting
		}
	},
	Component
	{
		HeavyLaserGun
		{
			level: 2
			image.mask.a: turret.activate
			shooting: behaviour.shooting
		}
	},
	Component
	{
		TripleGun
		{
			level: 2
			image.mask.a: turret.activate
			shooting: behaviour.shooting
		}
	},
	Component
	{
		Quipoutre
		{
			level: 2
			image.mask.a: turret.activate
			shooting: behaviour.shooting
			range_bullet: scene.seconds_to_frames(20)
		}
	}]
	EquipmentSlot
	{
		scale: 0.7
		Component.onCompleted: equipment = turret.weapons[turret.weapon_choice].createObject(null, {slot: this})
		onEquipmentChanged: if(!equipment) turret.deleteLater()
	}
	Behaviour_attack_distance
	{
		id: behaviour
		target_groups: turret.activate ? groups.enemy_targets : 0
		max_distance: 40
	}
}
