import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-2/chapter-2.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal one_boss_destroyed
	signal screen_cleared
	property bool request_patrols: false
	property var camera_position: Qt.point(0, 0)
	property bool save_boss : saved_game.self.get("save_boss", false)
	property bool mod_easy : saved_game.self.get("mod_easy", false)
	property real t0: 0
	property real t1: 0
	property real t2: 0
	property real t3: 0
	property int enemy_count: 0
	property bool base: true
	property real current_position_x: 0
	property real current_position_y: 0
	property real ship_angle: 0
	property real boss_position_x: 0
	property real boss_position_y: 0
	property int elevator_state: 0

	Q.Component
	{
		id: laser_gun_factory
		LaserGun
		{
			level: 2
		}
	}
	Q.Component
	{
		id: heavy_laser_gun_factory
		HeavyLaserGun
		{
			level: 2
		}
	}

	Q.Component
	{
		id: missile_launcher_factory
		MissileLauncher
		{
			level: 2
		}
	}

	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	function next_checkpoint_position() {return Qt.point(ship.position.x + (Math.random()+3)*(root.random_sign())*(10), ship.position.y + (Math.random()+3)*(root.random_sign())*(10))}

	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false
		Background
		{
			z: altitudes.background_near
			scale: 10
			material: "chapter-2/surface"
		}
		Sensor
		{
			id: timer_patrols
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(20)
			target_value: !value && request_patrols
		}
		Ship
		{
			id: ship
			function move_ship_to_base() {return Qt.point(t1 * (boss_position_x - current_position_x) + current_position_x ,t1 * (boss_position_y - current_position_y) + current_position_y)}

			function face_ship_to_base()
			{
				return (1 - t0) * ship_angle
			}
			//ship_angle entre -pi et pi
			function calcul_ship_angle()
			{
				var result = ship.angle
				while(result < -Math.PI)
				{
					result += 2 * Math.PI
				}
				while(result > Math.PI)
				{
					result -= 2 * Math.PI
				}
				return result
			}
			function enter_ship_in_base() {return Qt.point(t3 * (boss_position_x - current_position_x) + current_position_x ,t3 * (boss_position_y - current_position_y) + current_position_y)}

			position.y: root.save_boss ? 0 : 10 * (1 - t0)
			Q.Component.onDestruction: game_over = true
		}
		Sensor
		{
			id: checkpoint
			type: Body.STATIC
			duration_in: 50
			duration_out: 1
			icon: 0
			icon_color: "#06F"
			CircleCollider
			{
				id: checkpoint_collider
				group: groups.sensor
				sensor: true
				radius: 7
			}
		}
		Animation {id: animation}
	}
	Q.Component
	{
		id: wall_factory
		Background
		{
			y: 1
			z: -0.0001
			scale: 20*1.29
			position.y: boss_position_y
			shift_x: boss_position_x
			material: "chapter-2/surface/cliff"
			PolygonCollider
			{
				vertexes: [Qt.point(5,-0.6), Qt.point(-5,-0.6), Qt.point(-5,-5), Qt.point(5,-5)]
				group: groups.wall
			}
		}
	}
	Q.Component
	{
		id: black_factory
		Body
		{
			id: obscurcir
			scale: 40
			position.y: boss_position_y
			position.x: boss_position_x
			property color mask: Qt.rgba( 1, 1, 1, t2/10 )
			Image
			{
				id: image
				material: "chapter-2/rectangle_noir"
				mask: obscurcir.mask
				z: 0.003
			}
		}
	}
	Q.Component
	{
		id: rock_factory
		Background
		{
			y: 1
			z: -0.0001
			scale: 20*1.29
			position.y: boss_position_y + 2 * scale
			shift_x: boss_position_x
			material: "chapter-2/surface/rocks"
		}
	}
	Q.Component
	{
		id: elevator_factory
		Body
		{
			Image
			{
				z: -0.00005
				scale: 20*1.29 * (1 + t2)
				material: ["chapter-2/surface/entry-closed","chapter-2/surface/entry-boss","chapter-2/surface/entry-open"][elevator_state]
			}
		}
	}
	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			id: drone
			property var weapon_factory
			max_speed: 5
			icon: 2
			icon_color: "red"
			Q.Component.onCompleted: ++root.enemy_count
			Q.Component.onDestruction: --root.enemy_count
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				material: "folks/heter/1"
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
			EquipmentSlot
			{
				position.y: 0.3
				scale: 0.5
				Q.Component.onCompleted: equipment = weapon_factory.createObject(null, {slot: this, shooting: true})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
		}
	}
	Q.Component
	{
		id: clear_screen_factory
		Vehicle
		{
			scale: 50
			type: Body.KINEMATIC
			CircleCollider
			{
				group: groups.clear_screen
			}
		}
	}
	Q.Component
	{
		id: clear_screen_animation
		Animation
		{
			id: timer
			property bool first_time: true
			property var clear_screen
			time: 0.1
			speed: -1

			onTimeChanged:
			{
				if(first_time)
				{
					clear_screen=clear_screen_factory.createObject(null, {scene: scene, position: Qt.point(root.boss_position_x,root.boss_position_y)})
					first_time= false
				}
				if(time<0)
				{
					clear_screen.deleteLater()
					screen_cleared()
					timer.deleteLater()
				}
			}
		}
	}
	Q.Component
	{
		id: turret_factory
		Vehicle
		{
			id: turret
			property var weapon_factory
			max_speed: 0
			icon: 3
			icon_color: "red"
			type: Body.KINEMATIC
			Q.Component.onCompleted: ++root.enemy_count, animation.time = 0, animation.speed = 10
			Q.Component.onDestruction: --root.enemy_count
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				id: image
				material: "chapter-2/turret-opening/" + Math.floor(Math.max(0, Math.min(14, animation.time - 1)))
				mask: Qt.rgba(1, 1, 1, animation.time)
			}
			Image
			{
				material: "chapter-2/round-turret"
				scale: slot.scale
			}
			CircleCollider
			{
				id: collider
			}
			EquipmentSlot
			{
				id: slot
				scale: Math.max(0, animation.time - 10) / 10
				Q.Component.onCompleted: equipment = weapon_factory.createObject(null, {slot: this})
				onEquipmentChanged: if(!equipment) turret.deleteLater()
				onScaleChanged: if(equipment && scale >= 1)
				{
					animation.speed = 0
					image.mask = "transparent"
					collider.group = groups.enemy_hull_naked
					equipment.shooting = true
				}
			}
			Behaviour_attack_distance
			{
				target_groups: groups.enemy_targets
			}
		}
	}
	Q.Component
	{
		id: cliff_turret_factory
		Vehicle
		{
			max_speed: 0
			property bool survive: root.base
			onSurviveChanged: if(!survive) destroy()
			type: Body.KINEMATIC
			EquipmentSlot
			{
				position.y: 0.5
				equipment: MaserEmitter
				{
					shooting: true
					level: 2
					power: 1.5
				}
			}
			Behaviour_attack_distance
			{
				target_groups: groups.enemy_targets
			}
		}
	}
	Q.Component
	{
		id: base_factory
		Vehicle
		{
			id: component
			property bool onfire: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} // plink fait des images de boss en 2048x2048
			function group_of(x) {return x ? shield ? groups.enemy_hull : groups.enemy_hull_naked : 0}
			property int active_bloc_up_left_left: 3
			property int active_bloc_up_left: 4
			property int active_bloc_left_left_left: 3
			property int active_bloc_left_left: 3
			property int active_bloc_left_bot: 3
			property int active_bloc_bot_left_left: 4
			property int active_bloc_bot_left_bot: 3
			property int active_bloc_bot_left: 3
			property int active_bloc_up_right_right: 3
			property int active_bloc_up_right: 4
			property int active_bloc_right_right_right: 3
			property int active_bloc_right_right: 3
			property int active_bloc_right_bot: 3
			property int active_bloc_bot_right_right: 4
			property int active_bloc_bot_right_bot: 3
			property int active_bloc_bot_right: 3
			property int active_bloc_right: 4
			property int active_bloc_left: 4
			property int active_bloc_right_up: 3
			property int active_bloc_left_up: 3
			property int active_bloc_mid: 10
			property int active_bloc_mid_mid: 3
			readonly property bool alive: active_bloc_up_left_left || active_bloc_up_left || active_bloc_left_left_left || active_bloc_left_left || active_bloc_left_bot || active_bloc_bot_left_left || active_bloc_bot_left_bot || active_bloc_bot_left || active_bloc_up_right_right || active_bloc_up_right || active_bloc_right_right_right || active_bloc_right_right || active_bloc_right_bot || active_bloc_bot_right_right || active_bloc_bot_right_bot || active_bloc_bot_right || active_bloc_right || active_bloc_left || active_bloc_right_up || active_bloc_left_up || active_bloc_mid || active_bloc_mid_mid
			onAliveChanged: if(!alive) destroy()
			type: Body.STATIC
			icon: 5
			icon_color: "red"
			max_speed: 0
			max_angular_speed: 0
			scale: 20
			Q.Component.onCompleted: shield = max_shield
			Q.Component.onDestruction: one_boss_destroyed()
			loot_factory: Repeater
			{
				count: 30
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "chapter-2/boss-1"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				position: coords(1024, 572)
				scale: 1.1
				z: altitudes.shield
			}
			PolygonCollider
			{
				vertexes: [coords(808, 687), coords(1242, 687), coords(1242, 1055), coords(808, 1055)]
				group: group_of(active_bloc_mid)
			}
			PolygonCollider
			{
				vertexes: [coords(851, 749), coords(851, 927), coords(1197, 927), coords(1197, 749)]
				group: group_of(active_bloc_mid_mid)
			}

			//shield
			CircleCollider
			{
				group: component.shield ? groups.enemy_shield_ignore_wall : 0
				position: coords(1024, 572)
				radius: 1.1
			}

			//gros gauche
			CircleCollider
			{
				group: group_of(active_bloc_left)
				position: coords(627, 817)
				radius: 135/1024
			}

			//gros droite
			CircleCollider
			{
				group: group_of(active_bloc_right)
				position: coords(1421, 818)
				radius: 135/1024
			}

			//moyen fond droite
			CircleCollider
			{
				group: group_of(active_bloc_right_up)
				position: coords(1207, 577)
				radius: 120/1024
			}

			//moyen fond gauche
			CircleCollider
			{
				group: group_of(active_bloc_left_up)
				position: coords(839, 577)
				radius: 120/1024
			}

			//bloc de 2 en haut a gauche
			CircleCollider
			{
				group: group_of(active_bloc_up_left_left)
				position: coords(117, 724)
				radius: 87/1024
			}
			CircleCollider
			{
				group: group_of(active_bloc_up_left)
				position: coords(257, 771)
				radius: 87/1024
			}

			//bloc des 3 a gauche
			CircleCollider
			{
				group: group_of(active_bloc_left_left_left)
				position: coords(177, 968)
				radius: 87/1024
			}
			CircleCollider
			{
				group: group_of(active_bloc_left_left)
				position: coords(320, 1026)
				radius: 87/1024
			}
			CircleCollider
			{
				group: group_of(active_bloc_left_bot)
				position: coords(285, 1172)
				radius: 87/1024
			}

			//bloc des trois en bas a gauche
			CircleCollider
			{
				group: group_of(active_bloc_bot_left)
				position: coords(660, 1160)
				radius: 87/1024
			}
			CircleCollider
			{
				group: group_of(active_bloc_bot_left_left)
				position: coords(516, 1204)
				radius: 87/1024
			}
			CircleCollider
			{
				group: group_of(active_bloc_bot_left_bot)
				position: coords(722, 1294)
				radius: 87/1024
			}

			//bloc des trois en bas a droite
			CircleCollider
			{
				group: group_of(active_bloc_bot_right_bot)
				position: coords(1326, 1294)
				radius: 87/1024
			}
			CircleCollider
			{
				group: group_of(active_bloc_bot_right)
				position: coords(1387, 1154)
				radius: 87/1024
			}
			CircleCollider
			{
				group: group_of(active_bloc_bot_right_right)
				position: coords(1531, 1195)
				radius: 87/1024
			}

			//bloc des trois tout a droite
			CircleCollider
			{
				group: group_of(active_bloc_right_right)
				position: coords(1729, 1029)
				radius: 87/1024
			}
			CircleCollider
			{
				group: group_of(active_bloc_right_bot)
				position: coords(1766, 1174)
				radius: 87/1024
			}
			CircleCollider
			{
				group: group_of(active_bloc_right_right_right)
				position: coords(1872, 972)
				radius: 87/1024
			}

			//bloc de 2 en haut à droite
			CircleCollider
			{
				group: group_of(active_bloc_up_right)
				position: coords(1794, 774)
				radius: 87/1024
			}
			CircleCollider
			{
				group: group_of(active_bloc_up_right_right)
				position: coords(1934, 724)
				radius: 87/1024
			}

			//bloc des 2 en haut à gauche
			EquipmentSlot
			{
				position: coords(78, 709)
				scale: 0.15*6/component.scale
				angle: Math.PI * -1/2
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_left_left
				}
			}
			EquipmentSlot
			{
				position: coords(85, 753)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_left_left
				}
			}
			EquipmentSlot
			{
				position: coords(136, 757)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_left_left
				}
			}

			EquipmentSlot
			{
				position: coords(211, 711)
				scale: 0.12*6/component.scale
				angle: Math.PI * -5/8
				equipment: MissileLauncher
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_left
				}
			}
			EquipmentSlot
			{
				position: coords(283, 736)
				scale: 0.15*6/component.scale
				angle: Math.PI * -5/8
				equipment: RandomSpray
				{
					shooting: onfire
					enemy: 1
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_left
				}
			}
			EquipmentSlot
			{
				position: coords(309, 782)
				scale: 0.15*6/component.scale
				angle: Math.PI * -5/8
				equipment: RandomSpray
				{
					shooting: onfire
					enemy: 1
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_left
				}
			}
			EquipmentSlot
			{
				position: coords(258, 856)
				scale: 0.12*6/component.scale
				angle: Math.PI * -5/8
				equipment: MissileLauncher
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_left
				}
			}

			//bloc des 3 tout a gauche
			EquipmentSlot
			{
				position: coords(132, 955)
				scale: 0.15*6/component.scale
				angle: Math.PI * -1/2
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_left_left
				}
			}
			EquipmentSlot
			{
				position: coords(149, 1009)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_left_left
				}
			}
			EquipmentSlot
			{
				position: coords(191, 1021)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_left_left
				}
			}

			EquipmentSlot
			{
				position: coords(285, 1021)
				scale: 0.15*6/component.scale
				angle: Math.PI * -5/8
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_left
				}
			}
			EquipmentSlot
			{
				position: coords(326, 1069)
				scale: 0.15*6/component.scale
				angle: Math.PI * -7/8
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_left
				}
			}
			EquipmentSlot
			{
				position: coords(365, 986)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: RandomSpray
				{
					shooting: onfire
					enemy: 1
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_left
				}
			}

			EquipmentSlot
			{
				position: coords(236, 1184)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_bot
				}
			}
			EquipmentSlot
			{
				position: coords(274, 1223)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_bot
				}
			}
			EquipmentSlot
			{
				position: coords(321, 1207)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_bot
				}
			}

			//bloc des 3 en bas à gauche
			EquipmentSlot
			{
				position: coords(629, 1180)
				scale: 0.15*6/component.scale
				angle: Math.PI * -7/8
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_left
				}
			}
			EquipmentSlot
			{
				position: coords(690, 1180)
				scale: 0.15*6/component.scale
				angle: Math.PI * 7/8
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_left
				}
			}
			EquipmentSlot
			{
				position: coords(624, 1105)
				scale: 0.15*6/component.scale
				angle: Math.PI * -7/9
				equipment: RandomSpray
				{
					shooting: onfire
					enemy: 1
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_left
				}
			}

			EquipmentSlot
			{
				position: coords(523, 1160)
				scale: 0.23*6/component.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_left_left
				}
			}
			EquipmentSlot
			{
				position: coords(470, 1224)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_left_left
				}
			}
			EquipmentSlot
			{
				position: coords(519, 1238)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_left_left
				}
			}
			EquipmentSlot
			{
				position: coords(560, 1224)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_left_left
				}
			}

			EquipmentSlot
			{
				position: coords(684, 1313)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_left_bot
				}
			}
			EquipmentSlot
			{
				position: coords(716, 1337)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_left_bot
				}
			}
			EquipmentSlot
			{
				position: coords(755, 1320)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_left_bot
				}
			}

			//gros bloc à gauche
			EquipmentSlot
			{
				position: coords(624, 755)
				scale: 0.30*6/component.scale
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left
				}
			}
			EquipmentSlot
			{
				position: coords(564, 842)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left
				}
			}
			EquipmentSlot
			{
				position: coords(626, 877)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left
				}
			}
			EquipmentSlot
			{
				position: coords(682, 842)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left
				}
			}

			//moyen bloc fond gauche
			EquipmentSlot
			{
				position: coords(838, 538)
				scale: 0.30*6/component.scale
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_up
				}
			}
			EquipmentSlot
			{
				position: coords(794, 622)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_up
				}
			}
			EquipmentSlot
			{
				position: coords(888, 622)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_left_up
				}
			}

			//partie droite

			//bloc des 2 en haut à droite
			EquipmentSlot
			{
				position: coords(1967, 700)
				scale: 0.15*6/component.scale
				angle: Math.PI * 1/2
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_right_right
				}
			}
			EquipmentSlot
			{
				position: coords(1962, 753)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_right_right
				}
			}
			EquipmentSlot
			{
				position: coords(1910, 778)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_right_right
				}
			}

			EquipmentSlot
			{
				position: coords(1811, 709)
				scale: 0.12*6/component.scale
				angle: Math.PI * 5/8
				equipment: MissileLauncher
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_right
				}
			}
			EquipmentSlot
			{
				position: coords(1767, 746)
				scale: 0.15*6/component.scale
				angle: Math.PI * 5/8
				equipment: RandomSpray
				{
					shooting: onfire
					enemy: 1
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_right
				}
			}
			EquipmentSlot
			{
				position: coords(1758, 791)
				scale: 0.15*6/component.scale
				angle: Math.PI * 5/8
				equipment: RandomSpray
				{
					shooting: onfire
					enemy: 1
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_right
				}
			}
			EquipmentSlot
			{
				position: coords(1798, 843)
				scale: 0.12*6/component.scale
				angle: Math.PI * 5/8
				equipment: MissileLauncher
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_up_right
				}
			}

			//bloc des 3 tout a droite
			EquipmentSlot
			{
				position: coords(1910, 963)
				scale: 0.15*6/component.scale
				angle: Math.PI * 1/2
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_right_right
				}
			}
			EquipmentSlot
			{
				position: coords(1899, 1005)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_right_right
				}
			}
			EquipmentSlot
			{
				position: coords(1863, 1026)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_right_right
				}
			}

			EquipmentSlot
			{
				position: coords(1762, 1029)
				scale: 0.15*6/component.scale
				angle: Math.PI * 5/8
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_right
				}
			}
			EquipmentSlot
			{
				position: coords(1723, 1073)
				scale: 0.15*6/component.scale
				angle: Math.PI * 7/8
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_right
				}
			}
			EquipmentSlot
			{
				position: coords(1680, 986)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: RandomSpray
				{
					shooting: onfire
					enemy: 1
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_right
				}
			}

			EquipmentSlot
			{
				position: coords(1812, 1187)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_bot
				}
			}
			EquipmentSlot
			{
				position: coords(1766, 1217)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_bot
				}
			}
			EquipmentSlot
			{
				position: coords(1725, 1204)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_bot
				}
			}

			//bloc des 3 en bas à droite
			EquipmentSlot
			{
				position: coords(1414, 1180)
				scale: 0.15*6/component.scale
				angle: Math.PI * 7/8
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_right
				}
			}
			EquipmentSlot
			{
				position: coords(1361, 1180)
				scale: 0.15*6/component.scale
				angle: Math.PI * -7/8
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_right
				}
			}
			EquipmentSlot
			{
				position: coords(1437, 1105)
				scale: 0.15*6/component.scale
				angle: Math.PI * 7/9
				equipment: RandomSpray
				{
					shooting: onfire
					enemy: 1
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_right
				}
			}

			EquipmentSlot
			{
				position: coords(1521, 1160)
				scale: 0.23*6/component.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_right_right
				}
			}
			EquipmentSlot
			{
				position: coords(1567, 1226)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_right_right
				}
			}
			EquipmentSlot
			{
				position: coords(1528, 1241)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_right_right
				}
			}
			EquipmentSlot
			{
				position: coords(1492, 1226)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_right_right
				}
			}

			EquipmentSlot
			{
				position: coords(1367, 1324)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_right_bot
				}
			}
			EquipmentSlot
			{
				position: coords(1329, 1346)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_right_bot
				}
			}
			EquipmentSlot
			{
				position: coords(1288, 1324)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_bot_right_bot
				}
			}

			//gros bloc à droite
			EquipmentSlot
			{
				position: coords(1424, 755)
				scale: 0.30*6/component.scale
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right
				}
			}
			EquipmentSlot
			{
				position: coords(1360, 842)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right
				}
			}
			EquipmentSlot
			{
				position: coords(1416, 877)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right
				}
			}
			EquipmentSlot
			{
				position: coords(1470, 842)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right
				}
			}

			//moyen bloc fond droite
			EquipmentSlot
			{
				position: coords(1201, 538)
				scale: 0.30*6/component.scale
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_up
				}
			}
			EquipmentSlot
			{
				position: coords(1163, 622)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_up
				}
			}
			EquipmentSlot
			{
				position: coords(1256, 622)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: HeavyLaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_right_up
				}
			}

			//bloc millieu avancé
			EquipmentSlot
			{
				position: coords(875, 1016)
				scale: 0.15*6/component.scale
				angle: Math.PI * -7/8
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid
				}
			}
			EquipmentSlot
			{
				position: coords(905, 1016)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid
				}
			}
			EquipmentSlot
			{
				position: coords(935, 1016)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid
				}
			}
			EquipmentSlot
			{
				position: coords(965, 1016)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid
				}
			}
			EquipmentSlot
			{
				position: coords(995, 1016)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid
				}
			}
			EquipmentSlot
			{
				position: coords(1053, 1016)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid
				}
			}
			EquipmentSlot
			{
				position: coords(1083, 1016)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid
				}
			}
			EquipmentSlot
			{
				position: coords(1113, 1016)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid
				}
			}
			EquipmentSlot
			{
				position: coords(1143, 1016)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid
				}
			}
			EquipmentSlot
			{
				position: coords(1173, 1016)
				scale: 0.15*6/component.scale
				angle: Math.PI * 7/8
				equipment: LaserGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid
				}
			}

			//bloc millieu milieu
			EquipmentSlot
			{
				position: coords(921, 804)
				scale: 0.15*6/component.scale
				angle: Math.PI * -3/4
				equipment: TripleGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid_mid
				}
			}
			EquipmentSlot
			{
				position: coords(1024, 804)
				scale: 0.15*6/component.scale
				angle: Math.PI
				equipment: TripleGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid_mid
				}
			}
			EquipmentSlot
			{
				position: coords(1127, 804)
				scale: 0.15*6/component.scale
				angle: Math.PI * 3/4
				equipment: TripleGun
				{
					shooting: onfire
					level: 2
					Q.Component.onDestruction: --component.active_bloc_mid_mid
				}
			}
		}
	}

	StateMachine
	{
		begin: state_story_0 // state_test
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_before_checkpoint_5 //state_test_1
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					ship.position.y = 0
					camera_position = undefined
					//animation_turret_factory.createObject(null, {scene: scene, position: Qt.point( ship.position.x + 3, ship.position.y + 3), angle: 0 , weapon_type: 4, vulnerable : 0, lifetime: scene.seconds_to_frames(1.125) })
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 30, ship.position.y + 30), weapon_factory: laser_gun_factory})
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				guard: !root.save_boss
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 2000}
				onTriggered:
				{
					music.objectName = "chapter-2/main"
					saved_game.set("travel/chapter-2/chapter-2", false)
				}
			}
			SignalTransition
			{
				targetState: state_before_checkpoint_5
				signal: state_story_0.propertiesAssigned
				guard: root.save_boss
				onTriggered:
				{
					music.objectName = "chapter-2/main"
					ship.position.y = 0
					camera_position = undefined
					drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 30, ship.position.y + 30), weapon_factory: laser_gun_factory})
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_after_story_1
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					ship.position.y = 0
					camera_position = undefined

					messages.add("nectaire/normal", qsTr("story 1 1"))
					messages.add("lycop/normal", qsTr("story 1 2"))
					messages.add("nectaire/normal", qsTr("story 1 3"))
					messages.add("lycop/normal", qsTr("story 1 4"))
					messages.add("nectaire/normal", qsTr("story 1 5"))
					messages.add("nectaire/normal", qsTr("story 1 6"))
					checkpoint.position = next_checkpoint_position()
				}
			}
		}
		State {id: state_after_story_1; SignalTransition {targetState: state_search_1; signal: state_after_story_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: checkpoint; property: "icon"; value: 1}
			AssignProperty {target: root; property: "request_patrols"; value: true}
			SignalTransition
			{
				signal: timer_patrols.value_changed
				guard: timer_patrols.value && !state_search_5.active
				onTriggered:
				{
					var z = [
						[0, 40, 1],
						[0, -40, 0],
						[-40, 0, 1/2],
						[40, 0, -1/2],
						]
					for(var i = 0; i < z.length; ++i) drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + z[i][0], ship.position.y - z[i][1]), angle: z[i][2] * Math.PI, weapon_factory: laser_gun_factory})
				}
			}
			State
			{
				id: state_search_1
				SignalTransition
				{
					targetState: state_after_checkpoint_1
					signal: checkpoint.value_changed
					guard: checkpoint.value
					onTriggered:
					{
						messages.add("lycop/neutral", qsTr("point 1 1"))
						messages.add("nectaire/panic", qsTr("point 1 2"))
						messages.add("lycop/normal", qsTr("point 1 3"))

						var z = [
							[0, 15, 0],
							[10, 10, -1/4],
							[-10, 10, 1/4],
							[10, -10, -3/4],
							[-10, -10, 3/4],
							[0, -15, 1],
							]
						for(var i = 0; i < z.length; ++i) turret_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + z[i][0], ship.position.y + z[i][1]), angle: z[i][2] * Math.PI, weapon_factory: heavy_laser_gun_factory, lifetime: scene.seconds_to_frames(1.125)})

						var z = [
							[30, -5, -1/2],
							[30, -3, -1/2],
							[30, 0, -1/2],
							[30, 3, -1/2],
							[30, 5, -1/2],
							[-30, -5, 1/2],
							[-30, -3, 1/2],
							[-30, 0, 1/2],
							[-30, 3, 1/2],
							[-30, 5, 1/2],
							]
						for(var i = 0; i < z.length; ++i) drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + z[i][0], ship.position.y - z[i][1]), angle: z[i][2] * Math.PI, weapon_factory: laser_gun_factory})
					}
				}
			}
			State {id: state_after_checkpoint_1; SignalTransition {targetState: state_before_checkpoint_2; signal: state_after_checkpoint_1.propertiesAssigned; guard: !messages.has_unread}}
			State
			{
				id: state_search_2
				SignalTransition
				{
					targetState: state_before_checkpoint_3
					signal: checkpoint.value_changed
					guard: checkpoint.value
					onTriggered:
					{
						var z = [
							[0, 10, 0, heavy_laser_gun_factory],
							[5, 10, 0, laser_gun_factory],
							[-5, 10, 0, laser_gun_factory],
							[10, 10, 0, laser_gun_factory],
							[-10, 10, 0, laser_gun_factory],
							[0, -10, 1, heavy_laser_gun_factory],
							[5, -10, 1, laser_gun_factory],
							[-5, -10, 1, laser_gun_factory],
							[10, -10, 1, laser_gun_factory],
							[-10, -10, 1, laser_gun_factory],
							]
						for(var i = 0; i < z.length; ++i) turret_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + z[i][0], ship.position.y + z[i][1]), angle: z[i][2] * Math.PI, weapon_factory: z[i][3], lifetime: scene.seconds_to_frames(1.125)})

						var z = [
							[-30, 0],
							[-30, 3],
							[-30, -3],
							[-35, 2],
							[-35, -2],
							[-35, 4],
							[-35, -4],
							[-40, 0],
							[-40, 3],
							[-40, 5],
							[-40, -3],
							[-40, -5],
							]
						for(var i = 0; i < z.length; ++i) drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + z[i][0], ship.position.y + z[i][1]), angle: Math.PI / 2, weapon_factory: laser_gun_factory})
					}
				}
			}
			State
			{
				id: state_search_3
				SignalTransition
				{
					targetState: state_before_checkpoint_4
					signal: checkpoint.value_changed
					guard: checkpoint.value
					onTriggered:
					{
						var z = [
							[10, 10, -1/4, laser_gun_factory],
							[-10, 10, 1/4, laser_gun_factory],
							[10, -10, -3/4, laser_gun_factory],
							[-10, -10, 3/4, laser_gun_factory],
							[-5, -20, 1, missile_launcher_factory],
							[5, -20, 1, missile_launcher_factory],
							[-5, 20, 0, missile_launcher_factory],
							[5, 20, 0, missile_launcher_factory],
							[20, 5, -1/2, missile_launcher_factory],
							[20, -5, -1/2, missile_launcher_factory],
							[-20, 5, 1/2, missile_launcher_factory],
							[-20, -5, 1/2, missile_launcher_factory],
							]
						for(var i = 0; i < z.length; ++i) turret_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + z[i][0], ship.position.y + z[i][1]), angle: z[i][2] * Math.PI, weapon_factory: z[i][3], lifetime: scene.seconds_to_frames(1.125)})
					}
				}
			}
			State
			{
				id: state_search_4
				SignalTransition
				{
					targetState: state_before_checkpoint_5
					signal: checkpoint.value_changed
					guard: checkpoint.value
					onTriggered:
					{
						var z = [
							[5, -20, 1, laser_gun_factory],
							[-5, -20, 1, laser_gun_factory],
							[0, -20, 1, missile_launcher_factory],
							[-5, 20, 0, laser_gun_factory],
							[5, 20, 0, laser_gun_factory],
							[0, 20, 0, missile_launcher_factory],
							[-20, 5, 1/2, laser_gun_factory],
							[-20, -5, 1/2, laser_gun_factory],
							[-20, 0, 1/2, missile_launcher_factory],
							[20, -5, -1/2, laser_gun_factory],
							[20, 5, -1/2, laser_gun_factory],
							[20, 0, -1/2, missile_launcher_factory],
							]
						for(var i = 0; i < z.length; ++i) turret_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + z[i][0], ship.position.y + z[i][1]), angle: z[i][2] * Math.PI, weapon_factory: z[i][3], lifetime: scene.seconds_to_frames(1.125)})

						var z = [
							[-20, -20, 1/2],
							[-20, -25, 1/2],
							[-25, -20, 1/2],
							[-20, 25, 1/2],
							[-25, 20, 1/2],
							[-20, 20, 1/2],
							[25, -20, -1/2],
							[20, -20, -1/2],
							[20, -25, -1/2],
							[25, 20, -1/2],
							[20, 25, -1/2],
							[20, 20, -1/2],
							]
						for(var i = 0; i < z.length; ++i) drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + z[i][0], ship.position.y + z[i][1]), angle: z[i][2] * Math.PI, weapon_factory: laser_gun_factory})
					}
				}
			}
			State
			{
				id: state_search_5
				SignalTransition
				{
					targetState: state_after_video
					signal: checkpoint.value_changed
					guard: checkpoint.value
					onTriggered:
					{
						screen_video.video.file = ":/data/game/chapter-2/video_boss_mercure.ogg"
						function f ()
						{
							music.objectName = "chapter-2/boss"
							base_factory.createObject(null, {scene: scene, position: Qt.point(boss_position_x, boss_position_y)})
							elevator_state = 1
							screen_video.video.file_changed.disconnect(f)
						}
						screen_video.video.file_changed.connect(f)
					}
				}
			}
			State
			{
				id: state_after_video;
				SignalTransition
				{
					targetState:state_after_checkpoint_5
					signal: state_after_video.propertiesAssigned
					guard:!screen_video.video.file
				}
			}
		}

		//checkpoint 1
		State
		{
			id: state_before_checkpoint_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_message_checkpoint_2
				signal: root.enemy_countChanged
				guard: !root.enemy_count
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("story 2 1"))
					messages.add("nectaire/normal", qsTr("story 2 2"))
					checkpoint.position = next_checkpoint_position()
				}
			}
		}
		State {id: state_message_checkpoint_2; SignalTransition {targetState: state_search_2; signal: state_message_checkpoint_2.propertiesAssigned; guard: !messages.has_unread}}

		//checkpoint 2
		State
		{
			id: state_before_checkpoint_3
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_message_checkpoint_3
				signal: root.enemy_countChanged
				guard: !root.enemy_count
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("story 3 1"))
					messages.add("nectaire/normal", qsTr("story 3 2"))
					messages.add("lycop/normal", qsTr("story 3 3"))
					messages.add("nectaire/normal", qsTr("story 3 4"))
					checkpoint.position = next_checkpoint_position()
				}
			}
		}
		State {id: state_message_checkpoint_3; SignalTransition {targetState: state_search_3; signal: state_message_checkpoint_3.propertiesAssigned; guard: !messages.has_unread}}

		//checkpoint 3
		State
		{
			id: state_before_checkpoint_4
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_message_checkpoint_4
				signal: root.enemy_countChanged
				guard: !root.enemy_count
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("story 4 1"))
					messages.add("nectaire/normal", qsTr("story 4 2"))
					messages.add("lycop/normal", qsTr("story 4 3"))
					messages.add("nectaire/normal", qsTr("story 4 4"))
					messages.add("lycop/normal", qsTr("story 4 5"))
					checkpoint.position = next_checkpoint_position()
				}
			}
		}
		State {id: state_message_checkpoint_4; SignalTransition {targetState: state_search_4; signal: state_message_checkpoint_4.propertiesAssigned; guard: !messages.has_unread}}

		//checkpoint 4
		State
		{
			id: state_before_checkpoint_5
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_message_checkpoint_5
				signal: root.enemy_countChanged
				guard: !root.enemy_count
				onTriggered:
				{
					if(!save_boss && mod_easy)
					{
						saved_game.set("save_boss", true)
						scene_loader.save()
					}
					messages.add("lycop/angry", qsTr("story 5 1"))
					messages.add("nectaire/panic", qsTr("story 5 2"))
					messages.add("lycop/normal", qsTr("story 5 3"))
					messages.add("nectaire/normal", qsTr("story 5 4"))
					messages.add("lycop/normal", qsTr("story 5 5"))
					messages.add("nectaire/normal", qsTr("story 5 6"))
					messages.add("lycop/normal", qsTr("story 5 7"))
					messages.add("nectaire/normal", qsTr("story 5 8"))
					messages.add("lycop/normal", qsTr("story 5 9"))
					saved_game.add_science(1)
					var z = [-24, -32, -40, -48, -56, 24, 32, 40, 48, 56]
					boss_position_x= ship.position.x
					boss_position_y = ship.position.y - 147
					for(var i = 0; i < z.length; ++i) cliff_turret_factory.createObject(null, {scene: scene, position: Qt.point(boss_position_x + z[i], boss_position_y - 18)})
					wall_factory.createObject(null, {scene: scene})
					rock_factory.createObject(null, {scene: scene})
					elevator_factory.createObject(null, {scene: scene, position: Qt.point(boss_position_x, boss_position_y)})
					checkpoint_collider.radius = 35
					checkpoint.position = Qt.point(boss_position_x, boss_position_y - 8)
				}
			}
		}
		State {id: state_message_checkpoint_5; SignalTransition {targetState: state_search_5; signal: state_message_checkpoint_5.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_after_checkpoint_5
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_clean_screen
				signal: one_boss_destroyed
				onTriggered:
				{
					boss_position_y= boss_position_y - 5
					root.base = false
					elevator_state = 2
					clear_screen_animation.createObject(null, {parent: scene, position: Qt.point(boss_position_x, boss_position_y)})
				}
			}
		}

		State
		{
			id: state_clean_screen
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_message_end
				signal: screen_cleared
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("story 6 1"))
					messages.add("lycop/normal", qsTr("story 6 2"))
					saved_game.add_matter(4000)
					saved_game.add_science(2)
					ship_angle = ship.calcul_ship_angle()
					ship.angle = Qt.binding(ship.face_ship_to_base)
					current_position_x = ship.position.x
					current_position_y = ship.position.y
					ship.position = Qt.binding(ship.move_ship_to_base)
					black_factory.createObject(null, {scene: scene})
				}
			}
		}
		State
		{
			id: state_message_end
			SignalTransition
			{
				targetState: state_move_in_base
				signal: state_message_end.propertiesAssigned
				guard: !messages.has_unread
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 500}
				PropertyAnimation {targetObject: root; propertyName: "t1"; duration: 2000}
			}
		}
		State
		{
			id: state_move_in_base
			AssignProperty {target: root; property: "t0"; value: 1}
			AssignProperty {target: root; property: "t1"; value: 1}
			SignalTransition
			{
				targetState: state_enter_in_base
				signal: state_move_in_base.propertiesAssigned
				PropertyAnimation {targetObject: root; propertyName: "t2"; duration: 2000}
				PropertyAnimation {targetObject: root; propertyName: "t3"; duration: 3000}
				onTriggered:
				{
					ship.position = Qt.point(boss_position_x , boss_position_y)
					current_position_x = ship.position.x
					current_position_y = ship.position.y
					boss_position_y= ship.position.y - 10
					ship.position = Qt.binding(ship.enter_ship_in_base)
					ship_angle = 0
				}
			}
		}
		State
		{
			id: state_enter_in_base
			AssignProperty {target: root; property: "t2"; value: 10}
			AssignProperty {target: root; property: "t3"; value: 2}
			SignalTransition
			{
				targetState: state_end
				signal: state_enter_in_base.propertiesAssigned
				onTriggered:
				{
					saved_game.set("file", "game/chapter-2/chapter-2b.qml")
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["chapter-2/surface"])
		jobs.run(scene_view, "preload", ["chapter-2/surface/cliff"])
		jobs.run(scene_view, "preload", ["chapter-2/surface/entry-boss"])
		jobs.run(scene_view, "preload", ["chapter-2/surface/entry-closed"])
		jobs.run(scene_view, "preload", ["chapter-2/surface/entry-open"])
		jobs.run(scene_view, "preload", ["chapter-2/surface/rocks"])
		jobs.run(scene_view, "preload", ["chapter-2/round-turret"])
		jobs.run(scene_view, "preload", ["chapter-2/boss-1"])
		jobs.run(scene_view, "preload", ["chapter-2/rectangle_noir"])
		jobs.run(scene_view, "preload", ["folks/heter/1"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		for(var i = 0; i < 15; ++i) jobs.run(scene_view, "preload", ["chapter-2/turret-opening/" + i])
		jobs.run(music, "preload", ["chapter-2/main"])
		jobs.run(music, "preload", ["chapter-2/boss"])
		groups.init(scene)
	}
}
