import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"

Chapter
{
	id: root
	property var hideleo
	property alias ship: ship
	property real ship_x
	property real ship_y
	property alias armagedon: armagedon
	property alias floor_opening: floor_opening
	property var camera_position: Qt.point(0, 0)
	property bool boom_anim_fin: false
	signal hideleo_run
	signal camera_finish
	property real t0: 0
	title: qsTr("title")
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	function next_boom_position()
	{
		var a = 2 * Math.PI * Math.random()
		var b = Math.random() * 15
		return Qt.point(b * Math.cos(a), b * Math.sin(a))
	}

	Q.Component
	{
		id: boom_factory
		Explosion
		{
			scale: 8
			material: "explosion/" + (Math.floor(scene.time/2) % 8)
		}
	}

	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	function sign(x) {return x < 0 ? -1 : 1}
	Q.Component
	{
		id: flying_rock_factory
		Bullet
		{
			id: flying_rock
			property real var_x
			property int img
			angle: root.sign(var_x) * (Math.PI * 3 / 4 + Math.random() * (Math.PI / 4 - Math.PI / 120))
			Q.Component.onCompleted: velocity = direction_to_scene(Qt.point(0, -25 - Math.random() * 10))
			range: scene.seconds_to_frames(15)
			damages: 1
			angular_velocity: Math.cos(Math.random()* 2 * Math.PI) * 5
			Image
			{
				id: image
				material: ["chapter-2/surface/stone/1","chapter-2/surface/stone/2","chapter-2/surface/stone/3","chapter-2/surface/stone/4"][flying_rock.img]
			}
		}
	}

	Q.Component
	{
		id: pit_factory
		Body
		{
			id: pit
			property real z: -1
			scale: 10
			Image
			{
				id: image
				material: "chapter-2/escape-hideleo/hole"
				z: pit.z
			}
		}
	}
	Q.Component
	{
		id: hideleo_factory
		Vehicle
		{
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			id: hideleo
			property real z: -1
			scale: 0.001
			angle: Math.PI
			property color mask: Qt.rgba(0, 0, 0, 1)
			Image
			{
				id: image
				material: "chapter-3/hideleo"
				z: hideleo.z
				mask: hideleo.mask
			}
			EquipmentSlot
			{
				position: coords(460, 761)
				scale: 0.15/2
				angle: Math.PI * -7/8
				equipment: TripleGun
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(564, 761)
				scale: 0.15/2
				angle: Math.PI * 7/8
				equipment: TripleGun
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(512, 806)
				scale: 0.15/2
				angle: 0
				equipment: ActiveEquipment
				{
					id: equipment
					max_health: 200
					image: Image
					{
						material: "equipments/secondary-shield-generator"
						scale: 0.5
					}
				}
			}
			EquipmentSlot
			{
				position: coords(460, 867)
				scale: 0.15/2
				angle: Math.PI
				equipment: HeavyLaserGun
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(564, 867)
				scale: 0.15/2
				angle: Math.PI
				equipment: HeavyLaserGun
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(512, 903)
				scale: 0.15/2
				angle: Math.PI
				equipment: Quipoutre
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(407, 585)
				scale: 0.15/2
				angle: Math.PI
				equipment: VortexGun
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(617, 585)
				scale: 0.15/2
				angle: Math.PI
				equipment: VortexGun
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(460, 585)
				scale: 0.15/2
				angle: Math.PI * -7/8
				equipment: LaserGun
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(564, 585)
				scale: 0.15/2
				angle: Math.PI * 7/8
				equipment: LaserGun
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(407, 418)
				scale: 0.15/2
				angle: Math.PI
				equipment: MissileLauncher
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(617, 418)
				scale: 0.15/2
				angle: Math.PI
				equipment: MissileLauncher
				{
					shooting: false
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(512, 330)
				scale: 0.15/2
				angle: 0
				equipment: Teleporter
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(407, 295)
				scale: 0.15/2
				angle: 0
				equipment: Quipoutre
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(617, 295)
				scale: 0.15/2
				angle: 0
				equipment: Quipoutre
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(464, 250)
				scale: 0.15/2
				angle: 0
				equipment: LaserGun
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(560, 250)
				scale: 0.15/2
				angle: 0
				equipment: LaserGun
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(476, 40)
				scale: 0.15/2
				angle: 0
				equipment: PlasmaSpray
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(558, 40)
				scale: 0.15/2
				angle: 0
				equipment: PlasmaSpray
				{
					level: 2
				}
			}
		}
	}
	Q.Component
	{
		id: gigaboom_factory
		Animation
		{
			id: timer
			property bool first_time: true
			property var boom
			property var flying_stone
			property real last_time: 7
			property color mask: "orange"
			time: 7
			speed: -1
			onTimeChanged:
			{
				ship.position.y += 0.2
				if(time > -0.7 )
				{
					if(time<last_time-0.15)
					{
						boom.createObject(null, {scene: scene, position: root.next_boom_position()})
						last_time = time
					}
				}
				if(time < -1)
				{
					if(first_time)
					{
						armagedon.position= Qt.point( ship.position.x ,ship.position.y - 1)
						armagedon.scale = 1
						armagedon.mask_c= timer.mask
						armagedon.activate= true
						first_time= false
					}
					if(time > -2.586 )
					{
						armagedon.scale_img = 130 * -(time + 1) / 1.586
					}
					else
					{
						if(time > -4.694)
						{
							armagedon.mask_c.g = 0.5 + 0.5 * (- time - 2.586) / (3.694-1.586)
							armagedon.mask_c.b = 1 * (- time - 2.586) / (3.694-1.586)
						}
						else
						{
							if(time > -6.262)
							{
								armagedon.mask_c.a = 1 - (-time -4.694) / (5.282-3.694)
							}
						}
					}
					if(time < -1 && time > -5.262 && time < last_time - 0.01 )
					{
						var local_var_x = Math.cos(Math.random()* 2 * Math.PI) * 2
						var local_var_x_2 = Math.cos(Math.random()* 2 * Math.PI) * 2
						flying_stone.createObject(null, {scene: scene, position: Qt.point(ship.position.x + local_var_x ,ship.position.y - 25), scale: 0.30 + Math.random(), var_x: local_var_x, img: Math.floor(random_index(4))})
						flying_stone.createObject(null, {scene: scene, position: Qt.point(ship.position.x + local_var_x_2 ,ship.position.y - 25), scale: 0.30 + Math.random(), var_x: local_var_x_2, img: Math.floor(random_index(4))})
						last_time = time
					}
					if(time < -11.5) //-6.262)
					{
						armagedon.suicide= true
						speed = 0
						root.boom_anim_fin = true
					}
				}
			}
		}
	}
	Q.Component
	{
		id: earthquake_factory
		Body
		{
			Q.Component.onCompleted: sound_earthquake.play()
			Sound
			{
				id: sound_earthquake
				objectName: "chapter-2/earthquake"
				scale: 20
				looping: true
			}
		}
	}
	Q.Component
	{
		id: hideleo_animation_factory
		Animation
		{
			id: timer
			property bool swap: true
			property bool first: true
			property bool suicide: false
			property bool hideleo_build: false
			property bool earthquake: true
			property real hideleo_vitesse: 0
			property var earthquake_sound
			property var hideleo_ship
			onSuicideChanged: if(suicide) deleteLater()
			time: 7
			property real last_time: 6
			speed: -1

			onEarthquakeChanged: if(!earthquake) earthquake_sound.deleteLater()

			onTimeChanged:
			{
				if(earthquake)
				{
					var camera_x = root.camera_position.x
					var camera_y = root.camera_position.y
					camera_x += swap ? -0.1 : 0.1
					camera_y += swap ? -0.1 : 0.1
					swap= !swap
					root.camera_position = Qt.point(camera_x, camera_y)
				}
				if(last_time>time)
				{
					if(first)
					{
						earthquake_sound = earthquake_factory.createObject(null, {parent: scene, position: root.hideleo})
						pit_factory.createObject(null, {parent: scene, position : root.hideleo, z: altitudes.background_near + 0.000001 })
						floor_opening.z= altitudes.boss + 0.000002
						first= false
					}
					if(floor_opening.img<floor_opening.length)
					{
						floor_opening.img++
						last_time=last_time-0.05
					}
					else
					{
						last_time = -1235
					}
				}

				if(floor_opening.img == floor_opening.length)
				{
					if(floor_opening.position.x - root.hideleo.x < 16.66)
					{
						floor_opening.position.x += 0.05
					}
					else
					{
						floor_opening.z= altitudes.background_near + 0.000002
						earthquake = false
					}
					if(floor_opening.position.x - root.hideleo.x > 13)
					{
						if(!hideleo_build)
						{
							hideleo_ship=hideleo_factory.createObject(null, {parent: scene, position : Qt.point(root.hideleo.x,root.hideleo.y - 1.8), z: altitudes.boss, angle: Math.PI/2 })
							hideleo_build = true
						}
						else
						{
							var hideleo_scale = 12
							var hideleo_step = 0.1
							var angle_step = 0.005
							var vitesse_plus = 0.005
							if(hideleo_ship.scale < hideleo_scale)
							{
								if(hideleo_ship.scale < hideleo_scale - 4)
								{
									var mask_cour = Math.min(1, hideleo_ship.scale / (hideleo_scale - 4))
									hideleo_ship.mask.r = mask_cour
									hideleo_ship.mask.g = mask_cour
									hideleo_ship.mask.b = mask_cour
								}
								if(hideleo_ship.scale > hideleo_scale - 7 && hideleo_ship.scale < hideleo_scale - 2)
								{
									hideleo_ship.angle += angle_step
								}
								if(hideleo_ship.scale > hideleo_scale - 2)
								{
									hideleo_vitesse += vitesse_plus
									hideleo_ship.angle += angle_step
									hideleo_ship.position.y += Math.cos(hideleo_ship.angle) * hideleo_vitesse * hideleo_vitesse
									hideleo_ship.position.x -= Math.sin(hideleo_ship.angle) * hideleo_vitesse * hideleo_vitesse
								}
								hideleo_ship.scale += hideleo_step
								hideleo_ship.position.y += 1.8 / hideleo_scale * hideleo_step
							}
							else
							{
								if((hideleo_ship.position.y - root.hideleo.y) * (hideleo_ship.position.y - root.hideleo.y) + (hideleo_ship.position.x - root.hideleo.x) * (hideleo_ship.position.x - root.hideleo.x) < 1600)
								{
									hideleo_vitesse += vitesse_plus
									hideleo_ship.angle += angle_step
									hideleo_ship.position.y += Math.cos(hideleo_ship.angle) * hideleo_vitesse * hideleo_vitesse
									hideleo_ship.position.x -= Math.sin(hideleo_ship.angle) * hideleo_vitesse * hideleo_vitesse
								}
								else
								{
									root.hideleo_run()
									suicide = true
								}
							}
						}
					}
				}
			}
		}
	}
	Q.Component
	{
		id: move_camera_to_hideleo
		Animation
		{
			id: timer
			property var hideleo_position
			property real chemin_x
			property real chemin_y
			property real vitesse_max: 1
			property real vitesse_min: 0.1
			time: 7
			speed: -1
			onTimeChanged:
			{
				var vitesse_max_x = vitesse_max
				var vitesse_max_y = vitesse_max * Math.abs(chemin_y / chemin_x)
				var vitesse_min_x = vitesse_min
				var vitesse_min_y = vitesse_min * Math.abs(chemin_y / chemin_x)
				var vitesse_x = vitesse_max_x
				var vitesse_y = vitesse_max_y
				var position_x = hideleo_position.x - root.camera_position.x
				var position_y = hideleo_position.y - root.camera_position.y

				if(Math.abs(position_x) > Math.abs(chemin_x * 9 / 10))
				{
					vitesse_x = vitesse_min_x + (vitesse_max_x - vitesse_min_x) * (1 - (Math.abs(position_x - chemin_x * 9 / 10) / (Math.abs(chemin_x) / 10)))
				}
				if(Math.abs(position_y) > Math.abs(chemin_y * 9 / 10))
				{
					vitesse_y = vitesse_min_y + (vitesse_max_y - vitesse_min_y) * (1 - Math.abs(position_y - chemin_y * 9 / 10) / Math.abs(chemin_y/10))
				}

				if(Math.abs(position_x) < Math.abs(chemin_x / 10))
				{
					vitesse_x = vitesse_min_x + (vitesse_max_x - vitesse_min_x) * (1 - Math.abs(position_x - chemin_x / 10) / Math.abs(chemin_x / 10))
				}
				if(Math.abs(position_y) < Math.abs(chemin_y / 10))
				{
					vitesse_y = vitesse_min_y + (vitesse_max_y - vitesse_min_y) * (1 - Math.abs(position_y - chemin_y / 10) / Math.abs(chemin_y / 10))
				}
				if(Math.abs(position_x) < 0.02)
				{
					vitesse_x = 0
				}
				if(Math.abs(position_y) < 0.02)
				{
					vitesse_y = 0
				}

				root.camera_position = Qt.point(root.camera_position.x + vitesse_x * (position_x > 0 ? 1 : -1), root.camera_position.y + vitesse_y * (position_y > 0 ? 1 : -1))

				if(vitesse_x == 0 && vitesse_y ==0 )
				{
					timer.deleteLater()
					root.camera_finish()
				}
			}
		}
	}
	scene: Scene
	{
		running: false

		Vehicle
		{
			id: clear_screen
			scale: 30
			type: Body.KINEMATIC
			position: ship.position
			CircleCollider
			{
				group: groups.clear_screen
			}
		}

		Body
		{
			id: floor_opening
			property int img: 0
			property int length: 4
			scale: 10
			property real z: -1
			Image
			{
				id: image_f
				material: ["chapter-2/escape-hideleo/door-1","chapter-2/escape-hideleo/door-2","chapter-2/escape-hideleo/door-3","chapter-2/escape-hideleo/door-4","chapter-2/escape-hideleo/door-5"][floor_opening.img]
				z: floor_opening.z
			}
		}

		Body
		{
			id: armagedon
			type: Body.STATIC
			property color mask_c: "transparent"
			property bool activate: false
			property bool suicide: false
			property real scale_img: 1
			onActivateChanged: sound.play()
			onSuicideChanged: if(suicide) destroy()
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				scale: armagedon.scale_img
				position.y: - 84
			}
			Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 30
			}
		}

		Background
		{
			z: altitudes.background_near
			scale: 10
			material: "chapter-2/surface"
		}
		Ship
		{
			id: ship
			angle: Math.PI
			type: Body.STATIC
		}
		Background
		{
			y: 1
			z: -0.0002
			scale: 20*1.29
			position.y: 2 * scale
			material: "chapter-2/surface/rocks"
		}
		Background
		{
			y: 1
			z: -0.0001
			scale: 20*1.29
			position.y: 0
			material: "chapter-2/surface/cliff"
		}
		Body
		{
			Image
			{
				z: -0.00005
				scale: 20*1.29
				material: "chapter-2/surface/entry-open"
			}
		}
	}
	StateMachine
	{
		begin: state_story_0
		active: game_running
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_boom
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					music.objectName = "chapter-2/fuite"
					camera_position = undefined
				}
			}
		}
		State
		{
			id: state_boom
			SignalTransition
			{
				targetState: state_cur_animation
				signal: state_boom.propertiesAssigned
				onTriggered:
				{
					gigaboom_factory.createObject(null, {parent: scene, boom: boom_factory, flying_stone: flying_rock_factory})
				}
			}
		}
		State
		{
			id: state_cur_animation
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_end_animation_message
				signal: root.boom_anim_finChanged
				guard: root.boom_anim_fin
				onTriggered:
				{
					music.objectName = "survival/main"
					messages.add("nectaire/normal", qsTr("story 1 1"))
					messages.add("lycop/happy", qsTr("story 1 2"))
					camera_position = Qt.point(ship.position.x,ship.position.y)
				}
			}
		}
		State {id: state_end_animation_message; SignalTransition {targetState: state_hideleo; signal: state_end_animation_message.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_hideleo
			SignalTransition
			{
				targetState: state_hideleo_animation
				signal: state_hideleo.propertiesAssigned
				onTriggered:
				{
					root.hideleo = Qt.point(Math.floor((ship.position.x)/20)*20 - 10 - 80, Math.floor((ship.position.y)/20) * 20 - 10 - 80)
					move_camera_to_hideleo.createObject(null, {parent: scene, hideleo_position : root.hideleo, chemin_x: root.hideleo.x - root.camera_position.x, chemin_y: root.hideleo.y - root.camera_position.y})
				}
			}
		}
		State
		{
			id: state_hideleo_animation
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_camera_return_ship
				signal: camera_finish
				onTriggered:
				{
					floor_opening.position = root.hideleo
					hideleo_animation_factory.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_camera_return_ship
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_hideleo_dialogue
				signal: hideleo_run
				onTriggered:
				{
					move_camera_to_hideleo.createObject(null, {parent: scene, hideleo_position : ship.position, chemin_x: ship.position.x - root.camera_position.x, chemin_y: ship.position.y - root.camera_position.y})
				}
			}
		}
		State
		{
			id: state_hideleo_dialogue
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_hideleo_dialogue_fin
				signal: camera_finish
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("story 1 3"))
					messages.add("lycop/surprised", qsTr("story 1 4"))
					messages.add("nectaire/normal", qsTr("story 1 5"))
					messages.add("lycop/neutral", qsTr("story 1 6"))
					messages.add("lycop/normal", qsTr("story 1 7"))
					messages.add("nectaire/panic", qsTr("story 1 8"))
					messages.add("lycop/normal", qsTr("story 1 9"))
				}
			}
		}
		State {id: state_hideleo_dialogue_fin; SignalTransition {targetState: state_after_run; signal: state_hideleo_dialogue_fin.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_after_run
			SignalTransition
			{
				targetState: state_end
				signal: state_after_run.propertiesAssigned
				onTriggered:
				{
					saved_game.set("file", global.version() < 0 ? "game/chapter-2/chapter-2z.qml" : "game/chapter-3/chapter-3.qml")
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	Q.Component.onCompleted:
	{
		for(var i1 = 1; i1 < 5; ++i1) jobs.run(scene_view, "preload", ["chapter-2/surface/stone/" + i1])
		jobs.run(scene_view, "preload", ["chapter-2/escape-hideleo/hole"])
		for(var i2 = 1; i2 < 6; ++i2) jobs.run(scene_view, "preload", ["chapter-2/escape-hideleo/door-" + i2])
		jobs.run(scene_view, "preload", ["chapter-2/surface"])
		jobs.run(scene_view, "preload", ["chapter-2/surface/cliff"])
		jobs.run(scene_view, "preload", ["chapter-2/surface/entry-open"])
		jobs.run(scene_view, "preload", ["chapter-2/surface/rocks"])
		jobs.run(scene_view, "preload", ["chapter-3/hideleo"])
		for(var i = 1; i < 8; ++i) jobs.run(scene_view, "preload", ["explosion/" + i])
		jobs.run(music, "preload", ["survival/main"])
		jobs.run(music, "preload", ["chapter-2/fuite"])
		jobs.run(music, "preload", ["explosion/sound_plus_gros"])
		jobs.run(music, "preload", ["chapter-2/earthquake"])
		jobs.run(music, "preload", ["explosion/sound"])

		groups.init(scene)
	}
}
