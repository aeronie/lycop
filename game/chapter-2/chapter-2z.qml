import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import QtQuick.Controls 1.0 as Q
import QtQuick.Layouts 1.0 as Q
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q
import "qrc:///controls/styles" as S

Chapter
{
	Q.Component
	{
		id: splash_factory_demo
		Splash
		{
			Q.Image
			{
				id: splash
				source: "///data/game/title/title-bg.png"
				fillMode: Q.Image.PreserveAspectCrop
				Q.ColumnLayout
				{
					anchors.fill: parent
					anchors.margins: 20
					Q.Label
					{
						id: text
						Q.Layout.fillWidth: true
						text: qsTr("demo")
						wrapMode: Q.Text.Wrap
						horizontalAlignment: Q.Text.AlignHCenter
					}
					Q.Buttons
					{
						Q.Layout.alignment: Qt.AlignCenter
						model: [action_vote, action_continue]
						flow: Q.GridLayout.TopToBottom
					}
					Q.Action
					{
						id: action_vote
						text: qsTr("vote")
						onTriggered: Qt.openUrlExternally("http://store.steampowered.com/app/525070")
					}
					Q.Action
					{
						id: action_continue
						text: qsTr("continue")
						onTriggered: splash.enabled = false
					}
				}
			}
		}
	}
	Q.Component
	{
		id: splash_factory_final
		Splash
		{
			Q.Image
			{
				z: 1000
				source: "///data/game/chapter-2/chapter-2.png"
				fillMode: Q.Image.PreserveAspectFit
				Q.Label
				{
					anchors.right: parent.right
					anchors.top: parent.top
					anchors.margins: 20
					text: root.title
					font.pointSize: 20
				}
			}
		}
	}
	id: root
	property alias ship: ship
	property var camera_position: ship.position
	title: qsTr("title")
	splash: global.version() < 0 ? splash_factory_demo : splash_factory_final
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	scene: Scene
	{
		running: false
		Background
		{
			z: -0.5
			scale: 10
			material: "vide"
		}
		Ship
		{
			id: ship
			Q.Component.onDestruction: game_over = true
		}
	}
	StateMachine
	{
		begin: state_begin
		active: game_running
		State
		{
			id: state_begin
			SignalTransition
			{
				targetState: state_end
				signal: state_begin.propertiesAssigned
				onTriggered:
				{
					music.objectName = "chapter-1/main"
				}
			}
		}
		State
		{
			id: state_end
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: screen_menu.about_screen_travel; property: "blink"; value: true}
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(music, "preload", ["chapter-1/main"])
		groups.init(scene)
	}
}
