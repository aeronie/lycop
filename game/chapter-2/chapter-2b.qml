import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-2/chapter-2b.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	property var clock: null
	property var tab_hidden_enemy: []
	property var animation_hidden_enemy
	property bool escape_ss: false
	property bool hidden_boss_reveal: false
	property bool sortie_on : false
	signal one_boss_destroyed
	property real temps_restant: 0
	title: qsTr("title")
	property real chapter_scale: 2
	property bool save_back : saved_game.self.get("save_back", false)
	property bool mod_easy : saved_game.self.get("mod_easy", false)
	property bool generator_room_4_destroyed: false
	property bool generator_room_7_destroyed: false
	property int open_corridor_1_2: 0
	property int open_corridor_1_3: 0
	property int open_corridor_1_4: 0
	property int open_corridor_1_5: 0
	property int open_corridor_2: 0
	property int open_corridor_3_1: 0
	property int open_corridor_3_2: 0
	property int open_corridor_4_1: 0
	property int open_corridor_4_2: 0
	property int open_corridor_4_3: 0
	property int open_corridor_4_4: 0
	property int open_corridor_4_5: 0
	property int open_corridor_4_6: 0
	property int open_room_1: 0
	property int open_room_2: 0
	property int open_room_3: 0
	property int open_room_4: 0
	property int open_room_5: 0
	property int open_room_6: 0
	property int open_room_7: 0
	property int open_room_boss: 0
	property real t0: 0
	property var walls: [
	[[14.5,10.5],[34.5,10.5]],
	[[19.5,10.5],[19.5,11.5]],
	[[14.5,10.5],[14.5,68.5]],
	[[19.5,14.5],[19.5,63.5]],
	[[14.5,34.5],[15.5,34.5]],
	[[18.5,34.5],[19.5,34.5]],
	[[14.5,50.5],[15.5,50.5]],
	[[18.5,50.5],[19.5,50.5]],
	[[14.5,68.5],[55.5,68.5]],
	[[55.5,68.5],[55.5,73.5]],
	[[55.5,73.5],[60.5,73.5]],
	[[60.5,51.5],[60.5,73.5]],
	[[52.5,63.5],[60.5,63.5]],
	[[53.5,63.5],[53.5,64.5]],
	[[53.5,67.5],[53.5,68.5]],
	[[48.5,63.5],[48.5,64.5]],
	[[48.5,67.5],[48.5,68.5]],
	[[19.5,63.5],[49.5,63.5]],
	[[34.5,62.5],[34.5,63.5]],
	[[34.5,53.5],[34.5,59.5]],
	[[24.5,58.5],[34.5,58.5]],
	[[24.5,45.5],[24.5,58.5]],
	[[24.5,45.5],[27.5,45.5]],
	[[31.5,45.5],[34.5,45.5]],
	[[34.5,8.5],[34.5,50.5]],
	[[19.5,40.5],[30.5,40.5]],
	[[33.5,40.5],[56.5,40.5]],
	[[59.5,40.5],[60.5,40.5]],
	[[60.5,35.5],[60.5,47.5]],
	[[39.5,35.5],[68.5,35.5]],
	[[39.5,13.5],[39.5,35.5]],
	[[39.5,13.5],[52.5,13.5]],
	[[55.5,13.5],[68.5,13.5]],
	[[68.5,13.5],[68.5,14.5]],
	[[68.5,18.5],[68.5,29.5]],
	[[68.5,33.5],[68.5,35.5]],
	[[34.5,8.5],[73.5,8.5]],
	[[73.5,0.5],[73.5,44.5]],
	[[71.5,40.5],[73.5,40.5]],
	[[65.5,40.5],[67.5,40.5]],
	[[65.5,40.5],[65.5,41.5]],
	[[65.5,44.5],[65.5,64.5]],
	[[65.5,67.5],[65.5,68.5]],
	[[60.5,68.5],[98.5,68.5]],
	[[98.5,64.5],[98.5,71.5]],
	[[98.5,71.5],[128.5,71.5]],
	[[128.5,0.5],[128.5,71.5]],
	[[126.5,48.5],[128.5,48.5]],
	[[77.5,48.5],[122.5,48.5]],
	[[98.5,48.5],[98.5,52.5]],
	[[65.5,48.5],[74.5,48.5]],
	[[73.5,47.5],[73.5,48.5]],
	[[73.5,43.5],[88.5,43.5]],
	[[92.5,43.5],[123.5,43.5]],
	[[116.5,43.5],[116.5,44.5]],
	[[116.5,47.5],[116.5,48.5]],
	[[123.5,5.5],[123.5,43.5]],
	[[122.5,20.5],[123.5,20.5]],
	[[118.5,20.5],[119.5,20.5]],
	[[123.5,8.5],[124.5,8.5]],
	[[127.5,8.5],[128.5,8.5]],
	[[73.5,0.5],[128.5,0.5]],
	[[78.5,5.5],[123.5,5.5]],
	[[101.5,0.5],[101.5,1.5]],
	[[101.5,4.5],[101.5,5.5]],
	[[78.5,5.5],[78.5,13.5]],
	[[78.5,17.5],[78.5,38.5]],
	[[73.5,27.5],[74.5,27.5]],
	[[77.5,27.5],[78.5,27.5]],
	[[78.5,38.5],[118.5,38.5]],
	[[118.5,10.5],[118.5,38.5]],
	[[83.5,10.5],[118.5,10.5]],
	[[83.5,10.5],[83.5,34.5]],
	[[83.5,37.5],[83.5,38.5]],
	/* Murs extérieurs: */
	[[8.5,68.5],[55.5,80.5]],
	[[55.5,73.5],[60.5,80.5]],
	[[60.5,68.5],[98.5,80.5]],
	[[98.5,71.5],[134.5,80.5]],
	[[128.5,-5.5],[134.5,71.5]],
	[[8.5,-5.5],[128.5,0.5]],
	[[8.5,0.5],[73.5,8.5]],
	[[8.5,8.5],[34.5,10.5]],
	[[8.5,10.5],[14.5,68.5]]
    ]

	//[scene.coords(),angle,weapon,scene.room]
	property var enemy_tab_corridor_1_3: [
	[scene.coords(46.5,67.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(42.5,67.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(33.5,67.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(27.5,67.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(16.5,67.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(46.5,65.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(42.5,65.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(33.5,65.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(27.5,65.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(16.5,64.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(18.5,64.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(18.5,66.5),1/2,0,root.open_corridor_1_3],
	[scene.coords(16.5,60.5),1,0,root.open_corridor_1_3],
	[scene.coords(16.5,55.5),1,0,root.open_corridor_1_3],
	[scene.coords(18.5,60.5),1,0,root.open_corridor_1_3],
	[scene.coords(18.5,55.5),1,0,root.open_corridor_1_3]]

	property var enemy_tab_corridor_1_4: [
	[scene.coords(16.5,48.5),1,0,root.open_corridor_1_4],
	[scene.coords(16.5,45.5),1,0,root.open_corridor_1_4],
	[scene.coords(16.5,42.5),1,0,root.open_corridor_1_4],
	[scene.coords(16.5,39.5),1,0,root.open_corridor_1_4],
	[scene.coords(18.5,48.5),1,0,root.open_corridor_1_4],
	[scene.coords(18.5,45.5),1,0,root.open_corridor_1_4],
	[scene.coords(18.5,42.5),1,0,root.open_corridor_1_4],
	[scene.coords(18.5,39.5),1,0,root.open_corridor_1_4]]

	property var enemy_tab_corridor_1_5: [
	[scene.coords(16.5,32.5),1,0,root.open_corridor_1_5],
	[scene.coords(16.5,26.5),1,0,root.open_corridor_1_5],
	[scene.coords(16.5,20.5),1,0,root.open_corridor_1_5],
	[scene.coords(16.5,12),1,0,root.open_corridor_1_5],
	[scene.coords(18.5,32.5),1,0,root.open_corridor_1_5],
	[scene.coords(18.5,26.5),1,0,root.open_corridor_1_5],
	[scene.coords(18.5,20.5),1,0,root.open_corridor_1_5]]

	property var enemy_tab_room_1: [
	[scene.coords(32.5,13.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,15.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,17.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,19.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,21.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,23.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,25.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,27.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,29.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,31.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,33.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,35.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,37.5),-1/2,0,root.open_room_1],
	[scene.coords(32.5,39.5),-1/4,0,root.open_room_1],
	[scene.coords(30.5,39.5),0,0,root.open_room_1],
	[scene.coords(28.5,39.5),0,0,root.open_room_1],
	[scene.coords(26.5,39.5),0,0,root.open_room_1],
	[scene.coords(24.5,39.5),0,0,root.open_room_1],
	[scene.coords(22.5,39.5),0,0,root.open_room_1],
	[scene.coords(31,14.5),-1/2,1,root.open_room_1],
	[scene.coords(31,16.5),-1/2,1,root.open_room_1],
	[scene.coords(31,18.5),-1/2,1,root.open_room_1],
	[scene.coords(31,20.5),-1/2,1,root.open_room_1],
	[scene.coords(31,22.5),-1/2,1,root.open_room_1],
	[scene.coords(31,24.5),-1/2,1,root.open_room_1],
	[scene.coords(31,26.5),-1/2,1,root.open_room_1],
	[scene.coords(31,28.5),-1/2,1,root.open_room_1],
	[scene.coords(31,30.5),-1/2,1,root.open_room_1],
	[scene.coords(31,32.5),-1/2,1,root.open_room_1],
	[scene.coords(31,34.5),-1/2,1,root.open_room_1],
	[scene.coords(31,36.5),-1/2,1,root.open_room_1],
	[scene.coords(31,38.5),-1/4,1,root.open_room_1],
	[scene.coords(29,38.5),1,1,root.open_room_1],
	[scene.coords(27,38.5),1,1,root.open_room_1],
	[scene.coords(25,38.5),1,1,root.open_room_1],
	[scene.coords(23,38.5),1,1,root.open_room_1]]

	property var enemy_tab_corridor_2: [
	[scene.coords(29.5,42.5),1/2,0,root.open_corridor_2],
	[scene.coords(23.5,42.5),1/2,0,root.open_corridor_2],
	[scene.coords(21.5,42.5),1/2,0,root.open_corridor_2],
	[scene.coords(29.5,44.5),1/2,0,root.open_corridor_2],
	[scene.coords(22.5,44.5),1/2,0,root.open_corridor_2],
	[scene.coords(22.5,43.5),1/2,0,root.open_corridor_2],
	[scene.coords(21.5,45.5),1/2,0,root.open_corridor_2],
	[scene.coords(23.5,45.5),1/2,0,root.open_corridor_2],
	[scene.coords(21.5,50.5),0,0,root.open_corridor_2],
	[scene.coords(23.5,50.5),0,0,root.open_corridor_2],
	[scene.coords(21.5,62.5),0,0,root.open_corridor_2],
	[scene.coords(23.5,60.5),0,0,root.open_corridor_2],
	[scene.coords(32.5,60.5),-1/2,0,root.open_corridor_2],
	[scene.coords(32.5,61.5),-1/2,0,root.open_corridor_2],
	[scene.coords(32.5,62.5),-1/2,0,root.open_corridor_2]]

	property var enemy_tab_room_2: [
	[scene.coords(38.5,44.5),1,0,root.open_room_2],
	[scene.coords(39.5,44.5),1,0,root.open_room_2],
	[scene.coords(42.5,44.5),1,0,root.open_room_2],
	[scene.coords(43.5,44.5),1,0,root.open_room_2],
	[scene.coords(38.5,45.5),1,0,root.open_room_2],
	[scene.coords(39.5,45.5),1,0,root.open_room_2],
	[scene.coords(42.5,45.5),1,0,root.open_room_2],
	[scene.coords(43.5,45.5),1,0,root.open_room_2],
	[scene.coords(39.5,47.5),1,0,root.open_room_2],
	[scene.coords(40.5,47.5),1,0,root.open_room_2],
	[scene.coords(41.5,47.5),1,0,root.open_room_2],
	[scene.coords(42.5,47.5),1,0,root.open_room_2],
	[scene.coords(39.5,48.5),1,0,root.open_room_2],
	[scene.coords(40.5,48.5),1,0,root.open_room_2],
	[scene.coords(41.5,48.5),1,0,root.open_room_2],
	[scene.coords(42.5,48.5),1,0,root.open_room_2],
	[scene.coords(50.5,44.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,44.5),-1/2,0,root.open_room_2],
	[scene.coords(50.5,45.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,45.5),-1/2,0,root.open_room_2],
	[scene.coords(50.5,47.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,47.5),-1/2,0,root.open_room_2],
	[scene.coords(50.5,48.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,48.5),-1/2,0,root.open_room_2],
	[scene.coords(50.5,50.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,50.5),-1/2,0,root.open_room_2],
	[scene.coords(50.5,51.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,51.5),-1/2,0,root.open_room_2],
	[scene.coords(50.5,53.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,53.5),-1/2,0,root.open_room_2],
	[scene.coords(50.5,54.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,54.5),-1/2,0,root.open_room_2],
	[scene.coords(50.5,56.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,56.5),-1/2,0,root.open_room_2],
	[scene.coords(50.5,57.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,57.5),-1/2,0,root.open_room_2],
	[scene.coords(50.5,59.5),-1/2,0,root.open_room_2],
	[scene.coords(51.5,59.5),-1/2,0,root.open_room_2],
	[scene.coords(53.5,43.5),-1/2,1,root.open_room_2],
	[scene.coords(53.5,45.5),-1/2,1,root.open_room_2],
	[scene.coords(53.5,47.5),-1/2,1,root.open_room_2],
	[scene.coords(53.5,49.5),-1/2,1,root.open_room_2],
	[scene.coords(53.5,51.5),-1/2,1,root.open_room_2],
	[scene.coords(53.5,53.5),-1/2,1,root.open_room_2],
	[scene.coords(53.5,55.5),-1/2,1,root.open_room_2],
	[scene.coords(53.5,57.5),-1/2,1,root.open_room_2],
	[scene.coords(53.5,59.5),-1/2,1,root.open_room_2]]

	property var enemy_tab_room_3: [
	[scene.coords(31,49.5),1/2,2,root.open_room_3],
	[scene.coords(31,50.5),1/2,2,root.open_room_3],
	[scene.coords(31,51.5),1/2,2,root.open_room_3],
	[scene.coords(31,52.5),1/2,2,root.open_room_3],
	[scene.coords(31,53.5),1/2,2,root.open_room_3],
	[scene.coords(31,54.5),1/2,2,root.open_room_3],
	[scene.coords(30,50),1/2,3,root.open_room_3],
	[scene.coords(30,51),1/2,3,root.open_room_3],
	[scene.coords(30,52),1/2,3,root.open_room_3],
	[scene.coords(30,53),1/2,3,root.open_room_3],
	[scene.coords(30,54),1/2,3,root.open_room_3]]

	property var enemy_tab_corridor_3_1: [
	[scene.coords(55.5,39.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(51.5,39.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(48.5,39.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(44.5,39.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(37.5,39.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(55.5,37.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(51.5,37.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(48.5,37.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(44.5,37.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(38.5,37.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(36.5,38.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(37.5,36.5),1/2,0,root.open_corridor_3_1],
	[scene.coords(38.5,31.5),1,0,root.open_corridor_3_1],
	[scene.coords(38.5,27.5),1,0,root.open_corridor_3_1],
	[scene.coords(38.5,23.5),1,0,root.open_corridor_3_1],
	[scene.coords(38.5,19.5),1,0,root.open_corridor_3_1],
	[scene.coords(38.5,15.5),1,0,root.open_corridor_3_1],
	[scene.coords(38.5,12.5),1,0,root.open_corridor_3_1],
	[scene.coords(36,31.5),1,0,root.open_corridor_3_1],
	[scene.coords(36,27.5),1,0,root.open_corridor_3_1],
	[scene.coords(36,23.5),1,0,root.open_corridor_3_1],
	[scene.coords(36,19.5),1,0,root.open_corridor_3_1],
	[scene.coords(36,15.5),1,0,root.open_corridor_3_1],
	[scene.coords(36.5,10.5),1,0,root.open_corridor_3_1],
	[scene.coords(43.5,10.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(48.5,10.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(58.5,10.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(62.5,10.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(66.5,10.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(72.5,10.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(43.5,12.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(48.5,12.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(58.5,12.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(62.5,12.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(66.5,12.5),-1/2,0,root.open_corridor_3_1],
	[scene.coords(70.5,12.5),-1/2,0,root.open_corridor_3_1]]

	property var enemy_tab_corridor_3_2: [
	[scene.coords(70,22.5),0,0,root.open_corridor_3_2],
	[scene.coords(70,26.5),0,0,root.open_corridor_3_2],
	[scene.coords(70,30.5),0,0,root.open_corridor_3_2],
	[scene.coords(70,34.5),0,0,root.open_corridor_3_2],
	[scene.coords(72.5,22.5),0,0,root.open_corridor_3_2],
	[scene.coords(72.5,26.5),0,0,root.open_corridor_3_2],
	[scene.coords(72.5,30.5),0,0,root.open_corridor_3_2],
	[scene.coords(72.5,34.5),0,0,root.open_corridor_3_2],
	[scene.coords(68.5,37.5),1/2,0,root.open_corridor_3_2],
	[scene.coords(69.5,37.5),1/2,0,root.open_corridor_3_2],
	[scene.coords(68.5,38.5),1/2,0,root.open_corridor_3_2],
	[scene.coords(69.5,38.5),1/2,0,root.open_corridor_3_2],
	[scene.coords(62.5,37.5),1/2,0,root.open_corridor_3_2],
	[scene.coords(63.5,37.5),1/2,0,root.open_corridor_3_2],
	[scene.coords(62.5,38.5),1/2,0,root.open_corridor_3_2],
	[scene.coords(63.5,38.5),1/2,0,root.open_corridor_3_2],
	[scene.coords(62,41.5),0,0,root.open_corridor_3_2],
	[scene.coords(62,45.5),0,0,root.open_corridor_3_2],
	[scene.coords(62,50.5),0,0,root.open_corridor_3_2],
	[scene.coords(62,56.5),0,0,root.open_corridor_3_2],
	[scene.coords(62,60.5),0,0,root.open_corridor_3_2],
	[scene.coords(64.5,41.5),0,0,root.open_corridor_3_2],
	[scene.coords(64.5,45.5),0,0,root.open_corridor_3_2],
	[scene.coords(64.5,50.5),0,0,root.open_corridor_3_2],
	[scene.coords(64.5,56.5),0,0,root.open_corridor_3_2],
	[scene.coords(64.5,60.5),0,0,root.open_corridor_3_2],
	[scene.coords(62.5,64),0,0,root.open_corridor_3_2],
	[scene.coords(63.5,66),0,0,root.open_corridor_3_2],
	[scene.coords(63.5,64),0,0,root.open_corridor_3_2],
	[scene.coords(62.5,66),0,0,root.open_corridor_3_2]]

	property var enemy_tab_room_4: [
	[scene.coords(44.5,21.5),0,0,root.open_room_4],
	[scene.coords(45.5,21.5),0,0,root.open_room_4],
	[scene.coords(46.5,21.5),0,0,root.open_room_4],
	[scene.coords(47.5,21.5),0,0,root.open_room_4],
	[scene.coords(52.5,21.5),0,0,root.open_room_4],
	[scene.coords(53.5,21.5),0,0,root.open_room_4],
	[scene.coords(54.5,21.5),0,0,root.open_room_4],
	[scene.coords(55.5,21.5),0,0,root.open_room_4],
	[scene.coords(60.5,21.5),0,0,root.open_room_4],
	[scene.coords(61.5,21.5),0,0,root.open_room_4],
	[scene.coords(62.5,21.5),0,0,root.open_room_4],
	[scene.coords(63.5,21.5),0,0,root.open_room_4],
	[scene.coords(44.5,22.5),0,0,root.open_room_4],
	[scene.coords(45.5,22.5),0,0,root.open_room_4],
	[scene.coords(46.5,22.5),0,0,root.open_room_4],
	[scene.coords(47.5,22.5),0,0,root.open_room_4],
	[scene.coords(52.5,22.5),0,0,root.open_room_4],
	[scene.coords(53.5,22.5),0,0,root.open_room_4],
	[scene.coords(54.5,22.5),0,0,root.open_room_4],
	[scene.coords(55.5,22.5),0,0,root.open_room_4],
	[scene.coords(60.5,22.5),0,0,root.open_room_4],
	[scene.coords(61.5,22.5),0,0,root.open_room_4],
	[scene.coords(62.5,22.5),0,0,root.open_room_4],
	[scene.coords(63.5,22.5),0,0,root.open_room_4],
	[scene.coords(44.5,26.5),0,0,root.open_room_4],
	[scene.coords(45.5,26.5),0,0,root.open_room_4],
	[scene.coords(46.5,26.5),0,0,root.open_room_4],
	[scene.coords(47.5,26.5),0,0,root.open_room_4],
	[scene.coords(52.5,26.5),0,0,root.open_room_4],
	[scene.coords(53.5,26.5),0,0,root.open_room_4],
	[scene.coords(54.5,26.5),0,0,root.open_room_4],
	[scene.coords(55.5,26.5),0,0,root.open_room_4],
	[scene.coords(60.5,26.5),0,0,root.open_room_4],
	[scene.coords(61.5,26.5),0,0,root.open_room_4],
	[scene.coords(62.5,26.5),0,0,root.open_room_4],
	[scene.coords(63.5,26.5),0,0,root.open_room_4],
	[scene.coords(44.5,27.5),0,0,root.open_room_4],
	[scene.coords(45.5,27.5),0,0,root.open_room_4],
	[scene.coords(46.5,27.5),0,0,root.open_room_4],
	[scene.coords(47.5,27.5),0,0,root.open_room_4],
	[scene.coords(52.5,27.5),0,0,root.open_room_4],
	[scene.coords(53.5,27.5),0,0,root.open_room_4],
	[scene.coords(54.5,27.5),0,0,root.open_room_4],
	[scene.coords(55.5,27.5),0,0,root.open_room_4],
	[scene.coords(60.5,27.5),0,0,root.open_room_4],
	[scene.coords(61.5,27.5),0,0,root.open_room_4],
	[scene.coords(62.5,27.5),0,0,root.open_room_4],
	[scene.coords(63.5,27.5),0,0,root.open_room_4],
	[scene.coords(50.5,22.5),0,1,root.open_room_4],
	[scene.coords(57.5,22.5),0,1,root.open_room_4],
	[scene.coords(57.5,27.5),0,1,root.open_room_4],
	[scene.coords(50.5,27.5),0,1,root.open_room_4],
	[scene.coords(45.5,15.5),1/2,1,root.open_room_4],
	[scene.coords(45.5,17.5),1/2,1,root.open_room_4],
	[scene.coords(45.5,19.5),1/2,1,root.open_room_4],
	[scene.coords(62,15.5),-1/2,1,root.open_room_4],
	[scene.coords(62,17.5),-1/2,1,root.open_room_4],
	[scene.coords(62,19.5),-1/2,1,root.open_room_4]]

	property var enemy_tab_room_5: [
	[scene.coords(70,43),-1/2,3,root.open_room_5],
	[scene.coords(70,44),-1/2,3,root.open_room_5],
	[scene.coords(70,45),-1/2,3,root.open_room_5]]

	property var enemy_tab_room_6: [
	[scene.coords(68.5,51.5),1,0,root.open_room_6],
	[scene.coords(69.5,51.5),1,0,root.open_room_6],
	[scene.coords(70.5,51.5),1,0,root.open_room_6],
	[scene.coords(71.5,51.5),1,0,root.open_room_6],
	[scene.coords(68.5,52.5),1,0,root.open_room_6],
	[scene.coords(69.5,52.5),1,0,root.open_room_6],
	[scene.coords(70.5,52.5),1,0,root.open_room_6],
	[scene.coords(71.5,52.5),1,0,root.open_room_6],
	[scene.coords(76.5,50.5),-1/2,0,root.open_room_6],
	[scene.coords(76.5,51.5),-1/2,0,root.open_room_6],
	[scene.coords(76.5,54.5),-1/2,0,root.open_room_6],
	[scene.coords(76.5,55.5),-1/2,0,root.open_room_6],
	[scene.coords(76.5,58.5),-1/2,0,root.open_room_6],
	[scene.coords(76.5,59.5),-1/2,0,root.open_room_6],
	[scene.coords(76.5,62.5),-1/2,0,root.open_room_6],
	[scene.coords(76.5,63.5),-1/2,0,root.open_room_6],
	[scene.coords(76.5,66.5),-1/2,0,root.open_room_6],
	[scene.coords(76.5,67.5),-1/2,0,root.open_room_6],
	[scene.coords(77.5,50.5),-1/2,0,root.open_room_6],
	[scene.coords(77.5,51.5),-1/2,0,root.open_room_6],
	[scene.coords(77.5,54.5),-1/2,0,root.open_room_6],
	[scene.coords(77.5,55.5),-1/2,0,root.open_room_6],
	[scene.coords(77.5,58.5),-1/2,0,root.open_room_6],
	[scene.coords(77.5,59.5),-1/2,0,root.open_room_6],
	[scene.coords(77.5,62.5),-1/2,0,root.open_room_6],
	[scene.coords(77.5,63.5),-1/2,0,root.open_room_6],
	[scene.coords(77.5,66.5),-1/2,0,root.open_room_6],
	[scene.coords(77.5,67.5),-1/2,0,root.open_room_6],
	[scene.coords(84.5,52.5),-1/2,0,root.open_room_6],
	[scene.coords(84.5,53.5),-1/2,0,root.open_room_6],
	[scene.coords(84.5,55.5),-1/2,0,root.open_room_6],
	[scene.coords(84.5,56.5),-1/2,0,root.open_room_6],
	[scene.coords(84.5,58.5),-1/2,0,root.open_room_6],
	[scene.coords(84.5,59.5),-1/2,0,root.open_room_6],
	[scene.coords(84.5,61.5),-1/2,0,root.open_room_6],
	[scene.coords(84.5,62.5),-1/2,0,root.open_room_6],
	[scene.coords(84.5,64.5),-1/2,0,root.open_room_6],
	[scene.coords(84.5,65.5),-1/2,0,root.open_room_6],
	[scene.coords(85.5,52.5),-1/2,0,root.open_room_6],
	[scene.coords(85.5,53.5),-1/2,0,root.open_room_6],
	[scene.coords(85.5,55.5),-1/2,0,root.open_room_6],
	[scene.coords(85.5,56.5),-1/2,0,root.open_room_6],
	[scene.coords(85.5,58.5),-1/2,0,root.open_room_6],
	[scene.coords(85.5,59.5),-1/2,0,root.open_room_6],
	[scene.coords(85.5,61.5),-1/2,0,root.open_room_6],
	[scene.coords(85.5,62.5),-1/2,0,root.open_room_6],
	[scene.coords(85.5,64.5),-1/2,0,root.open_room_6],
	[scene.coords(85.5,65.5),-1/2,0,root.open_room_6],
	[scene.coords(87,51.5),-1/2,1,root.open_room_6],
	[scene.coords(87,53.5),-1/2,1,root.open_room_6],
	[scene.coords(87,55.5),-1/2,1,root.open_room_6],
	[scene.coords(87,57.5),-1/2,1,root.open_room_6],
	[scene.coords(87,59.5),-1/2,1,root.open_room_6],
	[scene.coords(87,61.5),-1/2,1,root.open_room_6],
	[scene.coords(87,63.5),-1/2,1,root.open_room_6],
	[scene.coords(87,65.5),-1/2,1,root.open_room_6],
	[scene.coords(88,56.5),-1/2,3,root.open_room_6],
	[scene.coords(88,57.5),-1/2,3,root.open_room_6],
	[scene.coords(88,58.5),-1/2,3,root.open_room_6],
	[scene.coords(88,59.5),-1/2,3,root.open_room_6],
	[scene.coords(88,60.5),-1/2,3,root.open_room_6]]

	property var enemy_tab_corridor_4_1: [
	[scene.coords(84.5,45.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(85.5,45.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(89.5,45.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(90.5,45.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(93.5,45.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(94.5,45.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(98.5,45.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(99.5,45.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(105.5,45.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(106.5,45.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(84.5,46.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(85.5,46.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(89.5,46.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(90.5,46.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(93.5,46.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(94.5,46.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(98.5,46.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(99.5,46.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(105.5,46.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(106.5,46.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(84.5,47.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(85.5,47.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(89.5,47.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(90.5,47.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(93.5,47.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(94.5,47.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(98.5,47.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(99.5,47.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(105.5,47.5),-1/2,0,root.open_corridor_4_1],
	[scene.coords(106.5,47.5),-1/2,0,root.open_corridor_4_1]]

	property var enemy_tab_corridor_4_2: [
	[scene.coords(119.5,45.5),-1/2,0,root.open_corridor_4_2],
	[scene.coords(120.5,45.5),-1/2,0,root.open_corridor_4_2],
	[scene.coords(125.5,45.5),-1/2,0,root.open_corridor_4_2],
	[scene.coords(126.5,45.5),-1/2,0,root.open_corridor_4_2],
	[scene.coords(119.5,46.5),-1/2,0,root.open_corridor_4_2],
	[scene.coords(120.5,46.5),-1/2,0,root.open_corridor_4_2],
	[scene.coords(125.5,46.5),-1/2,0,root.open_corridor_4_2],
	[scene.coords(126.5,46.5),-1/2,0,root.open_corridor_4_2],
	[scene.coords(119.5,47.5),-1/2,0,root.open_corridor_4_2],
	[scene.coords(120.5,47.5),-1/2,0,root.open_corridor_4_2],
	[scene.coords(125,43.5),1,0,root.open_corridor_4_2],
	[scene.coords(125,38.5),1,0,root.open_corridor_4_2],
	[scene.coords(125,34.5),1,0,root.open_corridor_4_2],
	[scene.coords(125,30.5),1,0,root.open_corridor_4_2],
	[scene.coords(125,25.5),1,0,root.open_corridor_4_2],
	[scene.coords(125,21.5),1,0,root.open_corridor_4_2],
	[scene.coords(125,16.5),1,0,root.open_corridor_4_2],
	[scene.coords(125,12.5),1,0,root.open_corridor_4_2],
	[scene.coords(125,43.5),1,0,root.open_corridor_4_2],
	[scene.coords(127.5,38.5),1,0,root.open_corridor_4_2],
	[scene.coords(127.5,34.5),1,0,root.open_corridor_4_2],
	[scene.coords(127.5,30.5),1,0,root.open_corridor_4_2],
	[scene.coords(127.5,25.5),1,0,root.open_corridor_4_2],
	[scene.coords(127.5,21.5),1,0,root.open_corridor_4_2],
	[scene.coords(127.5,16.5),1,0,root.open_corridor_4_2],
	[scene.coords(127.5,12.5),1,0,root.open_corridor_4_2]]

	property var enemy_tab_corridor_4_3: [
	[scene.coords(127.5,6.5),1,0,root.open_corridor_4_3],
	[scene.coords(125.5,6.5),1,0,root.open_corridor_4_3],
	[scene.coords(126.5,3.5),1,0,root.open_corridor_4_3],
	[scene.coords(125.5,4.5),1,0,root.open_corridor_4_3],
	[scene.coords(126.5,4.5),1,0,root.open_corridor_4_3],
	[scene.coords(125.5,3.5),1,0,root.open_corridor_4_3],
	[scene.coords(121.5,2.5),1/2,0,root.open_corridor_4_3],
	[scene.coords(115.5,2.5),1/2,0,root.open_corridor_4_3],
	[scene.coords(109.5,2.5),1/2,0,root.open_corridor_4_3],
	[scene.coords(104.5,2.5),1/2,0,root.open_corridor_4_3],
	[scene.coords(121.5,4.5),1/2,0,root.open_corridor_4_3],
	[scene.coords(115.5,4.5),1/2,0,root.open_corridor_4_3],
	[scene.coords(109.5,4.5),1/2,0,root.open_corridor_4_3],
	[scene.coords(104.5,4.5),1/2,0,root.open_corridor_4_3]]

	property var enemy_tab_corridor_4_4: [
	[scene.coords(96.5,2.5),1/2,0,root.open_corridor_4_4],
	[scene.coords(91.5,2.5),1/2,0,root.open_corridor_4_4],
	[scene.coords(87.5,2.5),1/2,0,root.open_corridor_4_4],
	[scene.coords(82.5,2.5),1/2,0,root.open_corridor_4_4],
	[scene.coords(75.5,2.5),1/2,0,root.open_corridor_4_4],
	[scene.coords(96.5,4.5),1/2,0,root.open_corridor_4_4],
	[scene.coords(91.5,4.5),1/2,0,root.open_corridor_4_4],
	[scene.coords(87.5,4.5),1/2,0,root.open_corridor_4_4],
	[scene.coords(82.5,4.5),1/2,0,root.open_corridor_4_4],
	[scene.coords(77.5,4.5),1/2,0,root.open_corridor_4_4],
	[scene.coords(75,7.5),0,0,root.open_corridor_4_4],
	[scene.coords(75,11.5),0,0,root.open_corridor_4_4],
	[scene.coords(75,15.5),0,0,root.open_corridor_4_4],
	[scene.coords(75,19.5),0,0,root.open_corridor_4_4],
	[scene.coords(75,23.5),0,0,root.open_corridor_4_4],
	[scene.coords(77.5,7.5),0,0,root.open_corridor_4_4],
	[scene.coords(77.5,11.5),0,0,root.open_corridor_4_4],
	[scene.coords(77.5,15.5),0,0,root.open_corridor_4_4],
	[scene.coords(77.5,19.5),0,0,root.open_corridor_4_4],
	[scene.coords(77.5,23.5),0,0,root.open_corridor_4_4]]

	property var enemy_tab_corridor_4_5: [
	[scene.coords(77.5,31.5),0,0,root.open_corridor_4_5],
	[scene.coords(77.5,35.5),0,0,root.open_corridor_4_5],
	[scene.coords(77.5,39.5),0,0,root.open_corridor_4_5],
	[scene.coords(77.5,42.5),0,0,root.open_corridor_4_5],
	[scene.coords(75,31.5),0,0,root.open_corridor_4_5],
	[scene.coords(75,35.5),0,0,root.open_corridor_4_5],
	[scene.coords(75,39.5),0,0,root.open_corridor_4_5],
	[scene.coords(75,42.5),0,0,root.open_corridor_4_5],
	[scene.coords(87.5,40.5),-1/2,0,root.open_corridor_4_5],
	[scene.coords(96.5,40.5),-1/2,0,root.open_corridor_4_5],
	[scene.coords(104.5,40.5),-1/2,0,root.open_corridor_4_5],
	[scene.coords(111.5,40.5),-1/2,0,root.open_corridor_4_5],
	[scene.coords(119.5,40.5),-1/2,0,root.open_corridor_4_5],
	[scene.coords(87.5,42.5),-1/2,0,root.open_corridor_4_5],
	[scene.coords(96.5,42.5),-1/2,0,root.open_corridor_4_5],
	[scene.coords(104.5,42.5),-1/2,0,root.open_corridor_4_5],
	[scene.coords(111.5,42.5),-1/2,0,root.open_corridor_4_5],
	[scene.coords(121.5,42.5),-1/2,0,root.open_corridor_4_5],
	[scene.coords(83,41),-1/2,3,root.open_corridor_4_5],
	[scene.coords(93,41),-1/2,3,root.open_corridor_4_5],
	[scene.coords(100,41),-1/2,3,root.open_corridor_4_5],
	[scene.coords(108,41),-1/2,3,root.open_corridor_4_5],
	[scene.coords(114,41),-1/2,3,root.open_corridor_4_5],
	[scene.coords(120.5,38),1,3,root.open_corridor_4_5],
	[scene.coords(120.5,34),1,3,root.open_corridor_4_5],
	[scene.coords(120.5,32),1,3,root.open_corridor_4_5],
	[scene.coords(120.5,27),1,3,root.open_corridor_4_5],
	[scene.coords(120.5,23),1,3,root.open_corridor_4_5],
	[scene.coords(122,38),1,3,root.open_corridor_4_5],
	[scene.coords(122,34),1,3,root.open_corridor_4_5],
	[scene.coords(122,32),1,3,root.open_corridor_4_5],
	[scene.coords(122,27),1,3,root.open_corridor_4_5],
	[scene.coords(122,23),1,3,root.open_corridor_4_5],
	[scene.coords(120,36.5),1,0,root.open_corridor_4_5],
	[scene.coords(120,30.5),1,0,root.open_corridor_4_5],
	[scene.coords(120,25.5),1,0,root.open_corridor_4_5],
	[scene.coords(122.5,36.5),1,0,root.open_corridor_4_5],
	[scene.coords(122.5,30.5),1,0,root.open_corridor_4_5],
	[scene.coords(122.5,25.5),1,0,root.open_corridor_4_5]
	]

	property var enemy_tab_corridor_4_6: [
	[scene.coords(120.5,17),1,3,root.open_corridor_4_6],
	[scene.coords(120.5,15),1,3,root.open_corridor_4_6],
	[scene.coords(122,17),1,3,root.open_corridor_4_6],
	[scene.coords(122,15),1,3,root.open_corridor_4_6],
	[scene.coords(120,19.5),1,0,root.open_corridor_4_6],
	[scene.coords(120,13.5),1,0,root.open_corridor_4_6],
	[scene.coords(122.5,19.5),1,0,root.open_corridor_4_6],
	[scene.coords(122.5,13.5),1,0,root.open_corridor_4_6],
	[scene.coords(120.5,9.5),1,0,root.open_corridor_4_6],
	[scene.coords(121.5,9.5),1,0,root.open_corridor_4_6],
	[scene.coords(120.5,8.5),1,0,root.open_corridor_4_6],
	[scene.coords(121.5,8.5),1,0,root.open_corridor_4_6],
	[scene.coords(113.5,8.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(114.5,8.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(113.5,9.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(114.5,9.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(108.5,7.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(103.5,7.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(98.5,7.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(94.5,7.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(89.5,7.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(84.5,7.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(80.5,7.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(108.5,9.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(103.5,9.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(98.5,9.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(94.5,9.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(89.5,9.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(84.5,9.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(82.5,9.5),1/2,0,root.open_corridor_4_6],
	[scene.coords(80,13.5),0,0,root.open_corridor_4_6],
	[scene.coords(80,17.5),0,0,root.open_corridor_4_6],
	[scene.coords(80,21.5),0,0,root.open_corridor_4_6],
	[scene.coords(80,26.5),0,0,root.open_corridor_4_6],
	[scene.coords(82.5,13.5),0,0,root.open_corridor_4_6],
	[scene.coords(82.5,17.5),0,0,root.open_corridor_4_6],
	[scene.coords(82.5,21.5),0,0,root.open_corridor_4_6],
	[scene.coords(82.5,26.5),0,0,root.open_corridor_4_6]]

	property var enemy_tab_room_7: [
	[scene.coords(85.5,27.5),1,0,root.open_room_7],
	[scene.coords(86.5,27.5),1,0,root.open_room_7],
	[scene.coords(87.5,27.5),1,0,root.open_room_7],
	[scene.coords(88.5,27.5),1,0,root.open_room_7],
	[scene.coords(89.5,27.5),1,0,root.open_room_7],
	[scene.coords(90.5,27.5),1,0,root.open_room_7],
	[scene.coords(91.5,27.5),1,0,root.open_room_7],
	[scene.coords(92.5,27.5),1,0,root.open_room_7],
	[scene.coords(85.5,28.5),1,0,root.open_room_7],
	[scene.coords(86.5,28.5),1,0,root.open_room_7],
	[scene.coords(87.5,28.5),1,0,root.open_room_7],
	[scene.coords(88.5,28.5),1,0,root.open_room_7],
	[scene.coords(89.5,28.5),1,0,root.open_room_7],
	[scene.coords(90.5,28.5),1,0,root.open_room_7],
	[scene.coords(91.5,28.5),1,0,root.open_room_7],
	[scene.coords(92.5,28.5),1,0,root.open_room_7],
	[scene.coords(97.5,14.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,15.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,17.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,18.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,20.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,21.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,23.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,24.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,26.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,27.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,29.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,30.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,32.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,33.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,35.5),-1/2,0,root.open_room_7],
	[scene.coords(97.5,36.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,14.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,15.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,17.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,18.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,20.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,21.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,23.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,24.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,26.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,27.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,29.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,30.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,32.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,33.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,35.5),-1/2,0,root.open_room_7],
	[scene.coords(98.5,36.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,13.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,14.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,15.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,17.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,18.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,19.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,21.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,22.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,23.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,25.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,26.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,27.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,29.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,30.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,31.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,33.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,34.5),-1/2,0,root.open_room_7],
	[scene.coords(108.5,35.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,13.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,14.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,15.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,17.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,18.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,19.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,21.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,22.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,23.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,25.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,26.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,27.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,29.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,30.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,31.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,33.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,34.5),-1/2,0,root.open_room_7],
	[scene.coords(109.5,35.5),-1/2,0,root.open_room_7],
	[scene.coords(100,13.5),-1/2,1,root.open_room_7],
	[scene.coords(100,15.5),-1/2,1,root.open_room_7],
	[scene.coords(100,17.5),-1/2,1,root.open_room_7],
	[scene.coords(100,19.5),-1/2,1,root.open_room_7],
	[scene.coords(100,21.5),-1/2,1,root.open_room_7],
	[scene.coords(100,23.5),-1/2,1,root.open_room_7],
	[scene.coords(100,25.5),-1/2,1,root.open_room_7],
	[scene.coords(100,27.5),-1/2,1,root.open_room_7],
	[scene.coords(100,29.5),-1/2,1,root.open_room_7],
	[scene.coords(100,31.5),-1/2,1,root.open_room_7],
	[scene.coords(100,33.5),-1/2,1,root.open_room_7],
	[scene.coords(100,35.5),-1/2,1,root.open_room_7],
	[scene.coords(100,37.5),-1/2,1,root.open_room_7],
	[scene.coords(108.5,14),-1/2,3,root.open_room_7],
	[scene.coords(108.5,15),-1/2,3,root.open_room_7],
	[scene.coords(108.5,18),-1/2,3,root.open_room_7],
	[scene.coords(108.5,19),-1/2,3,root.open_room_7],
	[scene.coords(108.5,22),-1/2,3,root.open_room_7],
	[scene.coords(108.5,23),-1/2,3,root.open_room_7],
	[scene.coords(108.5,26),-1/2,3,root.open_room_7],
	[scene.coords(108.5,27),-1/2,3,root.open_room_7],
	[scene.coords(108.5,30),-1/2,3,root.open_room_7],
	[scene.coords(108.5,31),-1/2,3,root.open_room_7],
	[scene.coords(108.5,34),-1/2,3,root.open_room_7],
	[scene.coords(108.5,35),-1/2,3,root.open_room_7]
	]

	property var enemy_tab_retour: [
	[scene.coords(82.5,35.5),1/2,1,root.generator_room_7_destroyed],
	[scene.coords(82.5,37.5),1/2,1,root.generator_room_7_destroyed],
	[scene.coords(85.5,35.5),1/2,1,root.generator_room_7_destroyed],
	[scene.coords(85.5,37.5),1/2,1,root.generator_room_7_destroyed],
	[scene.coords(75.5,10.5),1,0,root.generator_room_7_destroyed],
	[scene.coords(75.5,12.5),1,0,root.generator_room_7_destroyed],
	[scene.coords(77,10.5),1,0,root.generator_room_7_destroyed],
	[scene.coords(77,12.5),1,0,root.generator_room_7_destroyed],
	[scene.coords(75.5,18.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(75.5,18.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(77,21),0,0,root.generator_room_7_destroyed],
	[scene.coords(77,21),0,0,root.generator_room_7_destroyed],
	[scene.coords(80.5,45.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(80.5,47.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(82.5,45.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(82.5,47.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(70.5,43.5),1/2,0,root.generator_room_7_destroyed],
	[scene.coords(70.5,46.5),1/2,0,root.generator_room_7_destroyed],
	[scene.coords(72,43.5),1/2,0,root.generator_room_7_destroyed],
	[scene.coords(72,46.5),1/2,0,root.generator_room_7_destroyed],
	[scene.coords(70.5,31.5),1,2,root.generator_room_7_destroyed],
	[scene.coords(70.5,33.5),1,2,root.generator_room_7_destroyed],
	[scene.coords(72,31.5),1,2,root.generator_room_7_destroyed],
	[scene.coords(72,33.5),1,2,root.generator_room_7_destroyed],
	[scene.coords(70.5,15.5),-1/2,0,root.generator_room_7_destroyed],
	[scene.coords(70.5,18.5),-1/2,0,root.generator_room_7_destroyed],
	[scene.coords(72,15.5),-1/2,0,root.generator_room_7_destroyed],
	[scene.coords(72,18.5),-1/2,0,root.generator_room_7_destroyed],
	[scene.coords(62.5,64.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(62.5,67),0,0,root.generator_room_7_destroyed],
	[scene.coords(64,64.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(64,67),0,0,root.generator_room_7_destroyed],
	[scene.coords(62.5,62.5),0,3,root.generator_room_7_destroyed],
	[scene.coords(62.5,65.5),0,3,root.generator_room_7_destroyed],
	[scene.coords(64,62.5),0,3,root.generator_room_7_destroyed],
	[scene.coords(64,65.5),0,3,root.generator_room_7_destroyed],
	[scene.coords(56.5,46.5),1/2,3,root.generator_room_7_destroyed],
	[scene.coords(57.5,48.5),1/2,3,root.generator_room_7_destroyed],
	[scene.coords(57.5,50.5),1/2,3,root.generator_room_7_destroyed],
	[scene.coords(56.5,52.5),1/2,3,root.generator_room_7_destroyed],
	[scene.coords(58.5,46.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(58.5,49.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(58.5,52.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(56.5,49.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(36.5,49.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(36.5,51.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(36.5,53.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(36.5,55.5),1/2,2,root.generator_room_7_destroyed],
	[scene.coords(31,44.5),1,0,root.generator_room_7_destroyed],
	[scene.coords(28.5,44.5),1,0,root.generator_room_7_destroyed],
	[scene.coords(30.5,35.5),1,0,root.generator_room_7_destroyed],
	[scene.coords(33,35.5),1,0,root.generator_room_7_destroyed],
	[scene.coords(31,38.5),1,0,root.generator_room_7_destroyed],
	[scene.coords(33,38.5),1,0,root.generator_room_7_destroyed],
	[scene.coords(16,18.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(16,21.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(16,54),0,0,root.generator_room_7_destroyed],
	[scene.coords(16,59.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(18.5,18.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(18.5,21.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(18.5,54),0,0,root.generator_room_7_destroyed],
	[scene.coords(18.5,59.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(37.5,65.5),-1/2,0,root.generator_room_7_destroyed],
	[scene.coords(57.5,65.5),-1/2,0,root.generator_room_7_destroyed],
	[scene.coords(59,65.5),-1/2,0,root.generator_room_7_destroyed],
	[scene.coords(37.5,67.5),-1/2,0,root.generator_room_7_destroyed],
	[scene.coords(57.5,68),-1/2,0,root.generator_room_7_destroyed],
	[scene.coords(59,68),-1/2,0,root.generator_room_7_destroyed],
	[scene.coords(50.5,65.5),-1/2,3,root.generator_room_7_destroyed],
	[scene.coords(50.5,67),-1/2,3,root.generator_room_7_destroyed],
	[scene.coords(52,65.5),-1/2,3,root.generator_room_7_destroyed],
	[scene.coords(52,67),-1/2,3,root.generator_room_7_destroyed],
	[scene.coords(50,60),0,1,root.generator_room_7_destroyed],
	[scene.coords(51,60),0,1,root.generator_room_7_destroyed],
	[scene.coords(52,60),0,1,root.generator_room_7_destroyed],
	[scene.coords(53,60),0,2,root.generator_room_7_destroyed],
	[scene.coords(49,60),0,2,root.generator_room_7_destroyed],
	[scene.coords(53,61),0,1,root.generator_room_7_destroyed],
	[scene.coords(53,62),0,1,root.generator_room_7_destroyed],
	[scene.coords(49,61),0,1,root.generator_room_7_destroyed],
	[scene.coords(49,62),0,1,root.generator_room_7_destroyed],
	[scene.coords(48.5,60.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(48.5,61.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(53.5,60.5),0,0,root.generator_room_7_destroyed],
	[scene.coords(53.5,61.5),0,0,root.generator_room_7_destroyed]
	]

	property var enemy_tab_hidden_boss: [
	[scene.coords(111,57),1,1,root.open_room_boss],
	[scene.coords(111,59.5),1,2,root.open_room_boss],
	[scene.coords(111,62),1,1,root.open_room_boss],
	[scene.coords(113.5,57),1,2,root.open_room_boss],
	[scene.coords(113.5,62),1,2,root.open_room_boss],
	[scene.coords(116,57),1,1,root.open_room_boss],
	[scene.coords(116,59.5),1,2,root.open_room_boss],
	[scene.coords(116,62),1,1,root.open_room_boss]
	]

	Q.Component
	{
		id: turret_factory
		Turret
		{
		}
	}

	Q.Component
	{
		id: mini_boss_factory
		Vehicle
		{
			function local_coords(x, y) {return Qt.point(x / 540 - 1, y / 540 - 1)} // images en 1080*1080
			function group_of(x) {return x ? shield ? groups.enemy_hull : groups.enemy_hull_naked : 0}
			id: mini_boss
			icon: 5
			icon_color: "red"
			type: Body.STATIC
			scale: 6
			Q.Component.onCompleted: shield = max_shield
			property bool onfire: behaviour.shooting
			max_speed: 0
			max_angular_speed: 0
			property bool shield_1: true
			property bool shield_2: true
			property bool shield_3: true
			property bool quipoutre_1: true
			property bool quipoutre_2: true
			property bool quipoutre_3: true
			readonly property bool alive: shield_1 || shield_2 || shield_3 || quipoutre_1 || quipoutre_2 || quipoutre_3
			onAliveChanged: if(!alive) destroy()
			loot_factory: Repeater
			{
				count: 10
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "chapter-2/mini-boss-ss"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: Qt.rgba(0, 0.4, 1, mini_boss.shield / mini_boss.max_shield)
				position: local_coords(540, 717)
				scale: 510/540
				z: altitudes.shield
			}
			Behaviour_attack_distance
			{
				id: behaviour
				target_groups: groups.enemy_targets
				max_distance: 40
			}
			CircleCollider
			{
				position: local_coords(540, 717)
				group: shield ? groups.enemy_shield : 0
				radius: 510/540
			}

			CircleCollider
			{
				group: group_of(quipoutre_1)
				radius: 80/540
				position: local_coords(223, 540)
			}
			EquipmentSlot
			{
				position: local_coords(223, 540)
				scale: 0.15*6/mini_boss.scale
				angle: Math.PI * -1/4
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_1 = false
				}
			}

			CircleCollider
			{
				group: group_of(quipoutre_2)
				radius: 80/540
				position: local_coords(540, 540)
			}
			EquipmentSlot
			{
				position: local_coords(540, 540)
				scale: 0.15*6/mini_boss.scale
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_2 = false
				}
			}

			CircleCollider
			{
				group: group_of(quipoutre_3)
				radius: 80/540
				position: local_coords(854, 540)
			}
			EquipmentSlot
			{
				position: local_coords(854, 540)
				scale: 0.15*6/mini_boss.scale
				angle: Math.PI * 1/4
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_3 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_1)
				radius: 60/540
				position: local_coords(303, 848)
			}
			EquipmentSlot
			{
				position: local_coords(303, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_1 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_2)
				radius: 60/540
				position: local_coords(540, 848)
			}
			EquipmentSlot
			{
				position: local_coords(540, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_2 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_3)
				radius: 60/540
				position: local_coords(776, 848)
			}
			EquipmentSlot
			{
				position: local_coords(776, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_3 = false
				}
			}
		}
	}
	Q.Component
	{
		id: move_turret_hidden_boss_factory
		Animation
		{
			id: timer
			property var tab_enemy
			property bool fin: false
			property real rotation_speed: 0.1 // (= 1/nbframe pour parcourir une distance de 7)
			onFinChanged:
			{
				if(fin)
				{
					for(var i = 0; i < tab_enemy.length; ++i)
					{
						if(tab_enemy[i])
						{
							tab_enemy[i].alive = false
						}
					}
					destroy()
				}
			}
			property color mask: "orange"
			time: 0
			speed: -1
			function mae(x){return x * 2 * chapter_scale}
			onTimeChanged:
			{
				if(tab_enemy)
				{
					for(var i = 0; i < tab_enemy.length; ++i)
					{
						if(tab_enemy[i] && tab_enemy[i].position)
						{
							if(tab_enemy[i].position.x > mae(111) && tab_enemy[i].position.y > mae(57))
							{
								if(tab_enemy[i].position.y < mae(62))
								{
									tab_enemy[i].position.y -= rotation_speed
								}
								else
								{
									if(tab_enemy[i].position.x < mae(116))
									{
										tab_enemy[i].position.x += rotation_speed
									}
									else
									{
										tab_enemy[i].position.y -= rotation_speed
									}
								}
							}
							if(tab_enemy[i].position.x < mae(116) && tab_enemy[i].position.y < mae(62))
							{
								if(tab_enemy[i].position.y > mae(57))
								{
									tab_enemy[i].position.y += rotation_speed
								}
								else
								{
									if(tab_enemy[i].position.x > mae(111))
									{
										tab_enemy[i].position.x -= rotation_speed
									}
									else
									{
										tab_enemy[i].position.y += rotation_speed
									}
								}
							}
							if(Math.abs(tab_enemy[i].position.x - mae(111)) < rotation_speed && Math.abs(tab_enemy[i].position.y - mae(62)) < rotation_speed)
							{
								tab_enemy[i].position.x = mae(111) + rotation_speed
								tab_enemy[i].position.y = mae(62)
							}
							if(Math.abs(tab_enemy[i].position.x - mae(116)) < rotation_speed && Math.abs(tab_enemy[i].position.y - mae(57)) < rotation_speed)
							{
								tab_enemy[i].position.x = mae(116) - rotation_speed
								tab_enemy[i].position.y = mae(57)
							}
						}
					}
				}
			}
		}
	}
	Q.Component
	{
		id: hidden_boss_factory
		Vehicle
		{
			id: vauban
			icon: 5
			icon_color: "red"
			max_speed: 0
			max_angular_speed: 0.4
			type: Body.KINEMATIC
			//weapon_regeneration: 5
			scale: 10
			Q.Component.onCompleted: shield = max_shield
			Q.Component.onDestruction: one_boss_destroyed()
			loot_factory: Repeater
			{
				count: 31
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "chapter-2/boss-cache"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: Qt.rgba(0, 0.4, 1, vauban.shield / vauban.max_shield)
				scale: 1.5
				z: altitudes.shield
			}

			CircleCollider
			{
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
				radius: 0.8
			}

			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				radius: 1.5
			}
			Behaviour_attack_rotate
			{
				target_groups: groups.enemy_targets
				target_distance: 10
			}
			EquipmentSlot
			{
				scale: 0.1
				equipment: Shield
				{
					level: 2
				}
			}
			Repeater
			{
				count: 5
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .22
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .22
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Shield
						{
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .74
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .74
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: LaserGun
						{
							shooting: true
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .47
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .47
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.08
						equipment: Quipoutre
						{
							shooting: true
							period: scene.seconds_to_frames(2)
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index - .025) * 72 * Math.PI / 180) * .66
						position.y: Math.sin((index - .025) * 72 * Math.PI / 180) * .66
						angle: (index - .01) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Gun
						{
							shooting: true
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .525) * 72 * Math.PI / 180) * .66
						position.y: Math.sin((index + .525) * 72 * Math.PI / 180) * .66
						angle: (index + .51) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Gun
						{
							shooting: true
							level: 2
						}
					}
				}
			}
		}
	}

	scene: Scene
	{
		Animation {id: timer}
		Body
		{
			id: non_destroyed_walls
			scale: chapter_scale
			type: Body.STATIC
			Repeater
			{
				count: walls.length
				Q.Component
				{
					PolygonCollider
					{
						vertexes: [	Qt.point(walls[index][0][0]*2-0.621,walls[index][0][1]*2-0.621),
									Qt.point(walls[index][1][0]*2+0.621,walls[index][0][1]*2-0.621),
									Qt.point(walls[index][1][0]*2+0.621,walls[index][1][1]*2+0.621),
									Qt.point(walls[index][0][0]*2-0.621,walls[index][1][1]*2+0.621)]
						group: groups.wall
					}
				}
			}
		}
		Body
		{
			id: sortie
			type: Body.STATIC
			position.x: 58*2*chapter_scale
			position.y: 70*2*chapter_scale
			icon: visible ? 1 : 0
			icon_color: "blue"
			property bool visible : root.sortie_on
		}
		/*
		LandscapeModification
		{
			position.x: x du centre de l element
			position.y: y du centre de l element
			vertical: true ou false (par defaut false)
			count: nombre de tuiles creees (longueur du mur *2)
			mask: masque a apporter a l objet
			group: le groupe du PolygonCollider
			element_scale: chapter_scale
			texture: texture d element: "collapsed-door","destroyed-wall" ou "collapsed-corridor"
		}
		Exemple:
		LandscapeModification
		{
			position.x: (60.5*2)*chapter_scale
			position.y: (49.5*2)*chapter_scale
			vertical: true
			count: 8
			mask: root.escape_ss ? "white" : "transparent"
			group: root.escape_ss ? 0 : groups.wall
			element_scale: chapter_scale
			texture: "collapsed-door"
		}
		*/
		// Mur entre room_3 et corridor_2
		LandscapeModification
		{
			position.x: (29.5*2)*chapter_scale
			position.y: (45.5*2)*chapter_scale
			vertical: false
			count: 8
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "destroyed-wall"
			group: root.escape_ss ? 0 : groups.wall
		}
		// Dans corridor_2
		LandscapeModification
		{
			position.x: (22*2)*chapter_scale
			position.y: (55.5*2)*chapter_scale
			vertical: false
			count: 10
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "collapsed-corridor"
			group: root.escape_ss ? groups.wall : 0
		}
		// Mur entre room_2 et corridor_3_2
		LandscapeModification
		{
			position.x: (60.5*2)*chapter_scale
			position.y: (49.5*2)*chapter_scale
			vertical: true
			count: 8
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "destroyed-wall"
			group: root.escape_ss ? 0 : groups.wall
		}
		// Dans corridor_3_1
		LandscapeModification
		{
			position.x: (42.5*2)*chapter_scale
			position.y: (11*2)*chapter_scale
			vertical: true
			count: 10
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "collapsed-corridor"
			group: root.escape_ss ? groups.wall : 0
		}
		// Porte entre corridor_3_1 et room_4
		LandscapeModification
		{
			position.x: (54*2)*chapter_scale
			position.y: (13.5*2)*chapter_scale
			vertical: false
			count: 6
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "collapsed-door"
			group: root.escape_ss ? groups.wall : 0
		}
		// Mur entre corridor_3_1 et room_4
		LandscapeModification
		{
			position.x: (68.5*2)*chapter_scale
			position.y: (16.5*2)*chapter_scale
			vertical: true
			count: 8
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "destroyed-wall"
			group: root.escape_ss ? 0 : groups.wall
		}
		// Entre corridor_3_1 et corridor_3_2
		LandscapeModification
		{
			position.x: (71*2)*chapter_scale
			position.y: (19.5*2)*chapter_scale
			vertical: false
			count: 10
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "collapsed-corridor"
			group: root.escape_ss ? groups.wall : 0
		}
		// Mur entre corridor_3_2 et room_4
		LandscapeModification
		{
			position.x: (68.5*2)*chapter_scale
			position.y: (31.5*2)*chapter_scale
			vertical: true
			count: 8
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "destroyed-wall"
			group: root.escape_ss ? 0 : groups.wall
		}
		// Mur entre corridor_3_2 et room_5
		LandscapeModification
		{
			position.x: (69.5*2)*chapter_scale
			position.y: (40.5*2)*chapter_scale
			vertical: false
			count: 8
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "destroyed-wall"
			group: root.escape_ss ? 0 : groups.wall
		}
		// Porte entre corridor_3_2 et room_5
		LandscapeModification
		{
			position.x: (65.5*2)*chapter_scale
			position.y: (43*2)*chapter_scale
			vertical: true
			count: 6
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "collapsed-door"
			group: root.escape_ss ? groups.wall : 0
		}
		// Mur entre corridor_4_1 et room_5
		LandscapeModification
		{
			position.x: (73.5*2)*chapter_scale
			position.y: (46*2)*chapter_scale
			vertical: true
			count: 6
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "destroyed-wall"
			group: root.escape_ss ? 0 : groups.wall
		}
		// Porte entre corridor_3_2 et room_6
		LandscapeModification
		{
			position.x: (76*2)*chapter_scale
			position.y: (48.5*2)*chapter_scale
			vertical: false
			count: 6
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "collapsed-door"
			group: root.escape_ss ? groups.wall : 0
		}
		// Passage entre room_6 et room_boss
		LandscapeModification
		{
			position.x: (98.5*2)*chapter_scale
			position.y: (58.5*2)*chapter_scale
			vertical: true
			count: 24
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "collapsed-door"
			group: root.escape_ss ? groups.wall : 0
		}
		// Mur entre corridor_4_1 et corridor_4_5
		LandscapeModification
		{
			position.x: (90.5*2)*chapter_scale
			position.y: (43.5*2)*chapter_scale
			vertical: false
			count: 8
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "destroyed-wall"
			group: root.escape_ss ? 0 : groups.wall
		}
		// Mur entre corridor_4_2 et room_boss
		LandscapeModification
		{
			position.x: (124.5*2)*chapter_scale
			position.y: (48.5*2)*chapter_scale
			vertical: false
			count: 8
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "destroyed-wall"
			group: root.escape_ss ? 0 : groups.wall
		}
		// Dans corridor_4_3
		LandscapeModification
		{
			position.x: (119.5*2)*chapter_scale
			position.y: (3*2)*chapter_scale
			vertical: true
			count: 10
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "collapsed-corridor"
			group: root.escape_ss ? groups.wall : 0
		}
		// Mur entre corridor_4_4 et corridor_4_6
		LandscapeModification
		{
			position.x: (78.5*2)*chapter_scale
			position.y: (15.5*2)*chapter_scale
			vertical: true
			count: 8
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "destroyed-wall"
			group: root.escape_ss ? 0 : groups.wall
		}
		// Porte entre corridor_4_5 et corridor_4_6
		LandscapeModification
		{
			position.x: (121*2)*chapter_scale
			position.y: (20.5*2)*chapter_scale
			vertical: false
			count: 6
			mask: root.escape_ss ? "white" : "transparent"
			element_scale: chapter_scale
			texture: "collapsed-door"
			group: root.escape_ss ? groups.wall : 0
		}
		Tiles
		{
			id: corridor_1_1
			z: altitudes.background_near
			scale: chapter_scale
			mask: "white"
			tiles: create_tiles([
				[60,64,'n','dirt'],
				[60,73,'e','dirt'],
				[56,73,'s','dirt'],
				[56,68,'w','dirt'],
				[54,68,'s','dirt'],
				[54,65,'w',''],
				[54,64,'w','dirt'],
				]).concat(
				fill(55,65,59,67,'dirt'),
				fill(57,68,59,72,'dirt'),
				[[54,65,'chapter-2/dirt/door/v4ne'],
				[54,66,'chapter-2/dirt/door/v4e'],
				[54,67,'chapter-2/dirt/door/v4se']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(56*2-1,70*2-1), Qt.point(60*2+1,70*2-1), Qt.point(60*2+1,72*2+1), Qt.point(56*2-1,72*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Tiles
		{
			id: corridor_1_2
			z: altitudes.background_near
			scale: chapter_scale
			mask: root.escape_ss && open_room_2 ? "white":"transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_1_2 = 1
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_1_2 = 1
					mask = "white"
				}
			}
			tiles: create_tiles([
				[52,64,'n',''],
				[53,64,'n','metal'],
				[53,67,'e',''],
				[53,68,'e','metal'],
				[49,68,'s','metal'],
				[49,65,'w',''],
				[49,64,'w','metal']
				]).concat(
				fill(50,65,52,67,'metal'),
				[[49,65,'chapter-2/metal/door/v4ne'],
				[49,66,'chapter-2/metal/door/v4e'],
				[49,67,'chapter-2/metal/door/v4se']],
				[[53,65,'chapter-2/metal/door/v4nw'],
				[53,66,'chapter-2/metal/door/v4w'],
				[53,67,'chapter-2/metal/door/v4sw']],
				[[51,64,'chapter-2/metal/c'],
				[50,64,'chapter-2/metal/nwi'],
				[52,64,'chapter-2/metal/nei']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(49*2-1,64*2-1), Qt.point(53*2+1,64*2-1), Qt.point(53*2+1,68*2+1), Qt.point(49*2-1,68*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (53*2+1)*chapter_scale
			position.y: (65*2+2)*chapter_scale
			vertical: true
			door_scale: chapter_scale
			data: [["dirt", corridor_1_1], ["metal", corridor_1_2]]
		}
		Tiles
		{
			id: corridor_1_3
			z: altitudes.background_near
			scale: chapter_scale
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_1_3 = 1
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_1_3 = 1
					mask = "white"
				}
			}

			tiles: create_tiles([
				[18,51,'n',''],
				[19,51,'n','dirt'],
				[19,64,'e','dirt'],
				[48,64,'n','dirt'],
				[48,67,'e',''],
				[48,68,'e','dirt'],
				[15,68,'s','dirt'],
				[15,51,'w','dirt'],
				]).concat(
				fill(16,52,18,67,'dirt'),
				fill(19,65,47,67,'dirt'),
				[[48,65,'chapter-2/dirt/door/v4nw'],
				[48,66,'chapter-2/dirt/door/v4w'],
				[48,67,'chapter-2/dirt/door/v4sw']],
				[[16,51,'chapter-2/dirt/door/h4sw'],
				[17,51,'chapter-2/dirt/door/h4s'],
				[18,51,'chapter-2/dirt/door/h4se']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(15*2-1,51*2-1), Qt.point(19*2+1,51*2-1), Qt.point(19*2+1,68*2+1), Qt.point(15*2-1,68*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(20*2-1,64*2-1), Qt.point(48*2+1,64*2-1), Qt.point(48*2+1,68*2+1), Qt.point(20*2-1,68*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (48*2+1)*chapter_scale
			position.y: (65*2+2)*chapter_scale
			vertical: true
			door_scale: chapter_scale
			data: [["metal", corridor_1_2], ["dirt", corridor_1_3]]
		}
		Tiles
		{
			id: corridor_1_4
			z: altitudes.background_near
			scale: chapter_scale
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_1_4 = 1
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_1_4 = 1
					mask = "white"
				}
			}
			tiles: create_tiles([
				[18,35,'n',''],
				[19,35,'n','dirt'],
				[19,50,'e','dirt'],
				[16,50,'s',''],
				[15,50,'s','dirt'],
				[15,35,'w','dirt'],
				]).concat(
				fill(16,36,18,49,'dirt'),
				[[16,50,'chapter-2/dirt/door/h4nw'],
				[17,50,'chapter-2/dirt/door/h4n'],
				[18,50,'chapter-2/dirt/door/h4ne']],
				[[16,35,'chapter-2/dirt/door/h4sw'],
				[17,35,'chapter-2/dirt/door/h4s'],
				[18,35,'chapter-2/dirt/door/h4se']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(15*2-1,35*2-1), Qt.point(19*2+1,35*2-1), Qt.point(19*2+1,50*2+1), Qt.point(15*2-1,50*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (16*2+2)*chapter_scale
			position.y: (50*2+1)*chapter_scale
			door_scale: chapter_scale
			data: [["dirt", corridor_1_4], ["dirt", corridor_1_3]]
		}
		Tiles
		{
			id: corridor_1_5
			z: altitudes.background_near
			scale: chapter_scale
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_1_5 = 1
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_1_5 = 1
					mask = "white"
				}
			}
			tiles: create_tiles([
				[19,11,'n','dirt'],
				[19,14,'e',''],
				[19,34,'e','dirt'],
				[16,34,'s',''],
				[15,34,'s','dirt'],
				[15,11,'w','dirt'],
				]).concat(
				fill(16,12,18,33,'dirt'),
				[[19,12,'chapter-2/dirt/door/v4nw'],
				[19,13,'chapter-2/dirt/door/v4w'],
				[19,14,'chapter-2/dirt/door/v4sw']],
				[[16,34,'chapter-2/dirt/door/h4nw'],
				[17,34,'chapter-2/dirt/door/h4n'],
				[18,34,'chapter-2/dirt/door/h4ne']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(15*2-1,11*2-1), Qt.point(19*2+1,11*2-1), Qt.point(19*2+1,34*2+1), Qt.point(15*2-1,34*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (16*2+2)*chapter_scale
			position.y: (34*2+1)*chapter_scale
			door_scale: chapter_scale
			data: [["dirt", corridor_1_5], ["dirt", corridor_1_4]]
		}
		Tiles
		{
			id: room_1
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_room_1 = 1
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_room_1 = 1
					mask = "white"
				}
			}
			scale: chapter_scale
			tiles: create_tiles([
				[34,11,'n','metal'],
				[34,40,'e','metal'],
				[31,40,'s',''],
				[20,40,'s','metal'],
				[20,15,'w','metal'],
				[20,12,'w',''],
				[20,11,'w','metal'],
				]).concat(
				fill(21,12,33,39,'metal'),
				[[20,12,'chapter-2/metal/door/v4ne'],
				[20,13,'chapter-2/metal/door/v4e'],
				[20,14,'chapter-2/metal/door/v4se']],
				[[31,40,'chapter-2/metal/door/h4nw'],
				[32,40,'chapter-2/metal/door/h4n'],
				[33,40,'chapter-2/metal/door/h4ne']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(20*2-1,11*2-1), Qt.point(34*2+1,11*2-1), Qt.point(34*2+1,40*2+1), Qt.point(20*2-1,40*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			/* Pour une porte verticale:
				x*2+1 =>
				*2 car une tuile fait 2 de largeur et
				+1 car la première tuile est en y = -1
				y*2+2 =>
				*2 car une tuile fait 2 de largeur et
				+1 car la première tuile est en y = -2
			*/
			position.x: (19*2+1)*chapter_scale
			position.y: (12*2+2)*chapter_scale
			vertical: true
			door_scale: chapter_scale
			data: [["metal", room_1], ["dirt", corridor_1_5]]
		}
		Tiles
		{
			id: corridor_2
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_2 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_2 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[30,41,'n','dirt'],
				[33,41,'n',''],
				[34,41,'n','dirt'],
				[34,45,'e','dirt'],
				[24,45,'s','dirt'],
				[24,59,'e','dirt'],
				[34,59,'n','dirt'],
				[34,62,'e',''],
				[34,63,'e','dirt'],
				[20,63,'s','dirt'],
				[20,41,'w','dirt'],
				]).concat(
				fill(21,42,33,44,'dirt'),
				fill(21,45,23,62,'dirt'),
				fill(24,60,33,62,'dirt'),
				[[34,60,'chapter-2/dirt/door/v4nw'],
				[34,61,'chapter-2/dirt/door/v4w'],
				[34,62,'chapter-2/dirt/door/v4sw']],
				[[31,41,'chapter-2/dirt/door/h4sw'],
				[32,41,'chapter-2/dirt/door/h4s'],
				[33,41,'chapter-2/dirt/door/h4se']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(20*2-1,41*2-1), Qt.point(34*2+1,41*2-1), Qt.point(34*2+1,45*2+1), Qt.point(20*2-1,45*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(20*2-1,46*2-1), Qt.point(24*2+1,46*2-1), Qt.point(24*2+1,63*2+1), Qt.point(20*2-1,63*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(25*2-1,59*2-1), Qt.point(34*2+1,59*2-1), Qt.point(34*2+1,63*2+1), Qt.point(25*2-1,63*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			/* Pour une porte horizontale:
				x*2+2 =>
				*2 car une tuile fait 2 de largeur et
				+2 car la première tuile est en x = -2
				y*2+1 =>
				*2 car une tuile fait 2 de largeur et
				+1 car la première tuile est en y = -1
			*/
			position.x: (31*2+2)*chapter_scale
			position.y: (40*2+1)*chapter_scale
			//position.x: 31 64
			//position.y: 40 81
			door_scale: chapter_scale
			data: [["metal", room_1], ["dirt", corridor_2]]
		}
		Tiles
		{
			id: room_2
			z: altitudes.background_near
			scale: chapter_scale
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_room_2 = 1
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_room_2 = 1
					mask = "white"
				}
			}
			tiles: create_tiles([
				[56,41,'n','metal'],
				[59,41,'n',''],
				[60,41,'n','metal'],
				[60,63,'e','metal'],
				[53,63,'s','metal'],
				[50,63,'s',''],
				[35,63,'s','metal'],
				[35,60,'w',''],
				[35,54,'w','metal'],
				[35,51,'w',''],
				[35,41,'w','metal'],
				]).concat(
				fill(36,42,59,62,'metal'),
				[[35,51,'chapter-2/metal/door/v4ne'],
				[35,52,'chapter-2/metal/door/v4e'],
				[35,53,'chapter-2/metal/door/v4se']],
				[[35,60,'chapter-2/metal/door/v4ne'],
				[35,61,'chapter-2/metal/door/v4e'],
				[35,62,'chapter-2/metal/door/v4se']],
				[[57,41,'chapter-2/metal/door/h4sw'],
				[58,41,'chapter-2/metal/door/h4s'],
				[59,41,'chapter-2/metal/door/h4se']],
				[[51,63,'chapter-2/metal/c'],
				[50,63,'chapter-2/metal/swi'],
				[52,63,'chapter-2/metal/sei']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(35*2-1,41*2-1), Qt.point(60*2+1,41*2-1), Qt.point(60*2+1,63*2+1), Qt.point(35*2-1,63*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (34*2+1)*chapter_scale
			position.y: (60*2+2)*chapter_scale
			vertical: true
			door_scale: chapter_scale
			data: [["metal", room_2],["dirt", corridor_2]]
		}
		Tiles
		{
			id: room_3
			z: altitudes.background_near
			scale: chapter_scale
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_room_3 = 1
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_room_3 = 1
					mask = "white"
				}
			}
			tiles: create_tiles([
				[34,46,'n','dirt'],
				[34,50,'e','dirt'],
				[34,53,'e',''],
				[34,58,'e','dirt'],
				[25,58,'s','dirt'],
				[25,46,'w','dirt'],
				]).concat(
				fill(26,47,33,57,'dirt'),
				[[34,51,'chapter-2/dirt/door/v4nw'],
				[34,52,'chapter-2/dirt/door/v4w'],
				[34,53,'chapter-2/dirt/door/v4sw']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(25*2-1,46*2-1), Qt.point(34*2+1,46*2-1), Qt.point(34*2+1,58*2+1), Qt.point(25*2-1,58*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (34*2+1)*chapter_scale
			position.y: (51*2+2)*chapter_scale
			vertical: true
			door_scale: chapter_scale
			data: [["metal", room_2],["dirt", room_3]]
		}
		Tiles
		{
			id: corridor_3_1
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_3_1 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_3_1 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[73,9,'n','dirt'],
				[73,18,'e','dirt'],
				[73,19,'e',''],
				[69,19,'s',''],
				[69,13,'w','dirt'],
				[56,13,'s','dirt'],
				[53,13,'s',''],
				[39,13,'s','dirt'],
				[39,36,'e','dirt'],
				[60,36,'n','dirt'],
				[60,40,'e','dirt'],
				[57,40,'s',''],
				[35,40,'s','dirt'],
				[35,9,'w','dirt'],
				]).concat(
				fill(36,37,59,39,'dirt'),
				fill(36,10,38,36,'dirt'),
				fill(39,10,72,12,'dirt'),
				fill(70,13,72,19,'dirt'),
				[[57,40,'chapter-2/dirt/door/h4nw'],
				[58,40,'chapter-2/dirt/door/h4n'],
				[59,40,'chapter-2/dirt/door/h4ne']],
				[[53,13,'chapter-2/dirt/door/h4nw'],
				[54,13,'chapter-2/dirt/door/h4n'],
				[55,13,'chapter-2/dirt/door/h4ne']],
				[[69,19,'chapter-2/dirt/w'],
				[73,19,'chapter-2/dirt/e']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(35*2-1,36*2-1), Qt.point(60*2+1,36*2-1), Qt.point(60*2+1,40*2+1), Qt.point(35*2-1,40*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(35*2-1,9*2-1), Qt.point(39*2+1,9*2-1), Qt.point(39*2+1,35*2+1), Qt.point(35*2-1,35*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(40*2-1,9*2-1), Qt.point(73*2+1,9*2-1), Qt.point(73*2+1,13*2+1), Qt.point(40*2-1,13*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(69*2-1,14*2-1), Qt.point(73*2+1,14*2-1), Qt.point(73*2+1,19*2+1), Qt.point(69*2-1,19*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Tiles
		{
			id: corridor_3_2
			z: altitudes.background_near
			mask: "transparent"
			property bool barrier_open: root.generator_room_4_destroyed
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_3_2 = 1
			onBarrier_openChanged:
			{
				if(barrier_open)
				{
					root.open_corridor_3_2 = 1
					mask = "white"
				}
			}
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_3_2 = 1
					mask = "white"
				}
			}
			scale: chapter_scale
			tiles:create_tiles([
				[73,20,'n',''],
				[73,40,'e','dirt'],
				[65,40,'s','dirt'],
				[65,41,'e','dirt'],
				[65,44,'e',''],
				[65,64,'e','dirt'],
				[65,67,'e',''],
				[65,68,'e','dirt'],
				[61,68,'s','dirt'],
				[61,36,'w','dirt'],
				[69,36,'n','dirt'],
				[69,21,'w','dirt'],
				[69,20,'w',''],
				]).concat(
				fill(70,20,72,39,'dirt'),
				fill(62,37,69,39,'dirt'),
				fill(62,40,64,67,'dirt'),
				[[65,42,'chapter-2/dirt/door/v4nw'],
				[65,43,'chapter-2/dirt/door/v4w'],
				[65,44,'chapter-2/dirt/door/v4sw']],
				[[65,65,'chapter-2/dirt/door/v4nw'],
				[65,66,'chapter-2/dirt/door/v4w'],
				[65,67,'chapter-2/dirt/door/v4sw']],
				[[69,20,'chapter-2/dirt/w'],
				[73,20,'chapter-2/dirt/e']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(69*2-1,20*2-1), Qt.point(73*2+1,20*2-1), Qt.point(73*2+1,40*2+1), Qt.point(69*2-1,40*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(61*2-1,36*2-1), Qt.point(68*2+1,36*2-1), Qt.point(68*2+1,40*2+1), Qt.point(61*2-1,40*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(61*2-1,41*2-1), Qt.point(65*2+1,41*2-1), Qt.point(65*2+1,68*2+1), Qt.point(61*2-1,68*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (57*2+2)*chapter_scale
			position.y: (40*2+1)*chapter_scale
			door_scale: chapter_scale
			data: [["dirt", corridor_3_1], ["metal", room_2]]
		}
		Tiles
		{
			id: room_4
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_room_4 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_room_4 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[52,14,'n','metal'],
				[55,14,'n',''],
				[68,14,'n','metal'],
				[68,35,'e','metal'],
				[40,35,'s','metal'],
				[40,14,'w','metal'],
				]).concat(
				fill(41,15,67,34,'metal'),
				[[53,14,'chapter-2/metal/door/h4sw'],
				[54,14,'chapter-2/metal/door/h4s'],
				[55,14,'chapter-2/metal/door/h4se']])
			PolygonCollider
			{
				vertexes: [Qt.point(40*2-1,14*2-1), Qt.point(68*2+1,14*2-1), Qt.point(68*2+1,35*2+1), Qt.point(40*2-1,35*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (53*2+2)*chapter_scale
			position.y: (13*2+1)*chapter_scale
			door_scale: chapter_scale
			data: [["dirt", corridor_3_1], ["metal", room_4]]
		}
		Tiles
		{
			id: room_5
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_room_5 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_room_5 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[73,41,'n','metal'],
				[73,48,'e','metal'],
				[66,48,'s','metal'],
				[66,45,'w','metal'],
				[66,42,'w',''],
				[66,41,'w','metal'],
				]).concat(
				fill(67,42,72,47,'metal'),
				[[66,42,'chapter-2/metal/door/v4ne'],
				[66,43,'chapter-2/metal/door/v4e'],
				[66,44,'chapter-2/metal/door/v4se']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(66*2-1,41*2-1), Qt.point(73*2+1,41*2-1), Qt.point(73*2+1,48*2+1), Qt.point(66*2-1,48*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (65*2+1)*chapter_scale
			position.y: (42*2+2)*chapter_scale
			vertical: true
			door_scale: chapter_scale
			data: [["metal", room_5], ["dirt", corridor_3_2]]
		}
		Tiles
		{
			id: room_6
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_room_6 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_room_6 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[74,49,'n','metal'],
				[77,49,'n',''],
				[98,49,'n','metal'],
				[98,52,'e','metal'],
				[98,64,'e',''],
				[98,68,'e','metal'],
				[66,68,'s','metal'],
				[66,65,'w',''],
				[66,49,'w','metal'],
				]).concat(
				fill(67,50,97,67,'metal'),
				fill(98,54,98,63,'metal'),
				[[66,65,'chapter-2/metal/door/v4ne'],
				[66,66,'chapter-2/metal/door/v4e'],
				[66,67,'chapter-2/metal/door/v4se']],
				[[75,49,'chapter-2/metal/door/h4sw'],
				[76,49,'chapter-2/metal/door/h4s'],
				[77,49,'chapter-2/metal/door/h4se']],
				[[98,53,'chapter-2/metal/nei'],
				[98,64,'chapter-2/metal/sei']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(66*2-1,49*2-1), Qt.point(98*2+1,49*2-1), Qt.point(98*2+1,68*2+1), Qt.point(66*2-1,68*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Tiles
		{
			id: room_boss
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_room_boss = 1
			scale: chapter_scale
			tiles:create_tiles([
				[128,49,'n','metal'],
				[128,71,'e','metal'],
				[99,71,'s','metal'],
				[99,65,'w','metal'],
				[99,53,'w',''],
				[99,49,'w','metal'],
				]).concat(
				fill(99,54,99,63,'metal'),
				fill(100,50,127,70,'metal'),
				[[99,53,'chapter-2/metal/nwi'],
				[99,64,'chapter-2/metal/swi']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(98.5*2,48.5*2), Qt.point(128.5*2,48.5*2), Qt.point(128.5*2+1,71.5*2), Qt.point(98.5*2,71.5*2)]
				group: root.escape_ss? groups.sensor : groups.wall
				sensor: root.escape_ss
			}
			PolygonCollider
			{
				vertexes: [Qt.point(98.25*2,52.5*2), Qt.point(98.75*2,52.5*2), Qt.point(98.75*2,64.5*2), Qt.point(98.25*2,64.5*2)]
				group: groups.wall
			}
		}
		Door
		{
			position.x: (65*2+1)*chapter_scale
			position.y: (65*2+2)*chapter_scale
			vertical: true
			door_scale: chapter_scale
			data: [["metal", room_6], ["dirt", corridor_3_2]]
		}
		Tiles
		{
			id: corridor_4_1
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_4_1 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_4_1 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[116,44,'n','dirt'],
				[116,47,'e',''],
				[116,48,'e','dirt'],
				[78,48,'s','dirt'],
				[75,48,'s',''],
				[74,48,'s','dirt'],
				[74,44,'w','dirt'],
				]).concat(
				fill(75,45,115,47,'dirt'),
				[[116,45,'chapter-2/dirt/door/v4nw'],
				[116,46,'chapter-2/dirt/door/v4w'],
				[116,47,'chapter-2/dirt/door/v4sw']],
				[[75,48,'chapter-2/dirt/door/h4nw'],
				[76,48,'chapter-2/dirt/door/h4n'],
				[77,48,'chapter-2/dirt/door/h4ne']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(74*2-1,44*2-1), Qt.point(116*2+1,44*2-1), Qt.point(116*2+1,48*2+1), Qt.point(74*2-1,48*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (75*2+2)*chapter_scale
			position.y: (48*2+1)*chapter_scale
			door_scale: chapter_scale
			data: [["dirt", corridor_4_1], ["metal", room_6]]
		}
		Tiles
		{
			id: corridor_4_2
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_4_2 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_4_2 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[127,9,'n',''],
				[128,9,'n','dirt'],
				[128,48,'e','dirt'],
				[117,48,'s','dirt'],
				[117,45,'w',''],
				[117,44,'w','dirt'],
				[124,44,'n','dirt'],
				[124,9,'w','dirt'],
				]).concat(
				fill(118,45,127,47,'dirt'),
				fill(125,10,127,44,'dirt'),
				[[117,45,'chapter-2/dirt/door/v4ne'],
				[117,46,'chapter-2/dirt/door/v4e'],
				[117,47,'chapter-2/dirt/door/v4se']],
				[[125,9,'chapter-2/dirt/door/h4sw'],
				[126,9,'chapter-2/dirt/door/h4s'],
				[127,9,'chapter-2/dirt/door/h4se']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(117*2-1,44*2-1), Qt.point(128*2+1,44*2-1), Qt.point(128*2+1,48*2+1), Qt.point(117*2-1,48*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(124*2-1,9*2-1), Qt.point(128*2+1,9*2-1), Qt.point(128*2+1,43*2+1), Qt.point(124*2-1,43*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (116*2+1)*chapter_scale
			position.y: (45*2+2)*chapter_scale
			vertical: true
			door_scale: chapter_scale
			data: [["dirt", corridor_4_2], ["dirt", corridor_4_1]]
		}
		Tiles
		{
			id: corridor_4_3
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_4_3 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_4_3 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[128,1,'n','dirt'],
				[128,8,'e','dirt'],
				[125,8,'s',''],
				[124,8,'s','dirt'],
				[124,5,'w','dirt'],
				[102,5,'s','dirt'],
				[102,2,'w',''],
				[102,1,'w','dirt'],
				]).concat(
				fill(125,2,127,7,'dirt'),
				fill(103,2,124,4,'dirt'),
				[[102,2,'chapter-2/dirt/door/v4ne'],
				[102,3,'chapter-2/dirt/door/v4e'],
				[102,4,'chapter-2/dirt/door/v4se']],
				[[125,8,'chapter-2/dirt/door/h4nw'],
				[126,8,'chapter-2/dirt/door/h4n'],
				[127,8,'chapter-2/dirt/door/h4ne']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(102*2-1,1*2-1), Qt.point(128*2+1,1*2-1), Qt.point(128*2+1,5*2+1), Qt.point(102*2-1,5*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(124*2-1,6*2-1), Qt.point(128*2+1,6*2-1), Qt.point(128*2+1,8*2+1), Qt.point(124*2-1,8*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (125*2+2)*chapter_scale
			position.y: (8*2+1)*chapter_scale
			door_scale: chapter_scale
			data: [["dirt", corridor_4_3], ["dirt", corridor_4_2]]
		}
		Tiles
		{
			id: corridor_4_4
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_4_4 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_4_4 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[101,1,'n','dirt'],
				[101,4,'e',''],
				[101,5,'e','dirt'],
				[78,5,'s','dirt'],
				[78,27,'e','dirt'],
				[75,27,'s',''],
				[74,27,'s','dirt'],
				[74,1,'w','dirt'],
				]).concat(
				fill(75,2,100,4,'dirt'),
				fill(75,5,77,26,'dirt'),
				[[101,2,'chapter-2/dirt/door/v4nw'],
				[101,3,'chapter-2/dirt/door/v4w'],
				[101,4,'chapter-2/dirt/door/v4sw']],
				[[75,27,'chapter-2/dirt/door/h4nw'],
				[76,27,'chapter-2/dirt/door/h4n'],
				[77,27,'chapter-2/dirt/door/h4ne']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(74*2-1,1*2-1), Qt.point(101*2+1,1*2-1), Qt.point(101*2+1,5*2+1), Qt.point(74*2-1,5*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(74*2-1,6*2-1), Qt.point(78*2+1,6*2-1), Qt.point(78*2+1,27*2+1), Qt.point(74*2-1,27*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (101*2+1)*chapter_scale
			position.y: (2*2+2)*chapter_scale
			vertical: true
			door_scale: chapter_scale
			data: [["dirt", corridor_4_3], ["dirt", corridor_4_4]]
		}
		Tiles
		{
			id: corridor_4_5
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_4_5 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_4_5 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[77,28,'n',''],
				[78,28,'n','dirt'],
				[78,39,'e','dirt'],
				[119,39,'n','dirt'],
				[119,21,'w','dirt'],
				[122,21,'n',''],
				[123,21,'n','dirt'],
				[123,43,'e','dirt'],
				[74,43,'s','dirt'],
				[74,28,'w','dirt'],
				]).concat(
				fill(75,29,77,42,'dirt'),
				fill(78,40,122,42,'dirt'),
				fill(120,22,122,39,'dirt'),
				[[120,21,'chapter-2/dirt/door/h4sw'],
				[121,21,'chapter-2/dirt/door/h4s'],
				[122,21,'chapter-2/dirt/door/h4se']],
				[[75,28,'chapter-2/dirt/door/h4sw'],
				[76,28,'chapter-2/dirt/door/h4s'],
				[77,28,'chapter-2/dirt/door/h4se']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(74*2-1,28*2-1), Qt.point(78*2+1,28*2-1), Qt.point(78*2+1,43*2+1), Qt.point(74*2-1,43*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(79*2-1,39*2-1), Qt.point(123*2+1,39*2-1), Qt.point(123*2+1,43*2+1), Qt.point(79*2-1,43*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(119*2-1,21*2-1), Qt.point(123*2+1,21*2-1), Qt.point(123*2+1,38*2+1), Qt.point(119*2-1,38*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (75*2+2)*chapter_scale
			position.y: (27*2+1)*chapter_scale
			door_scale: chapter_scale
			data: [["dirt", corridor_4_4], ["dirt", corridor_4_5]]
		}
		Tiles
		{
			id: corridor_4_6
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_corridor_4_6 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_corridor_4_6 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[123,6,'n','dirt'],
				[123,20,'e','dirt'],
				[120,20,'s',''],
				[119,20,'s','dirt'],
				[119,10,'w','dirt'],
				[83,10,'s','dirt'],
				[83,34,'e','dirt'],
				[83,37,'e',''],
				[83,38,'e','dirt'],
				[79,38,'s','dirt'],
				[79,6,'w','dirt'],
				]).concat(
				fill(120,7,122,19,'dirt'),
				fill(80,7,119,9,'dirt'),
				fill(80,10,82,37,'dirt'),
				[[83,35,'chapter-2/dirt/door/v4nw'],
				[83,36,'chapter-2/dirt/door/v4w'],
				[83,37,'chapter-2/dirt/door/v4sw']],
				[[120,20,'chapter-2/dirt/door/h4nw'],
				[121,20,'chapter-2/dirt/door/h4n'],
				[122,20,'chapter-2/dirt/door/h4ne']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(119*2-1,6*2-1), Qt.point(123*2+1,6*2-1), Qt.point(123*2+1,20*2+1), Qt.point(119*2-1,20*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(79*2-1,6*2-1), Qt.point(118*2+1,6*2-1), Qt.point(118*2+1,10*2+1), Qt.point(79*2-1,10*2+1)]
				group: groups.sensor
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(79*2-1,11*2-1), Qt.point(83*2+1,11*2-1), Qt.point(83*2+1,38*2+1), Qt.point(79*2-1,38*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (120*2+2)*chapter_scale
			position.y: (20*2+1)*chapter_scale
			door_scale: chapter_scale
			data: [["dirt", corridor_4_6], ["dirt", corridor_4_5]]
		}
		Tiles
		{
			id: room_7
			z: altitudes.background_near
			mask: "transparent"
			onValueChanged: mask = "white"
			onMaskChanged: if(mask.a == 1) root.open_room_7 = 1
			scale: chapter_scale
			property bool open: root.escape_ss
			onOpenChanged:
			{
				if(open)
				{
					root.open_room_7 = 1
					mask = "white"
				}
			}
			tiles:create_tiles([
				[118,11,'n','metal'],
				[118,38,'e','metal'],
				[84,38,'s','metal'],
				[84,35,'w',''],
				[84,11,'w','metal'],
				]).concat(
				fill(85,12,117,37,'metal'),
				[[84,35,'chapter-2/metal/door/v4ne'],
				[84,36,'chapter-2/metal/door/v4e'],
				[84,37,'chapter-2/metal/door/v4se']]
				)
			PolygonCollider
			{
				vertexes: [Qt.point(84*2-1,11*2-1), Qt.point(118*2+1,11*2-1), Qt.point(118*2+1,38*2+1), Qt.point(84*2-1,38*2+1)]
				group: groups.sensor
				sensor: true
			}
		}
		Door
		{
			position.x: (83*2+1)*chapter_scale
			position.y: (35*2+2)*chapter_scale
			vertical: true
			door_scale: chapter_scale
			data: [["metal", room_7],["dirt", corridor_4_6]]
		}
		Ship
		{
			id: ship
			position.x: root.save_back ? 117*2*chapter_scale : 58*2*chapter_scale
			position.y: root.save_back ? 25.5*2*chapter_scale : 70*2*chapter_scale + 10*(1 - t0)
			Q.Component.onDestruction: game_over = true
		}
		Sensor
		{
			id: near_black_door_checkpoint
			type: Body.STATIC
			duration_in: 30
			duration_out: 1
			icon: 0
			icon_color: "cyan"
			property bool active: true
			PolygonCollider
			{
				vertexes: [ scene.coords(93,52), scene.coords(98,52), scene.coords(98,65), scene.coords(93,65)]
				group: near_black_door_checkpoint.active ? groups.sensor : 0
				sensor: active
			}
		}

		//permet d'ajuster les coordonnées avec le plan
		function coords(x,y){return Qt.point(x * 2 * chapter_scale, y * 2 * chapter_scale)}

		//generateurs salle 4 + barriere
		Vehicle
		{
			id: generators_room_4
			type: Body.STATIC
			property bool alive: equipment_1 || equipment_2
			onAliveChanged: if(!alive) {root.generator_room_4_destroyed = true, destroy()}
			PolygonCollider
			{
				vertexes: [	scene.coords(68.5,19.25), scene.coords(73.5,19.25), scene.coords(73.5,19.75), scene.coords(68.5,19.75) ]
				group: groups.wall
			}
			Repeater
			{
				count: 18
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (68.875+index/4)*2*chapter_scale
						position.y: 19.375*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI/2
						mask: open_corridor_3_1 ? Qt.rgba(0.58, 0, 0.82, 1) : "transparent"
					}
				}
			}
			Repeater
			{
				count: 18
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (68.875+index/4)*2*chapter_scale
						position.y: 19.625*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI/2
						mask: open_corridor_3_2 ? Qt.rgba(0.58, 0, 0.82, 1) : "transparent"
					}
				}
			}

			CircleCollider
			{
				group: equipment_1 ? groups.item_destructible : 0
				position: scene.coords(44.5, 29.5)
				radius: 2
			}
			EquipmentSlot
			{
				position: scene.coords(44.5, 29.5)
				equipment: Equipment
				{
					id: equipment_1
					health: max_health
					max_health: 2000
					Image
					{
						material: "chapter-2/generator-1"
						mask: Qt.rgba(1, equipment_1.health / equipment_1.max_health, equipment_1.health / equipment_1.max_health, room_4.mask.a)
						scale: 2
					}
				}
			}

			CircleCollider
			{
				group: equipment_2 ? groups.item_destructible : 0
				position: scene.coords(64.5, 29.5)
				radius: 2
			}
			EquipmentSlot
			{
				position: scene.coords(64.5, 29.5)
				equipment: Equipment
				{
					id: equipment_2
					health: max_health
					max_health: 2000
					Image
					{
						material: "chapter-2/generator-1"
						mask: Qt.rgba(1, equipment_2.health / equipment_2.max_health, equipment_2.health / equipment_2.max_health, room_4.mask.a)
						scale: 2
					}
				}
			}
		}

		//generateurs salle 7
		Vehicle
		{
			id: generators_room_7
			type: Body.STATIC
			property bool alive: big_generator
			onAliveChanged: if(!alive) {root.generator_room_7_destroyed = true, destroy()}
			PolygonCollider
			{
				vertexes: [	scene.coords(50-0.3105,63.5-0.25), scene.coords(52.5-0.3105,63.5-0.25), scene.coords(52.5-0.3105,63.5+0.25), scene.coords(50-0.3105,63.5+0.25)]
				group: groups.enemy_invincible
			}
			Repeater
			{
				count: 10
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (49.875+index/4)*2*chapter_scale
						position.y: 63.375*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI/2
						mask: open_room_2 ? Qt.rgba(0.58, 0, 0.82, 1) : "transparent"
					}
				}
			}
			Repeater
			{
				count: 10
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (49.875+index/4)*2*chapter_scale
						position.y: 63.625*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI/2
						mask: open_corridor_1_2 ? Qt.rgba(0.58, 0, 0.82, 1) : "transparent"
					}
				}
			}
			Repeater
			{
				count: 46
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: 98.375*2*chapter_scale
						position.y: (52.875+index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: open_room_6 ? Qt.rgba(0.58, 0, 0.82, 1) : "transparent"
					}
				}
			}
			// En théorie ces images là ne devrait pas être nécessaires car présentes dans la salle du boss lorsque celle ci est inaccessible.
			Repeater
			{
				count: 46
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: 98.625*2*chapter_scale
						position.y: (52.875+index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: open_room_boss ? Qt.rgba(0.58, 0, 0.82, 1) : "transparent"
					}
				}
			}

			CircleCollider
			{
				group: big_generator ? groups.item_destructible : 0
				position: scene.coords(115, 25)
				radius: 6
			}
			EquipmentSlot
			{
				position: scene.coords(115, 25)
				equipment: Equipment
				{
					id: big_generator
					health: max_health
					max_health: 5000
					Image
					{
						material: "chapter-2/generator-2"
						mask: Qt.rgba(1, big_generator.health / big_generator.max_health, big_generator.health / big_generator.max_health, room_7.mask.a)
						scale: 6
					}
				}
			}
		}

		//barrière bouclier de la room 2
		Vehicle
		{
			id: barriere_room_2
			max_speed: 0
			type: Body.STATIC
			//de haut en bas
			property bool shield_1: true
			property bool shield_2: true
			property bool shield_3: true
			property bool shield_4: true
			property bool shield_5: true
			property bool shield_6: true
			readonly property bool alive: (shield_1 || shield_2 || shield_3 || shield_4 || shield_5 || shield_6) && !root.escape_ss
			onAliveChanged: if(!alive) destroy()
			Q.Component.onCompleted: shield = max_shield

			Repeater
			{
				count: 45
				Q.Component
				{
					Image
					{
						material: "chapter-2/shield-wall"
						position.x: 48.75*2*chapter_scale
						position.y: (41+index/2)*2*chapter_scale
						scale: 0.5*chapter_scale
						//mask: Qt.rgba(0, 0.4, 1, 1)
						mask: Qt.rgba(0, 0.4, 1, room_2.mask.a*barriere_room_2.shield / barriere_room_2.max_shield)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [ scene.coords(48.5,40.5), scene.coords(49,40.5), scene.coords(49,63.5), scene.coords(48.5,63.5)]
				group: barriere_room_2.shield ? groups.enemy_shield : 0
			}
			CircleCollider
			{
				group: barriere_room_2.shield_1 ? groups.enemy_hull_naked : 0
				position: scene.coords(51, 43.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(51, 43.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: if(barriere_room_2) barriere_room_2.shield_1 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_2.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_2.shield_2 ? groups.enemy_hull_naked : 0
				position: scene.coords(51, 46.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(51, 46.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: if(barriere_room_2) barriere_room_2.shield_2 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_2.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_2.shield_3 ? groups.enemy_hull_naked : 0
				position: scene.coords(51, 49.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(51, 49.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: if(barriere_room_2) barriere_room_2.shield_3 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_2.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_2.shield_4 ? groups.enemy_hull_naked : 0
				position: scene.coords(51, 52.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(51, 52.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: if(barriere_room_2) barriere_room_2.shield_4 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_2.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_2.shield_5 ? groups.enemy_hull_naked : 0
				position: scene.coords(51, 55.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(51, 55.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: if(barriere_room_2) barriere_room_2.shield_5 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_2.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_2.shield_6 ? groups.enemy_hull_naked : 0
				position: scene.coords(51, 58.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(51, 58.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: if(barriere_room_2) barriere_room_2.shield_6 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_2.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
		}

		//barrière bouclier de la room 4
		Vehicle
		{
			id: barriere_room_4
			max_speed: 0
			type: Body.STATIC
			//de haut en bas
			property bool shield_1: true
			property bool shield_2: true
			property bool shield_3: true
			property bool shield_4: true
			property bool shield_5: true
			property bool shield_6: true
			property bool shield_7: true
			readonly property bool alive: shield_1 || shield_2 || shield_3 || shield_4 || shield_5 || shield_6 || shield_7
			onAliveChanged: if(!alive) destroy()
			Q.Component.onCompleted: shield = max_shield

			Repeater
			{
				count: 57
				Q.Component
				{
					Image
					{
						material: "chapter-2/shield-wall"
						position.x: (40+index/2)*2*chapter_scale
						position.y: 25.25*2*chapter_scale
						scale: 0.5*chapter_scale
						//mask: Qt.rgba(0, 0.4, 1, 1)
						mask: Qt.rgba(0, 0.4, 1, room_4.mask.a*barriere_room_4.shield / barriere_room_4.max_shield)
						angle: Math.PI/2
					}
				}
			}
			PolygonCollider
			{
				vertexes: [ scene.coords(39.5,25), scene.coords(68.5,25), scene.coords(68.5,25.5), scene.coords(39.5,25.5)]
				group: barriere_room_4.shield ? groups.enemy_shield : 0
			}
			CircleCollider
			{
				group: barriere_room_4.shield_1 ? groups.enemy_hull_naked : 0
				position: scene.coords(45.5, 31.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(45.5, 31.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_4.shield_1 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_4.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_4.shield_2 ? groups.enemy_hull_naked : 0
				position: scene.coords(48.5, 31.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(48.5, 31.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_4.shield_2 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_4.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_4.shield_3 ? groups.enemy_hull_naked : 0
				position: scene.coords(51.5, 31.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(51.5, 31.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_4.shield_3 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_4.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_4.shield_4 ? groups.enemy_hull_naked : 0
				position: scene.coords(54.5, 31.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(54.5, 31.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_4.shield_4 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_4.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_4.shield_5 ? groups.enemy_hull_naked : 0
				position: scene.coords(57.5, 31.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(57.5, 31.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_4.shield_5 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_4.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_4.shield_6 ? groups.enemy_hull_naked : 0
				position: scene.coords(60.5, 31.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(60.5, 31.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_4.shield_6 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_4.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_4.shield_7 ? groups.enemy_hull_naked : 0
				position: scene.coords(63.5, 31.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(63.5, 31.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_4.shield_7 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_4.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
		}

		//barrière bouclier de la room 5
		Vehicle
		{
			id: barriere_room_5
			max_speed: 0
			type: Body.STATIC
			//de haut en bas
			property bool shield_1: true
			property bool shield_2: true
			property bool shield_3: true
			readonly property bool alive: (shield_1 || shield_2 || shield_3) && !root.escape_ss
			onAliveChanged: if(!alive) destroy()
			Q.Component.onCompleted: shield = max_shield

			Repeater
			{
				count: 15
				Q.Component
				{
					Image
					{
						material: "chapter-2/shield-wall"
						position.x: 69.25*2*chapter_scale
						position.y: (41+index/2)*2*chapter_scale
						scale: 0.5*chapter_scale
						//mask: Qt.rgba(0, 0.4, 1, 1)
						mask: Qt.rgba(0, 0.4, 1, room_5.mask.a*barriere_room_5.shield / barriere_room_5.max_shield)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [ scene.coords(69,40.5), scene.coords(69.5,40.5), scene.coords(69.5,48.5), scene.coords(69,48.5)]
				group: barriere_room_5.shield ? groups.enemy_shield : 0
			}
			CircleCollider
			{
				group: barriere_room_5.shield_1 ? groups.enemy_hull_naked : 0
				position: scene.coords(71.5, 42.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(71.5, 42.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: if(barriere_room_5) barriere_room_5.shield_1 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_5.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_5.shield_2 ? groups.enemy_hull_naked : 0
				position: scene.coords(71.5, 44.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(71.5, 44.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: if(barriere_room_5) barriere_room_5.shield_2 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_5.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_5.shield_3 ? groups.enemy_hull_naked : 0
				position: scene.coords(71.5, 46.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(71.5, 46.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: if(barriere_room_5) barriere_room_5.shield_3 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_5.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
		}

		//barrière bouclier de la room 6
		Vehicle
		{
			id: barriere_room_6
			max_speed: 0
			type: Body.STATIC
			//de haut en bas
			property bool shield_1: true
			property bool shield_2: true
			property bool shield_3: true
			property bool shield_4: true
			property bool shield_5: true
			property bool shield_6: true
			readonly property bool alive: shield_1 || shield_2 || shield_3 || shield_4 || shield_5 || shield_6
			onAliveChanged: if(!alive) destroy()
			Q.Component.onCompleted: shield = max_shield

			Repeater
			{
				count: 39
				Q.Component
				{
					Image
					{
						material: "chapter-2/shield-wall"
						position.x: 81.75*2*chapter_scale
						position.y: (49+index/2)*2*chapter_scale
						scale: 0.5*chapter_scale
						//mask: Qt.rgba(0, 0.4, 1, 1)
						mask: Qt.rgba(0, 0.4, 1, room_6.mask.a*barriere_room_6.shield / barriere_room_6.max_shield)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [ scene.coords(81.5,48.5), scene.coords(82,48.5), scene.coords(82,68.5), scene.coords(81.5,68.5)]
				group: barriere_room_6.shield ? groups.enemy_shield : 0
			}
			CircleCollider
			{
				group: barriere_room_6.shield_1 ? groups.enemy_hull_naked : 0
				position: scene.coords(85, 51.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(85, 51.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_6.shield_1 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_6.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_6.shield_2 ? groups.enemy_hull_naked : 0
				position: scene.coords(85, 54.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(85, 54.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_6.shield_2 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_6.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_6.shield_3 ? groups.enemy_hull_naked : 0
				position: scene.coords(85, 57.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(85, 57.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_6.shield_3 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_6.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_6.shield_4 ? groups.enemy_hull_naked : 0
				position: scene.coords(85, 60.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(85, 60.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_6.shield_4 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_6.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_6.shield_5 ? groups.enemy_hull_naked : 0
				position: scene.coords(85, 63.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(85, 63.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_6.shield_5 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_6.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_6.shield_6 ? groups.enemy_hull_naked : 0
				position: scene.coords(85, 66.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(85, 66.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_6.shield_6 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_6.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
		}

		//1er barrière bouclier de la room 7
		Vehicle
		{
			id: barriere_room_7_1
			max_speed: 0
			type: Body.STATIC
			//de haut en bas
			property bool shield_1: true
			property bool shield_2: true
			property bool shield_3: true
			property bool shield_4: true
			property bool shield_5: true
			property bool shield_6: true
			property bool shield_7: true
			property bool shield_8: true
			property bool shield_9: true
			readonly property bool alive: shield_1 || shield_2 || shield_3 || shield_4 || shield_5 || shield_6 || shield_7 || shield_8 || shield_9
			onAliveChanged: if(!alive) destroy()
			Q.Component.onCompleted: shield = max_shield

			Repeater
			{
				count: 55
				Q.Component
				{
					Image
					{
						material: "chapter-2/shield-wall"
						position.x: 95.75*2*chapter_scale
						position.y: (11+index/2)*2*chapter_scale
						scale: 0.5*chapter_scale
						//mask: Qt.rgba(0, 0.4, 1, 1)
						mask: Qt.rgba(0, 0.4, 1, room_7.mask.a*barriere_room_7_1.shield / barriere_room_7_1.max_shield)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [ scene.coords(95.5,10.5), scene.coords(96,10.5), scene.coords(96,38.5), scene.coords(95.5,38.5)]
				group: barriere_room_7_1.shield ? groups.enemy_shield : 0
			}
			CircleCollider
			{
				group: barriere_room_7_1.shield_1 ? groups.enemy_hull_naked : 0
				position: scene.coords(98, 13.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(98, 13.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_1.shield_1 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_1.shield_2 ? groups.enemy_hull_naked : 0
				position: scene.coords(98, 16.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(98, 16.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_1.shield_2 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_1.shield_3 ? groups.enemy_hull_naked : 0
				position: scene.coords(98, 19.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(98, 19.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_1.shield_3 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_1.shield_4 ? groups.enemy_hull_naked : 0
				position: scene.coords(98, 22.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(98, 22.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_1.shield_4 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_1.shield_5 ? groups.enemy_hull_naked : 0
				position: scene.coords(98, 25.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(98, 25.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_1.shield_5 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_1.shield_6 ? groups.enemy_hull_naked : 0
				position: scene.coords(98, 28.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(98, 28.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_1.shield_6 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_1.shield_7 ? groups.enemy_hull_naked : 0
				position: scene.coords(98, 31.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(98, 31.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_1.shield_7 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_1.shield_8 ? groups.enemy_hull_naked : 0
				position: scene.coords(98, 34.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(98, 34.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_1.shield_8 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_1.shield_9 ? groups.enemy_hull_naked : 0
				position: scene.coords(98, 37.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(98, 37.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_1.shield_9 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
		}

		//2ème barrière bouclier de la room 7
		Vehicle
		{
			id: barriere_room_7_2
			max_speed: 0
			type: Body.STATIC
			//de haut en bas
			property bool shield_1: true
			property bool shield_2: true
			property bool shield_3: true
			property bool shield_4: true
			property bool shield_5: true
			property bool shield_6: true
			property bool shield_7: true
			readonly property bool alive: shield_1 || shield_2 || shield_3 || shield_4 || shield_5 || shield_6 || shield_7
			onAliveChanged: if(!alive) destroy()
			Q.Component.onCompleted: shield = max_shield

			Repeater
			{
				count: 55
				Q.Component
				{
					Image
					{
						material: "chapter-2/shield-wall"
						position.x: 107.25*2*chapter_scale
						position.y: (11+index/2)*2*chapter_scale
						scale: 0.5*chapter_scale
						//mask: Qt.rgba(0, 0.4, 1, 1)
						mask: Qt.rgba(0, 0.4, 1, room_7.mask.a*barriere_room_7_2.shield / barriere_room_7_2.max_shield)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [ scene.coords(107,10.5), scene.coords(107.5,10.5), scene.coords(107.5,38.5), scene.coords(107,38.5)]
				group: barriere_room_7_2.shield ? groups.enemy_shield : 0
			}
			CircleCollider
			{
				group: barriere_room_7_2.shield_1 ? groups.enemy_hull_naked : 0
				position: scene.coords(109, 12.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(109, 12.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_2.shield_1 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_2.shield_2 ? groups.enemy_hull_naked : 0
				position: scene.coords(109, 16.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(109, 16.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_2.shield_2 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_2.shield_3 ? groups.enemy_hull_naked : 0
				position: scene.coords(109, 20.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(109, 20.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_2.shield_3 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_2.shield_4 ? groups.enemy_hull_naked : 0
				position: scene.coords(109, 24.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(109, 24.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_2.shield_4 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_2.shield_5 ? groups.enemy_hull_naked : 0
				position: scene.coords(109, 28.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(109, 28.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_2.shield_5 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_2.shield_6 ? groups.enemy_hull_naked : 0
				position: scene.coords(109, 32.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(109, 32.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_2.shield_6 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
			CircleCollider
			{
				group: barriere_room_7_2.shield_7 ? groups.enemy_hull_naked : 0
				position: scene.coords(109, 36.5)
				radius: 0.5
			}
			EquipmentSlot
			{
				position: scene.coords(109, 36.5)
				equipment: Shield
				{
					level: 2
					Q.Component.onDestruction: barriere_room_7_2.shield_7 = false
					image.mask: Qt.rgba(1, health / max_health, health / max_health, room_7.mask.a )
					image.material: "chapter-2/shield-generator"
					image.scale: 0.8
				}
			}
		}
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-2/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-2/"+mat+"/c"])
			}
		}
		return fill_tiles
	}
	Q.Component
	{
		id: clock_factory
		Clock {text: qsTr("escape %1").arg(timer.time.toFixed(1))}
	}
	StateMachine
	{
		active: game_running
		begin: state_story_0 //state_escape
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				guard: !root.save_back
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 2000}
				onTriggered: music.objectName = "chapter-2/mainss"
			}
			SignalTransition
			{
				targetState: state_escape_dialogue
				signal: state_story_0.propertiesAssigned
				guard: root.save_back
				onTriggered:
				{
					generators_room_7.alive = false
					messages.add("nectaire/normal", qsTr("room 7b 1"))
					messages.add("lycop/normal", qsTr("room 7b 2"))
					ship.position.x = 117 * 2 * chapter_scale
					ship.position.y = 25.5 * 2 * chapter_scale
					timer.time = 240
					timer.speed = -1
					root.escape_ss= true
					music.objectName = "chapter-2/fuite"
					mini_boss_factory.createObject(null, {scene: scene, position: scene.coords(51,61.5)})
					for(var i = 0; i < root.enemy_tab_retour.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_retour[i][0],angle: root.enemy_tab_retour[i][1] * Math.PI,weapon_choice: root.enemy_tab_retour[i][2],activate: root.enemy_tab_retour[i][3]})
					}
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_after_story_1
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					ship.position.x= 58*2*chapter_scale
					ship.position.y= 70*2*chapter_scale
					messages.add("lycop/normal", qsTr("story 1 1"))
					messages.add("nectaire/normal", qsTr("story 1 2"))
					messages.add("lycop/happy", qsTr("story 1 3"))
					messages.add("nectaire/normal", qsTr("story 1 4"))
					messages.add("lycop/normal", qsTr("story 1 5"))
					//saved_game.add_science(30)
					//saved_game.add_matter(5000)
				}
			}
		}
		State
		{
			id: state_after_story_1
			SignalTransition
			{
				targetState: state_before_escape
				signal: state_after_story_1.propertiesAssigned
				guard: !messages.has_unread
			}
		}
		State
		{
			id: state_before_escape
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: room_4_clean_dialogue
				signal: root.generator_room_4_destroyedChanged
				guard: root.generator_room_4_destroyed
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("room 4 1"))
					messages.add("lycop/normal", qsTr("room 4 2"))
					scene.corridor_3_2.mask = "white"
				}
			}
			SignalTransition
			{
				targetState: near_black_door_dialogue
				signal: near_black_door_checkpoint.value_changed
				guard: near_black_door_checkpoint.value
				onTriggered:
				{
					near_black_door_checkpoint.active = false
					messages.add("nectaire/normal", qsTr("room 6 1"))
					messages.add("lycop/normal", qsTr("room 6 2"))
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_1_3Changed
				guard: root.open_corridor_1_3
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_1_3.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_1_3[i][0],angle: root.enemy_tab_corridor_1_3[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_1_3[i][2],activate: root.enemy_tab_corridor_1_3[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_1_4Changed
				guard: root.open_corridor_1_4
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_1_4.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_1_4[i][0],angle: root.enemy_tab_corridor_1_4[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_1_4[i][2],activate: root.enemy_tab_corridor_1_4[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_1_5Changed
				guard: root.open_corridor_1_5
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_1_5.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_1_5[i][0],angle: root.enemy_tab_corridor_1_5[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_1_5[i][2],activate: root.enemy_tab_corridor_1_5[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_2Changed
				guard: root.open_corridor_2
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_2.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_2[i][0],angle: root.enemy_tab_corridor_2[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_2[i][2],activate: root.enemy_tab_corridor_2[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_3_1Changed
				guard: root.open_corridor_3_1
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_3_1.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_3_1[i][0],angle: root.enemy_tab_corridor_3_1[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_3_1[i][2],activate: root.enemy_tab_corridor_3_1[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_3_2Changed
				guard: root.open_corridor_3_2
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_3_2.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_3_2[i][0],angle: root.enemy_tab_corridor_3_2[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_3_2[i][2],activate: root.enemy_tab_corridor_3_2[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_4_1Changed
				guard: root.open_corridor_4_1
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_4_1.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_4_1[i][0],angle: root.enemy_tab_corridor_4_1[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_4_1[i][2],activate: root.enemy_tab_corridor_4_1[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_4_2Changed
				guard: root.open_corridor_4_2
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_4_2.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_4_2[i][0],angle: root.enemy_tab_corridor_4_2[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_4_2[i][2],activate: root.enemy_tab_corridor_4_2[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_4_3Changed
				guard: root.open_corridor_4_3
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_4_3.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_4_3[i][0],angle: root.enemy_tab_corridor_4_3[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_4_3[i][2],activate: root.enemy_tab_corridor_4_3[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_4_4Changed
				guard: root.open_corridor_4_4
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_4_4.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_4_4[i][0],angle: root.enemy_tab_corridor_4_4[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_4_4[i][2],activate: root.enemy_tab_corridor_4_4[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_4_5Changed
				guard: root.open_corridor_4_5
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_4_5.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_4_5[i][0],angle: root.enemy_tab_corridor_4_5[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_4_5[i][2],activate: root.enemy_tab_corridor_4_5[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_corridor_4_6Changed
				guard: root.open_corridor_4_6
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_corridor_4_6.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_corridor_4_6[i][0],angle: root.enemy_tab_corridor_4_6[i][1] * Math.PI,weapon_choice: root.enemy_tab_corridor_4_6[i][2],activate: root.enemy_tab_corridor_4_6[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_room_1Changed
				guard: root.open_room_1
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_room_1.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_room_1[i][0],angle: root.enemy_tab_room_1[i][1] * Math.PI,weapon_choice: root.enemy_tab_room_1[i][2],activate: root.enemy_tab_room_1[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_room_2Changed
				guard: root.open_room_2
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_room_2.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_room_2[i][0],angle: root.enemy_tab_room_2[i][1] * Math.PI,weapon_choice: root.enemy_tab_room_2[i][2],activate: root.enemy_tab_room_2[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_room_3Changed
				guard: root.open_room_3
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_room_3.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_room_3[i][0],angle: root.enemy_tab_room_3[i][1] * Math.PI,weapon_choice: root.enemy_tab_room_3[i][2],activate: root.enemy_tab_room_3[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_room_4Changed
				guard: root.open_room_4
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_room_4.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_room_4[i][0],angle: root.enemy_tab_room_4[i][1] * Math.PI,weapon_choice: root.enemy_tab_room_4[i][2],activate: root.enemy_tab_room_4[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_room_5Changed
				guard: root.open_room_5
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_room_5.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_room_5[i][0],angle: root.enemy_tab_room_5[i][1] * Math.PI,weapon_choice: root.enemy_tab_room_5[i][2],activate: root.enemy_tab_room_5[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: state_before_escape
				signal: root.open_room_6Changed
				guard: root.open_room_6
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_room_6.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_room_6[i][0],angle: root.enemy_tab_room_6[i][1] * Math.PI,weapon_choice: root.enemy_tab_room_6[i][2],activate: root.enemy_tab_room_6[i][3]})
					}
				}
			}
			SignalTransition
			{
				targetState: last_room_dialogue
				signal: root.open_room_7Changed
				guard: root.open_room_7
				onTriggered:
				{
					for(var i = 0; i < root.enemy_tab_room_7.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_room_7[i][0],angle: root.enemy_tab_room_7[i][1] * Math.PI,weapon_choice: root.enemy_tab_room_7[i][2],activate: root.enemy_tab_room_7[i][3]})
					}
					messages.add("nectaire/normal", qsTr("room 7a 1"))
					messages.add("lycop/normal", qsTr("room 7a 2"))
					messages.add("nectaire/normal", qsTr("room 7a 3"))
					messages.add("lycop/normal", qsTr("room 7a 4"))
				}
			}
			SignalTransition
			{
				targetState: state_escape_dialogue
				signal: root.generator_room_7_destroyedChanged
				guard: root.generator_room_7_destroyed
				onTriggered:
				{
					if(!save_back && mod_easy)
					{
						saved_game.set("save_back", true)
						scene_loader.save()
					}
					messages.add("nectaire/normal", qsTr("room 7b 1"))
					messages.add("lycop/normal", qsTr("room 7b 2"))
					timer.time = 240
					timer.speed = -1
					root.escape_ss= true
					music.objectName = "chapter-2/fuite"
					mini_boss_factory.createObject(null, {scene: scene, position: scene.coords(51,61.5)})
					for(var i = 0; i < root.enemy_tab_retour.length; ++i)
					{
						turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_retour[i][0],angle: root.enemy_tab_retour[i][1] * Math.PI,weapon_choice: root.enemy_tab_retour[i][2],activate: root.enemy_tab_retour[i][3]})
					}
					saved_game.add_science(1)
				}
			}
		}
		State {id: room_4_clean_dialogue; SignalTransition {targetState: state_before_escape; signal: room_4_clean_dialogue.propertiesAssigned; guard: !messages.has_unread}}
		State {id: near_black_door_dialogue; SignalTransition {targetState: state_before_escape; signal: near_black_door_dialogue.propertiesAssigned; guard: !messages.has_unread}}
		State {id: last_room_dialogue; SignalTransition {targetState: state_before_escape; signal: last_room_dialogue.propertiesAssigned; guard: !messages.has_unread}}
		State 
		{
			id: state_escape_dialogue 
			SignalTransition 
			{
				targetState: state_escape
				signal: state_escape_dialogue.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					root.sortie_on = true
				}
			}
		}

		State
		{
			id: state_escape
			AssignProperty {target: scene; property: "running"; value: true}
			onEntered: clock = clock_factory.createObject(screen_hud)
			onExited: clock.destroy()
			SignalTransition
			{
				targetState: state_after_escape
				signal: corridor_1_1.value_changed
				guard: corridor_1_1.value
				onTriggered:
				{
					saved_game.add_science(1)
				}
			}
			SignalTransition
			{
				targetState: state_game_over
				signal: timer.onTime_changed
				guard: timer.time <= 0
			}
			SignalTransition
			{
				targetState: state_hidden_boss_dialogue
				signal: room_boss.value_changed
				guard: !root.hidden_boss_reveal
				onTriggered:
				{
					root.sortie_on = false
					root.hidden_boss_reveal = true
					messages.add("nectaire/normal", qsTr("story 2 1"))
					messages.add("lycop/normal", qsTr("story 2 2"))
					messages.add("nectaire/normal", qsTr("story 2 3"))
					messages.add("lycop/normal", qsTr("story 2 4"))
					root.temps_restant = timer.time
					for(var i = 0; i < root.enemy_tab_hidden_boss.length; ++i)
					{
						tab_hidden_enemy.push(turret_factory.createObject(null, {scene: scene, position: root.enemy_tab_hidden_boss[i][0],angle: root.enemy_tab_hidden_boss[i][1] * Math.PI,weapon_choice: root.enemy_tab_hidden_boss[i][2],activate: root.enemy_tab_hidden_boss[i][3]}))
					}
					animation_hidden_enemy=move_turret_hidden_boss_factory.createObject(null, {parent: scene, tab_enemy: tab_hidden_enemy})
					hidden_boss_factory.createObject(null, {scene: scene, position: scene.coords(113.5,59.5)})
					music.objectName = "chapter-2/boss_hidden"
				}
			}
		}

		State {id: state_hidden_boss_dialogue; SignalTransition {targetState: state_hidden_boss; signal: state_hidden_boss_dialogue.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_hidden_boss
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_escape
				signal: one_boss_destroyed
				onTriggered:
				{
					root.sortie_on = true
					saved_game.set("secret_base_heter", true)
					platform.set_bool("secret-base-heter")
					timer.time = root.temps_restant
					timer.speed = -1
					messages.add("nectaire/normal", qsTr("story 3 1"))
					messages.add("lycop/normal", qsTr("story 3 2"))
					messages.add("nectaire/normal", qsTr("story 3 3"))
					music.objectName = "chapter-2/fuite"
					animation_hidden_enemy.fin = true
					saved_game.add_science(2)
				}
			}
		}

		State
		{
			id: state_after_escape
			SignalTransition
			{
				targetState: state_end
				signal: state_after_escape.propertiesAssigned
				onTriggered:
				{
					saved_game.set("file", "game/chapter-2/chapter-2-escape.qml")
					saved_game.set("travel/chapter-3/chapter-3", true)
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
		State
		{
			id: state_game_over
			SignalTransition
			{
				signal: state_game_over.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: game_over = true
			}
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["chapter-2/barrier-wall"])
		jobs.run(scene_view, "preload", ["chapter-2/boss-cache"])
		var a1 = ["dirt", "metal"]; for(var i1 in a1)
		{
			var a2 = ["c", "e", "n", "ne", "nei", "nw", "nwi", "s", "se", "sei", "sw", "swi", "w"]; for(var i2 in a2) jobs.run(scene_view, "preload", ["chapter-2/" + a1[i1] + "/" + a2[i2]])
			for(var i2 = 0; i2 < 5; ++i2)
			{
				var a3 = ["n", "ne", "nw", "s", "se", "sw"]; for(var i3 in a3) jobs.run(scene_view, "preload", ["chapter-2/" + a1[i1] + "/door/h" + i2 + a3[i3]])
				var a3 = ["e", "ne", "nw", "se", "sw", "w"]; for(var i3 in a3) jobs.run(scene_view, "preload", ["chapter-2/" + a1[i1] + "/door/v" + i2 + a3[i3]])
			}
		}
		var a4 = ["collapsed-corridor", "collapsed-door", "destroyed-wall"]; for(var i4 in a4)
		{
			var a5 = ["e", "ne", "nw", "w", "se", "sw"]; for(var i5 in a5) jobs.run(scene_view, "preload", ["chapter-2/" + a4[i4] + "/" + a5[i5]])
		}
		jobs.run(scene_view, "preload", ["chapter-2/generator-1"])
		jobs.run(scene_view, "preload", ["chapter-2/generator-2"])
		jobs.run(scene_view, "preload", ["chapter-2/shield-generator"])
		jobs.run(scene_view, "preload", ["chapter-2/shield-wall"])
		jobs.run(scene_view, "preload", ["chapter-2/square-turret"])
		jobs.run(scene_view, "preload", ["chapter-2/mini-boss-ss"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		jobs.run(music, "preload", ["chapter-2/mainss"])
		jobs.run(music, "preload", ["chapter-2/fuite"])
		jobs.run(music, "preload", ["chapter-2/boss_hidden"])
		groups.init(scene)
	}

	finalize: function()
	{
		if(timer)
		{
			timer.destroy()
		}
	}
}
