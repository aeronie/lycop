import QtQuick 2.0
import "qrc:///controls"

Label
{
	id: root
	color: "red"
	font.pointSize: 24
	font.capitalization: Font.MixedCase
	anchors.top: parent.top
	anchors.left: parent.left
	anchors.margins: 5
	SequentialAnimation
	{
		running: true
		loops: Animation.Infinite
		NumberAnimation {target: root; property: "opacity"; from: 0; to: 1; duration: 500; easing.type: Easing.InOutExpo}
		NumberAnimation {target: root; property: "opacity"; from: 1; to: 0; duration: 500; easing.type: Easing.InOutExpo}
	}
}
