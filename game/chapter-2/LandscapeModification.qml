import QtQuick 2.0 as Q
import aw.game 0.0

Body
{
	id: root
	property bool vertical: false
	property var element_scale
	property var count: 6
	property var texture
	property color mask
	property var group
	type: Body.KINEMATIC
	angle: vertical ? 0 : Math.PI / 2
	PolygonCollider
	{
		vertexes: [
			Qt.point(-0.621*element_scale, -(-0.6+count/2)*element_scale),
			Qt.point( 0.621*element_scale, -(-0.6+count/2)*element_scale),
			Qt.point( 0.621*element_scale, (-0.6+count/2)*element_scale),
			Qt.point(-0.621*element_scale, (-0.6+count/2)*element_scale)
			]
		group: root.group
	}
	Repeater
	{
		count: root.count
		Q.Component
		{
			Image
			{
				position.x: ((index % 2) * 2 - 1)*element_scale
				position.y: (1+2*(Math.floor(index / 2)-root.count/4))*element_scale
				material: getMaterial(index, texture, root.count)
				mask: root.mask
				scale: element_scale
				z: 0
			}
		}
	}
	function getMaterial(index,texture,count)
	{
		var tab = ["nw", "ne", "w", "e", "sw", "se"]
		var i = index
		if ( index > 3 )
		{
			if ( index < count-2 )
			{
				i = 2+index%2
			}
			else
			{
				i = 4+index%2
			}
		}
		var mat = "chapter-2/"+texture+"/"+tab[i]
		return mat
	}
}
