#include "Body.h"
#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {

struct Mine
: Body
, Observer<1> // timer
{
	AW_DECLARE_OBJECT_STUB(Mine)
	AW_DECLARE_PROPERTY_STORED(int, launch_time) = 0;
	AW_DECLARE_PROPERTY_STORED(int, activation_time) = 0;
	void update(const Observer<1>::Tag &) override;
	void update(const Observer<-1>::Tag &) override;
protected:
	Mine() = default;
	~Mine() = default;
};

}
}
