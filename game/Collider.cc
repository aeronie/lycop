#include <Box2D.h>

#include "Body.h"
#include "Collider.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

static void destroy_fixture(b2Fixture *fixture)
{
	Q_ASSERT(fixture);
	Q_ASSERT(fixture->GetBody());
	fixture->GetBody()->DestroyFixture(fixture);
}

}

Collider::Collider()
: fixture_(0, destroy_fixture)
{
	Observer<0>::connect(signal_parent_changed());
	Observer<-1>::connect(signal_ready());
}

void Collider::update(const Observer<0>::Tag &)
{
	Observer<-2>::connect(body() ? body()->signal_ready() : 0);
}

void Collider::update(const Observer<-1>::Tag &)
{
	update(Observer<-2>::Tag());
}

void Collider::update(const Observer<-2>::Tag &)
{
	if(body() && body()->body_ && qml_initialized_ && !fixture_)
	{
		initialize();
	}
}

void Collider::initialize(b2Shape *shape)
{
	Q_ASSERT(body());
	Q_ASSERT(body()->body_);
	Q_ASSERT(shape);
	shape->m_radius = transform_length(radius());
	b2FixtureDef def;
	def.shape = shape;
	def.density = density();
	def.friction = friction();
	def.restitution = restitution();
	def.filter.groupIndex = int16(group());
	def.userData = this;
	fixture_.reset(body()->body_->CreateFixture(&def));
	Q_ASSERT(fixture_);
}

void Collider::set_density(float const &density)
{
	if(density_ == density) return;
	density_ = density;
	if(fixture_) fixture_->SetDensity(density_);
	Q_EMIT density_changed(density_);
}

void Collider::set_friction(float const &friction)
{
	if(friction_ == friction) return;
	friction_ = friction;
	if(fixture_) fixture_->SetFriction(friction_);
	Q_EMIT friction_changed(friction_);
}

void Collider::set_restitution(float const &restitution)
{
	if(restitution_ == restitution) return;
	restitution_ = restitution;
	if(fixture_) fixture_->SetRestitution(restitution_);
	Q_EMIT restitution_changed(restitution_);
}

void Collider::set_radius(float const &radius)
{
	if(radius_ == radius) return;
	radius_ = radius;
	if(fixture_)
	{
		fixture_->GetShape()->m_radius = transform_length(radius_);
		fixture_->GetBody()->SetAwake(true);
	}
	Q_EMIT radius_changed(radius_);
}

/*

Il faut détruire et reconstruire le b2Fixture pour que Box2D appelle BeginContact et EndContact.
On copie toutes les propriétés de l'ancienne b2Fixture en changeant seulement groupIndex.
On retarde cette opération avec QueuedConnection parce que Box2D verrouille le monde pendant Step().

Attention, ça risque de mettre à jour des propriétés du b2Body, notamment sa masse.

*/
void Collider::set_group(int const &group)
{
	if(group_ == group) return;
	group_ = group;
	if(fixture_ && !QMetaObject::invokeMethod(this, "update_fixture", Qt::QueuedConnection)) Q_UNREACHABLE();
	Q_EMIT group_changed(group_);
}

void Collider::update_fixture()
{
	b2FixtureDef def;
	def.shape = fixture_->GetShape();
	def.userData = fixture_->GetUserData();
	def.friction = fixture_->GetFriction();
	def.restitution = fixture_->GetRestitution();
	def.density = fixture_->GetDensity();
	def.isSensor = fixture_->IsSensor();
	def.filter = fixture_->GetFilterData();
	def.filter.groupIndex = int16(group_);
	fixture_.reset(fixture_->GetBody()->CreateFixture(&def));
}

void Collider::update_transform()
{
	if(!fixture_) return;
	fixture_->GetShape()->m_radius = transform_length(radius_);
	fixture_->GetBody()->ResetMassData();
}

float Collider::transform_length(float a) const
{
	Q_ASSERT(body());
	return a * body()->scale();
}

b2Vec2 Collider::transform_point(b2Vec2 a) const
{
	Q_ASSERT(body());
	a *= body()->scale();
	return a;
}

Body *Collider::body() const
{
	Q_ASSERT(!parent() || dynamic_cast<Body *>(parent()));
	return static_cast<Body *>(parent());
}

void Collider::set_body(Body *const &body)
{
	if(parent() == body) return;
	setParent(body);
	Q_EMIT body_changed(body);
}

AW_DEFINE_PROPERTY_STORED(Collider, sensor)

}
}
