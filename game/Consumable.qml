import aw.game 0.0

Consumable
{
	property real energy: 0
	property real matter: 0
	available: window.energy >= energy && saved_game.matter >= matter
	onConsumed:
	{
		if(energy) window.energy -= energy
		if(matter) saved_game.add_matter(-matter)
	}
}
