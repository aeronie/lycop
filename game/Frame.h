#pragma once

#include <Common/b2Math.h>

#include "Object.h"
#include "meta.h"

namespace aw {
namespace game {

struct Frame
: Object
{
	Q_OBJECT
	AW_DECLARE_PROPERTY_STORED(b2Vec2, position) = b2Vec2_zero; ///< m
	AW_DECLARE_PROPERTY_STORED(float, angle) = 0; ///< rad
	AW_DECLARE_PROPERTY_STORED(float, scale) = 1;
};

}
}
