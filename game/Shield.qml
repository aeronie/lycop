import aw.game 0.0

Shield
{
	property int level: 1
	readonly property int time: body.time_before_shield_regeneration
	readonly property int period: body.max_time_before_shield_regeneration
	property var material_image: "equipments/shield-generator"
	health: max_health
	max_health: [0, 200, 300][level] +~~body.health_boost
	max_shield: [0, 500, 700][level]
	image: Image
	{
		material: material_image
		scale: 0.5
		mask: mask_equipment(parent)
	}
}
