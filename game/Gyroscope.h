#include "Equipment.h"

namespace aw {
namespace game {

struct Gyroscope
: Equipment
{
	AW_DECLARE_OBJECT_STUB(Gyroscope)
	AW_DECLARE_PROPERTY_STORED(float, multiplicateur) = 0;
protected:
	Gyroscope() = default;
	~Gyroscope() = default;
};

}
}
