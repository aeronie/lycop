#pragma once

#include "Vehicle.h"
#include "Scene.h"

namespace aw {
namespace game {

struct Behaviour
: Object
, Observer<0> // parent = body
, Observer<1> // scene
, Observer<2> // body ready
, Observer<-1> // timer
{
	Q_OBJECT
	AW_DECLARE_PROPERTY_STORED(bool, enabled) = true;
	void update(const Observer<0>::Tag &) override final;
	void update(const Observer<1>::Tag &) override final;
	void update(const Observer<2>::Tag &) override final;
protected:
	Behaviour();

	Vehicle *body() const
	{
		Q_ASSERT(!parent() || dynamic_cast<Vehicle *>(parent()));
		return static_cast<Vehicle *>(parent());
	}

	Scene *scene() const
	{
		return body() ? body()->scene() : 0;
	}
};

}
}
