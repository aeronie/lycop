import QtQuick 2.0 as Q
import aw.game 0.0

Body
{
	id: root
	property int x: 5
	property int y: 5
	property real z
	property string material
	property var center: scene_view.center
	property real shift_x: 0
	property real shift_y: 0
	property color mask: "white"
	readonly property real shift_x_: shift_x - Math.floor(shift_x / scale / 2) * scale * 2
	readonly property real shift_y_: shift_y - Math.floor(shift_y / scale / 2) * scale * 2
	type: Body.STATIC
	position.x: Math.floor(center.x / scale / 2) * scale * 2 + shift_x_
	position.y: Math.floor(center.y / scale / 2) * scale * 2 + shift_y_
	Repeater
	{
		count: root.x * root.y
		Q.Component
		{
			Image
			{
				position.x: 1 + 2 * (index % root.x) - root.x
				position.y: 1 + 2 * Math.floor(index / root.x) - root.y
				material: root.material
				z: root.z
				mask: root.mask
			}
		}
	}
}
