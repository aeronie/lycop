#pragma once

#include <QColor>
#include <QMatrix4x4>

#include "Frame.h"

namespace aw {
namespace game {

struct Image
: Frame
{
	AW_DECLARE_OBJECT_STUB(Image)
	AW_DECLARE_PROPERTY_STORED(QString, material);
	AW_DECLARE_PROPERTY_STORED(QColor, mask) = Qt::white;
	AW_DECLARE_PROPERTY_STORED(QMatrix4x4, transform);
	AW_DECLARE_PROPERTY_STORED(float, z) = 0;
protected:
	Image() = default;
	~Image();
public:
	Image **pointer_ = 0;
};

}
}
