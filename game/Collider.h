#pragma once

#include <Dynamics/b2Fixture.h>

#include "Object.h"
#include "meta.h"

namespace aw {
namespace game {

struct Body;

struct Collider
: Object
, Observer<0> // parent = body
, Observer<-1> // ready
, Observer<-2> // body ready
{
	Q_OBJECT
	AW_DECLARE_PROPERTY(aw::game::Body *, body)
	AW_DECLARE_PROPERTY_STORED(float, density) = 1; ///< kg m⁻²
	AW_DECLARE_PROPERTY_STORED(float, friction) = 0; ///< ∈ [0, 1]
	AW_DECLARE_PROPERTY_STORED(float, restitution) = 0; ///< ∈ [0, 1]
	AW_DECLARE_PROPERTY_STORED(bool, sensor) = false;
	AW_DECLARE_PROPERTY_STORED(float, radius) = 1; ///< m
	AW_DECLARE_PROPERTY_STORED(int, group) = 0;
	void update(const Observer<0>::Tag &) override;
	void update(const Observer<-1>::Tag &) override;
	void update(const Observer<-2>::Tag &) override;
	Q_SLOT void update_fixture();
	virtual void initialize() = 0;
	float transform_length(float) const;
protected:
	Collider();
	b2Vec2 transform_point(b2Vec2) const;
	void initialize(b2Shape *);
public:
	Q_SLOT virtual void update_transform();
	std::unique_ptr<b2Fixture, void(*)(b2Fixture *)> fixture_;
};

}
}
