#include "Image.h"
#include "meta.private.h"

namespace aw {
namespace game {

Image::~Image()
{
	if(pointer_) *pointer_ = 0; // il faut se désinscrire de la liste Scene::images_
}

AW_DEFINE_OBJECT_STUB(Image)
AW_DEFINE_PROPERTY_STORED(Image, material)
AW_DEFINE_PROPERTY_STORED(Image, mask)
AW_DEFINE_PROPERTY_STORED(Image, transform)
AW_DEFINE_PROPERTY_STORED(Image, z)

}
}
