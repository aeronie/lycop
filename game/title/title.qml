import QtQuick 2.0 as Q
import aw.game 0.0
import "qrc:///game"
import "qrc:///controls" as Q

Chapter
{
	main_menu: true
	splash: Splash
	{
		enabled: !saved_game.get("immediate")
		Q.Rectangle
		{
			z: 2000
			color: "white"
			Q.Image
			{
				anchors.fill: parent
				source: "///data/game/title/logo.png"
				fillMode: Q.Image.PreserveAspectFit
			}
		}
		Q.Image
		{
			z: 2000
			source: "///data/game/title/title-bg.png"
			fillMode: Q.Image.PreserveAspectCrop
			Q.Image
			{
				anchors.fill: parent
				source: "///data/game/title/title-fg.png"
				fillMode: Q.Image.PreserveAspectFit
			}
		}
	}
	readonly property alias ship: ship
	property var label: null
	property var tab_credit: tab_cre.tab_final
	scene: Scene
	{
		running: game_running
		Tab_credit
		{
			id: tab_cre
		}
		Background
		{
			z: -0.5
			scale: 10
			material: "vide"
		}
		Body
		{
			type: Body.STATIC
			scale: 20
			Image
			{
				material: "title"
				z: altitudes.background_near
			}
		}
		Ship
		{
			id: ship
		}
		
		Repeater
		{
			count: tab_credit.length
			Q.Component
			{
				Text
				{
					position.x: tab_credit[index][2]
					position.y: tab_credit[index][3]
					text: tab_credit[index][0]
					Image
					{
						material: tab_credit[index][1]
						mask: Qt.rgba(1, 1, 1, 0.25)
						z: -0.0001
					}
				}
			}
		}
	}
	Q.Component
	{
		id: label_factory
		Q.Label
		{
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
			font.capitalization: Q.Font.MixedCase
			enabled: false
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["title"])
		jobs.run(music, "preload", ["title/main"])
		jobs.run(music, "objectName", ["title/main"])
		for(var i in technologies.data) if(technologies.data[i].factory) jobs.run(scene_view, "preload", ["equipments/" + technologies.data[i].objectName])
		jobs.run(scene_view, "preload", ["folks/lycop"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		jobs.run(scene_view, "preload", ["explosion"])

		groups.init(scene)

		label = label_factory.createObject(screen_hud)
		finalize = function() {label.destroy()}
	}
}
