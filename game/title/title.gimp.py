# coding: utf-8
from gimpfu import *
pdb.gimp_gradients_refresh()
pdb.gimp_context_set_gradient('Golden')
pdb.gimp_context_set_antialias(True)
pdb.gimp_context_set_interpolation(INTERPOLATION_CUBIC)

# importe le PDF en gris pour économiser la mémoire
image = pdb.gimp_file_load('title.pdf', 'title.pdf')
layer = pdb.gimp_image_get_active_layer(image)
pdb.gimp_image_convert_grayscale(image)
pdb.gimp_layer_flatten(layer)

# creuse une rainure sur le contour
pdb.gimp_image_select_color(image, CHANNEL_OP_ADD, layer, (0, 0, 0))
pdb.gimp_selection_shrink(image, 6)
pdb.gimp_edit_fill(layer, BACKGROUND_FILL)
pdb.gimp_selection_shrink(image, 4)
pdb.gimp_edit_fill(layer, FOREGROUND_FILL)
pdb.gimp_selection_none(image)

# 1024×1024
pdb.gimp_image_scale(image, pdb.gimp_drawable_width(layer) / 4, pdb.gimp_drawable_height(layer) / 4)
pdb.gimp_image_resize(image, 1024, 1024, 512 - pdb.gimp_drawable_width(layer) / 2, 512 - pdb.gimp_drawable_height(layer) / 2)
pdb.gimp_layer_resize_to_image_size(layer)

# ajoute une copie floutée
layer2 = pdb.gimp_layer_copy(layer, False)
pdb.gimp_image_insert_layer(image, layer2, None, -1)
pdb.plug_in_gauss_iir2(image, layer2, 64, 64)
pdb.gimp_layer_set_mode(layer2, MULTIPLY_MODE)

# convertit le tout en masque
layer = pdb.gimp_image_merge_visible_layers(image, CLIP_TO_IMAGE)
pdb.gimp_invert(layer)
mask = pdb.gimp_layer_create_mask(layer, ADD_COPY_MASK)
pdb.gimp_layer_add_mask(layer, mask)

# repasse en RGB
pdb.gimp_image_convert_rgb(image)

# garde une copie pour plus tard
layer2 = pdb.gimp_layer_copy(layer, False)

# peint le dégradé
pdb.gimp_edit_blend(layer, CUSTOM_MODE, NORMAL_MODE, GRADIENT_LINEAR, 100, 0, REPEAT_NONE, False, False, 1, 0, True, 0, 896, 0, 128)
pdb.gimp_layer_remove_mask(layer, MASK_APPLY)

# exporte
pdb.file_png_save2(image, layer, 'colors.png', 'colors.png', False, 9, False, False, False, False, False, False, False)

# décale l'image pour faire du relief quand on colle l'ombre derrière
# on en colle 2 pour que ça soit plus sombre
pdb.gimp_image_resize(image, 1024, 1022, 0, -2)
pdb.gimp_image_insert_layer(image, layer2, None, 1)
pdb.gimp_drawable_fill(layer2, FOREGROUND_FILL)
layer3 = pdb.gimp_layer_copy(layer2, False)
pdb.gimp_image_insert_layer(image, layer3, None, 1)

# 1024×768
layer = pdb.gimp_image_merge_visible_layers(image, CLIP_TO_IMAGE)
pdb.gimp_image_resize(image, 1024, 768, 0, -126)
pdb.gimp_layer_resize_to_image_size(layer)

#exporte
pdb.file_png_save2(image, layer, 'title-fg.png', 'title-fg.png', False, 9, False, False, False, False, False, False, False)
pdb.gimp_quit(False)
