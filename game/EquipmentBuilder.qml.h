#include "Body.h"
#include "Equipment.h"
#include "EquipmentSlot.h"
#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct EquipmentBuilder
: Equipment
, Observer<1> // slot
, Observer<2> // body
, Observer<3> // scene
, Observer<4> // timer
{
	AW_DECLARE_OBJECT_STUB(EquipmentBuilder)
	AW_DECLARE_PROPERTY_STORED(QQmlComponent *, factory) = 0;
	AW_DECLARE_PROPERTY_STORED(int, slot_id) = -1;
	AW_DECLARE_PROPERTY_STORED(int, period) = 0;
	AW_DECLARE_PROPERTY_STORED_READONLY(int, time) = 0;

	void update(const Observer<1>::Tag &) override
	{
		Observer<2>::connect(slot() ? slot()->signal_body_changed() : 0);
	}

	void update(const Observer<2>::Tag &) override
	{
		Observer<3>::connect(body() ? body()->signal_scene_changed() : 0);
	}

	void update(const Observer<3>::Tag &) override
	{
		Observer<4>::connect(scene() ? scene()->signal_frame_begin() : 0);
	}

	void update(const Observer<4>::Tag &) override
	{
		if(!qml_initialized_)
		{
			return;
		}
		else if(time_ > 0)
		{
			--time_;
			Q_EMIT time_changed(time_);
		}
		else
		{
			QObject *equipment = factory_->beginCreate(factory_->creationContext());
			Q_ASSERT(dynamic_cast<Equipment *>(equipment));
			slot()->set_equipment(static_cast<Equipment *>(equipment));
			factory_->completeCreate();
			deleteLater();
		}
	}

protected:

	EquipmentBuilder()
	{
		Observer<1>::connect(signal_slot_changed());
	}
};

void EquipmentBuilder::set_period(int const &period)
{
	if(period_ == period) return;
	period_ = period;
	time_ = period_;
	Q_EMIT period_changed(period_);
	Q_EMIT time_changed(time_);
}

AW_DEFINE_OBJECT_STUB(EquipmentBuilder)
AW_DEFINE_PROPERTY_STORED(EquipmentBuilder, factory)
AW_DEFINE_PROPERTY_STORED(EquipmentBuilder, slot_id)

}
}
}
