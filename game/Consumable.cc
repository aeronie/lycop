#include "Consumable.h"
#include "meta.private.h"

namespace aw {
namespace game {

AW_DEFINE_OBJECT_STUB(Consumable)
AW_DEFINE_PROPERTY_STORED(Consumable, available)

}
}
