import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	property alias ship: ship
	property var camera_position: ship.position
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false
		Background
		{
			z: -0.5
			scale: 10
			material: "vide"
		}
		Ship
		{
			id: ship
			Q.Component.onDestruction: game_over = true
		}
	}
	StateMachine
	{
		begin: state_begin
		active: game_running
		State
		{
			id: state_begin
			SignalTransition
			{
				targetState: state_end
				signal: state_begin.propertiesAssigned
				onTriggered:
				{
					music.objectName = "chapter-1/main"
				}
			}
		}
		State
		{
			id: state_end
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: screen_menu.about_screen_travel; property: "blink"; value: true}
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(music, "preload", ["chapter-1/main"])
		groups.init(scene)
	}
}
