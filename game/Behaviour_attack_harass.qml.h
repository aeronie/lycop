#include "Behaviour_attack.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct Behaviour_attack_harass
: Behaviour_attack
{
	AW_DECLARE_OBJECT_STUB(Behaviour_attack_harass)
	AW_DECLARE_PROPERTY_STORED(float, target_distance) = 0;
	AW_DECLARE_PROPERTY_STORED(int, relaxation_time) = 0;
	int time_ = 0;

	void attack(b2Vec2 const &target) override
	{
		if(time_)
		{
			--time_;
		}
		else
		{
			body()->set_relative_velocity(b2Vec2(0, -1));
			b2Vec2 direction = target - body()->position();
			float distance = direction.Length();
			if(distance <= target_distance())
			{
				time_ = relaxation_time();
			}
			else
			{
				body()->set_target_direction(target_distance() / distance * b2Vec2(-direction.y, direction.x) + direction);
			}
		}
	}
};

AW_DEFINE_OBJECT_STUB(Behaviour_attack_harass)
AW_DEFINE_PROPERTY_STORED(Behaviour_attack_harass, target_distance)
AW_DEFINE_PROPERTY_STORED(Behaviour_attack_harass, relaxation_time)

}
}
}
