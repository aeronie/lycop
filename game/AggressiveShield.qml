import aw.game 0.0

AggressiveShield
{
	property int level: 1
	health: max_health
	max_health: 200 +~~body.health_boost
	damages: [0, 250, 500][level]
	image: Image
	{
		material: "equipments/aggressive-shield-generator"
		scale: 0.5
		mask: mask_equipment(parent)
	}
}
