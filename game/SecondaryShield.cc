#include "SecondaryShield.h"
#include "meta.private.h"

namespace aw {
namespace game {

AW_DEFINE_OBJECT_STUB(SecondaryShield)
AW_DEFINE_PROPERTY_STORED(SecondaryShield, max_shield)

}
}
