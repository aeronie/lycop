#include <cmath>

#include "Vehicle.h"
#include "Equipment.h"
#include "EquipmentSlot.h"
#include "Scene.h"
#include "Shield.h"
#include "SecondaryShield.h"
#include "Battery.h"
#include "Generator.h"
#include "Gyroscope.h"
#include "AggressiveShield.h"
#include "meta.private.h"
#include "AbsorbingShield.h"
#include "Dash.h"

namespace aw {
namespace game {
namespace {

/*

Nécessaire parce que std::isnan() ne fonctionne pas avec -ffast-math.
Fonctionne parce que même avec -ffast-math float est codé selon IEEE 754 binary32.

*/
static bool defined(float x)
{
	union {
	float in;
	int32_t out; // 1 bit de signe, 8 bits d'exposant, 23 bits de mantisse
	} a;
	a.in = x;
	return std::abs(a.out) <= 0X7F800000; // NAN a les 8 bits d'exposant et au moins 1 bit de mantisse
}

}

void Vehicle::update(Observer<-1>::Tag const &tag)
{
	Body::update(tag);

	if(body_)
	{
		Observer<1>::connect(scene()->signal_frame_begin());
	}
}

void Vehicle::update_equipments()
{
	float angular_speed_boost = 1;
	float speed_boost = 1;
	float max_shield = 0;
	float shield_regeneration = 0;
	float max_secondary_shield = 0;
	float max_energy = 500;
	float energy_regeneration = 30;
	float aggressive_shield_active_damages = 0;
	float absorbtion_damages_multiplier = 0;
	float absorbtion_shield_multiplier = 0;

	for(QObject *child: children())
	{
		if(EquipmentSlot *slot = static_qobject_cast<EquipmentSlot>(child))
		{
			if(!slot->equipment());
			else if(Shield *e = static_qobject_cast<Shield>(slot->equipment()))
			{
				max_shield += e->max_shield();
				shield_regeneration += e->shield_regeneration();
			}
			else if(SecondaryShield *e = static_qobject_cast<SecondaryShield>(slot->equipment()))
			{
				max_secondary_shield += e->max_shield();
			}
			else if(Battery *e = static_qobject_cast<Battery>(slot->equipment()))
			{
				max_energy += e->max_energy();
			}
			else if(Generator *e = static_qobject_cast<Generator>(slot->equipment()))
			{
				energy_regeneration += e->energy_regeneration();
			}
			else if(Gyroscope *e = static_qobject_cast<Gyroscope>(slot->equipment()))
			{
				angular_speed_boost += e->multiplicateur();
			}
			else if(AggressiveShield *e = static_qobject_cast<AggressiveShield>(slot->equipment()))
			{
				if(e->shooting())
				{
					aggressive_shield_active_damages += e->damages();
				}
			}
			else if(AbsorbingShield *e = static_qobject_cast<AbsorbingShield>(slot->equipment()))
			{
				if(e->shooting())
				{
					absorbtion_damages_multiplier = e->damages_multiplier();
					absorbtion_shield_multiplier = e->shield_multiplier();
				}
			}
			else if(Dash *e = static_qobject_cast<Dash>(slot->equipment()))
			{
				speed_boost += e->speed_amplification();
			}
		}
	}

	#define _(x) \
		if(x##_ != x) \
		{ \
			x##_ = x; \
			Q_EMIT x##_changed(x##_); \
		}
	_(angular_speed_boost)
	_(speed_boost)
	_(max_shield)
	_(shield_regeneration)
	_(max_secondary_shield)
	_(max_energy)
	_(energy_regeneration)
	_(aggressive_shield_active_damages)
	_(absorbtion_damages_multiplier)
	_(absorbtion_shield_multiplier)
	#undef _
}

void Vehicle::update(const Observer<1>::Tag &)
{
	update_equipments();

	// -- Déplacement

	if(defined(target_angle_))
	{
		set_target_angular_velocity(std::max(-1.f, std::min(1.f, float(std::fmod(std::fmod(target_angle_ - body_->GetAngle() + M_PI, 2 * M_PI) + 2 * M_PI, 2 * M_PI) - M_PI) / scene()->time_step() / (max_angular_speed() * angular_speed_boost_))));
	}
	{
		float error = body_->GetAngularVelocity() - previous_target_angular_velocity_;
		previous_target_angular_velocity_ = max_angular_speed() * angular_speed_boost_ * target_angular_velocity();
		body_->SetAngularVelocity(previous_target_angular_velocity_ + std::copysign(std::max(0.f, std::abs(error) - max_torque_), error));
	}
	if(body_->GetAngularVelocity())
	{
		if(defined(relative_velocity_.x))
		{
			set_target_velocity(body_->GetWorldVector(relative_velocity_));
		}
	}
	else
	{
		target_angle_ = NAN;
	}
	{
		b2Vec2 error = body_->GetLinearVelocity() - previous_target_velocity_;
		previous_target_velocity_ = speed_boost_mul() * max_speed() * speed_boost_ * target_velocity();
		if(error.Length() > max_force_) previous_target_velocity_ += (1 - max_force_ / error.Length()) * error;
		float s = previous_target_velocity_.LengthSquared();
		if(s > 14400) previous_target_velocity_ *= std::sqrt(14400 / s); // vitesse limitée à 120 m s⁻¹ (obtenue avec 7 accélérateurs améliorés et le boost). Attention, b2_maxTranslation doit être suffisamment grand.
		body_->SetLinearVelocity(previous_target_velocity_);
	}

	// -- Bouclier principal

	if(time_before_shield_regeneration_ > 0)
	{
		--time_before_shield_regeneration_;
		Q_EMIT time_before_shield_regeneration_changed(time_before_shield_regeneration_);
	}
	else
	{
		set_shield(std::min(shield() + shield_regeneration_ * shield_regeneration_boost() * scene()->time_step(), max_shield_));
	}

	// -- Bouclier secondaire

	for(QObject *child: children())
	{
		if(EquipmentSlot *slot = static_qobject_cast<EquipmentSlot>(child))
		{
			if(slot->equipment() && (slot->equipment()->shield() > max_secondary_shield_ || !time_before_secondary_shield_regeneration_))
			{
				slot->equipment()->set_shield(max_secondary_shield_);
			}
		}
	}
	if(time_before_secondary_shield_regeneration_ > 0)
	{
		--time_before_secondary_shield_regeneration_;
		Q_EMIT time_before_secondary_shield_regeneration_changed(time_before_secondary_shield_regeneration_);
	}
	//dot && regen
	{
		if(health_regen() > 0.1)
		{	
			std::vector<Equipment *> equipments;
			for(QObject *child: children())
			{
				if(EquipmentSlot *slot = static_qobject_cast<EquipmentSlot>(child))
				{
					if(Equipment *equipment = slot->equipment())
					{
						equipments.push_back(equipment);
					}
				}
			}
			for(Equipment *e: equipments)
			{
				e->set_health(std::min(e->health() + health_regen() / float(equipments.size()), e->max_health()));
			}
		}
		if(this->dot_timed_time_increment>0)
		{
			this->dot_timed_duration+=this->dot_timed_time_increment;
			this->dot_timed+=this->dot_timed_damages_increment;
		}
		if(this->dot_timed_duration>0)
		{
			--this->dot_timed_duration;
		}
		else
		{
			this->dot_timed=0;
		}
		float damages = damage_shield(this->dot+this->dot_timed) + this->dot_without_shield;
		
		if(damages > 0) damage_global(damages, false);
		
	}
}

void Vehicle::add_dot(float damages)
{
	this->dot+=damages;
}

void Vehicle::add_dot_without_shield(float damages)
{
	this->dot_without_shield+=damages;
}

//actuellement frame = 2 ou -2
void Vehicle::add_dot_timed(float damages,int frame)
{
	this->dot_timed_damages_increment+=damages;
	this->dot_timed_time_increment+=frame;
}

void Vehicle::damage_global(float damages, bool ignore_secondary_shield)
{
	time_before_secondary_shield_regeneration_ = max_time_before_secondary_shield_regeneration();
	Q_EMIT time_before_secondary_shield_regeneration_changed(time_before_secondary_shield_regeneration_);
	std::vector<Equipment *> equipments;

	for(QObject *child: children())
	{
		if(EquipmentSlot *slot = static_qobject_cast<EquipmentSlot>(child))
		{
			if(Equipment *equipment = slot->equipment())
			{
				equipments.push_back(equipment);
			}
		}
	}
	while(!equipments.empty())
	{
		float damages_per_equipment = damages / float(equipments.size());
		if(damages_per_equipment <= 0.01) return;
		auto function = [damages_per_equipment, &damages, ignore_secondary_shield ] (Equipment *e)
		{
			float local_damages = damages_per_equipment;	
			if(!ignore_secondary_shield)
			{
				if(e->shield() > local_damages)
				{
					e->set_shield(e->shield() - local_damages);
					damages -= local_damages;
					return false;
				}
				else
				{
					local_damages -= e->shield();
					damages -= e->shield();
					e->set_shield(0);
				}
			}
			if(e->health() > local_damages)
			{
				
				e->set_health(e->health() - local_damages);
				damages -= local_damages;
				return false;
			}
			else
			{
				damages -= e->health();
				e->deleteLater();
				return true;
			}
		};
		equipments.erase(remove_if(equipments.begin(), equipments.end(), function), equipments.end());
	}
	deleteLater();
}

int Vehicle::max_time_before_shield_regeneration() const
{
	return scene()->seconds_to_frames(3);
}

int Vehicle::max_time_before_secondary_shield_regeneration() const
{
	return scene()->seconds_to_frames(7);
}

void Vehicle::set_relative_velocity(b2Vec2 velocity)
{
	if(velocity.LengthSquared() >= .01)
	{
		relative_velocity_ = velocity;
		set_target_velocity(body_->GetWorldVector(relative_velocity_));
	}
	else
	{
		relative_velocity_.x = NAN;
		set_target_velocity(b2Vec2_zero);
	}
}

void Vehicle::set_target_direction(b2Vec2 direction)
{
	if(direction.LengthSquared() > .1)
	{
		target_angle_ = std::atan2(direction.x, -direction.y);
	}
	else
	{
		target_angle_ = NAN;
		set_target_angular_velocity(0);
	}
}

float Vehicle::damage_shield(float damages)
{
	if(shield() >= damages)
	{
		set_shield(shield() - damages);
		return 0;
	}
	else
	{
		damages -= shield();
		negate_shield();
		return damages;
	}
}

void Vehicle::absorbtion_shield(float damages)
{
	set_shield(shield() + damages * absorbtion_shield_multiplier());

	if(shield() > max_shield())
	{
		damages = (shield() - max_shield()) / absorbtion_shield_multiplier() * absorbtion_damages_multiplier();
		set_shield(max_shield());
		if(damages > 0) damage_global(damages, true);
	}
}

void Vehicle::negate_shield()
{
	set_shield(0);
	time_before_shield_regeneration_ = max_time_before_shield_regeneration();
	Q_EMIT time_before_shield_regeneration_changed(time_before_shield_regeneration_);
}

void Vehicle::negate_secondary_shield()
{
	time_before_secondary_shield_regeneration_ = max_time_before_secondary_shield_regeneration();
	Q_EMIT time_before_secondary_shield_regeneration_changed(time_before_secondary_shield_regeneration_);

	for(QObject *child: children())
	{
		if(EquipmentSlot *slot = static_qobject_cast<EquipmentSlot>(child))
		{
			if(Equipment *equipment = slot->equipment())
			{
				equipment->set_shield(0);
			}
		}
	}
}

float Vehicle::damage_hull(float damages, b2Vec2 point, int type)
{
	Q_ASSERT(body_);
	time_before_secondary_shield_regeneration_ = max_time_before_secondary_shield_regeneration();
	Q_EMIT time_before_secondary_shield_regeneration_changed(time_before_secondary_shield_regeneration_);
	point = point_from_scene(point);
	std::vector<std::pair<float, Equipment *>> equipments;

	for(QObject *child: children())
	{
		if(EquipmentSlot *slot = static_qobject_cast<EquipmentSlot>(child))
		{
			if(Equipment *equipment = slot->equipment())
			{
				equipments.emplace_back((slot->position() - point).LengthSquared(), equipment);
			}
		}
	}

	sort(equipments.begin(), equipments.end());

	for(auto &equipment: equipments)
	{
		Equipment &e = *equipment.second;
		switch(type)
		{
			case 0: // normal
			{
				if(e.shield() > damages)
				{
					e.set_shield(e.shield() - damages);
					return 0;
				}
				damages -= e.shield();
				e.set_shield(0);

				if(e.health() > damages)
				{
					e.set_health(e.health() - damages);
					return 0;
				}
				damages -= e.health();
				e.deleteLater();
				break;
			}
			case 1: // électrique (ne touche que le bouclier)
			{
				e.set_shield(std::max(0.f, e.shield() - damages));
				return 0;
			}
			case 2: // acide (détruit 1 équipement)
			{
				if(!e.shield()) e.deleteLater();
				return 0;
			}
			default: Q_UNREACHABLE();
		}
	}
	if(damages > 0)
	{
		switch(type)
		{
			case 0:
			case 2:
			{
				deleteLater();
				damages -= 1;
				break;
			}
		}
	}

	return damages;
}

void Vehicle::collision_ship_hull(b2Vec2 velocity_enemy, float masse_enemy, b2Vec2 point,float bonus_damages)
{
	b2Vec2 velocity_energy=velocity();
	velocity_energy *= masse() * velocity().Normalize();
	b2Vec2 velocity_enemy_energy=velocity_enemy;
	velocity_enemy_energy*= masse_enemy * velocity_enemy.Normalize();
	b2Vec2 energy_result = velocity_energy;
	energy_result += velocity_enemy_energy;
	float damages= energy_result.Normalize() + bonus_damages;
	damages=damage_hull(damages,point,0);
}

void Vehicle::collision_ship_shield(b2Vec2 velocity_enemy, float masse_enemy, b2Vec2 point,float bonus_damages)
{
	b2Vec2 velocity_energy=velocity();
	velocity_energy *= masse() * velocity().Normalize();
	b2Vec2 velocity_enemy_energy=velocity_enemy;
	velocity_enemy_energy*= masse_enemy * velocity_enemy.Normalize();
	b2Vec2 energy_result = velocity_energy;
	energy_result += velocity_enemy_energy;
	float damages= energy_result.Normalize() + bonus_damages;
	damages=damage_shield(damages);
	//reflechir si on repercute les domages supplémentaires à la coque ou non
	if(damages>=0)
	{
		damage_hull(damages,point,0);
	}
}

float Vehicle::get_norm_velocity()
{
	return velocity().Normalize();
}

AW_DEFINE_OBJECT_STUB(Vehicle)
AW_DEFINE_PROPERTY_STORED(Vehicle, target_velocity)
AW_DEFINE_PROPERTY_STORED(Vehicle, target_angular_velocity)
AW_DEFINE_PROPERTY_STORED(Vehicle, health_regen)
AW_DEFINE_PROPERTY_STORED(Vehicle, max_speed)
AW_DEFINE_PROPERTY_STORED(Vehicle, max_angular_speed)
AW_DEFINE_PROPERTY_STORED(Vehicle, max_force)
AW_DEFINE_PROPERTY_STORED(Vehicle, max_torque)
AW_DEFINE_PROPERTY_STORED(Vehicle, shield)
AW_DEFINE_PROPERTY_STORED(Vehicle, shield_regeneration_boost)
AW_DEFINE_PROPERTY_STORED(Vehicle, speed_boost_mul)

}
}
