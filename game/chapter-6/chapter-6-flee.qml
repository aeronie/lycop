import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-6/chapter-6-flee.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal trapped
	signal escaped
	property var camera_position: Qt.point(44.5 * 4, 24.5 * 4)
	property var last_ship_position: Qt.point(0, 0)
	property var barriere_bas
	property real ti: 0
	property real ti_deb: 90
	property var info_print
	property real chapter_scale: 2
	property var barriere
	property var barriere_hidden
	property bool open_corridor_1: true
	property bool open_corridor_2: true
	property bool open_corridor_3: true
	property bool open_corridor_4: true
	property bool open_corridor_5: true
	property bool open_corridor_6: true
	property bool open_corridor_7: true
	property bool open_corridor_8: true
	property bool open_hidden_room: true
	property bool open_sortie: true
	property var ind_close: 8
	property var armagedon
	property bool spawn_1: false
	property bool spawn_2: false
	property bool spawn_3: false
	property bool spawn_4: false
	property bool spawn_5: false
	
	
	onTiChanged:
	{
		if(ind_close > 0 && ti < (ti_deb - (9 - ind_close) * 10))
		{
			close(ind_close)
			ind_close--
		}
		
		if(!spawn_1 && ti < 80)
		{
			spawn_1 = true
			for(var i=0; i< tab_pos_1.length; i++)
			{
				turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_pos_1[i][0] * 2 * chapter_scale ,tab_pos_1[i][1] * 2 * chapter_scale), angle : -Math.PI / 2})
			}
		}
		if(!spawn_2 && ti < 70)
		{
			spawn_2 = true
			for(var i=0; i< tab_pos_2.length; i++)
			{
				turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_pos_2[i][0] * 2 * chapter_scale ,tab_pos_2[i][1] * 2 * chapter_scale), angle : Math.PI / 2})
			}
		}
		if(!spawn_3 && ti < 70)
		{
			spawn_3 = true
			for(var i=0; i< tab_pos_3.length; i++)
			{
				turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_pos_3[i][0] * 2 * chapter_scale ,tab_pos_3[i][1] * 2 * chapter_scale), angle : -Math.PI / 2})
			}
		}
		if(!spawn_4 && ti < 40)
		{
			spawn_4 = true
			for(var i=0; i< tab_pos_4.length; i++)
			{
				turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_pos_4[i][0] * 2 * chapter_scale ,tab_pos_4[i][1] * 2 * chapter_scale), angle : Math.PI})
			}
		}
		if(!spawn_5 && ti < 40)
		{
			spawn_5 = true
			for(var i=0; i< tab_pos_last.length; i++)
			{
				turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_pos_last[i][0] * 2 * chapter_scale ,tab_pos_last[i][1] * 2 * chapter_scale), cheat: true, angle : Math.PI / 2})
			}
		}
		
		if(ti < 0)
		{
			if(!armagedon)
			{
				armagedon = armagedon_factory.createObject(null, {scene: ship.scene, position: Qt.point(66 * 2 * chapter_scale ,24 * 2 * chapter_scale)})
			}
			else
			{
				armagedon.scale += 2
			}
		}
	}
	
	property var tab_pos_1:[
	[42,16],
	[42,17],
	[42,18],
	[44,16],
	[44,17],
	[44,18]
	]
	
	property var tab_pos_2:[
	[25,31],
	[25,32],
	[25,33],
	[27,31],
	[27,32],
	[27,33],
	[29,31],
	[29,32],
	[29,33],
	[31,31],
	[31,32],
	[31,33],
	[33,31],
	[33,32],
	[33,33]
	]
	
	property var tab_pos_3:[
	[39,36],
	[39,37],
	[39,38],
	[41,36],
	[41,37],
	[41,38],
	[43,36],
	[43,37],
	[43,38],
	[43,36],
	[43,37],
	[43,38],
	[45,36],
	[45,37],
	[45,38],
	[47,36],
	[47,37],
	[47,38]
	]
	
	property var tab_pos_4:[
	[53,16],
	[54,16],
	[55,16],
	[53,18],
	[54,18],
	[55,18],
	[53,20],
	[54,20],
	[55,20],
	[53,22],
	[54,22],
	[55,22]
	]
	
	property var tab_pos_last:[
	[25,11],
	[25,12],
	[25,13]
	]
	
	function close(i)
	{
		if(i == 1 && root.open_corridor_1)
		{
			root.open_corridor_1 = false
		}
		if(i == 2 && root.open_corridor_2)
		{
			root.open_corridor_2 = false
		}
		if(i == 3 && root.open_corridor_3)
		{
			root.open_corridor_3 = false
		}
		if(i == 4 && root.open_corridor_4)
		{
			root.open_corridor_4 = false
		}
		if(i == 5 && root.open_corridor_5)
		{
			root.open_corridor_5 = false
		}
		if(i == 6 && root.open_corridor_6)
		{
			root.open_corridor_6 = false
		}
		if(i == 7 && root.open_corridor_7)
		{
			root.open_corridor_7 = false
		}
		if(i == 8 && root.open_corridor_8)
		{
			root.open_corridor_8 = false
		}
	}
	
	Q.Component
	{
		id: armagedon_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: Qt.rgba(1,0,0,1)
			scale: 0.01
			property real z: 0.01
			readonly property real damages: 10000
			Q.Component.onCompleted: 
			{
				boom_s.play()
			}
			property Sound boom_s : Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 50000
			}
			onScaleChanged:
			{
				collider.update_transform()
			}
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				sensor: true
			}
		}
	}
	Q.Component
	{
		id: heavy_laser_gun_factory
		HeavyLaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: random_spray_factory
		RandomSpray
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: false
			enemy: 1
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: false
			period: scene.seconds_to_frames(3)
			level: 2
		}
	}
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-6/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-6/"+mat+"/c"])
			}
		}
		return fill_tiles
	}
	
	function fill_2(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y, mat])
			}
		}
		return fill_tiles
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}
	
	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("Temps restant: %1 s").arg(Math.max(0, Math.ceil(root.ti)))
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}
	
	scene: Scene
	{
		running: false
		
		Background
		{
			position.x: -45  
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Background
		{
			position.x: -95  
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Background
		{
			position.x: 13
			x:1
			y : 10
			z: altitudes.background_near
			scale: 5
			material: "chapter-6/starship/outdoor-masked"
		}
		
		
		Animation
		{
			id: timer
			time: root.ti_deb
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		
		Body
		{
			position.x: 16
			position.y: 48
			Image
			{
				material: "chapter-6/starship/exit"
				z: altitudes.background_near+0.000001
				scale: 6
				mask: "white"
			}
		}
		
		Tiles
		{
			id: corridor_1
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[51,10,'n','metal'],
				[52,10,'n',''],
				[52,14,'e',''],
				[5,14,'s','metal'],
				[5,10,'w','metal']
				]).concat(
				fill(6,11,51,13,'metal'),
				[[9,12,'chapter-6/metal/c2'],
				[14,12,'chapter-6/metal/c2'],
				[19,12,'chapter-6/metal/c2'],
				[24,12,'chapter-6/metal/c2'],
				[29,12,'chapter-6/metal/c2'],
				[34,12,'chapter-6/metal/c2'],
				[39,12,'chapter-6/metal/c2'],
				[44,12,'chapter-6/metal/c2'],
				[49,12,'chapter-6/metal/c2']]
				)
				
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 100
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.trapped()
				}
			}
				
				
			PolygonCollider
			{
				vertexes: [Qt.point(4.5 * 2, 9.5 * 2), Qt.point(51.5 * 2, 9.5 * 2), Qt.point(51.5 * 2, 14.5 * 2), Qt.point(4.5 * 2, 14.5 * 2)]
				group: open_corridor_1 ? 0 : groups.sensor
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: console_1
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(50 *  chapter_scale*2,13 *  chapter_scale*2)
			property int indice: 24
			Image
			{
				material: "chapter-6/console/" + console_1.indice
				z: altitudes.boss
				scale: 2
			}

		}
		
		Tiles
		{
			id: corridor_2
			z: altitudes.background_near
			scale: root.chapter_scale
			mask: "white"
			tiles: create_tiles([
				[56,10,'n','metal'],
				[56,39,'e','metal'],
				[52,39,'s','metal'],
				[51,39,'s',''],
				[51,35,'w',''],
				[52,35,'n','metal'],
				[52,14,'w','metal'],
				[51,14,'s',''],
				[51,10,'w','']
				]).concat(
				fill(52,11,52,13,'metal'),
				fill(52,36,52,38,'metal'),
				fill(53,11,55,38,'metal'),
				[[54,12,'chapter-6/metal/c2'],
				[54,17,'chapter-6/metal/c2'],
				[54,22,'chapter-6/metal/c2'],
				[54,27,'chapter-6/metal/c2'],
				[54,32,'chapter-6/metal/c2'],
				[52,14,'chapter-6/metal/swi']]
				)
				
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 100
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.trapped()
				}
			}
			PolygonCollider
			{
				vertexes: [Qt.point(51.5 * 2, 9 * 2), Qt.point(56.5 * 2, 9 * 2), Qt.point(56.5 * 2,40 * 2), Qt.point(51.5 * 2, 40 * 2)]
				group: open_corridor_2 ? 0 : groups.sensor
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: console_2
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(55 *  chapter_scale*2, 38 *  chapter_scale*2)
			property int indice: 24
			Image
			{
				material: "chapter-6/console/" + console_2.indice
				z: altitudes.boss
				scale: 2
			}

		}
		
		Tiles
		{
			id: corridor_3
			z: altitudes.background_near
			scale: root.chapter_scale
			mask: "white"
			tiles: create_tiles([
				[52,35,'n',''],
				[52,39,'e',''],
				[5,39,'s','metal'],
				[5,35,'w','metal'],
				[5,36,'w',''],
				[9,36,'n',''],
				[9,35,'e','metal'],
				[51,35,'n','metal']
				]).concat(
				fill(6,35,8,35,'metal'),
				fill(6,36,51,38,'metal'),
				[[7,37,'chapter-6/metal/c2'],
				[12,37,'chapter-6/metal/c2'],
				[17,37,'chapter-6/metal/c2'],
				[22,37,'chapter-6/metal/c2'],
				[27,37,'chapter-6/metal/c2'],
				[32,37,'chapter-6/metal/c2'],
				[37,37,'chapter-6/metal/c2'],
				[42,37,'chapter-6/metal/c2'],
				[47,37,'chapter-6/metal/c2'],
				[51,35,'chapter-6/metal/n']]
				)
					
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 100
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.trapped()
				}
			}
			PolygonCollider
			{
				vertexes: [Qt.point(4 * 2, 34 * 2), Qt.point(51.5 * 2, 34 * 2), Qt.point(51.5 * 2,40 * 2), Qt.point(4 * 2, 40 * 2)]
				group: open_corridor_3 ? 0 : groups.sensor
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_3
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(10 *  chapter_scale*2, 36 *  chapter_scale*2)
			property int indice: 24
			Image
			{
				material: "chapter-6/console/" + console_3.indice
				z: altitudes.boss
				scale: 2
			}

		}
		
		
		Tiles
		{
			id: corridor_4
			z: altitudes.background_near
			scale: root.chapter_scale
			mask: "white"
			tiles: create_tiles([
				[46,30,'n','metal'],
				[47,30,'n',''],
				[47,34,'e',''],
				[9,34,'s','metal'],
				[9,35,'e',''],
				[5,35,'s',''],
				[5,30,'w','metal']]).concat(
				fill(6,34,8,34,'metal'),
				fill(6,31,46,33,'metal'),
				[[7,32,'chapter-6/metal/c2'],
				[12,32,'chapter-6/metal/c2'],
				[17,32,'chapter-6/metal/c2'],
				[22,32,'chapter-6/metal/c2'],
				[27,32,'chapter-6/metal/c2'],
				[32,32,'chapter-6/metal/c2'],
				[37,32,'chapter-6/metal/c2'],
				[42,32,'chapter-6/metal/c2']]
				)
				
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 100
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.trapped()
				}
			}
			PolygonCollider
			{
				vertexes: [Qt.point(4 * 2, 29 * 2), Qt.point(46.5 * 2, 29 * 2), Qt.point(46.5 * 2,34.5 * 2), Qt.point(4 * 2, 34.5 * 2)]
				group: open_corridor_4 ? 0 : groups.sensor
				sensor: true
			}	
		}
		
		
		Sensor
		{
			id: console_4
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(44 *  chapter_scale*2, 33 *  chapter_scale*2)
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + console_4.indice
				z: altitudes.boss
				scale: 2
			}

		}
		
		Tiles
		{
			id: corridor_5
			z: altitudes.background_near
			scale: root.chapter_scale
			mask: "white"
			tiles: create_tiles([
				[51,15,'n','metal'],
				[51,34,'e','metal'],
				[47,34,'s','metal'],
				[46,34,'s',''],
				[46,30,'w',''],
				[47,30,'n','metal'],
				[47,19,'w','metal'],
				[46,19,'s',''],
				[46,15,'w','']]).concat(
				fill(47,16,47,18,'metal'),
				fill(47,31,47,33,'metal'),
				fill(48,16,50,33,'metal'),
				[[49,17,'chapter-6/metal/c2'],
				[49,22,'chapter-6/metal/c2'],
				[49,27,'chapter-6/metal/c2'],
				[49,32,'chapter-6/metal/c2'],
				[47,19,'chapter-6/metal/swi']]
				)
				
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 100
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.trapped()
				}
			}	
			PolygonCollider
			{
				vertexes: [Qt.point(46.5 * 2, 14.5 * 2), Qt.point(51.5 * 2, 14.5 * 2), Qt.point(51.5 * 2,34.5 * 2), Qt.point(46.5 * 2, 34.5 * 2)]
				group: open_corridor_5 ? 0 : groups.sensor
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: console_5
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(49 *  chapter_scale*2, 16 *  chapter_scale*2)
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + console_5.indice
				z: altitudes.boss
				scale: 2
			}

		}
		
		
		Tiles
		{
			id: corridor_6
			z: altitudes.background_near
			scale: root.chapter_scale
			mask: "white"
			tiles: create_tiles([
				[46,15,'n','metal'],
				[47,15,'n',''],
				[47,19,'e',''],
				[9,19,'s','metal'],
				[9,20,'e',''],
				[5,20,'s',''],
				[5,15,'w','metal']]).concat(
				fill(6,19,8,19,'metal'),
				fill(6,16,46,18,'metal'),
				[[7,17,'chapter-6/metal/c2'],
				[12,17,'chapter-6/metal/c2'],
				[17,17,'chapter-6/metal/c2'],
				[22,17,'chapter-6/metal/c2'],
				[27,17,'chapter-6/metal/c2'],
				[32,17,'chapter-6/metal/c2'],
				[37,17,'chapter-6/metal/c2'],
				[42,17,'chapter-6/metal/c2']]
				)
				
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 100
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.trapped()
				}
			}	
			PolygonCollider
			{
				vertexes: [Qt.point(4 * 2, 14.5 * 2), Qt.point(46.5 * 2, 14.5 * 2), Qt.point(46.5 * 2,19.5 * 2), Qt.point(4 * 2, 19.5 * 2)]
				group: open_corridor_6 ? 0 : groups.sensor
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_6
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(10 *  chapter_scale*2, 18 *  chapter_scale*2)
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + console_6.indice
				z: altitudes.boss
				scale: 2
			}
			
		}
		
		Tiles
		{
			id: corridor_7
			z: altitudes.background_near
			scale: root.chapter_scale
			mask: "white"
			tiles: create_tiles([
				[35,20,'n','metal'],
				[35,23,'e','metal'],
				[36,23,'n',''],
				[36,26,'e',''],
				[35,26,'s','metal'],
				[35,29,'e','metal'],
				[22,29,'s','metal'],
				[22,22,'w','metal'],
				[21,22,'s','metal'],
				[21,29,'e','metal'],
				[5,29,'s','metal'],
				[5,20,'w','metal'],
				[5,19,'w',''],
				[9,19,'n',''],
				[9,20,'e','metal'],
				[11,20,'n','metal'],
				[11,25,'e','metal'],
				[12,25,'n','metal'],
				[12,24,'w',''],
				[16,24,'n',''],
				[16,25,'e','metal'],
				[17,25,'n','metal'],
				[17,20,'w','metal']]).concat(
				fill(6,20,8,20,'metal'),
				fill(21,21,22,21,'metal'),
				fill(6,21,10,28,'metal'),
				fill(11,26,17,28,'metal'),
				fill(18,21,20,28,'metal'),
				fill(23,21,34,28,'metal'),
				fill(13,25,15,25,'metal'),
				fill(35,24,35,25,'metal'),
				[[7,23,'chapter-6/metal/c2'],
				[7,27,'chapter-6/metal/c2'],
				[12,27,'chapter-6/metal/c2'],
				[16,27,'chapter-6/metal/c2'],
				[19,23,'chapter-6/metal/c2'],
				[25,23,'chapter-6/metal/c2'],
				[25,27,'chapter-6/metal/c2'],
				[29,27,'chapter-6/metal/c2'],
				[29,23,'chapter-6/metal/c2'],
				[33,27,'chapter-6/metal/c2'],
				[33,23,'chapter-6/metal/c2']]
				)
				
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 100
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.trapped()
				}
			}
			PolygonCollider
			{
				vertexes: [Qt.point(4 * 2, 19.5 * 2), Qt.point(35.5 * 2, 19.5 * 2), Qt.point(35.5 * 2, 29.5 * 2), Qt.point(4 * 2, 29.5 * 2)]
				group: open_corridor_7 ? 0 : groups.sensor
				sensor: true
			}	
		}
		
		Sensor
		{
			id: console_7
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(34 *  chapter_scale*2, 28 *  chapter_scale * 2)
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + console_7.indice
				z: altitudes.boss
				scale: 2
			}
			
		}
		
		Sensor
		{
			id: console_hidden
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(23 *  chapter_scale*2, 28 *  chapter_scale * 2)
			property int indice: 24
			Image
			{
				material: "chapter-6/console/" + console_hidden.indice
				z: altitudes.boss
				scale: 2
			}
			
		}
		
		Tiles
		{
			id: corridor_8
			z: altitudes.background_near
			scale: root.chapter_scale
			mask: "white"
			tiles: create_tiles([
				[46,20,'n','metal'],
				[46,29,'e','metal'],
				[36,29,'s','metal'],
				[36,26,'w','metal'],
				[35,26,'s',''],
				[35,23,'w',''],
				[36,23,'n','metal'],
				[36,20,'w','metal']]).concat(
				fill(36,24,36,25,'metal'),
				fill(37,21,45,28,'metal'),
				[[38,22,'chapter-6/metal/c2'],
				[42,22,'chapter-6/metal/c2'],
				[38,27,'chapter-6/metal/c2'],
				[42,27,'chapter-6/metal/c2']]
				)
				
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 100
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.trapped()
				}
			}	
			PolygonCollider
			{
				vertexes: [Qt.point(35.5 * 2, 19.5 * 2), Qt.point(46.5 * 2, 19.5 * 2), Qt.point(46.5 * 2, 29.5 * 2), Qt.point(35.5 * 2, 29.5 * 2)]
				group: open_corridor_8 ? 0 : groups.sensor
				sensor: true
			}	
		}
		
		Tiles
		{
			id: hidden_room
			z: altitudes.background_near
			scale: root.chapter_scale
			mask: "white"
			tiles: create_tiles([
				[16,20,'n','metal'],
				[16,24,'e','metal'],
				[16,25,'e',''],
				[12,25,'s',''],
				[12,20,'w','metal']]).concat(
				fill(13,21,15,24,'metal'),
				[[14,22,'chapter-6/metal/c2']]
				)
				
		}
		
		
		Sensor
		{
			id: secret
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 100
			type:  Body.STATIC
			position: Qt.point(14 *  chapter_scale*2, 21 *  chapter_scale * 2)
			property bool active: true
			property int indice: 24
			Image
			{
				material: "chapter-6/console/" + secret.indice
				z: altitudes.boss
				scale: 2
			}
		}
		
		Body
		{
			id: clear_screen
			scale: 30
			type: Body.KINEMATIC
			position: ship ? ship.position : Qt.point(0,0)
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			CircleCollider
			{
				group: groups.no_blink
			}
		}
		Ship
		{
			id: ship	
			position.x: 44.5 * 4
			position.y:  24.5 * 4
			angle: -Math.PI/2
			Q.Component.onDestruction: 
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
			
		}
			
		Body
		{
			id: all_wall
			position.y: 0
			position.x: 0
			scale: chapter_scale*2
			
			type: Body.STATIC
			
			function point(x,y)
			{
				return Qt.point(x - 0.25 , y + 0.25)
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(5,0), all_wall.point(56 + 2*0.25,0), all_wall.point(56 + 2 * 0.25, 10 - 2 *0.25), all_wall.point(5,10 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(56 + 2 * 0.25,0), all_wall.point(66,0), all_wall.point(66,49), all_wall.point(56 + 2 * 0.25, 49)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(5,39), all_wall.point(56 + 2 * 0.25,39), all_wall.point(56 + 2 * 0.25,49), all_wall.point(5,49)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(-5,0), all_wall.point(5,0), all_wall.point(5 ,49), all_wall.point(-5,49)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(5,14), all_wall.point(52,14), all_wall.point(52,15 - 2 * 0.25), all_wall.point(5 ,15 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(52 ,15 - 2 * 0.25), all_wall.point(52,35 - 2 * 0.25), all_wall.point(51 + 2 * 0.25,35 - 2 * 0.25), all_wall.point(51 + 2 * 0.25 , 15 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(51  + 2*0.25 ,34), all_wall.point(51  + 2*0.25, 35 - 2 * 0.25), all_wall.point(9 + 2*0.25, 35 - 2 * 0.25), all_wall.point(9 + 2*0.25, 34)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(5 ,29), all_wall.point(47, 29), all_wall.point(47, 30 - 2 * 0.25), all_wall.point(5, 30 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(47 ,19), all_wall.point(47, 29), all_wall.point(46 + 2 * 0.25, 29), all_wall.point(46 + 2 * 0.25, 19)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(9 + 2 * 0.25 ,19), all_wall.point(46 + 2 * 0.25, 19), all_wall.point(46 + 2 * 0.25, 20 - 2 * 0.25), all_wall.point(9 + 2 * 0.25, 20 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(11 + 2 * 0.25 ,20 - 2 * 0.25), all_wall.point(12, 20 - 2 * 0.25), all_wall.point(12, 25 - 2 * 0.25), all_wall.point(11 + 2 * 0.25, 25 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(16 + 2 * 0.25 ,20 - 2 * 0.25), all_wall.point(17, 20 - 2 * 0.25), all_wall.point(17, 25 - 2 * 0.25), all_wall.point(16 + 2 * 0.25, 25 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(21 + 2 * 0.25 ,22), all_wall.point(22, 22 ), all_wall.point(22, 29), all_wall.point(21 + 2 * 0.25, 29)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(35 + 2 * 0.25 ,20 - 2 * 0.25), all_wall.point(36, 20 - 2 * 0.25 ), all_wall.point(36, 23 - 2 * 0.25), all_wall.point(35 + 2 * 0.25, 23 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(35 + 2 * 0.25 ,26), all_wall.point(36, 26 ), all_wall.point(36, 29), all_wall.point(35 + 2 * 0.25, 29)]
				group: groups.wall
			}
		}
		
		Body
		{
			id: barriere_0
			type: Body.STATIC
			property real x:7.5
			property real y:13
			property int nb_repe: 18
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_0.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_0.x - 0.625)*2*chapter_scale
						position.y: (barriere_0.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: root.open_corridor_1 ? Qt.rgba(0.58, 0, 0.82, 0) : Qt.rgba(0.58, 0, 0.82, 1) 
					}
				}
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_0.x - 0.375)*2*chapter_scale
						position.y: (barriere_0.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_1 ? 0 : 1) 
					}
				}
			}
			PolygonCollider
			{
				vertexes: [
				barriere_0.coords(barriere_0.x - 0.75, barriere_0.y + 1.25 - ( barriere_0.nb_repe + 1 ) / 4),
				barriere_0.coords(barriere_0.x - 0.25, barriere_0.y + 1.25 - ( barriere_0.nb_repe + 1 ) / 4), 
				barriere_0.coords(barriere_0.x - 0.25, barriere_0.y + 1.25), 
				barriere_0.coords(barriere_0.x - 0.75, barriere_0.y + 1.25)
				]
				group:  root.open_corridor_1 ? 0 : groups.enemy_invincible
			}
		}
		
		
		Body
		{
			id: barriere_1
			type: Body.STATIC
			property real x:52
			property real y:13
			property int nb_repe: 18
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_1.x - 0.625)*2*chapter_scale
						position.y: (barriere_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: root.open_corridor_2 ? Qt.rgba(0.58, 0, 0.82, 0) : Qt.rgba(0.58, 0, 0.82, 1) 
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_1.x - 0.375)*2*chapter_scale
						position.y: (barriere_1.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_2 ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_1.coords(barriere_1.x - 0.75, barriere_1.y + 1.25 - ( barriere_1.nb_repe + 1 ) / 4),
				barriere_1.coords(barriere_1.x - 0.25, barriere_1.y + 1.25 - ( barriere_1.nb_repe + 1 ) / 4), 
				barriere_1.coords(barriere_1.x - 0.25, barriere_1.y + 1.25), 
				barriere_1.coords(barriere_1.x - 0.75, barriere_1.y + 1.25)
				]
				group:  root.open_corridor_2 ? 0 : groups.enemy_invincible
			}
		}
		
		
		Body
		{
			id: barriere_2
			type: Body.STATIC
			property real x:52
			property real y:38
			property int nb_repe: 18
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_2.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_2.x - 0.375)*2*chapter_scale
						position.y: (barriere_2.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_3 ? 0 : 1) 
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_2.x - 0.625)*2*chapter_scale
						position.y: (barriere_2.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_3 ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_2.coords(barriere_2.x - 0.75, barriere_2.y + 1.25 - ( barriere_2.nb_repe + 1 ) / 4),
				barriere_2.coords(barriere_2.x - 0.25, barriere_2.y + 1.25 - ( barriere_2.nb_repe + 1 ) / 4), 
				barriere_2.coords(barriere_2.x - 0.25, barriere_2.y + 1.25), 
				barriere_2.coords(barriere_2.x - 0.75, barriere_2.y + 1.25)
				]
				group:  root.open_corridor_3 ? 0 : groups.enemy_invincible
			}
		}
		
		Body
		{
			id: barriere_3
			type: Body.STATIC
			property real x: 8
			property real y: 34
			property int nb_repe: 18
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_3.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_3.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_3.y + 0.625)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_4 ? 0 : 1) 
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_3.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_3.y+0.375)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_4 ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_3.coords(barriere_3.x + 1.25 - barriere_3.nb_repe / 4, barriere_3.y + 0.25), 
				barriere_3.coords(barriere_3.x + 1.25, barriere_3.y + 0.25), 
				barriere_3.coords(barriere_3.x + 1.25, barriere_3.y + 0.75), 
				barriere_3.coords(barriere_3.x + 1.25 - barriere_3.nb_repe / 4, barriere_3.y + 0.75)
				]
				group: root.open_corridor_4 ? 0 : groups.enemy_invincible
			}
		}
		
		Body
		{
			id: barriere_4
			type: Body.STATIC
			property real x:47
			property real y:33
			property int nb_repe: 18
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			Repeater
			{
				count: barriere_4.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_4.x - 0.625)*2*chapter_scale
						position.y: (barriere_4.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_5 ? 0 : 1) 
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_4.x - 0.375)*2*chapter_scale
						position.y: (barriere_4.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_5 ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_4.coords(barriere_4.x - 0.75, barriere_4.y + 1.25 - ( barriere_4.nb_repe + 1 ) / 4),
				barriere_4.coords(barriere_4.x - 0.25, barriere_4.y + 1.25 - ( barriere_4.nb_repe + 1 ) / 4), 
				barriere_4.coords(barriere_4.x - 0.25, barriere_4.y + 1.25), 
				barriere_4.coords(barriere_4.x - 0.75, barriere_4.y + 1.25)
				]
				group:  root.open_corridor_5 ? 0 : groups.enemy_invincible
			}
		}
		
		Body
		{
			id: barriere_5
			type: Body.STATIC
			property real x:47
			property real y:18
			property int nb_repe: 18
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_5.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_5.x - 0.375)*2*chapter_scale
						position.y: (barriere_5.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_6 ? 0 : 1) 
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_5.x - 0.625)*2*chapter_scale
						position.y: (barriere_5.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_6 ? 0 : 1) 
					}
				}
			}
			
			
			PolygonCollider
			{
				vertexes: [
				barriere_5.coords(barriere_5.x - 0.75, barriere_5.y + 1.25 - ( barriere_5.nb_repe + 1 ) / 4),
				barriere_5.coords(barriere_5.x - 0.25, barriere_5.y + 1.25 - ( barriere_5.nb_repe + 1 ) / 4), 
				barriere_5.coords(barriere_5.x - 0.25, barriere_5.y + 1.25), 
				barriere_5.coords(barriere_5.x - 0.75, barriere_5.y + 1.25)
				]
				group:  root.open_corridor_6 ? 0 : groups.enemy_invincible
			}
		}
		
		Body
		{
			id: barriere_6
			type: Body.STATIC
			property real x: 8
			property real y: 19
			property int nb_repe: 18
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_6.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_6.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_6.y+0.375)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_7 ? 0 : 1) 
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_6.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_6.y + 0.625)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_7 ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_6.coords(barriere_6.x + 1.25 - barriere_6.nb_repe / 4, barriere_6.y + 0.25), 
				barriere_6.coords(barriere_6.x + 1.25, barriere_6.y + 0.25), 
				barriere_6.coords(barriere_6.x + 1.25, barriere_6.y + 0.75), 
				barriere_6.coords(barriere_6.x + 1.25 - barriere_6.nb_repe / 4, barriere_6.y + 0.75)
				]
				group: root.open_corridor_7 ? 0 : groups.enemy_invincible
			}
		}
		
		
		Body
		{
			id: barriere_7
			type: Body.STATIC
			property real x:36
			property real y:25
			property int nb_repe: 14
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_7.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_7.x - 0.625)*2*chapter_scale
						position.y: (barriere_7.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_8 ? 0 : 1) 
					}
				}
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_7.x - 0.375)*2*chapter_scale
						position.y: (barriere_7.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_8 ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_7.coords(barriere_7.x - 0.75, barriere_7.y + 1.25 - ( barriere_7.nb_repe + 1 ) / 4),
				barriere_7.coords(barriere_7.x - 0.25, barriere_7.y + 1.25 - ( barriere_7.nb_repe + 1 ) / 4), 
				barriere_7.coords(barriere_7.x - 0.25, barriere_7.y + 1.25), 
				barriere_7.coords(barriere_7.x - 0.75, barriere_7.y + 1.25)
				]
				group:  root.open_corridor_8 ? 0 : groups.enemy_invincible
			}
		}
		
		Sensor
		{
			id: enter
			type: Body.STATIC
			property real x:44.5
			property real y:24.5
			scale: 4
			position: enter.coords(x,y)
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Image
			{
				material: "chapter-6/exit/24"
				z: altitudes.boss
			}
			
		}
		
		Sensor
		{
			id: exit
			type: Body.STATIC
			duration_in: root.scene.seconds_to_frames(0.1)
			duration_out: 30
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.escaped()
				}
			}
			PolygonCollider
			{
				vertexes: [
				exit.coords(5,10),
				exit.coords(7,10),
				exit.coords(7,14),
				exit.coords(5,14)
				]
				group: groups.sensor
				sensor: true
			}
		}
		
	}	
	
	
	Q.Component
	{
		id: shield_barriere_factory
		Vehicle
		{
			id: shield_1
			type: Body.STATIC
			property real x:52
			property real y:13
			property int nb_repe: 18
			property var tab_shield:[] // x,y
			property var tab_shield_alive:[] // x,y
			property var tab_collider: []
			property real ti_local: root.ti
			property color mask: Qt.rgba(0, 0.4, 1, shield_1.shield ? Math.max(0.2, shield_1.shield / shield_1.max_shield) : 0)
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			
			onTi_localChanged:
			{
				var alive = false
				for(var i = 0; i < tab_collider.length; i++)
				{
					if(!tab_shield_alive[i])
					{
						tab_collider[i].group = 0
					}
					else
					{
						alive = true
					}
				}
				if(tab_collider.length > 0 && !alive)
				{
					destroy()
				}
			}
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			
			Q.Component.onCompleted:
			{
				shield_1.shield = max_shield
			}
			
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.375)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: shield_1.mask
					}
				}
			}
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.625)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: shield_1.mask
					}
				}
			}
			
			Repeater
			{
				count: shield_1.tab_shield.length
				Q.Component
				{
					EquipmentSlot
					{
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						equipment: Shield
						{
							level: 2	
							material_image: "chapter-2/shield-generator"
							Q.Component.onCompleted:
							{
								shield_1.tab_shield_alive.push(true)
							}
							
							Q.Component.onDestruction: 
							{
								shield_1.tab_shield_alive[index] = false
							}
						}
					}
				}
				Q.Component
				{
					CircleCollider
					{
						id: collider
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						group: shield_1.tab_shield_alive[index] ? groups.enemy_hull_naked : 0
						radius: 0.5
						
						Q.Component.onCompleted:
						{
							shield_1.tab_collider.push(collider)
						}
						
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4),
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4), 
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25), 
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25)
				]
				group: shield_1.shield ? groups.enemy_shield : 0
			}
		}
	}
	
	
	Q.Component
	{
		id: turret_factory
		Vehicle
		{
			id: turret
			property bool activate: false
			property int weapon_choice: cheat ? 2 : random_index(tab_weapons.length)
			type: Body.KINEMATIC
			max_speed: 0
			icon: 3
			icon_color: activate ? "red" : "transparent"
			property var tab_weapons: [random_spray_factory,heavy_laser_gun_factory,quipoutre_factory]
			property bool invulnerable: false
			property bool cheat: false
			property bool alive: !root.no_turret
			onAliveChanged: if(!alive) destroy()
			property real scale_deb: 0.1
			scale: scale_deb
			property real scale_finish: 1
			property real ti_deb: root.ti
			property real time_to_appear: 1
			property int ind_img: 0
			property real ti_local: root.ti
			property bool appear: false
			property int indice: 0
			property real ti_cour: 0
			onTi_localChanged:
			{
				if(!activate)
				{
					if(ti_deb - ti_local < time_to_appear)
					{
						if(ind_img < 14)
						{
							ind_img = Math.floor( 2 * (ti_deb - ti_local) / time_to_appear * 14)
							if(ind_img > 6)
							{
								if(!appear)
								{
									appear = true
									ti_cour = ti_local
								}
								scale += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb)
							}
						}
						else
						{
							scale += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb)
						}
					}
					else
					{
						appear = true
						scale = 1
						activate = true
					}
				}
			}
			
			onScaleChanged:
			{
				collider.update_transform()
			}
			
			Q.Component.onCompleted:
			{
				ti_deb = root.ti			
			}
			
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 3}}
			}
			
			
			Image
			{
				material: "chapter-2/round-turret"
				mask: Qt.rgba(1, 1, 1, turret.appear ? 1 : 0)
				z: altitudes.boss
			}
			
			Image
			{
				id: image
				material: "chapter-2/turret-opening/" + ind_img
				mask: Qt.rgba(1, 1, 1, activate ? 0 : 1)
				z: altitudes.boss - 0.00001
				scale: turret.scale_finish / turret.scale
			}
			
			CircleCollider
			{
				id: collider
				group: turret.activate ? groups.enemy_hull_naked : 0
			}
			EquipmentSlot
			{
				scale: 0.7 * turret.scale/turret.scale_finish
				Q.Component.onCompleted:
				{ 
					equipment = turret.tab_weapons[turret.weapon_choice].createObject(null, {slot: this})
					if(turret.cheat)
					{
						equipment.period = turret.scene.seconds_to_frames(2)
						equipment.bullet_velocity = 8
					}
					equipment.image.mask.a = Qt.binding(function(){return (turret.appear ? 1 : 0)})
					equipment.shooting = Qt.binding(function(){return turret.activate})
				}
				onEquipmentChanged: if(!equipment) turret.deleteLater()
			}
			Behaviour_attack_distance
			{
				id: behaviour
				enabled: turret.activate
				target_groups: groups.enemy_targets
				max_distance: 40
			}
		}
	}
	
	
	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState:   state_test_1 //state_story_0 // 
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					music.objectName = "chapter-2/fuite"
					camera_position= undefined
					messages.add("nectaire/normal", qsTr("begin 1"))
					messages.add("lycop/normal", qsTr("begin 2"))
					messages.add("nectaire/normal", qsTr("begin 3"))
					shield_barriere_factory.createObject(null, {scene: ship.scene, x: 8, y: 13, tab_shield:[[14,11],[14,12],[14,13]] })
					shield_barriere_factory.createObject(null, {scene: ship.scene, x: 15.5, y: 13, tab_shield:[[23.75,10.75],[23.75,11.75],[23.75,12.75],[24.25,11.25],[24.25,12.25],[24.25,13.25]] })
					shield_barriere_factory.createObject(null, {scene: ship.scene, x: 21.5, y: 33, tab_shield:[[34.75,30.75],[34.75,31.75],[34.75,32.75],[35.25,31.25],[35.25,32.25],[35.25,33.25],[35.75,30.75],[35.75,31.75],[35.75,32.75],[36.25,31.25],[36.25,32.25],[36.25,33.25],] })
					shield_barriere_factory.createObject(null, {scene: ship.scene, x: 32, y: 18, tab_shield:[[16.75,15.75],[16.75,16.75],[16.75,17.75],[17.25,16.25],[17.25,17.25],[17.25,18.25]] })
					shield_barriere_factory.createObject(null, {scene: ship.scene, x: 22, y: 21, nb_repe: 10,  tab_shield:[[31.75,21.25],[31.75,23.25],[31.75,25.25],[31.75,27.25],[33.75,21.25],[33.75,23.25],[33.75,25.25],[33.75,27.25],[32.25,22.25],[32.25,24.25],[32.25,26.25]] })
					shield_barriere_factory.createObject(null, {scene: ship.scene, x: 40, y: 38,  tab_shield:[[35,17],[36,17],[37,17],[35,18],[36,18],[37,18],[35,16],[36,16],[37,16],[48,21],[49,21],[50,21],[48,22],[49,22],[50,22],[48,23],[49,23],[50,23],[48,24],[49,24],[50,24],[48,25],[49,25],[50,25]]})
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_1; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_2; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: trapped
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("piege 1"))
					messages.add("lycop/normal", qsTr("piege 2"))
				}
			}
			SignalTransition
			{
				targetState: state_end
				signal: escaped
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-6/escape.ogg"
					function f ()
					{
						saved_game.add_science(1)
						camera_position = Qt.point(-24 * 4, 25 * 4)
						ship.position = Qt.point(-24 * 4, 25 * 4)
						ship.angle = - Math.PI / 2
						saved_game.set("file", "game/chapter-6-conclusion/conclusion.qml")
						platform.set_bool("chapter-6-end")
						saved_game.save()
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
				
			}
		}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard:!screen_video.video.file
				onTriggered:
				{
					scene_loader.load(settings.get_last_saved_game())
				}
			}
		}
		
	}
	
	Q.Component.onCompleted:
	{	
		jobs.run(music, "preload", ["chapter-2/fuite"])
		jobs.run(scene_view, "preload", ["chapter-2/round-turret"])
		for(var i = 0; i < 15; ++i) jobs.run(scene_view, "preload", ["chapter-2/turret-opening/" + i])
		var a2 = ["c", "e", "n", "ne", "nei", "nw", "nwi", "s", "se", "sei", "sw", "swi", "w"]; for(var i2 in a2) jobs.run(scene_view, "preload", ["chapter-2/metal/" + a2[i2]])
		jobs.run(scene_view, "preload", ["chapter-6/starship/outdoor-masked"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		jobs.run(scene_view, "preload", ["chapter-2/generator-1"])
		jobs.run(scene_view, "preload", ["chapter-2/shield-generator"])
		jobs.run(scene_view, "preload", ["chapter-2/barrier-wall"])
		jobs.run(scene_view, "preload", ["explosion"])
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["chapter-6/starship/exit"])
		jobs.run(scene_view, "preload", ["chapter-6/exit/24"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-6/console/" + i])
		groups.init(scene)
		root.info_print = screen_info_factory.createObject(screen_hud)
		root.finalize = function() {if(info_print) info_print.destroy()}
	}
}