import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-6/chapter-6-boss.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	property var shield_gen: shield_1
	signal boss_destroyed
	property var camera_position: Qt.point(15 * 4, 14 * 4)
	property var last_ship_position: Qt.point(0, 0)
	property real ti: 0
	property var info_print
	property real chapter_scale: 2
	property var tab_turret: []
	property bool burrin: false
	property bool start_r: false
	property bool start_boss: false
	property bool shield_down: false
	property bool boom: false
	property color mask_sh: "white"
	property var anim_turret
	property var boss
	property bool no_turret: false
	
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	
	onTiChanged:
	{
		if(ti < -1 && shield_gen)
		{
			var x = 0
			for(var i = 0; i < shield_gen.slots.length; ++i)
			{
				var a = shield_gen.slots[i].equipment
				if(a) x += a.health
			}
			if(x == 0)
			{
				shield_down = true
			}
		}
		if(ti < -1 && !shield_gen)
		{
			shield_down = true
		}
	}
	
	Q.Component
	{
		id: heavy_laser_gun_factory
		HeavyLaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: harpoon_factory
		HarpoonLauncher
		{
			range_bullet: scene.seconds_to_frames(10)
			period: scene.seconds_to_frames(6)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: missile_launcher_factory
		MissileLauncher
		{
			range_bullet: scene.seconds_to_frames(2)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: secondary_shield_factory
		SecondaryShield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: random_spray_factory
		RandomSpray
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: false
			enemy: 1
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: false
			period: scene.seconds_to_frames(3)
			level: 2
		}
	}
	
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-6/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-6/"+mat+"/c"])
			}
		}
		return fill_tiles
	}
	
	function fill_2(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y, mat])
			}
		}
		return fill_tiles
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}
	
	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("%1,%2").arg(ship.position.x).arg(ship.position.y)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}
	
	scene: Scene
	{
		running: false
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		
		Tiles
		{
			id: room
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[28,8,'n','metal'],
				[28,23,'e','metal'],
				[13,23,'s','metal'],
				[13,8,'w','metal']
				]).concat(
				fill(14,9,27,22,'metal'),
				[[15,10,'chapter-6/metal/c2'],
				[18,10,'chapter-6/metal/c2'],
				[26,10,'chapter-6/metal/c2'],
				[23,10,'chapter-6/metal/c2'],
				[26,13,'chapter-6/metal/c2'],
				[26,18,'chapter-6/metal/c2'],
				[26,21,'chapter-6/metal/c2'],
				[23,21,'chapter-6/metal/c2'],
				[18,21,'chapter-6/metal/c2'],
				[15,21,'chapter-6/metal/c2'],
				[15,18,'chapter-6/metal/c2'],
				[15,13,'chapter-6/metal/c2']]
				)
		}
		
		
		
		Ship
		{
			id: ship	
			position.x: 15 * 4
			position.y: 14 * 4
			angle: Math.PI/2
			Q.Component.onDestruction: 
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
			
		}
			
		Body
		{
			id: all_wall
			position.y: 0
			position.x: 0
			scale: chapter_scale*2
			
			type: Body.STATIC
			
			function point(x,y)
			{
				return Qt.point(x - 0.25 , y + 0.25)
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(5,2), all_wall.point(28 + 2* 0.25,2), all_wall.point(28 + 2 * 0.25, 8 - 2 *0.25), all_wall.point(5,8 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(28 + 2 * 0.25,2), all_wall.point(38,2), all_wall.point(38,23), all_wall.point(28 + 2 * 0.25,23)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(13,23), all_wall.point(38,23), all_wall.point(38,33), all_wall.point(13,33)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(5 , 8 - 2 * 0.25),
				all_wall.point(13 , 8 - 2 * 0.25),
				all_wall.point(13 , 33),
				all_wall.point(5 , 33)
				]
				group: groups.wall
			}	
		}
		
		Vehicle
		{
			id: shield_1
			property bool start: root.start_r
			property bool next: true
			property var tab_shield: [
			[15,21],
			[26,21],
			[26,10],
			[15,10]
			]
			readonly property var slots:
			{
				var selected = []
				var all = children
				for(var i = 0; i < all.length; ++i) if(all[i].equipment !== undefined) selected.push(all[i])
				return selected
			}
			property var tab_collider: []
			property var tab_im: []
			property var tab_collider_shield: []
			property var tab_shield_alive: []
			property var tab_inv: []
			property real ti_local: root.ti
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			type: Body.STATIC
			
			Q.Component.onDestruction: 
			{
				root.shield_down = true
			}
			
			onTi_localChanged:
			{
				var alive_l = false
				for(var i = 0; i < tab_collider.length; i++)
				{
					if(!tab_shield_alive[i])
					{
						tab_collider[i].group = 0
					}
					else
					{
						alive_l = true
					}
				}
				if(tab_collider.length > 0 && !alive_l)
				{
					root.shield_down = true
					alive = false
				}
				if(start && alive_l)
				{
					if(next)
					{
						var open = random_index(tab_inv.length)
						while(!tab_inv[open])
						{
							open = random_index(tab_inv.length)
						}
						tab_inv[open] = false
						tab_collider[open].inv = false
						tab_collider_shield[open].inv = false
						tab_im[open].inv= false
						next = false
					}
				}
			}
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			
			
			Repeater
			{
				count: shield_1.tab_shield.length
				Q.Component
				{
					EquipmentSlot
					{
						equipment: Energizer
						{
							Q.Component.onCompleted:
							{
								shield_1.tab_shield_alive.push(true)
							}
							Q.Component.onDestruction: 
							{
								shield_1.next = true
								shield_1.tab_shield_alive[index] = false
							}
						}
						Q.Component.onCompleted:
						{
							position.x = shield_1.tab_shield[index][0] * 2 * root.chapter_scale
							position.y = shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						}
					}
				}
				Q.Component
				{
					CircleCollider
					{
						id: collider
						property bool active: false
						property bool inv: false
						group: inv ? 0 : (active ? groups.item_ignore_enemy : 0)
						radius: 2
						
						Q.Component.onCompleted:
						{
							shield_1.tab_inv.push(true)
							shield_1.tab_collider.push(collider)
							collider.active = Qt.binding(function(){
								return shield_1.tab_shield_alive[index]
							})
							collider.inv = Qt.binding(function(){
								return shield_1.tab_inv[index]
							})
							position.x = shield_1.tab_shield[index][0] * 2 * root.chapter_scale
							position.y = shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						}
					}
				}
				Q.Component
				{
					Image
					{
						id: image_sh
						material: "equipments/shield"
						mask: Qt.rgba(0.58, 0, 0.82, active ? (inv ? 1 : 0) : 0)
						scale: 2.6
						property bool active: false
						property bool inv: false
						Q.Component.onCompleted:
						{
							active = Qt.binding(function(){
								return shield_1.tab_shield_alive[index]
							})
							inv = Qt.binding(function(){
								return shield_1.tab_inv[index]
							})
							position.x = shield_1.tab_shield[index][0] * 2 * root.chapter_scale
							position.y = shield_1.tab_shield[index][1] * 2 * root.chapter_scale
							shield_1.tab_im.push(image_sh)
						}
					}
				}
				
				Q.Component
				{
					CircleCollider
					{
						id: collider
						property bool inv: false
						group: inv ? groups.enemy_invincible : 0
						radius: 2.6
						
						Q.Component.onCompleted:
						{
							inv = Qt.binding(function(){
								return shield_1.tab_inv[index]
							})
							position.x = shield_1.tab_shield[index][0] * 2 * root.chapter_scale
							position.y = shield_1.tab_shield[index][1] * 2 * root.chapter_scale
							shield_1.tab_collider_shield.push(collider)
						}
					}
				}
			}
		}
		
		Sensor
		{
			id: console_1
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(15.5 *  chapter_scale * 2, 8.5 *  chapter_scale * 2)
			
			onFuzzy_valueChanged:
			{	
				if(!root.start_boss)
				{
					if(fuzzy_value == 1)
					{
						root.start_boss = true
						indice = 24
					}
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-6/console/" + console_1.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.start_boss ?  0 : groups.sensor
				sensor: true
			}
		}
		
	}	


	Q.Component
	{
		id: turret_factory
		Vehicle
		{
			id: turret
			property bool activate: false
			property int weapon_choice: cheat ? 2 : random_index(tab_weapons.length)
			type: Body.KINEMATIC
			max_speed: 0
			icon: 3
			icon_color: activate ? "red" : "transparent"
			property var tab_weapons: [random_spray_factory,heavy_laser_gun_factory,quipoutre_factory]
			property bool invulnerable: false
			property bool cheat: false
			property bool alive: !root.no_turret
			onAliveChanged: if(!alive) destroy()
			property real scale_deb: 0.1
			scale: scale_deb
			property real scale_finish: 1
			property real ti_deb: root.ti
			property real time_to_appear: 1
			property int ind_img: 0
			property real ti_local: root.ti
			property bool appear: false
			property int indice: 0
			property real ti_cour: 0
			onTi_localChanged:
			{
				if(!activate)
				{
					if(ti_deb - ti_local < time_to_appear)
					{
						if(ind_img < 14)
						{
							ind_img = Math.floor( 2 * (ti_deb - ti_local) / time_to_appear * 14)
							if(ind_img > 6)
							{
								if(!appear)
								{
									appear = true
									ti_cour = ti_local
								}
								scale += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb)
							}
						}
						else
						{
							scale += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb)
						}
					}
					else
					{
						appear = true
						scale = 1
						activate = true
					}
				}
			}
			
			onScaleChanged:
			{
				collider.update_transform()
			}
			
			Q.Component.onCompleted:
			{
				ti_deb = root.ti			
			}
			
			Q.Component.onDestruction: 
			{
				if(turret.indice == root.tab_turret.length - 1)
				{
					root.tab_turret.pop()
				}
				else
				{
					root.tab_turret[turret.indice] = root.tab_turret.pop()
					root.tab_turret[turret.indice].indice = turret.indice
				}
			}
			
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 3}}
			}
			
			
			
			Image
			{
				material: "chapter-2/round-turret"
				mask: Qt.rgba(1, 1, 1, turret.appear ? 1 : 0)
				z: altitudes.boss
			}
			Image
			{
				id: image
				material: "chapter-2/turret-opening/" + ind_img
				mask: Qt.rgba(1, 1, 1, activate ? 0 : 1)
				z: altitudes.boss - 0.00001
				scale: turret.scale_finish / turret.scale
			}
			CircleCollider
			{
				id: collider
				group: turret.activate ? groups.enemy_hull_naked : 0
			}
			EquipmentSlot
			{
				scale: 0.7 * turret.scale/turret.scale_finish
				Q.Component.onCompleted:
				{ 
					equipment = turret.tab_weapons[turret.weapon_choice].createObject(null, {slot: this})
					if(turret.cheat)
					{
						equipment.period = turret.scene.seconds_to_frames(2)
						equipment.bullet_velocity = 8
					}
					equipment.image.mask.a = Qt.binding(function(){return (turret.appear ? 1 : 0)})
					equipment.shooting = Qt.binding(function(){return turret.activate})
				}
				onEquipmentChanged: if(!equipment) turret.deleteLater()
			}
			Behaviour_attack_distance
			{
				id: behaviour
				enabled: turret.activate
				target_groups: groups.enemy_targets
				max_distance: 80
			}
		}
	}
	
	Q.Component
	{
		id: turret_spawner_factory
		Animation
		{
			id: timer
			time: 0
			speed: -1
			property bool activate: false
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property int max_turret: 10
			property real time_to_spawn: 5
			property real vener: root.burrin ? 1 : 5 / 3.5 
			property real last_spawn: 10
			property bool cheat_a: root.burrin
			property int cap_j: 30
			
			property real sq_diff: 2.5
			
			
			Q.Component.onDestruction: 
			{
				root.no_turret = true
			}
			
			
			function next_position()
			{
				var choose = false
				var j = 0
				while(!choose && j < timer.cap_j)
				{
					var module = 4.5 * 2 * chapter_scale + 2.3 * 2 * chapter_scale * Math.random()
					var angle = 2 * Math.PI * Math.random()
					var eps = Math.PI / 4 / 3
					while((angle <  Math.PI / 4 + eps && angle >  Math.PI / 4 - eps) || (angle <  3 * Math.PI / 4 + eps && angle >  3* Math.PI / 4 - eps) || (angle <  5 * Math.PI / 4 + eps && angle >  5 * Math.PI / 4 - eps) || (angle <  7 * Math.PI / 4 + eps && angle >  7 * Math.PI / 4 - eps) )
					{
						angle = 2 * Math.PI * Math.random()
					}
					
					
					var x = 20.5 * 2 * chapter_scale + module * Math.cos(angle) 
					var y = 15.5 * 2 * chapter_scale + module * Math.sin(angle) 
					choose = true
					for(var i = 0; i< root.tab_turret.length; i++)
					{
						var x_diff = x - root.tab_turret[i].position.x
						var y_diff = y - root.tab_turret[i].position.y
						if(x_diff * x_diff + y_diff * y_diff < sq_diff * sq_diff)
						{
							choose = false
						}
					}
					j++
				}
				return [Qt.point(x,y), j]
			}
			
			onTimeChanged:
			{
				if(activate)
				{
					if(last_spawn - time > time_to_spawn / vener)
					{
						last_spawn = time
						var ind = tab_turret.length
						var pos_j = next_position()
						if(pos_j[1] < cap_j)
						{
							root.tab_turret.push(turret_factory.createObject(null, {scene: ship.scene, position: pos_j[0], cheat: cheat_a, indice: ind }))
						}
					}
				}
			}
		}
	}
	
	
	Q.Component
	{
		id: explosion_factory
		Explosion
		{
			scale: 4
		}
	}
	Q.Component
	{
		id: armagedon_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: "transparent"
			scale: 0.01
			property real z: 0.01
			readonly property real damages: 10000
			Q.Component.onCompleted: 
			{
				boom_s.play()
			}
			onScaleChanged:
			{
				collider.update_transform()
			}
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			property Sound boom_s : Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 5000
			}
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				sensor: true
			}
		}
	}
	
	Q.Component
	{
		id: crack_factory
		Body
		{
			id: cracks
			property int ind: 0
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask: Qt.rgba(0.58, 0, 0.82,1)
			Image
			{
				id: image
				material: "chapter-4/shield-cracks/" + cracks.ind
				mask: cracks.mask
				z: altitudes.shield
				scale : 15 * 1.04
			}
		}	
	}
	//collider et position des arme à revoir lorsque plink aura donné sa texture
	Q.Component
	{
		id: boss_transition_end
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 15
			property real ti_local: root.ti
			property real last_boom: root.ti
			property bool swap: true
			property real scale_sh : 0.9
			property real time_to_boom: 3
			onTi_localChanged:
			{
				var explosion_step = 0.1
				var ajust = swap ? 0.1 : -0.1
				swap= !swap
				boss.position = Qt.point(boss.position.x + ajust, boss.position.y + ajust)
				var sc =  scale_sh - 0.9 / scene.seconds_to_frames(time_to_boom)
				if(last_boom - root.ti  > explosion_step)
				{
					last_boom = root.ti 
					var angle_ran = 2 * Math.PI * Math.random()
					var scale_ran = boss.scale * Math.random()
					explosion_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss.position.x + scale_ran * Math.cos(angle_ran), boss.position.y + scale_ran * Math.sin(angle_ran)) , scale: 8})
				}
				if(sc > 0.1 / boss.scale)
				{
					scale_sh = sc
				}
				else
				{
					root.boss_destroyed()
				}
			}
			
			Q.Component.onCompleted:
			{
				last_boom = root.ti
			}
			Image
			{
				material: "chapter-6/boss/24"
				mask: Qt.rgba(1, 0, 0, 1 )
				scale: 1
				z: altitudes.boss - 0.00001
			}
			Image
			{
				position: coords(1024,790)
				material: "equipments/shield"
				mask: Qt.rgba(1, 0, 0, 1 )
				scale: boss.scale_sh
				z:altitudes.shield
			}
		}
	}
	Q.Component
	{
		id: canon_factory
		Vehicle
		{
			id: canon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool shoot: false
			property real shoot_period: 0.5
			EquipmentSlot
			{
				id: eq_slot
				equipment: Weapon
				{
					id: weapon
					health: max_health
					max_health: 25000
					shooting: canon.shoot
					period: scene.seconds_to_frames(canon.shoot_period)
					readonly property Sound sound_shooting: Sound
					{
						objectName: "equipments/heavy-laser-gun/shooting"
						scale: 40
					}
					bullet_factory: Bullet
					{
						id: bullet
						scale: 1.5
						range: scene.seconds_to_frames(20)
						damages: 10000
						Image
						{
							material: "chapter-6/boss/bullet"
							z: altitudes.bullet
							scale: 1.2
						}
						PolygonCollider
						{
							vertexes: [Qt.point(-0.125, -1), Qt.point(-0.125, 1), Qt.point(0.125, 1), Qt.point(0.125, -1)]
							group: groups.enemy_laser
							sensor: true
						}
						Q.Component.onCompleted:
						{
							position = weapon.slot.point_to_scene(Qt.point(0, -bullet.scale))
							angle = weapon.slot.angle_to_scene(0)
							weapon.sound_shooting.play()
							weapon.set_bullet_velocity(this, 7)
						}
					}
				}
			}
		}
	}
	
	Q.Component
	{
		id: turret_boss_factory
		Vehicle
		{
			id: turret
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool shoot: false
			EquipmentSlot
			{
				id: eq_slot
				equipment: RandomSpray
				{
					range_bullet: scene.seconds_to_frames(3)
					shooting: turret.shoot
					enemy: 1
					level: 2
				}
			}
		}
	}
	
	
	
	//collider et position des arme à revoir lorsque plink aura donné sa texture
	Q.Component
	{
		id: boss_4
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 15
			property real angular_speed: 1 + 10 * ((health_boss && health_boss.health > (health_boss.max_health / 2)) ? (1.1 - (health_boss.health - health_boss.max_health / 2) / (health_boss.max_health / 2)) : 1.1)
			property real time_to_rotate: 20
			property real ti_local: root.ti
			property real deb: root.ti
			property real sens: 1
			property var canon_left
			property var canon_right
			property var coord_canon: [coords(625, 5),coords(1423,5)]
			property var coord_turret: [
			[coords(800, 827), -1/2],
			[coords(936, 809), -7/8],
			[coords(1248,827), 1/2],
			[coords(1112,809), 7/8]
			]
			property var tab_local_turret: []
			
			onTi_localChanged:
			{
				if(canon_right && !canon_right.shoot && deb - root.ti > 0.25)
				{
					canon_right.shoot = true
				}
				if(canon_right)
				{
					canon_right.shoot_period = 0.04 + ((health_boss && health_boss.health > (health_boss.max_health / 2)) ? 0.5 * (health_boss.health - health_boss.max_health / 2) / (health_boss.max_health / 2) : 0)
				}
				if(canon_left)
				{
					canon_left.shoot_period = 0.04 + ((health_boss && health_boss.health > (health_boss.max_health / 2)) ? 0.5 * (health_boss.health - health_boss.max_health / 2) / (health_boss.max_health / 2) : 0)
				}
				angle += sens * angular_speed * 2 * Math.PI / root.scene.seconds_to_frames(time_to_rotate)
				if(angle > 10*Math.PI)
				{
					sens = -1
				}
				if(angle < -10*Math.PI)
				{
					sens = 1
				}
				
				
			}
			
			EquipmentSlot
			{
				equipment: Equipment
				{
					id: health_boss
					health: max_health
					max_health: 75000
				}
			}
			
			
			function canon_left_pos()
			{
				var x = boss.position.x + coord_canon[0].x * scale * Math.cos(boss.angle) - coord_canon[0].y * scale * Math.sin(boss.angle)
				var y = boss.position.y + coord_canon[0].y * scale * Math.cos(boss.angle) + coord_canon[0].x * scale * Math.sin(boss.angle)
				return Qt.point(x,y)
			}
			
			function canon_right_pos()
			{
				var x = boss.position.x + coord_canon[1].x * scale * Math.cos(boss.angle) - coord_canon[1].y * scale * Math.sin(boss.angle)
				var y = boss.position.y + coord_canon[1].y * scale * Math.cos(boss.angle) + coord_canon[1].x * scale * Math.sin(boss.angle)
				return Qt.point(x,y)
			}
			function canon_angle()
			{
				return boss.angle
			}
			Q.Component.onCompleted:
			{
				deb = root.ti
				canon_left  = canon_factory.createObject(null, {scene: scene, position: canon_left_pos(), shoot: true})
				canon_left.position = Qt.binding(canon_left_pos)
				canon_left.angle = Qt.binding(canon_angle)
				
				canon_right  = canon_factory.createObject(null, {scene: scene, position: canon_right_pos(), shoot: false})
				canon_right.position = Qt.binding(canon_right_pos)
				canon_right.angle = Qt.binding(canon_angle)
				
				for(var i = 0; i< coord_turret.length; i++)
				{
					tab_local_turret.push(turret_boss_factory.createObject(null, {scene: ship.scene, position: coord_turret[i][0], shoot: true, angle: boss.angle + coord_turret[i][1]  * Math.PI}))
				}
				tab_local_turret[0].position = Qt.binding(function(){
						var x = boss.position.x + coord_turret[0][0].x * scale * Math.cos(boss.angle) - coord_turret[0][0].y * scale * Math.sin(boss.angle)
						var y = boss.position.y + coord_turret[0][0].y * scale * Math.cos(boss.angle) + coord_turret[0][0].x * scale * Math.sin(boss.angle)
						return Qt.point(x,y)})
				tab_local_turret[1].position = Qt.binding(function(){
						var x = boss.position.x + coord_turret[1][0].x * scale * Math.cos(boss.angle) - coord_turret[1][0].y * scale * Math.sin(boss.angle)
						var y = boss.position.y + coord_turret[1][0].y * scale * Math.cos(boss.angle) + coord_turret[1][0].x * scale * Math.sin(boss.angle)
						return Qt.point(x,y)})
				tab_local_turret[2].position = Qt.binding(function(){
						var x = boss.position.x + coord_turret[2][0].x * scale * Math.cos(boss.angle) - coord_turret[2][0].y * scale * Math.sin(boss.angle)
						var y = boss.position.y + coord_turret[2][0].y * scale * Math.cos(boss.angle) + coord_turret[2][0].x * scale * Math.sin(boss.angle)
						return Qt.point(x,y)})
				tab_local_turret[3].position = Qt.binding(function(){
						var x = boss.position.x + coord_turret[3][0].x * scale * Math.cos(boss.angle) - coord_turret[3][0].y * scale * Math.sin(boss.angle)
						var y = boss.position.y + coord_turret[3][0].y * scale * Math.cos(boss.angle) + coord_turret[3][0].x * scale * Math.sin(boss.angle)
						return Qt.point(x,y)})
						
				tab_local_turret[0].angle = Qt.binding(function(){
					return boss.angle + coord_turret[0][1] * Math.PI
					})
				tab_local_turret[1].angle = Qt.binding(function(){
					return boss.angle + coord_turret[1][1] * Math.PI
					})
				tab_local_turret[2].angle = Qt.binding(function(){
					return boss.angle + coord_turret[2][1] * Math.PI
					})
				tab_local_turret[3].angle = Qt.binding(function(){
					return boss.angle + coord_turret[3][1] * Math.PI
					})
				
				
			}
			Q.Component.onDestruction: 
			{
				
				for(var i = 0; i< tab_local_turret.length; i++)
				{
					tab_local_turret[i].alive = false
				}
				if(canon_left)
				{
					canon_left.alive = false
				}
				if(canon_right)
				{
					canon_right.alive = false
				}
				//changement de phase
				root.boss = boss_transition_end.createObject(null, {scene: ship.scene, position: Qt.point(20.5 * 2 * chapter_scale ,15.5 * 2 * chapter_scale), angle: boss.angle})
			}	
			
			Image
			{
				material: "chapter-6/boss/24"
				mask: health_boss ? Qt.rgba(1, health_boss.health/health_boss.max_health, health_boss.health/health_boss.max_health, 1 ) : Qt.rgba(1, 0, 0, 1 )
				scale: 1
				z: altitudes.boss - 0.00001
			}
			Image
			{
				position: coords(1024,790)
				material: "equipments/shield"
				mask: Qt.rgba(1, 0, 0, 1 )
				scale: 0.9
				z:altitudes.shield
			}
			
			CircleCollider
			{
				position: coords(1024,790)
				group: groups.enemy_shield_aggressive
				radius: 0.9
			}
			PolygonCollider
			{
				vertexes: [coords(533,57),coords(586,7),coords(670,7),coords(712,57),coords(754,752),coords(505,930)]
				group: groups.item_ignore_enemy
			}
			PolygonCollider
			{
				vertexes: [coords(1515,57),coords(1462,7),coords(1378,7),coords(1336,57),coords(1294,752),coords(1543,930)]
				group: groups.item_ignore_enemy
			}
			PolygonCollider
			{
				vertexes: [coords(400,977),coords(833,714),coords(1215,714),coords(1648,977),coords(1648,1277),coords(1515,1476),coords(533,1476),coords(400,1277)]
				group: groups.item_ignore_enemy
			}
		}
	}
	
	
	Q.Component
	{
		id: boss_transition
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 15
			property color mask_sh : Qt.rgba(0.58, 0, 0.82, 1 )
			property real ti_local: root.ti
			property real deb_trans: root.ti
			property real time_to_white: 2
			property real deb_sc: root.ti
			property real time_to_sc: 5
			property real scale_sh: 1.04
			property real last_boom: root.ti
			property bool swap: true
			property bool finish: false
			onTi_localChanged:
			{
				if(im_boss.mask.g < 0.8)
				{
					var explosion_step = 0.1
					var ajust = swap ? 0.1 : -0.1
					swap= !swap
					boss.position = Qt.point(boss.position.x + ajust, boss.position.y + ajust)
					if(last_boom - root.ti  > explosion_step)
					{
						last_boom = root.ti 
						var angle_ran = 2 * Math.PI * Math.random()
						var scale_ran = boss.scale * Math.random()
						explosion_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss.position.x + scale_ran * Math.cos(angle_ran), boss.position.y + scale_ran * Math.sin(angle_ran))})
					}
				}
				if(!finish)
				{
					finish = true
					var g = mask_sh.g + (1 - mask_sh.g)*(deb_trans - root.ti) / time_to_white
					var r = mask_sh.r + (1 - mask_sh.r)*(deb_trans - root.ti) / time_to_white
					var b = mask_sh.b + (1 - mask_sh.b)*(deb_trans - root.ti) / time_to_white
					if(r > 1)
					{
						r = 1
					}
					else
					{
						finish = false
					}
					if(g > 1)
					{
						g = 1
					}
					else
					{
						finish = false
					}
					if(b > 1)
					{
						b = 1
					}
					else
					{
						finish = false
					}
					im_shield.mask.r = r
					im_shield.mask.g = g
					im_shield.mask.b = b
					if(finish)
					{
						deb_sc = root.ti
					}
					
				}
				else
				{
					var sc =  1.04 - 1.04 *(deb_sc - root.ti) / time_to_sc
					var g = 1 *(deb_sc - root.ti) / time_to_sc
					if(sc < 0.1 / boss.scale)
					{
						g = 1
						sc = 0.1
						im_shield.mask.a = 0
						root.boss_destroyed()
					}
					im_boss.mask.g = g
					im_boss.mask.b = g
					scale_sh = sc
				}	
			}
			
			Q.Component.onCompleted:
			{
				deb_trans = root.ti
				last_boom = root.ti
			}
			
			Image
			{
				id: im_boss
				material: "chapter-6/boss/0"
				scale: 1.4
				mask: Qt.rgba(1, 0, 0, 1 )
				z: altitudes.boss - 0.00001
			}
			Image
			{
				id: im_shield
				material: "equipments/shield"
				mask: boss.mask_sh
				scale: scale_sh
				z:altitudes.shield
			}
			
		}
	}
	
	
	Q.Component
	{
		id: boss_3
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 15
			property real ti_local: root.ti
			property bool reflection : false
			property bool transition : false
			property bool protec : true
			property int sens: 1
			property real deb_reflec : root.ti
			property real deb_protec : root.ti
			property real deb_trans : root.ti
			property real time_to_protec: 3
			property real time_to_trans: 5
			property real time_to_reflec: 3
			property color mask_sh : root.mask_sh
			property bool frame_to_load: false
			
			onTi_localChanged:
			{
				//swap de boubou
				//im_shield.mask.r
				
				if(!frame_to_load)
				{
					im_shield.mask = mask_sh
					frame_to_load = true
				}
				else
				{				
					if(reflection)
					{
						if(deb_reflec - root.ti > time_to_reflec)
						{
							reflection = false
							transition = true
							deb_trans = root.ti
							sens = -1
						}
					}
					if(transition)
					{
						//Qt.rgba(0.58, 0, 0.82, 1) protec
						//Qt.rgba(1, 1, 0, 1) reflection
						
						if(deb_trans - root.ti > time_to_trans)
						{
							if(sens == 1)
							{
								reflection = true
								transition = false
								deb_reflec = root.ti
							}
							if(sens == -1)
							{
								protec = true
								transition = false
								deb_protec = root.ti
							}
						}
						else
						{
							if(sens == 1)
							{
								im_shield.mask.g = (deb_trans - root.ti) / time_to_trans
								im_shield.mask.r = 0.58 + 0.42 * (deb_trans - root.ti) / time_to_trans
								im_shield.mask.b = 0.82 * (1 - (deb_trans - root.ti) / time_to_trans)
							}
							else
							{
								im_shield.mask.g = 1 - (deb_trans - root.ti) / time_to_trans
								im_shield.mask.r = 1 - 0.42 * (deb_trans - root.ti) / time_to_trans
								im_shield.mask.b = 0.82 * (deb_trans - root.ti) / time_to_trans
							}
						}
					}
					if(protec)
					{
						if(deb_protec - ti_local > time_to_protec)
						{
							protec = false
							transition = true
							deb_trans = root.ti
							sens = 1
						}
					}
				}		
			}
			
			EquipmentSlot
			{
				equipment: Equipment
				{
					id: health_boss
					health: max_health
					max_health: 50000
				}
			}
			
			function begin(other, point)
			{
				var v = other.velocity
				var p = point
				var vect_radian = Qt.point(point.x - boss.position.x, point.y - boss.position.y)
				var tempx = vect_radian.x
				var tempy = vect_radian.y
				vect_radian.x = tempx / Math.sqrt(tempx * tempx + tempy * tempy)
				vect_radian.y = tempy / Math.sqrt(tempx * tempx + tempy * tempy)
				var point_dep = Qt.point(p.x - v.x, p.y - v.y)
				var vect_dep = Qt.point(point_dep.x - p.x, point_dep.y - p.y)
				
				// AH = <AC.v> / norm(v) * v
				var prod_proj = (vect_dep.x * vect_radian.x + vect_dep.y * vect_radian.y) / Math.sqrt(vect_radian.x * vect_radian.x + vect_radian.y * vect_radian.y)
				
				var vect_proj = Qt.point(prod_proj * vect_radian.x, prod_proj * vect_radian.y)
				
				var point_proj = Qt.point(p.x + vect_proj.x, p.y + vect_proj.y)
				var vect_ortho = Qt.point(point_proj.x - point_dep.x, point_proj.y - point_dep.y)
				
				var point_final = Qt.point(vect_ortho.x + point_proj.x, vect_ortho.y + point_proj.y)
				var vect_velocity =  Qt.point(point_final.x - p.x, point_final.y - p.y)
				
				other.velocity = Qt.point(vect_velocity.x, vect_velocity.y)
				var a = Math.atan2(vect_velocity.y, vect_velocity.x) + Math.PI / 2
				
				var e = scene.time_changed
				function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
				e.connect(f)
				for(var i in other.children)
				{
					var child = other.children[i]
					if(child.group) child.group ^= 1
					if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
				}
			}
			Q.Component.onCompleted:
			{
				deb_reflec = root.ti
				deb_protec = root.ti
				deb_trans = root.ti
			}
			Q.Component.onDestruction: 
			{
				//changement de phase
				root.boss = boss_transition.createObject(null, {scene: ship.scene, position: Qt.point(20.5 * 2 * chapter_scale ,15.5 * 2 * chapter_scale), mask_sh: im_shield.mask})
				anim_turret.alive = false
			}
			
			Image
			{
				material: "chapter-6/boss/0"
				scale: 1.4
				mask: health_boss ? Qt.rgba(1, health_boss.health/health_boss.max_health, health_boss.health/health_boss.max_health, 1 ) : Qt.rgba(1, 0, 0, 1 )
				z: altitudes.boss - 0.00001
			}
			Image
			{
				id: im_shield
				material: "equipments/shield"
				mask: boss.mask_sh
				scale: 1.04
				z:altitudes.shield
			}
			
			CircleCollider
			{
				id: collider
				group: protec ? groups.enemy_invincible : (reflection ? groups.reflector_only_player : 0)
				radius: 1.04
			}
			
			
			PolygonCollider
			{
				vertexes: [coords(38,750),coords(750,38),coords(1298,38),coords(2010,750),coords(2010,1298),coords(1298,2010),coords(750,2010),coords(38,1298)]
				group: transition ? groups.item_ignore_enemy : 0
			}
		}
	}
	
	
	
	
	Q.Component
	{
		id: boss_2
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 15
			property real ti_local: root.ti
			property bool activate: false
			property bool activate_op: false
			property bool visible: false
			property real scale_sh: 1 / 6 / boss.scale
			property real scale_slot: scale_deb
		
			property var tab_slot_equipment:[
			[710,161,0,3],
			[902,137,0,4],
			[408,408,-1/4,5],
			[673,408,-1/4,1],
			[408,673,-1/4,5],
			[161,710,-1/2,3],
			[137,902,-1/2,4],
			[540,540,0,0],
			[1338,161,0,3],
			[1146,137,0,4],
			[1640,408,1/4,5],
			[1375,408,1/4,1],
			[1640,673,1/4,5],
			[1887,710,1/2,3],
			[1911,902,1/2,4],
			[1508,540,0,0],
			[710,1887,1,3],
			[902,1911,1,4],
			[408,1640,-3/4,5],
			[673,1640,-3/4,1],
			[408,1375,-3/4,5],
			[161,1338,-1/2,3],
			[137,1146,-1/2,4],
			[540,1508,0,0],
			[1338,1887,1,3],
			[1146,1911,1,4],
			[1640,1640,3/4,5],
			[1375,1640,3/4,1],
			[1640,1375,3/4,5],
			[1887,1338,1/2,3],
			[1911,1146,1/2,4],
			[1508,1508,0,0],
			[408,1166,0,0],
			[630,1166,0,0],
			[673,1375,-7/8,1],
			[902,1716,-7/8,3],
			[1640,1166,0,0],
			[1418,1166,0,0],
			[1375,1375,7/8,1],
			[1146,1716,7/8,3]
			]
			
			property var tab_weapon:[
			secondary_shield_factory,
			random_spray_factory,
			missile_launcher_factory,
			heavy_laser_gun_factory,
			quipoutre_factory,
			harpoon_factory
			]
			
			property var tab_equi:[]
			property var tab_tu:[]
			property var tab_op:[]
			property real ti_deb: root.ti
			property real time_to_boom: 5
			property real last_boom: root.ti
			property bool swap: true
			property real scale_deb: 1 / 30 / boss.scale
			property real scale_finish: ((1-0.2)/boss.scale)
			property real ti_deb_vi: root.ti
			property real ti_deb_cr: root.ti
			property real time_to_appear: 1
			property int ind_img: 0
			property real ti_cour: 0
			property bool first: true
			property var crack
			property var armageddon
			property bool reflection : false
			property bool transition : false
			property bool protec : true
			property int sens: 1
			property real deb_reflec : root.ti
			property real deb_protec : root.ti
			property real deb_trans : root.ti
			property real time_to_protec: 3
			property real time_to_trans: 5
			property real time_to_reflec: 3
			property bool frame_to_load: false
			
			onTi_localChanged:
			{
				if(!frame_to_load)
				{
					if(tab_op)
					{
						frame_to_load = true
					}
				}
				else
				{
					var explosion_step = 0.1
					if(ti_deb - root.ti < time_to_boom)
					{
						//explosion + boubou
						var ajust = swap ? 0.1 : -0.1
						swap= !swap
						boss.position = Qt.point(boss.position.x + ajust, boss.position.y + ajust)
						scale_sh += (1.04 - 1 / 6 / boss.scale) / root.scene.seconds_to_frames(time_to_boom)
						if(last_boom - root.ti  > explosion_step)
						{
							last_boom = root.ti 
							var angle_ran = 2 * Math.PI * Math.random()
							var scale_ran = boss.scale * Math.random()
							explosion_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss.position.x + scale_ran * Math.cos(angle_ran), boss.position.y + scale_ran * Math.sin(angle_ran))})
						}
						ti_deb_vi = root.ti
					}
					else
					{
						//ouverture des nouvelles tourelles
						if(!activate && !root.shield_down)
						{
							if(!activate_op)
							{
								scale_sh = 1.04
								activate_op = true
							}
							for(var i = 0; i < boss.tab_op.length; i++)
							{
								boss.tab_op[i].material = "chapter-2/turret-opening/" + ind_img
								boss.tab_tu[i].scale = scale_slot
								boss.tab_equi[i].scale = scale_slot
							}
							if(ti_deb_vi - root.ti < time_to_appear)
							{
								if(ind_img < 14)
								{
									ind_img = Math.floor( 2 * (ti_deb_vi - root.ti) / time_to_appear * 14)
									if(ind_img > 6)
									{
										if(!visible)
										{
											visible = true
											ti_cour = root.ti
										}
										scale_slot += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb_vi)
									}
								}
								else
								{
									scale_slot += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb_vi)
								}
							}
							else
							{
								visible = true
								scale_slot = 1
								activate = true
								start_r = true
								activate_op = false
							}
						}
						
						//changement de phase
						if(root.shield_down)
						{
							if(!root.boom)
							{
								//animation de l'armageddon
								if(activate)
								{
									activate = false
								}
								else
								{
									if(first)
									{
										first = false
										crack = crack_factory.createObject(null, {scene: ship.scene, position: Qt.point(20.5 * 2 * chapter_scale ,15.5 * 2 * chapter_scale)})
										ti_deb_cr = root.ti
									}
									else
									{
										if(crack && crack.ind < 5)
										{
											if(ti_deb_cr - root.ti > 0.4)
											{
												crack.ind++
												ti_deb_cr = root.ti
											}
											im_shield.mask.b = 0.82 * (1 - (0.4 * crack.ind + ti_deb_cr - root.ti) / 2)
											crack.mask.b = 0.82 * (1 - (0.4 * crack.ind + ti_deb_cr - root.ti) / 2)
											im_shield.mask.r =0.58 + 0.42 *((0.4 * crack.ind + ti_deb_cr - root.ti) / 2)
											crack.mask.r = 0.58 + 0.42 *((0.4 * crack.ind + ti_deb_cr - root.ti) / 2)
										}
										else
										{
											if(armageddon)
											{
												if(armageddon.scale < 8*4)
												{
													armageddon.scale += 0.3
												}
												else
												{
													if(crack)
													{
														crack.alive = false
														im_shield.mask = Qt.rgba(0.58, 0, 0.82, 1)
													}
													else
													{
														if(armageddon.mask_c.a > 0.06)
														{
															armageddon.mask_c.a -= 0.01
														}
														else
														{
															armageddon.alive = false
															root.boom = true
															activate = true
															root.anim_turret.activate = true
															deb_protec = root.ti
														}
													}
												}
											}
											else
											{
												armageddon = armagedon_factory.createObject(null, {scene: ship.scene, position: Qt.point(20.5 * 2 * chapter_scale ,15.5 * 2 * chapter_scale), mask_c: Qt.rgba(1, 0, 0, 1)})
											}
											
										}
									}
								}
							}
							else
							{
								//swap de boubou
								//im_shield.mask.r
								if(reflection)
								{
									if(deb_reflec - root.ti > time_to_reflec)
									{
										reflection = false
										transition = true
										deb_trans = root.ti
										sens = -1
									}
								}
								if(transition)
								{
									//Qt.rgba(0.58, 0, 0.82, 1) protec
									//Qt.rgba(1, 1, 0, 1) reflection
									
									if(deb_trans - root.ti > time_to_trans)
									{
										if(sens == 1)
										{
											reflection = true
											transition = false
											deb_reflec = root.ti
										}
										if(sens == -1)
										{
											protec = true
											transition = false
											deb_protec = root.ti
										}
									}
									else
									{
										if(sens == 1)
										{
											im_shield.mask.g = (deb_trans - root.ti) / time_to_trans
											im_shield.mask.r = 0.58 + 0.42 * (deb_trans - root.ti) / time_to_trans
											im_shield.mask.b = 0.82 * (1 - (deb_trans - root.ti) / time_to_trans)
										}
										else
										{
											im_shield.mask.g = 1 - (deb_trans - root.ti) / time_to_trans
											im_shield.mask.r = 1 - 0.42 * (deb_trans - root.ti) / time_to_trans
											im_shield.mask.b = 0.82 * (deb_trans - root.ti) / time_to_trans
										}
									}
								}
								if(protec)
								{
									if(deb_protec - ti_local > time_to_protec)
									{
										protec = false
										transition = true
										deb_trans = root.ti
										sens = 1
									}
								}
							}
						}
					}
				}
			}
			Repeater
			{
				count: boss.tab_slot_equipment.length
				Q.Component
				{
					EquipmentSlot
					{
						id: slot
						scale: 1 / boss.scale / 30
						Q.Component.onCompleted:
						{
							equipment = boss.tab_weapon[boss.tab_slot_equipment[index][3]].createObject(null, {slot: this})
							boss.tab_equi.push(slot)
							equipment.image.mask.a = Qt.binding(function(){
								return (boss.visible ? 1 : 0)})
							if(boss.tab_slot_equipment[index][3] != 0)
							{
								equipment.shooting = Qt.binding(function(){return boss.activate})
							}
							angle = boss.tab_slot_equipment[index][2] * Math.PI
							position =  boss.coords(boss.tab_slot_equipment[index][0] , boss.tab_slot_equipment[index][1])
						}
					}
				}
				Q.Component
				{
					Image
					{
						id: image
						material: "chapter-2/turret-opening/0"
						mask: Qt.rgba(1, 1, 1, boss.activate ? 0 : (boss.activate_op ? 1 : 0))
						z: altitudes.boss
						scale: ((1-0.2)/boss.scale)
						position :  boss.coords(boss.tab_slot_equipment[index][0] , boss.tab_slot_equipment[index][1])
						Q.Component.onCompleted:
						{
							boss.tab_op.push(image)
						}
					}
				}
				
				Q.Component
				{
					Image
					{
						id: image
						material: "chapter-2/round-turret"
						mask: Qt.rgba(1, 1, 1, root.shield_down ? 0 : (boss.activate ? 0 : (boss.visible ? 1 : 0)))
						position :  boss.coords(boss.tab_slot_equipment[index][0] , boss.tab_slot_equipment[index][1])
						scale: 1 / boss.scale / 30
						z: altitudes.boss + 0.0000001
						Q.Component.onCompleted:
						{
							boss.tab_tu.push(image)
						}
					}
				}
			}
			
			function begin(other, point)
			{
				var v = other.velocity
				var p = point
				var vect_radian = Qt.point(point.x - boss.position.x, point.y - boss.position.y)
				var tempx = vect_radian.x
				var tempy = vect_radian.y
				vect_radian.x = tempx / Math.sqrt(tempx * tempx + tempy * tempy)
				vect_radian.y = tempy / Math.sqrt(tempx * tempx + tempy * tempy)
				var point_dep = Qt.point(p.x - v.x, p.y - v.y)
				var vect_dep = Qt.point(point_dep.x - p.x, point_dep.y - p.y)
				
				// AH = <AC.v> / norm(v) * v
				var prod_proj = (vect_dep.x * vect_radian.x + vect_dep.y * vect_radian.y) / Math.sqrt(vect_radian.x * vect_radian.x + vect_radian.y * vect_radian.y)
				
				var vect_proj = Qt.point(prod_proj * vect_radian.x, prod_proj * vect_radian.y)
				
				var point_proj = Qt.point(p.x + vect_proj.x, p.y + vect_proj.y)
				var vect_ortho = Qt.point(point_proj.x - point_dep.x, point_proj.y - point_dep.y)
				
				var point_final = Qt.point(vect_ortho.x + point_proj.x, vect_ortho.y + point_proj.y)
				var vect_velocity =  Qt.point(point_final.x - p.x, point_final.y - p.y)
				
				other.velocity = Qt.point(vect_velocity.x, vect_velocity.y)
				var a = Math.atan2(vect_velocity.y, vect_velocity.x) + Math.PI / 2
				
				var e = scene.time_changed
				function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
				e.connect(f)
				for(var i in other.children)
				{
					var child = other.children[i]
					if(child.group) child.group ^= 1
					if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
				}
			}
			
			onScale_shChanged:
			{
				collider.update_transform()
			}
			Q.Component.onCompleted:
			{
				ti_deb = root.ti
				last_boom = root.ti
			}
			Q.Component.onDestruction: 
			{
				//changement de phase
				root.mask_sh = im_shield.mask
				root.boss = boss_3.createObject(null, {scene: ship.scene, position: Qt.point(20.5 * 2 * chapter_scale ,15.5 * 2 * chapter_scale), reflection: boss.reflection, transition: boss.transition, protec: boss.protec, sens: boss.sens, deb_reflec: boss.deb_reflec, deb_protec: boss.deb_protec, deb_trans: boss.deb_trans})
				root.burrin = true

			}
			
			Image
			{
				material: "chapter-6/boss/0"
				scale: 1.4
				z: altitudes.boss - 0.00001
			}
			Image
			{
				id: im_shield
				material: "equipments/shield"
				mask: Qt.rgba(0.58, 0, 0.82, 1 )
				scale: scale_sh
				z:altitudes.shield
			}
			
			CircleCollider
			{
				id: collider
				group: protec ? groups.enemy_invincible : (reflection ? groups.reflector_only_player : 0)
				radius: scale_sh
			}
			
			
			PolygonCollider
			{
				vertexes: [coords(38,750),coords(750,38),coords(1298,38),coords(2010,750),coords(2010,1298),coords(1298,2010),coords(750,2010),coords(38,1298)]
				group: transition ? groups.enemy_hull_naked : 0
			}
		}
	}
	
	
	
	
	Q.Component
	{
		id: boss_1
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 15
			property real ti_local: root.ti
			property bool activate: root.start_boss
			property bool activate_missile: false
			property var tab_slot_equipment:[
			[710,161,0,3],
			[902,137,0,4],
			[408,408,-1/4,1],
			[673,408,-1/4,2],
			[408,673,-1/4,1],
			[161,710,-1/2,3],
			[137,902,-1/2,4],
			[540,540,0,0],
			[1338,161,0,3],
			[1146,137,0,4],
			[1640,408,1/4,1],
			[1375,408,1/4,2],
			[1640,673,1/4,1],
			[1887,710,1/2,3],
			[1911,902,1/2,4],
			[1508,540,0,0],
			[710,1887,1,3],
			[902,1911,1,4],
			[408,1640,-3/4,1],
			[673,1640,-3/4,2],
			[408,1375,-3/4,1],
			[161,1338,-1/2,3],
			[137,1146,-1/2,4],
			[540,1508,0,0],
			[1338,1887,1,3],
			[1146,1911,1,4],
			[1640,1640,3/4,1],
			[1375,1640,3/4,2],
			[1640,1375,3/4,1],
			[1887,1338,1/2,3],
			[1911,1146,1/2,4],
			[1508,1508,0,0],
			[408,1166,0,0],
			[630,1166,0,0],
			[673,1375,-7/8,1],
			[902,1716,-7/8,3],
			[1640,1166,0,0],
			[1418,1166,0,0],
			[1375,1375,7/8,1],
			[1146,1716,7/8,3]
			]
			property var tab_weapon:[secondary_shield_factory,random_spray_factory,missile_launcher_factory,heavy_laser_gun_factory,quipoutre_factory]
			property real ti_deb: 0
			property real time_to_salve: 4
			onTi_localChanged:
			{
				if(activate)
				{
					if(ti_deb - root.ti > time_to_salve)
					{
						ti_deb = root.ti
						activate_missile = !activate_missile
					}
				}
			}
			
			Q.Component.onDestruction: 
			{
				root.boss = boss_2.createObject(null, {scene: root.scene, position: Qt.point(20.5 * 2 * chapter_scale ,15.5 * 2 * chapter_scale),activate_op : false})
			}
			
			
			Image
			{
				material: "chapter-6/boss/0"
				scale: 1.4
				z: altitudes.boss
			}
			Repeater
			{
				count: boss.tab_slot_equipment.length
				Q.Component
				{
					EquipmentSlot
					{
						scale: ((1-0.2)/boss.scale)
						Q.Component.onCompleted:
						{
							equipment = boss.tab_weapon[boss.tab_slot_equipment[index][3]].createObject(null, {slot: this})
							if(boss.tab_slot_equipment[index][3] != 0)
							{
								equipment.shooting = Qt.binding(function(){
								return boss.activate})
							}
							if(boss.tab_slot_equipment[index][3] == 2)
							{
								equipment.shooting = Qt.binding(function(){
								return boss.activate_missile})
							}
							if(boss.tab_slot_equipment[index][3] == 4)
							{
								equipment.period = Qt.binding(function(){
								return boss.activate_missile ? root.scene.seconds_to_frames(3) : root.scene.seconds_to_frames(1.5)})
							}
							angle = boss.tab_slot_equipment[index][2] * Math.PI
							position =  boss.coords(boss.tab_slot_equipment[index][0] , boss.tab_slot_equipment[index][1])
						}
					}
				}
			}
			Image
			{
				material: "equipments/shield"
				mask: Qt.rgba(0.58, 0, 0.82, activate ?  0 : 1)
				scale: 1.04
				z:altitudes.shield
			}
			CircleCollider
			{
				group: activate ? 0 : groups.enemy_invincible
				radius: 1.04
			}
			
			
			PolygonCollider
			{
				vertexes: [coords(38,750),coords(750,38),coords(1298,38),coords(2010,750),coords(2010,1298),coords(1298,2010),coords(750,2010),coords(38,1298)]
				group: activate ? groups.enemy_hull_naked : 0
			}
		}
	}
	
	
	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState:   state_story_0 // state_test_1 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState:   state_dialogue_0
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					camera_position= undefined
					music.objectName = "chapter-2/boss"
					messages.add("nectaire/normal", qsTr("begin 1"))
					messages.add("lycop/normal", qsTr("begin 2"))
				}
			}
		}	
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_1; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_video_opening
				signal: boss_destroyed
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-6/transformers.ogg"
					function f ()
					{
						root.boss.alive = false
						root.boss = boss_4.createObject(null, {scene: ship.scene, position: Qt.point(20.5 * 2 * chapter_scale ,15.5 * 2 * chapter_scale)})
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
			}
		}
		State
		{
			id: state_after_video_opening
			SignalTransition
			{
				targetState: state_story_2
				signal: state_after_video_opening.propertiesAssigned
				guard:!screen_video.video.file
			}
		}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: boss_destroyed
				onTriggered:
				{
					music.objectName = "chapter-2/fuite"
					messages.add("nectaire/normal", qsTr("end 1"))
				}
			}
		}
		State 
		{
			id: state_dialogue_1
			SignalTransition 
			{
				targetState: state_end
				signal: state_dialogue_1.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					saved_game.set("file", "game/chapter-6/chapter-6-flee.qml")
					saved_game.add_science(3)
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	
	Q.Component.onCompleted:
	{	
		jobs.run(music, "preload", ["chapter-2/boss"])
		jobs.run(music, "preload", ["chapter-2/fuite"])
		for(var i = 0; i < 6; ++i) jobs.run(scene_view, "preload", ["chapter-4/shield-cracks/" + i])
		jobs.run(scene_view, "preload", ["chapter-2/round-turret"])
		jobs.run(scene_view, "preload", ["chapter-6/boss/0"])
		jobs.run(scene_view, "preload", ["chapter-6/boss/24"])
		for(var i = 0; i < 15; ++i) jobs.run(scene_view, "preload", ["chapter-2/turret-opening/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-6/console/" + i])
		jobs.run(scene_view, "preload", ["explosion"])
		jobs.run(scene_view, "preload", ["chapter-6/boss/bullet"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		
		groups.init(scene)
		
		boss = boss_1.createObject(null, {scene: ship.scene, position: Qt.point(20.5 * 2 * chapter_scale ,15.5 * 2 * chapter_scale)})
		anim_turret = turret_spawner_factory.createObject(null, {parent: scene})
		//root.info_print = screen_info_factory.createObject(screen_hud)
		//root.finalize = function() {if(info_print) info_print.destroy()}
	}
}