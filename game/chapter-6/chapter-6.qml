import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-6/chapter-6.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	readonly property bool secret_tomate: saved_game.self.get("secret_tomate", false)
	property alias ship: ship
	signal enter_open
	property var camera_position: Qt.point(0, 80)
	property var last_ship_position: Qt.point(0, 0)
	property var info_print
	property real t0: 0
	property real ti: 0
	property bool barriere_active: true
	property bool reset_cooldown: false
	property bool new_wave: false
	property real time_to_restart: 7
	property real starship_pos_x: 250
	property var tab_hole: []
	property var tab_rock: [
	[0,-30],
	[-20,-50],
	[-20,-70],
	[0,-90],
	[20,-70],
	[20,-50],
	[60,-30],
	[10,-80],
	[-20,-50],
	[10,-110],
	[0,20],
	[15,15]
	]
	
	
	
	Q.Component
	{
		id: laser_gun_factory
		LaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: aggressive_shield
		AggressiveShield
		{
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: shield_weapon
		Shield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: secondary_shield_weapon
		SecondaryShield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: heavy_laser_gun_factory
		HeavyLaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: acid_gun_factory
		AcidGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: gun_factory
		Gun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: missile_launcher_factory
		MissileLauncher
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: random_spray_factory
		RandomSpray
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			enemy: 1
			level: 2
		}
	}
	Q.Component
	{
		id: coil_gun_factory
		CoilGun
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: triple_gun_factory
		TripleGun
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: vortex_gun_factory
		VortexGun
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	
	
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}

	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false
		
		Animation
		{
			//animation qui gère le comportement de la barriere
			id: timer
			time: 0
			speed: -1
			property real last_spawn: 0
			property real time_spawner: 20
			onTimeChanged:
			{
				root.ti = timer.time
				
				//gestion de la barriere
				if(root.reset_cooldown)
				{
					root.reset_cooldown = false
					root.barriere_active = false
				}
				
				//gestion des spawners
				if(last_spawn - time > time_spawner)
				{
					last_spawn = time
					root.new_wave = true
				}
				
			}
		}
		Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Background
		{
			x: 1
			y: 4
			z: altitudes.boss + 0.00001
			scale: 15
			position.x: root.starship_pos_x
			material: "chapter-6/starship/shield"
			property bool on: false
			mask: root.barriere_active ? "white" : "transparent"
			PolygonCollider
			{
				vertexes: [Qt.point(-1,-3), Qt.point(0,-3), Qt.point(0,3), Qt.point(-1,3)]
				group: root.barriere_active ? groups.checkpoint : 0
				sensor: true
			}
		}
		
		Background
		{
			x: 1
			y: 5
			z: altitudes.boss
			scale: 15
			position.x: root.starship_pos_x + 20
			material: "chapter-6/starship/behind-outdoor"
		}
		Background
		{
			x: 1
			y: 11
			z: altitudes.boss
			scale: 5
			icon: 3
			icon_color: "red"
			position.x: root.starship_pos_x
			material: "chapter-6/starship/outdoor"
			function calcul_wall_open_position(p)
			{
				var x = root.starship_pos_x
				var y = Math.floor(p.y / scale * 3 / 2) * scale / 3 * 2 + shift_y_
				return Qt.point(x,y)
			}
			
			function begin(rock, p)
			{
				root.reset_cooldown = true
				//animation	story of wall_broken on ne peut pas creer de body pendant la collision
				wall_destroyed_animation.createObject(null, {parent: root.scene, pos: calcul_wall_open_position(p), point : p})
				rock.deleteLater()
			}
			
			PolygonCollider
			{
				vertexes: [Qt.point(0,-5), Qt.point(5,-5), Qt.point(5,5), Qt.point(0,5)]
				group: groups.enemy_invincible
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-0.001,-5), Qt.point(5,-5), Qt.point(5,5), Qt.point(-0.001,5)]
				group: groups.paroi
			}
		}
		Vehicle
		{
			id: laser
			property int y: 20
			scale: 5
			property real z
			property var center: scene_view.center
			property real shift_y: 0
			property bool w_shooting: root.barriere_active
			readonly property real shift_y_: shift_y - Math.floor(shift_y / scale / 2) * scale * 2
			type: Body.STATIC
			position.x: root.starship_pos_x
			position.y: Math.floor(center.y / scale / 2) * scale * 2 + shift_y_
			Repeater
			{
				count: laser.y
				Q.Component
				{
					EquipmentSlot
					{
						position.x: 2/3
						position.y: (index - laser.y / 2) + 2/3
						scale: 0.15
						angle: -Math.PI / 2
						equipment: LaserGun
						{
							shooting: laser.w_shooting
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: 2/3
						position.y: (index - laser.y / 2) - 2/3
						scale: 0.15
						angle: -Math.PI / 2
						equipment: LaserGun
						{
							shooting: laser.w_shooting
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: 2/3
						position.y: (index - laser.y / 2)
						scale: 0.15
						angle: -Math.PI / 2
						equipment: LaserGun
						{
							shooting: laser.w_shooting
						}
					}
				}
			}
		}
		Ship
		{
			id: ship	
			position.y: 80 + (1 - t0) * 10
			property bool on: false
			onOnChanged:
			{
				if(on)
				{
					ship.destroy()
				}
			}
			Q.Component.onDestruction: 
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
			
		}	
		Repeater
		{
			count: root.tab_rock.length
			Q.Component
			{
				Rock
				{
					icon: 2
					icon_color: "blue"
					property real ti_local : root.ti
					position.x : root.tab_rock[index][0]
					position.y : root.tab_rock[index][1]
					angle: Math.random() * 2 * Math.PI
					onTi_localChanged:
					{
						if(on)
						{
							var g = mask.g - 0.01
							if(g <= 0 )
							{
								deleteLater()
							}
							else
							{
								mask.g = g
								mask.b = g
							}
						}
					}
				}
			}
		}
	}	
	Q.Component
	{
		id: space_door_factory
		Body
		{
			id: space_door
			scale: 15
			type: Body.STATIC
			property int indice: 0
			property color mask: "white"
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real z: 0
			Image
			{
				id: image
				material: "chapter-3/door/" + indice
				z: space_door.z
				mask: space_door.mask
			}
		}
	}
	Q.Component
	{
		id: wave_assault_factory
		Animation
		{
			id: timer
			property var tab_pos : [
					[-20,10],
					[-20,-10],
					[20,-10],
					[20,10],
					[0,-30],
					[0,30],
					]

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(root.new_wave)
				{
					for(var i = 0 ; i < tab_pos.length ; ++i)
					{
						tp_enemy_factory.createObject(null, {parent: scene,  position_enemy:  Qt.point(ship.position.x + tab_pos[i][0], ship.position.y + tab_pos[i][1]), position_central: Qt.point(ship.position.x,ship.position.y)})
					}
					root.new_wave = false
				}
				
			}
			
		}
	}
	Q.Component
	{
		id: tp_enemy_factory
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property real value: 0
			property real enemy_count: 2
			property var position_enemy
			property var position_central
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property real last_spawn: 7
			property real time_spawn: 2
			property var tab_drone:[drone_factory,dualdrone_factory,starship_factory]

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: 4 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						if(new_enemy)
						{
							var i = random_index(tab_drone.length)
							enemy = tab_drone[i].createObject(null, {scene: scene, position: Qt.point(door.position.x, door.position.y), angle: Math.atan2(position_central.y - door.position.y, position_central.x - door.position.x )})
							new_enemy = false
							last_spawn = time
							enemy_count--
						}
						else
						{
							if(enemy_count == 0)
							{
								reapp_end = true
							}
							else
							{
								if(last_spawn - time > time_spawn)
								{
									new_enemy = true
								}
							}
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							timer.alive = false
						}
					}
				}
			}
		}
	}
	
	
	Q.Component
	{
		id: wall_destroyed_factory
		Sensor
		{
			id: wall
			duration_in: 1
			duration_out: 1
			scale: 5
			property int indice: 0
			property real life_time: scene.seconds_to_frames(root.time_to_restart)
			property real ti_local: root.ti
			
			onTi_localChanged:
			{
				life_time--
				if(life_time <= 0)
				{
					destroy()
				}
			}
			
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.enter_open()
				}
			}
			
			Q.Component.onCompleted:
			{
				indice = root.tab_hole.length
			}
			
			Q.Component.onDestruction: 
			{
				if(root.tab_hole.length > 1)
				{
					tab_hole[indice] = root.tab_hole.pop()
				}
				else
				{
					root.tab_hole.pop()
					root.barriere_active = true
				}
				wall_repair_factory.createObject(null, {scene: ship.scene, position: Qt.point(wall.position.x,wall.position.y) , angle: wall.angle})
			}
			
			Image
			{
				material: "chapter-6/starship/broken"
			}
			
			PolygonCollider
			{
				vertexes: [Qt.point(-0.3,1), Qt.point(1,1), Qt.point(1,-1), Qt.point(-0.3,-1)]
				group: groups.sensor
				sensor: true
			}
		}
	}
	
	
	Q.Component
	{
		id: wall_repair_factory
		Body
		{
			id: wall
			scale: 5
			property int indice: 0
			property int time_to_change_frame: 3
			property real life_time: 12 * time_to_change_frame
			property real ti_local: root.ti
			
			onTi_localChanged:
			{
				life_time--
				if(Math.floor(life_time) % time_to_change_frame == 0 && indice < 12)
				{
					indice++
				}
				
				if(life_time <= 0)
				{
					destroy()
				}
			}
			
			
			Image
			{
				material: "chapter-6/starship/repair/" + wall.indice
			}
		}
	}
	
	
	Q.Component
	{
		id: armagedon_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: "white"
			scale: 0.01
			property real z: 0.01
			Q.Component.onCompleted: 
			{
				sound.play()
			}
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
			}
		}
	}
	
	
	Q.Component
	{
		id: wall_destroyed_animation
		Animation
		{
			//animation qui gère le comportement de la barriere
			id: timer
			time: 0
			speed: -1
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool first: true
			property bool door_wait: true
			property var point
			property var wall
			property var pos
			property var armagedon
			onTimeChanged:
			{ 
				if(time < -0.1)
				{
					if(first)
					{
						//wall = wall_destroyed_factory.createObject(null, {scene: root.scene, position: pos, scale: 5})
						armagedon = armagedon_factory.createObject(null, {scene: root.scene, position: point})
						first = false
					}
					else
					{
						if(armagedon)
						{
							armagedon.scale += 30 / scene.seconds_to_frames(2)
							if(armagedon.scale >= 25)
							{
								armagedon.mask_c.a = Math.max(1 - ((armagedon.scale - 25) / 5) , 0)
								if(door_wait)
								{
									var ind = 0
									var build_hole = true
									for(var i = 0; i < root.tab_hole.length; i++)
									{
										var y = pos.y - root.tab_hole[i].position.y
										if(Math.abs(y) < 5)
										{
											ind = i
											build_hole = false
										}
										if(Math.abs(y) < 10 && Math.abs(y) > 5)
										{
											var sens = (y > 0) ? 1 : -1
											pos.y = root.tab_hole[i].position.y + sens * 10
										}
									}
									if(build_hole)
									{
										wall = wall_destroyed_factory.createObject(null, {scene: root.scene, position: pos, scale: 5})
										root.tab_hole.push(wall)
										door_wait = false
									}
									else
									{
										root.tab_hole[ind].life_time = scene.seconds_to_frames(root.time_to_restart)
									}
								}
								if(armagedon.mask_c.a == 0)
								{
									armagedon.alive = false
								}
							}
							
						}
						else
						{	
							//animation de reconstruction
							if(!wall)
							{
								timer.alive = false
							}	
						}
					}
				}
			}
		}
	}
	
	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			id: drone
			property var tab_weapons: [laser_gun_factory,vortex_gun_factory,quipoutre_factory]
			property int indice_weapon: random_index(tab_weapons.length)
			max_speed: 10
			max_angular_speed: 6
			icon: 2
			icon_color: "red"
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				material: "folks/heter/1"
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
			EquipmentSlot
			{
				position.y: 0.3
				scale: 0.5
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
		}
	}

	Q.Component
	{
		id: dualdrone_factory
		Vehicle
		{
			id: dualdrone
			max_speed: 5
			max_angular_speed: 4
			icon: 2
			icon_color: "red"
			property var tab_weapons: [laser_gun_factory,coil_gun_factory,triple_gun_factory,heavy_laser_gun_factory,missile_launcher_factory,shield_weapon]
			property int indice_weapon_1: random_index(tab_weapons.length)
			property int indice_weapon_2: indice_weapon_1==(tab_weapons.length - 1) ? random_index(tab_weapons.length - 1) : random_index(tab_weapons.length) //pas deux boucliers
			Q.Component.onCompleted: shield = max_shield
			loot_factory: Repeater
			{
				count: 2
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				material: "folks/heter/2"
			}

			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
				z: altitudes.shield
			}

			CircleCollider
			{
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
			}

			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				radius: 1.1
			}

			EquipmentSlot
			{
				position.x: 0.3
				position.y: 0.5
				scale: 0.5
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon_1].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: -0.3
				position.y: 0.5
				scale: 0.5
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon_2].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
		}
	}

	Q.Component
	{
		id: starship_factory
		Vehicle
		{
			id: starship
			max_speed: 7
			max_angular_speed: strategie ? 1 : 5
			icon: 3
			weapon_regeneration: 1
			icon_color: "red"
			// 1 = etoile , 2 = tout devant , 3 = gauche droite et un au centre
			property int strategie: random_index(tab_angle.length)
			property int index: tab_weapons.length
			property var tab_angle: [
			[0 , 0 , 0 , 0 , 0],
			[-1/4 , -1/2 , 1 , 1/2 , 1/4],
			[-1/2 , -1/2 , 0 , 1/2 , 1/2]]
			property var tab_weapons: [secondary_shield_weapon,coil_gun_factory,vortex_gun_factory,quipoutre_factory]
			property int indice_weapon_1:  random_index(index - 1) + 1
			property int indice_weapon_2:  random_index(index - 1) + 1
			property int indice_weapon_3:  random_index(index)
			property int indice_weapon_4:  1
			property int indice_weapon_5:  1

			scale: 1.4
			loot_factory: Repeater
			{
				count: 5
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				material: "folks/heter/6"
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
				z: altitudes.shield
				position.y: -0.2
			}

			CircleCollider
			{
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
				radius: 0.75
			}

			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				position.y: -0.2
			}

			EquipmentSlot
			{
				position.x: -0.70
				position.y: -0.37
				scale: 0.5 / starship.scale
				angle: tab_angle[starship.strategie][0] * Math.PI
				Q.Component.onCompleted: equipment = starship.tab_weapons[indice_weapon_4].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: -0.35
				position.y:  0
				scale: 0.5 / starship.scale
				angle: starship.tab_angle[starship.strategie][1] * Math.PI
				Q.Component.onCompleted: equipment = starship.tab_weapons[indice_weapon_1].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: 0
				position.y: 0.5
				scale: 0.5 / starship.scale
				angle: tab_angle[starship.strategie][2] * Math.PI
				Q.Component.onCompleted: equipment = starship.tab_weapons[indice_weapon_3].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: 0.35
				position.y: 0
				scale: 0.5 / starship.scale
				angle: tab_angle[starship.strategie][3] * Math.PI
				Q.Component.onCompleted: equipment = starship.tab_weapons[indice_weapon_2].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: 0.70
				position.y: -0.37
				scale: 0.5 / starship.scale
				angle: tab_angle[starship.strategie][4] * Math.PI
				Q.Component.onCompleted: equipment = starship.tab_weapons[indice_weapon_5].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
		}
	}

	
	StateMachine
	{
		begin:    state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState:   state_story_0 //state_test_1 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					//camera_position= undefined
					//wave_assault_factory.createObject(null, {parent: root.scene})
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 2000}
				onTriggered: 
				{
					saved_game.set("travel/chapter-2/chapter-2c", false)
					saved_game.set("travel/chapter-6/chapter-6", false)
					if(!secret_tomate)
					{
						saved_game.set("travel/chapter-4/chapter-4b", false)
					}
					music.objectName = "chapter-6/outside"
					wave_assault_factory.createObject(null, {parent: root.scene})
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					ship.position.y = 80
					camera_position= undefined
					messages.add("lycop/normal", qsTr("begin 1"))
					messages.add("nectaire/normal", qsTr("begin 2"))
					messages.add("lycop/normal", qsTr("begin 3"))
					messages.add("nectaire/normal", qsTr("begin 4"))
					messages.add("lycop/normal", qsTr("begin 5"))
					messages.add("nectaire/normal", qsTr("begin 6"))
					messages.add("lycop/normal", qsTr("begin 7"))
					messages.add("nectaire/normal", qsTr("begin 8"))
					messages.add("lycop/normal", qsTr("begin 9"))
					messages.add("nectaire/normal", qsTr("begin 10"))
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_2; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_end
				signal: enter_open
				onTriggered:
				{
					saved_game.set("file", "game/chapter-6/chapter-6-inside.qml")
					saved_game.add_science(1)
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	
	Q.Component.onCompleted:
	{
		groups.init(scene)
		root.finalize = function() {if(info_print) info_print.destroy()}
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-3/door/" + i])
		for(var i = 0; i < 12; ++i) jobs.run(scene_view, "preload", ["chapter-6/starship/repair/" + i])
		jobs.run(scene_view, "preload", ["folks/heter/2"])
		jobs.run(scene_view, "preload", ["folks/heter/6"])
		jobs.run(scene_view, "preload", ["folks/heter/1"])
		jobs.run(scene_view, "preload", ["asteroid/0"])
		jobs.run(scene_view, "preload", ["asteroid/1"])
		jobs.run(scene_view, "preload", ["asteroid/2"])
		jobs.run(scene_view, "preload", ["chapter-6/starship/shield"])
		jobs.run(scene_view, "preload", ["chapter-6/starship/outdoor"])
		jobs.run(scene_view, "preload", ["chapter-6/starship/broken"])
		jobs.run(scene_view, "preload", ["chapter-6/starship/behind-outdoor"])
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["explosion"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		jobs.run(music, "preload", ["chapter-6/outside"])
		
	}
}