import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-6/chapter-6-inside.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal open_secret
	signal sortie
	property var camera_position: Qt.point(5.5 * 4, 12 * 4)
	property var last_ship_position: Qt.point(0, 0)
	property var barriere_bas
	property real ti: 0
	property var info_print
	property real chapter_scale: 2
	property var barriere
	property var barriere_hidden
	property bool open_corridor_2: false
	property bool open_corridor_3: false
	property bool open_corridor_4: false
	property bool open_corridor_5: false
	property bool open_corridor_6: false
	property bool open_corridor_7: false
	property bool open_corridor_8: false
	property bool generator_down: false
	property bool open_hidden_room: false
	property bool open_sortie: false
	readonly property bool secret_base_heter: saved_game.self.get("secret_base_heter", false)
	readonly property bool secret_cathedrale: saved_game.self.get("secret_cathedrale", false)
	readonly property bool secret_foreuse: saved_game.self.get("secret_foreuse", false)
	property bool secret_commandement: false
	property real fuzzy_comp: 0
	onOpen_corridor_2Changed:
	{
		if(open_corridor_2)
		{
			for(var i =0; i < tab_turret_corridor_2.length; i++)
			{
				var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_2[i][0] * 2 * root.chapter_scale, tab_turret_corridor_2[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_2[i][2], angle: tab_turret_corridor_2[i][3] * Math.PI, activate: true, invulnerable: tab_turret_corridor_2[i][5]})
				tur.alive = Qt.binding(tab_turret_corridor_2[i][4])
			}
		}
	}
	
	onOpen_corridor_3Changed:
	{
		if(open_corridor_3)
		{
			for(var i =0; i < tab_turret_corridor_3.length; i++)
			{
				var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_3[i][0] * 2 * root.chapter_scale, tab_turret_corridor_3[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_3[i][2], angle: tab_turret_corridor_3[i][3] * Math.PI, activate: true, invulnerable: tab_turret_corridor_3[i][5]})
				tur.alive =   Qt.binding(tab_turret_corridor_3[i][4])
			}
		}
	}
	
	onOpen_corridor_4Changed:
	{
		if(open_corridor_4)
		{
			for(var i =0; i < tab_turret_corridor_4.length; i++)
			{
				var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_4[i][0] * 2 * root.chapter_scale, tab_turret_corridor_4[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_4[i][2], angle: tab_turret_corridor_4[i][3] * Math.PI, activate: true, invulnerable: tab_turret_corridor_4[i][5]})
				tur.alive =   Qt.binding(tab_turret_corridor_4[i][4])
			}
			var shield = shield_barriere_factory.createObject(null, {scene: ship.scene, x: 21.5, y: 33, tab_shield:[[34.75,30.75],[34.75,31.75],[34.75,32.75],[35.25,31.25],[35.25,32.25],[35.25,33.25],[35.75,30.75],[35.75,31.75],[35.75,32.75],[36.25,31.25],[36.25,32.25],[36.25,33.25],] })
			shield.alive = Qt.binding(tab_turret_corridor_4[0][4])
		}
	}
	
	
	onOpen_corridor_5Changed:
	{
		if(open_corridor_5)
		{
			for(var i =0; i < tab_turret_corridor_5.length; i++)
			{
				var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_5[i][0] * 2 * root.chapter_scale, tab_turret_corridor_5[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_5[i][2], angle: tab_turret_corridor_5[i][3] * Math.PI, activate: true, invulnerable: tab_turret_corridor_5[i][5], shoot: false, deb: root.ti, ti_local:root.ti})
				tur.alive =   Qt.binding(tab_turret_corridor_5[i][4])
				tur.ti_local = Qt.binding(function(){return root.ti})
			}
		}
	}
	
	
	onOpen_corridor_6Changed:
	{
		if(open_corridor_6)
		{
			for(var i =0; i < tab_turret_corridor_6.length; i++)
			{
				var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_6[i][0] * 2 * root.chapter_scale, tab_turret_corridor_6[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_6[i][2], angle: tab_turret_corridor_6[i][3] * Math.PI, activate: true, invulnerable: tab_turret_corridor_6[i][5]})
				tur.alive =   Qt.binding(tab_turret_corridor_6[i][4])
			}
			var shield = shield_barriere_factory.createObject(null, {scene: ship.scene, x: 32, y: 18, tab_shield:[[16.75,15.75],[16.75,16.75],[16.75,17.75],[17.25,16.25],[17.25,17.25],[17.25,18.25]] })
			shield.alive = Qt.binding(tab_turret_corridor_6[0][4])
		}
	}
	
	onOpen_corridor_7Changed:
	{
		if(open_corridor_7)
		{
			for(var i =0; i < tab_turret_corridor_7.length; i++)
			{
				var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_7[i][0] * 2 * root.chapter_scale, tab_turret_corridor_7[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_7[i][2], angle: tab_turret_corridor_7[i][3] * Math.PI, activate: true, invulnerable: tab_turret_corridor_7[i][5]})
				tur.alive =   Qt.binding(tab_turret_corridor_7[i][4])
			}
			
			var shield = shield_barriere_factory.createObject(null, {scene: ship.scene, x: 22, y: 21, nb_repe: 10,  tab_shield:[[31.75,21.25],[31.75,23.25],[31.75,25.25],[31.75,27.25],[33.75,21.25],[33.75,23.25],[33.75,25.25],[33.75,27.25],[32.25,22.25],[32.25,24.25],[32.25,26.25]] })
			shield.alive = Qt.binding(tab_turret_corridor_7[0][4])
			var boss = hidden_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(8 * 2 * chapter_scale,26 * 2 * chapter_scale)})
			boss.alive = Qt.binding(tab_turret_corridor_7[0][4])
			boss = hidden_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(29 * 2 * chapter_scale,22 * 2 * chapter_scale)})
			boss.alive = Qt.binding(tab_turret_corridor_7[0][4])
			boss = hidden_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(29 * 2 * chapter_scale,27 * 2 * chapter_scale)})
			boss.alive = Qt.binding(tab_turret_corridor_7[0][4])
			
		}
	}
	
	
	onOpen_corridor_8Changed:
	{
		if(open_corridor_8)
		{
			for(var i =0; i < tab_turret_corridor_8.length; i++)
			{
				var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_8[i][0] * 2 * root.chapter_scale, tab_turret_corridor_8[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_8[i][2], angle: tab_turret_corridor_8[i][3] * Math.PI, activate: true, invulnerable: tab_turret_corridor_8[i][5]})
				tur.alive = Qt.binding(tab_turret_corridor_8[i][4])
			}
			
			var shield = barriere_last_room_factory.createObject(null, {scene: ship.scene, x: 39.5, y: 28, nb_repe: 38,  tab_shield:[[41,24.5],[42,22],[42,27]] })
			var boss = mini_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(40 * 2 * chapter_scale, 22.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
			boss.live = Qt.binding(tab_turret_corridor_8[0][4])
			boss = mini_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(40 * 2 * chapter_scale, 26.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
			boss.live = Qt.binding(tab_turret_corridor_8[0][4])
		}
	}
	
	
	onOpen_hidden_roomChanged:
	{
		if(open_hidden_room)
		{
			for(var i =0; i < tab_hidden.length; i++)
			{
				var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_hidden[i][0] * 2 * root.chapter_scale, tab_hidden[i][1] * 2 * root.chapter_scale), weapon_choice: tab_hidden[i][2], angle: tab_hidden[i][3] * Math.PI, activate: true, invulnerable: tab_hidden[i][5]})
				tur.alive = Qt.binding(tab_hidden[i][4])
			}
		}
	}
	
	function is_not_2_open()
	{
		return !root.open_corridor_2
	}
	function is_true()
	{
		return true
	}
	function is_not_3_open()
	{
		return !root.open_corridor_3
	}
	function is_not_4_open()
	{
		return !root.open_corridor_4
	}
	function is_not_5_open()
	{
		return !root.open_corridor_5
	}
	function is_not_6_open()
	{
		return !root.open_corridor_6
	}
	function is_not_7_open()
	{
		return !root.open_corridor_7
	}
	function is_not_8_open()
	{
		return !root.open_corridor_8
	}
	function is_not_generator_down()
	{
		return !root.generator_down
	}
	// x, y , weapon choice, angle, bind, invulnerable
	property var tab_turret_corridor_1:[
	[11,11,3, -1/2 ,root.is_not_2_open, false],
	[11,12,3, -1/2 ,root.is_not_2_open, false],
	[11,13 ,3, -1/2 ,root.is_not_2_open, false],
	[12.75,11,3, -1/2 ,root.is_not_2_open, false],
	[12.75,12,3, -1/2 ,root.is_not_2_open, false],
	[12.75,13 ,3, -1/2 ,root.is_not_2_open, false],
	[34.75,11,3, -1/2 ,root.is_not_2_open, false],
	[34.75,12,3, -1/2 ,root.is_not_2_open, false],
	[34.75,13 ,3, -1/2 ,root.is_not_2_open, false],
	[26.75,12,3, -1/2 ,root.is_not_2_open, false],
	[30.75,12,3, -1/2 ,root.is_not_2_open, false],
	[32.75,12,3, -1/2 ,root.is_not_2_open, false],
	[17.25,11,0, -1/2 ,root.is_not_2_open, false],
	[17.25,12,0, -1/2 ,root.is_not_2_open, false],
	[17.25,13,0, -1/2 ,root.is_not_2_open, false],
	[15.75,11,1, -1/2 ,root.is_not_2_open, false],
	[15.75,12,1, -1/2 ,root.is_not_2_open, false],
	[15.75,13,1, -1/2 ,root.is_not_2_open, false],
	[20.75,11,1, -1/2 ,root.is_not_2_open, false],
	[20.75,12,1, -1/2 ,root.is_not_2_open, false],
	[20.75,13,1, -1/2 ,root.is_not_2_open, false],
	[21.75,11,1, -1/2 ,root.is_not_2_open, false],
	[21.75,12,1, -1/2 ,root.is_not_2_open, false],
	[21.75,13,1, -1/2 ,root.is_not_2_open, false],
	[37.75,11,1, -1/2 ,root.is_not_2_open, false],
	[37.75,12,1, -1/2 ,root.is_not_2_open, false],
	[37.75,13,1, -1/2 ,root.is_not_2_open, false],
	[41.75,11,1, -1/2 ,root.is_not_2_open, false],
	[41.75,12,1, -1/2 ,root.is_not_2_open, false],
	[41.75,13,1, -1/2 ,root.is_not_2_open, false],
	[44.75,11,1, -1/2 ,root.is_not_2_open, false],
	[44.75,12,1, -1/2 ,root.is_not_2_open, false],
	[44.75,13,1, -1/2 ,root.is_not_2_open, false],
	[47.75,11,1, -1/2 ,root.is_not_2_open, false],
	[47.75,12,1, -1/2 ,root.is_not_2_open, false],
	[47.75,13,1, -1/2 ,root.is_not_2_open, false],
	[26.75,10.25,2, -1/2 ,root.is_not_2_open, true],
	[27.75,10.25,2, -1/2 ,root.is_not_2_open, true],
	[28.75,10.25,2, -1/2 ,root.is_not_2_open, true],
	[29.75,10.25,2, -1/2 ,root.is_not_2_open, true],
	[30.75,10.25,2, -1/2 ,root.is_not_2_open, true],
	[31.75,10.25,2, -1/2 ,root.is_not_2_open, true],
	[32.75,10.25,2, -1/2 ,root.is_not_2_open, true],
	[33.75,10.25,2, -1/2 ,root.is_not_2_open, true],
	[26.75,13.75,2, -1/2 ,root.is_not_2_open, true],
	[27.75,13.75,2, -1/2 ,root.is_not_2_open, true],
	[28.75,13.75,2, -1/2 ,root.is_not_2_open, true],
	[29.75,13.75,2, -1/2 ,root.is_not_2_open, true],
	[30.75,13.75,2, -1/2 ,root.is_not_2_open, true],
	[31.75,13.75,2, -1/2 ,root.is_not_2_open, true],
	[32.75,13.75,2, -1/2 ,root.is_not_2_open, true],
	[33.75,13.75,2, -1/2 ,root.is_not_2_open, true]	
	]
	
	property var tab_turret_corridor_2:[
	[53,14 ,1, 0 ,root.is_not_3_open, true],
	[53,17 ,1, 0 ,root.is_not_3_open, true],
	[53,20 ,1, 0 ,root.is_not_3_open, true],
	[53,23 ,1, 0 ,root.is_not_3_open, true],
	[53,26 ,1, 0 ,root.is_not_3_open, true],
	[53,29 ,1, 0 ,root.is_not_3_open, true],
	[53,32 ,1, 0 ,root.is_not_3_open, true],
	[55,14 ,1, 0 ,root.is_not_3_open, true],
	[55,17 ,1, 0 ,root.is_not_3_open, true],
	[55,20 ,1, 0 ,root.is_not_3_open, true],
	[55,23 ,1, 0 ,root.is_not_3_open, true],
	[55,26 ,1, 0 ,root.is_not_3_open, true],
	[55,29 ,1, 0 ,root.is_not_3_open, true],
	[55,32 ,1, 0 ,root.is_not_3_open, true]
	]	
	
	property var tab_turret_corridor_3:[
	[45,36 ,1, 1/2 ,root.is_not_4_open, false],
	[45,37 ,1, 1/2 ,root.is_not_4_open, false],
	[45,38 ,1, 1/2 ,root.is_not_4_open, false],
	[36,36 ,1, 1/2 ,root.is_not_4_open, false],
	[36,37 ,1, 1/2 ,root.is_not_4_open, false],
	[36,38 ,1, 1/2 ,root.is_not_4_open, false],
	[34,36 ,1, 1/2 ,root.is_not_4_open, false],
	[34,37 ,1, 1/2 ,root.is_not_4_open, false],
	[34,38 ,1, 1/2 ,root.is_not_4_open, false],
	[42,36 ,0, 1/2 ,root.is_not_4_open, false],
	[42,37 ,0, 1/2 ,root.is_not_4_open, false],
	[42,38 ,0, 1/2 ,root.is_not_4_open, false],
	[41,36 ,0, 1/2 ,root.is_not_4_open, false],
	[41,37 ,0, 1/2 ,root.is_not_4_open, false],
	[41,38 ,0, 1/2 ,root.is_not_4_open, false],
	[21,36 ,0, 1/2 ,root.is_not_4_open, false],
	[21,37 ,0, 1/2 ,root.is_not_4_open, false],
	[21,38 ,0, 1/2 ,root.is_not_4_open, false],
	[19,36 ,0, 1/2 ,root.is_not_4_open, false],
	[19,37 ,0, 1/2 ,root.is_not_4_open, false],
	[19,38 ,0, 1/2 ,root.is_not_4_open, false],
	[17,36 ,0, 1/2 ,root.is_not_4_open, false],
	[17,37 ,0, 1/2 ,root.is_not_4_open, false],
	[17,38 ,0, 1/2 ,root.is_not_4_open, false],
	[23,35.1 ,3, 1/2 ,root.is_not_4_open, false],
	[25,35.1 ,3, 1/2 ,root.is_not_4_open, false],
	[27,35.1 ,3, 1/2 ,root.is_not_4_open, false],
	[29,35.1 ,3, 1/2 ,root.is_not_4_open, false],
	[23,38.9 ,3, 1/2 ,root.is_not_4_open, false],
	[25,38.9 ,3, 1/2 ,root.is_not_4_open, false],
	[27,38.9 ,3, 1/2 ,root.is_not_4_open, false],
	[29,38.9 ,3, 1/2 ,root.is_not_4_open, false],
	[24,35.5 ,2, 1/2 ,root.is_not_4_open, true],
	[26,35.5 ,2, 1/2 ,root.is_not_4_open, true],
	[28,35.5 ,2, 1/2 ,root.is_not_4_open, true],
	[30,35.5 ,2, 1/2 ,root.is_not_4_open, true],
	[31,35.5 ,2, 1/2 ,root.is_not_4_open, true],
	[24,38.5 ,2, 1/2 ,root.is_not_4_open, true],
	[26,38.5 ,2, 1/2 ,root.is_not_4_open, true],
	[28,38.5 ,2, 1/2 ,root.is_not_4_open, true],
	[30,38.5 ,2, 1/2 ,root.is_not_4_open, true],
	[31,38.5 ,2, 1/2 ,root.is_not_4_open, true]
	]
	
	
	property var tab_turret_corridor_4:[
	[6,32 ,1, 1 ,root.is_not_5_open, false],
	[7,32 ,1, 1 ,root.is_not_5_open, false],
	[8,32 ,1, 1 ,root.is_not_5_open, false],
	[9,32 ,1, 1 ,root.is_not_5_open, false],
	[6,33 ,1, 1 ,root.is_not_5_open, false],
	[7,33 ,1, 1 ,root.is_not_5_open, false],
	[8,33 ,1, 1 ,root.is_not_5_open, false],
	[9,33 ,1, 1 ,root.is_not_5_open, false],
	[12,31 ,1, -1 / 2 ,root.is_not_5_open, false],
	[12,32 ,1, -1 / 2 ,root.is_not_5_open, false],
	[12,33 ,1, -1 / 2 ,root.is_not_5_open, false],
	[14,31 ,1, -1 / 2 ,root.is_not_5_open, false],
	[14,32 ,1, -1 / 2 ,root.is_not_5_open, false],
	[14,33 ,1, -1 / 2 ,root.is_not_5_open, false],
	[16,31 ,1, -1 / 2 ,root.is_not_5_open, false],
	[16,32 ,1, -1 / 2 ,root.is_not_5_open, false],
	[16,33 ,1, -1 / 2 ,root.is_not_5_open, false],
	[18,31 ,1, -1 / 2 ,root.is_not_5_open, false],
	[18,32 ,1, -1 / 2 ,root.is_not_5_open, false],
	[18,33 ,1, -1 / 2 ,root.is_not_5_open, false],
	[20,31 ,1, -1 / 2 ,root.is_not_5_open, false],
	[20,32 ,1, -1 / 2 ,root.is_not_5_open, false],
	[20,33 ,1, -1 / 2 ,root.is_not_5_open, false],
	[27,31 ,1, -1 / 2 ,root.is_not_5_open, false],
	[27,32 ,1, -1 / 2 ,root.is_not_5_open, false],
	[27,33 ,1, -1 / 2 ,root.is_not_5_open, false],
	[29,31 ,1, -1 / 2 ,root.is_not_5_open, false],
	[29,32 ,1, -1 / 2 ,root.is_not_5_open, false],
	[29,33 ,1, -1 / 2 ,root.is_not_5_open, false],
	[31,31 ,1, -1 / 2 ,root.is_not_5_open, false],
	[31,32 ,1, -1 / 2 ,root.is_not_5_open, false],
	[31,33 ,1, -1 / 2 ,root.is_not_5_open, false],
	[26,31 ,0, -1 / 2 ,root.is_not_5_open, false],
	[26,32 ,0, -1 / 2 ,root.is_not_5_open, false],
	[26,33 ,0, -1 / 2 ,root.is_not_5_open, false],
	[34,31 ,0, -1 / 2 ,root.is_not_5_open, false],
	[34,32 ,0, -1 / 2 ,root.is_not_5_open, false],
	[34,33 ,0, -1 / 2 ,root.is_not_5_open, false],
	[37,31 ,0, -1 / 2 ,root.is_not_5_open, false],
	[37,32 ,0, -1 / 2 ,root.is_not_5_open, false],
	[37,33 ,0, -1 / 2 ,root.is_not_5_open, false],
	[23,31 ,3, -1 / 2 ,root.is_not_5_open, false],
	[23,32 ,3, -1 / 2 ,root.is_not_5_open, false],
	[23,33 ,3, -1 / 2 ,root.is_not_5_open, false],
	[24,30.5 ,3, -1 / 2 ,root.is_not_5_open, false],
	[24,32 ,3, -1 / 2 ,root.is_not_5_open, false],
	[24,33.5 ,3, -1 / 2 ,root.is_not_5_open, false]
	]
	
	property var tab_turret_corridor_5:[
	[48,21 ,3, 1 ,root.is_not_6_open, true],
	[48,23 ,3, 1 ,root.is_not_6_open, true],
	[48,25 ,3, 1 ,root.is_not_6_open, true],
	[48,27 ,3, 1 ,root.is_not_6_open, true],
	[48,29 ,3, 1 ,root.is_not_6_open, true],
	[48,31 ,3, 1 ,root.is_not_6_open, true],
	[50,21 ,3, 1 ,root.is_not_6_open, true],
	[50,23 ,3, 1 ,root.is_not_6_open, true],
	[50,25 ,3, 1 ,root.is_not_6_open, true],
	[50,27 ,3, 1 ,root.is_not_6_open, true],
	[50,29 ,3, 1 ,root.is_not_6_open, true],
	[50,31 ,3, 1 ,root.is_not_6_open, true]
	]
	
	property var tab_turret_corridor_6:[
	[42,16 ,0, 1/2 ,root.is_not_7_open, false],
	[42,17 ,0, 1/2 ,root.is_not_7_open, false],
	[42,18 ,0, 1/2 ,root.is_not_7_open, false],
	[40,16 ,0, 1/2 ,root.is_not_7_open, false],
	[40,17 ,0, 1/2 ,root.is_not_7_open, false],
	[40,18 ,0, 1/2 ,root.is_not_7_open, false],
	[38,16 ,0, 1/2 ,root.is_not_7_open, false],
	[38,17 ,0, 1/2 ,root.is_not_7_open, false],
	[38,18 ,0, 1/2 ,root.is_not_7_open, false],
	[36,16 ,0, 1/2 ,root.is_not_7_open, false],
	[36,17 ,0, 1/2 ,root.is_not_7_open, false],
	[36,18 ,0, 1/2 ,root.is_not_7_open, false],
	[34,16 ,0, 1/2 ,root.is_not_7_open, false],
	[34,17 ,0, 1/2 ,root.is_not_7_open, false],
	[34,18 ,0, 1/2 ,root.is_not_7_open, false],
	[29,16 ,0, 1/2 ,root.is_not_7_open, false],
	[29,17 ,0, 1/2 ,root.is_not_7_open, false],
	[29,18 ,0, 1/2 ,root.is_not_7_open, false],
	[27,16 ,0, 1/2 ,root.is_not_7_open, false],
	[27,17 ,0, 1/2 ,root.is_not_7_open, false],
	[27,18 ,0, 1/2 ,root.is_not_7_open, false],
	[25,16 ,0, 1/2 ,root.is_not_7_open, false],
	[25,17 ,0, 1/2 ,root.is_not_7_open, false],
	[25,18 ,0, 1/2 ,root.is_not_7_open, false],
	[23,16 ,0, 1/2 ,root.is_not_7_open, false],
	[23,17 ,0, 1/2 ,root.is_not_7_open, false],
	[23,18 ,0, 1/2 ,root.is_not_7_open, false],
	[21,16 ,0, 1/2 ,root.is_not_7_open, false],
	[21,17 ,0, 1/2 ,root.is_not_7_open, false],
	[21,18 ,0, 1/2 ,root.is_not_7_open, false],
	[19,16 ,0, 1/2 ,root.is_not_7_open, false],
	[19,17 ,0, 1/2 ,root.is_not_7_open, false],
	[19,18 ,0, 1/2 ,root.is_not_7_open, false],
	[15,16 ,0, 1/2 ,root.is_not_7_open, false],
	[15,17 ,0, 1/2 ,root.is_not_7_open, false],
	[15,18 ,0, 1/2 ,root.is_not_7_open, false],
	[13,16 ,0, 1/2 ,root.is_not_7_open, false],
	[13,17 ,0, 1/2 ,root.is_not_7_open, false],
	[13,18 ,0, 1/2 ,root.is_not_7_open, false]
	]
	
	
	property var tab_turret_corridor_7:[
	[13,26 ,0, -1/2 ,root.is_not_8_open, false],
	[13,27 ,0, -1/2 ,root.is_not_8_open, false],
	[13,28 ,0, -1/2 ,root.is_not_8_open, false],
	[15,26 ,0, -1/2 ,root.is_not_8_open, false],
	[15,27 ,0, -1/2 ,root.is_not_8_open, false],
	[15,28 ,0, -1/2 ,root.is_not_8_open, false],
	[17,26 ,0, -1/2 ,root.is_not_8_open, false],
	[17,27 ,0, -1/2 ,root.is_not_8_open, false],
	[17,28 ,0, -1/2 ,root.is_not_8_open, false],
	[19,26 ,0, -1/2 ,root.is_not_8_open, false],
	[19,27 ,0, -1/2 ,root.is_not_8_open, false],
	[19,28 ,0, -1/2 ,root.is_not_8_open, false]
	]
	
	property var tab_turret_corridor_8:[
	[41,21 ,0, -1/2 ,is_not_generator_down, false],
	[42,23 ,0, -1/2 ,is_not_generator_down, false],
	[42,26 ,0, -1/2 ,is_not_generator_down, false],
	[41,28 ,0, -1/2 ,is_not_generator_down, false],
	[43,21 ,1, -1/2 ,is_not_generator_down, false],
	[43,22 ,1, -1/2 ,is_not_generator_down, false],
	[43,23 ,1, -1/2 ,is_not_generator_down, false],
	[43,24 ,1, -1/2 ,is_not_generator_down, false],
	[43,25 ,1, -1/2 ,is_not_generator_down, false],
	[43,26 ,1, -1/2 ,is_not_generator_down, false],
	[43,27 ,1, -1/2 ,is_not_generator_down, false],
	[40,20.05 ,3, -1/2 ,is_not_generator_down, false],
	[40,20.65 ,3, -1/2 ,is_not_generator_down, false],
	[40,24.25 ,3, -1/2 ,is_not_generator_down, false],
	[40,24.75 ,3, -1/2 ,is_not_generator_down, false],
	[40,28.35 ,3, -1/2 ,is_not_generator_down, false],
	[40,28.95 ,3, -1/2 ,is_not_generator_down, false]
	]
	
	property var tab_hidden:[
	[13,23 ,3, 1 ,is_true, false],
	[14,23 ,3, 1 ,is_true, false],
	[15,23 ,3, 1 ,is_true, false],
	[13,22 ,3, 1 ,is_true, false],
	[14,22 ,3, 1 ,is_true, false],
	[15,22 ,3, 1 ,is_true, false]
	]
	
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-6/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-6/"+mat+"/c"])
			}
		}
		return fill_tiles
	}
	
	function fill_2(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y, mat])
			}
		}
		return fill_tiles
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}
	
	/*Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("Temps restant: %1").arg(root.ti)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}*/
	
	scene: Scene
	{
		running: false
		
		Background
		{
			position.x: -45  
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Background
		{
			position.x: 13
			x:1
			y : 10
			z: altitudes.background_near
			scale: 5
			material: "chapter-6/starship/outdoor-masked"
		}
		
		
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		/*Tiles
		{
			id: fond
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[56,10,'n','metal'],
				[56,39,'e','metal'],
				[5,39,'s','metal'],
				[5,30,'w','metal'],
				[47,30,'n','metal'],
				[47,19,'w','metal'],
				[9,19,'s','metal'],
				[9,20,'e','metal'],
				[11,20,'n','metal'],
				[11,25,'e','metal'],
				[12,25,'n','metal'],
				[12,20,'w','metal'],
				[16,20,'n','metal'],
				[16,25,'e','metal'],
				[17,25,'n','metal'],
				[17,20,'w','metal'],
				[35,20,'n','metal'],
				[35,23,'e','metal'],
				[36,23,'n','metal'],
				[36,20,'w','metal'],
				[46,20,'n','metal'],
				[46,29,'e','metal'],
				[36,29,'s','metal'],
				[36,26,'w','metal'],
				[35,26,'s','metal'],
				[35,29,'e','metal'],
				[22,29,'s','metal'],
				[22,22,'w','metal'],
				[21,22,'s','metal'],
				[21,29,'e','metal'],
				[5,29,'s','metal'],
				[5,15,'w','metal'],
				[51,15,'n','metal'],
				[51,34,'e','metal'],
				[9,34,'s','metal'],
				[9,35,'e','metal'],
				[52,35,'n','metal'],
				[52,14,'w','metal'],
				[5,14,'s','metal'],
				[5,10,'w','metal']
				]).concat(
				fill(6,11,55,13,'metal'),
				fill(53,14,55,38,'metal'),
				fill(6,36,52,38,'metal'),
				fill(6,31,8,35,'metal'),
				fill(9,31,50,33,'metal'),
				fill(48,16,50,30,'metal'),
				fill(6,16,47,18,'metal'),
				fill(6,19,8,20,'metal'),
				fill(6,21,10,28,'metal'),
				fill(11,26,20,28,'metal'),
				fill(18,21,20,25,'metal'),
				fill(13,21,15,25,'metal'),
				fill(21,21,22,21,'metal'),
				fill(23,21,34,28,'metal'),
				fill(35,24,36,25,'metal'),
				fill(37,21,45,28,'metal')
				)
		}*/	
		
		Tiles
		{
			id: corridor_1
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[51,10,'n','metal'],
				[52,10,'n',''],
				[52,14,'e',''],
				[5,14,'s','metal'],
				[5,10,'w','metal']
				]).concat(
				fill(6,11,51,13,'metal'),
				[[9,12,'chapter-6/metal/c2'],
				[14,12,'chapter-6/metal/c2'],
				[19,12,'chapter-6/metal/c2'],
				[24,12,'chapter-6/metal/c2'],
				[29,12,'chapter-6/metal/c2'],
				[34,12,'chapter-6/metal/c2'],
				[39,12,'chapter-6/metal/c2'],
				[44,12,'chapter-6/metal/c2'],
				[49,12,'chapter-6/metal/c2']]
				)
		}
		
		
		Sensor
		{
			id: console_1
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(50 *  chapter_scale*2,13 *  chapter_scale*2)
			
			onFuzzy_valueChanged:
			{	
				root.fuzzy_comp = fuzzy_value
				if(fuzzy_value == 1)
				{
					root.open_corridor_2 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-6/console/" + console_1.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.open_corridor_2 ?  0 : groups.sensor
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_2
			z: altitudes.background_near
			mask: root.open_corridor_2 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[56,10,'n','metal'],
				[56,39,'e','metal'],
				[52,39,'s','metal'],
				[51,39,'s',''],
				[51,35,'w',''],
				[52,35,'n','metal'],
				[52,14,'w','metal'],
				[51,14,'s',''],
				[51,10,'w','']
				]).concat(
				fill(52,11,52,13,'metal'),
				fill(52,36,52,38,'metal'),
				fill(53,11,55,38,'metal'),
				[[54,12,'chapter-6/metal/c2'],
				[54,17,'chapter-6/metal/c2'],
				[54,22,'chapter-6/metal/c2'],
				[54,27,'chapter-6/metal/c2'],
				[54,32,'chapter-6/metal/c2'],
				[52,14,'chapter-6/metal/swi']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(51.5 * 2, 9 * 2), Qt.point(56.5 * 2, 9 * 2), Qt.point(56.5 * 2,40 * 2), Qt.point(51.5 * 2, 40 * 2)]
				group: root.open_corridor_2 ? 0 : groups.wall
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: console_2
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(55 *  chapter_scale*2, 38 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_3 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-6/console/" + console_2.indice
				z: altitudes.boss
				mask: root.open_corridor_2 ? "white" : "transparent"
				scale: 2
			}
			
			CircleCollider
			{
				group: root.open_corridor_3 ?  0 : groups.sensor
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_3
			z: altitudes.background_near
			mask: root.open_corridor_3 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[52,35,'n',''],
				[52,39,'e',''],
				[5,39,'s','metal'],
				[5,35,'w','metal'],
				[5,36,'w',''],
				[9,36,'n',''],
				[9,35,'e','metal'],
				[51,35,'n','metal']
				]).concat(
				fill(6,35,8,35,'metal'),
				fill(6,36,51,38,'metal'),
				[[7,37,'chapter-6/metal/c2'],
				[12,37,'chapter-6/metal/c2'],
				[17,37,'chapter-6/metal/c2'],
				[22,37,'chapter-6/metal/c2'],
				[27,37,'chapter-6/metal/c2'],
				[32,37,'chapter-6/metal/c2'],
				[37,37,'chapter-6/metal/c2'],
				[42,37,'chapter-6/metal/c2'],
				[47,37,'chapter-6/metal/c2'],
				[51,35,'chapter-6/metal/n']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(4 * 2, 34 * 2), Qt.point(51.5 * 2, 34 * 2), Qt.point(51.5 * 2,40 * 2), Qt.point(4 * 2, 40 * 2)]
				group: root.open_corridor_3 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_3
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(10 *  chapter_scale*2, 36 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_4 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-6/console/" + console_3.indice
				z: altitudes.boss
				mask: root.open_corridor_3 ? "white" : "transparent"
				scale: 2
			}
			
			CircleCollider
			{
				group: root.open_corridor_4 ?  0 : groups.sensor
				sensor: true
			}
		}
		
		
		Tiles
		{
			id: corridor_4
			z: altitudes.background_near
			mask: root.open_corridor_4 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[46,30,'n','metal'],
				[47,30,'n',''],
				[47,34,'e',''],
				[9,34,'s','metal'],
				[9,35,'e',''],
				[5,35,'s',''],
				[5,30,'w','metal']]).concat(
				fill(6,34,8,34,'metal'),
				fill(6,31,46,33,'metal'),
				[[7,32,'chapter-6/metal/c2'],
				[12,32,'chapter-6/metal/c2'],
				[17,32,'chapter-6/metal/c2'],
				[22,32,'chapter-6/metal/c2'],
				[27,32,'chapter-6/metal/c2'],
				[32,32,'chapter-6/metal/c2'],
				[37,32,'chapter-6/metal/c2'],
				[42,32,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(4 * 2, 29 * 2), Qt.point(46.5 * 2, 29 * 2), Qt.point(46.5 * 2,34.5 * 2), Qt.point(4 * 2, 34.5 * 2)]
				group: root.open_corridor_4 ? 0 : groups.wall
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: console_4
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(44 *  chapter_scale*2, 33 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_5 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + console_4.indice
				z: altitudes.boss
				mask: root.open_corridor_4 ? "white" : "transparent"
				scale: 2
			}
			
			CircleCollider
			{
				group: root.open_corridor_5 ?  0 : groups.sensor
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_5
			z: altitudes.background_near
			mask: root.open_corridor_5 ? "white" : "transparent"
			scale: root.chapter_scale
			property var collider_wall: []
			tiles: create_tiles([
				[51,15,'n','metal'],
				[51,34,'e','metal'],
				[47,34,'s','metal'],
				[46,34,'s',''],
				[46,30,'w',''],
				[47,30,'n','metal'],
				[47,19,'w','metal'],
				[46,19,'s',''],
				[46,15,'w','']]).concat(
				fill(47,16,47,18,'metal'),
				fill(47,31,47,33,'metal'),
				fill(48,16,50,33,'metal'),
				[[49,17,'chapter-6/metal/c2'],
				[49,22,'chapter-6/metal/c2'],
				[49,27,'chapter-6/metal/c2'],
				[49,32,'chapter-6/metal/c2'],
				[47,19,'chapter-6/metal/swi']]
				)
				
			PolygonCollider
			{
				id: collider_1
				vertexes: [Qt.point(46.5 * 2, 14.5 * 2), Qt.point(51.5 * 2, 14.5 * 2), Qt.point(51.5 * 2,34.5 * 2), Qt.point(46.5 * 2, 34.5 * 2)]
				group: root.open_corridor_5 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_5
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(49 *  chapter_scale*2, 16 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_6 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + console_5.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_5 ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: root.open_corridor_6 ?  0 : groups.sensor
				sensor: true
			}
		}
		
		
		Tiles
		{
			id: corridor_6
			z: altitudes.background_near
			mask: root.open_corridor_6 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[46,15,'n','metal'],
				[47,15,'n',''],
				[47,19,'e',''],
				[9,19,'s','metal'],
				[9,20,'e',''],
				[5,20,'s',''],
				[5,15,'w','metal']]).concat(
				fill(6,19,8,19,'metal'),
				fill(6,16,46,18,'metal'),
				[[7,17,'chapter-6/metal/c2'],
				[12,17,'chapter-6/metal/c2'],
				[17,17,'chapter-6/metal/c2'],
				[22,17,'chapter-6/metal/c2'],
				[27,17,'chapter-6/metal/c2'],
				[32,17,'chapter-6/metal/c2'],
				[37,17,'chapter-6/metal/c2'],
				[42,17,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(4 * 2, 14.5 * 2), Qt.point(46.5 * 2, 14.5 * 2), Qt.point(46.5 * 2,19.5 * 2), Qt.point(4 * 2, 19.5 * 2)]
				group: root.open_corridor_6 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_6
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(10 *  chapter_scale*2, 18 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_7 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + console_6.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_6 ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: root.open_corridor_7 ?  0 : groups.sensor
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_7
			z: altitudes.background_near
			mask: root.open_corridor_7 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[35,20,'n','metal'],
				[35,23,'e','metal'],
				[36,23,'n',''],
				[36,26,'e',''],
				[35,26,'s','metal'],
				[35,29,'e','metal'],
				[22,29,'s','metal'],
				[22,22,'w','metal'],
				[21,22,'s','metal'],
				[21,29,'e','metal'],
				[5,29,'s','metal'],
				[5,20,'w','metal'],
				[5,19,'w',''],
				[9,19,'n',''],
				[9,20,'e','metal'],
				[11,20,'n','metal'],
				[11,25,'e','metal'],
				[12,25,'n','metal'],
				[12,24,'w',''],
				[16,24,'n',''],
				[16,25,'e','metal'],
				[17,25,'n','metal'],
				[17,20,'w','metal']]).concat(
				fill(6,20,8,20,'metal'),
				fill(21,21,22,21,'metal'),
				fill(6,21,10,28,'metal'),
				fill(11,26,17,28,'metal'),
				fill(18,21,20,28,'metal'),
				fill(23,21,34,28,'metal'),
				fill(13,25,15,25,'metal'),
				fill(35,24,35,25,'metal'),
				[[7,23,'chapter-6/metal/c2'],
				[7,27,'chapter-6/metal/c2'],
				[12,27,'chapter-6/metal/c2'],
				[16,27,'chapter-6/metal/c2'],
				[19,23,'chapter-6/metal/c2'],
				[25,23,'chapter-6/metal/c2'],
				[25,27,'chapter-6/metal/c2'],
				[29,27,'chapter-6/metal/c2'],
				[29,23,'chapter-6/metal/c2'],
				[33,27,'chapter-6/metal/c2'],
				[33,23,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(4 * 2, 19.5 * 2), Qt.point(35.5 * 2, 19.5 * 2), Qt.point(35.5 * 2, 29.5 * 2), Qt.point(4 * 2, 29.5 * 2)]
				group: root.open_corridor_7 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_7
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(34 *  chapter_scale*2, 28 *  chapter_scale * 2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_8 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + console_7.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_7 ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: root.open_corridor_8 ?  0 : groups.sensor
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_hidden
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(23 *  chapter_scale*2, 28 *  chapter_scale * 2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_hidden_room = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-6/console/" + console_hidden.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_7 ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: root.open_hidden_room ?  0 : groups.sensor
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_8
			z: altitudes.background_near
			mask: root.open_corridor_8 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[46,20,'n','metal'],
				[46,29,'e','metal'],
				[36,29,'s','metal'],
				[36,26,'w','metal'],
				[35,26,'s',''],
				[35,23,'w',''],
				[36,23,'n','metal'],
				[36,20,'w','metal']]).concat(
				fill(36,24,36,25,'metal'),
				fill(37,21,45,28,'metal'),
				[[38,22,'chapter-6/metal/c2'],
				[42,22,'chapter-6/metal/c2'],
				[38,27,'chapter-6/metal/c2'],
				[42,27,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(35.5 * 2, 19.5 * 2), Qt.point(46.5 * 2, 19.5 * 2), Qt.point(46.5 * 2, 29.5 * 2), Qt.point(35.5 * 2, 29.5 * 2)]
				group: root.open_corridor_8 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Tiles
		{
			id: hidden_room
			z: altitudes.background_near
			mask: root.open_hidden_room ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[16,20,'n','metal'],
				[16,24,'e','metal'],
				[16,25,'e',''],
				[12,25,'s',''],
				[12,20,'w','metal']]).concat(
				fill(13,21,15,24,'metal'),
				[[14,22,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(11.5 * 2, 19.5 * 2), Qt.point(16.5 * 2, 19.5 * 2), Qt.point(16.5 * 2, 24.5 * 2), Qt.point(11.5 * 2, 24.5 * 2)]
				group: root.open_hidden_room ? 0 : groups.wall
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: secret
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 100
			type:  Body.STATIC
			position: Qt.point(14 *  chapter_scale*2, 21 *  chapter_scale * 2)
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_secret()
					indice = 24
					active = false
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-6/console/" + secret.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_hidden_room ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: secret.active ? groups.sensor : 0
				sensor: true
			}
		}
		
		Ship
		{
			id: ship	
			position.x: 5.5 * 4
			position.y: 12 * 4
			angle: Math.PI/2
			Q.Component.onDestruction: 
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
			
		}
			
		Body
		{
			id: all_wall
			position.y: 0
			position.x: 0
			scale: chapter_scale*2
			
			type: Body.STATIC
			
			function point(x,y)
			{
				return Qt.point(x - 0.25 , y + 0.25)
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(5,0), all_wall.point(56 + 2*0.25,0), all_wall.point(56 + 2 * 0.25, 10 - 2 *0.25), all_wall.point(5,10 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(56 + 2 * 0.25,0), all_wall.point(66,0), all_wall.point(66,49), all_wall.point(56 + 2 * 0.25, 49)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(5,39), all_wall.point(56 + 2 * 0.25,39), all_wall.point(56 + 2 * 0.25,49), all_wall.point(5,49)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(-5,0), all_wall.point(5,0), all_wall.point(5 ,49), all_wall.point(-5,49)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(5,14), all_wall.point(52,14), all_wall.point(52,15 - 2 * 0.25), all_wall.point(5 ,15 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(52 ,15 - 2 * 0.25), all_wall.point(52,35 - 2 * 0.25), all_wall.point(51 + 2 * 0.25,35 - 2 * 0.25), all_wall.point(51 + 2 * 0.25 , 15 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(51  + 2*0.25 ,34), all_wall.point(51  + 2*0.25, 35 - 2 * 0.25), all_wall.point(9 + 2*0.25, 35 - 2 * 0.25), all_wall.point(9 + 2*0.25, 34)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(5 ,29), all_wall.point(47, 29), all_wall.point(47, 30 - 2 * 0.25), all_wall.point(5, 30 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(47 ,19), all_wall.point(47, 29), all_wall.point(46 + 2 * 0.25, 29), all_wall.point(46 + 2 * 0.25, 19)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(9 + 2 * 0.25 ,19), all_wall.point(46 + 2 * 0.25, 19), all_wall.point(46 + 2 * 0.25, 20 - 2 * 0.25), all_wall.point(9 + 2 * 0.25, 20 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(11 + 2 * 0.25 ,20 - 2 * 0.25), all_wall.point(12, 20 - 2 * 0.25), all_wall.point(12, 25 - 2 * 0.25), all_wall.point(11 + 2 * 0.25, 25 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(16 + 2 * 0.25 ,20 - 2 * 0.25), all_wall.point(17, 20 - 2 * 0.25), all_wall.point(17, 25 - 2 * 0.25), all_wall.point(16 + 2 * 0.25, 25 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(21 + 2 * 0.25 ,22), all_wall.point(22, 22 ), all_wall.point(22, 29), all_wall.point(21 + 2 * 0.25, 29)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(35 + 2 * 0.25 ,20 - 2 * 0.25), all_wall.point(36, 20 - 2 * 0.25 ), all_wall.point(36, 23 - 2 * 0.25), all_wall.point(35 + 2 * 0.25, 23 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(35 + 2 * 0.25 ,26), all_wall.point(36, 26 ), all_wall.point(36, 29), all_wall.point(35 + 2 * 0.25, 29)]
				group: groups.wall
			}
		}
		
		Body
		{
			id: barriere_1
			type: Body.STATIC
			property real x:52
			property real y:13
			property int nb_repe: 18
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_1.x - 0.625)*2*chapter_scale
						position.y: (barriere_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: root.open_corridor_2 ? Qt.rgba(0.58, 0, 0.82, 0) : Qt.rgba(0.58, 0, 0.82, 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_1.coords(barriere_1.x - 0.75, barriere_1.y + 1.25 - ( barriere_1.nb_repe + 1 ) / 4),
				barriere_1.coords(barriere_1.x - 0.25, barriere_1.y + 1.25 - ( barriere_1.nb_repe + 1 ) / 4), 
				barriere_1.coords(barriere_1.x - 0.25, barriere_1.y + 1.25), 
				barriere_1.coords(barriere_1.x - 0.75, barriere_1.y + 1.25)
				]
				group:  root.open_corridor_2 ? 0 : groups.enemy_invincible
			}
		}
		
		
		Body
		{
			id: barriere_2
			type: Body.STATIC
			property real x:52
			property real y:38
			property int nb_repe: 18
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_2.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_2.x - 0.375)*2*chapter_scale
						position.y: (barriere_2.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_3 ? 0 : (root.open_corridor_2 ? 1 : 0)) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_2.coords(barriere_2.x - 0.75, barriere_2.y + 1.25 - ( barriere_2.nb_repe + 1 ) / 4),
				barriere_2.coords(barriere_2.x - 0.25, barriere_2.y + 1.25 - ( barriere_2.nb_repe + 1 ) / 4), 
				barriere_2.coords(barriere_2.x - 0.25, barriere_2.y + 1.25), 
				barriere_2.coords(barriere_2.x - 0.75, barriere_2.y + 1.25)
				]
				group:  root.open_corridor_3 ? 0 : groups.enemy_invincible
			}
		}
		
		Body
		{
			id: barriere_3
			type: Body.STATIC
			property real x: 8
			property real y: 34
			property int nb_repe: 18
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_3.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_3.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_3.y + 0.625)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_4 ? 0 : (root.open_corridor_3 ? 1 : 0)) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_3.coords(barriere_3.x + 1.25 - barriere_3.nb_repe / 4, barriere_3.y + 0.25), 
				barriere_3.coords(barriere_3.x + 1.25, barriere_3.y + 0.25), 
				barriere_3.coords(barriere_3.x + 1.25, barriere_3.y + 0.75), 
				barriere_3.coords(barriere_3.x + 1.25 - barriere_3.nb_repe / 4, barriere_3.y + 0.75)
				]
				group: root.open_corridor_4 ? 0 : groups.enemy_invincible
			}
		}
		
		Body
		{
			id: barriere_4
			type: Body.STATIC
			property real x:47
			property real y:33
			property int nb_repe: 18
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			Repeater
			{
				count: barriere_4.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_4.x - 0.625)*2*chapter_scale
						position.y: (barriere_4.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_5 ? 0 : (root.open_corridor_4 ? 1 : 0)) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_4.coords(barriere_4.x - 0.75, barriere_4.y + 1.25 - ( barriere_4.nb_repe + 1 ) / 4),
				barriere_4.coords(barriere_4.x - 0.25, barriere_4.y + 1.25 - ( barriere_4.nb_repe + 1 ) / 4), 
				barriere_4.coords(barriere_4.x - 0.25, barriere_4.y + 1.25), 
				barriere_4.coords(barriere_4.x - 0.75, barriere_4.y + 1.25)
				]
				group:  root.open_corridor_5 ? 0 : groups.enemy_invincible
			}
		}
		
		Body
		{
			id: barriere_5
			type: Body.STATIC
			property real x:47
			property real y:18
			property int nb_repe: 18
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_5.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_5.x - 0.375)*2*chapter_scale
						position.y: (barriere_5.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_6 ? 0 : (root.open_corridor_5 ? 1 : 0)) 
					}
				}
			}
			
			
			PolygonCollider
			{
				vertexes: [
				barriere_5.coords(barriere_5.x - 0.75, barriere_5.y + 1.25 - ( barriere_5.nb_repe + 1 ) / 4),
				barriere_5.coords(barriere_5.x - 0.25, barriere_5.y + 1.25 - ( barriere_5.nb_repe + 1 ) / 4), 
				barriere_5.coords(barriere_5.x - 0.25, barriere_5.y + 1.25), 
				barriere_5.coords(barriere_5.x - 0.75, barriere_5.y + 1.25)
				]
				group:  root.open_corridor_6 ? 0 : groups.enemy_invincible
			}
		}
		
		Body
		{
			id: barriere_6
			type: Body.STATIC
			property real x: 8
			property real y: 19
			property int nb_repe: 18
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_6.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_6.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_6.y+0.375)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_7 ? 0 : (root.open_corridor_6 ? 1 : 0)) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_6.coords(barriere_6.x + 1.25 - barriere_6.nb_repe / 4, barriere_6.y + 0.25), 
				barriere_6.coords(barriere_6.x + 1.25, barriere_6.y + 0.25), 
				barriere_6.coords(barriere_6.x + 1.25, barriere_6.y + 0.75), 
				barriere_6.coords(barriere_6.x + 1.25 - barriere_6.nb_repe / 4, barriere_6.y + 0.75)
				]
				group: root.open_corridor_7 ? 0 : groups.enemy_invincible
			}
		}
		
		
		Body
		{
			id: barriere_7
			type: Body.STATIC
			property real x:36
			property real y:25
			property int nb_repe: 14
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_7.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_7.x - 0.625)*2*chapter_scale
						position.y: (barriere_7.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_8 ? 0 : (root.open_corridor_7 ? 1 : 0)) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_7.coords(barriere_7.x - 0.75, barriere_7.y + 1.25 - ( barriere_7.nb_repe + 1 ) / 4),
				barriere_7.coords(barriere_7.x - 0.25, barriere_7.y + 1.25 - ( barriere_7.nb_repe + 1 ) / 4), 
				barriere_7.coords(barriere_7.x - 0.25, barriere_7.y + 1.25), 
				barriere_7.coords(barriere_7.x - 0.75, barriere_7.y + 1.25)
				]
				group:  root.open_corridor_8 ? 0 : groups.enemy_invincible
			}
		}
		
		
		Body
		{
			id: barriere_hidden
			type: Body.STATIC
			property real x: 15
			property real y: 24
			property int nb_repe: 18
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_hidden.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_hidden.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_hidden.y + 0.625)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_hidden_room ? 0 : (root.open_corridor_7 ? 1 : 0)) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_hidden.coords(barriere_hidden.x + 1.25 - barriere_hidden.nb_repe / 4, barriere_hidden.y + 0.25), 
				barriere_hidden.coords(barriere_hidden.x + 1.25, barriere_hidden.y + 0.25), 
				barriere_hidden.coords(barriere_hidden.x + 1.25, barriere_hidden.y + 0.75), 
				barriere_hidden.coords(barriere_hidden.x + 1.25 - barriere_hidden.nb_repe / 4, barriere_hidden.y + 0.75)
				]
				group: root.open_hidden_room ? 0 : groups.enemy_invincible
			}
		}
		
		Sensor
		{
			id: exit
			type: Body.STATIC
			property real x:44.5
			property real y:24.5
			scale: 4
			position: exit.coords(x,y)
			property int indice: 0
			property bool changed: true
			property real ti_local: root.ti
			property bool open : false
			
			onTi_localChanged:
			{
				if(root.open_sortie)
				{
					if(changed)
					{
						if(indice < 24)
						{
							indice++
						}
						else
						{
							open = true
						}
						changed = false
					}
					else
					{
						changed = true
					}
				}
			}
			
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Image
			{
				material: "chapter-6/exit/" + exit.indice
				z: altitudes.boss
				mask:  root.open_corridor_8 ? "white" : "transparent"
			}
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie()
				}
			}
			PolygonCollider
			{
				vertexes: [
				Qt.point(- 1 , - 1),
				Qt.point(1 , - 1),
				Qt.point(1 , 1),
				Qt.point(- 1 , 1)
				]
				group:  exit.open ? groups.sensor : 0
				sensor : true
			}
		}		
	}
	
	Q.Component
	{
		id: hidden_boss_factory
		Vehicle
		{
			id: vauban
			icon: 5
			icon_color: "red"
			max_speed: 0
			max_angular_speed: 0.4
			type: Body.KINEMATIC
			//weapon_regeneration: 5
			scale: 10
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			Q.Component.onCompleted: shield = max_shield
			loot_factory: Repeater
			{
				count: 31
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "chapter-2/boss-cache"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: Qt.rgba(0, 0.4, 1, vauban.shield / vauban.max_shield)
				scale: 1.1
				z: altitudes.shield
			}

			CircleCollider
			{
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
				radius : 0.8
			}

			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				radius: 1.1
			}
			Behaviour_attack_rotate
			{
				target_groups: groups.enemy_targets
				target_distance: 10
			}
			EquipmentSlot
			{
				scale: 0.1
				equipment: Shield
				{
					level: 2
				}
			}
			Repeater
			{
				count: 5
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .22
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .22
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Shield
						{
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .74
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .74
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: LaserGun
						{
							shooting: true
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .47
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .47
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.08
						equipment: Quipoutre
						{
							shooting: true
							period: scene.seconds_to_frames(2)
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index - .025) * 72 * Math.PI / 180) * .66
						position.y: Math.sin((index - .025) * 72 * Math.PI / 180) * .66
						angle: (index - .01) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Gun
						{
							shooting: true
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .525) * 72 * Math.PI / 180) * .66
						position.y: Math.sin((index + .525) * 72 * Math.PI / 180) * .66
						angle: (index + .51) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Gun
						{
							shooting: true
							level: 2
						}
					}
				}
			}
		}
	}
	
	Q.Component
	{
		id: turret_factory
		Turret
		{
		
		}
	}
	
	Q.Component
	{
		id: shield_barriere_factory
		Vehicle
		{
			id: shield_1
			type: Body.STATIC
			property real x:52
			property real y:13
			property int nb_repe: 18
			property var tab_shield:[] // x,y
			property var tab_shield_alive:[] // x,y
			property var tab_collider: []
			property real ti_local: root.ti
			property color mask:  Qt.rgba(0, 0.4, 1, shield_1.shield ? Math.max(0.2, shield_1.shield / shield_1.max_shield) : 0)
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			
			onTi_localChanged:
			{
				var alive = false
				for(var i = 0; i < tab_collider.length; i++)
				{
					if(!tab_shield_alive[i])
					{
						tab_collider[i].group = 0
					}
					else
					{
						alive = true
					}
				}
				if(tab_collider.length > 0 && !alive)
				{
					destroy()
				}
			}
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			
			Q.Component.onCompleted:
			{
				shield_1.shield = max_shield
			}
			
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.375)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: shield_1.mask
					}
				}
			}
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.625)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: shield_1.mask
					}
				}
			}
			
			Repeater
			{
				count: shield_1.tab_shield.length
				Q.Component
				{
					EquipmentSlot
					{
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						equipment: Shield
						{
							level: 2
							material_image: "chapter-2/shield-generator"
							Q.Component.onCompleted:
							{
								shield_1.tab_shield_alive.push(true)
							}
							
							Q.Component.onDestruction: 
							{
								shield_1.tab_shield_alive[index] = false
							}
						}
					}
				}
				Q.Component
				{
					CircleCollider
					{
						id: collider
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						group: shield_1.tab_shield_alive[index] ? groups.enemy_hull_naked : 0
						radius: 0.5
						
						Q.Component.onCompleted:
						{
							shield_1.tab_collider.push(collider)
						}
						
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4),
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4), 
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25), 
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25)
				]
				group: shield_1.shield ? groups.enemy_shield : 0
			}
		}
	}
	
	
	Q.Component
	{
		id: barriere_last_room_factory
		Vehicle
		{
			id: shield_1
			type: Body.STATIC
			property real x:52
			property real y:13
			property int nb_repe: 18
			property var tab_shield:[] // x,y
			property var tab_shield_alive:[] 
			property var tab_collider: []
			property real ti_local: root.ti
			property bool reflection: true
			property real time_to_reflec: 3
			property real deb_reflec: 0
			property bool protec: false
			property real time_to_trans: 5
			property real deb_trans: 0
			property bool transition: false
			property real time_to_protec: 3
			property real deb_protec: 0
			property color mask: Qt.rgba(1, 1, 0, 1)
			property bool alive: true
			property int sens: -1
			onAliveChanged: if(!alive) destroy()
			
			Q.Component.onCompleted:
			{
				deb_reflec = root.ti
			}
						
			onTi_localChanged:
			{
				var alive = false
				for(var i = 0; i < tab_collider.length; i++)
				{
					if(!tab_shield_alive[i])
					{
						tab_collider[i].group = 0
					}
					else
					{
						alive = true
					}
				}
				if(tab_collider.length > 0 && !alive)
				{
					destroy()
				}
				else
				{
					if(reflection)
					{
						if(deb_reflec - ti_local > time_to_reflec)
						{
							reflection = false
							transition = true
							deb_trans = ti_local
							sens = -1
						}
					}
					if(transition)
					{
						//Qt.rgba(0.58, 0, 0.82, 1) protec
						//Qt.rgba(1, 1, 0, 1) reflection
						
						if(deb_trans - ti_local > time_to_trans)
						{
							if(sens == 1)
							{
								reflection = true
								transition = false
								deb_reflec = ti_local
							}
							if(sens == -1)
							{
								protec = true
								transition = false
								deb_protec = ti_local
							}
						}
						else
						{
							if(sens == 1)
							{
								mask.g = (deb_trans - ti_local) / time_to_trans
								mask.r = 0.58 + 0.42 * (deb_trans - ti_local) / time_to_trans
								mask.b = 0.82 * (1 - (deb_trans - ti_local) / time_to_trans)
							}
							else
							{
								mask.g = 1 - (deb_trans - ti_local) / time_to_trans
								mask.r = 1 - 0.42 * (deb_trans - ti_local) / time_to_trans
								mask.b = 0.82 * (deb_trans - ti_local) / time_to_trans
							}
						}
					}
					if(protec)
					{
						if(deb_protec - ti_local > time_to_protec)
						{
							protec = false
							transition = true
							deb_trans = ti_local
							sens = 1
						}
					}
				}
			}
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.375)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: shield_1.mask
					}
				}
			}
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.625)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: shield_1.mask
					}
				}
			}
			
			Repeater
			{
				count: shield_1.tab_shield.length
				Q.Component
				{
					EquipmentSlot
					{
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						equipment: Energizer
						{
							Q.Component.onCompleted:
							{
								shield_1.tab_shield_alive.push(true)
							}
							Q.Component.onDestruction: 
							{
								shield_1.tab_shield_alive[index] = false
							}
						}
					}
				}
				Q.Component
				{
					CircleCollider
					{
						id: collider
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						group: shield_1.tab_shield_alive[index] ? groups.item_ignore_enemy : 0
						radius: 2
						
						Q.Component.onCompleted:
						{
							shield_1.tab_collider.push(collider)
						}
						
					}
				}
			}
			
			function begin(other)
			{
				var v = direction_from_scene(other.velocity)
				other.velocity = direction_to_scene(Qt.point(-v.x, v.y))
				var a = 2 * angle - other.angle + Math.PI
				var e = scene.time_changed
				function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
				e.connect(f)
				for(var i in other.children)
				{
					var child = other.children[i]
					if(child.group) child.group ^= 1
					if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
				}
			}
			
			Q.Component.onDestruction: 
			{
				root.generator_down = true
			}
			
			PolygonCollider
			{
				vertexes: [
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4),
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4), 
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25), 
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25)
				]
				group: shield_1.reflection ? groups.reflector_only_player : (shield_1.protec ? groups.enemy_invincible : 0)
			}
		}
	}
	
	Q.Component
	{
		id: mini_boss_factory
		Vehicle
		{
			function local_coords(x, y) {return Qt.point(x / 540 - 1, y / 540 - 1)} // images en 1080*1080
			function group_of(x) {return x ? shield ? groups.enemy_hull : groups.enemy_hull_naked : 0}
			id: mini_boss
			icon: 5
			icon_color: "red"
			type: Body.STATIC
			scale: 6
			Q.Component.onCompleted: shield = max_shield
			property bool onfire: behaviour.shooting
			max_speed: 0
			max_angular_speed: 0
			property bool shield_1: true
			property bool shield_2: true
			property bool shield_3: true
			property bool quipoutre_1: true
			property bool quipoutre_2: true
			property bool quipoutre_3: true
			property bool live: true
			onLiveChanged: if(!live) destroy()
			
			property bool alive: shield_1 || shield_2 || shield_3 || quipoutre_1 || quipoutre_2 || quipoutre_3
			onAliveChanged: if(!alive) destroy()
			loot_factory: Repeater
			{
				count: 10
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "chapter-2/mini-boss-ss"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: Qt.rgba(0, 0.4, 1, mini_boss.shield / mini_boss.max_shield)
				position: local_coords(540, 717)
				scale: 510/540
				z: altitudes.shield
			}
			Behaviour_attack_distance
			{
				id: behaviour
				target_groups: groups.enemy_targets
				max_distance: 40
			}
			CircleCollider
			{
				position: local_coords(540, 717)
				group: shield ? groups.enemy_shield : 0
				radius: 510/540
			}

			CircleCollider
			{
				group: group_of(quipoutre_1)
				radius: 80/540
				position: local_coords(223, 540)
			}
			EquipmentSlot
			{
				position: local_coords(223, 540)
				scale: 0.15*6/mini_boss.scale
				angle: Math.PI * -1/4
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_1 = false
				}
			}

			CircleCollider
			{
				group: group_of(quipoutre_2)
				radius: 80/540
				position: local_coords(540, 540)
			}
			EquipmentSlot
			{
				position: local_coords(540, 540)
				scale: 0.15*6/mini_boss.scale
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_2 = false
				}
			}

			CircleCollider
			{
				group: group_of(quipoutre_3)
				radius: 80/540
				position: local_coords(854, 540)
			}
			EquipmentSlot
			{
				position: local_coords(854, 540)
				scale: 0.15*6/mini_boss.scale
				angle: Math.PI * 1/4
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_3 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_1)
				radius: 60/540
				position: local_coords(303, 848)
			}
			EquipmentSlot
			{
				position: local_coords(303, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_1 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_2)
				radius: 60/540
				position: local_coords(540, 848)
			}
			EquipmentSlot
			{
				position: local_coords(540, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_2 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_3)
				radius: 60/540
				position: local_coords(776, 848)
			}
			EquipmentSlot
			{
				position: local_coords(776, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_3 = false
				}
			}
		}
	}
	
	
	
	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState:  state_test_1 // state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState:  state_dialogue_0
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					music.objectName = "chapter-6/inside"
					camera_position= undefined
					for(var i = 0; i< tab_turret_corridor_1.length; i++)
					{
						var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_1[i][0] * 2 * root.chapter_scale, tab_turret_corridor_1[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_1[i][2], angle: tab_turret_corridor_1[i][3] * Math.PI, activate: true, invulnerable: tab_turret_corridor_1[i][5]})
						tur.alive =   Qt.binding(tab_turret_corridor_1[i][4])
					}
					var shield = shield_barriere_factory.createObject(null, {scene: ship.scene, x: 8, y: 13, tab_shield:[[14,11],[14,12],[14,13]] })
					shield.alive = Qt.binding(tab_turret_corridor_1[0][4])
					shield = shield_barriere_factory.createObject(null, {scene: ship.scene, x: 15.5, y: 13, tab_shield:[[23.75,10.75],[23.75,11.75],[23.75,12.75],[24.25,11.25],[24.25,12.25],[24.25,13.25]] })
					shield.alive = Qt.binding(tab_turret_corridor_1[0][4])
					messages.add("nectaire/normal", qsTr("begin 1"))
					messages.add("lycop/normal", qsTr("begin 2"))
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_1; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState:  state_dialogue_1
				signal: root.fuzzy_compChanged
				guard: root.fuzzy_comp > 0
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("console 1"))
					messages.add("lycop/normal", qsTr("console 2"))
				}
			}
		}
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_2; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: open_secret
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("secret 1"))
					messages.add("lycop/normal", qsTr("secret 2"))
					messages.add("nectaire/normal", qsTr("secret 3"))
					if(secret_foreuse)
					{
						messages.add("nectaire/normal", qsTr("secret_foreuse ok"))						
						if(secret_base_heter)
						{
							messages.add("nectaire/normal", qsTr("secret_base_heter ok"))					
							if(secret_cathedrale)
							{
								messages.add("nectaire/normal", qsTr("secret_cathedrale ok"))
							}
							else
							{
								messages.add("nectaire/normal", qsTr("secret_cathedrale pas ok"))
							}
						}
						else
						{
							messages.add("nectaire/normal", qsTr("secret_base_heter pas ok"))
						}
					}
					else
					{
						messages.add("nectaire/normal", qsTr("secret_foreuse pas ok"))
					}
					
					if(secret_base_heter && secret_cathedrale && secret_foreuse)
					{
						messages.add("lycop/normal", qsTr("secret debloque 1"))
						messages.add("nectaire/normal", qsTr("secret debloque 2"))
						messages.add("lycop/normal", qsTr("secret debloque 3"))
						messages.add("nectaire/normal", qsTr("secret debloque 4"))
						messages.add("lycop/normal", qsTr("secret debloque 5"))
						messages.add("nectaire/normal", qsTr("secret debloque 6"))
						messages.add("lycop/normal", qsTr("secret debloque 7"))
						messages.add("nectaire/normal", qsTr("secret debloque 8"))
						messages.add("lycop/normal", qsTr("secret debloque 9"))
						root.secret_commandement = true
					}
					else
					{
						messages.add("lycop/normal", qsTr("secret pas debloque 1"))
					}
				}
			}
			
			SignalTransition
			{
				targetState: state_story_2
				signal: root.generator_downChanged
				guard: root.generator_down
				onTriggered:
				{
					root.open_sortie = true
				}
			}
			
			SignalTransition
			{
				targetState: state_end
				signal: sortie
				onTriggered:
				{
					saved_game.set("secret_commandement", secret_commandement)
					if(secret_commandement)
					{
						platform.set_bool("secret-commandement")
					}
					saved_game.add_science(1)
					saved_game.set("file", "game/chapter-6/chapter-6-boss.qml")
					saved_game.save()
				}
			}
		}
		State {id: state_dialogue_2; SignalTransition {targetState: state_story_2; signal: state_dialogue_2.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	
	Q.Component.onCompleted:
	{	
		for(var i = 0; i < 15; ++i) jobs.run(scene_view, "preload", ["chapter-2/turret-opening/" + i])
		var a2 = ["c", "e", "n", "ne", "nei", "nw", "nwi", "s", "se", "sei", "sw", "swi", "w"]; for(var i2 in a2) jobs.run(scene_view, "preload", ["chapter-2/metal/" + a2[i2]])
		jobs.run(scene_view, "preload", ["chapter-2/generator-1"])
		jobs.run(scene_view, "preload", ["chapter-6/starship/outdoor-masked"])
		
		jobs.run(music, "preload", ["chapter-6/inside"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-6/exit/" + i])
		jobs.run(scene_view, "preload", ["chapter-2/shield-generator"])
		jobs.run(scene_view, "preload", ["chapter-2/barrier-wall"])
		jobs.run(scene_view, "preload", ["chapter-2/square-turret"])
		jobs.run(scene_view, "preload", ["chapter-2/mini-boss-ss"])
		jobs.run(scene_view, "preload", ["chapter-2/boss-cache"])
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-6/console/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		groups.init(scene)
		//root.info_print = screen_info_factory.createObject(screen_hud)
		//root.finalize = function() {if(info_print) info_print.destroy()}
	}
}