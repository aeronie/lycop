import QtQuick 2.0 as Q
import aw.game 0.0

Equipment
{
	id: equipment_1
	health: max_health
	max_health: 4000
	Image
	{
		material: "chapter-2/generator-1"
		mask: Qt.rgba(1, equipment_1.health / equipment_1.max_health, equipment_1.health / equipment_1.max_health, 1)
		scale: 2
	}
}