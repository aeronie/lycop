import QtQuick 2.0 as Q
import aw.game 0.0

Weapon
{
	property int level: 1
	property int bullet_group: [groups.enemy_shield_canceler_2,groups.shield_canceler_1,groups.shield_canceler_2][level]
	period: scene.seconds_to_frames(1)
	health: max_health
	max_health: 200 +~~body.health_boost
	range_bullet: 20
	image: Image
	{
		material: "equipments/shield-canceler"
		scale: 0.5
		mask: mask_equipment(parent)
	}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/shield-canceler/shooting"
		scale: slot.sound_scale
	}
	bullet_factory: Shockwave
	{
		Q.Component.onCompleted: weapon.sound_shooting.play()
		position: weapon.slot.point_to_scene(Qt.point(0, 0))
		scale: 0.1
		target_scale: weapon.range_bullet
		duration: scene.seconds_to_frames(0.2)
		onScaleChanged: collider.update_transform()
		Image
		{
			material: "equipments/shield"
			mask: Qt.rgba(255, 0, 255, 0.2)
		}
		CircleCollider
		{
			id: collider
			group: weapon.bullet_group
			sensor: true
		}
	}
}
