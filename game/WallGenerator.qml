import QtQuick 2.0 as Q
import aw.game 0.0

Weapon
{
	property int level: 1
	property int wall_count: 0
	signal wall_destroyed()
	onWall_destroyed: --wall_count
	period: scene.seconds_to_frames([0 , 1 / 3 , 1 / 3] [level])
	health: max_health
	max_health: 200 +~~body.health_boost
	consumable: Consumable {available: wall_count < 3}
	image: Image
	{
		material: "equipments/wall-generator"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/wall-generator/shooting"
		scale: slot.sound_scale
	}
	bullet_factory: Mine
	{
		Q.Component.onCompleted:
		{
			++weapon.wall_count
			Q.Component.destruction.connect(weapon.wall_destroyed)
			position = weapon.slot.point_to_scene(Qt.point(0, 0))
			angle = weapon.slot.angle_to_scene(0)
			weapon.sound_shooting.play()
			weapon.set_bullet_velocity(this, 70)

			var group = [groups.reflector_only_player, groups.reflector_1, groups.reflector_2][weapon.level]
			launch_time_changed.connect(function(x)
			{
				image.scale = 1 - x / scene.seconds_to_frames(1)
				if(!x)
				{
					collider.group = group
					image.mask = "white"
				}
			})

			activation_time_changed.connect(function(x)
			{
				if(!x) deleteLater()
			})
		}
		scale: 2
		damping: 10
		launch_time: scene.seconds_to_frames(0.5)
		activation_time: scene.seconds_to_frames(6)
		loot_factory: Explosion {scale: 2}
		Image
		{
			id: image
			material: "equipments/wall-generator/bullet"
			scale: 0
		}
		PolygonCollider
		{
			id: collider
			vertexes: [Qt.point(-1, -0.125), Qt.point(1, -0.125), Qt.point(1, 0.125), Qt.point(-1, 0.125)]
			density: 1e30
		}
		function begin(other)
		{
			var v = direction_from_scene(other.velocity)
			other.velocity = direction_to_scene(Qt.point(v.x, -v.y))
			var a = 2 * angle - other.angle + Math.PI
			var e = scene.time_changed
			function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
			e.connect(f)
			for(var i in other.children)
			{
				var child = other.children[i]
				if(child.group) child.group ^= 1
				if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
			}
		}
	}
}
