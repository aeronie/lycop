#include "Body.h"
#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct Sensor
: Body
, Observer<1> // timer
{
	AW_DECLARE_OBJECT_STUB(Sensor)
	AW_DECLARE_PROPERTY_STORED(int, duration_in) = 0;
	AW_DECLARE_PROPERTY_STORED(int, duration_out) = 0;
	AW_DECLARE_PROPERTY_STORED(bool, target_value) = false;
	AW_DECLARE_PROPERTY_STORED_READONLY(bool, value) = false;
	AW_DECLARE_PROPERTY_STORED_READONLY(float, fuzzy_value) = 0;
	int time_ = 0;

	void update(const Observer<-1>::Tag &tag) override
	{
		Body::update(tag);

		if(body_)
		{
			Observer<1>::connect(scene()->signal_frame_begin());
		}
	}

	void update(const Observer<1>::Tag &) override
	{
		if(time_)
		{
			--time_;
			update_fuzzy_value();
		}
		else if(value_ != target_value_)
		{
			value_ = target_value_;
			Q_EMIT value_changed(value_);
		}
	}

	void update_fuzzy_value()
	{
		fuzzy_value_ = target_value_ ? (duration_in_ ? 1 - float(time_) / float(duration_in_) : 1) : (duration_out_ ? float(time_) / float(duration_out_) : 0);
		Q_EMIT fuzzy_value_changed(fuzzy_value_);
	}
};

void Sensor::set_target_value(bool const &value)
{
	if(target_value_ == value) return;
	target_value_ = value;
	if(target_value_ == value_ && !time_) {} // repos si (target ∧ value ∧ ¬ time) ∨ (¬ target ∧ ¬ value ∧ ¬ time)
	else if(target_value_ && (!value_ || time_)) time_ = duration_in_ - (duration_out_ ? time_ * duration_in_ / duration_out_ : 0); // montée si (target ∧ value ∧ time) ∨ (target ∧ ¬ value ∧ time) ∨ (target ∧ ¬ value ∧ ¬ time)
	else time_ = duration_out_ - (duration_in_ ? time_ * duration_out_ / duration_in_ : 0); // descente si (¬ target ∧ value ∧ time) ∨ (¬ target ∧ value ∧ ¬ time) ∨ (¬ target ∧ ¬ value ∧ time)
	update_fuzzy_value();
	Q_EMIT target_value_changed(target_value_);
}

AW_DEFINE_OBJECT_STUB(Sensor)
AW_DEFINE_PROPERTY_STORED(Sensor, duration_in)
AW_DEFINE_PROPERTY_STORED(Sensor, duration_out)

}
}
}
