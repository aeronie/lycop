#include "Battery.h"
#include "meta.private.h"

namespace aw {
namespace game {

AW_DEFINE_OBJECT_STUB(Battery)
AW_DEFINE_PROPERTY_STORED(Battery, max_energy)

}
}
