#pragma once

#include "Equipment.h"

namespace aw {
namespace game {

struct SecondaryShield
: Equipment
{
	AW_DECLARE_OBJECT_STUB(SecondaryShield)
	AW_DECLARE_PROPERTY_STORED(float, max_shield) = 0;
protected:
	SecondaryShield() = default;
	~SecondaryShield() = default;
};

}
}
