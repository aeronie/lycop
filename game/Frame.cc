#include "Frame.h"
#include "meta.private.h"

namespace aw {
namespace game {

AW_DEFINE_PROPERTY_STORED(Frame, position)
AW_DEFINE_PROPERTY_STORED(Frame, angle)
AW_DEFINE_PROPERTY_STORED(Frame, scale)

}
}
