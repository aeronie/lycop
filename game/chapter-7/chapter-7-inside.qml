import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-7/chapter-7-inside.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal open_secret
	signal sortie
	signal on_sortie
	property var camera_position: Qt.point(17 * 4, 48 * 4)
	property var last_ship_position: Qt.point(0, 0)
	property var barriere_bas
	property real ti: 0
	property var info_print
	property real chapter_scale: 2
	property bool open_corridor_2: false
	property bool travel_map_2: true
	property bool open_corridor_3: false
	property bool open_corridor_4: false
	property bool open_corridor_5: false
	property bool open_sortie: security_1 && security_2 && security_3 && security_4
	property bool security_1: false
	property bool security_2: false
	property bool security_3: false
	property bool security_4: false
	property bool can_open: true
	property real fuzzy_comp: 0
	
	property int open_ind: 0
	
	
	onSecurity_1Changed:
	{
		if(security_1)
		{
			can_open = true
		}
	}
	onSecurity_2Changed:
	{
		if(security_2)
		{
			can_open = true
		}
	}
	onSecurity_3Changed:
	{
		if(security_3)
		{
			can_open = true
		}
	}
	onSecurity_4Changed:
	{
		if(security_4)
		{
			can_open = true
		}
	}
	
	
	Q.Component
	{
		id: heavy_laser_gun_factory
		HeavyLaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: harpoon_factory
		HarpoonLauncher
		{
			range_bullet: scene.seconds_to_frames(10)
			period: scene.seconds_to_frames(6)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: missile_launcher_factory
		MissileLauncher
		{
			range_bullet: scene.seconds_to_frames(2)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: secondary_shield_factory
		SecondaryShield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: random_spray_factory
		RandomSpray
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: false
			enemy: 1
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: false
			period: scene.seconds_to_frames(3)
			level: 2
		}
	}
	
	onOpen_corridor_2Changed:
	{
		if(open_corridor_2)
		{
			open_ind++
			boss_1.createObject(null, {scene: ship.scene, position: Qt.point(26.5 * 2 * chapter_scale ,30.5 * 2 * chapter_scale)})
		}
	}
	
	onOpen_corridor_3Changed:
	{
		if(open_corridor_3)
		{
			open_ind++
			var boss = hidden_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(42 * 2 * chapter_scale, 25 * 2 * chapter_scale)})
			boss = hidden_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(42 * 2 * chapter_scale, 37 * 2 * chapter_scale)})
			boss = hidden_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(56 * 2 * chapter_scale, 25 * 2 * chapter_scale)})
			boss = hidden_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(56 * 2 * chapter_scale, 37 * 2 * chapter_scale)})
		}
	}
	
	onOpen_corridor_4Changed:
	{
		if(open_corridor_4)
		{
			open_ind++
			for(var i =0; i < tab_turret_corridor_4.length; i++)
			{
				var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_4[i][0] * 2 * root.chapter_scale, tab_turret_corridor_4[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_4[i][2], angle: tab_turret_corridor_4[i][3] * Math.PI, activate: true})
			}
			
			var shield = barriere_last_room_factory.createObject(null, {scene: ship.scene, x: 31.5, y: 63, nb_repe: 38,  tab_shield:[[33,59.5],[34,57],[34,62]] })
			var boss = mini_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(32 * 2 * chapter_scale, 57.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
			boss.live = Qt.binding(function(){return !root.security_3})
			boss = mini_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(32 * 2 * chapter_scale, 61.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
			boss.live = Qt.binding(function(){return !root.security_3})
			boss = hidden_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(20 * 2 * chapter_scale, 59 * 2 * chapter_scale)})
		}
	}
	
	
	onOpen_corridor_5Changed:
	{
		if(open_corridor_5)
		{
			open_ind++
			for(var i =0; i < tab_turret_corridor_5.length; i++)
			{
				var tur = turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_5[i][0] * 2 * root.chapter_scale, tab_turret_corridor_5[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_5[i][2], angle: tab_turret_corridor_5[i][3] * Math.PI, activate: true})
			}
			
			var shield = barriere_last_room_factory.createObject(null, {scene: ship.scene, x: 44.5, y: 63, nb_repe: 38,  tab_shield:[[42,59.5],[41,57],[41,62]] })
			var boss = mini_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(43 * 2 * chapter_scale, 57.5 * 2 * chapter_scale), angle: Math.PI / 2 })
			boss.live = Qt.binding(function(){return !root.security_4})
			boss = mini_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(43 * 2 * chapter_scale, 61.5 * 2 * chapter_scale), angle: Math.PI / 2 })
			boss.live = Qt.binding(function(){return !root.security_4})
			
			shield = barriere_last_room_factory.createObject(null, {scene: ship.scene, x: 53.5, y: 63, nb_repe: 38,  tab_shield:[[55,59.5],[56,57],[56,62]] })
			boss = mini_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(54 * 2 * chapter_scale, 57.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
			boss.live = Qt.binding(function(){return !root.security_4})
			boss = mini_boss_factory.createObject(null, {scene: ship.scene, position: Qt.point(54 * 2 * chapter_scale, 61.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
			boss.live = Qt.binding(function(){return !root.security_4})
		}
	}
	
	
	
	property var tab_turret_corridor_1:[
	[29,42 ,1, -1/2 ],
	[29,43 ,1, -1/2 ],
	[30,43 ,1, -1/2 ],
	[31,43 ,1, -1/2 ],
	[31,42 ,1, -1/2 ],
	[29,53 ,1, -1/2 ],
	[29,52 ,1, -1/2 ],
	[30,52 ,1, -1/2 ],
	[31,52 ,1, -1/2 ],
	[31,53 ,1, -1/2 ],
	[45,53 ,1, -1/2 ],
	[45,52 ,1, -1/2 ],
	[46,52 ,1, -1/2 ],
	[47,52 ,1, -1/2 ],
	[47,53 ,1, -1/2 ],
	[45,42 ,1, -1/2 ],
	[45,43 ,1, -1/2 ],
	[46,43 ,1, -1/2 ],
	[47,43 ,1, -1/2 ],
	[47,42 ,1, -1/2 ],
	[39,42 ,1, -1/2 ],
	[39,43 ,1, -1/2 ],
	[39,44 ,1, -1/2 ],
	[39,45 ,1, -1/2 ],
	[39,46 ,1, -1/2 ],
	[39,47 ,1, -1/2 ],
	[39,48 ,1, -1/2 ],
	[39,49 ,1, -1/2 ],
	[39,50 ,1, -1/2 ],
	[39,51 ,1, -1/2 ],
	[39,52 ,1, -1/2 ],
	[39,53 ,1, -1/2 ],
	[40,42.5 ,3, -1/2 ],
	[40,43.5 ,3, -1/2 ],
	[40,44.5 ,3, -1/2 ],
	[40,45.5 ,3, -1/2 ],
	[40,46.5 ,3, -1/2 ],
	[40,47.5 ,3, -1/2 ],
	[40,48.5 ,3, -1/2 ],
	[40,49.5 ,3, -1/2 ],
	[40,50.5 ,3, -1/2 ],
	[40,51.5 ,3, -1/2 ],
	[40,52.5 ,3, -1/2 ]
	]
	
	
	
	property var tab_turret_corridor_4:[
	[33,56 ,0, -1/2 ],
	[34,58 ,0, -1/2 ],
	[34,61 ,0, -1/2 ],
	[33,63 ,0, -1/2 ],
	[35,56 ,1, -1/2 ],
	[35,57 ,1, -1/2 ],
	[35,58 ,1, -1/2 ],
	[35,59 ,1, -1/2 ],
	[35,60 ,1, -1/2 ],
	[35,61 ,1, -1/2 ],
	[35,62 ,1, -1/2 ],
	[32,55.05 ,3, -1/2],
	[32,55.65 ,3, -1/2],
	[32,59.25 ,3, -1/2],
	[32,59.75 ,3, -1/2],
	[32,63.35 ,3, -1/2],
	[32,63.95 ,3, -1/2]
	]
	
	
	property var tab_turret_corridor_5:[
	[55,56 ,0, -1/2 ],
	[56,58 ,0, -1/2 ],
	[56,61 ,0, -1/2 ],
	[55,63 ,0, -1/2 ],
	[57,56 ,1, -1/2 ],
	[57,57 ,1, -1/2 ],
	[57,58 ,1, -1/2 ],
	[57,59 ,1, -1/2 ],
	[57,60 ,1, -1/2 ],
	[57,61 ,1, -1/2 ],
	[57,62 ,1, -1/2 ],
	[54,55.05 ,3, -1/2],
	[54,55.65 ,3, -1/2],
	[54,59.25 ,3, -1/2],
	[54,59.75 ,3, -1/2],
	[54,63.35 ,3, -1/2],
	[54,63.95 ,3, -1/2],
	[42,56 ,0, 1/2 ],
	[41,58 ,0, 1/2 ],
	[41,61 ,0, 1/2 ],
	[42,63 ,0, 1/2 ],
	[40,56 ,1, 1/2 ],
	[40,57 ,1, 1/2 ],
	[40,58 ,1, 1/2 ],
	[40,59 ,1, 1/2 ],
	[40,60 ,1, 1/2 ],
	[40,61 ,1, 1/2 ],
	[40,62 ,1, 1/2 ],
	[43,55.05 ,3, 1/2],
	[43,55.65 ,3, 1/2],
	[43,59.25 ,3, 1/2],
	[43,59.75 ,3, 1/2],
	[43,63.35 ,3, 1/2],
	[43,63.95 ,3, 1/2]
	]
	
	
	function is_not_2_open()
	{
		return !root.open_corridor_2
	}
	function is_not_3_open()
	{
		return !root.open_corridor_3
	}
	function is_not_4_open()
	{
		return !root.open_corridor_4
	}
	function is_not_5_open()
	{
		return !root.open_corridor_5
	}
	// x, y , weapon choice, angle, bind, invulnerable
	
	
	
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-6/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-6/"+mat+"/c"])
			}
		}
		return fill_tiles
	}
	
	function fill_2(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y, mat])
			}
		}
		return fill_tiles
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}
	
	/*Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("Temps restant: %1").arg(root.ti)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}*/
	
	scene: Scene
	{
		running: false
		
		Background
		{
			position.x: -1  
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Background
		{
			position.x: 57
			x:1
			y : 10
			z: altitudes.background_near
			scale: 5
			material: "chapter-7/starship/outdoor-masked"
		}
		
		
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		
		Tiles
		{
			id: corridor_1
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[25,41,'n','metal'],
				[25,40,'w',''],
				[29,40,'n',''],
				[29,41,'e','metal'],
				[47,41,'n','metal'],
				[47,40,'w',''],
				[51,40,'n',''],
				[51,41,'e','metal'],
				[59,41,'n','metal'],
				[59,54,'e','metal'],
				[51,54,'s','metal'],
				[51,55,'e',''],
				[47,55,'s',''],
				[47,54,'w','metal'],
				[29,54,'s','metal'],
				[29,55,'e',''],
				[25,55,'s',''],
				[25,54,'w','metal'],
				[16,54,'s','metal'],
				[16,41,'w','metal']
				]).concat(
				fill(17,42,58,53,'metal'),
				fill(26,41,28,41,'metal'),
				fill(48,41,50,41,'metal'),
				fill(26,54,28,54,'metal'),
				fill(48,54,50,54,'metal'),
				[[18,43,'chapter-6/metal/c2'],
				[22,43,'chapter-6/metal/c2'],
				[26,43,'chapter-6/metal/c2'],
				[30,43,'chapter-6/metal/c2'],
				[34,43,'chapter-6/metal/c2'],
				[38,43,'chapter-6/metal/c2'],
				[42,43,'chapter-6/metal/c2'],
				[46,43,'chapter-6/metal/c2'],
				[50,43,'chapter-6/metal/c2'],
				[54,43,'chapter-6/metal/c2'],
				[58,43,'chapter-6/metal/c2'],
				[18,46,'chapter-6/metal/c2'],
				[22,46,'chapter-6/metal/c2'],
				[26,46,'chapter-6/metal/c2'],
				[30,46,'chapter-6/metal/c2'],
				[34,46,'chapter-6/metal/c2'],
				[38,46,'chapter-6/metal/c2'],
				[42,46,'chapter-6/metal/c2'],
				[46,46,'chapter-6/metal/c2'],
				[50,46,'chapter-6/metal/c2'],
				[54,46,'chapter-6/metal/c2'],
				[18,49,'chapter-6/metal/c2'],
				[22,49,'chapter-6/metal/c2'],
				[26,49,'chapter-6/metal/c2'],
				[30,49,'chapter-6/metal/c2'],
				[34,49,'chapter-6/metal/c2'],
				[38,49,'chapter-6/metal/c2'],
				[42,49,'chapter-6/metal/c2'],
				[46,49,'chapter-6/metal/c2'],
				[50,49,'chapter-6/metal/c2'],
				[54,49,'chapter-6/metal/c2'],
				[18,52,'chapter-6/metal/c2'],
				[22,52,'chapter-6/metal/c2'],
				[26,52,'chapter-6/metal/c2'],
				[30,52,'chapter-6/metal/c2'],
				[34,52,'chapter-6/metal/c2'],
				[38,52,'chapter-6/metal/c2'],
				[42,52,'chapter-6/metal/c2'],
				[46,52,'chapter-6/metal/c2'],
				[50,52,'chapter-6/metal/c2'],
				[54,52,'chapter-6/metal/c2'],
				[58,52,'chapter-6/metal/c2']]
				)
		}
		
		Tiles
		{
			id: corridor_2
			z: altitudes.background_near
			mask: root.open_corridor_2 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[37,21,'n','metal'],
				[37,40,'e','metal'],
				[29,40,'s','metal'],
				[29,41,'e',''],
				[25,41,'s',''],
				[25,40,'w','metal'],
				[16,40,'s','metal'],
				[16,21,'w','metal']
				]).concat(
				fill(17,22,36,39,'metal'),
				fill(26,40,28,40,'metal'),
				[[18,24,'chapter-6/metal/c2'],
				[22,24,'chapter-6/metal/c2'],
				[26,24,'chapter-6/metal/c2'],
				[30,24,'chapter-6/metal/c2'],
				[34,24,'chapter-6/metal/c2'],
				[34,28,'chapter-6/metal/c2'],
				[34,32,'chapter-6/metal/c2'],
				[34,36,'chapter-6/metal/c2'],
				[18,28,'chapter-6/metal/c2'],
				[18,32,'chapter-6/metal/c2'],
				[18,36,'chapter-6/metal/c2'],
				[22,36,'chapter-6/metal/c2'],
				[26,36,'chapter-6/metal/c2'],
				[30,36,'chapter-6/metal/c2'],
				[34,36,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(15 * 2, 20 * 2), Qt.point(37.5 * 2, 20 * 2), Qt.point(37.5 * 2,40.5 * 2), Qt.point(15 * 2, 40.5 * 2)]
				group: root.open_corridor_2 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_3
			z: altitudes.background_near
			mask: root.open_corridor_3 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[59,21,'n','metal'],
				[59,40,'e','metal'],
				[51,40,'s','metal'],
				[51,41,'e',''],
				[47,41,'s',''],
				[47,40,'w','metal'],
				[38,40,'s','metal'],
				[38,21,'w','metal']
				]).concat(
				fill(39,22,58,39,'metal'),
				fill(48,40,50,40,'metal'),
				[[41,31,'chapter-6/metal/c2'],
				[45,31,'chapter-6/metal/c2'],
				[53,31,'chapter-6/metal/c2'],
				[57,31,'chapter-6/metal/c2'],
				[49,23,'chapter-6/metal/c2'],
				[49,27,'chapter-6/metal/c2'],
				[49,31,'chapter-6/metal/c2'],
				[49,35,'chapter-6/metal/c2'],
				[49,39,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(37.5 * 2, 20 * 2), Qt.point(60 * 2, 20 * 2), Qt.point(60 * 2,40.5 * 2), Qt.point(37.5 * 2, 40.5 * 2)]
				group: root.open_corridor_3 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_4
			z: altitudes.background_near
			mask: root.open_corridor_4 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[25,55,'n','metal'],
				[25,54,'w',''],
				[29,54,'n',''],
				[29,55,'e','metal'],
				[37,55,'n','metal'],
				[37,64,'e','metal'],
				[16,64,'s','metal'],
				[16,55,'w','metal']]).concat(
				fill(17,56,36,63,'metal'),
				fill(26,55,28,55,'metal'),
				[[18,56,'chapter-6/metal/c2'],
				[22,56,'chapter-6/metal/c2'],
				[26,56,'chapter-6/metal/c2'],
				[30,56,'chapter-6/metal/c2'],
				[34,56,'chapter-6/metal/c2'],
				[18,59,'chapter-6/metal/c2'],
				[22,59,'chapter-6/metal/c2'],
				[26,59,'chapter-6/metal/c2'],
				[30,59,'chapter-6/metal/c2'],
				[34,59,'chapter-6/metal/c2'],
				[18,62,'chapter-6/metal/c2'],
				[22,62,'chapter-6/metal/c2'],
				[26,62,'chapter-6/metal/c2'],
				[30,62,'chapter-6/metal/c2'],
				[34,62,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(15 * 2, 54.5 * 2), Qt.point(37.5 * 2, 54.5 * 2), Qt.point(37.5 * 2, 65 * 2), Qt.point(15 * 2, 65 * 2)]
				group: root.open_corridor_4 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_5
			z: altitudes.background_near
			mask: root.open_corridor_5 ? "white" : "transparent"
			scale: root.chapter_scale
			property var collider_wall: []
			tiles: create_tiles([
				[47,55,'n','metal'],
				[47,54,'w',''],
				[51,54,'n',''],
				[51,55,'e','metal'],
				[59,55,'n','metal'],
				[59,64,'e','metal'],
				[38,64,'s','metal'],
				[38,55,'w','metal']
				]).concat(
				fill(39,56,58,63,'metal'),
				fill(48,55,50,55,'metal'),
				[[40,56,'chapter-6/metal/c2'],
				[44,56,'chapter-6/metal/c2'],
				[48,56,'chapter-6/metal/c2'],
				[52,56,'chapter-6/metal/c2'],
				[56,56,'chapter-6/metal/c2'],
				[40,59,'chapter-6/metal/c2'],
				[44,59,'chapter-6/metal/c2'],
				[48,59,'chapter-6/metal/c2'],
				[52,59,'chapter-6/metal/c2'],
				[56,59,'chapter-6/metal/c2'],
				[40,62,'chapter-6/metal/c2'],
				[44,62,'chapter-6/metal/c2'],
				[48,62,'chapter-6/metal/c2'],
				[52,62,'chapter-6/metal/c2'],
				[56,62,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				id: collider_1
				vertexes: [Qt.point(37.5 * 2, 54.5 * 2), Qt.point(60 * 2, 54.5 * 2), Qt.point(60 * 2,65 * 2), Qt.point(37.5 * 2, 65 * 2)]
				group: root.open_corridor_5 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_2
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(30 *  chapter_scale*2, 42 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_2 = true
					root.can_open = false
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + console_2.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.can_open ? root.open_corridor_2 ?  0 : groups.sensor : 0
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: console_3
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(46 *  chapter_scale*2, 42 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_3 = true
					root.can_open = false
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + console_3.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.can_open ? root.open_corridor_3 ?  0 : groups.sensor : 0
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: console_4
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(30 *  chapter_scale*2, 53 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_4 = true
					root.can_open = false
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + console_4.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.can_open ? root.open_corridor_4 ?  0 : groups.sensor : 0
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_5
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(46 *  chapter_scale*2, 53 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_5 = true
					root.can_open = false
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + console_5.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.can_open ? root.open_corridor_5 ?  0 : groups.sensor : 0
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: secu_1
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(36 *  chapter_scale * 2, 22 *  chapter_scale * 2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.security_1 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + secu_1.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_2 ? "white" : "transparent" 
			}
			
			CircleCollider
			{
				group: root.security_1 ?  0 : groups.sensor
				sensor: true
			}
		}
		Sensor
		{
			id: secu_2
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(48.5 *  chapter_scale*2, 22 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.security_2 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + secu_2.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_3 ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: root.security_2 ?  0 : groups.sensor
				sensor: true
			}
		}
		Sensor
		{
			id: secu_3
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(36 *  chapter_scale*2, 59 *  chapter_scale * 2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.security_3 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + secu_3.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_4 ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: root.security_3 ?  0 : groups.sensor
				sensor: true
			}
		}
		Sensor
		{
			id: secu_4
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(39 *  chapter_scale * 2, 59 *  chapter_scale * 2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.security_4 = true
					indice = 24
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			
			Image
			{
				material: "chapter-6/console/" + secu_4.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_5 ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: root.security_4 ?  0 : groups.sensor
				sensor: true
			}
		}
		
		Ship
		{
			id: ship	
			position.x: 17 * 4
			position.y: 48 * 4
			angle: Math.PI/2
			Q.Component.onDestruction: 
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
			
		}
			
		Body
		{
			id: all_wall
			position.y: 0
			position.x: 0
			scale: chapter_scale*2
			
			type: Body.STATIC
			
			function point(x,y)
			{
				return Qt.point(x - 0.25 , y + 0.25)
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(7,14), all_wall.point(59 + 2*0.25,14), all_wall.point(59 + 2 * 0.25, 21 - 2 *0.25), all_wall.point(7,21 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(59 + 2 * 0.25,14), all_wall.point(67,14), all_wall.point(67,64), all_wall.point(59+ 2 *0.25,64)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(16,64), all_wall.point(67,64), all_wall.point(67,70), all_wall.point(16, 70)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(7,21 - 2 * 0.25), all_wall.point(16,21 - 2 * 0.25), all_wall.point(16 ,70), all_wall.point(7,70)]
				group: groups.wall
			}
			
			
			PolygonCollider
			{
				vertexes: [all_wall.point(37 + 2 * 0.25, 21 - 2 * 0.25), all_wall.point(38,21 - 2 * 0.25), all_wall.point(38,40), all_wall.point(37 + 2 * 0.25,40)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(29 + 2 * 0.25, 40), all_wall.point(47,40), all_wall.point(47,41 - 2 *0.25), all_wall.point(29 + 2 * 0.25,41 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(16, 40), all_wall.point(25,40), all_wall.point(25,41 - 2 *0.25), all_wall.point(16,41 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(51 + 2 * 0.25, 40), all_wall.point(59 + 2 * 0.25,40), all_wall.point(59 + 2 * 0.25 ,41 - 2 *0.25), all_wall.point(51 + 2 * 0.25,41 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(37 + 2 * 0.25, 55 - 2 * 0.25), all_wall.point(38,55 - 2 * 0.25), all_wall.point(38,64), all_wall.point(37 + 2 * 0.25, 64)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(29 + 2 * 0.25, 54), all_wall.point(47,54), all_wall.point(47,55 - 2 *0.25), all_wall.point(29 + 2 * 0.25,55 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(16, 54), all_wall.point(25,54), all_wall.point(25,55 - 2 *0.25), all_wall.point(16,55 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(51 + 2 * 0.25, 54), all_wall.point(59 + 2 * 0.25, 54), all_wall.point(59 + 2 * 0.25 ,55 - 2 *0.25), all_wall.point(51 + 2 * 0.25,55 - 2 * 0.25)]
				group: groups.wall
			}
		}
		
		Sensor
		{
			id: exit
			type: Body.STATIC
			property real x: 56.5
			property real y: 47.5
			scale: 4
			position: exit.coords(x,y)
			property int indice: 0
			property bool changed: true
			property real ti_local: root.ti
			property bool open : false
			
			onTi_localChanged:
			{
				if(root.open_sortie)
				{
					if(changed)
					{
						if(indice < 24)
						{	
							indice++
						}
						else
						{
							open = true
						}
						changed = false
					}
					else
					{
						changed = true
					}
				}
			}
			
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Image
			{
				material: "chapter-6/exit/" + exit.indice
				z: altitudes.boss
			}
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie()
				}
			}
			PolygonCollider
			{
				vertexes: [
				Qt.point(- 1 , - 1),
				Qt.point(1 , - 1),
				Qt.point(1 , 1),
				Qt.point(- 1 , 1)
				]
				group:  exit.open ? groups.sensor : 0
				sensor : true
			}
		}
		Sensor
		{
			id: info
			type: Body.STATIC
			property real x: 56.5
			property real y: 47.5
			scale: 4
			position: info.coords(x,y)
			property bool open : false
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.on_sortie()
				}
			}
			PolygonCollider
			{
				vertexes: [
				Qt.point(- 1 , - 1.5),
				Qt.point(1 , - 1.5),
				Qt.point(1 , 1.5),
				Qt.point(- 1 , 1.5)
				]
				group:  exit.open ? 0 : groups.sensor
				sensor : true
			}
		}

		
		Body
		{
			id: barriere_2
			type: Body.STATIC
			property real x: 28
			property real y: 40
			property int nb_repe: 18
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_2.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_2.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_2.y + 0.625)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_2 ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_2.coords(barriere_2.x + 1.25 - barriere_2.nb_repe / 4, barriere_2.y + 0.25), 
				barriere_2.coords(barriere_2.x + 1.25, barriere_2.y + 0.25), 
				barriere_2.coords(barriere_2.x + 1.25, barriere_2.y + 0.75), 
				barriere_2.coords(barriere_2.x + 1.25 - barriere_2.nb_repe / 4, barriere_2.y + 0.75)
				]
				group: root.open_corridor_2 ? 0 : groups.enemy_invincible
			}
		}
		
		
		Body
		{
			id: barriere_3
			type: Body.STATIC
			property real x: 50
			property real y: 40
			property int nb_repe: 18
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_3.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_3.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_3.y + 0.625)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_3 ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_3.coords(barriere_3.x + 1.25 - barriere_3.nb_repe / 4, barriere_3.y + 0.25), 
				barriere_3.coords(barriere_3.x + 1.25, barriere_3.y + 0.25), 
				barriere_3.coords(barriere_3.x + 1.25, barriere_3.y + 0.75), 
				barriere_3.coords(barriere_3.x + 1.25 - barriere_3.nb_repe / 4, barriere_3.y + 0.75)
				]
				group: root.open_corridor_3 ? 0 : groups.enemy_invincible
			}
		}
		
		
		Body
		{
			id: barriere_4
			type: Body.STATIC
			property real x: 28
			property real y: 54
			property int nb_repe: 18
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_4.nb_repe
				Q.Component
				{	
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_4.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_4.y+0.375)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_4 ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_4.coords(barriere_4.x + 1.25 - barriere_4.nb_repe / 4, barriere_4.y + 0.25), 
				barriere_4.coords(barriere_4.x + 1.25, barriere_4.y + 0.25), 
				barriere_4.coords(barriere_4.x + 1.25, barriere_4.y + 0.75), 
				barriere_4.coords(barriere_4.x + 1.25 - barriere_4.nb_repe / 4, barriere_4.y + 0.75)
				]
				group: root.open_corridor_4 ? 0 : groups.enemy_invincible
			}
		}
		
		
		Body
		{
			id: barriere_5
			type: Body.STATIC
			property real x: 50
			property real y: 54
			property int nb_repe: 18
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_5.nb_repe
				Q.Component
				{	
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_5.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere_5.y+0.375)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.open_corridor_5 ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_5.coords(barriere_5.x + 1.25 - barriere_5.nb_repe / 4, barriere_5.y + 0.25), 
				barriere_5.coords(barriere_5.x + 1.25, barriere_5.y + 0.25), 
				barriere_5.coords(barriere_5.x + 1.25, barriere_5.y + 0.75), 
				barriere_5.coords(barriere_5.x + 1.25 - barriere_5.nb_repe / 4, barriere_5.y + 0.75)
				]
				group: root.open_corridor_5 ? 0 : groups.enemy_invincible
			}
		}
		
		
	}
	
	
	
	Q.Component
	{
		id: hidden_boss_factory
		Vehicle
		{
			id: vauban
			icon: 5
			icon_color: "red"
			max_speed: 0
			max_angular_speed: 0.4
			type: Body.KINEMATIC
			//weapon_regeneration: 5
			scale: 10
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			Q.Component.onCompleted: shield = max_shield
			loot_factory: Repeater
			{
				count: 31
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "chapter-2/boss-cache"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: Qt.rgba(0, 0.4, 1, vauban.shield / vauban.max_shield)
				scale: 1.1
				z: altitudes.shield
			}

			CircleCollider
			{
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
				radius : 0.8
			}

			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				radius: 1.1
			}
			Behaviour_attack_rotate
			{
				target_groups: groups.enemy_targets
				target_distance: 10
			}
			EquipmentSlot
			{
				scale: 0.1
				equipment: Shield
				{
					level: 2
				}
			}
			Repeater
			{
				count: 5
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .22
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .22
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Shield
						{
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .74
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .74
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: LaserGun
						{
							shooting: true
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .47
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .47
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.08
						equipment: Quipoutre
						{
							shooting: true
							period: scene.seconds_to_frames(2)
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index - .025) * 72 * Math.PI / 180) * .66
						position.y: Math.sin((index - .025) * 72 * Math.PI / 180) * .66
						angle: (index - .01) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Gun
						{
							shooting: true
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .525) * 72 * Math.PI / 180) * .66
						position.y: Math.sin((index + .525) * 72 * Math.PI / 180) * .66
						angle: (index + .51) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Gun
						{
							shooting: true
							level: 2
						}
					}
				}
			}
		}
	}
	
	Q.Component
	{
		id: turret_factory
		Turret
		{
		
		}
	}
	
	Q.Component
	{
		id: shield_barriere_factory
		Vehicle
		{
			id: shield_1
			type: Body.STATIC
			property real x:52
			property real y:13
			property int nb_repe: 18
			property var tab_shield:[] // x,y
			property var tab_shield_alive:[] // x,y
			property var tab_collider: []
			property real ti_local: root.ti
			property color mask:  Qt.rgba(0, 0.4, 1, shield_1.shield ? Math.max(0.2, shield_1.shield / shield_1.max_shield) : 0)
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			
			onTi_localChanged:
			{
				var alive = false
				for(var i = 0; i < tab_collider.length; i++)
				{
					if(!tab_shield_alive[i])
					{
						tab_collider[i].group = 0
					}
					else
					{
						alive = true
					}
				}
				if(tab_collider.length > 0 && !alive)
				{
					destroy()
				}
			}
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			
			Q.Component.onCompleted:
			{
				shield_1.shield = max_shield
			}
			
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.375)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: shield_1.mask
					}
				}
			}
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.625)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: shield_1.mask
					}
				}
			}
			
			Repeater
			{
				count: shield_1.tab_shield.length
				Q.Component
				{
					EquipmentSlot
					{
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						equipment: Shield
						{
							level: 2	
							material_image: "chapter-2/shield-generator"							
							Q.Component.onCompleted:
							{
								shield_1.tab_shield_alive.push(true)
							}
							
							Q.Component.onDestruction: 
							{
								shield_1.tab_shield_alive[index] = false
							}
						}
					}
				}
				Q.Component
				{
					CircleCollider
					{
						id: collider
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						group: shield_1.tab_shield_alive[index] ? groups.enemy_hull_naked : 0
						radius: 0.5
						
						Q.Component.onCompleted:
						{
							shield_1.tab_collider.push(collider)
						}
						
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4),
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4), 
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25), 
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25)
				]
				group: shield_1.shield ? groups.enemy_shield : 0
			}
		}
	}
	
	
	Q.Component
	{
		id: barriere_last_room_factory
		Vehicle
		{
			id: shield_1
			type: Body.STATIC
			property real x:52
			property real y:13
			property int nb_repe: 18
			property var tab_shield:[] // x,y
			property var tab_shield_alive:[] 
			property var tab_collider: []
			property real ti_local: root.ti
			property bool reflection: true
			property real time_to_reflec: 3
			property real deb_reflec: 0
			property bool protec: false
			property real time_to_trans: 5
			property real deb_trans: 0
			property bool transition: false
			property real time_to_protec: 3
			property real deb_protec: 0
			property color mask: Qt.rgba(1, 1, 0, 1)
			property bool alive: true
			property int sens: -1
			onAliveChanged: if(!alive) destroy()
			
			Q.Component.onCompleted:
			{
				deb_reflec = root.ti
			}
						
			onTi_localChanged:
			{
				var alive = false
				for(var i = 0; i < tab_collider.length; i++)
				{
					if(!tab_shield_alive[i])
					{
						tab_collider[i].group = 0
					}
					else
					{
						alive = true
					}
				}
				if(tab_collider.length > 0 && !alive)
				{
					destroy()
				}
				else
				{
					if(reflection)
					{
						if(deb_reflec - ti_local > time_to_reflec)
						{
							reflection = false
							transition = true
							deb_trans = ti_local
							sens = -1
						}
					}
					if(transition)
					{
						//Qt.rgba(0.58, 0, 0.82, 1) protec
						//Qt.rgba(1, 1, 0, 1) reflection
						
						if(deb_trans - ti_local > time_to_trans)
						{
							if(sens == 1)
							{
								reflection = true
								transition = false
								deb_reflec = ti_local
							}
							if(sens == -1)
							{
								protec = true
								transition = false
								deb_protec = ti_local
							}
						}
						else
						{
							if(sens == 1)
							{
								mask.g = (deb_trans - ti_local) / time_to_trans
								mask.r = 0.58 + 0.42 * (deb_trans - ti_local) / time_to_trans
								mask.b = 0.82 * (1 - (deb_trans - ti_local) / time_to_trans)
							}
							else
							{
								mask.g = 1 - (deb_trans - ti_local) / time_to_trans
								mask.r = 1 - 0.42 * (deb_trans - ti_local) / time_to_trans
								mask.b = 0.82 * (deb_trans - ti_local) / time_to_trans
							}
						}
					}
					if(protec)
					{
						if(deb_protec - ti_local > time_to_protec)
						{
							protec = false
							transition = true
							deb_trans = ti_local
							sens = 1
						}
					}
				}
			}
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.375)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: shield_1.mask
					}
				}
			}
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.625)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: shield_1.mask
					}
				}
			}
			
			Repeater
			{
				count: shield_1.tab_shield.length
				Q.Component
				{
					EquipmentSlot
					{
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						equipment: Energizer
						{
							Q.Component.onCompleted:
							{
								shield_1.tab_shield_alive.push(true)
							}
							Q.Component.onDestruction: 
							{
								shield_1.tab_shield_alive[index] = false
							}
						}
					}
				}
				Q.Component
				{
					CircleCollider
					{
						id: collider
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						group: shield_1.tab_shield_alive[index] ? groups.item_ignore_enemy : 0
						radius: 2
						
						Q.Component.onCompleted:
						{
							shield_1.tab_collider.push(collider)
						}
						
					}
				}
			}
			
			function begin(other)
			{
				var v = direction_from_scene(other.velocity)
				other.velocity = direction_to_scene(Qt.point(-v.x, v.y))
				var a = 2 * angle - other.angle + Math.PI
				var e = scene.time_changed
				function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
				e.connect(f)
				for(var i in other.children)
				{
					var child = other.children[i]
					if(child.group) child.group ^= 1
					if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
				}
			}
			
			
			PolygonCollider
			{
				vertexes: [
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4),
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4), 
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25), 
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25)
				]
				group: shield_1.reflection ? groups.reflector_only_player : (shield_1.protec ? groups.enemy_invincible : 0)
			}
		}
	}
	
	Q.Component
	{
		id: mini_boss_factory
		Vehicle
		{
			function local_coords(x, y) {return Qt.point(x / 540 - 1, y / 540 - 1)} // images en 1080*1080
			function group_of(x) {return x ? shield ? groups.enemy_hull : groups.enemy_hull_naked : 0}
			id: mini_boss
			icon: 5
			icon_color: "red"
			type: Body.STATIC
			scale: 6
			Q.Component.onCompleted: shield = max_shield
			property bool onfire: behaviour.shooting
			max_speed: 0
			max_angular_speed: 0
			property bool shield_1: true
			property bool shield_2: true
			property bool shield_3: true
			property bool quipoutre_1: true
			property bool quipoutre_2: true
			property bool quipoutre_3: true
			property bool live: true
			onLiveChanged: if(!live) destroy()
			
			property bool alive: shield_1 || shield_2 || shield_3 || quipoutre_1 || quipoutre_2 || quipoutre_3
			onAliveChanged: if(!alive) destroy()
			loot_factory: Repeater
			{
				count: 10
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "chapter-2/mini-boss-ss"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: Qt.rgba(0, 0.4, 1, mini_boss.shield / mini_boss.max_shield)
				position: local_coords(540, 717)
				scale: 510/540
				z: altitudes.shield
			}
			Behaviour_attack_distance
			{
				id: behaviour
				target_groups: groups.enemy_targets
				max_distance: 40
			}
			CircleCollider
			{
				position: local_coords(540, 717)
				group: shield ? groups.enemy_shield : 0
				radius: 510/540
			}

			CircleCollider
			{
				group: group_of(quipoutre_1)
				radius: 80/540
				position: local_coords(223, 540)
			}
			EquipmentSlot
			{
				position: local_coords(223, 540)
				scale: 0.15*6/mini_boss.scale
				angle: Math.PI * -1/4
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_1 = false
				}
			}

			CircleCollider
			{
				group: group_of(quipoutre_2)
				radius: 80/540
				position: local_coords(540, 540)
			}
			EquipmentSlot
			{
				position: local_coords(540, 540)
				scale: 0.15*6/mini_boss.scale
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_2 = false
				}
			}

			CircleCollider
			{
				group: group_of(quipoutre_3)
				radius: 80/540
				position: local_coords(854, 540)
			}
			EquipmentSlot
			{
				position: local_coords(854, 540)
				scale: 0.15*6/mini_boss.scale
				angle: Math.PI * 1/4
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_3 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_1)
				radius: 60/540
				position: local_coords(303, 848)
			}
			EquipmentSlot
			{
				position: local_coords(303, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_1 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_2)
				radius: 60/540
				position: local_coords(540, 848)
			}
			EquipmentSlot
			{
				position: local_coords(540, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_2 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_3)
				radius: 60/540
				position: local_coords(776, 848)
			}
			EquipmentSlot
			{
				position: local_coords(776, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_3 = false
				}
			}
		}
	}
	
		Q.Component
	{
		id: canon_factory
		Vehicle
		{
			id: canon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool shoot: false
			property real shoot_period: 0.5
			EquipmentSlot
			{
				id: eq_slot
				equipment: Weapon
				{
					id: weapon
					health: max_health
					max_health: 25000
					shooting: canon.shoot
					period: scene.seconds_to_frames(canon.shoot_period)
					readonly property Sound sound_shooting: Sound
					{
						objectName: "equipments/heavy-laser-gun/shooting"
						scale: 40
					}
					bullet_factory: Bullet
					{
						id: bullet
						scale: 2
						range: scene.seconds_to_frames(20)
						damages: 10000
						Image
						{
							material: "chapter-6/boss/bullet"
							z: altitudes.bullet
						}
						PolygonCollider
						{
							vertexes: [Qt.point(-0.125, -1), Qt.point(-0.125, 1), Qt.point(0.125, 1), Qt.point(0.125, -1)]
							group: groups.enemy_laser
							sensor: true
						}
						Q.Component.onCompleted:
						{
							position = weapon.slot.point_to_scene(Qt.point(0, -bullet.scale))
							angle = weapon.slot.angle_to_scene(0)
							weapon.sound_shooting.play()
							weapon.set_bullet_velocity(this, 7)
						}
					}
				}
			}
		}
	}
	
	Q.Component
	{
		id: turret_boss_factory
		Vehicle
		{
			id: turret
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool shoot: false
			property bool activate: false
			property real ti_local : root.ti
			property real scale_deb: 0.1
			scale: scale_deb
			property real scale_finish: 1
			property real ti_deb: root.ti
			property real time_to_appear: 1
			property int ind_img: 0
			property bool appear: false
			property int indice: 0
			property real ti_cour: 0
			property bool first: true
			onTi_localChanged:
			{
				if(shoot)
				{
					if(first)
					{
						first = false
						ti_deb = ti_local
					}
				
					if(!activate)
					{
						if(ti_deb - ti_local < time_to_appear)
						{
							if(ind_img < 14)
							{
								ind_img = Math.floor( 2 * (ti_deb - ti_local) / time_to_appear * 14)
								if(ind_img > 6)
								{
									if(!appear)
									{
										appear = true
										ti_cour = ti_local
									}
									scale += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb)
								}
							}
							else
							{
								scale += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb)
							}
						}
						else
						{
							appear = true
							scale = 1
							activate = true
						}
					}
				}
			}
			
			Image
			{
				material: "chapter-2/round-turret"
				mask: Qt.rgba(1, 1, 1, turret.appear ? 1 : 0)
				z: altitudes.boss
			}
			Image
			{
				id: image
				material: "chapter-2/turret-opening/" + ind_img
				mask: Qt.rgba(1, 1, 1, activate ? 0 : 1)
				z: altitudes.boss - 0.00001
				scale: turret.scale_finish / turret.scale
			}
			
			EquipmentSlot
			{
				id: eq_slot
				scale: 1 * turret.scale/turret.scale_finish
				equipment: RandomSpray
				{
					range_bullet: scene.seconds_to_frames(3)
					shooting: turret.activate
					enemy: 1
					level: 2
				}
				Q.Component.onCompleted:
				{ 
					equipment.image.mask.a = Qt.binding(function(){return (turret.appear ? 1 : 0)})
				}
			}
		}
	}
	
	
	
	//collider et position des arme à revoir lorsque plink aura donné sa texture
	Q.Component
	{
		id: boss_4
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 21
			property real angular_speed: 1 + 10 * (health_boss ? (1 - health_boss.health / health_boss.max_health) : 1)
			property real time_to_rotate: 20
			property real ti_local: root.ti
			property int indice: 0
			property real deb: root.ti
			property real sens: 1
			property bool activate: false
			property var canon_left
			property var canon_right
			property var coord_canon: [coords(625, 5),coords(1423,5)]
			property var coord_turret: [
			[coords(800, 827), -1/2],
			[coords(936, 809), -7/8],
			[coords(1248,827), 1/2],
			[coords(1112,809), 7/8]
			]
			property var tab_local_turret: []
			property bool swap_ind: true
			
			onTi_localChanged:
			{
				if(indice != 0 || deb - root.ti > 0.2)
				{
					if(indice < 24)
					{
						if(swap_ind)
						{
							indice++
						}
						else
						{
							swap_ind = true
						}
						if(indice == 24)
						{
							boss.activate = true
						}
					}
					else
					{
						if(canon_right && !canon_right.shoot && deb - root.ti > 0.25)
						{
							canon_right.shoot = true
						}
						if(canon_right)
						{
							canon_right.shoot_period = 0.05 + (health_boss ? 0.5 * health_boss.health / health_boss.max_health : 0.01)
						}
						if(canon_left)
						{
							canon_left.shoot_period = 0.05 + (health_boss ? 0.5 * health_boss.health / health_boss.max_health : 0.01)
						}
						angle += sens * angular_speed * 2 * Math.PI / root.scene.seconds_to_frames(time_to_rotate)
						if(angle > 10*Math.PI)
						{
							sens = -1
						}
						if(angle < -10*Math.PI)
						{
							sens = 1
						}
					}
				}
				
			}
			
			loot_factory: Repeater
			{
				count: 40
				Q.Component {Loot {}}
				Q.Component {Explosion { scale : 20}}
			}
			
			EquipmentSlot
			{
				equipment: Equipment
				{
					id: health_boss
					health: max_health
					max_health: 75000
				}
			}
			
			
			function canon_left_pos()
			{
				var x = boss.position.x + coord_canon[0].x * scale * Math.cos(boss.angle) - coord_canon[0].y * scale * Math.sin(boss.angle)
				var y = boss.position.y + coord_canon[0].y * scale * Math.cos(boss.angle) + coord_canon[0].x * scale * Math.sin(boss.angle)
				return Qt.point(x,y)
			}
			
			function canon_right_pos()
			{
				var x = boss.position.x + coord_canon[1].x * scale * Math.cos(boss.angle) - coord_canon[1].y * scale * Math.sin(boss.angle)
				var y = boss.position.y + coord_canon[1].y * scale * Math.cos(boss.angle) + coord_canon[1].x * scale * Math.sin(boss.angle)
				return Qt.point(x,y)
			}
			function canon_angle()
			{
				return boss.angle
			}
			Q.Component.onCompleted:
			{
				deb = root.ti
				canon_left  = canon_factory.createObject(null, {scene: scene, position: canon_left_pos(), shoot: false})
				canon_left.position = Qt.binding(canon_left_pos)
				canon_left.angle = Qt.binding(canon_angle)
				canon_left.shoot = Qt.binding(function(){return boss.activate})
				
				canon_right  = canon_factory.createObject(null, {scene: scene, position: canon_right_pos(), shoot: false})
				canon_right.position = Qt.binding(canon_right_pos)
				canon_right.angle = Qt.binding(canon_angle)
				
				for(var i = 0; i< coord_turret.length; i++)
				{
					tab_local_turret.push(turret_boss_factory.createObject(null, {scene: ship.scene, position: coord_turret[i][0], shoot: false, angle: boss.angle + coord_turret[i][1]  * Math.PI}))
				}
				tab_local_turret[0].position = Qt.binding(function(){
						var x = boss.position.x + coord_turret[0][0].x * scale * Math.cos(boss.angle) - coord_turret[0][0].y * scale * Math.sin(boss.angle)
						var y = boss.position.y + coord_turret[0][0].y * scale * Math.cos(boss.angle) + coord_turret[0][0].x * scale * Math.sin(boss.angle)
						return Qt.point(x,y)})
				tab_local_turret[1].position = Qt.binding(function(){
						var x = boss.position.x + coord_turret[1][0].x * scale * Math.cos(boss.angle) - coord_turret[1][0].y * scale * Math.sin(boss.angle)
						var y = boss.position.y + coord_turret[1][0].y * scale * Math.cos(boss.angle) + coord_turret[1][0].x * scale * Math.sin(boss.angle)
						return Qt.point(x,y)})
				tab_local_turret[2].position = Qt.binding(function(){
						var x = boss.position.x + coord_turret[2][0].x * scale * Math.cos(boss.angle) - coord_turret[2][0].y * scale * Math.sin(boss.angle)
						var y = boss.position.y + coord_turret[2][0].y * scale * Math.cos(boss.angle) + coord_turret[2][0].x * scale * Math.sin(boss.angle)
						return Qt.point(x,y)})
				tab_local_turret[3].position = Qt.binding(function(){
						var x = boss.position.x + coord_turret[3][0].x * scale * Math.cos(boss.angle) - coord_turret[3][0].y * scale * Math.sin(boss.angle)
						var y = boss.position.y + coord_turret[3][0].y * scale * Math.cos(boss.angle) + coord_turret[3][0].x * scale * Math.sin(boss.angle)
						return Qt.point(x,y)})
						
				tab_local_turret[0].angle = Qt.binding(function(){
					return boss.angle + coord_turret[0][1] * Math.PI
					})
				tab_local_turret[1].angle = Qt.binding(function(){
					return boss.angle + coord_turret[1][1] * Math.PI
					})
				tab_local_turret[2].angle = Qt.binding(function(){
					return boss.angle + coord_turret[2][1] * Math.PI
					})
				tab_local_turret[3].angle = Qt.binding(function(){
					return boss.angle + coord_turret[3][1] * Math.PI
					})
				
				tab_local_turret[0].shoot = Qt.binding(function(){return boss.activate})
				tab_local_turret[1].shoot = Qt.binding(function(){return boss.activate})
				tab_local_turret[2].shoot = Qt.binding(function(){return boss.activate})
				tab_local_turret[3].shoot = Qt.binding(function(){return boss.activate})
				
			}
			Q.Component.onDestruction: 
			{
				
				for(var i = 0; i< tab_local_turret.length; i++)
				{
					tab_local_turret[i].alive = false
				}
				if(canon_left)
				{
					canon_left.alive = false
				}
				if(canon_right)
				{
					canon_right.alive = false
				}
				//changement de phase
			}	
			
			Image
			{
				material: "chapter-6/boss/" + boss.indice
				mask: health_boss ? Qt.rgba(1, health_boss.health/health_boss.max_health, health_boss.health/health_boss.max_health, 1 ) : Qt.rgba(1, 0, 0, 1 )
				scale: 1
				z: altitudes.boss - 0.00001
			}
			
			PolygonCollider
			{
				vertexes: [coords(533,57),coords(586,7),coords(670,7),coords(712,57),coords(754,752),coords(505,930)]
				group: groups.item_ignore_enemy
			}
			PolygonCollider
			{
				vertexes: [coords(1515,57),coords(1462,7),coords(1378,7),coords(1336,57),coords(1294,752),coords(1543,930)]
				group: groups.item_ignore_enemy
			}
			PolygonCollider
			{
				vertexes: [coords(400,977),coords(833,714),coords(1215,714),coords(1648,977),coords(1648,1277),coords(1515,1476),coords(533,1476),coords(400,1277)]
				group: groups.item_ignore_enemy
			}
		}
	}
		
	Q.Component
	{
		id: boss_1
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 15
			property real ti_local: root.ti
			property bool activate: true
			property bool activate_missile: false
			property var tab_slot_equipment:[
			[710,161,0,3],
			[902,137,0,4],
			[408,408,-1/4,1],
			[673,408,-1/4,2],
			[408,673,-1/4,1],
			[161,710,-1/2,3],
			[137,902,-1/2,4],
			[540,540,0,0],
			[1338,161,0,3],
			[1146,137,0,4],
			[1640,408,1/4,1],
			[1375,408,1/4,2],
			[1640,673,1/4,1],
			[1887,710,1/2,3],
			[1911,902,1/2,4],
			[1508,540,0,0],
			[710,1887,1,3],
			[902,1911,1,4],
			[408,1640,-3/4,1],
			[673,1640,-3/4,2],
			[408,1375,-3/4,1],
			[161,1338,-1/2,3],
			[137,1146,-1/2,4],
			[540,1508,0,0],
			[1338,1887,1,3],
			[1146,1911,1,4],
			[1640,1640,3/4,1],
			[1375,1640,3/4,2],
			[1640,1375,3/4,1],
			[1887,1338,1/2,3],
			[1911,1146,1/2,4],
			[1508,1508,0,0],
			[408,1166,0,0],
			[630,1166,0,0],
			[673,1375,-7/8,1],
			[902,1716,-7/8,3],
			[1640,1166,0,0],
			[1418,1166,0,0],
			[1375,1375,7/8,1],
			[1146,1716,7/8,3]
			]
			property var tab_weapon:[secondary_shield_factory,random_spray_factory,missile_launcher_factory,heavy_laser_gun_factory,quipoutre_factory]
			property real ti_deb: 0
			property real time_to_salve: 4
			onTi_localChanged:
			{
				if(activate)
				{
					if(ti_deb - root.ti > time_to_salve)
					{
						ti_deb = root.ti
						activate_missile = !activate_missile
					}
				}
			}
			
			Q.Component.onDestruction: 
			{
				boss_4.createObject(null, {scene: ship.scene, position: Qt.point(26.5 * 2 * chapter_scale ,30.5 * 2 * chapter_scale)})
			}
			
			
			Image
			{
				material: "chapter-6/boss/0"
				scale: 1.4
				z: altitudes.boss
			}
			Repeater
			{
				count: boss.tab_slot_equipment.length
				Q.Component
				{
					EquipmentSlot
					{
						scale: ((1-0.2)/boss.scale)
						Q.Component.onCompleted:
						{
							equipment = boss.tab_weapon[boss.tab_slot_equipment[index][3]].createObject(null, {slot: this})
							if(boss.tab_slot_equipment[index][3] != 0)
							{
								equipment.shooting = Qt.binding(function(){
								return boss.activate})
							}
							if(boss.tab_slot_equipment[index][3] == 2)
							{
								equipment.shooting = Qt.binding(function(){
								return boss.activate_missile})
							}
							if(boss.tab_slot_equipment[index][3] == 4)
							{
								equipment.period = Qt.binding(function(){
								return boss.activate_missile ? root.scene.seconds_to_frames(3) : root.scene.seconds_to_frames(1.5)})
							}
							angle = boss.tab_slot_equipment[index][2] * Math.PI
							position =  boss.coords(boss.tab_slot_equipment[index][0] , boss.tab_slot_equipment[index][1])
						}
					}
				}
			}
			Image
			{
				material: "equipments/shield"
				mask: Qt.rgba(0.58, 0, 0.82, activate ?  0 : 1)
				scale: 1.04
				z:altitudes.shield
			}
			CircleCollider
			{
				group: activate ? 0 : groups.enemy_invincible
				radius: 1.04
			}
			
			
			PolygonCollider
			{
				vertexes: [coords(38,750),coords(750,38),coords(1298,38),coords(2010,750),coords(2010,1298),coords(1298,2010),coords(750,2010),coords(38,1298)]
				group: activate ? groups.enemy_hull_naked : 0
			}
		}
	}
	
	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState:  state_test_1 // state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState:   state_dialogue_0  //state_test_1
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					music.objectName = "chapter-6/inside"
					camera_position= undefined
					for(var i = 0; i< tab_turret_corridor_1.length; i++)
					{
						turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_1[i][0] * 2 * root.chapter_scale, tab_turret_corridor_1[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_1[i][2], angle: tab_turret_corridor_1[i][3] * Math.PI, activate: true})
					}
					var shield = shield_barriere_factory.createObject(null, {scene: ship.scene, x: 38, y: 53, nb_repe: 54 ,tab_shield:[[43,42],[43,43],[43,44],[43,45],[43,46],[43,47],[43,48],[43,49],[43,50],[43,51],[43,52],[43,53]]})
					messages.add("lycop/normal", qsTr("begin 1"))
					messages.add("nectaire/normal", qsTr("begin 2"))
					messages.add("lycop/normal", qsTr("begin 3"))
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_2; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_story_2 
				signal: root.open_indChanged
				guard: root.open_ind == 1
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("open 1 1"))
					messages.add("nectaire/normal", qsTr("open 1 2"))
					messages.add("lycop/normal", qsTr("open 1 3"))
				}
			}
			
			SignalTransition
			{
				targetState: state_story_2 
				signal: root.open_indChanged
				guard: root.open_ind == 2
				onTriggered:
				{	
					messages.add("nectaire/normal", qsTr("open 2 1"))
					messages.add("lycop/normal", qsTr("open 2 2"))
					messages.add("nectaire/normal", qsTr("open 2 3"))
					messages.add("lycop/normal", qsTr("open 2 4"))
				}
			}
			
			SignalTransition
			{
				targetState: state_story_2 
				signal: root.open_indChanged
				guard: root.open_ind == 3
				onTriggered:
				{	
					messages.add("nectaire/normal", qsTr("open 3 1"))
					messages.add("lycop/normal", qsTr("open 3 2"))
					messages.add("nectaire/normal", qsTr("open 3 3"))
				}
			}
			
			SignalTransition
			{
				targetState: state_story_2 
				signal: root.open_indChanged
				guard: root.open_ind == 4
				onTriggered:
				{	
					messages.add("nectaire/normal", qsTr("open 4 1"))
					messages.add("lycop/normal", qsTr("open 4 2"))
				}
			}
			
			SignalTransition
			{
				targetState: state_story_2 
				signal: root.open_sortieChanged
				guard: root.open_sortie
				onTriggered:
				{	
					messages.add("nectaire/normal", qsTr("sortie 1"))
					messages.add("lycop/normal", qsTr("sortie 2"))
				}
			}
			
			SignalTransition
			{
				targetState: state_story_2 
				signal: on_sortie
				onTriggered:
				{	
					messages.add("nectaire/normal", qsTr("on_sortie 1"))
				}
			}
			
			SignalTransition
			{
				targetState: state_end 
				signal: sortie
				onTriggered:
				{	
					saved_game.add_science(1)
					saved_game.set("file", "game/chapter-7/chapter-7-corridor.qml")
					saved_game.save()
				}
			}
		}
		
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	
	Q.Component.onCompleted:
	{	
		for(var i = 0; i < 15; ++i) jobs.run(scene_view, "preload", ["chapter-2/turret-opening/" + i])
		var a2 = ["c", "e", "n", "ne", "nei", "nw", "nwi", "s", "se", "sei", "sw", "swi", "w"]; for(var i2 in a2) jobs.run(scene_view, "preload", ["chapter-2/metal/" + a2[i2]])
		jobs.run(scene_view, "preload", ["chapter-2/generator-1"])
		jobs.run(scene_view, "preload", ["chapter-7/starship/outdoor-masked"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-6/boss/" + i])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-6/exit/" + i])
		jobs.run(scene_view, "preload", ["chapter-2/shield-generator"])
		jobs.run(scene_view, "preload", ["chapter-2/barrier-wall"])
		jobs.run(scene_view, "preload", ["chapter-2/square-turret"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		jobs.run(scene_view, "preload", ["chapter-2/mini-boss-ss"])
		jobs.run(scene_view, "preload", ["chapter-2/boss-cache"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-6/console/" + i])
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		jobs.run(scene_view, "preload", ["chapter-6/boss/bullet"])
		jobs.run(scene_view, "preload", ["chapter-2/round-turret"])
		jobs.run(music, "preload", ["chapter-6/inside"])
		groups.init(scene)
		//root.info_print = screen_info_factory.createObject(screen_hud)
		//root.finalize = function() {if(info_print) info_print.destroy()}
	}
}