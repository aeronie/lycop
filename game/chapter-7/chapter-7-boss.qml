import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-7/chapter-7-boss.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	
	property alias ship: ship
	signal boss_1_dead
	signal boss_2_dead
	signal boss_3_dead
	signal boss_destroyed
	signal sortie
	property var camera_position: Qt.point(13 * 4, 21 * 4)
	property var last_ship_position: Qt.point(0, 0)
	property real ti: 0
	property var info_print
	property real chapter_scale: 2
	property var tab_turret: []
	property bool start_boss: false
	property var anim_turret
	property bool no_turret: false
	property bool travel_map_2: true
	property var boss
	property bool boss_alive: true
	property bool invulnerable: true
	property var tab_generator: []
	property int generator: 4
	property var barriere
	onGeneratorChanged:
	{
		if(generator == 0)
		{
			invulnerable = false
		}
	}
	
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	
	Q.Component
	{
		id: heavy_laser_gun_factory
		HeavyLaserGun
		{
			range_bullet: scene.seconds_to_frames(6)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: secondary_shield_factory
		SecondaryShield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: random_spray_factory
		RandomSpray
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: false
			enemy: 1
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(16)
			shooting: false
			period: scene.seconds_to_frames(3)
			level: 2
		}
	}
	
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-6/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-6/"+mat+"/c"])
			}
		}
		return fill_tiles
	}
	
	function fill_2(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y, mat])
			}
		}
		return fill_tiles
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}
	
	scene: Scene
	{
		running: false
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		
		Tiles
		{
			id: room
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[13,19,'n','metal'],
				[13,16,'w','metal'],
				[33,16,'n','metal'],
				[33,26,'e','metal'],
				[13,26,'s','metal'],
				[13,23,'w','metal'],
				[3,23,'s','metal'],
				[3,19,'w','metal']
				]).concat(
				fill(4,20,13,22,'metal'),
				fill(14,17,32,25,'metal'),
				[[15,18,'chapter-6/metal/c2'],
				[15,21,'chapter-6/metal/c2'],
				[15,24,'chapter-6/metal/c2'],
				[18,18,'chapter-6/metal/c2'],
				[18,21,'chapter-6/metal/c2'],
				[18,24,'chapter-6/metal/c2'],
				[21,18,'chapter-6/metal/c2'],
				[21,21,'chapter-6/metal/c2'],
				[21,24,'chapter-6/metal/c2']]
				)
		}
		
		Ship
		{
			id: ship	
			position.x: 13 * 4
			position.y: 21 * 4
			angle: Math.PI/2
			Q.Component.onDestruction: 
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
			
		}
		
		Body
		{
			id: all_wall
			position.y: 0
			position.x: 0
			scale: chapter_scale*2
			
			type: Body.STATIC
			
			function point(x,y)
			{
				return Qt.point(x - 0.25 , y + 0.25)
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(3,6), all_wall.point(13,6), all_wall.point(13, 19 - 2 *0.25), all_wall.point(3,19 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(13,6), all_wall.point(33 + 2 * 0.25, 6), all_wall.point(33 + 2 * 0.25 , 16 - 2 * 0.25), all_wall.point(13 , 16 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(33 + 2 * 0.25, 6), all_wall.point(43 , 6), all_wall.point(43,26), all_wall.point(33 + 2 * 0.25,26)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(13 , 26),
				all_wall.point(43 , 26),
				all_wall.point(43 , 36),
				all_wall.point(13 , 36)
				]
				group: groups.wall
			}	
			PolygonCollider
			{
				vertexes: [all_wall.point(3 , 23),
				all_wall.point(13 , 23),
				all_wall.point(13 , 36),
				all_wall.point(3 , 36)
				]
				group: groups.wall
			}	
		}

		
		Body
		{
			id: barriere_1
			type: Body.STATIC
			property real x:12.5
			property real y:22
			property int nb_repe: 18
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_1.x - 0.625)*2*chapter_scale
						position.y: (barriere_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, root.boss_alive ? 1 : 0) 
					}
				}
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_1.x - 0.375)*2*chapter_scale
						position.y: (barriere_1.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, root.boss_alive ? 1 : 0) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [barriere_1.coords(barriere_1.x - 0.75, barriere_1.y + 1.25 - ( barriere_1.nb_repe + 1 ) / 4),
				barriere_1.coords(barriere_1.x - 0.25, barriere_1.y + 1.25 - ( barriere_1.nb_repe + 1 ) / 4), 
				barriere_1.coords(barriere_1.x - 0.25, barriere_1.y + 1.25), 
				barriere_1.coords(barriere_1.x - 0.75, barriere_1.y + 1.25)]
				group: root.boss_alive ? groups.enemy_invincible : 0
			}
		}
		
		Body
		{
			id: barriere_2
			type: Body.STATIC
			property real x:22.5
			property real y:25
			property int nb_repe: 42
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_2.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_2.x - 0.625)*2*chapter_scale
						position.y: (barriere_2.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, root.start_boss ? 0 : 1 ) 
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_2.x - 0.375)*2*chapter_scale
						position.y: (barriere_2.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82,  root.start_boss ? 0 : 1) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_2.coords(barriere_2.x - 0.75, barriere_2.y + 1.25 - ( barriere_2.nb_repe + 1 ) / 4),
				barriere_2.coords(barriere_2.x - 0.25, barriere_2.y + 1.25 - ( barriere_2.nb_repe + 1 ) / 4), 
				barriere_2.coords(barriere_2.x - 0.25, barriere_2.y + 1.25), 
				barriere_2.coords(barriere_2.x - 0.75, barriere_2.y + 1.25)
				]
				group: root.start_boss ? 0 : groups.enemy_invincible
			}
			PolygonCollider
			{
				vertexes: [
				barriere_2.coords(22, 15.5),
				barriere_2.coords(33, 15.5), 
				barriere_2.coords(33, 26.5), 
				barriere_2.coords(22, 26.5)
				]
				group: root.start_boss ? 0 : groups.enemy_invincible
			}
		}
		
		Sensor
		{
			id: console_1
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(20 *  chapter_scale * 2, 17 *  chapter_scale * 2)
			
			onFuzzy_valueChanged:
			{	
				if(!root.start_boss)
				{
					if(fuzzy_value == 1)
					{
						root.start_boss = true
						music.objectName = "chapter-2/boss"
						indice = 24
					}
				}
			}
			property int indice: fuzzy_value != 1 ? Math.floor(fuzzy_value * 25) : 24
			Image
			{
				material: "chapter-6/console/" + console_1.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.start_boss ?  0 : groups.sensor
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: exit
			type: Body.STATIC
			position.x : 0
			position.y : 0
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie()
				}
			}
			PolygonCollider
			{
				vertexes: [
				exit.coords(4, 18.75),
				exit.coords(12, 18.75),
				exit.coords(12, 23.25),
				exit.coords(4, 23.25)
				]
				group:  root.boss_alive ? 0 : groups.sensor
				sensor : true
			}
		}
		
		Body
		{
			id: no_blink
			type: Body.STATIC
			position.x : 0
			position.y : 0
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			PolygonCollider
			{
				vertexes: [
				exit.coords(4, 18.75),
				exit.coords(12 , 18.75),
				exit.coords(12 , 23.25),
				exit.coords(4 , 23.25)
				]
				group: root.boss_alive ? groups.no_blink : 0 
				sensor : true
			}
		}
	}	
	
	Q.Component
	{
		id: barriere_inv_factory
		Body
		{
			id: barriere_2
			type: Body.STATIC
			property real x:26
			property real y:25
			property int nb_repe: 42
			property bool alive: root.invulnerable
			onAliveChanged: if(!alive) destroy()
			property real damages: 1000
			Q.Component.onDestruction: 
			{
				root.barriere = barriere_reflec_factory.createObject(null, {scene: root.scene, x:26 , y: 25 , nb_repe: 42})
			}
			
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere_2.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_2.x - 0.625)*2*chapter_scale
						position.y: (barriere_2.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, root.invulnerable ? 1 : 0 ) 
					}
				}
				
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere_2.x - 0.375)*2*chapter_scale
						position.y: (barriere_2.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82,  root.invulnerable ? 1 : 0) 
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_2.coords(barriere_2.x - 0.75, barriere_2.y + 1.25 - ( barriere_2.nb_repe + 1 ) / 4),
				barriere_2.coords(barriere_2.x - 0.25, barriere_2.y + 1.25 - ( barriere_2.nb_repe + 1 ) / 4), 
				barriere_2.coords(barriere_2.x - 0.25, barriere_2.y + 1.25), 
				barriere_2.coords(barriere_2.x - 0.75, barriere_2.y + 1.25)
				]
				group: root.invulnerable ? groups.enemy_invincible : 0
			}
			
			PolygonCollider
			{
				vertexes: [
				barriere_2.coords(26,15.75),
				barriere_2.coords(30,15.75), 
				barriere_2.coords(30,26.25), 
				barriere_2.coords(26,26.25)
				]
				group: root.invulnerable ? groups.enemy_dot : 0
			}
			
			
			
		}
	}
	
	
	Q.Component
	{
		id: barriere_reflec_factory
		Vehicle
		{
			id: shield_1
			type: Body.STATIC
			property real x:26
			property real y:25
			property int nb_repe: 42
			property var tab_shield:[] // x,y
			property var tab_shield_alive:[] 
			property var tab_collider: []
			property real ti_local: root.ti
			property bool reflection: false
			property real time_to_reflec: 3
			property real deb_reflec: 0
			property bool protec: true
			property real time_to_trans: 5
			property real deb_trans: 0
			property bool transition: false
			property real time_to_protec: 3
			property real deb_protec: 0
			property color mask: Qt.rgba(0.58, 0, 0.82, 1)
			property bool alive: true
			property int sens: -1
			onAliveChanged: if(!alive) destroy()
			
			Q.Component.onCompleted:
			{
				deb_protec = root.ti
			}
						
			onTi_localChanged:
			{
				if(reflection)
				{
					if(deb_reflec - ti_local > time_to_reflec)
					{
						reflection = false
						transition = true
						deb_trans = ti_local
						sens = -1
					}
				}
				if(transition)
				{
					//Qt.rgba(0.58, 0, 0.82, 1) protec
					//Qt.rgba(1, 1, 0, 1) reflection
					
					if(deb_trans - ti_local > time_to_trans)
					{
						if(sens == 1)
						{
							reflection = true
							transition = false
							deb_reflec = ti_local
						}
						if(sens == -1)
						{
							protec = true
							transition = false
							deb_protec = ti_local
						}
					}
					else
					{
						if(sens == 1)
						{
							mask.g = (deb_trans - ti_local) / time_to_trans
							mask.r = 0.58 + 0.42 * (deb_trans - ti_local) / time_to_trans
							mask.b = 0.82 * (1 - (deb_trans - ti_local) / time_to_trans)
						}
						else
						{
							mask.g = 1 - (deb_trans - ti_local) / time_to_trans
							mask.r = 1 - 0.42 * (deb_trans - ti_local) / time_to_trans
							mask.b = 0.82 * (deb_trans - ti_local) / time_to_trans
						}
					}
				}
				if(protec)
				{
					if(deb_protec - ti_local > time_to_protec)
					{
						protec = false
						transition = true
						deb_trans = ti_local
						sens = 1
					}
				}
			}
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.375)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: shield_1.mask
					}
				}
			}
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.625)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: shield_1.mask
					}
				}
			}
			
			
			function begin(other)
			{
				var v = direction_from_scene(other.velocity)
				other.velocity = direction_to_scene(Qt.point(-v.x, v.y))
				var a = 2 * angle - other.angle + Math.PI
				var e = scene.time_changed
				function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
				e.connect(f)
				for(var i in other.children)
				{
					var child = other.children[i]
					if(child.group) child.group ^= 1
					if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
				}
			}
			
			
			PolygonCollider
			{
				vertexes: [
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4),
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4), 
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25), 
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25)
				]
				group: shield_1.reflection ? groups.reflector_only_player : (shield_1.protec ? groups.enemy_invincible : 0)
			}
		}
	}

	Q.Component
	{
		id: energizer_factory
		Vehicle
		{
			id: turret
			property bool activate: false
			property int weapon_choice: cheat ? 2 : random_index(tab_weapons.length)
			type: Body.KINEMATIC
			max_speed: 0
			icon: 3
			icon_color: activate ? "red" : "transparent"
			property var tab_weapons: [random_spray_factory,heavy_laser_gun_factory,quipoutre_factory]
			property bool invulnerable: false
			property bool cheat: false
			property bool alive: !root.no_turret
			onAliveChanged: if(!alive) destroy()
			property real scale_deb: 0.1
			scale: scale_deb
			
			property real scale_finish: 3
			property real ti_deb: root.ti
			property real time_to_appear: 1
			property int ind_img: 0
			property real ti_local: root.ti
			property bool appear: false
			property int indice: 0
			property real ti_cour: 0
			onTi_localChanged:
			{
				if(!activate)
				{
					if(ti_deb - ti_local < time_to_appear)
					{
						if(ind_img < 14)
						{
							ind_img = Math.floor( 2 * (ti_deb - ti_local) / time_to_appear * 14)
							if(ind_img > 6)
							{
								if(!appear)
								{
									appear = true
									ti_cour = ti_local
								}
								scale += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb)
							}
						}
						else
						{
							scale += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb)
						}
					}
					else
					{
						appear = true
						scale = scale_finish
						activate = true
					}
				}
			}
			
			onScaleChanged:
			{
				collider.update_transform()
			}
			
			Q.Component.onCompleted:
			{
				ti_deb = root.ti			
			}
			
			Q.Component.onDestruction: 
			{
				root.generator--
			}
			
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 3}}
			}
			
			
			Image
			{
				id: image
				material: "chapter-2/turret-opening/" + ind_img
				mask: Qt.rgba(1, 1, 1, activate ? 0 : 1)
				z:  altitudes.boss + 0.000001
				scale: turret.scale_finish / turret.scale
			}
			
			CircleCollider
			{
				id: collider
				radius: 0.7 * turret.scale/turret.scale_finish
				group: turret.activate ? groups.item_ignore_enemy : 0
			}
			EquipmentSlot
			{
				scale: 0.7 * turret.scale/turret.scale_finish
				equipment: Energizer
				{
					scale_sh : 1
				}
				Q.Component.onCompleted:
				{ 
					equipment.image.mask.a = Qt.binding(function(){return (turret.appear ? 1 : 0)})
					equipment.image.mask.r = 1
					equipment.image.mask.g = Qt.binding(function(){return equipment.health / equipment.max_health})
					equipment.image.mask.b = Qt.binding(function(){return equipment.health / equipment.max_health})
				}
				onEquipmentChanged: if(!equipment) turret.deleteLater()
			}
		}
	}
	
	
	
	Q.Component
	{
		id: turret_factory
		Vehicle
		{
			id: turret
			property bool activate: false
			property int weapon_choice: cheat ? 2 : random_index(tab_weapons.length)
			type: Body.KINEMATIC
			max_speed: 0
			icon: 3
			icon_color: activate ? "red" : "transparent"
			property var tab_weapons: [random_spray_factory,heavy_laser_gun_factory,quipoutre_factory]
			property bool invulnerable: false
			property bool cheat: false
			property bool alive: !root.no_turret
			onAliveChanged: if(!alive) destroy()
			property real scale_deb: 0.1
			scale: scale_deb
			property real scale_finish: 1
			property real ti_deb: root.ti
			property real time_to_appear: 1
			property int ind_img: 0
			property real ti_local: root.ti
			property bool appear: false
			property int indice: 0
			property real ti_cour: 0
			onTi_localChanged:
			{
				if(!activate)
				{
					if(ti_deb - ti_local < time_to_appear)
					{
						if(ind_img < 14)
						{
							ind_img = Math.floor( 2 * (ti_deb - ti_local) / time_to_appear * 14)
							if(ind_img > 6)
							{
								if(!appear)
								{
									appear = true
									ti_cour = ti_local
								}
								scale += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb)
							}
						}
						else
						{
							scale += (scale_finish - scale_deb) /  scene.seconds_to_frames(time_to_appear + ti_cour - ti_deb)
						}
					}
					else
					{
						appear = true
						scale = 1
						activate = true
					}
				}
			}
			
			onScaleChanged:
			{
				collider.update_transform()
			}
			
			Q.Component.onCompleted:
			{
				ti_deb = root.ti			
			}
			
			Q.Component.onDestruction: 
			{
				if(turret.indice == root.tab_turret.length - 1)
				{
					root.tab_turret.pop()
				}
				else
				{
					root.tab_turret[turret.indice] = root.tab_turret.pop()
					root.tab_turret[turret.indice].indice = turret.indice
				}
			}
			
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 3}}
			}
			
			
			Image
			{
				material: "chapter-2/round-turret"
				mask: Qt.rgba(1, 1, 1, turret.appear ? 1 : 0)
				z: altitudes.boss + 0.00001
			}
			
			Image
			{
				id: image
				material: "chapter-2/turret-opening/" + ind_img
				mask: Qt.rgba(1, 1, 1, activate ? 0 : 1)
				z:  altitudes.boss + 0.000001
				scale: turret.scale_finish / turret.scale
			}
			
			CircleCollider
			{
				id: collider
				group: turret.activate ? groups.enemy_hull_naked : 0
			}
			EquipmentSlot
			{
				scale: 0.7 * turret.scale/turret.scale_finish
				Q.Component.onCompleted:
				{ 
					equipment = turret.tab_weapons[turret.weapon_choice].createObject(null, {slot: this})
					if(turret.cheat)
					{
						equipment.period = turret.scene.seconds_to_frames(2)
						equipment.bullet_velocity = 8
					}
					equipment.image.mask.a = Qt.binding(function(){return (turret.appear ? 1 : 0)})
					equipment.shooting = Qt.binding(function(){return turret.activate})
				}
				onEquipmentChanged: if(!equipment) turret.deleteLater()
			}
			Behaviour_attack_distance
			{
				id: behaviour
				enabled: turret.activate
				target_groups: groups.enemy_targets
				max_distance: 60
			}
		}
	}
	
	Q.Component
	{
		id: turret_spawner_factory
		Animation
		{
			id: timer
			time: 0
			speed: -1
			property bool activate: root.start_boss
			property bool alive: root.boss_alive
			onAliveChanged: if(!alive) destroy()
			property int max_turret: 10
			property real time_to_spawn: 5
			property real vener: 4
			property real last_spawn: 10
			property int cap_j: tab.length
			property var tab: [
			[922,970],
			[922,1173],
			[1126,970],
			[1126,1173]
			]
			property real sq_diff: 0.5
			
			
			Q.Component.onDestruction: 
			{
				root.no_turret = true
			}
			
			
			function next_position()
			{
				var choose = false
				var j = 0
				if(root.boss)
				{
					while(!choose && j < timer.cap_j)
					{
						var module = 4.5 * 2 * chapter_scale + 2.3 * 2 * chapter_scale * Math.random()
						var angle = 2 * Math.PI * Math.random()
						var x = root.boss.position.x + root.boss.scale * ( ( tab[j][1] / 1024 ) - 1 )
						var y = root.boss.position.y + root.boss.scale * ( ( tab[j][0] / 1024 ) - 1 ) 
						choose = true
						for(var i = 0; i< root.tab_turret.length; i++)
						{
							var x_diff = x - root.tab_turret[i].position.x
							var y_diff = y - root.tab_turret[i].position.y
							if(x_diff * x_diff + y_diff * y_diff < sq_diff * sq_diff)
							{
								choose = false
								j++
							}
						}
					}
				}
				else
				{
					j = 5
					var x = 0
					var y = 0
				}
				return [Qt.point(x,y), j]
			}
			
			onTimeChanged:
			{
				if(activate)
				{
					if(last_spawn - time > time_to_spawn / vener)
					{
						last_spawn = time
						var ind = tab_turret.length
						var pos_j = next_position()
						if(pos_j[1] < cap_j)
						{
							root.tab_turret.push(turret_factory.createObject(null, {scene: ship.scene, position: pos_j[0], cheat: false, indice: ind, weapon_choice: 1, angle : - Math.PI / 2}))
						}
					}
				}
			}
		}
	}
	
	
	Q.Component
	{
		id: armagedon_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: "transparent"
			scale: 0.01
			property real z: 0.01
			readonly property real damages: 10000
			Q.Component.onCompleted: 
			{
				boom_s.play()
			}
			onScaleChanged:
			{
				collider.update_transform()
			}
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			property Sound boom_s : Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 5000
			}
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				sensor: true
			}
		}
	}
	
	Q.Component
	{
		id: laser_invisible
		Weapon
		{
			property int level: 2
			property int bullet_group: groups.enemy_laser
			period: scene.seconds_to_frames([0, 1 / 3, 1 / 10] [level])
			health: max_health
			max_health: 150 +~~body.health_boost
			range_bullet: scene.seconds_to_frames(2)
			readonly property Sound sound_shooting: Sound
			{
				objectName: "equipments/laser-gun/shooting"
				scale: slot.sound_scale
			}
			bullet_factory: Bullet
			{
				scale: 0.3
				range: weapon.range_bullet
				damages: 100
				Image
				{
					material: "equipments/laser-gun/bullet"
					z: altitudes.bullet
				}
				PolygonCollider
				{
					vertexes: [Qt.point(-0.125, -1), Qt.point(-0.125, 1), Qt.point(0.125, 1), Qt.point(0.125, -1)]
					group: weapon.bullet_group
					sensor: true
				}
				Q.Component.onCompleted:
				{
					position = weapon.slot.point_to_scene(Qt.point(0, -0.5))
					angle = weapon.slot.angle_to_scene(0)
					weapon.sound_shooting.play()
					weapon.set_bullet_velocity(this, 20)
				}
			}
		}
	}
	
	Q.Component
	{
		id: canon_factory_3
		Vehicle
		{
			id: canon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool shoot: false
			EquipmentSlot
			{
				id: eq_slot
				equipment: Weapon
				{
					id: weapon
					health: max_health
					max_health: 75000
					shooting: canon.shoot
					period: scene.seconds_to_frames(15)
					bullet_factory: Vehicle
					{
						id: drone
						property bool alive: true
						function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
						onAliveChanged: if(!alive) destroy()
						property real damages: 100000
						property real ti_local: root.ti / 1.2
						max_angular_speed: 0
						scale: 3
						max_speed: 5
						property bool start: false
						property real ti_deb: 0
						property int ind_star: 0
						onTi_localChanged:
						{
							ind_star = (ind_star + 1)% 10
							if(ti_deb - root.ti < 0.7)
							{
								scale += 3 / scene.seconds_to_frames(0.7)
							}
							else
							{
								if(!start)
								{
									start = true
								}
							}
						}

						Sound
						{
							id: sound
							objectName: "chapter-4/boss_canon"
							scale: 4000
						}

						function is_started()
						{
							return drone.start
						}
						Image
						{
							material: "chapter-5/star-bullet/" + drone.ind_star
							angle: drone.ti_local
						}

						Q.Component.onCompleted:
						{
							if(root.boss)
							{
								angle = root.boss.angle
							}
							ti_deb = root.ti
							position = weapon.slot.point_to_scene(Qt.point(0, 0))
							sound.play()
						}

						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: 1 / 2 * Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: 3 / 2 * Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						Behaviour_attack_missile
						{
							target_groups: [groups.enemy_checkpoint]
							range: scene.seconds_to_frames(15)
						}
						CircleCollider
						{
							group: groups.enemy_dot
							radius: 0.76
							sensor: true
						}
					}
				}
			}
		}
	}
	Q.Component
	{
		id: canon_factory_2
		Vehicle
		{
			id: canon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool shoot: false
			property real shoot_period: 4
			EquipmentSlot
			{
				id: eq_slot
				equipment: Weapon
				{
					id: weapon
					health: max_health
					max_health: 25000
					shooting: canon.shoot
					period: scene.seconds_to_frames(canon.shoot_period)
					readonly property Sound sound_shooting: Sound
					{
						objectName: "equipments/heavy-laser-gun/shooting"
						scale: 40
					}
					bullet_factory: Bullet
					{
						id: bullet
						scale: 1.5
						range: scene.seconds_to_frames(20)
						damages: 10000
						Image
						{
							material: "chapter-6/boss/bullet"
							z: altitudes.bullet
							scale: 1.2
						}
						PolygonCollider
						{
							vertexes: [Qt.point(-0.125, -1), Qt.point(-0.125, 1), Qt.point(0.125, 1), Qt.point(0.125, -1)]
							group: groups.enemy_laser
							sensor: true
						}
						Q.Component.onCompleted:
						{
							position = weapon.slot.point_to_scene(Qt.point(0, -bullet.scale))
							angle = weapon.slot.angle_to_scene(0)
							weapon.sound_shooting.play()
							weapon.set_bullet_velocity(this, 7)
						}
					}
				}
			}
		}
	}
	
	Q.Component
	{
		id: explosion_laser_factory
		Body
		{
			id: component
			type: Body.STATIC
			property bool megaboom: false
			scale: scale_deb
			property color mask: "orange"
			property var material: "chapter-4/explosion-bullet"
			property real ti_local: root.ti
			property real damages: 5000 * scene.time_step
			property real deb: root.ti
			property real scale_finish : megaboom ? 36 : 12
			property real scale_deb: 0.1
			onTi_localChanged:
			{
				if(scale_finish > 0.1)
				{
					scale += (scale_finish - scale_deb)/scene.seconds_to_frames(0.5)
					if(scale > scale_finish)
					{
						scale = scale_finish
					}
					image.mask.a = (scale_finish - scale)/scale_finish
					if(scale == scale_finish)
					{	
						component.deleteLater()
					}
				}
			}
			Q.Component.onCompleted:
			{
				
				deb = root.ti
				sound.play()
			}
			onScaleChanged:
			{
				angle += 0.0001
				collider.update_transform()
			}
			Image
			{
				id: image
				material: component.material
				mask: component.mask
			}
			Sound
			{
				id: sound
				objectName: "chapter-4/bullet_boom"
				scale: 5000
			}
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				sensor: true
			}
		}
	}
	Q.Component
	{
		id: canon_factory_1
		Vehicle
		{
			id: boss
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			property bool alive: true
			property int period: scene.seconds_to_frames(5)
			property bool megashoot: false
			property bool shoot: true
			onAliveChanged: if(!alive) destroy()
			scale: 15
			EquipmentSlot
			{
				id: eq_slot
				scale: 0.15/3
				equipment: Weapon
				{
					id: weapon
					shooting: boss.shoot
					property bool mega: boss.megashoot
					property int bullet_group: groups.enemy_dot
					property real bullet_velocity: 5 //utile pour cheater des enemy
					property real damages_aoe: 3000* scene.time_step
					period: boss.period
					range_bullet: scene.seconds_to_frames(8)
					health: max_health
					max_health: 400 +~~body.health_boost
					readonly property Sound sound_shooting: Sound
					{
						objectName: "equipments/qui-poutre/shooting"
						scale: weapon.slot.sound_scale
					}
					bullet_factory: Bullet
					{
						id: bullet
						readonly property int max_range: weapon.range_bullet
						property bool mega: false
						property real scale_temp: mega ? 4 : 2
						scale: scale_temp * Math.min(1, (max_range - range) / scene.seconds_to_frames(0.5))
						onScaleChanged: collider.update_transform()
						range: max_range
						damages: 3000
						angular_velocity: 1
						Image
						{
							material: "chapter-4/boss/gigabullet/" + (Math.floor(scene.time / 5) % 8)
							mask: Qt.rgba(1, range / max_range, 0, 1)
							z: altitudes.bullet
						}
						CircleCollider
						{
							id: collider
							group: weapon.bullet_group
							radius: 0.5
							sensor: true
						}
						Q.Component.onCompleted:
						{
							if(weapon.mega)
							{
								mega = true
							}
							position = weapon.slot.point_to_scene(Qt.point(0, 0))
							angle = weapon.slot.angle_to_scene(0)
							weapon.sound_shooting.play()
							weapon.set_bullet_velocity(this, weapon.bullet_velocity)
						}
						Q.Component.onDestruction:
						{
							explosion_laser_factory.createObject(null, {scene: root.scene ,position: Qt.point(bullet.position.x,bullet.position.y), mask: Qt.rgba(1,0,0,1), megaboom: mega})
						}
					}
				}
			}
		}
	}
	
	Q.Component
	{
		id: explosion_factory
		Explosion
		{
			scale: 4
		}
	}
	Q.Component
	{
		id: boss_transition_end
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 22
			angle: - Math.PI / 2
			property real ti_local: root.ti
			property real deb_trans: root.ti
			property real time_to_white: 2
			property real deb_sc: root.ti
			property real time_to_sc: 15
			property real scale_sh: 1.04
			property real last_boom: root.ti
			property bool swap: true
			property bool finish: false
			property var armagedon
			onTi_localChanged:
			{
				if(deb_trans != 0)
				{
					if(deb_trans - root.ti < time_to_sc)
					{
						var explosion_step = 0.1
						var ajust = swap ? 0.1 : -0.1
						swap= !swap
						boss.position = Qt.point(boss.position.x + ajust, boss.position.y + ajust)
						if(last_boom - root.ti  > explosion_step)
						{
							last_boom = root.ti 
							var angle_ran = 2 * Math.PI * Math.random()
							var scale_ran = boss.scale * Math.random()
							explosion_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss.position.x + scale_ran * Math.cos(angle_ran), boss.position.y + scale_ran * Math.sin(angle_ran)), scale : 6})
						}
					}
					else
					{
						if(!armagedon)
						{
							armagedon = armagedon_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss.position.x + 15 , boss.position.y), mask_c: Qt.rgba(1,0,0,1)})
						}
						else
						{
							armagedon.scale += 10/scene.seconds_to_frames(1) 
						}
					}
				}
			}
			
			Q.Component.onCompleted:
			{
				deb_trans = root.ti
				last_boom = root.ti
			}
			
			Image
			{
				material: "chapter-7/boss-generator/3"
				mask: Qt.rgba(1,0,0,1)
				z: altitudes.boss
			}
			
			
		}
	}
	Q.Component
	{
		id: boss_5
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 22
			angle: - Math.PI / 2
			property real ti_local: root.ti
			property bool activate: root.start_boss
			property bool invulnerable : root.invulnerable
			property var tab_canon: []
			property var tab_canon_pos_1:[
			[355,816],
			[1693,816]
			]
			property var tab_canon_pos_2:[
			[156,869],
			[562,869],
			[1892,869],
			[1486,869]
			]
			property var tab_canon_pos_3:[
			[1024,666]
			]
			property var tab_weapon:[secondary_shield_factory,heavy_laser_gun_factory,quipoutre_factory]
			property real ti_deb: 0
			property real time_to_start: 2.5
			onTi_localChanged:
			{
				if(activate)
				{
					if(tab_canon && tab_canon[1] && !tab_canon[1].shoot && ti_deb - root.ti > time_to_start)
					{
						tab_canon[1].shoot = true
					}
				}
			}
			
			Q.Component.onCompleted:
			{	
				ti_deb = root.ti
				for(var i = 0 ; i < tab_canon_pos_1.length; i++)
				{
					var x = root.boss.position.x + root.boss.scale * ( ( tab_canon_pos_1[i][1] / 1024 ) - 1 )
					var y = root.boss.position.y + root.boss.scale * ( ( tab_canon_pos_1[i][0] / 1024 ) - 1 ) 
					tab_canon.push(canon_factory_1.createObject(null, {scene: ship.scene, position: Qt.point(x ,y), angle: -Math.PI/2, shoot: false}))
				}
				tab_canon[0].shoot = true
				for(var i = 0 ; i < tab_canon_pos_2.length; i++)
				{
					var x = root.boss.position.x + root.boss.scale * ( ( tab_canon_pos_2[i][1] / 1024 ) - 1 )
					var y = root.boss.position.y + root.boss.scale * ( ( tab_canon_pos_2[i][0] / 1024 ) - 1 ) 
					tab_canon.push(canon_factory_2.createObject(null, {scene: ship.scene, position: Qt.point(x ,y), angle: -Math.PI/2, shoot: true}))
				}
				for(var i = 0 ; i < tab_canon_pos_3.length; i++)
				{
					var x = root.boss.position.x + root.boss.scale * ( ( tab_canon_pos_3[i][1] / 1024 ) - 1 )
					var y = root.boss.position.y + root.boss.scale * ( ( tab_canon_pos_3[i][0] / 1024 ) - 1 ) 
					tab_canon.push(canon_factory_3.createObject(null, {scene: ship.scene, position: Qt.point(x ,y), angle: -Math.PI/2, shoot: true}))
				}
			}
			
			Q.Component.onDestruction: 
			{
				for(var i = 0 ; i < tab_canon.length; i++)
				{
					tab_canon[i].alive = false
				}
				if(root.barriere)
				{
					root.barriere.alive = false
				}
				root.boss_alive = false
				root.boss = boss_transition_end.createObject(null, {scene: root.scene, position: Qt.point(28 * 2 * chapter_scale ,21 * 2 * chapter_scale)})
			}
			
			
			Image
			{
				material: "chapter-7/boss-generator/3"
				mask: health_boss ? Qt.rgba(1, health_boss.health / health_boss.max_health, health_boss.health / health_boss.max_health, 1) : Qt.rgba(1,0,0,1)
				z: altitudes.boss
			}
			EquipmentSlot
			{
				id: slot_1
				equipment: Equipment
				{
					id: health_boss
					health: max_health
					max_health: 75000
				}
			}
			PolygonCollider
			{
				vertexes: [coords(57,977),coords(1991,977),coords(1991,1996),coords(57,1996)]
				group: activate ? (boss.invulnerable ? groups.enemy_invincible : groups.item_ignore_enemy) : 0
			}
			PolygonCollider
			{
				vertexes: [coords(768,977),coords(768,817),coords(1280,817),coords(1280,977)]
				group: activate ? (boss.invulnerable ? groups.enemy_invincible : groups.item_ignore_enemy)  : 0
			}
		}
	}
	
	Q.Component
	{
		id: boss_4
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 22
			angle: - Math.PI / 2
			property real ti_local: root.ti
			property bool activate: root.start_boss
			property var tab_canon: []
			property var tab_energizer:[
			[14,17],
			[16,17],
			[14,25],
			[16,25]
			]
			property var tab_canon_pos_1:[
			[355,816],
			[1693,816]
			]
			property var tab_canon_pos_2:[
			[156,869],
			[562,869],
			[1892,869],
			[1486,869]
			]
			property var tab_canon_pos_3:[
			[1024,666]
			]
			property var tab_slot_equipment:[
			[101,1178,0,1],
			[306,1178,0,1],
			[511,1178,0,1],
			[716,1178,0,1],
			[151,1535,0,2],
			[382,1535,0,2],
			[613,1535,0,2],
			[154,1893,0,0],
			[458,1893,0,0],
			[1947,1178,0,1],
			[1742,1178,0,1],
			[1537,1178,0,1],
			[1332,1178,0,1],
			[1897,1535,0,2],
			[1666,1535,0,2],
			[1435,1535,0,2],
			[1894,1893,0,0],
			[1590,1893,0,0]
			]
			property var tab_weapon:[secondary_shield_factory,heavy_laser_gun_factory,quipoutre_factory]
			property real ti_deb: 0
			property real time_to_start: 2.5
			onTi_localChanged:
			{
				if(activate)
				{
					if(tab_canon && tab_canon[1] && !tab_canon[1].shoot && ti_deb - root.ti > time_to_start)
					{
						tab_canon[1].shoot = true
					}
				}
			}
			
			Q.Component.onCompleted:
			{	
				ti_deb = root.ti
				for(var i = 0 ; i < tab_canon_pos_1.length; i++)
				{
					var x = root.boss.position.x + root.boss.scale * ( ( tab_canon_pos_1[i][1] / 1024 ) - 1 )
					var y = root.boss.position.y + root.boss.scale * ( ( tab_canon_pos_1[i][0] / 1024 ) - 1 ) 
					tab_canon.push(canon_factory_1.createObject(null, {scene: ship.scene, position: Qt.point(x ,y), angle: -Math.PI/2, shoot: false}))
				}
				tab_canon[0].shoot = true
				for(var i = 0 ; i < tab_canon_pos_2.length; i++)
				{
					var x = root.boss.position.x + root.boss.scale * ( ( tab_canon_pos_2[i][1] / 1024 ) - 1 )
					var y = root.boss.position.y + root.boss.scale * ( ( tab_canon_pos_2[i][0] / 1024 ) - 1 ) 
					tab_canon.push(canon_factory_2.createObject(null, {scene: ship.scene, position: Qt.point(x ,y), angle: -Math.PI/2, shoot: true}))
				}
				for(var i = 0 ; i < tab_canon_pos_3.length; i++)
				{
					var x = root.boss.position.x + root.boss.scale * ( ( tab_canon_pos_3[i][1] / 1024 ) - 1 )
					var y = root.boss.position.y + root.boss.scale * ( ( tab_canon_pos_3[i][0] / 1024 ) - 1 ) 
					tab_canon.push(canon_factory_3.createObject(null, {scene: ship.scene, position: Qt.point(x ,y), angle: -Math.PI/2, shoot: true}))
				}
			}
			
			Q.Component.onDestruction: 
			{
				for(var i = 0 ; i < tab_canon.length; i++)
				{
					tab_canon[i].alive = false
				}
				for(var i = 0 ; i < tab_energizer.length ; i++)
				{
					root.tab_generator.push(energizer_factory.createObject(null, {scene: root.scene, position: Qt.point(tab_energizer[i][0] * 2 * chapter_scale ,tab_energizer[i][1] * 2 * chapter_scale), indice:i}))
				}
				barriere_inv_factory.createObject(null, {scene: root.scene})
				root.boss = boss_5.createObject(null, {scene: root.scene, position: Qt.point(28 * 2 * chapter_scale ,21 * 2 * chapter_scale)})
			}
			
			
			Image
			{
				material: "chapter-7/boss-generator/3"
				z: altitudes.boss
			}
			Repeater
			{
				count: boss.tab_slot_equipment.length
				Q.Component
				{
					EquipmentSlot
					{
						scale: ((1-0.2)/boss.scale)
						Q.Component.onCompleted:
						{
							equipment = boss.tab_weapon[boss.tab_slot_equipment[index][3]].createObject(null, {slot: this})
							if(boss.tab_slot_equipment[index][3] != 0)
							{
								equipment.shooting = Qt.binding(function(){
								return boss.activate})
							}
							angle = boss.tab_slot_equipment[index][2] * Math.PI
							position =  boss.coords(boss.tab_slot_equipment[index][0] , boss.tab_slot_equipment[index][1])
						}
					}
				}
			}
			
			
			PolygonCollider
			{
				vertexes: [coords(57,977),coords(1991,977),coords(1991,1996),coords(57,1996)]
				group: activate ? groups.enemy_hull_naked : 0
			}
			PolygonCollider
			{
				vertexes: [coords(768,977),coords(768,817),coords(1280,817),coords(1280,977)]
				group: activate ? groups.enemy_hull_naked : 0
			}
		}
	}
	
	Q.Component
	{
		id: boss_3_transition
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 22
			angle: - Math.PI / 2
			
			
			Image
			{
				material: "chapter-7/boss-generator/2"
				z: altitudes.boss
			}
			
			
		}
	}
	Q.Component
	{
		id: boss_3
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 22
			angle: - Math.PI / 2
			property real ti_local: root.ti
			property bool activate: root.start_boss
			property var tab_canon: []
			property var tab_canon_pos_1:[
			[355,816],
			[1693,816]
			]
			property var tab_canon_pos_2:[
			[156,869],
			[562,869],
			[1892,869],
			[1486,869]
			]
			property var tab_slot_equipment:[
			[101,1178,0,1],
			[306,1178,0,1],
			[511,1178,0,1],
			[716,1178,0,1],
			[151,1535,0,2],
			[382,1535,0,2],
			[613,1535,0,2],
			[154,1893,0,0],
			[458,1893,0,0],
			[1947,1178,0,1],
			[1742,1178,0,1],
			[1537,1178,0,1],
			[1332,1178,0,1],
			[1897,1535,0,2],
			[1666,1535,0,2],
			[1435,1535,0,2],
			[1894,1893,0,0],
			[1590,1893,0,0]
			]
			property var tab_weapon:[secondary_shield_factory,heavy_laser_gun_factory,quipoutre_factory]
			property real ti_deb: 0
			property real time_to_start: 2.5
			onTi_localChanged:
			{
				if(activate)
				{
					if(tab_canon && tab_canon[1] && !tab_canon[1].shoot && ti_deb - root.ti > time_to_start)
					{
						tab_canon[1].shoot = true
					}
				}
			}
			
			Q.Component.onCompleted:
			{	
				ti_deb = root.ti
				for(var i = 0 ; i < tab_canon_pos_1.length; i++)
				{
					var x = root.boss.position.x + root.boss.scale * ( ( tab_canon_pos_1[i][1] / 1024 ) - 1 )
					var y = root.boss.position.y + root.boss.scale * ( ( tab_canon_pos_1[i][0] / 1024 ) - 1 ) 
					tab_canon.push(canon_factory_1.createObject(null, {scene: ship.scene, position: Qt.point(x ,y), angle: -Math.PI/2, shoot: false}))
				}
				tab_canon[0].shoot = true
				for(var i = 0 ; i < tab_canon_pos_2.length; i++)
				{
					var x = root.boss.position.x + root.boss.scale * ( ( tab_canon_pos_2[i][1] / 1024 ) - 1 )
					var y = root.boss.position.y + root.boss.scale * ( ( tab_canon_pos_2[i][0] / 1024 ) - 1 ) 
					tab_canon.push(canon_factory_2.createObject(null, {scene: ship.scene, position: Qt.point(x ,y), angle: -Math.PI/2, shoot: true}))
				}
			}
			
			Q.Component.onDestruction: 
			{
				for(var i = 0 ; i < tab_canon.length; i++)
				{
					tab_canon[i].alive = false
				}
				root.boss_3_dead()
				root.boss = boss_3_transition.createObject(null, {scene: ship.scene, position: Qt.point(28 * 2 * chapter_scale ,21 * 2 * chapter_scale)})
			}
			
			
			Image
			{
				material: "chapter-7/boss-generator/2"
				z: altitudes.boss
			}
			Repeater
			{
				count: boss.tab_slot_equipment.length
				Q.Component
				{
					EquipmentSlot
					{
						scale: ((1-0.2)/boss.scale)
						Q.Component.onCompleted:
						{
							equipment = boss.tab_weapon[boss.tab_slot_equipment[index][3]].createObject(null, {slot: this})
							if(boss.tab_slot_equipment[index][3] != 0)
							{
								equipment.shooting = Qt.binding(function(){
								return boss.activate})
							}
							angle = boss.tab_slot_equipment[index][2] * Math.PI
							position =  boss.coords(boss.tab_slot_equipment[index][0] , boss.tab_slot_equipment[index][1])
						}
					}
				}
			}
			
			
			PolygonCollider
			{
				vertexes: [coords(57,977),coords(1991,977),coords(1991,1996),coords(57,1996)]
				group: activate ? groups.enemy_hull_naked : 0
			}
			PolygonCollider
			{
				vertexes: [coords(768,977),coords(768,817),coords(1280,817),coords(1280,977)]
				group: activate ? groups.enemy_hull_naked : 0
			}
		}
	}
	
	Q.Component
	{
		id: boss_2_transition
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 22
			angle: - Math.PI / 2
			
			
			Image
			{
				material: "chapter-7/boss-generator/1"
				z: altitudes.boss
			}
			
			
		}
	}
	
	
	Q.Component
	{
		id: boss_2
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 22
			angle: - Math.PI / 2
			property real ti_local: root.ti
			property bool activate: root.start_boss
			property var tab_canon: []
			property var tab_canon_pos_1:[
			[355,816],
			[1693,816]
			]
			property var tab_slot_equipment:[
			[101,1178,0,1],
			[306,1178,0,1],
			[511,1178,0,1],
			[716,1178,0,1],
			[151,1535,0,2],
			[382,1535,0,2],
			[613,1535,0,2],
			[154,1893,0,0],
			[458,1893,0,0],
			[1947,1178,0,1],
			[1742,1178,0,1],
			[1537,1178,0,1],
			[1332,1178,0,1],
			[1897,1535,0,2],
			[1666,1535,0,2],
			[1435,1535,0,2],
			[1894,1893,0,0],
			[1590,1893,0,0]
			]
			property var tab_weapon:[secondary_shield_factory,heavy_laser_gun_factory,quipoutre_factory]
			property real ti_deb: 0
			property real time_to_start: 2.5
			onTi_localChanged:
			{
				if(activate)
				{
					if(tab_canon && tab_canon[1] && !tab_canon[1].shoot && ti_deb - root.ti > time_to_start)
					{
						tab_canon[1].shoot = true
					}
				}
			}
			
			Q.Component.onCompleted:
			{	
				ti_deb = root.ti
				for(var i = 0 ; i < tab_canon_pos_1.length; i++)
				{
					var x = root.boss.position.x + root.boss.scale * ( ( tab_canon_pos_1[i][1] / 1024 ) - 1 )
					var y = root.boss.position.y + root.boss.scale * ( ( tab_canon_pos_1[i][0] / 1024 ) - 1 ) 
					tab_canon.push(canon_factory_1.createObject(null, {scene: ship.scene, position: Qt.point(x ,y), angle: -Math.PI/2, shoot: false}))
				}
				tab_canon[0].shoot = true
			}
			
			Q.Component.onDestruction: 
			{
				for(var i = 0 ; i < tab_canon.length; i++)
				{
					tab_canon[i].alive = false
				}
				root.boss_2_dead()
				root.boss = boss_2_transition.createObject(null, {scene: root.scene, position: Qt.point(28 * 2 * chapter_scale ,21 * 2 * chapter_scale)})
			}
			
			
			Image
			{
				material: "chapter-7/boss-generator/1"
				z: altitudes.boss
			}
			Repeater
			{
				count: boss.tab_slot_equipment.length
				Q.Component
				{
					EquipmentSlot
					{
						scale: ((1-0.2)/boss.scale)
						Q.Component.onCompleted:
						{
							equipment = boss.tab_weapon[boss.tab_slot_equipment[index][3]].createObject(null, {slot: this})
							if(boss.tab_slot_equipment[index][3] != 0)
							{
								equipment.shooting = Qt.binding(function(){
								return boss.activate})
							}
							angle = boss.tab_slot_equipment[index][2] * Math.PI
							position =  boss.coords(boss.tab_slot_equipment[index][0] , boss.tab_slot_equipment[index][1])
						}
					}
				}
			}
			
			
			PolygonCollider
			{
				vertexes: [coords(57,977),coords(1991,977),coords(1991,1996),coords(57,1996)]
				group: activate ? groups.enemy_hull_naked : 0
			}
			PolygonCollider
			{
				vertexes: [coords(768,977),coords(768,817),coords(1280,817),coords(1280,977)]
				group: activate ? groups.enemy_hull_naked : 0
			}
		}
	}
	
	Q.Component
	{
		id: boss_1_transition
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 22
			angle: - Math.PI / 2
			
			
			Image
			{
				material: "chapter-7/boss-generator/0"
				z: altitudes.boss
			}
			
			
		}
	}
	
	Q.Component
	{
		id: boss_1
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 22
			angle: - Math.PI / 2
			property real ti_local: root.ti
			property bool activate: root.start_boss
			property bool activate_missile: false
			property var tab_slot_equipment:[
			[101,1178,0,1],
			[306,1178,0,1],
			[511,1178,0,1],
			[716,1178,0,1],
			[151,1535,0,2],
			[382,1535,0,2],
			[613,1535,0,2],
			[154,1893,0,0],
			[458,1893,0,0],
			[1947,1178,0,1],
			[1742,1178,0,1],
			[1537,1178,0,1],
			[1332,1178,0,1],
			[1897,1535,0,2],
			[1666,1535,0,2],
			[1435,1535,0,2],
			[1894,1893,0,0],
			[1590,1893,0,0]
			]
			property var tab_weapon:[secondary_shield_factory,heavy_laser_gun_factory,quipoutre_factory]
			property real ti_deb: 0
			property real time_to_salve: 4
		
			
			Q.Component.onDestruction: 
			{
				root.boss_1_dead()
				root.boss = boss_1_transition.createObject(null, {scene: root.scene, position: Qt.point(28 * 2 * chapter_scale ,21 * 2 * chapter_scale)})
			}
			
			
			Image
			{
				material: "chapter-7/boss-generator/0"
				z: altitudes.boss
			}
			Repeater
			{
				count: boss.tab_slot_equipment.length
				Q.Component
				{
					EquipmentSlot
					{
						scale: ((1-0.2)/boss.scale)
						Q.Component.onCompleted:
						{
							equipment = boss.tab_weapon[boss.tab_slot_equipment[index][3]].createObject(null, {slot: this})
							if(boss.tab_slot_equipment[index][3] != 0)
							{
								equipment.shooting = Qt.binding(function(){
								return boss.activate})
							}
							angle = boss.tab_slot_equipment[index][2] * Math.PI
							position =  boss.coords(boss.tab_slot_equipment[index][0] , boss.tab_slot_equipment[index][1])
						}
					}
				}
			}
			
			
			PolygonCollider
			{
				vertexes: [coords(57,977),coords(1991,977),coords(1991,1996),coords(57,1996)]
				group: activate ? groups.enemy_hull_naked : 0
			}
			PolygonCollider
			{
				vertexes: [coords(768,977),coords(768,817),coords(1280,817),coords(1280,977)]
				group: activate ? groups.enemy_hull_naked : 0
			}
		}
	}
	
	
	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState:   state_test_1 // state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState:   state_dialogue_0
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					camera_position= undefined
					music.objectName = "chapter-6/inside"
					messages.add("nectaire/normal", qsTr("begin 1"))
					messages.add("lycop/normal", qsTr("begin 2"))
				}
			}
		}	
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_1; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_video_opening_1
				signal: boss_1_dead
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-7/first-cannon.ogg"
					function f ()
					{
						root.boss.alive = false
						root.boss = boss_2.createObject(null, {scene: ship.scene, position: Qt.point(28 * 2 * chapter_scale ,21 * 2 * chapter_scale)})
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
			}
		}
		
		State
		{
			id: state_after_video_opening_1
			SignalTransition
			{
				targetState: state_story_2
				signal: state_after_video_opening_1.propertiesAssigned
				guard:!screen_video.video.file
			}
		}	
		
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_video_opening_2
				signal: boss_2_dead
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-7/second-cannon.ogg"
					function f ()
					{
						root.boss.alive = false
						root.boss = boss_3.createObject(null, {scene: ship.scene, position: Qt.point(28 * 2 * chapter_scale ,21 * 2 * chapter_scale)})
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
			}
		}
		
		State
		{
			id: state_after_video_opening_2
			SignalTransition
			{
				targetState: state_story_3
				signal: state_after_video_opening_2.propertiesAssigned
				guard:!screen_video.video.file
			}
		}	
		
		State
		{
			id: state_story_3
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_video_opening_3
				signal: boss_3_dead
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-7/third-cannon.ogg"
					function f ()
					{
						root.boss.alive = false
						root.boss = boss_4.createObject(null, {scene: ship.scene, position: Qt.point(28 * 2 * chapter_scale ,21 * 2 * chapter_scale)})
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
			}
		}
		
		State
		{
			id: state_after_video_opening_3
			SignalTransition
			{
				targetState: state_story_4
				signal: state_after_video_opening_3.propertiesAssigned
				guard:!screen_video.video.file
			}
		}
		
		State
		{
			id: state_story_4
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: root.boss_aliveChanged
				guard: !root.boss_alive
				onTriggered:
				{
					music.objectName = "chapter-2/fuite"
					messages.add("nectaire/normal", qsTr("end 1"))
					messages.add("lycop/normal", qsTr("end 2"))
				}
			}
		}
		
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_5; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_5
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_end
				signal: sortie
				onTriggered:
				{
					saved_game.add_science(1)
					saved_game.set("file", "game/chapter-7/chapter-7-flee-1.qml")
					saved_game.save()
				}
			}
		}
		
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	
	Q.Component.onCompleted:
	{	
		
		jobs.run(music, "preload", ["chapter-6/inside"])
		jobs.run(music, "preload", ["chapter-2/fuite"])
		jobs.run(music, "preload", ["chapter-2/boss"])
		jobs.run(scene_view, "preload", ["chapter-2/round-turret"])
		for(var i = 0; i < 4; ++i) jobs.run(scene_view, "preload", ["chapter-7/boss-generator/" + i])
		for(var i = 0; i < 15; ++i) jobs.run(scene_view, "preload", ["chapter-2/turret-opening/" + i])
		jobs.run(scene_view, "preload", ["chapter-2/barrier-wall"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-6/console/" + i])
		jobs.run(scene_view, "preload", ["explosion"])
		jobs.run(scene_view, "preload", ["equipments/laser-gun/bullet"])
		jobs.run(scene_view, "preload", ["chapter-6/boss/bullet"])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["chapter-5/star-bullet/" + i])
		jobs.run(scene_view, "preload", ["chapter-4/explosion-bullet"])
		for(var i = 0; i < 8; ++i) jobs.run(scene_view, "preload", ["chapter-4/boss/gigabullet/" + i])
		
		groups.init(scene)
		
		boss = boss_1.createObject(null, {scene: ship.scene, position: Qt.point(28 * 2 * chapter_scale ,21 * 2 * chapter_scale)})
		anim_turret = turret_spawner_factory.createObject(null, {parent: scene})
		//root.info_print = screen_info_factory.createObject(screen_hud)
		//root.finalize = function() {if(info_print) info_print.destroy()}
	}
}