import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-7/chapter-7-fuite.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal open_secret
	signal sortie
	signal on_sortie
	property var camera_position: Qt.point(56.5 * 4, 47.5 * 4)
	property var last_ship_position: Qt.point(0, 0)
	property var barriere_bas
	property real ti: 0
	property var info_print
	property real chapter_scale: 2
	property bool open_corridor_2: true
	property bool open_corridor_3: true
	property bool open_corridor_4: true
	property bool open_corridor_5: true
	property bool open_sortie: security_1 && security_2 && security_3 && security_4
	property bool security_1: true
	property bool security_2: true
	property bool security_3: true
	property bool security_4: true
	property bool can_open: true
	property bool travel_map_2: true
	property real fuzzy_comp: 0
	
	property int open_ind: 0
	
	
	
	Q.Component
	{
		id: heavy_laser_gun_factory
		HeavyLaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: harpoon_factory
		HarpoonLauncher
		{
			range_bullet: scene.seconds_to_frames(10)
			period: scene.seconds_to_frames(6)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: missile_launcher_factory
		MissileLauncher
		{
			range_bullet: scene.seconds_to_frames(2)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: secondary_shield_factory
		SecondaryShield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: random_spray_factory
		RandomSpray
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: false
			enemy: 1
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: false
			period: scene.seconds_to_frames(3)
			level: 2
		}
	}
	
	
	property var tab_turret_corridor_1:[
	[40,43.5 ,3, 1/2, false,2 ],
	[40,44.5 ,3, 1/2, false,2 ],
	[40,45.5 ,3, 1/2, false,2 ],
	[40,46.5 ,3, 1/2, false,2 ],
	[40,47.5 ,3, 1/2, false,2 ],
	[40,48.5 ,3, 1/2, false,2 ],
	[40,49.5 ,3, 1/2, false,2 ],
	[40,50.5 ,3, 1/2, false,2 ],
	[40,51.5 ,3, 1/2, false,2 ],
	[40,52.5 ,3, 1/2, false,2 ],
	[40,42.5 ,3, 1/2, false,2 ],
	[39,42 ,1, 1/2, false,1 ],
	[39,43 ,1, 1/2, false,1 ],
	[39,44 ,1, 1/2, false,1 ],
	[39,45 ,1, 1/2, false,1 ],
	[39,46 ,1, 1/2, false,1 ],
	[39,47 ,1, 1/2, false,1 ],
	[39,48 ,1, 1/2, false,1 ],
	[39,49 ,1, 1/2, false,1 ],
	[39,50 ,1, 1/2, false,1 ],
	[39,51 ,1, 1/2, false,1 ],
	[39,52 ,1, 1/2, false,1 ],
	[39,53 ,1, 1/2, false,1 ],
	[17,53 ,1, 1/2, true,1 ],
	[18,53 ,1, 1/2, true,1 ],
	[19,53 ,1, 1/2, true,1 ],
	[20,53 ,1, 1/2, true,1 ],
	[21,53 ,1, 1/2, true,1 ],
	[22,53 ,1, 1/2, true,1 ],
	[23,53 ,1, 1/2, true,1 ],
	[24,53 ,1, 1/2, true,1 ],
	[25,53 ,1, 1/2, true,1 ],
	[26,53 ,1, 1/2, true,1 ],
	[27,53 ,1, 1/2, true,1 ],
	[28,53 ,1, 1/2, true,1 ],
	[29,53 ,1, 1/2, true,1 ],
	[30,53 ,1, 1/2, true,1 ],
	[31,53 ,1, 1/2, true,1 ],
	[32,53 ,1, 1/2, true,1 ],
	[33,53 ,1, 1/2, true,1 ],
	[17,42 ,1, 1/2, true,1 ],
	[18,42 ,1, 1/2, true,1 ],
	[19,42 ,1, 1/2, true,1 ],
	[20,42 ,1, 1/2, true,1 ],
	[21,42 ,1, 1/2, true,1 ],
	[22,42 ,1, 1/2, true,1 ],
	[23,42 ,1, 1/2, true,1 ],
	[24,42 ,1, 1/2, true,1 ],
	[25,42 ,1, 1/2, true,1 ],
	[26,42 ,1, 1/2, true,1 ],
	[27,42 ,1, 1/2, true,1 ],
	[28,42 ,1, 1/2, true,1 ],
	[29,42 ,1, 1/2, true,1 ],
	[30,42 ,1, 1/2, true,1 ],
	[31,42 ,1, 1/2, true,1 ],
	[32,42 ,1, 1/2, true,1 ],
	[33,42 ,1, 1/2, true,1 ]
	]
	
	
	
	
	
	function is_not_2_open()
	{
		return !root.open_corridor_2
	}
	function is_not_3_open()
	{
		return !root.open_corridor_3
	}
	function is_not_4_open()
	{
		return !root.open_corridor_4
	}
	function is_not_5_open()
	{
		return !root.open_corridor_5
	}
	// x, y , weapon choice, angle, bind, invulnerable
	
	
	
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-6/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-6/"+mat+"/c"])
			}
		}
		return fill_tiles
	}
	
	function fill_2(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y, mat])
			}
		}
		return fill_tiles
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}
	
	/*Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("Temps restant: %1").arg(root.ti)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}*/
	
	scene: Scene
	{
		running: false
		
		Background
		{
			position.x: -1  
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Background
		{
			position.x: 57
			x:1
			y : 10
			z: altitudes.background_near
			scale: 5
			material: "chapter-7/starship/outdoor-masked"
		}
		
		
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		
		Tiles
		{
			id: corridor_1
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[25,41,'n','metal'],
				[25,40,'w',''],
				[29,40,'n',''],
				[29,41,'e','metal'],
				[47,41,'n','metal'],
				[47,40,'w',''],
				[51,40,'n',''],
				[51,41,'e','metal'],
				[59,41,'n','metal'],
				[59,54,'e','metal'],
				[51,54,'s','metal'],
				[51,55,'e',''],
				[47,55,'s',''],
				[47,54,'w','metal'],
				[29,54,'s','metal'],
				[29,55,'e',''],
				[25,55,'s',''],
				[25,54,'w','metal'],
				[16,54,'s','metal'],
				[16,41,'w','metal']
				]).concat(
				fill(17,42,58,53,'metal'),
				fill(26,41,28,41,'metal'),
				fill(48,41,50,41,'metal'),
				fill(26,54,28,54,'metal'),
				fill(48,54,50,54,'metal'),
				[[18,43,'chapter-6/metal/c2'],
				[22,43,'chapter-6/metal/c2'],
				[26,43,'chapter-6/metal/c2'],
				[30,43,'chapter-6/metal/c2'],
				[34,43,'chapter-6/metal/c2'],
				[38,43,'chapter-6/metal/c2'],
				[42,43,'chapter-6/metal/c2'],
				[46,43,'chapter-6/metal/c2'],
				[50,43,'chapter-6/metal/c2'],
				[54,43,'chapter-6/metal/c2'],
				[58,43,'chapter-6/metal/c2'],
				[18,46,'chapter-6/metal/c2'],
				[22,46,'chapter-6/metal/c2'],
				[26,46,'chapter-6/metal/c2'],
				[30,46,'chapter-6/metal/c2'],
				[34,46,'chapter-6/metal/c2'],
				[38,46,'chapter-6/metal/c2'],
				[42,46,'chapter-6/metal/c2'],
				[46,46,'chapter-6/metal/c2'],
				[50,46,'chapter-6/metal/c2'],
				[54,46,'chapter-6/metal/c2'],
				[18,49,'chapter-6/metal/c2'],
				[22,49,'chapter-6/metal/c2'],
				[26,49,'chapter-6/metal/c2'],
				[30,49,'chapter-6/metal/c2'],
				[34,49,'chapter-6/metal/c2'],
				[38,49,'chapter-6/metal/c2'],
				[42,49,'chapter-6/metal/c2'],
				[46,49,'chapter-6/metal/c2'],
				[50,49,'chapter-6/metal/c2'],
				[54,49,'chapter-6/metal/c2'],
				[18,52,'chapter-6/metal/c2'],
				[22,52,'chapter-6/metal/c2'],
				[26,52,'chapter-6/metal/c2'],
				[30,52,'chapter-6/metal/c2'],
				[34,52,'chapter-6/metal/c2'],
				[38,52,'chapter-6/metal/c2'],
				[42,52,'chapter-6/metal/c2'],
				[46,52,'chapter-6/metal/c2'],
				[50,52,'chapter-6/metal/c2'],
				[54,52,'chapter-6/metal/c2'],
				[58,52,'chapter-6/metal/c2']]
				)
		}
		
		Tiles
		{
			id: corridor_2
			z: altitudes.background_near
			mask: root.open_corridor_2 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[37,21,'n','metal'],
				[37,40,'e','metal'],
				[29,40,'s','metal'],
				[29,41,'e',''],
				[25,41,'s',''],
				[25,40,'w','metal'],
				[16,40,'s','metal'],
				[16,21,'w','metal']
				]).concat(
				fill(17,22,36,39,'metal'),
				fill(26,40,28,40,'metal'),
				[[18,24,'chapter-6/metal/c2'],
				[22,24,'chapter-6/metal/c2'],
				[26,24,'chapter-6/metal/c2'],
				[30,24,'chapter-6/metal/c2'],
				[34,24,'chapter-6/metal/c2'],
				[34,28,'chapter-6/metal/c2'],
				[34,32,'chapter-6/metal/c2'],
				[34,36,'chapter-6/metal/c2'],
				[18,28,'chapter-6/metal/c2'],
				[18,32,'chapter-6/metal/c2'],
				[18,36,'chapter-6/metal/c2'],
				[22,36,'chapter-6/metal/c2'],
				[26,36,'chapter-6/metal/c2'],
				[30,36,'chapter-6/metal/c2'],
				[34,36,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(15 * 2, 20 * 2), Qt.point(37.5 * 2, 20 * 2), Qt.point(37.5 * 2,40.5 * 2), Qt.point(15 * 2, 40.5 * 2)]
				group: root.open_corridor_2 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_3
			z: altitudes.background_near
			mask: root.open_corridor_3 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[59,21,'n','metal'],
				[59,40,'e','metal'],
				[51,40,'s','metal'],
				[51,41,'e',''],
				[47,41,'s',''],
				[47,40,'w','metal'],
				[38,40,'s','metal'],
				[38,21,'w','metal']
				]).concat(
				fill(39,22,58,39,'metal'),
				fill(48,40,50,40,'metal'),
				[[41,31,'chapter-6/metal/c2'],
				[45,31,'chapter-6/metal/c2'],
				[53,31,'chapter-6/metal/c2'],
				[57,31,'chapter-6/metal/c2'],
				[49,23,'chapter-6/metal/c2'],
				[49,27,'chapter-6/metal/c2'],
				[49,31,'chapter-6/metal/c2'],
				[49,35,'chapter-6/metal/c2'],
				[49,39,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(37.5 * 2, 20 * 2), Qt.point(60 * 2, 20 * 2), Qt.point(60 * 2,40.5 * 2), Qt.point(37.5 * 2, 40.5 * 2)]
				group: root.open_corridor_3 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_4
			z: altitudes.background_near
			mask: root.open_corridor_4 ? "white" : "transparent"
			scale: root.chapter_scale
			tiles: create_tiles([
				[25,55,'n','metal'],
				[25,54,'w',''],
				[29,54,'n',''],
				[29,55,'e','metal'],
				[37,55,'n','metal'],
				[37,64,'e','metal'],
				[16,64,'s','metal'],
				[16,55,'w','metal']]).concat(
				fill(17,56,36,63,'metal'),
				fill(26,55,28,55,'metal'),
				[[18,56,'chapter-6/metal/c2'],
				[22,56,'chapter-6/metal/c2'],
				[26,56,'chapter-6/metal/c2'],
				[30,56,'chapter-6/metal/c2'],
				[34,56,'chapter-6/metal/c2'],
				[18,59,'chapter-6/metal/c2'],
				[22,59,'chapter-6/metal/c2'],
				[26,59,'chapter-6/metal/c2'],
				[30,59,'chapter-6/metal/c2'],
				[34,59,'chapter-6/metal/c2'],
				[18,62,'chapter-6/metal/c2'],
				[22,62,'chapter-6/metal/c2'],
				[26,62,'chapter-6/metal/c2'],
				[30,62,'chapter-6/metal/c2'],
				[34,62,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				vertexes: [Qt.point(15 * 2, 54.5 * 2), Qt.point(37.5 * 2, 54.5 * 2), Qt.point(37.5 * 2, 65 * 2), Qt.point(15 * 2, 65 * 2)]
				group: root.open_corridor_4 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Tiles
		{
			id: corridor_5
			z: altitudes.background_near
			mask: root.open_corridor_5 ? "white" : "transparent"
			scale: root.chapter_scale
			property var collider_wall: []
			tiles: create_tiles([
				[47,55,'n','metal'],
				[47,54,'w',''],
				[51,54,'n',''],
				[51,55,'e','metal'],
				[59,55,'n','metal'],
				[59,64,'e','metal'],
				[38,64,'s','metal'],
				[38,55,'w','metal']
				]).concat(
				fill(39,56,58,63,'metal'),
				fill(48,55,50,55,'metal'),
				[[40,56,'chapter-6/metal/c2'],
				[44,56,'chapter-6/metal/c2'],
				[48,56,'chapter-6/metal/c2'],
				[52,56,'chapter-6/metal/c2'],
				[56,56,'chapter-6/metal/c2'],
				[40,59,'chapter-6/metal/c2'],
				[44,59,'chapter-6/metal/c2'],
				[48,59,'chapter-6/metal/c2'],
				[52,59,'chapter-6/metal/c2'],
				[56,59,'chapter-6/metal/c2'],
				[40,62,'chapter-6/metal/c2'],
				[44,62,'chapter-6/metal/c2'],
				[48,62,'chapter-6/metal/c2'],
				[52,62,'chapter-6/metal/c2'],
				[56,62,'chapter-6/metal/c2']]
				)
				
			PolygonCollider
			{
				id: collider_1
				vertexes: [Qt.point(37.5 * 2, 54.5 * 2), Qt.point(60 * 2, 54.5 * 2), Qt.point(60 * 2,65 * 2), Qt.point(37.5 * 2, 65 * 2)]
				group: root.open_corridor_5 ? 0 : groups.wall
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_2
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(30 *  chapter_scale*2, 42 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_2 = true
					root.can_open = false
					indice = 24
				}
			}
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + console_2.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.can_open ? root.open_corridor_2 ?  0 : groups.sensor : 0
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: console_3
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(46 *  chapter_scale*2, 42 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_3 = true
					root.can_open = false
					indice = 24
				}
			}
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + console_3.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.can_open ? root.open_corridor_3 ?  0 : groups.sensor : 0
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: console_4
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(30 *  chapter_scale*2, 53 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_4 = true
					root.can_open = false
					indice = 24
				}
			}
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + console_4.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.can_open ? root.open_corridor_4 ?  0 : groups.sensor : 0
				sensor: true
			}
		}
		
		Sensor
		{
			id: console_5
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(46 *  chapter_scale*2, 53 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.open_corridor_5 = true
					root.can_open = false
					indice = 24
				}
			}
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + console_5.indice
				z: altitudes.boss
				scale: 2
			}
			
			CircleCollider
			{
				group: root.can_open ? root.open_corridor_5 ?  0 : groups.sensor : 0
				sensor: true
			}
		}
		
		
		Sensor
		{
			id: secu_1
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(36 *  chapter_scale * 2, 22 *  chapter_scale * 2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.security_1 = true
					indice = 24
				}
			}
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + secu_1.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_2 ? "white" : "transparent" 
			}
			
			CircleCollider
			{
				group: root.security_1 ?  0 : groups.sensor
				sensor: true
			}
		}
		Sensor
		{
			id: secu_2
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(48.5 *  chapter_scale*2, 22 *  chapter_scale*2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.security_2 = true
					indice = 24
				}
			}
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + secu_2.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_3 ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: root.security_2 ?  0 : groups.sensor
				sensor: true
			}
		}
		Sensor
		{
			id: secu_3
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(36 *  chapter_scale*2, 59 *  chapter_scale * 2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.security_3 = true
					indice = 24
				}
			}
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + secu_3.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_4 ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: root.security_3 ?  0 : groups.sensor
				sensor: true
			}
		}
		Sensor
		{
			id: secu_4
			duration_in: root.scene.seconds_to_frames(1.5)
			duration_out: 30
			type:  Body.STATIC
			position: Qt.point(39 *  chapter_scale * 2, 59 *  chapter_scale * 2)
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.security_4 = true
					indice = 24
				}
			}
			property int indice: 24
			
			Image
			{
				material: "chapter-6/console/" + secu_4.indice
				z: altitudes.boss
				scale: 2
				mask: root.open_corridor_5 ? "white" : "transparent"
			}
			
			CircleCollider
			{
				group: root.security_4 ?  0 : groups.sensor
				sensor: true
			}
		}
		
		Ship
		{
			id: ship	
			position.x: 56.5 * 4
			position.y: 47.5 * 4
			angle: -Math.PI/2
			Q.Component.onDestruction: 
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
			
		}
			
		Body
		{
			id: all_wall
			position.y: 0
			position.x: 0
			scale: chapter_scale*2
			
			type: Body.STATIC
			
			function point(x,y)
			{
				return Qt.point(x - 0.25 , y + 0.25)
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(7,14), all_wall.point(59 + 2*0.25,14), all_wall.point(59 + 2 * 0.25, 21 - 2 *0.25), all_wall.point(7,21 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(59 + 2 * 0.25,14), all_wall.point(67,14), all_wall.point(67,64), all_wall.point(59+ 2 *0.25,64)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(16,64), all_wall.point(67,64), all_wall.point(67,70), all_wall.point(16, 70)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(7,21 - 2 * 0.25), all_wall.point(16,21 - 2 * 0.25), all_wall.point(16 ,70), all_wall.point(7,70)]
				group: groups.wall
			}
			
			
			PolygonCollider
			{
				vertexes: [all_wall.point(37 + 2 * 0.25, 21 - 2 * 0.25), all_wall.point(38,21 - 2 * 0.25), all_wall.point(38,40), all_wall.point(37 + 2 * 0.25,40)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(29 + 2 * 0.25, 40), all_wall.point(47,40), all_wall.point(47,41 - 2 *0.25), all_wall.point(29 + 2 * 0.25,41 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(16, 40), all_wall.point(25,40), all_wall.point(25,41 - 2 *0.25), all_wall.point(16,41 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(51 + 2 * 0.25, 40), all_wall.point(59 + 2 * 0.25,40), all_wall.point(59 + 2 * 0.25 ,41 - 2 *0.25), all_wall.point(51 + 2 * 0.25,41 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(37 + 2 * 0.25, 55 - 2 * 0.25), all_wall.point(38,55 - 2 * 0.25), all_wall.point(38,64), all_wall.point(37 + 2 * 0.25, 64)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(29 + 2 * 0.25, 54), all_wall.point(47,54), all_wall.point(47,55 - 2 *0.25), all_wall.point(29 + 2 * 0.25,55 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(16, 54), all_wall.point(25,54), all_wall.point(25,55 - 2 *0.25), all_wall.point(16,55 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(51 + 2 * 0.25, 54), all_wall.point(59 + 2 * 0.25, 54), all_wall.point(59 + 2 * 0.25 ,55 - 2 *0.25), all_wall.point(51 + 2 * 0.25,55 - 2 * 0.25)]
				group: groups.wall
			}
		}
		
		Sensor
		{
			id: enter
			type: Body.STATIC
			property real x: 56.5
			property real y: 47.5
			scale: 4
			position: enter.coords(x,y)
			property int indice: 24
			property bool changed: true
			property real ti_local: root.ti
			property bool open : false
			
			onTi_localChanged:
			{
				if(root.open_sortie)
				{
					if(changed)
					{
						if(indice < 24)
						{	
							indice++
						}
						else
						{
							open = true
						}
						changed = false
					}
					else
					{
						changed = true
					}
				}
			}
			
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Image
			{
				material: "chapter-6/exit/" + enter.indice
				z: altitudes.boss
			}
			/*onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie()
				}
			}*/
			PolygonCollider
			{
				vertexes: [
				Qt.point(- 1 , - 1),
				Qt.point(1 , - 1),
				Qt.point(1 , 1),
				Qt.point(- 1 , 1)
				]
				group:  enter.open ? groups.sensor : 0
				sensor : true
			}
		}
		Sensor
		{
			id: exit
			type: Body.STATIC
			property real x: 0
			property real y: 0
			scale: 1
			position: exit.coords(x,y)
			property int indice: 24
			property bool changed: true
			property bool open : false
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Image
			{
				position: exit.coords(15,47.5)
				material: "chapter-6/starship/exit"
				scale: 6
				z: altitudes.boss
			}
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie()
				}
			}
			PolygonCollider
			{
				vertexes: [
				exit.coords(15.75,46),
				exit.coords(17,46),
				exit.coords(17,49),
				exit.coords(15.75,49)
				]
				group:  groups.sensor
				sensor : true
			}
		}

		
		
		
	}
	
	Q.Component
	{
		id: armagedon_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: Qt.rgba(1,0,0,1)
			scale: 0.01
			property real z: 0.01
			readonly property real damages: 10000
			property real ti_local: root.ti
			
			onTi_localChanged:
			{
				if(scale > 0)
				{
					angle += 0.01
					scale += 11 / scene.seconds_to_frames(1)
				
				}
			}
			
			
			/*Q.Component.onCompleted: 
			{
				boom_s.play()
			}*/
			onScaleChanged:
			{
				collider.update_transform()
			}
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			property Sound boom_s : Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 5000
			}
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				sensor: true
			}
		}
	}
	
	Q.Component
	{
		id: turret_factory
		Turret
		{
		
		}
	}
	
	Q.Component
	{
		id: shield_barriere_factory
		Vehicle
		{
			id: shield_1
			type: Body.STATIC
			property real x:52
			property real y:13
			property int nb_repe: 18
			property var tab_shield:[] // x,y
			property var tab_shield_alive:[] // x,y
			property var tab_collider: []
			property real ti_local: root.ti
			property color mask:  Qt.rgba(0, 0.4, 1, shield_1.shield ? Math.max(0.2, shield_1.shield / shield_1.max_shield) : 0)
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			
			onTi_localChanged:
			{
				var alive = false
				for(var i = 0; i < tab_collider.length; i++)
				{
					if(!tab_shield_alive[i])
					{
						tab_collider[i].group = 0
					}
					else
					{
						alive = true
					}
				}
				if(tab_collider.length > 0 && !alive)
				{
					destroy()
				}
			}
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			
			
			Q.Component.onCompleted:
			{
				shield_1.shield = max_shield
			}
			
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.375)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: shield_1.mask
					}
				}
			}
			Repeater
			{
				count: shield_1.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (shield_1.x - 0.625)*2*chapter_scale
						position.y: (shield_1.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: shield_1.mask
					}
				}
			}
			
			Repeater
			{
				count: shield_1.tab_shield.length
				Q.Component
				{
					EquipmentSlot
					{
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						equipment: Shield
						{
							level: 2	
							material_image: "chapter-2/shield-generator"							
							Q.Component.onCompleted:
							{
								shield_1.tab_shield_alive.push(true)
							}
							
							Q.Component.onDestruction: 
							{
								shield_1.tab_shield_alive[index] = false
							}
						}
					}
				}
				Q.Component
				{
					CircleCollider
					{
						id: collider
						position.x: shield_1.tab_shield[index][0] * 2 * root.chapter_scale
						position.y: shield_1.tab_shield[index][1] * 2 * root.chapter_scale
						group: shield_1.tab_shield_alive[index] ? groups.enemy_hull_naked : 0
						radius: 0.5
						
						Q.Component.onCompleted:
						{
							shield_1.tab_collider.push(collider)
						}
						
					}
				}
			}
			
			PolygonCollider
			{
				vertexes: [
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4),
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25 - ( shield_1.nb_repe + 1 ) / 4), 
				shield_1.coords(shield_1.x - 0.25, shield_1.y + 1.25), 
				shield_1.coords(shield_1.x - 0.75, shield_1.y + 1.25)
				]
				group: shield_1.shield ? groups.enemy_shield : 0
			}
		}
	}
	
	
	
	
	
	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState:  state_test_1 // state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState:   state_dialogue_0  //state_test_1
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					music.objectName = "chapter-2/fuite"
					camera_position = undefined
					for(var i = 0; i< tab_turret_corridor_1.length; i++)
					{
						turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_1[i][0] * 2 * root.chapter_scale, tab_turret_corridor_1[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_1[i][2], angle: tab_turret_corridor_1[i][3] * Math.PI, activate: true, invulnerable : tab_turret_corridor_1[i][4] ,cheat : tab_turret_corridor_1[i][5]})
					}
					armagedon_factory.createObject(null, {scene: ship.scene, position: Qt.point(75 * 2 * root.chapter_scale, 47.5 * 2 * root.chapter_scale), scale: 30})
					var shield = shield_barriere_factory.createObject(null, {scene: ship.scene, x: 38, y: 53, nb_repe: 54 ,tab_shield:[[43,42],[43,43],[43,44],[43,45],[43,46],[43,47],[43,48],[43,49],[43,50],[43,51],[43,52],[43,53]]})
					messages.add("lycop/normal", qsTr("begin 1"))
					messages.add("nectaire/normal", qsTr("begin 2"))
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_2; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_end
				signal: sortie
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-7/chapter-7-flee.ogg"
					function f ()
					{
						camera_position = Qt.point(-10 * 4, 48 * 4)
						ship.position = Qt.point(-10 * 4, 48 * 4)
						ship.angle = - Math.PI / 2
						saved_game.add_science(1)
						saved_game.set("file", "game/chapter-7/chapter-7-true-last-boss.qml")
						saved_game.save()
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
				
			}
		}
		
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard:!screen_video.video.file
				onTriggered:
				{
					scene_loader.load(settings.get_last_saved_game())
				}
			}
		}
	}
	
	Q.Component.onCompleted:
	{
		var a2 = ["c", "e", "n", "ne", "nei", "nw", "nwi", "s", "se", "sei", "sw", "swi", "w"]; for(var i2 in a2) jobs.run(scene_view, "preload", ["chapter-2/metal/" + a2[i2]])
		jobs.run(scene_view, "preload", ["chapter-7/starship/outdoor-masked"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-6/exit/" + i])
		jobs.run(scene_view, "preload", ["chapter-2/shield-generator"])
		jobs.run(music, "preload", ["chapter-2/fuite"])
		jobs.run(scene_view, "preload", ["chapter-2/barrier-wall"])
		jobs.run(scene_view, "preload", ["chapter-2/square-turret"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		jobs.run(scene_view, "preload", ["explosion"])
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["chapter-6/starship/exit"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-6/console/" + i])
		groups.init(scene)
		//root.info_print = screen_info_factory.createObject(screen_hud)
		//root.finalize = function() {if(info_print) info_print.destroy()}
	}
}