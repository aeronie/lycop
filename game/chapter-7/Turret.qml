import QtQuick 2.0
import aw.game 0.0
import "qrc:///game"

Vehicle
{
	id: turret
	property int activate: 0
	property bool shoot: true
	property int weapon_choice: 0
	type: Body.KINEMATIC
	max_speed: 0
	icon: 3
	icon_color: activate ? "red" : "transparent"
	property bool invulnerable: false
	property int cheat: 0
	property bool alive: true
	onAliveChanged: if(!alive) destroy()
	property real ti_local: 0
	property real deb: 0
	property real shoot_delay: 0.2
	onTi_localChanged:
	{
		if(!shoot)
		{
			if(deb - ti_local > shoot_delay)
			{
				shoot = true
			}
		}
	}
	
	
	loot_factory: Repeater
	{
		Component {Loot {}}
		Component {Explosion {scale: 3}}
	}
	
	Image
	{
		material: "equipments/shield"
		mask: Qt.rgba(0.58, 0, 0.82, turret.invulnerable ? 1 : 0)
		scale: 1.1
		z: altitudes.shield
	}
	CircleCollider
	{
		group: turret.invulnerable ? groups.enemy_invincible : 0
		radius: 1.1
	}
	
	
	Image
	{
		material: "chapter-2/square-turret"
		mask: Qt.rgba(1, 1, 1, turret.activate)
	}
	CircleCollider
	{
		id: collider
		group: turret.activate ? (!turret.invulnerable ? groups.enemy_hull_naked : 0) : 0
	}

	property list<Component> weapons: [
	Component
	{
		RandomSpray
		{
			level: 2
			image.mask.a: turret.activate
			shooting: turret.shoot ? behaviour.shooting : false
			enemy: 1
		}
	},
	Component
	{
		HeavyLaserGun
		{
			level: 2
			image.mask.a: turret.activate
			shooting: turret.shoot ? behaviour.shooting : false
		}
	},
	Component
	{
		PlasmaSpray
		{
			level: 2
			image.mask.a: turret.activate
			shooting: turret.shoot ? behaviour.shooting : false
		}
	},
	Component
	{
		Quipoutre
		{
			image.mask.a: turret.activate
			shooting: turret.shoot ? behaviour.shooting : false
			period: scene.seconds_to_frames([5 , 3.5 , 2] [turret.cheat])
			range_bullet: scene.seconds_to_frames(20)
		}
	}]
	EquipmentSlot
	{
		scale: 0.7
		Component.onCompleted: equipment = turret.weapons[turret.weapon_choice].createObject(null, {slot: this})
		onEquipmentChanged: if(!equipment) turret.deleteLater()
	}
	Behaviour_attack_distance
	{
		id: behaviour
		target_groups: turret.activate ? groups.enemy_targets : 0
		max_distance: 40
	}
}
