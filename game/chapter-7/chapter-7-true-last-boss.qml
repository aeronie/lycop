import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-7/chapter-7-true-last-boss.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	readonly property bool secret_tomate: saved_game.self.get("secret_tomate", false)
	property alias ship: ship
	signal boss_destroyed
	signal dialogue
	signal ship_ok
	signal sortie
	signal assimilated
	property var camera_position: Qt.point(0, 0)
	property var last_ship_position: Qt.point(0, 0)
	property var info_print
	property bool new_wave: false
	property bool the_one: false
	property bool travel_map_2: true
	property int concentration: 0
	property real ti: 7
	property var boss
	property bool level_begin: false
	property bool boss_alive: true
	property int nb_drone: 0
	property int door_open: 0
	property real time_propugnator: 0
	property real time_before_boss: 10 - (time_propugnator - root.ti)
	property var tp_behaviour
	property var wave_spawner
	property var black_hole
	property bool black_hole_alive: false
	property bool vener: false
	property bool blink_end : false
	property bool asteroid_phase : true
	property bool armagedon_end: false
	property bool incoming_end: false
	property bool tp_ship_player: false
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false

		Animation
		{
			id: timer
			time: 7
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Ship
		{
			id: ship
			position.x: 40
			angle: -Math.PI/2
			Q.Component.onDestruction:
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}

	}

	Q.Component
	{
		id: space_door_factory
		Body
		{
			id: space_door
			scale: 15
			type: Body.STATIC
			property int indice: 0
			property color mask: "white"
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real z: 0
			Image
			{
				id: image
				material: "chapter-3/door/" + indice
				z: space_door.z
				mask: space_door.mask
			}
		}
	}

	Q.Component
	{
		id: asteroide_factory
		Vehicle
		{
			id: asteroid
			property var barrier
			property bool alive: root.asteroid_phase && root.boss
			onAliveChanged: if(!alive) destroy()
			property int num_asteroid: Math.floor(Math.random() * 3)
			property bool start: false
			max_speed: root.vener ? 25 : 15 
			max_angular_speed: start ? (root.vener ? 10 : 2) : 0
			scale: 2
			property real ti_local: root.ti
			onTi_localChanged:
			{
				var x = asteroid.position.x - ship.position.x
				var y = asteroid.position.y - ship.position.y
				if(Math.sqrt(x*x+y*y) < 30)
				{
					start = false
				}
				else
				{
					start = true
				}
			}
			
			loot_factory: Repeater
			{
				Q.Component {Explosion {scale: 4}}
			}

			
			function begin(other, point)
			{
				var v = other.velocity
				var p = point
				var vect_radian = Qt.point(point.x - asteroid.position.x, point.y - asteroid.position.y)
				var tempx = vect_radian.x
				var tempy = vect_radian.y
				vect_radian.x = tempx / Math.sqrt(tempx * tempx + tempy * tempy)
				vect_radian.y = tempy / Math.sqrt(tempx * tempx + tempy * tempy)
				var point_dep = Qt.point(p.x - v.x, p.y - v.y)
				var vect_dep = Qt.point(point_dep.x - p.x, point_dep.y - p.y)
				
				// AH = <AC.v>/norm(v) * v
				var prod_proj = (vect_dep.x * vect_radian.x + vect_dep.y * vect_radian.y) / Math.sqrt(vect_radian.x * vect_radian.x + vect_radian.y * vect_radian.y)
				
				var vect_proj = Qt.point(prod_proj * vect_radian.x, prod_proj * vect_radian.y)
				
				var point_proj = Qt.point(p.x + vect_proj.x, p.y + vect_proj.y)
				var vect_ortho = Qt.point(point_proj.x - point_dep.x, point_proj.y - point_dep.y)
				
				var point_final = Qt.point(vect_ortho.x + point_proj.x, vect_ortho.y + point_proj.y)
				var vect_velocity =  Qt.point(point_final.x - p.x, point_final.y - p.y)
				
				other.velocity = Qt.point(vect_velocity.x, vect_velocity.y)
				var a = Math.atan2(vect_velocity.y, vect_velocity.x) + Math.PI / 2
				
				var e = scene.time_changed
				function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
				e.connect(f)
				for(var i in other.children)
				{
					var child = other.children[i]
					if(child.group) child.group ^= 1
					if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
				}
			}

			Image
			{
				material: "asteroid/" + num_asteroid % 3
				z: altitudes.boss + 0.0000000001
				angle: Math.random() * 2 * Math.PI
			}
			CircleCollider
			{
				group: groups.reflector_only_player
			}

			Behaviour_attack_missile
			{
				target_groups: groups.enemy_targets
				range: scene.seconds_to_frames(15)
			}
			
			EquipmentSlot
			{
				id: slot_1
				equipment: Equipment
				{
					id: health_boss
					health: max_health
					max_health: 60000
				}
			}
		}
	}
	
	Q.Component
	{
		id: concentration_factory
		Body
		{
			id: concentration
			scale: 16
			property bool alive: indice < 36
			property int indice_0: 0
			property int indice: (Math.floor(scene.time/2) - indice_0)
			onAliveChanged: if(!alive) destroy()
			property real z: altitudes.boss - 0.0000001
			property color mask: "white"
			Sound
			{
				id: sound_shooting
				objectName: "chapter-3/power_charging"
				scale: 1000
			}

			Q.Component.onCompleted:
			{
				indice_0 = Math.floor(root.scene.time/2)
				sound_shooting.play()
				root.concentration++
			}
			
			Q.Component.onDestruction:
			{
				root.concentration-- 
			}
			
			Image
			{
				id: image
				material: indice > 35 ? "chapter-3/concentration/35" : "chapter-3/concentration/" + indice
				z: concentration.z
				mask: concentration.mask
			}
		}
	}
	
	Q.Component
	{
		id: canon_star_bullet_factory
		Vehicle
		{
			id: canon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool shoot: root.boss ? root.boss.star_bullet_shoot : false
			property int indice: 0
			angle: root.boss ? root.boss.angle + Math.PI / 4 : 0
			position: root.boss ? Qt.point(root.boss.position.x,root.boss.position.y) : Qt.point(0,0)
			EquipmentSlot
			{
				id: eq_slot
				angle: canon.indice * Math.PI/2
				equipment: Weapon
				{
					id: weapon
					health: max_health
					max_health: 75000
					shooting: canon.shoot
					period: scene.seconds_to_frames(root.vener ? 5 : 10)
					bullet_factory: Vehicle
					{
						id: drone
						property bool alive: true
						function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
						onAliveChanged: if(!alive) destroy()
						property real damages: 100000
						property real ti_local: root.ti / 1.2
						max_angular_speed: 0
						scale: 1.5
						max_speed: 12
						property bool start: false
						property real ti_deb: 0
						property int ind_star: 0
						onTi_localChanged:
						{
							ind_star = (ind_star + 1)% 10
							if(ti_deb - root.ti < 0.7)
							{
								scale += 1.5 / scene.seconds_to_frames(0.7)
							}
							else
							{
								if(!start)
								{
									start = true
								}
							}
						}

						Sound
						{
							id: sound
							objectName: "chapter-4/boss_canon"
							scale: 4000
						}

						function is_started()
						{
							return drone.start
						}
						Image
						{
							material: "chapter-5/star-bullet/" + drone.ind_star
							angle: drone.ti_local
						}

						Q.Component.onCompleted:
						{
							angle = weapon.slot.angle_to_scene(0)
							ti_deb = root.ti
							position = weapon.slot.point_to_scene(Qt.point(0, root.boss ? -0.76 * root.boss.scale : 0 ))
							sound.play()
						}

						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: 1 / 2 * Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: 3 / 2 * Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						Behaviour_attack_missile
						{
							target_groups: [groups.enemy_checkpoint]
							range: scene.seconds_to_frames(15)
						}
						CircleCollider
						{
							group: groups.enemy_bullet
							radius: 0.76
						}
					}
				}
			}
		}
	}
	Q.Component
	{
		id: laser_invisible
		Weapon
		{
			property int level: 2
			property int bullet_group: groups.enemy_laser
			period: scene.seconds_to_frames([0, 1 / 3, 1 / 10] [level])
			health: max_health
			max_health: 150 
			range_bullet: scene.seconds_to_frames(2)
			readonly property Sound sound_shooting: Sound
			{
				objectName: "equipments/laser-gun/shooting"
				scale: slot.sound_scale
			}
			bullet_factory: Bullet
			{
				scale: 0.3
				range: weapon.range_bullet
				damages: 100
				Image
				{
					material: "equipments/laser-gun/bullet"
					z: altitudes.bullet
				}
				PolygonCollider
				{
					vertexes: [Qt.point(-0.125, -1), Qt.point(-0.125, 1), Qt.point(0.125, 1), Qt.point(0.125, -1)]
					group: weapon.bullet_group
					sensor: true
				}
				Q.Component.onCompleted:
				{
					position = weapon.slot.point_to_scene(Qt.point(0, -0.5))
					angle = weapon.slot.angle_to_scene(0)
					weapon.sound_shooting.play()
					weapon.set_bullet_velocity(this, 20)
				}
			}
		}
	}
	Q.Component
	{
		id: coil_gun_invisible
		Weapon
		{
			shooting: false
			property int bullet_group: groups.enemy_bullet
			range_bullet: scene.seconds_to_frames(4)
			period: scene.seconds_to_frames(root.vener ? 1 / 15 : 1 / 10)
			health: max_health
			max_health: 200
			readonly property Sound sound_shooting: Sound
			{
				objectName: "equipments/coil-gun/shooting"
				scale: slot.sound_scale
			}
			bullet_factory: Bullet
			{
				scale: 0.15
				range: weapon.range_bullet
				damages: 100
				Image
				{
					material: "equipments/coil-gun/bullet"
					z: altitudes.bullet
				}
				CircleCollider
				{
					group: weapon.bullet_group
					sensor: true
				}
				Q.Component.onCompleted:
				{
					position = weapon.slot.point_to_scene(Qt.point(0, 0))
					angle = weapon.slot.angle_to_scene(0)
					weapon.sound_shooting.play()
					weapon.set_bullet_velocity(this, 20)
				}
			}
		}
	}
	Q.Component
	{
		id: coil_gun_transport
		Vehicle
		{
			id: canon
			scale: boss ? root.boss.scale : 5
			property int num_canon: 18
			property bool shoot: false
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real rayon: 0.714
			Repeater
			{
				count: canon.num_canon
				Q.Component
				{
					EquipmentSlot
					{
						position.x:  canon.rayon * Math.sin( 2 * index * Math.PI / num_canon + Math.PI / num_canon)
						position.y: -canon.rayon * Math.cos( 2 * index * Math.PI / num_canon + Math.PI / num_canon)
						angle: 2 * index * Math.PI / num_canon + Math.PI / num_canon
						scale: 1/canon.scale
						Q.Component.onCompleted:
						{
							equipment = coil_gun_invisible.createObject(null, {slot: this})
							equipment.shooting = Qt.binding(function(){return canon.shoot})
						}
					}
				}
			}
		}
	}

	
	Q.Component
	{
		id: kamehameha_factory
		Body
		{
			id: spray
			property int nb_repete: 120
			readonly property real damages: 10000 * scene.time_step
			scale: 1
			property bool shoot: root.boss ? root.boss.laser_shoot : false
			Image
			{
				id: beam0
				position.y: -scale
				position.x: 0
				mask: spray.shoot ? "white" : "transparent"
				z: altitudes.bullet - 0.000001
				material: "chapter-3/freezing-beam/"+(Math.floor(scene.time))%29+"/0"
			}
			Image
			{
				id: beam1
				position.y: -3*scale
				position.x: 0
				mask: spray.shoot ? "white" : "transparent"
				z: altitudes.bullet - 0.000001
				material: "chapter-3/freezing-beam/"+(Math.floor(scene.time))%29+"/1"
			}
			Repeater
			{
				count: nb_repete
				Q.Component
				{
					Image
					{
						position.y: -(5+(index*2))*scale
						position.x: 0
						mask: spray.shoot ? "white" : "transparent"
						z: altitudes.bullet - 0.000001
						material: "chapter-3/freezing-beam/"+(Math.floor(scene.time))%29+"/2"
					}
				}
			}
			Image
			{
				id: beam3
				position.y: -(5+(nb_repete*2))*scale
				position.x: 0
				mask: spray.shoot ? "white" : "transparent"
				z: altitudes.bullet - 0.000001
				material: "chapter-3/freezing-beam/"+(Math.floor(scene.time))%29+"/3"
			}
			Image
			{
				id: beam4
				position.y: -(7+(nb_repete*2))*scale
				position.x: 0
				mask: spray.shoot ? "white" : "transparent"
				z: altitudes.bullet - 0.000001
				material: "chapter-3/freezing-beam/"+(Math.floor(scene.time))%29+"/4"
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-0.45,0),Qt.point(-0.45, - (7.5 + 2 * nb_repete)),Qt.point(0.45,-(7.5 + 2 * nb_repete)),Qt.point(0.45,0)]
				group: spray.shoot ? groups.enemy_dot : 0
				sensor: true
			}
		}
	}
	
	Q.Component
	{
		id: black_hole_behaviour_factory
		Animation
		{
			id: timer

			time: 7
			property var black_hole_object
			property real scale : 0.1
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(black_hole_object && ship && root.boss_alive)
				{
					var vect_x = black_hole_object.x - ship.position.x
					var vect_y = black_hole_object.y - ship.position.y
					var dist = vect_x * vect_x + vect_y * vect_y
					var vitesse = dist > 1e-4 ? (timer.scale/(root.vener ? 1.5 : 2)) / dist : 1e8
					ship.position.x += vect_x * vitesse
					ship.position.y += vect_y * vitesse
				}
			}
		}
	}

	Q.Component
	{
		id: black_hole_factory
		Bullet
		{
			id: hole
			function pos()
			{
				return Qt.point(hole.position.x, hole.position.y)
			}
			property var black_hole_behaviour
			scale: 0.1
			property int time: 14
			property real scale_obj: 12
			property int sens: 1
			range: scene.seconds_to_frames(time)
			damages: 100000
			angular_velocity: -0.5
			property bool boss_alive: root.boss
			onBoss_aliveChanged:
			{
				if(! boss_alive)
				{
					range = scene.seconds_to_frames(1)
				}
			}
			
			readonly property Sound sound_boom: Sound
			{
				objectName: "chapter-5/black_hole"
				scale: 50
			}
			onRangeChanged:
			{
				if(scale > 0)
				{
					if(range > scene.seconds_to_frames(time - 1))
					{
						scale += scale_obj / scene.seconds_to_frames(1)
					}
					
					if(range < scene.seconds_to_frames(1))
					{
						scale -= scale_obj / (scene.seconds_to_frames(1)+1)
					}
				}
			}
			onScaleChanged:
			{
				collider.update_transform()
			}
			Image
			{
				material: "chapter-5/black-hole"
				scale: 3
				z: altitudes.background_near
			}
			CircleCollider
			{
				id: collider
				group: root.boss_alive ? groups.enemy_dot : 0
				sensor: true
			}

			Q.Component.onCompleted:
			{
				black_hole_behaviour = black_hole_behaviour_factory.createObject(null, {parent: scene})
				black_hole_behaviour.black_hole_object = Qt.binding(pos)
				black_hole_behaviour.scale = Qt.binding(function(){return hole.scale})
				sound_boom.play()
				root.black_hole_alive = true
			}

			Q.Component.onDestruction:
			{
				root.black_hole_alive = false
				black_hole_behaviour.alive = false
			}
		}
	}
	Q.Component
	{
		id: wave_assault_factory
		Animation
		{
			id: timer
			property var tab_pos: [
					[-30,0],
					[-15,0],
					[15,0],
					[30,0]
					]

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(root.new_wave)
				{
					for(var i = 0 ; i < tab_pos.length ; ++i)
					{
						root.door_open++
						if(root.boss && ship)
						{
							var tp = tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(root.boss.position.x + tab_pos[i][0]*Math.cos(root.boss.angle) - tab_pos[i][1]*Math.sin(root.boss.angle), root.boss.position.y + tab_pos[i][1]*Math.cos(root.boss.angle) + tab_pos[i][0]*Math.sin(root.boss.angle)), position_central: Qt.point(ship.position.x,ship.position.y)})
							tp.position_central = Qt.binding(function(){return ship ? Qt.point(ship.position.x, ship.position.y) :  Qt.point(0,0)})
						}
					}
					root.new_wave = false
				}
			}
		}
	}
	Q.Component
	{
		id: tp_enemy_factory
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property real value: 0
			property real enemy_count: 4
			property var position_enemy
			property var position_central
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property real last_spawn: 7
			property real time_spawn: 1.5
			property var tab_drone:[asteroide_factory]

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: 8 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						if(new_enemy)
						{
							var i = random_index(tab_drone.length)
							enemy = tab_drone[i].createObject(null, {scene: scene, position: Qt.point(door.position.x, door.position.y), angle: Math.atan2(position_central.y - door.position.y, position_central.x - door.position.x )  + Math.PI / 2})
							new_enemy = false
							last_spawn = time
							enemy_count--
						}
						else
						{
							if(enemy_count == 0)
							{
								reapp_end = true
							}
							else
							{
								if(last_spawn - time > time_spawn)
								{
									new_enemy = true
								}
							}
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							timer.alive = false
							root.door_open--
						}
					}
				}
			}
		}
	}


	Q.Component
	{
		id: tp_behaviour_factory
		Animation
		{
			id: timer
			property var boss: root.boss
			time: 7
			speed: -1
			property var anim_tp
			property bool active: root.tp_ship_player
			property bool alive: true
			property real dist_blink: 40
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(timer.boss && ship && active)
				{
					if(!anim_tp)
					{
						var dist_x = ship.position.x - boss.position.x
						var dist_y = ship.position.y - boss.position.y
						var dist = Math.sqrt(dist_x * dist_x + dist_y * dist_y)
						if(dist > dist_blink)
						{
							anim_tp = tp_ship_factory.createObject(null, {parent: scene})
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: tp_ship_factory
		Animation
		{
			id: timer
			property var boss: root.boss
			property var door
			property var animation_apparition
			property real value:0
			property real sens: 1
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property bool before_tp: true
			property bool altern: true
			property real distance: 20 + 5 * Math.random()

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				var step = 0.05

				if(door && before_tp)
				{
					door.position.x = ship.position.x
					door.position.y = ship.position.y
					if(animation_apparition)
					{
						animation_apparition.position.x = ship.position.x
						animation_apparition.position.y = ship.position.y
					}
				}

				if(timer.boss)
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door=space_door_factory.createObject(null, {scene: ship.scene, position: Qt.point(ship.position.x, ship.position.y ), z: altitudes.boss - 0.00001, angle: 0, scale: 4 })
						reapp_beg=true
						animation_apparition = space_door_factory.createObject(null, {scene: door.scene, position: Qt.point(door.position.x, door.position.y), z: altitudes.shield + 0.001, angle : door.angle, indice: 10, mask: "transparent" , scale: 4 })
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						altern = !altern
						if(altern)
						{
							door.indice++
						}
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//disparition / apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						var a = animation_apparition.mask.a
						if(before_tp)
						{
							a = a + step
						}
						else
						{
							a = a - step
						}
						if(a >= 0.5 && before_tp)
						{
							a = 0.5
							before_tp = false
							var angle_tp = Math.random() * 2 * Math.PI

							ship.position.x = boss.position.x + distance * Math.cos(angle_tp)
							ship.position.y = boss.position.y + distance * Math.sin(angle_tp)
							door.position.x = ship.position.x
							door.position.y = ship.position.y
							animation_apparition.position.x = ship.position.x
							animation_apparition.position.y = ship.position.y
						}
						if(a <= 0 && !before_tp)
						{
							a = 0
							reapp_end = true
						}
						animation_apparition.mask.a = a
						if(reapp_end)
						{
							animation_apparition.alive = false
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						altern = !altern
						if(altern)
						{
							door.indice--
						}
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							timer.alive = false
						}
					}
				}
			}
		}
	}
	
	Q.Component
	{
		id: armagedon_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: Qt.rgba(1,0,0,1)
			scale: 0.01
			property real z: 0.01
			readonly property real damages: 10000
			property real ti_local: root.ti
			
			onTi_localChanged:
			{
				if(scale < 50)
				{
					if(scale > 0)
					{
						angle += 0.01
						scale += 15 / scene.seconds_to_frames(1)
					
					}
				}
				else
				{
					var a  = mask_c.a - 0.05
					if(a <= 0)
					{
						destroy()
					}
					else
					{
						mask_c.a = a
					}
				}
			}
			
			Q.Component.onDestruction:
			{
				root.armagedon_end = true
			}
			
			Q.Component.onCompleted: 
			{
				boom_s.play()
			}
			onScaleChanged:
			{
				collider.update_transform()
			}
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			property Sound boom_s : Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 5000
			}
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				sensor: true
			}
		}
	}

	Q.Component
	{
		id: move_tp_boss
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property real value: 0
			property real enemy_count: 4
			property var position_enemy: root.boss.position
			property var position_central
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property real last_spawn: 7
			property real time_spawn: 1.5

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: root.boss.scale * 2 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						var a = root.boss.alpha - 0.05
						if(a < 0)
						{
							a = 0
							reapp_end = true
							root.boss.untouchable = true
						}
						root.boss.alpha = a
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							//create objet
							tp_boss_factory.createObject(null, {parent: scene})
							timer.alive = false
							root.door_open--
						}
					}
				}
			}
		}
	}

	
	Q.Component
	{
		id: tp_boss_factory
		Animation
		{
			id: timer
			property var tab_pos: [
					[0,-15],
					[10,-7.5],
					[10,7.5],
					[0,15],
					[-10,7.5],
					[-10,-7.5]
					]

			function next_pos()
			{
				var a = random_index(tab_pos.length)
				return tab_pos[a]
			}
					
			time: 7
			speed: -1
			property bool alive: true
			property bool first: true
			property int counter: 25
			property real deb_frame: time
			property var pos
			property real time_frame: 5 / scene.seconds_to_frames(1)
			property real deb_blink: time
			property real time_blink: 3
			property real deb_dash: time
			property real time_dash: 0.5
			
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(first)
				{
					first = false
					for(var i = 0 ; i < tab_pos.length ; ++i)
					{
						if(root.boss && ship)
						{
							door_boss.createObject(null, {parent: scene, position_enemy: Qt.point(ship.position.x + tab_pos[i][0], root.boss.position.y + tab_pos[i][1]), position_central: tab_pos[i]})
						}
					}
				}
				else
				{
					if(counter > 0)
					{
						counter--
						if(counter == 1)
						{
							deb_frame = time
							deb_blink = time
						}
					}
					else
					{
					
						if(deb_blink - time > time_blink)
						{
							if(!root.boss.dash)
							{
								root.boss.alpha = 1
								root.boss.position = root.boss.position
								root.boss.angle = Math.atan2(ship.position.y - root.boss.position.y, ship.position.x - root.boss.position.x )  + Math.PI / 2
								root.boss.mask_sh = Qt.rgba(1,0,0,1)
								root.boss.dash = true
								root.boss.untouchable = false
								deb_dash = time
							}
							else
							{
								if(deb_dash - time > time_dash)
								{
									root.boss.dash = false
									root.blink_end = true
									timer.alive = false
								}
							}
						}
						else
						{
							if(deb_frame - time > time_frame)
							{
								deb_frame = time
								root.boss.alpha = 0.5
								pos = next_pos()
								root.boss.position = Qt.point(ship.position.x + pos[0],ship.position.y + pos[1])
								root.boss.position = Qt.binding(function(){return Qt.point(ship.position.x + pos[0],ship.position.y + pos[1])})
								root.boss.angle = Math.atan2(ship.position.y - root.boss.position.y, ship.position.x - root.boss.position.x )  + Math.PI / 2
							}
						}
					}
				}
			}
		}
	}
	Q.Component
	{
		id: door_boss
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property real value: 0
			property real enemy_count: 4
			property var position_enemy: root.boss.position
			property var position_central
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property real deb_spawn: time
			property real time_spawn: 4

			time: 0
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9 && ship) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: root.boss.scale * 2 })
						door.position = Qt.binding(function()
						{
							var res = Qt.point(0,0)
							if(!ship)
							{
								res = Qt.point(root.last_ship_position.x + position_central[0], root.last_ship_position.y + position_central[1])
							}
							else
							{
								res = Qt.point(ship.position.x + position_central[0], ship.position.y + position_central[1])
							}
							return res 
						})
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
							deb_spawn = time
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						if(deb_spawn - time > time_spawn)
						{
							reapp_end = true
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							timer.alive = false
							root.door_open--
						}
					}
				}
			}
		}
	}
	
	
	Q.Component
	{
		id: boss_incoming
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property real value: 0
			property real enemy_count: 4
			property var position_enemy: root.boss.position
			property var position_central
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property real last_spawn: 7
			property real time_spawn: 1.5

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: root.boss.scale * 2 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						if(!root.boss.protec)
						{
							root.boss.protec = true
						}
						var a = root.boss.alpha + 0.05
						if(a > 1)
						{
							a = 1
							reapp_end = true
						}
						root.boss.alpha = a
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							root.incoming_end = true
							reapp_end = false
							door.alive = false
							//create objet
							timer.alive = false
							root.door_open--
						}
					}
				}
			}
		}
	}
	Q.Component
	{
		id: concentration_player
		Animation
		{
			id: timer
			property var enemy
			property bool concentrate: true
			property int nb_concentrate: 2
			time: 0
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(concentrate && root.concentration == 0)
				{
					concentration_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y)})
					nb_concentrate--
					if(nb_concentrate == 0)
					{
						concentrate = false
					}
				}
				if(!concentrate && root.concentration == 0)
				{
					root.assimilated()
					timer.alive = false
				}
			}
		}
	}
	Q.Component
	{
		id: explosion_factory
		Explosion
		{
			scale: 4
		}
	}

	Q.Component
	{
		id: crack_factory
		Body
		{
			id: cracks
			property int ind: 0
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			scale: root.boss ? root.boss.scale * 0.8 : 0.1
			property real ti_local : root.ti
			property color mask: root.boss ? root.boss.mask_sh : "white"
			property real deb_crack: root.ti
			property real time_crack: 10
			property real deb_wait: root.ti
			property bool wait: false
			
			onTi_localChanged:
			{
				if(!wait)
				{
					if(deb_crack - root.ti > time_crack / scene.seconds_to_frames(1))
					{
						var a = cracks.ind + 1
						if(a > 5)
						{
							armagedon_factory.createObject(null, {scene: scene, position: Qt.point(root.boss.position.x, root.boss.position.y)})
							cracks.wait = true
							cracks.deb_wait = root.ti
						}
						else
						{
							cracks.ind = a
						}
						
					}
				}
				else
				{
					if(deb_wait - root.ti > 2)
					{
						destroy()
					}
				}
			}
			
			Q.Component.onCompleted:
			{
				deb_crack = root.ti
			}
			Image
			{
				id: image
				material: "chapter-4/shield-cracks/" + cracks.ind
				mask: cracks.mask
				z: altitudes.shield
			}
		}	
	}
	
	Q.Component
	{
		id: armagedon_animation_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: Qt.rgba(1,1,1,1)
			scale: 0.01
			property real z: 0.01
			property real ti_local: root.ti
			
			onTi_localChanged:
			{
				if(scale < 50)
				{
					if(scale > 0)
					{
						angle += 0.01
						scale += 15 / scene.seconds_to_frames(1)
					
					}
				}
				else
				{
					var a  = mask_c.a - 0.05
					if(a <= 0)
					{
						destroy()
					}
					else
					{
						mask_c.a = a
					}
				}
			}
			
			Q.Component.onCompleted: 
			{
				boom_s.play()
			}
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			property Sound boom_s : Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 5000
			}
		}
	}
	
	
	Q.Component
	{
		id: boss_end_transition
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)} 
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 0
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 6
			angle: 0
			property var material: "chapter-7/boss-brain/brain-asteroids/0"
			property real ti_local: root.ti
			property real deb_trans: root.ti
			property real time_to_white: 2
			property real deb_sc: root.ti
			property real time_to_sc: 5
			property real scale_sh: 1.04
			property real last_boom: root.ti
			property bool swap: true
			property bool finish: false
			property var armagedon
			property bool first: true
			property real alpha: 1
			onTi_localChanged:
			{
				if(deb_trans != 0)
				{
					if(deb_trans - root.ti < time_to_sc)
					{
						var explosion_step = 0.1
						var ajust = swap ? 0.1 : -0.1
						swap= !swap
						boss.position = Qt.point(boss.position.x + ajust, boss.position.y + ajust)
						if(last_boom - root.ti  > explosion_step)
						{
							last_boom = root.ti 
							var angle_ran = 2 * Math.PI * Math.random()
							var scale_ran = boss.scale * Math.random()
							explosion_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss.position.x + scale_ran * Math.cos(angle_ran), boss.position.y + scale_ran * Math.sin(angle_ran)), scale : 6})
						}
					}
					else
					{
						if(first)
						{
							deb_sc = root.ti
							armagedon = armagedon_animation_factory.createObject(null, {scene: ship.scene, position: Qt.point(boss.position.x , boss.position.y)})
							first = false
						}
						else
						{
							if(deb_sc - root.ti > 3)
							{
								if(boss.alpha != 0)
								{
									boss.alpha = 0
								}
							}
							if(!armagedon)
							{
								root.boss_destroyed()
								destroy()
							}
						}
					}
				}
			}
			
			Q.Component.onCompleted:
			{
				deb_trans = root.ti
				last_boom = root.ti
			}
			
			Image
			{
				material: boss.material
				z: altitudes.boss
				mask:  Qt.rgba(1,0,0, boss.alpha)
			}
			
			
		}
	}
	Q.Component
	{
		id: boss_1
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)}
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: boss.enable_phase ? (laser_shoot ? (root.vener ? 1.1 : 0.8) : 1.5) : 0
			icon: 3
			icon_color: "red"
			max_speed: boss.enable_phase ? (laser_shoot ? 0 : 10) :  dash ? 100 : 0
			scale: 6
			property real alpha: 0
			property int nb_wave_to_clean: 1
			property int wave: 0
			property real ti_local: root.ti
			property int indice_image: 0
			property var nb_image: [25,1,25,25,1,25]
			property int indice_phase: 0
			property bool enable_phase: false
			property bool agressive: false
			property var coil_gun_t
			property color mask_sh: Qt.rgba(1,1,0,0)
			property var tab_phase: [
			"chapter-7/boss-brain/brain-asteroids/",
			"chapter-7/boss-brain/brain-asteroids/",
			"chapter-7/boss-brain/brain-multishoot/",
			"chapter-7/boss-brain/brain-star-bullet/",
			"chapter-7/boss-brain/brain-asteroids/",
			"chapter-7/boss-brain/brain-big-cannon/"
			] // "matériaux" + indice_im
			property var door
			property bool deb_phase: true
			property bool end_phase: false
			property bool active_phase: true
			property real deb_anim: root.ti
			property real time_anim: 1
			property real deb_t_phase: root.ti
			property real time_phase: 7.5
			property bool start: false
			property bool first: true
			property bool protec: false
			property bool reflec: false
			property bool star_bullet_shoot: false
			property var star_bullet:[]
			property real deb_reflec: root.ti
			property real time_reflec: 4
			property real deb_protec: root.ti
			property real time_protec: 4
			property real deb_trans: root.ti
			property real time_trans: 6
			property bool untouchable: false
			property bool dash: false
			property bool laser_shoot: false
			property var kame
			property var kame_pos:[512,322]
			property bool dialogue: true
			property bool concentrate: true
			property int nb_concentrate: 3
			onNb_concentrateChanged:
			{
				music.objectName = "chapter-7/true_last_boss"
			}
			onStartChanged:
			{
				if(start)
				{
					enable_phase = true
					music.objectName = "chapter-7/true_last_boss"
				}
			}
			onTi_localChanged:
			{
				if(ship && boss.nb_image && !start)
				{
					//apparition
					if(first)
					{
						first = false
						boss_incoming.createObject(null, {parent: root.scene})
					}
					if(incoming_end)
					{
						if(dialogue)
						{
							dialogue = false
							root.dialogue()
						}
						else
						{
							if(concentrate && root.concentration == 0)
							{
								mask_sh = Qt.rgba(0.58, 0, 0.82,1)
								concentration_factory.createObject(null, {scene: scene, position: Qt.point(boss.position.x, boss.position.y)})
								nb_concentrate--
								if(nb_concentrate == 0)
								{
									concentrate = false
								}
							}
							else
							{
								if(!concentrate && root.concentration == 0)
								{
									mask_sh.a = 0
									protec = false
									incoming_end = false
									first = true
									start = true
								}
							}
						}
					}
				}
				if(ship && boss.nb_image && start)
				{
					if(deb_phase)
					{
						var x = ship.position.x - boss.position.x
						var y = ship.position.y - boss.position.y
						if(Math.sqrt(x*x + y*y) < 30)
						{
							tp_ship_player = false
							deb_phase = false
							active_phase = true
							deb_anim = root.ti
							deb_t_phase = root.ti
						}
					}
					if(!deb_phase && !end_phase)
					{	
						if(indice_phase == 0)
						{
							if(active_phase && indice_image < boss.nb_image[indice_phase] - 1 )
							{
								if(deb_anim - root.ti > time_anim / boss.nb_image[indice_phase] )
								{
									deb_anim = root.ti
									deb_t_phase = root.ti
									indice_image++
								}
							}
							else
							{
								if(deb_t_phase - root.ti < time_phase)
								{
									if(active_phase)
									{
										root.asteroid_phase = true
										root.new_wave = true
										boss.enable_phase = false
										boss.mask_sh = Qt.rgba(1,0,0,1)
										boss.agressive = true
										active_phase = false
									}
								}
								else
								{
									if( indice_image != 0 )
									{
										if(deb_anim - root.ti > time_anim / nb_image[indice_phase] )
										{
											deb_anim = root.ti
											indice_image--
										}
									}
									else
									{
										boss.enable_phase = true
										boss.agressive = false
										root.asteroid_phase = false
										boss.mask_sh.a = 0
										end_phase = true
									}
								}
							}
						}
						if(indice_phase == 1)
						{
							if(active_phase && root.concentration == 0)
							{
								concentration_factory.createObject(null, {scene: scene, position: Qt.point(boss.position.x, boss.position.y)})
								boss.enable_phase = false
							}
							else
							{
								if(active_phase)
								{
									active_phase = false
								}
								else
								{
									if(root.concentration == 0)
									{
										if(first)
										{
											black_hole_factory.createObject(null, {scene: scene, position: Qt.point(boss.position.x, boss.position.y)})
											first = false
										}
										if(!black_hole_alive)
										{
											boss.enable_phase = true
											first = true
											end_phase = true
										}
									}
								}
							}
						}
						if(indice_phase == 2)
						{
							if(active_phase && indice_image < boss.nb_image[indice_phase] - 1 )
							{
								if(deb_anim - root.ti > 2 * time_anim / boss.nb_image[indice_phase] )
								{
									deb_anim = root.ti
									deb_t_phase = root.ti
									indice_image++
								}
							}
							else
							{
								if(deb_t_phase - root.ti < time_phase)
								{
									if(active_phase)
									{
										active_phase = false
										coil_gun_t.shoot = true
										boss.enable_phase = false
									}
									angle += 2 * Math.PI / scene.seconds_to_frames(root.vener ? 8 : 10)
								}
								else
								{
									if(coil_gun_t.shoot)
									{
										coil_gun_t.shoot = false
									}
									if( indice_image != 0 )
									{
										if(deb_anim - root.ti > time_anim / nb_image[indice_phase] )
										{
											deb_anim = root.ti
											indice_image--
										}
									}
									else
									{
										boss.enable_phase = true
										end_phase = true
									}
								}
							}
						}
						
						if(indice_phase == 3)
						{
							if(active_phase && indice_image < boss.nb_image[indice_phase] - 1 )
							{
								if(deb_anim - root.ti > time_anim / boss.nb_image[indice_phase] )
								{
									deb_anim = root.ti
									deb_t_phase = root.ti
									indice_image++
								}
							}
							else
							{
								if(active_phase)
								{
									active_phase = false
									boss.reflec = true
									deb_reflec = root.ti
									boss.mask_sh = Qt.rgba(1,1,0,1)
									star_bullet_shoot = true
								}
								else
								{
									if(boss.reflec)
									{
										if(deb_reflec - root.ti > time_reflec)
										{
											reflec = false
											deb_trans = root.ti
										}
									}
									else
									{
										if(boss.protec)
										{
											if(boss.enable_phase)
											{
												if(deb_protec - root.ti > time_protec)
												{
													boss.enable_phase = false
													crack_factory.createObject(null, {scene: scene, position: Qt.point(boss.position.x, boss.position.y)})
													star_bullet_shoot = false
												}
											}
											else
											{
												var r = boss.mask_sh.r + 0.03
												var b = boss.mask_sh.b - 0.03
												if(r > 1)
												{
													r = 1
												}
												if(b < 0)
												{
													b = 0
												}
												boss.mask_sh.r = r
												boss.mask_sh.b = b
												if(root.armagedon_end)
												{
													if( indice_image != 0 )
													{
														if(deb_anim - root.ti > time_anim / nb_image[indice_phase] )
														{
															deb_anim = root.ti
															indice_image--
														}
													}
													else
													{
														root.armagedon_end = false
														end_phase = true
														boss.protec = false
														boss.enable_phase = true
														boss.mask_sh.a = 0
													}
												}
											}
										}
										else
										{
											//transition
											if(deb_trans - root.ti > time_trans)
											{
												boss.protec = true
												deb_protec = root.ti
											}
											else
											{											
												boss.mask_sh.g = 1 - (deb_trans - ti_local) / time_trans
												boss.mask_sh.r = 1 - 0.42 * (deb_trans - ti_local) / time_trans
												boss.mask_sh.b = 0.82 * (deb_trans - ti_local) / time_trans
											}
										}
									}
								}
							}
						}
						
						if(indice_phase == 4)
						{
							if(first)
							{
								first = false
								boss.enable_phase = false
								move_tp_boss.createObject(null, {parent: root.scene})
							}
							else
							{
								if(root.blink_end)
								{
									root.blink_end = false
									first = true
									boss.enable_phase = true
									boss.mask_sh.a = 0
									end_phase = true
								}
							}
						}
						if(indice_phase == 5)
						{
							if(active_phase && indice_image < boss.nb_image[indice_phase] - 1 )
							{
								if(deb_anim - root.ti > time_anim / boss.nb_image[indice_phase] )
								{
									deb_anim = root.ti
									deb_t_phase = root.ti
									indice_image++
								}
							}
							else
							{
								if(deb_t_phase - root.ti < time_phase * 1.5)
								{
									if(active_phase)
									{
										boss.laser_shoot = true
										active_phase = false
									}
								}
								else
								{
									if(boss.laser_shoot)
									{
										boss.laser_shoot = false
										boss.enable_phase = false
									}
									if( indice_image != 0 )
									{
										if(deb_anim - root.ti > time_anim / nb_image[indice_phase] )
										{
											deb_anim = root.ti
											indice_image--
										}
									}
									else
									{
										boss.enable_phase = true
										end_phase = true
									}
								}
							}
						}
						
					}
					if(end_phase)
					{
						if(health_boss.health < 150000)
						{
							indice_phase = random_index(6)
							if(!root.vener)
							{
								root.vener = true
							}
						}
						else
						{
							indice_phase = (indice_phase + 1)%6
						}
						end_phase = false
						deb_phase = true
						tp_ship_player = true
					}
				}
			}
			
			
			Q.Component.onCompleted:
			{
				kame = kamehameha_factory.createObject(null, {scene: scene, position: Qt.point(boss.position.x + boss.coords(512,314).x, boss.position.y + boss.coords(512,314).y), scale: 1})
				kame.angle = Qt.binding(function(){return boss.angle})
				kame.position = Qt.binding(function(){
					var x = boss.position.x - boss.scale * boss.coords(512,314).y * Math.sin(boss.angle)
					var y = boss.position.y + boss.scale * boss.coords(512,314).y * Math.cos(boss.angle) 
					return Qt.point(x,y)
				})
				for(var i = 0; i < 4; i++)
				{
					star_bullet.push(canon_star_bullet_factory.createObject(null, {scene: scene, indice: i}))
				}
				coil_gun_t = coil_gun_transport.createObject(null, {scene: scene, position: Qt.point(boss.position.x, boss.position.y), scale: boss.scale})
				coil_gun_t.position = Qt.binding(function(){return Qt.point(boss.position.x,boss.position.y)})
				coil_gun_t.angle = Qt.binding(function(){return boss.angle})
			}
			Q.Component.onDestruction:
			{
				for(var i =0; i< star_bullet.length; i++)
				{
					star_bullet[i].alive = false
				}
				coil_gun_t.alive = false
				boss_end_transition.createObject(null, {scene: scene, position: Qt.point(boss.position.x, boss.position.y), scale: boss.scale, angle: boss.angle, material: tab_phase[indice_phase] + indice_image})
			}

			Image
			{
				material: tab_phase[indice_phase] + indice_image
				z: altitudes.boss
				mask: health_boss ? Qt.rgba(1, health_boss.health / health_boss.max_health, health_boss.health / health_boss.max_health, boss.alpha) : Qt.rgba(1,0,0, boss.alpha)
			}
			
			Image
			{
				material: "equipments/shield"
				mask: boss.mask_sh
				scale: 0.8
			}
			
			EquipmentSlot
			{
				id: slot_1
				equipment: AggressiveShield
				{
					id: health_boss
					health: max_health
					max_health: 300000
					shooting: boss.dash
					damages: root.vener ? 10000 : 2000  
					Q.Component.onCompleted:
					{
						image.mask.a = 0
					}
					Q.Component.onDestruction:
					{
						if(boss)
						{
							boss.destroy()
						}
					}
				}
			}


			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: boss.dash ? 0 : 6 
			}
			
			function begin(other, point)
			{
				var v = other.velocity
				var p = point
				var vect_radian = Qt.point(point.x - boss.position.x, point.y - boss.position.y)
				var tempx = vect_radian.x
				var tempy = vect_radian.y
				vect_radian.x = tempx / Math.sqrt(tempx * tempx + tempy * tempy)
				vect_radian.y = tempy / Math.sqrt(tempx * tempx + tempy * tempy)
				var point_dep = Qt.point(p.x - v.x, p.y - v.y)
				var vect_dep = Qt.point(point_dep.x - p.x, point_dep.y - p.y)
				
				// AH = <AC.v>/norm(v) * v
				var prod_proj = (vect_dep.x * vect_radian.x + vect_dep.y * vect_radian.y) / Math.sqrt(vect_radian.x * vect_radian.x + vect_radian.y * vect_radian.y)
				
				var vect_proj = Qt.point(prod_proj * vect_radian.x, prod_proj * vect_radian.y)
				
				var point_proj = Qt.point(p.x + vect_proj.x, p.y + vect_proj.y)
				var vect_ortho = Qt.point(point_proj.x - point_dep.x, point_proj.y - point_dep.y)
				
				var point_final = Qt.point(vect_ortho.x + point_proj.x, vect_ortho.y + point_proj.y)
				var vect_velocity =  Qt.point(point_final.x - p.x, point_final.y - p.y)
				
				other.velocity = Qt.point(vect_velocity.x, vect_velocity.y)
				var a = Math.atan2(vect_velocity.y, vect_velocity.x) + Math.PI / 2
				
				var e = scene.time_changed
				function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
				e.connect(f)
				for(var i in other.children)
				{
					var child = other.children[i]
					if(child.group) child.group ^= 1
					if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
				}
			}
			
			CircleCollider
			{
				id: collider
				group: boss.protec ? 0 : (boss.untouchable ? 0 : groups.item_ignore_enemy)
				radius: 0.5
			}
			CircleCollider
			{
				id: collider_sh
				group: boss.protec ? groups.enemy_invincible : (boss.reflec ? groups.reflector_only_player : ( (boss.dash || boss.agressive) ? groups.enemy_shield_aggressive : 0)) 
				radius: 0.8
			}
		}
	}

	Q.Component
	{
		id: zoom_ship
		Animation
		{
			id: timer

			time: 7
			property var black_hole_object
			property real scale : 0.1
			speed: -1
			property bool alive: true
			property bool first: true
			property bool move: true
			property real deb: time
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)}
			Q.Component.onCompleted:
			{
				root.camera_position = Qt.point(ship.position.x,ship.position.y)
			}
			onTimeChanged:
			{
				if(root.camera_position && first)
				{
					first = false
					deb = time
				}
				if(root.camera_position)
				{
					root.camera_position.x +=  -ship.scale * timer.coords(512,328).y * Math.sin(ship.angle) * 6 / scene.seconds_to_frames(3)
					root.camera_position.y += ship.scale * timer.coords(512,328).y * Math.cos(ship.angle) * 6 / scene.seconds_to_frames(3)					
					ship.scale += 29 / scene.seconds_to_frames(3) 
				}
				if(ship.scale > 30)
				{
					root.sortie()
					timer.alive = false
				}
			}
		}
	}
	Q.Component
	{
		id: move_ship_to
		Animation
		{
			id: timer

			//ship_angle entre -pi et pi
			function calcul_ship_angle(angle)
			{
				var result = angle
				while(result <= -Math.PI)
				{
					result += 2 * Math.PI
				}
				while(result > Math.PI)
				{
					result -= 2 * Math.PI
				}
				return result
			}

			property real epsilon: 0.5
			property real x
			property real long_x
			property real y
			property real long_y
			property real angle
			property real long_angle
			property real time_to_move: 3
			property bool first: true
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(time < 6.9)
				{
					if(first)
					{
						ship.angle = calcul_ship_angle(ship.angle)
						long_x = ship.position.x - timer.x
						long_y = ship.position.y - timer.y
						long_angle = calcul_ship_angle(ship.angle - timer.angle)
						first = false
					}
					else
					{
						if(ship)
						{
							if(root.scene.seconds_to_frames(6.9) - root.scene.seconds_to_frames(time) < root.scene.seconds_to_frames(time_to_move) + 2)
							{
								ship.position.x -= long_x / root.scene.seconds_to_frames(time_to_move)
								ship.position.y -= long_y / root.scene.seconds_to_frames(time_to_move)
								ship.angle -= long_angle / root.scene.seconds_to_frames(time_to_move)
							}
							else
							{
								root.ship_ok()
								alive = false
							}
						}
					}
				}
			}
		}
	}
	StateMachine
	{
		begin: state_story_0 // state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_test_1 //state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined

					boss = boss_1.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y - 20)})
					tp_behaviour = tp_behaviour_factory.createObject(null, {parent: scene})
					//black_hole_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y - 40)})
					wave_spawner = wave_assault_factory.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					if(music.objectName != "chapter-7/true_last_boss")
					{
						music.objectName = "chapter-1/main"
					}
					move_ship_to.createObject(null, {parent: scene, x: 0, y: 0, angle: -Math.PI / 2, time_to_move: 1.5})
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "cutscene"; value: true}
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: ship_ok
				onTriggered:
				{
					boss = boss_1.createObject(null, {scene: scene, position: Qt.point(ship.position.x - 15, ship.position.y), angle: Math.PI / 2 })
					camera_position= undefined
					tp_behaviour = tp_behaviour_factory.createObject(null, {parent: scene})
					wave_spawner = wave_assault_factory.createObject(null, {parent: scene})
					messages.add("lycop/normal", qsTr("begin 1"))
					messages.add("nectaire/normal", qsTr("begin 2"))
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_2; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2
			AssignProperty {target: root; property: "cutscene"; value: true}
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: dialogue
				onTriggered:
				{
					messages.add("cerebus/normal", qsTr("cerebus 1"))
					messages.add("nectaire/normal", qsTr("cerebus 2"))
					messages.add("lycop/normal", qsTr("cerebus 3"))
					messages.add("cerebus/normal", qsTr("cerebus 4"))
					messages.add("lycop/normal", qsTr("cerebus 5"))
					messages.add("cerebus/normal", qsTr("cerebus 6"))
					messages.add("cerebus/normal", qsTr("cerebus 7"))
					messages.add("nectaire/normal", qsTr("cerebus 8"))
					messages.add("lycop/normal", qsTr("cerebus 9"))
					messages.add("cerebus/normal", qsTr("cerebus 10"))
				}
			}
		}
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_3; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_3
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_story_4
				signal: boss_destroyed
				onTriggered:
				{
					concentration_player.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_story_4
			AssignProperty {target: root; property: "cutscene"; value: true}
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: assimilated
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("end 1"))
					messages.add("nectaire/normal", qsTr("end 2"))
					messages.add("lycop/normal", qsTr("end 3"))
				}
			}
		}
		
		State 
		{
			id: state_dialogue_2
			SignalTransition 
			{
				targetState: state_story_5
				signal: state_dialogue_2.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					zoom_ship.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_story_5
			AssignProperty {target: root; property: "cutscene"; value: true}
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_end
				signal: sortie
				onTriggered:
				{
					saved_game.set("file", "game/chapter-7-epilogue/epilogue.qml")
					platform.set_bool("true-last-boss-end")
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}

	Q.Component.onCompleted:
	{
		jobs.run(music, "preload", ["chapter-1/main"])
		jobs.run(music, "preload", ["chapter-7/true_last_boss"])
		for(var i = 0; i < 6; ++i) jobs.run(scene_view, "preload", ["chapter-4/shield-cracks/" + i])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["chapter-5/star-bullet/" + i])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-3/door/" + i])
		jobs.run(scene_view, "preload", ["chapter-5/black-hole"])
		jobs.run(scene_view, "preload", ["asteroid/0"])
		jobs.run(scene_view, "preload", ["asteroid/1"])
		jobs.run(scene_view, "preload", ["asteroid/2"])
		for(var i = 0; i < 36; ++i) jobs.run(scene_view, "preload", ["chapter-3/concentration/" + i])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-7/boss-brain/brain-asteroids/" + i])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-7/boss-brain/brain-big-cannon/" + i])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-7/boss-brain/brain-star-bullet/" + i])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-7/boss-brain/brain-multishoot/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		for(var i = 0; i < 30; ++i)
		{
			for(var j = 0; j < 5; ++j)
			{
				jobs.run(scene_view, "preload", ["chapter-3/freezing-beam/" + i + "/" + j])
			}
		}
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["explosion"])
		jobs.run(scene_view, "preload", ["equipments/coil-gun/bullet"])
		jobs.run(scene_view, "preload", ["equipments/laser-gun/bullet"])

		groups.init(scene)

	}
}
