import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-7/chapter-7-corridor.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal sortie
	property var camera_position: Qt.point(7 * 4, 18.5 * 4)
	property var last_ship_position: Qt.point(0, 0)
	property var barriere_bas
	property real ti: 0
	property var info_print
	property real chapter_scale: 2
	property bool can_open: true
	property bool travel_map_2: true
	property real fuzzy_comp: 0
	
	
	
	Q.Component
	{
		id: heavy_laser_gun_factory
		HeavyLaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: harpoon_factory
		HarpoonLauncher
		{
			range_bullet: scene.seconds_to_frames(10)
			period: scene.seconds_to_frames(6)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: missile_launcher_factory
		MissileLauncher
		{
			range_bullet: scene.seconds_to_frames(2)
			level: 2
			shooting: false
		}
	}
	Q.Component
	{
		id: secondary_shield_factory
		SecondaryShield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: random_spray_factory
		RandomSpray
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: false
			enemy: 1
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: false
			period: scene.seconds_to_frames(3)
			level: 2
		}
	}
	
	
	
	//x,y,type,angle,invulnerable,cheat
	property var tab_turret_corridor_1:[
	[16,16 ,3, -1/2, false,2 ],
	[16,17 ,3, -1/2, false,2 ],
	[16,18 ,3, -1/2, false,2 ],
	[16,19 ,3, -1/2, false,2 ],
	[16,20 ,3, -1/2, false,2 ],
	[16,21 ,3, -1/2, false,2 ],
	[74,16 ,3, -1/2, false,2 ],
	[74,17 ,3, -1/2, false,2 ],
	[74,18 ,3, -1/2, false,2 ],
	[74,19 ,3, -1/2, false,2 ],
	[74,20 ,3, -1/2, false,2 ],
	[74,21 ,3, -1/2, false,2 ],
	[19,17 ,3, -1/2, false,2 ],
	[19,18 ,3, -1/2, false,2 ],
	[19,19 ,3, -1/2, false,2 ],
	[19,20 ,3, -1/2, false,2 ],
	[20,17 ,3, -1/2, false,2 ],
	[20,18 ,3, -1/2, false,2 ],
	[20,19 ,3, -1/2, false,2 ],
	[20,20 ,3, -1/2, false,2 ],
	[23,17 ,3, -1/2, false,2 ],
	[23,18 ,3, -1/2, false,2 ],
	[23,19 ,3, -1/2, false,2 ],
	[23,20 ,3, -1/2, false,2 ],
	[24,17 ,3, -1/2, false,2 ],
	[24,18 ,3, -1/2, false,2 ],
	[24,19 ,3, -1/2, false,2 ],
	[24,20 ,3, -1/2, false,2 ],
	[42,17 ,3, -1/2, false,2 ],
	[42,18 ,3, -1/2, false,2 ],
	[42,19 ,3, -1/2, false,2 ],
	[42,20 ,3, -1/2, false,2 ],
	[43,17 ,3, -1/2, false,2 ],
	[43,18 ,3, -1/2, false,2 ],
	[43,19 ,3, -1/2, false,2 ],
	[43,20 ,3, -1/2, false,2 ],
	[46,17 ,3, -1/2, false,2 ],
	[46,18 ,3, -1/2, false,2 ],
	[46,19 ,3, -1/2, false,2 ],
	[46,20 ,3, -1/2, false,2 ],
	[47,17 ,3, -1/2, false,2 ],
	[47,18 ,3, -1/2, false,2 ],
	[47,19 ,3, -1/2, false,2 ],
	[47,20 ,3, -1/2, false,2 ],
	[66,17 ,3, -1/2, false,2 ],
	[66,18 ,3, -1/2, false,2 ],
	[66,19 ,3, -1/2, false,2 ],
	[66,20 ,3, -1/2, false,2 ],
	[67,17 ,3, -1/2, false,2 ],
	[67,18 ,3, -1/2, false,2 ],
	[67,19 ,3, -1/2, false,2 ],
	[67,20 ,3, -1/2, false,2 ],
	[70,17 ,3, -1/2, false,2 ],
	[70,18 ,3, -1/2, false,2 ],
	[70,19 ,3, -1/2, false,2 ],
	[70,20 ,3, -1/2, false,2 ],
	[71,17 ,3, -1/2, false,2 ],
	[71,18 ,3, -1/2, false,2 ],
	[71,19 ,3, -1/2, false,2 ],
	[71,20 ,3, -1/2, false,2 ],
	[19.5,16 ,1, -1/2, true,0 ],
	[23.5,16 ,1, -1/2, true,0 ],
	[42.5,21 ,1, -1/2, true,0 ],
	[46.5,21 ,1, -1/2, true,0 ],
	[66.5,21 ,1, -1/2, true,0 ],
	[70.5,21 ,1, -1/2, true,0 ],
	[27,16 ,1, -1/2, true,0 ],
	[28,16 ,1, -1/2, true,0 ],
	[29,16 ,1, -1/2, true,0 ],
	[30,16 ,1, -1/2, true,0 ],
	[31,16 ,1, -1/2, true,0 ],
	[32,16 ,1, -1/2, true,0 ],
	[33,16 ,1, -1/2, true,0 ],
	[34,16 ,1, -1/2, true,0 ],
	[35,16 ,1, -1/2, true,0 ],
	[36,16 ,1, -1/2, true,0 ],
	[37,16 ,1, -1/2, true,0 ],
	[38,16 ,1, -1/2, true,0 ],
	[39,16 ,1, -1/2, true,0 ],
	[27,21 ,1, -1/2, true,0 ],
	[28,21 ,1, -1/2, true,0 ],
	[29,21 ,1, -1/2, true,0 ],
	[30,21 ,1, -1/2, true,0 ],
	[31,21 ,1, -1/2, true,0 ],
	[32,21 ,1, -1/2, true,0 ],
	[33,21 ,1, -1/2, true,0 ],
	[34,21 ,1, -1/2, true,0 ],
	[35,21 ,1, -1/2, true,0 ],
	[36,21 ,1, -1/2, true,0 ],
	[37,21 ,1, -1/2, true,0 ],
	[38,21 ,1, -1/2, true,0 ],
	[39,21 ,1, -1/2, true,0 ],
	[50,16 ,1, -1/2, true,0 ],
	[51,16 ,1, -1/2, true,0 ],
	[52,16 ,1, -1/2, true,0 ],
	[53,16 ,1, -1/2, true,0 ],
	[54,16 ,1, -1/2, true,0 ],
	[55,16 ,1, -1/2, true,0 ],
	[56,16 ,1, -1/2, true,0 ],
	[57,16 ,1, -1/2, true,0 ],
	[58,16 ,1, -1/2, true,0 ],
	[59,16 ,1, -1/2, true,0 ],
	[60,16 ,1, -1/2, true,0 ],
	[61,16 ,1, -1/2, true,0 ],
	[62,16 ,1, -1/2, true,0 ],
	[63,16 ,1, -1/2, true,0 ],
	[50,21 ,1, -1/2, true,0 ],
	[51,21 ,1, -1/2, true,0 ],
	[52,21 ,1, -1/2, true,0 ],
	[53,21 ,1, -1/2, true,0 ],
	[54,21 ,1, -1/2, true,0 ],
	[55,21 ,1, -1/2, true,0 ],
	[56,21 ,1, -1/2, true,0 ],
	[57,21 ,1, -1/2, true,0 ],
	[58,21 ,1, -1/2, true,0 ],
	[59,21 ,1, -1/2, true,0 ],
	[60,21 ,1, -1/2, true,0 ],
	[61,21 ,1, -1/2, true,0 ],
	[62,21 ,1, -1/2, true,0 ],
	[63,21 ,1, -1/2, true,0 ]
	]
	
	
	
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-6/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-6/"+mat+"/c"])
			}
		}
		return fill_tiles
	}
	
	function fill_2(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y, mat])
			}
		}
		return fill_tiles
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}
	
	/*Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("Temps restant: %1").arg(root.ti)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}*/
	
	scene: Scene
	{
		running: false
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		
		Tiles
		{
			id: corridor_1
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[17,15,'n','metal'],
				[17,20,'e','metal'],
				[18,20,'n','metal'],
				[18,15,'w','metal'],
				[25,15,'n','metal'],
				[25,20,'e','metal'],
				[26,20,'n','metal'],
				[26,15,'w','metal'],
				[44,15,'n','metal'],
				[44,20,'e','metal'],
				[45,20,'n','metal'],
				[45,15,'w','metal'],
				[68,15,'n','metal'],
				[68,20,'e','metal'],
				[69,20,'n','metal'],
				[69,15,'w','metal'],
				[90,15,'n','metal'],
				[90,22,'e','metal'],
				[73,22,'s','metal'],
				[73,17,'w','metal'],
				[72,17,'s','metal'],
				[72,22,'e','metal'],
				[65,22,'s','metal'],
				[65,17,'w','metal'],
				[64,17,'s','metal'],
				[64,22,'e','metal'],
				[49,22,'s','metal'],
				[49,17,'w','metal'],
				[48,17,'s','metal'],
				[48,22,'e','metal'],
				[41,22,'s','metal'],
				[41,17,'w','metal'],
				[40,17,'s','metal'],
				[40,22,'e','metal'],
				[22,22,'s','metal'],
				[22,17,'w','metal'],
				[21,17,'s','metal'],
				[21,22,'e','metal'],
				[6,22,'s','metal'],
				[6,15,'w','metal']
				]).concat(
				fill(7,16,16,21,'metal'),
				fill(17,21,18,21,'metal'),
				fill(19,16,20,21,'metal'),
				fill(21,16,22,16,'metal'),
				fill(23,16,24,21,'metal'),
				fill(25,21,26,21,'metal'),
				fill(27,16,39,21,'metal'),
				fill(40,16,41,16,'metal'),
				fill(42,16,43,21,'metal'),
				fill(44,21,45,21,'metal'),
				fill(46,16,47,21,'metal'),
				fill(48,16,49,16,'metal'),
				fill(50,16,63,21,'metal'),
				fill(64,16,65,16,'metal'),
				fill(66,16,67,21,'metal'),
				fill(68,21,69,21,'metal'),
				fill(70,16,71,21,'metal'),
				fill(72,16,73,16,'metal'),
				fill(74,16,89,21,'metal'),
				[[8,17,'chapter-6/metal/c2'],
				[12,17,'chapter-6/metal/c2'],
				[16,17,'chapter-6/metal/c2'],
				[27,17,'chapter-6/metal/c2'],
				[31,17,'chapter-6/metal/c2'],
				[35,17,'chapter-6/metal/c2'],
				[39,17,'chapter-6/metal/c2'],
				[50,17,'chapter-6/metal/c2'],
				[54,17,'chapter-6/metal/c2'],
				[58,17,'chapter-6/metal/c2'],
				[62,17,'chapter-6/metal/c2'],
				[74,17,'chapter-6/metal/c2'],
				[78,17,'chapter-6/metal/c2'],
				[82,17,'chapter-6/metal/c2'],
				[86,17,'chapter-6/metal/c2'],
				[8,20,'chapter-6/metal/c2'],
				[12,20,'chapter-6/metal/c2'],
				[16,20,'chapter-6/metal/c2'],
				[27,20,'chapter-6/metal/c2'],
				[31,20,'chapter-6/metal/c2'],
				[35,20,'chapter-6/metal/c2'],
				[39,20,'chapter-6/metal/c2'],
				[50,20,'chapter-6/metal/c2'],
				[54,20,'chapter-6/metal/c2'],
				[58,20,'chapter-6/metal/c2'],
				[62,20,'chapter-6/metal/c2'],
				[74,20,'chapter-6/metal/c2'],
				[78,20,'chapter-6/metal/c2'],
				[82,20,'chapter-6/metal/c2'],
				[86,20,'chapter-6/metal/c2']]
				)
		}
		Ship
		{
			id: ship	
			position.x: 7 * 4
			position.y: 18.5 * 4
			angle: Math.PI/2
			Q.Component.onDestruction: 
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
			
		}
		
		Body
		{
			id: all_wall
			position.y: 0
			position.x: 0
			scale: chapter_scale*2
			
			type: Body.STATIC
			
			function point(x,y)
			{
				return Qt.point(x - 0.25 , y + 0.25)
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(-5,4), all_wall.point(90 + 2*0.25,4), all_wall.point(90 + 2 * 0.25, 15 - 2 *0.25), all_wall.point(-5,15 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(90 + 2 * 0.25,4), all_wall.point(98,4), all_wall.point(98,22), all_wall.point(90+ 2 *0.25,22)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(6,22), all_wall.point(98,22), all_wall.point(98,32), all_wall.point(6, 32)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(-5,15 - 2 * 0.25), all_wall.point(6,15 - 2 * 0.25), all_wall.point(6 ,32), all_wall.point(-5,32)]
				group: groups.wall
			}
			
			
			PolygonCollider
			{
				vertexes: [all_wall.point(17 + 2 * 0.25, 15 - 2 * 0.25), all_wall.point(18,15 - 2 * 0.25), all_wall.point(18,20 - 2 * 0.25), all_wall.point(17 + 2 * 0.25, 20 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(21 + 2 * 0.25, 17), all_wall.point(22,17), all_wall.point(22,22), all_wall.point(21 + 2 * 0.25,22)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(25 + 2 * 0.25, 15 - 2 * 0.25), all_wall.point(26, 15 - 2 * 0.25), all_wall.point(26,20 - 2 *0.25), all_wall.point(25 + 2 * 0.25,20 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(40 + 2 * 0.25, 17), all_wall.point(41, 17), all_wall.point(41 ,22), all_wall.point(40 + 2 * 0.25,22)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(44 + 2 * 0.25, 15 - 2 * 0.25), all_wall.point(45,15 - 2 * 0.25), all_wall.point(45, 20 - 2 * 0.25), all_wall.point(44 + 2 * 0.25, 20 - 2 * 0.25)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(48 + 2 * 0.25, 17), all_wall.point(49,17), all_wall.point(49, 22), all_wall.point(48 + 2 * 0.25,22)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(64 + 2 * 0.25, 17), all_wall.point(65,17), all_wall.point(65,22), all_wall.point(64 + 2 * 0.25,22)]
				group: groups.wall
			}
			
			PolygonCollider
			{
				vertexes: [all_wall.point(68 + 2 * 0.25, 15 - 2 * 0.25), all_wall.point(69, 15 - 2 * 0.25), all_wall.point(69 , 20 - 2 *0.25), all_wall.point(68 + 2 * 0.25, 20 - 2 * 0.25)]
				group: groups.wall
			}
			
			
			PolygonCollider
			{
				vertexes: [all_wall.point(72 + 2 * 0.25, 17), all_wall.point(73,17), all_wall.point(73,22), all_wall.point(72 + 2 * 0.25,22)]
				group: groups.wall
			}
			
		}
		
		Sensor
		{
			id: exit
			type: Body.STATIC
			position.x : 0
			position.y : 0
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie()
				}
			}
			PolygonCollider
			{
				vertexes: [
				exit.coords(76.5, 14.75),
				exit.coords(90.25 , 14.75),
				exit.coords(90.25 , 22.25),
				exit.coords(76.5 , 22.25)
				]
				group:  groups.sensor
				sensor : true
			}
		}
	}
	Q.Component
	{
		id: turret_factory
		Turret
		{
		
		}
	}
	
	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState:  state_test_1 // state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_dialogue_0  //state_test_1 // 
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					music.objectName = "chapter-6/inside"
					camera_position = undefined
					for(var i = 0; i< tab_turret_corridor_1.length; i++)
					{
						turret_factory.createObject(null, {scene: ship.scene, position: Qt.point(tab_turret_corridor_1[i][0] * 2 * root.chapter_scale, tab_turret_corridor_1[i][1] * 2 * root.chapter_scale), weapon_choice: tab_turret_corridor_1[i][2], angle: tab_turret_corridor_1[i][3] * Math.PI, activate: true, invulnerable : tab_turret_corridor_1[i][4] ,cheat : tab_turret_corridor_1[i][5]})
					}
					messages.add("nectaire/normal", qsTr("begin 1"))
					messages.add("lycop/normal", qsTr("begin 2"))
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_1; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_end  
				signal: sortie
				onTriggered:
				{
					saved_game.add_science(1)
					saved_game.set("file", "game/chapter-7/chapter-7-boss.qml")
					saved_game.save()
				}
			}
		}
		
		
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	
	Q.Component.onCompleted:
	{
		var a2 = ["c", "e", "n", "ne", "nei", "nw", "nwi", "s", "se", "sei", "sw", "swi", "w"]; for(var i2 in a2) jobs.run(scene_view, "preload", ["chapter-2/metal/" + a2[i2]])
		jobs.run(music, "preload", ["chapter-6/inside"])
		jobs.run(scene_view, "preload", ["chapter-2/square-turret"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		groups.init(scene)
		//root.info_print = screen_info_factory.createObject(screen_hud)
		//root.finalize = function() {if(info_print) info_print.destroy()}
	}
}