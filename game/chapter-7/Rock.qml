import aw.game 0.0

Body
{
	id: rock
	function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de drone en 1024*1024
	property color mask: "white"
	scale : 3
	property int ind_rock: random_index(3)
	property bool on: false
	property var tab_rock: [	[coords(45,632),coords(512,52),coords(727,64),coords(931,313),coords(928,495),coords(571,960),coords(376,988),coords(89,814)],
	[coords(34,673),coords(165,402),coords(714,162),coords(868,202),coords(1021,539),coords(952,696),coords(394,934),coords(99,851)],
	[coords(53,586),coords(168,256),coords(411,176),coords(966,471),coords(1010,723),coords(771,1000),coords(390,898)]
	
	]
	Image
	{
		material: "asteroid/" + rock.ind_rock
		mask : rock.mask
	}
	/*CircleCollider
	{
		group: groups.rock
	}*/
	PolygonCollider
	{
		vertexes: rock.tab_rock[rock.ind_rock]
		group: groups.rock
	}
	function begin(bullet, p)
	{
		var a = bullet.damages * .001
		p = point_from_scene(p)
		var v = direction_to_scene(p)
		velocity.x -= v.x * a
		velocity.y -= v.y * a
		var v = direction_from_scene(bullet.velocity)
		angular_velocity += (p.x * v.y - p.y * v.x) / v.norm() * a * .5
		bullet.deleteLater()
	}
}
