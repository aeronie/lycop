#include <QtQml>

#include "Body.h"
#include "Collider.h"
#include "Scene.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

static void destroy_body(b2Body *body)
{
	Q_ASSERT(body);
	Q_ASSERT(body->GetWorld());
	body->GetWorld()->DestroyBody(body);
}

}

Body::Body()
: body_(0, destroy_body)
{
	Observer<0>::connect(signal_scene_changed());
	Observer<-1>::connect(signal_ready());
}

/*

Les Collider doivent être détruits avant ce Body.
Quand un Collider est détruit, le b2Fixture et les b2Contact correspondant sont détruits aussi, et b2ContactListener::EndContact est appelé.
Pour gérer cette collision on peut avoir besoin du Body, donc il ne doit pas être détruit avant, donc les Collider doivent être détruits avant ce Body.
Les autres enfants n'ont pas besoin d'être détruits avant ce Body, et utiliser delete est dangereux s'ils ont des messages en attente, donc on ne détruit que les Collider.
J'espère que les Collider n'auront jamais de message en attente et que les autres enfants n'auront jamais besoin d'être détruits avant ce Body.

*/
bool Body::event(QEvent *event)
{
	if(event->type() == QEvent::DeferredDelete)
	{
		if(loot_factory_)
		{
			QQmlContext *context = new QQmlContext(qmlContext(scene()));
			Q_ASSERT(context);
			Q_ASSERT(context->isValid());
			context->setContextProperty("body", this);
			QObject *loot = loot_factory_->beginCreate(context);
			Q_ASSERT(loot);
			loot->setParent(parent());
			context->setParent(loot);
			loot_factory_->completeCreate();
		}
		auto const a = children();
		for(QObject *child: a)
		{
			if(dynamic_cast<Collider *>(child))
			{
				delete child;
			}
		}
	}

	return Object::event(event);
};

void Body::update(const Observer<-1>::Tag &)
{
	if(scene() && scene()->world_ && qml_initialized_ && !body_)
	{
		b2BodyDef def;
		def.position = position();
		def.angle = angle();
		def.type = b2BodyType(type());
		def.linearVelocity.Set(velocity().x, velocity().y);
		def.angularVelocity = angular_velocity();
		def.linearDamping = damping();
		def.angularDamping = angular_damping();
		def.userData = this;
		body_.reset(scene()->world_->CreateBody(&def));
		Q_ASSERT(body_);
		masse_=body_->GetMass();
		signal_ready()->notify();
	}
}

void Body::update(const Observer<-2>::Tag &)
{
	Observer<-1>::update();
}

void Body::update(const Observer<0>::Tag &)
{
	Observer<-2>::connect(scene() ? scene()->signal_ready() : 0);
}

Scene *Body::scene() const
{
	Q_ASSERT(!parent() || dynamic_cast<Scene *>(parent()));
	return static_cast<Scene *>(parent());
}

void Body::set_scene(Scene *const &scene)
{
	if(parent() == scene) return;
	setParent(scene);
	Q_EMIT scene_changed(scene);
}

b2Vec2 Body::position() const
{
	return body_ ? body_->GetPosition() : position_;
}

void Body::set_position(b2Vec2 const &position)
{
	if(this->position() == position) return;
	position_ = position;
	if(body_) body_->SetTransform(position_, body_->GetAngle());
	Q_EMIT position_changed(position_);
}

float Body::angle() const
{
	return body_ ? body_->GetAngle() : angle_;
}

void Body::set_angle(float const &angle)
{
	if(this->angle() == angle) return;
	angle_ = angle;
	if(body_) body_->SetTransform(body_->GetPosition(), angle_);
	Q_EMIT angle_changed(angle_);
}

b2Vec2 Body::velocity() const
{
	return body_ ? body_->GetLinearVelocity() : velocity_;
}

void Body::set_velocity(b2Vec2 const &velocity)
{
	if(this->velocity() == velocity) return;
	velocity_ = velocity;
	if(body_) body_->SetLinearVelocity(velocity_);
	Q_EMIT velocity_changed(velocity_);
}

float Body::angular_velocity() const
{
	return body_ ? body_->GetAngularVelocity() : angular_velocity_;
}

void Body::set_angular_velocity(float const &velocity)
{
	if(this->angular_velocity() == velocity) return;
	angular_velocity_ = velocity;
	if(body_) body_->SetAngularVelocity(angular_velocity_);
	Q_EMIT angular_velocity_changed(angular_velocity_);
}

void Body::set_type(Type const &type)
{
	if(type_ == type) return;
	type_ = type;
	if(body_) body_->SetType(b2BodyType(type_));
	Q_EMIT type_changed(type_);
}

void Body::set_damping(float const &damping)
{
	if(damping_ == damping) return;
	damping_ = damping;
	if(body_) body_->SetLinearDamping(damping_);
	Q_EMIT damping_changed(damping_);
}

void Body::set_angular_damping(float const &damping)
{
	if(angular_damping_ == damping) return;
	angular_damping_ = damping;
	if(body_) body_->SetAngularDamping(angular_damping_);
	Q_EMIT angular_damping_changed(angular_damping_);
}

void Body::set_images(ArrayView<Image *> const &images)
{
	images_ = images;
	Q_EMIT images_changed(images_);
	Q_EMIT position_changed(position());
	Q_EMIT angle_changed(angle());
}

b2Vec2 Body::point_from_scene(b2Vec2 p) const
{
	p = body_->GetLocalPoint(p);
	p *= 1 / scale();
	return p;
}

b2Vec2 Body::point_to_scene(b2Vec2 p) const
{
	p *= scale();
	p = body_->GetWorldPoint(p);
	return p;
}

b2Vec2 Body::direction_from_scene(b2Vec2 p) const
{
	p = body_->GetLocalVector(p);
	p *= 1 / scale();
	return p;
}

b2Vec2 Body::direction_to_scene(b2Vec2 p) const
{
	p *= scale();
	p = body_->GetWorldVector(p);
	return p;
}

AW_DEFINE_OBJECT_STUB(Body)
AW_DEFINE_PROPERTY_STORED(Body, scale)
AW_DEFINE_PROPERTY_STORED(Body, icon)
AW_DEFINE_PROPERTY_STORED(Body, icon_color)
AW_DEFINE_PROPERTY_STORED(Body, weapon_regeneration)
AW_DEFINE_PROPERTY_STORED(Body, loot_factory)
AW_DEFINE_PROPERTY_STORED(Body, masse)

}
}
