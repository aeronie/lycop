import QtQuick 2.0 as Q
import aw.game 0.0

Weapon
{
	property int level: 1
	property int bullet_group: groups.enemy_laser_beam
	range_bullet: scene.seconds_to_frames(5)
	period: scene.seconds_to_frames([0, 5, 1/0.26][level])
	health: max_health
	max_health: 300 +~~body.health_boost
	image: Image
	{
		material: "equipments/vortex-gun"
		position.y: -0.4
		mask: mask_equipment(parent)
	}
	readonly property Sound sound_shooting: Sound
	{
		objectName: "equipments/vortex-gun/explosion"
		scale: slot.sound_scale
	}
	bullet_factory: Bullet
	{
		id: bullet
		property var joints: []
		readonly property int max_range: weapon.range_bullet
		scale: 1.5 * Math.min(1, (max_range - range) / scene.seconds_to_frames(0.5))
		onScaleChanged: collider.update_transform()
		range: max_range
		damages: 300 * scene.time_step
		angular_velocity: -5
		type: Body.KINEMATIC
		Image
		{
			material: "equipments/vortex-gun/" + (Math.floor(scene.time / 2) % 5)
			mask: Qt.rgba(1, 1, 1, Math.min(1, range / scene.seconds_to_frames(1)))
			scale: 2
			z: altitudes.bullet
		}
		CircleCollider
		{
			id: collider
			group: weapon.bullet_group
			sensor: true
		}
		Q.Component
		{
			id: joint_factory
			RopeJoint
			{
				body_1: bullet
				length: 2
				collide: true
			}
		}
		Q.Component.onCompleted:
		{
			position = weapon.slot.point_to_scene(Qt.point(0, -1.4))
			angle = weapon.slot.angle_to_scene(0)
			weapon.sound_shooting.play()
			weapon.set_bullet_velocity(this, 5)
		}
		Q.Component.onDestruction:
		{
			for(var i in joints) joints[i].destroy()
		}
		function begin(target)
		{
			var e = scene.time_changed
			function f() {joints.push(joint_factory.createObject(null, {body_2: target})); e.disconnect(f)}
			e.connect(f)
			target.add_dot(damages)
		}
		function end(target)
		{
			target.add_dot(-damages)
			for(var i = joints.length; i--;)
			{
				if(joints[i].body_2 == target)
				{
					joints[i].deleteLater()
					joints.splice(i, 1)
				}
			}
		}
		onRangeChanged:
		{
			for(var i = joints.length; i--;)
			{
				var p1 = joints[i].body_1.position
				var p2 = joints[i].body_2.position
				var x = p1.x - p2.x
				var y = p1.y - p2.y
				if(x * x + y * y > 10)
				{
					joints[i].deleteLater()
					joints.splice(i, 1)
				}
			}
		}
	}
}
