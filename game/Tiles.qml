import QtQuick 2.0 as Q
import aw.game 0.0

Sensor
{
	id: root
	property real z
	property color mask
	property var tiles
	type: Body.STATIC
	Repeater
	{
		count: tiles.length
		Q.Component
		{
			Image
			{
				position.x: 2 * tiles[index][0]
				position.y: 2 * tiles[index][1]
				material: tiles[index][2]
				mask: root.mask
				z: root.z
			}
		}
	}
}
