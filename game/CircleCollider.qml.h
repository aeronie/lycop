#include <Box2D.h>

#include "Collider.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct CircleCollider
: Collider
{
	AW_DECLARE_OBJECT_STUB(CircleCollider)
	AW_DECLARE_PROPERTY_STORED(b2Vec2, position) = b2Vec2_zero;

	void initialize() override
	{
		b2CircleShape shape;
		shape.m_p = transform_point(position());
		Collider::initialize(&shape);
	}
};

void CircleCollider::set_position(b2Vec2 const &position)
{
	if(position_ == position) return;
	position_ = position;
	if(fixture_) static_cast<b2CircleShape *>(fixture_->GetShape())->m_p = transform_point(position_);
	Q_EMIT position_changed(position_);
}

AW_DEFINE_OBJECT_STUB(CircleCollider)

}
}
}
