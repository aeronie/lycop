import QtQuick 2.0 as Q
import aw.game 0.0
import "."

Q.QtObject
{
	property alias hull: hull
	property alias nanomachines: nanomachines
	property alias shield_generator: shield_generator
	property alias plasma_spray: plasma_spray
	property alias vortex_gun: vortex_gun
	property alias qui_poutre: qui_poutre
	function get(x) {for(var i = 0; i < data.length; ++i) if(data[i].objectName == x) return data[i]}
	property list<Technology> data:
	[
		Technology
		{
			id: hull
			objectName: "hull"
			title: qsTr("hull")
			description: _title() + qsTr("hull 0") + _level(1, _health_boost(50)) + _level(2, _health_boost(100))
			x: 0
			y: 0
		},
		Technology
		{
			id: nanomachines
			objectName: "nanomachines"
			title: qsTr("nanomachines")
			description: _title() + qsTr("nanomachines 0") + _level(1, _health_restoration(25)) + _level(2, _health_restoration(50))
			enabled: hull.level
			x: 0
			y: 1
		},
		Technology
		{
			id: drone_launcher
			objectName: "drone-launcher"
			title: qsTr("drone-launcher")
			enabled: nanomachines.level
			description: _title() + qsTr("drone-launcher 0") + _level(1, _cost() + _health(200) + qsTr("drone-launcher 1") + _matter_used(100)) + _level(2, qsTr("drone-launcher 2"))
			control: 1
			x: 0
			y: 2
			cost: 300
			factory: DroneLauncher
			{
				level: drone_launcher.level
				target_groups: groups.targets
				naked_hull_group: groups.hull_naked
				tab_group: [groups.bullet, groups.bullet, groups.missile, groups.laser]
				bullet_group: tab_group[level]
				consumable: Consumable
				{
					matter: 100
					available: saved_game.matter >= matter && drone_count < 5
				}
			}
		},
		Technology
		{
			id: gyroscope
			objectName: "gyroscope"
			title: qsTr("gyroscope")
			description: _title() + qsTr("gyroscope 0") + _level(1, _cost() + _health(200)) + _level(2, qsTr("gyroscope 2"))
			control: 0
			x: 1
			y: 0
			cost: 200
			factory: Gyroscope
			{
				level: gyroscope.level
			}
		},
		Technology
		{
			id: dash
			objectName: "dash"
			title: qsTr("dash")
			enabled: gyroscope.level
			description: _title() + qsTr("dash 0") + _level(1, _cost() + _health(200)) + _level(2, qsTr("dash 2"))
			control: 0
			x: 1
			y: 1
			cost: 200
			factory: Dash
			{
				level: dash.level
			}
		},
		Technology
		{
			id: rocket_launcher
			objectName: "rocket-launcher"
			title: qsTr("rocket-launcher")
			description: _title() + qsTr("rocket-launcher 0") + _level(1, _cost() + _health(250) + _damages(200) + _frequency(4) + _matter_used(10)) + _level(2, _frequency(4.4) + _matter_used(9))
			enabled: gun.level
			x: 2
			y: 1
			cost: 400
			factory: RocketLauncher
			{
				bullet_group: groups.roquette
				level: rocket_launcher.level
				consumable: Consumable {matter: [0, 10, 9][level]}
			}
		},
		Technology
		{
			id: missile_launcher
			objectName: "missile-launcher"
			title: qsTr("missile-launcher")
			description: _title() + qsTr("missile-launcher 0") + _level(1, _cost() + _health(250) + _damages(200) + _frequency(2) + _matter_used(20)) + _level(2, _frequency(2.3) + _matter_used(17))
			enabled: rocket_launcher.level
			x: 2
			y: 2
			cost: 400
			factory: MissileLauncher
			{
				target_groups: groups.targets
				bullet_group: groups.missile
				level: missile_launcher.level
				consumable: Consumable {matter: [0, 20, 17][level]}
			}
		},
		Technology
		{
			id: gun
			objectName: "gun"
			title: qsTr("gun")
			description: _title() + qsTr("gun 0") + _level(1, _cost() + _health(100) + _damages(50) + _frequency(5)) + _level(2, _frequency(5.5))
			x: 3
			y: 0
			cost: 100
			factory: Gun
			{
				bullet_group: groups.bullet
				level: gun.level
			}
		},
		Technology
		{
			id: triple_gun
			objectName: "triple-gun"
			title: qsTr("triple-gun")
			description: _title() + qsTr("triple-gun 0") + _level(1, _cost() + _health(150) + _damages(150) + _frequency(2)) + _level(2, _frequency(2.4))
			enabled: gun.level
			x: 3
			y: 1
			cost: 100
			factory: TripleGun
			{
				bullet_group: groups.bullet
				level: triple_gun.level
			}
		},
		Technology
		{
			id: coil_gun
			objectName: "coil-gun"
			title: qsTr("coil-gun")
			description: _title() + qsTr("coil-gun 0") + _level(1, _cost() + _health(200) + _damages(100) + _frequency(3)) + _level(2, _frequency(3.9))
			enabled: triple_gun.level
			x: 3
			y: 2
			cost: 200
			factory: CoilGun
			{
				bullet_group: groups.bullet
				level: coil_gun.level
			}
		},
		Technology
		{
			id: electric_gun
			objectName: "electric-gun"
			title: qsTr("electric-gun")
			description: _title() + qsTr("electric-gun 0") + _level(1, _cost() + _health(200) + _damages(200) + _frequency(4)) + _level(2, _frequency(5.6))
			enabled: coil_gun.level
			x: 3
			y: 3
			cost: 200
			factory: ElectricGun
			{
				bullet_group: groups.bullet_only_shield
				level: electric_gun.level
			}
		},
		Technology
		{
			id: mine_launcher
			objectName: "mine-launcher"
			title: qsTr("mine-launcher")
			enabled: laser_gun.level
			description: _title() + qsTr("mine-launcher 0") + _level(1, _cost() + _health(250) + _damages(800) + _frequency(2) + _time_activation(3) + _matter_used(20)) + _level(2, _frequency(2.2) + _matter_used(18))
			x: 4
			y: 1
			cost: 400
			factory: MineLauncher
			{
				level: mine_launcher.level
				consumable: Consumable {matter: [20,20,18][level]}
			}
		},
		Technology
		{
			id: acid_gun
			objectName: "acid-gun"
			title: qsTr("acid-gun")
			description: _title() + qsTr("acid-gun 0") + _level(1, _cost() + _health(100) + _frequency(2)) + _level(2, _frequency(2.3))
			enabled: heavy_laser_gun.level
			x: 4
			y: 2
			cost: 200
			factory: AcidGun
			{
				bullet_group: groups.bullet_acid
				level: acid_gun.level
			}
		},
		Technology
		{
			id: random_spray
			objectName: "random-spray"
			title: qsTr("random-spray")
			enabled: electric_gun.level && acid_gun.level
			description: _title() + qsTr("random-spray 0") + _level(1, _cost() + _health(150) + _frequency(5)) + _level(2, _frequency(7))
			x: 4
			y: 3
			cost: 300
			factory: RandomSpray
			{
				level: random_spray.level
				enemy: 0
			}
		},
		Technology
		{
			id: plasma_spray
			objectName: "plasma-spray"
			title: qsTr("plasma-spray")
			enabled: random_spray.level
			description: _title() + qsTr("plasma-spray 0") + _level(1, _cost() + _health(250) + qsTr("plasma-spay 1") + _energy_used(50)) + _level(2, _energy_used(37.5))
			x: 4
			y: 4
			cost: 400
			factory: PlasmaSpray
			{
				level: plasma_spray.level
				bullet_group: groups.plasmaspray
				consumable: Consumable {energy: [50,50,37.5][level] * scene.time_step}
			}
		},
		Technology
		{
			id: vortex_gun
			objectName: "vortex-gun"
			title: qsTr("vortex-gun")
			enabled: plasma_spray.level
			description: _title() + qsTr("vortex-gun 0") + _level(1, _cost() + _health(300) + _damages_per_second(300) + _frequency(0.2) + _time(5) + _energy_used_per_use(100)) + _level(2, _frequency(0.26) + _energy_used_per_use(70))
			x: 4
			y: 5
			cost: 600
			factory: VortexGun
			{
				bullet_group: groups.laser_beam
				level: vortex_gun.level
				consumable: Consumable {energy: [100,100,70][level]}
			}
		},
		Technology
		{
			id: qui_poutre
			objectName: "qui-poutre"
			title: qsTr("qui-poutre")
			enabled: vortex_gun.level
			description: _title() + qsTr("qui-poutre 0") + _level(1, _cost() + _health(400) + qsTr("qui-poutre 1") + _frequency(0.14)) + _level(2, _frequency(0.22))
			x: 4
			y: 6
			cost: 600
			onLevelChanged:
			{
				if(level == 1)
				{
					platform.set_bool("qui-poutre-unlock")
				}
			}
			factory: Quipoutre
			{
				level: qui_poutre.level
				bullet_group: groups.bullet
				bullet_group_aoe: groups.dot_qui_poutre
			}
		},
		Technology
		{
			id: laser_gun
			objectName: "laser-gun"
			title: qsTr("laser-gun")
			description: _title() + qsTr("laser-gun 0") + _level(1, _cost() + _health(150) + _damages(100) + _frequency(3)) + _level(2, _frequency(3.3))
			x: 5
			y: 0
			cost: 200
			factory: LaserGun
			{
				bullet_group: groups.laser
				level: laser_gun.level
			}
		},
		Technology
		{
			id: heavy_laser_gun
			objectName: "heavy-laser-gun"
			title: qsTr("heavy-laser-gun")
			description: _title() + qsTr("heavy-laser-gun 0") + _level(1, _cost() + _health(200) + _damages(200) + _frequency(2)) + _level(2, _frequency(2.4))
			enabled: laser_gun.level
			x: 5
			y: 1
			cost: 200
			factory: HeavyLaserGun
			{
				bullet_group: groups.laser
				level: heavy_laser_gun.level
			}
		},
		Technology
		{
			id: maser_emitter
			objectName: "maser-emitter"
			title: qsTr("maser-emitter")
			description: _title() + qsTr("maser-emitter 0") + _level(1, _cost() + _health(300) + _damages_per_second(200) + _energy_used(50)) + _level(2, _damages_per_second(230) + _energy_used(42.5))
			enabled: heavy_laser_gun.level
			x: 5
			y: 2
			cost: 400
			factory: MaserEmitter
			{
				bullet_group: groups.dot
				level: maser_emitter.level
				consumable: Consumable {energy: [50,50,42.5][maser_emitter.level] * scene.time_step}
			}
		},
		Technology
		{
			id: laser_bouncer
			objectName: "laser-bouncer"
			title: qsTr("laser-bouncer")
			enabled: maser_emitter.level
			description: _title() + qsTr("laser-bouncer 0") + _level(1, _cost() + _health(400) + _frequency(2) + _damages(400) + _energy_used_per_use(70)) + _level(2, _frequency(2.4) + _energy_used_per_use(56))
			x: 5
			y: 3
			cost: 400
			factory: LaserBouncer
			{
				level: laser_bouncer.level
				bullet_group: groups.laser
				consumable: Consumable {energy: [0, 70, 56][level]}
			}
		},
		Technology
		{
			id: shield_generator
			objectName: "shield-generator"
			title: qsTr("shield-generator")
			description: _title() + qsTr("shield-generator 0") + _level(1, _cost() + _health(200) + _shield(500) + _shield_restoration(30) + _shield_restoration_time(3)) + _level(2, _health(300) + _shield(700))
			control: 0
			x: 6
			y: 0
			cost: 200
			factory: Shield
			{
				level: shield_generator.level
			}
		},
		Technology
		{
			id: shield_canceler
			objectName: "shield-canceler"
			title: qsTr("shield-canceler")
			enabled: shield_generator.level
			description: _title() + qsTr("shield-canceler 0") + _level(1, _cost() + _health(200) + _energy_used_per_use(50)) + _level(2, qsTr("shield-canceler 2"))
			control: 1
			x: 6
			y: 1
			cost: 400
			factory: ShieldCanceler
			{
				level: shield_canceler.level
				consumable: Consumable {energy: 50}
			}
		},
		Technology
		{
			id: aggressive_shield_generator
			objectName: "aggressive-shield-generator"
			title: qsTr("aggressive-shield-generator")
			enabled: shield_canceler.level
			description: _title() + qsTr("aggressive-shield-generator 0") + _level(1, _cost() + _health(200) + _damages_per_impact(250) + _energy_used(50)) + _level(2, _damages_per_impact(500))
			control: 2
			x: 6
			y: 2
			cost: 400
			factory: AggressiveShield
			{
				level: aggressive_shield_generator.level
				consumable: Consumable {energy: 50 * scene.time_step}
			}
		},
		Technology
		{
			id: secondary_generator
			objectName: "secondary-generator"
			title: qsTr("secondary-generator")
			description: _title() + qsTr("secondary-generator 0") + _level(1, _cost() + _health(200) + _energy_restoration(30)) + _level(2, _energy_restoration(60))
			control: 0
			x: 7
			y: 0
			cost: 200
			factory: Generator
			{
				level: secondary_generator.level
			}
		},
		Technology
		{
			id: secondary_shield_generator
			objectName: "secondary-shield-generator"
			title: qsTr("secondary-shield-generator")
			description: _title() + qsTr("secondary-shield-generator 0") + _level(1, _cost() + _health(200) + _shield(200) + _shield_restoration_time(7)) + _level(2, _shield(300))
			enabled: secondary_generator.level
			control: 0
			x: 7
			y: 1
			cost: 200
			factory: SecondaryShield
			{
				level: secondary_shield_generator.level
			}
		},
		Technology
		{
			id: teleporter
			objectName: "teleporter"
			title: qsTr("teleporter")
			enabled: secondary_shield_generator.level
			description: _title() + qsTr("teleporter 0") + _level(1, _cost() + _health(200) + _energy_used_per_use(50) + _frequency(0.2)) + _level(2, _frequency(0.25))
			control: 1
			x: 7
			y: 2
			cost: 300
			factory: Teleporter
			{
				level: teleporter.level
				consumable: Consumable {energy: 50}
			}
		},
		Technology
		{
			id: secondary_battery
			objectName: "secondary-battery"
			title: qsTr("secondary-battery")
			description: _title() + qsTr("secondary-battery 0") + _level(1, _cost() + _health(200) + _energy(750)) + _level(2, _energy(1500))
			control: 0
			x: 8
			y: 0
			cost: 200
			factory: Battery
			{
				level: secondary_battery.level
			}
		},
		Technology
		{
			id: harpoon_launcher
			objectName: "harpoon-launcher"
			title: qsTr("harpoon-launcher")
			enabled: secondary_battery.level
			description: _title() + qsTr("harpoon-launcher 0") + _level(1, _cost() + _health(200) + _time(5) + _energy_used_per_use(10)) + _level(2, _damages_per_second(200))
			control: 1
			x: 8
			y: 1
			cost: 200
			factory: HarpoonLauncher
			{
				level: harpoon_launcher.level
				consumable: Consumable {energy: 10}
			}
		},
		Technology
		{
			id: absorbing_shield_generator
			objectName: "absorbing-shield-generator"
			title: qsTr("absorbing-shield-generator")
			enabled: harpoon_launcher.level
			description: _title() + qsTr("absorbing-shield-generator 0") + _level(1, _cost() + _health(200) + _shield_restoration_absorbing("40%") + _damages_received_overload("150%")+ _energy_used(50)) + _level(2, _shield_restoration_absorbing("20%") + _damages_received_overload("75%"))
			control: 2
			x: 8
			y: 2
			cost: 400
			factory: AbsorbingShield
			{
				level: absorbing_shield_generator.level
				consumable: Consumable {energy: 50 * scene.time_step}
			}
		},
		Technology
		{
			id: wall_generator
			objectName: "wall-generator"
			title: qsTr("wall-generator")
			enabled: teleporter.level && absorbing_shield_generator.level
			description: _title() + qsTr("wall-generator 0") + _level(1, _cost() + _health(200) + qsTr("wall-generator 1") + _time(5) + _energy_used_per_use(100)) + _level(2, qsTr("wall-generator 2"))
			control: 1
			x: 8
			y: 3
			cost: 600
			factory: WallGenerator
			{
				level: wall_generator.level
				consumable: Consumable
				{
					energy: 100
					available: window.energy >= energy && wall_count < 3
				}
			}
		}
	]
}
