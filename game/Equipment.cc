#include "Equipment.h"
#include "EquipmentSlot.h"
#include "meta.private.h"

namespace aw {
namespace game {

Equipment::~Equipment()
{
	if(slot() && slot()->equipment() == this)
	{
		slot()->set_equipment(0);
	}
}

void Equipment::set_slot(EquipmentSlot *const &slot)
{
	if(slot_ == slot) return;
	slot_ = slot;
	Q_EMIT slot_changed(slot_);
	Q_EMIT body_changed(body());
	Q_EMIT scene_changed(scene());
	slot_changed_.notify();
}

Body *Equipment::body() const
{
	return slot() ? slot()->body() : 0;
}

Scene *Equipment::scene() const
{
	return slot() ? slot()->scene() : 0;
}

AW_DEFINE_OBJECT_STUB(Equipment)
AW_DEFINE_PROPERTY_STORED(Equipment, image)
AW_DEFINE_PROPERTY_STORED(Equipment, health)
AW_DEFINE_PROPERTY_STORED(Equipment, max_health)
AW_DEFINE_PROPERTY_STORED(Equipment, shield)

}
}
