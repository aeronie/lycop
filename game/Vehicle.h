#pragma once

#include "Body.h"

namespace aw {
namespace game {

struct Vehicle
: Body
, Observer<1> // timer
{
	AW_DECLARE_OBJECT_STUB(Vehicle)
	AW_DECLARE_PROPERTY_STORED(float, max_speed) = 1; ///< m s⁻¹
	AW_DECLARE_PROPERTY_STORED(float, max_angular_speed) = 1; ///< rad s⁻¹
	AW_DECLARE_PROPERTY_STORED(float, max_force) = 1; ///< kg m s⁻² ÷ kg × s = m s⁻¹
	AW_DECLARE_PROPERTY_STORED(float, max_torque) = 1; ///< kg m² s⁻² ÷ kg m² × s = rad s⁻¹
	AW_DECLARE_PROPERTY_STORED(float, health_regen) = 0;
	AW_DECLARE_PROPERTY_STORED(float, shield) = 0;
	AW_DECLARE_PROPERTY_STORED(float, shield_regeneration_boost) = 1;
	AW_DECLARE_PROPERTY_STORED(float, speed_boost_mul) = 1;
	AW_DECLARE_PROPERTY_STORED_READONLY(float, max_shield) = 0;
	AW_DECLARE_PROPERTY_STORED_READONLY(float, max_secondary_shield) = 0;
	AW_DECLARE_PROPERTY_STORED_READONLY(float, max_energy) = 0; ///< J
	AW_DECLARE_PROPERTY_STORED_READONLY(float, angular_speed_boost) = 1;
	AW_DECLARE_PROPERTY_STORED_READONLY(float, speed_boost) = 1;
	AW_DECLARE_PROPERTY_STORED_READONLY(float, shield_regeneration) = 0; ///< PV s⁻¹
	AW_DECLARE_PROPERTY_STORED_READONLY(float, energy_regeneration) = 0; ///< J s⁻¹
	AW_DECLARE_PROPERTY_STORED(b2Vec2, target_velocity) = b2Vec2_zero; ///< ∈ [0, 1]
	AW_DECLARE_PROPERTY_STORED(float, target_angular_velocity) = 0; ///< ∈ [0, 1]
	AW_DECLARE_PROPERTY_STORED_READONLY(int, time_before_shield_regeneration) = 0;
	AW_DECLARE_PROPERTY_STORED_READONLY(int, time_before_secondary_shield_regeneration) = 0;
	AW_DECLARE_PROPERTY_READONLY(int, max_time_before_shield_regeneration);
	AW_DECLARE_PROPERTY_READONLY(int, max_time_before_secondary_shield_regeneration);
	AW_DECLARE_PROPERTY_STORED_READONLY(float, aggressive_shield_active_damages) = 0; //bonus damages on collision
	AW_DECLARE_PROPERTY_STORED_READONLY(float, absorbtion_damages_multiplier) = 0; // multiplicateur de dommages en cas de bouclier absorbeur (0 pas de bouclier absorbeur
	AW_DECLARE_PROPERTY_STORED_READONLY(float, absorbtion_shield_multiplier) = 0; // multiplicateur d'absorbtion pour le bouclier(0 pas de bouclier absorbeur actif)
	b2Vec2 previous_target_velocity_ = b2Vec2_zero; ///< m s⁻¹
	b2Vec2 relative_velocity_ {NAN, NAN}; ///< ∈ [0, 1]
	float previous_target_angular_velocity_ = 0; ///< rad s⁻¹
	float target_angle_ = NAN; ///< rad
	float dot=0; //dot on ship
	float dot_without_shield=0; //dot on ship ignore primary shield
	float dot_timed=0; //dot qui s'incremente avec le temps
	int dot_timed_duration=0; //durée d'application du dot
	int dot_timed_time_increment=0; //nb de frame de durée ajoutée par frame
	float dot_timed_damages_increment=0; //nb de domage ajoutée par frame

protected:
	Vehicle() = default;
	~Vehicle() = default;
	void update(const Observer<1>::Tag &) override;
	void update(const Observer<-1>::Tag &) override;
public:
	Q_SLOT void set_relative_velocity(b2Vec2);
	Q_SLOT float get_norm_velocity();
	Q_SLOT void set_target_direction(b2Vec2);
	Q_SLOT void negate_secondary_shield();
	Q_SLOT void negate_shield();
	Q_SLOT float damage_shield(float);
	Q_SLOT void damage_global(float, bool);
	Q_SLOT void absorbtion_shield(float);
	Q_SLOT float damage_hull(float, b2Vec2, int);
	Q_SLOT void add_dot(float);
	Q_SLOT void add_dot_without_shield(float);
	Q_SLOT void add_dot_timed(float,int);
	Q_SLOT void collision_ship_hull(b2Vec2, float, b2Vec2,float);
	Q_SLOT void collision_ship_shield(b2Vec2, float, b2Vec2,float);
	Q_SLOT void update_equipments();
};

}
}
