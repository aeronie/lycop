import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-6-conclusion/conclusion.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	readonly property bool secret_tomate: saved_game.self.get("secret_tomate", false)
	readonly property bool secret_commandement: saved_game.self.get("secret_commandement", false)
	readonly property bool secret_apis: saved_game.self.get("secret_apis", false)
	property bool survie: secret_commandement && secret_apis && secret_tomate
	property alias ship: ship
	signal is_visible
	signal jailed
	signal camera_finish
	signal is_open
	signal fin
	signal ship_dead
	signal evacuated
	signal door_close
	property var camera_position: Qt.point(0, 0)
	property var ship_finish: Qt.point(-300, -300)
	property var last_ship_position: Qt.point(0, 0)
	property var info_print
	property var jail
	property real t0: 0
	property real ti: 0
	property bool new_wave: false
	property var drone_inv
	property real deb_spawn: ti
	property real deb_execution: ti
	property bool execution: false
	property int door_open : 0
	property var anim_evacuation
	
	onTiChanged:
	{
		if(!execution)
		{
			deb_execution = ti
		}
		else
		{
			if(deb_execution - ti > 1)
			{
				execution = false
			}
		}
	}
	
	property var tab_weapons: [qui_poutre_factory]
	Q.Component
	{
		id: qui_poutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: execution
			level: 2
		}
	}
	
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}

	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false
		
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		
		Ship
		{
			id: ship	
			angle : - Math.PI / 2
			Q.Component.onDestruction: 
			{
				root.ship_dead()
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}	
	}	

	Q.Component
	{
		id: wave_assault_factory
		Animation
		{
			id: timer
			property var dist:[10, 20]
			property var nombre: [8, 16]
			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(root.new_wave)
				{
					for(var i = 0 ; i < dist.length ; ++i)
					{
						for(var j = 0 ; j< nombre[i]; j++)
						{
							tp_enemy_factory.createObject(null, {parent: scene,  position_enemy:  Qt.point(ship.position.x + dist[i] * Math.cos(j * 2 * Math.PI / nombre[i]), ship.position.y + dist[i] * Math.sin(j * 2 * Math.PI / nombre[i])), position_central: Qt.point(ship.position.x,ship.position.y)})
							root.door_open++
						}
					}
					root.new_wave = false
				}
			}
		}
	}
	Q.Component
	{
		id: tp_enemy_factory
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property var door_up
			property real value: 0
			property real enemy_count: 1
			property var position_enemy
			property var position_central
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property bool appear: false
			property real last_spawn: 7
			property real time_spawn: 2
			property var tab_drone:[drone_police_2,drone_police_3,drone_police_5]

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: 9 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						if(new_enemy)
						{
							var i = random_index(tab_drone.length)
							enemy = tab_drone[i].createObject(null, {scene: scene, position: Qt.point(door.position.x, door.position.y), angle: Math.atan2(position_central.y - door.position.y, position_central.x - door.position.x ) + Math.PI / 2})
							new_enemy = false
							last_spawn = time
							enemy_count--
						}
						else
						{
							if(enemy_count == 0)
							{
								appear = true
							}
							else
							{
								if(last_spawn - time > time_spawn)
								{
									new_enemy = true
								}
							}
						}
					}
					
					if(appear)
					{
						var a = enemy.mask.a + 0.03
						if(a > 1)
						{
							a = 1
							appear = false 
							reapp_end = true
						}
						enemy.mask.a = a
					}
						
					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							timer.alive = false
							root.door_open--
						}
					}
				}
			}
		}
	}
		
	Q.Component
	{
		id: run_away
		Animation
		{
			id: timer
			time: 7
			speed: -1
			onTimeChanged:
			{
				ship.position.x -= 0.3
				if(Math.abs(ship.position.x - root.ship_finish.x) > 50)
				{
					root.fin()
				}
			}
		}
	}
	Q.Component
	{
		id: evacuation
		Animation
		{
			id: timer
			property var door
			property var door_up
			property var door_down
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool disappear: false
			property bool reapp_end: false
			property bool is_tp: false
			property bool pause: true
			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}
			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: ship.position, z: - 0.000001, angle : 0, scale: 4 })
						door_down = space_door_factory.createObject(null, {scene: ship.scene, position: ship.position, z: - 0.000001, angle : 0, scale: 4 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
							disappear = true
							door_up = space_door_factory.createObject(null, {scene: ship.scene, position: ship.position, z: altitudes.shield + 0.0000001, angle : 0, scale: 4, indice : 10, mask: "transparent" })
							door_down.indice = 10
						}
					}
					
					if(disappear)
					{
						var sens = is_tp? -1 : 1
						var a = door_up.mask.a + sens * 0.03
						if(a > 0.45 && !is_tp)
						{
							root.evacuated()
						}
						if(a > 0.5 && !is_tp)
						{
							is_tp = true
							ship.position.x = root.ship_finish.x
							ship.position.y = root.ship_finish.y
							door.position = ship.position
						}
						if(a < 0 && is_tp)
						{
							a = 0
							reapp_end = true
							disappear = false
						}
						door_up.mask.a = a
					}

					//fermeture porte
					if(reapp_end && !pause)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							root.door_close()
							door.alive = false
							timer.alive = false
							root.door_open--
						}
					}					
				}
			}		
		}
	}
	
	Q.Component
	{
		id: space_door_factory
		Body
		{
			id: space_door
			scale: 15
			type: Body.STATIC
			property int indice: 0
			property color mask: "white"
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real z: 0
			Image
			{
				id: image
				material: "chapter-3/door/" + indice
				z: space_door.z
				mask: space_door.mask
			}
		}
	}
	
	Q.Component
	{
		id: jail_factory
		Body
		{
			scale: 1
			property bool alive : ship ? true : false
			onAliveChanged: if(!alive) destroy()  
			position.x: ship ? ship.position.x : 0
			position.y: ship ? ship.position.y : 0
			property real ti_local: root.ti
			property real deb: root.ti
			onTi_localChanged:
			{
				if(deb - root.ti > 1)
				{
					root.jailed()
				}
			}
			
			Q.Component.onCompleted:
			{
				sound_dying.play()
				deb = root.ti
			}
			property Sound sound_dying : Sound
			{
				objectName: "chapter-6-conclusion/activation"
				scale: 5000
			}
			Image
			{
				material: "chapter-6-conclusion/jail"
				z:altitudes.bullet-0.00001
			}
		}
	}
	//drone police
	Q.Component
	{
		id: drone_police_1
		Vehicle
		{
			id: drone
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			icon: visible ? 2 : 0
			property bool shoot: false
			property bool is_shoot: false
			property bool visible: false
			icon_color: "red"
			max_angular_speed: 40
			max_speed: 12
			property real deb: root.ti
			property real time_to_shoot: 0.5
			property real ti_local: root.ti
			property color mask : Qt.rgba(1,1,1,0)
			onTi_localChanged:
			{
				if(shoot)
				{
					if(!is_shoot)
					{
						is_shoot = true
						deb = root.ti
					}
					else
					{
						if(deb - root.ti > time_to_shoot)
						{
							shoot = false
							is_shoot = false
						}
					}
					
				}
				if(visible)
				{
					if(mask.a < 1)
					{
						var a = mask.a + 0.03
						if(a > 1)
						{
							a = 1
							root.is_visible()
						}
						mask.a = a
					}
				}
			}
			loot_factory: Repeater
			{
				count: 1
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/1"
				mask: drone.mask
			}
			EquipmentSlot
			{
				scale: 0.5
				equipment:Weapon
				{
					shooting: drone.is_shoot 
					period: scene.seconds_to_frames(6)
					health: max_health
					max_health: 2000
					image: Image
					{
						material: "equipments/electric-gun"
						position.y: -0.4
						mask: drone.mask
					}
					
					readonly property Sound sound_shooting: Sound
					{
						objectName: "equipments/electric-gun/shooting"
						scale: 500000
					}
					bullet_factory: Body
					{
						id: bullet
						scale: 0.15
						property real ti_local: root.ti
						onTi_localChanged:
						{
							if(bullet.position.x != 0)
							{
								if(Math.abs(bullet.position.x - ship.position.x) < 0.9)
								{
									bullet.deleteLater()
								}
							}
						}
						
						Image
						{
							material: "equipments/electric-gun/bullet"
							z: altitudes.bullet
						}
						
						Q.Component.onDestruction:
						{
							root.jail = jail_factory.createObject(null, {scene: ship.scene})
						}
						Q.Component.onCompleted:
						{
							position = weapon.slot.point_to_scene(Qt.point(0, -1.4))
							angle = weapon.slot.angle_to_scene(0)
							weapon.sound_shooting.play()
							weapon.set_bullet_velocity(this, 20)
						}
					}
				}

			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_police_2
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 128 - 1, y / 128 - 1)} // plink fait des images de drone en 256*256
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 2
			icon: 2
			icon_color: "red"
			scale: 1.5
			max_speed: 0
			property color mask : Qt.rgba(1,1,1,0)
			loot_factory: Repeater
			{
				count: 1
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/2"
				mask : drone.mask
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(74,128)
				Q.Component.onCompleted: 
				{
					equipment = root.tab_weapons[0].createObject(null, {slot: this})
					equipment.image.mask.a = Qt.binding(function(){
								return drone.mask.a
							})
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(182,128)
				Q.Component.onCompleted: 
				{
					equipment = root.tab_weapons[0].createObject(null, {slot: this})
					equipment.image.mask.a = Qt.binding(function(){
								return drone.mask.a
							})
				}
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_police_3
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 128 - 1, y / 128 - 1)} // plink fait des images de drone en 256*256
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 2
			icon: 2
			icon_color: "red"
			scale: 1.5
			max_speed: 0
			property color mask : Qt.rgba(1,1,1,0)
			loot_factory: Repeater
			{
				count: 1
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/3"
				mask: drone.mask
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(128,176)
				Q.Component.onCompleted: 
				{
					equipment = root.tab_weapons[0].createObject(null, {slot: this})
					equipment.image.mask.a = Qt.binding(function(){
								return drone.mask.a
							})
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(74,100)
				Q.Component.onCompleted: 
				{
					equipment = root.tab_weapons[0].createObject(null, {slot: this})
					equipment.image.mask.a = Qt.binding(function(){
								return drone.mask.a
							})
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(182,100)
				Q.Component.onCompleted: 
				{
					equipment = root.tab_weapons[0].createObject(null, {slot: this})
					equipment.image.mask.a = Qt.binding(function(){
								return drone.mask.a
							})
				}
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_police_5
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 2
			icon: 2
			icon_color: "red"
			scale: 2
			max_speed: 0
			property color mask : Qt.rgba(1,1,1,0)
			loot_factory: Repeater
			{
				count: 2
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/5"
				mask: drone.mask
			}
			
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(406,269)
				Q.Component.onCompleted:
				{
					equipment = root.tab_weapons[0].createObject(null, {slot: this})
					equipment.image.mask.a = Qt.binding(function(){
								return drone.mask.a
							})
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(106,269)
				Q.Component.onCompleted:
				{
					equipment = root.tab_weapons[0].createObject(null, {slot: this})
					equipment.image.mask.a = Qt.binding(function(){
								return drone.mask.a
							})
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(200,310)
				Q.Component.onCompleted:
				{
					equipment = root.tab_weapons[0].createObject(null, {slot: this})
					equipment.image.mask.a = Qt.binding(function(){
								return drone.mask.a
							})
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(312,310)
				Q.Component.onCompleted: 
				{
					equipment = root.tab_weapons[0].createObject(null, {slot: this})
					equipment.image.mask.a = Qt.binding(function(){
								return drone.mask.a
							})
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(256, 411)
				Q.Component.onCompleted: 
				{
					equipment = root.tab_weapons[0].createObject(null, {slot: this})
					equipment.image.mask.a = Qt.binding(function(){
								return drone.mask.a
							})
				}
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	
	Q.Component
	{
		id: drone_apis_factory
		Vehicle
		{
			id: drone_apis
			max_speed: 0
			max_angular_speed: 6
			property var zone
			icon: 2
			icon_color: "blue"
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()

			Image
			{
				material: "folks/apis/4"
			}

		}
	}
	
	Q.Component
	{
		id: move_camera_to_hideleo
		Animation
		{
			id: timer
			property var hideleo_position
			property real chemin_x
			property real chemin_y
			property real vitesse_max: 1.5
			property real vitesse_min: 0.1
			time: 7
			speed: -1
			onTimeChanged:
			{
				var vitesse_max_x = vitesse_max
				var vitesse_max_y = vitesse_max * Math.abs(chemin_y / chemin_x)
				var vitesse_min_x = vitesse_min
				var vitesse_min_y = vitesse_min * Math.abs(chemin_y / chemin_x)
				var vitesse_x = vitesse_max_x
				var vitesse_y = vitesse_max_y
				var position_x = hideleo_position.x - root.camera_position.x
				var position_y = hideleo_position.y - root.camera_position.y

				if(Math.abs(position_x)  > Math.abs(chemin_x * 9 / 10))
				{
					vitesse_x = vitesse_min_x + (vitesse_max_x - vitesse_min_x) * (1 - (Math.abs(position_x - chemin_x * 9 / 10) / (Math.abs(chemin_x) / 10)))
				}
				if(Math.abs(position_y)  > Math.abs(chemin_y * 9 / 10))
				{
					vitesse_y = vitesse_min_y + (vitesse_max_y - vitesse_min_y) * (1 - Math.abs(position_y - chemin_y * 9 / 10) / Math.abs(chemin_y/10))
				}

				if(Math.abs(position_x)  < Math.abs(chemin_x / 10))
				{
					vitesse_x = vitesse_min_x + (vitesse_max_x - vitesse_min_x) * (1 - Math.abs(position_x - chemin_x / 10) / Math.abs(chemin_x / 10))
				}
				if(Math.abs(position_y)  < Math.abs(chemin_y / 10))
				{
					vitesse_y = vitesse_min_y + (vitesse_max_y - vitesse_min_y) * (1 - Math.abs(position_y - chemin_y / 10) / Math.abs(chemin_y / 10))
				}
				if(Math.abs(position_x) < 0.03)
				{
					vitesse_x = 0
				}
				if(Math.abs(position_y) < 0.03)
				{
					vitesse_y = 0
				}

				root.camera_position = Qt.point(root.camera_position.x + vitesse_x * (position_x > 0 ? 1 : -1), root.camera_position.y + vitesse_y * (position_y > 0 ? 1 : -1))

				if(vitesse_x == 0 && vitesse_y ==0 )
				{
					timer.deleteLater()
					root.camera_finish()
				}
			}
		}
	}
	StateMachine
	{
		begin:   state_story_0 // state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState:  state_test_1 // state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					//camera_position= undefined
					//wave_assault_factory.createObject(null, {parent: root.scene})
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
		}
		
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: state_story_0.propertiesAssigned
				onTriggered: 
				{
					
					music.objectName = "chapter-1/main"
					drone_inv = drone_police_1.createObject(null, {scene: ship.scene, position: Qt.point(ship.position.x + 40,ship.position.y), angle : Math.atan2(0, -40) + Math.PI / 2})
					drone_inv.shoot = true
					wave_assault_factory.createObject(null, {parent: root.scene})
					messages.add("nectaire/normal", qsTr("begin 1"))
					messages.add("lycop/normal", qsTr("begin 2"))
					messages.add("nectaire/normal", qsTr("begin 3"))
					if(secret_commandement)
					{
						messages.add("lycop/normal", qsTr("begin 4"))
						messages.add("nectaire/normal", qsTr("begin 5"))
					}
					else
					{
						messages.add("lycop/normal", qsTr("begin bad 4"))
						messages.add("nectaire/normal", qsTr("begin bad 5"))
						
					}
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_1; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: jailed
				onTriggered: 
				{
					music.objectName = "chapter-6-conclusion/bad_ending"
					messages.add("nectaire/normal", qsTr("jail 1"))
					messages.add("lycop/neutral", qsTr("jail 2"))
					move_camera_to_hideleo.createObject(null, {parent: scene, hideleo_position : drone_inv.position, chemin_x: drone_inv.position.x - root.camera_position.x, chemin_y: drone_inv.position.y - root.camera_position.y})
				}
			}
		}
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_2; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_story_2
				signal: camera_finish
				onTriggered: 
				{
					drone_inv.visible = true
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: is_visible
				onTriggered: 
				{
					messages.add("insec/normal", qsTr("spy 1"))
					move_camera_to_hideleo.createObject(null, {parent: scene, hideleo_position : ship.position, chemin_x: ship.position.x - root.camera_position.x, chemin_y: ship.position.y - root.camera_position.y})
				}
			}
		}
		State {id: state_dialogue_2; SignalTransition {targetState: state_story_3; signal: state_dialogue_2.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_3
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_story_3
				signal: camera_finish
				onTriggered: 
				{
					root.new_wave = true
					deb_spawn = ti
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: root.door_openChanged
				guard: root.door_open == 0
				onTriggered: 
				{
					messages.add("insec/normal", qsTr("condamnation 1"))
				}
			}
		}
		
		State 
		{
			id: state_dialogue_3
			SignalTransition 
			{
				targetState: state_story_4
				signal: state_dialogue_3.propertiesAssigned
				guard: !messages.has_unread && !survie
				onTriggered: 
				{
					messages.add("insec/normal", qsTr("execution 1"))
					execution = true
				}
			}
			SignalTransition 
			{
				targetState: state_story_5
				signal: state_dialogue_3.propertiesAssigned
				guard: !messages.has_unread && survie
				onTriggered: 
				{
					music.objectName = "chapter-6-conclusion/transition"
					//animation disparition
					jail.alive = false
					anim_evacuation = evacuation.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_story_4
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			AssignProperty {target: root; property: "collider_trash"; value: false}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: ship_dead
				onTriggered: 
				{
					messages.add("insec/normal", qsTr("end bad 1"))
					messages.add("heter/normal", qsTr("end bad 2"))
					messages.add("insec/normal", qsTr("end bad 3"))
					messages.add("heter/normal", qsTr("end bad 4"))
				}
			}
		}
		State 
		{	
			id: state_dialogue_4 
			SignalTransition 
			{
				targetState: state_end
				signal: state_dialogue_4.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
					saved_game.set("file", "game/chapter-7-credit/credit-bad.qml")
					platform.set_bool("bad-end")
					saved_game.save()
				}
			}
		}
		
		State
		{
			id: state_story_5
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			AssignProperty {target: root; property: "collider_trash"; value: false}
			SignalTransition
			{
				targetState: state_dialogue_5
				signal: evacuated
				onTriggered: 
				{
					messages.add("insec/normal", qsTr("evacuated 1"))
				}
			}
		}
		
		State 
		{	
			id: state_dialogue_5 
			SignalTransition 
			{
				targetState: state_story_6
				signal: state_dialogue_5.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
					execution = true
					drone_apis_factory.createObject(null, {scene: scene, position: Qt.point(ship_finish.x + 5, ship_finish.y + 5), angle: Math.atan2(-5,-5) + Math.PI / 2})
					move_camera_to_hideleo.createObject(null, {parent: scene, hideleo_position : ship_finish, chemin_x: ship_finish.x - root.camera_position.x, chemin_y: ship_finish.y - root.camera_position.y})
				}
			}
		}
		
		State
		{
			id: state_story_6
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			AssignProperty {target: root; property: "collider_trash"; value: false}
			SignalTransition
			{
				targetState: state_story_6
				signal: camera_finish
				onTriggered: 
				{
					anim_evacuation.pause = false
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_6
				signal: door_close
				onTriggered: 
				{
					messages.add("lycop/happy", qsTr("end 1"))
					messages.add("apis/normal", qsTr("end 2"))
					messages.add("lycop/normal", qsTr("end 3"))
					messages.add("apis/normal", qsTr("end 4"))
					messages.add("lycop/normal", qsTr("end 5"))
					messages.add("apis/normal", qsTr("end 6"))
					messages.add("lycop/normal", qsTr("end 7"))
				}
			}
		}
		
		State 
		{	
			id: state_dialogue_6 
			SignalTransition 
			{
				targetState: state_story_7
				signal: state_dialogue_6.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: 
				{
					run_away.createObject(null, {parent: scene})
				}
			}
		}
		
		
		State
		{
			id: state_story_7
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_end
				signal: fin
				onTriggered: 
				{
					saved_game.set("file", "game/chapter-7/chapter-7.qml")
					saved_game.save()
				}
			}
		}
		
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}
	
	
	Q.Component.onCompleted:
	{
		groups.init(scene)
		jobs.run(music, "preload", ["chapter-1/main"])
		jobs.run(music, "preload", ["chapter-6-conclusion/bad_ending"])
		jobs.run(music, "preload", ["chapter-6-conclusion/activation"])
		jobs.run(music, "preload", ["chapter-6-conclusion/transition"])
		root.finalize = function() {if(info_print) info_print.destroy()}
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-3/door/" + i])
		jobs.run(scene_view, "preload", ["chapter-6-conclusion/jail"])
		jobs.run(scene_view, "preload", ["folks/insec/1"])
		jobs.run(scene_view, "preload", ["folks/insec/2"])
		jobs.run(scene_view, "preload", ["folks/insec/3"])
		jobs.run(scene_view, "preload", ["folks/insec/5"])
		
	}
}