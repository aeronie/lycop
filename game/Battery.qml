import aw.game 0.0

Battery
{
	property int level: 1
	health: max_health
	max_health: 200 +~~body.health_boost
	max_energy: [0, 750, 1500][level]
	image: Image
	{
		material: "equipments/secondary-battery"
		scale: 0.5
		mask: mask_equipment(parent)
	}
}
