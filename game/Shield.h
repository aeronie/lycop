#pragma once

#include "Equipment.h"

namespace aw {
namespace game {

struct Shield
: Equipment
{
	AW_DECLARE_OBJECT_STUB(Shield)
	AW_DECLARE_PROPERTY_STORED(float, max_shield) = 0; ///< PV
	AW_DECLARE_PROPERTY_STORED(float, shield_regeneration) = 30; ///< PV s⁻¹
protected:
	Shield() = default;
	~Shield() = default;
};

}
}
