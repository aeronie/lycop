import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-5/boss-zone.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	readonly property bool secret_tomate: saved_game.self.get("secret_tomate", false)
	property alias ship: ship
	signal boss_destroyed
	signal ship_ok
	property var camera_position: Qt.point(0, 0)
	property var last_ship_position: Qt.point(0, 0)
	property var info_print
	property bool new_wave: false
	property bool the_one: false
	property bool apis_alive: true
	property real ti: 7
	property var boss
	property bool level_begin: false
	property bool boss_alive: true
	property int nb_drone: 0
	property int door_open: 0
	property real time_propugnator: 0
	property real time_before_boss: 10 - (time_propugnator - root.ti)
	property var tp_behaviour
	property var wave_spawner
	property var black_hole
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	property var tab_weapons: [coil_gun_factory,laser_gun_factory,laser_bouncer_factory,qui_poutre_factory,vortex_factory]
	Q.Component
	{
		id: laser_gun_factory
		LaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: laser_bouncer_factory
		LaserBouncer
		{
			range_bullet: scene.seconds_to_frames(2)
			level: 1
			shooting: true
		}
	}
	Q.Component
	{
		id: qui_poutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: vortex_factory
		VortexGun
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: coil_gun_factory
		CoilGun
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			level: 2
		}
	}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false

		Animation
		{
			id: timer
			time: 7
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Ship
		{
			id: ship
			position.y: -40
			angle: Math.PI
			Q.Component.onDestruction:
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}

		Vehicle
		{
			id: drone_apis
			max_speed: 15
			max_angular_speed: 6
			property var zone
			icon: 2
			icon_color: "blue"
			position.x: -2
			position.y: 8
			angle: Math.PI / 6
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()
			property bool start: level_begin
			Q.Component.onDestruction:
			{
				apis_alive = false
			}

			Image
			{
				material: "folks/apis/4"
			}

			EquipmentSlot
			{
				position: drone_apis.coords(256, 125)
				scale: 0.15
				equipment: SecondaryShield
				{
					level: 2
					max_shield: 600
				}
			}
			EquipmentSlot
			{
				position: drone_apis.coords(104, 125)
				scale: 0.15
				equipment: SecondaryShield
				{
					level: 2
					max_shield: 600
				}
			}
			EquipmentSlot
			{
				position: drone_apis.coords(408, 125)
				scale: 0.15
				equipment: SecondaryShield
				{
					level: 2
					max_shield: 600
				}
			}
			EquipmentSlot
			{
				position: drone_apis.coords(256, 425)
				scale: 0.15
				equipment: SecondaryShield
				{
					level: 2
					max_shield: 600
				}
			}

			CircleCollider
			{
				group: groups.enemy_hull_naked
			}

			Behaviour_attack_circle
			{
				enabled: drone_apis.start
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 8
			}
		}
	}

	Q.Component
	{
		id: space_door_factory
		Body
		{
			id: space_door
			scale: 15
			type: Body.STATIC
			property int indice: 0
			property color mask: "white"
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real z: 0
			Image
			{
				id: image
				material: "chapter-3/door/" + indice
				z: space_door.z
				mask: space_door.mask
			}
		}
	}
	Q.Component
	{
		id: drone_police_2
		Vehicle
		{
			id: drone
			property bool alive: boss_alive
			function coords(x, y) {return Qt.point(x / 128 - 1, y / 128 - 1)} // plink fait des images de drone en 256*256
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 2
			icon: 2
			icon_color: "red"
			scale: 1.5
			max_speed: 9
			loot_factory: Repeater
			{
				count: 1
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/2"
			}
			Q.Component.onCompleted:
			{
				root.nb_drone++
			}
			Q.Component.onDestruction:
			{
				root.nb_drone--
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(74,128)
				equipment: ShieldCanceler
				{
					range_bullet: 30
					level: 0
					shooting: true
					period: root.scene.seconds_to_frames(8)
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(182,128)
				Q.Component.onCompleted: equipment = root.tab_weapons[2].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_police_3
		Vehicle
		{
			id: drone
			property bool alive: boss_alive
			function coords(x, y) {return Qt.point(x / 128 - 1, y / 128 - 1)} // plink fait des images de drone en 256*256
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 2
			icon: 2
			icon_color: "red"
			scale: 1.5
			max_speed: 9
			loot_factory: Repeater
			{
				count: 1
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/3"
			}
			Q.Component.onCompleted:
			{
				root.nb_drone++
			}
			Q.Component.onDestruction:
			{
				root.nb_drone--
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(128,176)
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(74,100)
				Q.Component.onCompleted: equipment = root.tab_weapons[4].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(182,100)
				Q.Component.onCompleted: equipment = root.tab_weapons[4].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_police_5
		Vehicle
		{
			id: drone
			property bool alive: boss_alive
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 2
			icon: 2
			icon_color: "red"
			scale: 2
			max_speed: 9
			loot_factory: Repeater
			{
				count: 2
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/5"
			}

			Q.Component.onCompleted:
			{
				root.nb_drone++
			}
			Q.Component.onDestruction:
			{
				root.nb_drone--
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(406,269)
				Q.Component.onCompleted: equipment = root.tab_weapons[3].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(106,269)
				Q.Component.onCompleted: equipment = root.tab_weapons[3].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(200,310)
				Q.Component.onCompleted: equipment = root.tab_weapons[4].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(312,310)
				Q.Component.onCompleted: equipment = root.tab_weapons[4].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(256, 411)
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}

	Q.Component
	{
		id: laser_invisible
		Weapon
		{
			property int level: 2
			property int bullet_group: groups.enemy_laser
			period: scene.seconds_to_frames([0, 1 / 3, 1 / 10] [level])
			health: max_health
			max_health: 150 +~~body.health_boost
			range_bullet: scene.seconds_to_frames(2)
			readonly property Sound sound_shooting: Sound
			{
				objectName: "equipments/laser-gun/shooting"
				scale: slot.sound_scale
			}
			bullet_factory: Bullet
			{
				scale: 0.3
				range: weapon.range_bullet
				damages: 100
				Image
				{
					material: "equipments/laser-gun/bullet"
					z: altitudes.bullet
				}
				PolygonCollider
				{
					vertexes: [Qt.point(-0.125, -1), Qt.point(-0.125, 1), Qt.point(0.125, 1), Qt.point(0.125, -1)]
					group: weapon.bullet_group
					sensor: true
				}
				Q.Component.onCompleted:
				{
					position = weapon.slot.point_to_scene(Qt.point(0, -0.5))
					angle = weapon.slot.angle_to_scene(0)
					weapon.sound_shooting.play()
					weapon.set_bullet_velocity(this, 20)
				}
			}
		}
	}

	Q.Component
	{
		id: black_hole_behaviour_factory
		Animation
		{
			id: timer

			time: 7
			property var black_hole_object
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(black_hole_object && ship && root.boss_alive)
				{
					var vect_x = black_hole_object.x - ship.position.x
					var vect_y = black_hole_object.y - ship.position.y
					var dist = vect_x * vect_x + vect_y * vect_y
					var vitesse = dist > 1e-4 ? 1.5 / dist : 1e8
					ship.position.x += vect_x * vitesse
					ship.position.y += vect_y * vitesse
				}
			}
		}
	}

	Q.Component
	{
		id: black_hole_factory
		Bullet
		{
			id: hole
			function pos()
			{
				return Qt.point(hole.position.x, hole.position.y)
			}
			property var black_hole_behaviour
			scale: scale_obj
			property int time: root.boss_alive ? 5 : 10
			property real scale_obj: 3
			property int sens: 1
			range: (root.boss_alive || root.the_one )? scene.seconds_to_frames(time) : scene.seconds_to_frames(1)
			property int im: (range / 10) % 2
			damages: 100000
			angular_velocity: -2
			readonly property Sound sound_boom: Sound
			{
				objectName: "chapter-5/black_hole"
				scale: 50
			}
			onRangeChanged:
			{
				if(range < scene.seconds_to_frames(1))
				{
					scale -= scale_obj / (scene.seconds_to_frames(1)+1)
				}
			}
			onScaleChanged:
			{
				collider.update_transform()
			}
			Image
			{
				material: "chapter-5/black-hole"
				scale: 3
				z: altitudes.background_near
			}
			CircleCollider
			{
				id: collider
				group: root.boss_alive ? groups.enemy_dot : 0
				sensor: true
			}

			Q.Component.onCompleted:
			{
				black_hole_behaviour = black_hole_behaviour_factory.createObject(null, {parent: scene})
				black_hole_behaviour.black_hole_object = Qt.binding(pos)
				sound_boom.play()
			}

			Q.Component.onDestruction:
			{
				black_hole_behaviour.alive = false
			}
		}
	}
	Q.Component
	{
		id: wave_assault_factory
		Animation
		{
			id: timer
			property var tab_pos: [
					[-20,10],
					[-20,-10],
					[20,-10],
					[20,10],
					[0,-30],
					[0,30],
					]

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(root.new_wave)
				{
					for(var i = 0 ; i < tab_pos.length ; ++i)
					{
						root.door_open++
						tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(ship.position.x + tab_pos[i][0], ship.position.y + tab_pos[i][1]), position_central: Qt.point(ship.position.x,ship.position.y)})
					}
					root.new_wave = false
				}
			}
		}
	}
	Q.Component
	{
		id: tp_enemy_factory
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property real value: 0
			property real enemy_count: 2
			property var position_enemy
			property var position_central
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property real last_spawn: 7
			property real time_spawn: 2
			property var tab_drone:[drone_police_2,drone_police_3,drone_police_5]

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: 8 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						if(new_enemy)
						{
							var i = random_index(tab_drone.length)
							enemy = tab_drone[i].createObject(null, {scene: scene, position: Qt.point(door.position.x, door.position.y), angle: Math.atan2(position_central.y - door.position.y, position_central.x - door.position.x )  + Math.PI / 2})
							new_enemy = false
							last_spawn = time
							enemy_count--
						}
						else
						{
							if(enemy_count == 0)
							{
								reapp_end = true
							}
							else
							{
								if(last_spawn - time > time_spawn)
								{
									new_enemy = true
								}
							}
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							timer.alive = false
							root.door_open--
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("Propugnator: %1 secondes").arg(Math.floor(root.time_before_boss))
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}

	Q.Component
	{
		id: tp_behaviour_factory
		Animation
		{
			id: timer
			property var boss: root.boss
			time: 7
			speed: -1
			property var anim_tp
			property bool alive: true
			property real dist_blink: 120
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(timer.boss && ship)
				{
					if(!anim_tp)
					{
						var dist_x = ship.position.x - boss.position.x
						var dist_y = ship.position.y - boss.position.y
						var dist = Math.sqrt(dist_x * dist_x + dist_y * dist_y)
						if(dist > 120)
						{
							anim_tp = tp_ship_factory.createObject(null, {parent: scene})
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: tp_ship_factory
		Animation
		{
			id: timer
			property var boss: root.boss
			property var door
			property var animation_apparition
			property real value:0
			property real sens: 1
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property bool before_tp: true

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				var step = 0.05

				if(door && before_tp)
				{
					door.position.x = ship.position.x
					door.position.y = ship.position.y
					if(animation_apparition)
					{
						animation_apparition.position.x = ship.position.x
						animation_apparition.position.y = ship.position.y
					}
				}

				if(timer.boss)
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door=space_door_factory.createObject(null, {scene: ship.scene, position: Qt.point(ship.position.x, ship.position.y ), z: altitudes.boss - 0.00001, angle: 0, scale: 4 })
						reapp_beg=true
						animation_apparition = space_door_factory.createObject(null, {scene: door.scene, position: Qt.point(door.position.x, door.position.y), z: altitudes.shield + 0.001, angle : door.angle, indice: 10, mask: "transparent" , scale: 4 })
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//disparition / apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						var a = animation_apparition.mask.a
						if(before_tp)
						{
							a = a + step
						}
						else
						{
							a = a - step
						}
						if(a >= 0.5 && before_tp)
						{
							a = 0.5
							before_tp = false
							var angle_tp = Math.random() * 2 * Math.PI

							ship.position.x = boss.position.x + 30 * Math.cos(angle_tp)
							ship.position.y = boss.position.y + 30 * Math.sin(angle_tp)
							door.position.x = ship.position.x
							door.position.y = ship.position.y
							animation_apparition.position.x = ship.position.x
							animation_apparition.position.y = ship.position.y
						}
						if(a <= 0 && !before_tp)
						{
							a = 0
							reapp_end = true
						}
						animation_apparition.mask.a = a
						if(reapp_end)
						{
							animation_apparition.alive = false
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							door.alive = false
							timer.alive = false
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: canon_factory
		Vehicle
		{
			id: canon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool shoot: false
			EquipmentSlot
			{
				id: eq_slot
				equipment: Weapon
				{
					id: weapon
					health: max_health
					max_health: 25000
					shooting: canon.shoot
					period: scene.seconds_to_frames(3)
					readonly property Sound sound_shooting: Sound
					{
						objectName: "equipments/mine-launcher/shooting"
						scale: 40
					}
					bullet_factory: Bullet
					{
						id: bullet
						scale: 2
						range: scene.seconds_to_frames(1)
						damages: 1
						property int ind_bullet: 0
						onRangeChanged:
						{
							bullet.ind_bullet = (bullet.ind_bullet + 1) % 19
						}
						Image
						{
							material: "chapter-5/black-hole-bullet/" + ind_bullet
							z: altitudes.bullet
						}
						CircleCollider
						{
							group: groups.enemy_bullet
							sensor: true
							radius: 0.5
						}
						Q.Component.onCompleted:
						{
							position = weapon.slot.point_to_scene(Qt.point(0, 0))
							angle = weapon.slot.angle_to_scene(0)
							weapon.sound_shooting.play()
							weapon.set_bullet_velocity(this, 30)
						}

						Q.Component.onDestruction:
						{
							root.black_hole = black_hole_factory.createObject(null, {scene: scene, position: Qt.point(bullet.position.x, bullet.position.y), time: root.boss_alive ? 5 : 10})
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: armagedon_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: "transparent"
			scale: 0.01
			property real z: 0.01
			readonly property real damages: 10000
			Q.Component.onCompleted: sound.play()
			onScaleChanged:
			{
				collider.update_transform()
			}
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 5000
			}
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				sensor: true
			}
		}
	}

	Q.Component
	{
		id: boss_transition_end
		Vehicle
		{
			id: boss
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)}
			onAliveChanged: if(!alive) destroy()
			icon: 3
			icon_color: "red"
			scale: 15
			property real time: root.ti
			property color mask: Qt.rgba(1, 0, 0, 1)
			property var coord_canon: [coords(194,132),coords(512,256)]
			property real ti_beg
			property real last_boom
			property var black_hole
			property var canon_left
			property bool swap: true
			property real last_shoot: root.ti
			onTimeChanged:
			{
				//tjs des problemes bizarres d'initialisation de la classe
				if(ti_beg != 0)
				{
					if(root.boss_alive)
					{
						root.boss_alive = false
					}
					var explosion_step = 0.1
					var ajust = swap ? 0.1 : -0.1
					swap= !swap
					boss.position = Qt.point(boss.position.x + ajust, boss.position.y + ajust)
					if(last_boom - time > explosion_step)
					{
						last_boom = time
						var angle_ran = 2 * Math.PI * Math.random()
						var scale_ran = boss.scale * Math.random()
						explosion_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss.position.x + scale_ran * Math.cos(angle_ran), boss.position.y + scale_ran * Math.sin(angle_ran)), scale: boss.scale / 15 * 4})
					}
					if(ti_beg - time > 2)
					{
						root.the_one = true
						if(!black_hole)
						{
							canon_left.shoot = true
							if(last_shoot - root.ti > 1.5)
							{
								black_hole = true
							}
						}
						else
						{
							canon_left.shoot = false
							if(boss && root.black_hole && scale > 1)
							{
								var vect_x = root.black_hole.position.x - boss.position.x
								var vect_y = root.black_hole.position.y - boss.position.y
								var dist = Math.sqrt(vect_x * vect_x + vect_y * vect_y)
								var vitesse = 2 / (dist * dist)
								boss.position.x += vect_x * vitesse
								boss.position.y += vect_y * vitesse
								boss.angle += 1 / (10 * dist)
								if(dist < 30)
								{
									scale = dist / 2
								}
								if(root.black_hole.range > scene.seconds_to_frames(1) && dist < 2)
								{
									root.black_hole.range = scene.seconds_to_frames(1)
								}
							}
							if(scale < 1)
							{
								boss.alive = false
							}
						}
					}
				}
			}
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						mask: "red"
						scale: 8
						sound_scale: 10
					}
				}
			}

			Q.Component.onDestruction:
			{
				if(canon_left)
				{
					canon_left.alive = false
				}
				root.boss_destroyed()
			}

			function canon_left_pos()
			{
				var x = boss.position.x + coord_canon[0].x * scale * Math.cos(boss.angle) - coord_canon[0].y * scale * Math.sin(boss.angle)
				var y = boss.position.y + coord_canon[0].y * scale * Math.cos(boss.angle) + coord_canon[0].x * scale * Math.sin(boss.angle)
				return Qt.point(x,y)
			}
			Q.Component.onCompleted:
			{
				boss.canon_left = canon_factory.createObject(null, {scene: scene, position: canon_left_pos(), shoot: false, angle: boss.angle})
				ti_beg = root.ti
				last_boom = root.ti
				last_shoot = root.ti
			}

			Image
			{
				material: "chapter-5/boss"
				z: altitudes.boss
				mask: boss.mask
			}
		}
	}

	Q.Component
	{
		id: boss_4
		Vehicle
		{
			id: boss
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)}
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 5
			icon: 3
			icon_color: "red"
			type: Body.KINEMATIC
			max_speed: 7
			scale: 15
			property int nb_wave_to_clean: 2
			property int wave: 0
			property real ti_local: root.ti
			property int first: 0
			property real last_spawn: ti_local
			property real time_to_spawn: 4
			property bool shoot: true
			property var canon_left
			property var canon_right
			property real last_shoot: 10
			property real time_to_shoot: 5
			property bool black_hole_right_shoot: false
			property bool black_hole_left_shoot: false
			property var coord_canon: [coords(407,272),coords(1641,272)]
			property real health:
			{
				var x = 0
				if(weapon)
				{
					x = weapon.health / weapon.max_health
				}
				return x
			}

			onHealthChanged:
			{
				if(health < 0.66 && first == 0)
				{
					boss.wave = 0
					first++
				}
				if(health < 0.33 && first == 1)
				{
					boss.wave = 0
					first++
				}
			}

			onTi_localChanged:
			{
				//comprtement des canon black_hole
				if(last_shoot - ti_local > time_to_shoot / 2 && last_shoot - ti_local < time_to_shoot / 2 + time_to_shoot / 10)
				{
					black_hole_right_shoot = true
				}
				else
				{
					if(black_hole_right_shoot)
					{
						black_hole_right_shoot = false
					}
				}

				if(last_shoot - ti_local > time_to_shoot)
				{
					last_shoot = ti_local
					black_hole_left_shoot = true
				}
				else
				{
					if(black_hole_left_shoot && last_shoot - ti_local > time_to_shoot / 10)
					{
						black_hole_left_shoot = false
					}
				}

				//comportement des spawner
				if(wave < 1 && last_spawn - ti_local > time_to_spawn)
				{
					last_spawn = ti_local
					wave++
					root.new_wave = true
				}
			}

			function canon_angle()
			{
				return boss.angle
			}

			function canon_left_shoot()
			{
				return black_hole_left_shoot
			}
			function canon_right_shoot()
			{
				return black_hole_right_shoot
			}

			function canon_left_pos()
			{
				var x = boss.position.x + coord_canon[0].x * scale * Math.cos(boss.angle) - coord_canon[0].y * scale * Math.sin(boss.angle)
				var y = boss.position.y + coord_canon[0].y * scale * Math.cos(boss.angle) + coord_canon[0].x * scale * Math.sin(boss.angle)
				return Qt.point(x,y)
			}
			function canon_right_pos()
			{
				var x = boss.position.x + coord_canon[1].x * scale * Math.cos(boss.angle) - coord_canon[1].y * scale * Math.sin(boss.angle)
				var y = boss.position.y + coord_canon[1].y * scale * Math.cos(boss.angle) + coord_canon[1].x * scale * Math.sin(boss.angle)
				return Qt.point(x,y)
			}

			Q.Component.onCompleted:
			{
				wave = 0
				canon_left = canon_factory.createObject(null, {scene: scene, position: canon_left_pos(), shoot: black_hole_left_shoot})
				canon_left.position = Qt.binding(canon_left_pos)
				canon_left.angle = Qt.binding(canon_angle)
				canon_left.shoot = Qt.binding(canon_left_shoot)

				canon_right = canon_factory.createObject(null, {scene: scene, position: canon_right_pos(), shoot: black_hole_right_shoot})
				canon_right.position = Qt.binding(canon_right_pos)
				canon_right.angle = Qt.binding(canon_angle)
				canon_right.shoot = Qt.binding(canon_right_shoot)
			}
			Q.Component.onDestruction:
			{
				if(canon_left)
				{
					canon_left.alive = false
				}
				if(canon_right)
				{
					canon_right.alive = false
				}
				//changement de phase
				root.boss_alive = false
				root.boss = boss_transition_end.createObject(null, {scene: scene, position: Qt.point(boss.position.x, boss.position.y), angle: boss.angle})
			}

			Image
			{
				material: "chapter-5/boss"
				z: altitudes.boss
				mask:
				{
					var x = 0
					if(weapon)
					{
						x = weapon.health / weapon.max_health
					}
					return Qt.rgba(1, x, x, 1)
				}
			}

			EquipmentSlot
			{
				id: eq_slot
				position: coords(1024,705)
				equipment: Weapon
				{
					id: weapon
					health: max_health
					max_health: 75000
					shooting: boss.shoot
					period: scene.seconds_to_frames(2)
					bullet_factory: Vehicle
					{
						id: drone
						property bool alive: true
						function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
						onAliveChanged: if(!alive) destroy()
						property real damages: 100000
						property real ti_local: root.ti / 1.2
						max_angular_speed: 0
						scale: 1.5
						max_speed: 15
						property bool start: false
						property real ti_deb: 0
						property int ind_star: 0
						onTi_localChanged:
						{
							ind_star = (ind_star + 1)% 10
							if(ti_deb - root.ti < 0.7)
							{
								scale += 1.5 / scene.seconds_to_frames(0.7)
							}
							else
							{
								if(!start)
								{
									start = true
								}
							}
						}

						Sound
						{
							id: sound
							objectName: "chapter-4/boss_canon"
							scale: 40
						}

						function is_started()
						{
							return drone.start
						}
						Image
						{
							material: "chapter-5/star-bullet/" + drone.ind_star
							angle: drone.ti_local
						}

						Q.Component.onCompleted:
						{
							if(root.boss)
							{
								angle = root.boss.angle
							}
							ti_deb = root.ti
							position = weapon.slot.point_to_scene(Qt.point(0, 0))
							sound.play()
						}

						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: 1 / 2 * Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: 3 / 2 * Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						Behaviour_attack_missile
						{
							target_groups: [groups.enemy_checkpoint]
							range: scene.seconds_to_frames(3)
						}
						CircleCollider
						{
							group: groups.enemy_bullet
						}
					}
				}
				onEquipmentChanged: boss.alive = false
			}

			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			PolygonCollider
			{
				vertexes: [coords(16,920),coords(2032,920),coords(1857,1582),coords(1414,1984),coords(634,1984),coords(191,1582)]
				group: shoot ? groups.item_ignore_enemy : 0
			}
			PolygonCollider
			{
				vertexes: [coords(407,261),coords(733,909),coords(16,920)]
				group: shoot ? groups.item_ignore_enemy : 0
			}
			PolygonCollider
			{
				vertexes: [coords(1641,261),coords(1315,909),coords(2032,920)]
				group: shoot ? groups.item_ignore_enemy : 0
			}
			PolygonCollider
			{
				vertexes: [coords(857,920),coords(942,705),coords(1106,705),coords(1191,920)]
				group: shoot ? groups.item_ignore_enemy : 0
			}
		}
	}
	Q.Component
	{
		id: boss_transition_3
		Vehicle
		{
			id: boss
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()
			icon: 3
			icon_color: "red"
			scale: 15
			property real time: root.ti
			property color mask: Qt.rgba(1, 0, 0, 1)
			property color mask_shield: "white"
			property int ind_crack: 0
			property bool explosion: true
			property bool armagedon_ini: true
			property var armagedon
			property real ti_beg
			onTimeChanged:
			{
				//craquelure
				if(ind_crack < 5)
				{
					if(ti_beg - time > 0.4)
					{
						ind_crack++
						ti_beg = time
					}
					if(mask_shield.g > 0)
					{
						var g = mask_shield.g - 0.02
						if(g < 0)
						{
							g = 0
						}
						mask_shield.g = g
					}
				}
				else
				{
					//explosion
					if(explosion)
					{
						if(armagedon_ini)
						{
							armagedon = armagedon_factory.createObject(null, {scene: ship.scene, position: Qt.point(boss.position.x, boss.position.y), mask_c: Qt.rgba(1, 0.1 , 0, 1)})
							armagedon_ini = false
						}
						if(ship)
						{
							if(armagedon.scale < 80)
							{
								armagedon.scale += 30 * ship.scene.time_step
								armagedon.angle += 0.001
							}
							else
							{
								var a = armagedon.mask_c.a - ship.scene.time_step
								if(a<=0)
								{
									a=0
									explosion = false
								}
								armagedon.mask_c.a = a
							}
						}
					}

					//fin
					if(!explosion)
					{
						if(armagedon)
						{
							armagedon.alive = false
						}
						boss.alive= false
					}
				}
			}

			Q.Component.onCompleted:
			{
				ti_beg = root.ti
			}
			Q.Component.onDestruction:
			{
				//changement de phase
				root.boss = boss_4.createObject(null, {scene: boss.scene, position: Qt.point(boss.position.x, boss.position.y), angle : boss.angle, start: true })
			}

			Image
			{
				material: "chapter-4/shield-cracks/" + boss.ind_crack
				mask: boss.mask_shield
				z: altitudes.boss + 0.000000001 + 0.00001
				scale: 1.1
			}

			Image
			{
				material: "equipments/shield"
				mask: boss.mask_shield
				scale: 1.1
				z: altitudes.boss + 0.00001
			}
			Image
			{
				material: "chapter-5/boss"
				z: altitudes.boss
				mask: boss.mask
			}
		}
	}

	Q.Component
	{
		id: boss_3
		Vehicle
		{
			id: boss
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)}
			onAliveChanged: if(!alive) destroy()
			type: Body.KINEMATIC
			max_angular_speed: start ? (dash ? 0.1 : 5) : 0
			icon: 3
			icon_color: "red"
			max_speed: start ? (dash ? (wait ? 0 : 60) : 7): 0
			scale: 15
			property color mask_shield: Qt.rgba(1, 1, 0, 1)
			property real ti_local: root.ti
			property bool start: false
			property bool dash: false
			property bool first: true
			property real ti_beg
			property real ti_beg_dash
			property bool shoot: true
			property bool wait: false
			property bool activate_reflection: true
			property bool transition: false
			property real sens: -1

			onStartChanged:
			{
				ti_beg = root.ti
				ti_beg_dash = root.ti
			}

			onTi_localChanged:
			{
				if(start)
				{
					if(activate_reflection)
					{
						if(ti_beg - ti_local > 3)
						{
							activate_reflection = false
							transition = true
							sens = -1
						}
					}
					if(transition)
					{
						var g = mask_shield.g + sens * 0.005
						if(g > 1)
						{
							g = 1
							activate_reflection = true
							transition = false
							ti_beg = ti_local
						}
						if(g < 0)
						{
							g = 0
							dash = true
							wait = true
							transition = false
							ti_beg_dash = ti_local
						}
						mask_shield.g = g
					}
					if(dash)
					{
						if(wait && ti_beg_dash - ti_local > 1)
						{
							wait = false
						}
						if(ti_beg_dash - ti_local > 3)
						{
							dash = false
							transition = true
							sens = 1
						}
					}
				}
			}

			Q.Component.onCompleted:
			{
			}
			Q.Component.onDestruction:
			{
				//changement de phase
				root.boss = boss_transition_3.createObject(null, {scene: scene, position: Qt.point(boss.position.x, boss.position.y), angle: boss.angle, mask_shield: boss.mask_shield})
				if(root.tp_behaviour)
				{
					root.tp_behaviour.alive = false
				}
			}

			Image
			{
				material: "chapter-5/boss"
				z: altitudes.boss
				mask:
				{
					var x = 0
					if(weapon)
					{
						x = weapon.health / weapon.max_health
					}
					return Qt.rgba(1, x, x, 1)
				}
			}

			Image
			{
				material: "equipments/shield"
				mask: boss.mask_shield
				scale: 1.1
				z: start ? altitudes.shield : altitudes.boss + 0.00001
			}

			EquipmentSlot
			{
				id: eq_slot
				position: coords(1024,705)
				equipment: Weapon
				{
					id: weapon
					health: max_health
					max_health: 50000
					shooting: boss.shoot
					period: scene.seconds_to_frames(2.5)
					bullet_factory: Vehicle
					{
						id: drone
						property bool alive: true
						function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
						onAliveChanged: if(!alive) destroy()
						property real damages: 100000
						property real ti_local: root.ti / 1.2
						angle: Math.PI
						max_angular_speed: 5
						scale: 1.5
						max_speed: 15
						property bool start: false
						property real ti_deb: 0
						property int ind_star: 0
						onTi_localChanged:
						{
							ind_star = (ind_star + 1)% 10
							if(ti_deb - root.ti < 0.7)
							{
								scale += 1.5 / scene.seconds_to_frames(0.7)
							}
							else
							{
								if(!start)
								{
									start = true
								}
							}
						}

						Sound
						{
							id: sound
							objectName: "chapter-4/boss_canon"
							scale: 40
						}

						function is_started()
						{
							return drone.start
						}
						Image
						{
							material: "chapter-5/star-bullet/" + drone.ind_star
							angle: drone.ti_local
						}

						Q.Component.onCompleted:
						{
							if(root.boss)
							{
								angle = root.boss.angle
							}
							ti_deb = root.ti
							position = weapon.slot.point_to_scene(Qt.point(0, 0))
							sound.play()
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: 2 / 3 * Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: 4 / 3 * Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						Behaviour_attack_missile
						{
							target_groups: [groups.enemy_checkpoint]
							range: scene.seconds_to_frames(3)
						}
						CircleCollider
						{
							group: groups.enemy_bullet
						}
					}
				}
				onEquipmentChanged: boss.alive = false
			}

			
			function begin(other, point)
			{
				var v = other.velocity
				var p = point
				var vect_radian = Qt.point(point.x - boss.position.x, point.y - boss.position.y)
				var tempx = vect_radian.x
				var tempy = vect_radian.y
				vect_radian.x = tempx / Math.sqrt(tempx * tempx + tempy * tempy)
				vect_radian.y = tempy / Math.sqrt(tempx * tempx + tempy * tempy)
				var point_dep = Qt.point(p.x - v.x, p.y - v.y)
				var vect_dep = Qt.point(point_dep.x - p.x, point_dep.y - p.y)
				
				// AH = <AC.v>/norm(v) * v
				var prod_proj = (vect_dep.x * vect_radian.x + vect_dep.y * vect_radian.y) / Math.sqrt(vect_radian.x * vect_radian.x + vect_radian.y * vect_radian.y)
				
				var vect_proj = Qt.point(prod_proj * vect_radian.x, prod_proj * vect_radian.y)
				
				var point_proj = Qt.point(p.x + vect_proj.x, p.y + vect_proj.y)
				var vect_ortho = Qt.point(point_proj.x - point_dep.x, point_proj.y - point_dep.y)
				
				var point_final = Qt.point(vect_ortho.x + point_proj.x, vect_ortho.y + point_proj.y)
				var vect_velocity =  Qt.point(point_final.x - p.x, point_final.y - p.y)
				
				other.velocity = Qt.point(vect_velocity.x, vect_velocity.y)
				var a = Math.atan2(vect_velocity.y, vect_velocity.x) + Math.PI / 2
				
				var e = scene.time_changed
				function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
				e.connect(f)
				for(var i in other.children)
				{
					var child = other.children[i]
					if(child.group) child.group ^= 1
					if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
				}
			}

			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}

			PolygonCollider
			{
				vertexes: [coords(16,920),coords(2032,920),coords(1857,1582),coords(1414,1984),coords(634,1984),coords(191,1582)]
				group: boss.activate_reflection ? 0 : groups.item_ignore_enemy
			}
			PolygonCollider
			{
				vertexes: [coords(407,261),coords(733,909),coords(16,920)]
				group: boss.activate_reflection ? 0 : groups.item_ignore_enemy
			}
			PolygonCollider
			{
				vertexes: [coords(1641,261),coords(1315,909),coords(2032,920)]
				group: boss.activate_reflection ? 0 : groups.item_ignore_enemy
			}
			PolygonCollider
			{
				vertexes: [coords(857,920),coords(942,705),coords(1106,705),coords(1191,920)]
				group: boss.activate_reflection ? 0 : groups.item_ignore_enemy
			}
			CircleCollider
			{
				group: boss.activate_reflection ? groups.reflector_only_player : (boss.dash ? groups.enemy_shield_aggressive : 0)
				radius: 1.1
			}
		}
	}

	Q.Component
	{
		id: explosion_factory
		Explosion
		{
			scale: 4
		}
	}

	Q.Component
	{
		id: boss_transition_2
		Vehicle
		{
			id: boss
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()
			icon: 3
			icon_color: "red"
			scale: 15
			property real ti_beg: root.ti
			property real last_boom: root.ti
			property real time: root.ti
			property color mask: Qt.rgba(1, 0, 0, 1)
			property bool earthquake: true
			property bool swap: true
			property bool flee: true
			property bool door_ini: true
			property bool door_beg: false
			property bool door_end: false
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property var door
			property var animation_apparition
			onTimeChanged:
			{
				//tjs des probleme bizarres d'initialisation de la classe
				if(ti_beg != 0)
				{
					var explosion_step = 0.1
					if(earthquake)
					{
						var ajust = swap ? 0.1 : -0.1
						swap= !swap
						boss.position = Qt.point(boss.position.x + ajust, boss.position.y + ajust)
						if(last_boom - time > explosion_step)
						{
							last_boom = time
							var angle_ran = 2 * Math.PI * Math.random()
							var scale_ran = boss.scale * Math.random()
							explosion_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss.position.x + scale_ran * Math.cos(angle_ran), boss.position.y + scale_ran * Math.sin(angle_ran))})
						}
					}
					if(ti_beg - time > 4 && flee)
					{
						earthquake = false

						if(door_ini)
						{
							door_ini = false
							door = space_door_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss.position.x, boss.position.y), z: altitudes.boss - 0.000000001, angle: boss.angle, scale: 50})
							door_beg = true
						}
						//ouverture porte
						if(door_beg)
						{
							door.indice++
							if(door.indice == 10)
							{
								door_beg = false
							}
						}

						//disparition
						if(!door_beg && !door_end && !door_ini)
						{
							var a = boss.mask.a
							a = a - 0.02
							if(a <= 0)
							{
								a = 0
								door_end = true
							}
							boss.mask.a = a
						}

						//fermeture porte
						if(door_end)
						{
							door.indice--
							if(door.indice == 0)
							{
								door_end = false
								door.alive = false
								flee = false
							}
						}
					}
					if(!flee)
					{
						//initialisation
						if(reapp_ini)
						{
							reapp_ini = false
							door=space_door_factory.createObject(null, {scene: ship.scene, position: Qt.point(ship.position.x, ship.position.y - 20), z: altitudes.boss - 0.0000001, angle: 0, scale: 60 })
							reapp_beg=true
						}

						//ouverture de la porte
						if(reapp_beg)
						{
							door.indice++
							if(door.indice == 10)
							{
								reapp_beg = false
								animation_apparition = space_door_factory.createObject(null, {scene: door.scene, position: Qt.point(door.position.x, door.position.y), z: altitudes.boss + 0.01, angle : door.angle, indice: 10, scale: 60 })
								root.boss = boss_3.createObject(null, {scene: door.scene, position: Qt.point(door.position.x, door.position.y), angle: Math.PI})
							}
						}

						//apparition
						if(!reapp_beg && !reapp_end && !reapp_ini)
						{
							var a = animation_apparition.mask.a
							a = a - 0.02
							if(a <= 0)
							{
								a = 0
								reapp_end = true
							}
							animation_apparition.mask.a = a
							if(reapp_end)
							{
								animation_apparition.alive = false
							}
						}

						//fermeture porte
						if(reapp_end)
						{
							door.indice--
							if(door.indice == 0)
							{
								reapp_end = false
								root.boss.start = true
								door.alive = false
								boss.alive = false
							}
						}
					}
				}
			}

			Q.Component.onCompleted:
			{
				boss.last_boom = root.ti
				boss.ti_beg = root.ti
			}
			Q.Component.onDestruction:
			{
				//changement de phase
			}

			Image
			{
				material: "chapter-5/boss"
				z: altitudes.boss
				mask: boss.mask
			}
		}
	}
	Q.Component
	{
		id: boss_2
		Vehicle
		{
			id: boss
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)}
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 5
			type: Body.KINEMATIC
			icon: 3
			icon_color: "red"
			max_speed: 7
			scale: 15
			property var canon_left
			property var canon_right
			property int nb_wave_to_clean: 2
			property int wave: 0
			property real ti_local: root.ti
			property bool start: false
			property bool first: true
			property real last_shoot: 10
			property real time_to_shoot: 5
			property bool shoot: true
			property bool black_hole_right_shoot: false
			property bool black_hole_left_shoot: false
			property var coord_canon: [coords(407,272),coords(1641,272)]

			onTi_localChanged:
			{
				if(last_shoot - ti_local > time_to_shoot / 2 && last_shoot - ti_local < time_to_shoot / 2 + time_to_shoot / 10)
				{
					black_hole_right_shoot = true
				}
				else
				{
					if(black_hole_right_shoot)
					{
						black_hole_right_shoot = false
					}
				}

				if(last_shoot - ti_local > time_to_shoot)
				{
					last_shoot = ti_local
					black_hole_left_shoot = true
				}
				else
				{
					if(black_hole_left_shoot && last_shoot - ti_local > time_to_shoot / 10)
					{
						black_hole_left_shoot = false
					}
				}
			}

			function canon_angle()
			{
				return boss.angle
			}

			function canon_left_shoot()
			{
				return black_hole_left_shoot
			}
			function canon_right_shoot()
			{
				return black_hole_right_shoot
			}

			function canon_left_pos()
			{
				var x = boss.position.x + coord_canon[0].x * scale * Math.cos(boss.angle) - coord_canon[0].y * scale * Math.sin(boss.angle)
				var y = boss.position.y + coord_canon[0].y * scale * Math.cos(boss.angle) + coord_canon[0].x * scale * Math.sin(boss.angle)
				return Qt.point(x,y)
			}
			function canon_right_pos()
			{
				var x = boss.position.x + coord_canon[1].x * scale * Math.cos(boss.angle) - coord_canon[1].y * scale * Math.sin(boss.angle)
				var y = boss.position.y + coord_canon[1].y * scale * Math.cos(boss.angle) + coord_canon[1].x * scale * Math.sin(boss.angle)
				return Qt.point(x,y)
			}

			Q.Component.onCompleted:
			{
				canon_left = canon_factory.createObject(null, {scene: scene, position: canon_left_pos(), shoot: black_hole_left_shoot})
				canon_left.position = Qt.binding(canon_left_pos)
				canon_left.angle = Qt.binding(canon_angle)
				canon_left.shoot = Qt.binding(canon_left_shoot)

				canon_right = canon_factory.createObject(null, {scene: scene, position: canon_right_pos(), shoot: black_hole_right_shoot})
				canon_right.position = Qt.binding(canon_right_pos)
				canon_right.angle = Qt.binding(canon_angle)
				canon_right.shoot = Qt.binding(canon_right_shoot)
			}
			Q.Component.onDestruction:
			{
				if(canon_left)
				{
					canon_left.alive = false
				}
				if(canon_right)
				{
					canon_right.alive = false
				}
				//changement de phase
				root.boss = boss_transition_2.createObject(null, {scene: scene, position: Qt.point(boss.position.x, boss.position.y), angle: boss.angle})
			}

			Image
			{
				material: "chapter-5/boss"
				z: altitudes.boss
				mask:
				{
					var x = 0
					if(weapon)
					{
						x = weapon.health / weapon.max_health
					}
					return Qt.rgba(1, x, x, 1)
				}
			}

			EquipmentSlot
			{
				id: eq_slot
				position: coords(1024,705)
				equipment: Weapon
				{
					id: weapon
					health: max_health
					max_health: 25000
					shooting: boss.shoot
					period: scene.seconds_to_frames(3)
					bullet_factory: Vehicle
					{
						id: drone
						property bool alive: true
						function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
						onAliveChanged: if(!alive) destroy()
						property real damages: 50000
						property real ti_local: root.ti / 1.2
						max_angular_speed: 0
						scale: 1.5
						max_speed: 15
						property bool start: false
						property int ind_star: 0
						property real ti_deb: 0
						onTi_localChanged:
						{
							ind_star = (ind_star + 1)% 10
							if(ti_deb - root.ti < 0.7)
							{
								scale += 1.5 / scene.seconds_to_frames(0.7)
							}
							else
							{
								if(!start)
								{
									start = true
								}
							}
						}

						Sound
						{
							id: sound
							objectName: "chapter-4/boss_canon"
							scale: 40
						}
						function is_started()
						{
							return drone.start
						}
						Image
						{
							material: "chapter-5/star-bullet/" + drone.ind_star
							angle: drone.ti_local
						}

						Q.Component.onCompleted:
						{
							if(root.boss)
							{
								angle = root.boss.angle
							}
							ti_deb = root.ti
							position = weapon.slot.point_to_scene(Qt.point(0, 0))
							sound.play()
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							angle: drone.ti_local
							scale: 0.5
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						EquipmentSlot
						{
							position.x: 0
							position.y: 0
							scale: 0.5
							angle: Math.PI + drone.ti_local
							Q.Component.onCompleted:
							{
								equipment = laser_invisible.createObject(null, {slot: this})
								equipment.shooting = Qt.binding(is_started)
							}
						}
						Behaviour_attack_missile
						{
							target_groups: [groups.enemy_checkpoint]
							range: scene.seconds_to_frames(3)
						}
						CircleCollider
						{
							group: groups.enemy_dot
						}
					}
				}
				onEquipmentChanged: boss.alive = false
			}

			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}

			PolygonCollider
			{
				vertexes: [coords(16,920),coords(2032,920),coords(1857,1582),coords(1414,1984),coords(634,1984),coords(191,1582)]
				group: shoot ? groups.item_ignore_enemy : 0
			}
			PolygonCollider
			{
				vertexes: [coords(407,261),coords(733,909),coords(16,920)]
				group: shoot ? groups.item_ignore_enemy : 0
			}
			PolygonCollider
			{
				vertexes: [coords(1641,261),coords(1315,909),coords(2032,920)]
				group: shoot ? groups.item_ignore_enemy : 0
			}
			PolygonCollider
			{
				vertexes: [coords(857,920),coords(942,705),coords(1106,705),coords(1191,920)]
				group: shoot ? groups.item_ignore_enemy : 0
			}
		}
	}

	Q.Component
	{
		id: boss_1
		Vehicle
		{
			id: boss
			type: Body.KINEMATIC
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 1024 - 1, y / 1024 - 1)}
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 5
			icon: 3
			icon_color: "red"
			max_speed: 0
			scale: 15
			property int nb_wave_to_clean: 1
			property int wave: 0
			property real a: 0
			property real ti_local: root.ti
			property bool start: false
			property bool first: true
			property real last_shield_down: ti_local
			property real time_to_shield: 15
			property bool shoot: false
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property var door
			onTi_localChanged:
			{
				if(start)
				{
					if(first)
					{
						wave++
						root.new_wave = true
						first = false
					}
					else
					{
						if(wave%nb_wave_to_clean != 0)
						{
							if(!root.new_wave)
							{
								if(root.door_open == 0)
								{
									wave++
									root.new_wave = true
								}
								last_shield_down = ti_local
							}
						}
						else
						{
							if(root.door_open == 0 && root.nb_drone == 0)
							{
								shoot = true
								if(last_shield_down - ti_local > time_to_shield)
								{
									wave++
									root.new_wave = true
									shoot = false
								}
							}
							else
							{
								last_shield_down = ti_local
							}
						}
					}
				}
				else
				{
					if(reapp_ini)
					{
						reapp_ini = false
						door=space_door_factory.createObject(null, {scene: ship.scene, position: Qt.point(boss.position.x, boss.position.y), z: altitudes.boss - 0.0000001, angle: 0, scale: 60 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						var a = boss.a
						a = a + 0.02
						if(a >= 1)
						{
							a = 1
							reapp_end = true
						}
						boss.a = a
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							reapp_end = false
							boss.start = true
							door.alive = false
						}
					}
				}
			}

			Q.Component.onDestruction:
			{
				//changement de phase
				root.boss = boss_2.createObject(null, {scene: scene, position: Qt.point(boss.position.x, boss.position.y), angle: boss.angle})
			}

			Image
			{
				material: "chapter-5/boss"
				z: altitudes.boss
				mask: Qt.rgba(1, 1, 1, boss.a)
			}

			Image
			{
				material: "equipments/shield"
				mask: Qt.rgba(0.58, 0, 0.82, boss.a == 1 ? (shoot ? 0 : 1) : boss.a)
				scale: 1.1
				z: altitudes.shield
			}

			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(270,802)
				equipment: LaserBouncer
				{
					range_bullet: scene.seconds_to_frames(2)
					level: 1
					shooting: boss.shoot
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(512,802)
				equipment: LaserBouncer
				{
					range_bullet: scene.seconds_to_frames(2)
					level: 1
					shooting: boss.shoot
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(270,1050)
				equipment: MissileLauncher
				{
					level: 2
					shooting: boss.shoot
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(512,1050)
				equipment: MissileLauncher
				{
					level: 2
					shooting: boss.shoot
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(548,1314)
				equipment: MissileLauncher
				{
					level: 2
					shooting: boss.shoot
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(303,1438)
				angle: - Math.PI / 4
				equipment: CoilGun
				{
					range_bullet: scene.seconds_to_frames(3)
					shooting: boss.shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(711,1728)
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(923,1728)
				equipment: SecondaryShield
				{
					level: 2
				}
			}

			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(1778,802)
				equipment: LaserBouncer
				{
					range_bullet: scene.seconds_to_frames(2)
					level: 1
					shooting: boss.shoot
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(1536,802)
				equipment: LaserBouncer
				{
					range_bullet: scene.seconds_to_frames(2)
					level: 1
					shooting: boss.shoot
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(1778,1050)
				equipment: MissileLauncher
				{
					level: 2
					shooting: boss.shoot
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(1536,1050)
				equipment: MissileLauncher
				{
					level: 2
					shooting: boss.shoot
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(1500,1314)
				equipment: MissileLauncher
				{
					level: 2
					shooting: boss.shoot
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(1745,1438)
				angle: Math.PI / 4
				equipment: CoilGun
				{
					range_bullet: scene.seconds_to_frames(3)
					shooting: boss.shoot
					level: 2
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(1337,1728)
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				scale: 0.15/3
				position: coords(1125,1728)
				equipment: SecondaryShield
				{
					level: 2
				}
			}

			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			PolygonCollider
			{
				vertexes: [coords(16,920),coords(2032,920),coords(1857,1582),coords(1414,1984),coords(634,1984),coords(191,1582)]
				group: shoot ? groups.enemy_hull_naked : 0
			}
			PolygonCollider
			{
				vertexes: [coords(407,261),coords(733,909),coords(16,920)]
				group: shoot ? groups.enemy_hull_naked : 0
			}
			PolygonCollider
			{
				vertexes: [coords(1641,261),coords(1315,909),coords(2032,920)]
				group: shoot ? groups.enemy_hull_naked : 0
			}
			PolygonCollider
			{
				vertexes: [coords(857,920),coords(942,705),coords(1106,705),coords(1191,920)]
				group: shoot ? groups.enemy_hull_naked : 0
			}
			CircleCollider
			{
				group: shoot ? 0 : groups.enemy_invincible
				radius: 1.1
			}
		}
	}

	Q.Component
	{
		id: move_ship_to
		Animation
		{
			id: timer

			//ship_angle entre -pi et pi
			function calcul_ship_angle(angle)
			{
				var result = angle
				while(result <= -Math.PI)
				{
					result += 2 * Math.PI
				}
				while(result > Math.PI)
				{
					result -= 2 * Math.PI
				}
				return result
			}

			property real epsilon: 0.5
			property real x
			property real long_x
			property real y
			property real long_y
			property real angle
			property real long_angle
			property real time_to_move: 3
			property bool first: true
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(time < 6.9)
				{
					if(first)
					{
						ship.angle = calcul_ship_angle(ship.angle)
						long_x = ship.position.x - timer.x
						long_y = ship.position.y - timer.y
						long_angle = calcul_ship_angle(ship.angle - timer.angle)
						first = false
					}
					else
					{
						if(ship)
						{
							if(root.scene.seconds_to_frames(6.9) - root.scene.seconds_to_frames(time) < root.scene.seconds_to_frames(time_to_move) + 2)
							{
								ship.position.x -= long_x / root.scene.seconds_to_frames(time_to_move)
								ship.position.y -= long_y / root.scene.seconds_to_frames(time_to_move)
								ship.angle -= long_angle / root.scene.seconds_to_frames(time_to_move)
							}
							else
							{
								root.ship_ok()
								alive = false
							}
						}
					}
				}
			}
		}
	}
	StateMachine
	{
		begin: state_story_0 // state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_test_1 //state_story_0 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined

					//boss = boss_1.createObject(null, {scene: scene, position: Qt.point(0, 0), start: true})
					//tp_behaviour = tp_behaviour_factory.createObject(null, {parent: scene})
					//black_hole_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y - 40)})
					//wave_spawner = wave_assault_factory.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					music.objectName = "chapter-2/fuite"
					move_ship_to.createObject(null, {parent: scene, x: 0, y: 0, angle: Math.PI, time_to_move: 1.5})
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "cutscene"; value: true}
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: ship_ok
				onTriggered:
				{
					messages.add("apis/normal", qsTr("begin 1"))
					messages.add("lycop/normal", qsTr("begin 2"))
					messages.add("apis/normal", qsTr("begin 3"))
					messages.add("nectaire/normal", qsTr("begin 4"))
					messages.add("lycop/normal", qsTr("begin 5"))
					messages.add("apis/normal", qsTr("begin 6"))
					messages.add("lycop/normal", qsTr("begin 7"))
				}
			}
		}
		State
		{
			id: state_dialogue_0
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_story_2
				signal: state_dialogue_0.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					music.objectName = "survival/main"
					camera_position= undefined
					root.time_propugnator = root.ti
					root.info_print = screen_info_factory.createObject(screen_hud)
					root.level_begin = true
				}
			}
		}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_story_3
				signal: time_before_bossChanged
				guard: root.time_before_boss < 0.1
				onTriggered:
				{
					if(info_print)
					{
						info_print.destroy()
					}

					boss = boss_1.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y - 30), angle: Math.PI})
					tp_behaviour = tp_behaviour_factory.createObject(null, {parent: scene})
					wave_spawner = wave_assault_factory.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_story_3
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: boss_destroyed
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("after boss 1"))
					if(apis_alive)
					{
						messages.add("apis/normal", qsTr("after boss apis 1"))
						messages.add("lycop/normal", qsTr("after boss apis 2"))
						messages.add("apis/normal", qsTr("after boss apis 3"))
						messages.add("lycop/normal", qsTr("after boss apis 4"))
					}
					messages.add("nectaire/normal", qsTr("after boss 2"))
					messages.add("lycop/normal", qsTr("after boss 3"))
				}
			}
		}
		State
		{
			id: state_dialogue_1
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: scene; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_end
				signal: state_dialogue_1.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					if(apis_alive)
					{
						saved_game.add_science(3)
						platform.set_bool("secret-apis")
					}
					else
					{
						saved_game.add_science(1)
					}
					saved_game.add_matter(5000)
					saved_game.set("secret_apis", apis_alive)
					saved_game.set("travel/chapter-2/chapter-2c", true)
					saved_game.set("travel/chapter-6/chapter-6", true)
					if(!secret_tomate)
					{
						saved_game.set("travel/chapter-4/chapter-4b", true)
					}
					saved_game.set("niv_cour", 6)
					saved_game.set("file", "game/chapter-transition/chapter-transition.qml")
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}

	Q.Component.onCompleted:
	{
		jobs.run(music, "preload", ["chapter-2/fuite"])
		jobs.run(music, "preload", ["survival/main"])
		for(var i = 0; i < 6; ++i) jobs.run(scene_view, "preload", ["chapter-4/shield-cracks/" + i])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["chapter-5/star-bullet/" + i])
		jobs.run(scene_view, "preload", ["chapter-5/boss"])
		jobs.run(scene_view, "preload", ["folks/insec/2"])
		jobs.run(scene_view, "preload", ["folks/insec/3"])
		jobs.run(scene_view, "preload", ["folks/insec/5"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["folks/apis/4"])
		jobs.run(scene_view, "preload", ["equipments/laser-gun/bullet"])
		jobs.run(scene_view, "preload", ["chapter-5/black-hole"])
		jobs.run(scene_view, "preload", ["explosion"])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-3/door/" + i])
		for(var i = 0; i < 19; ++i)jobs.run(scene_view, "preload", ["chapter-5/black-hole-bullet/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		groups.init(scene)
		//root.info_print = screen_info_factory.createObject(screen_hud)
		root.finalize = function() {if(info_print) info_print.destroy()}
	}
}
