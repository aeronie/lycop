import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-5/mafia-zone.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal sortie_interdite_1
	signal sortie_interdite_2
	signal sortie_ville
	signal alert
	signal ship_ok
	signal police_killer
	signal mafia_visited_1
	signal mafia_visited_2
	signal mafia_visited_3
	signal mafia_visited_4
	signal mafia_visited_5
	property var camera_position: Qt.point(0, 0)
	property var last_ship_position: Qt.point(0, 0)
	property var info_print
	property var barriere_gauche
	property var barriere_droite
	property var barriere_bas
	property real t0: 0
	property real ti: 0
	property int indice_global: 0
	property bool level_begin: false
	property bool evacuation: false
	property bool aggression: false
	property bool mafia_visited: false
	property real chapter_scale: 2
	property var tab_weapon_general: [secondary_shield_weapon,laser_gun_factory,random_spray_factory,shield_canceler_factory]
	property var tab_config: [qsTr("secondary_shield"),qsTr("laser_gun"),qsTr("random_spray"),qsTr("shield_canceler")]
	property var tab_ind_config: init_config()
	property var indice_traitre: random_index(50)
	property int nb_ship: 0
	property var tab_checkpoint_enemy:[
	[41,70],
	[41,52],
	[44,52],
	[56,64],
	[60,64],
	[72,52],
	[75,52],
	[75,70],
	[72,70],
	[60,60],
	[56,60],
	[44,70]
	]

	function init_config()
	{
		var res = []
		//creation de la config à abattre
		for(var i = 0; i< 3; i++)
		{
			var temp = random_index(root.tab_weapon_general.length)
			res.push(temp)
		}
		return res
	}

	function random_sign() {return Math.random() < 0.5 ? -1 : 1}

	Q.Component
	{
		id: laser_gun_factory
		LaserGun
		{
		period: scene.seconds_to_frames(1 / 2)
			level: 2
		}
	}
	Q.Component
	{
		id: secondary_shield_weapon
		SecondaryShield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: random_spray_factory
		RandomSpray
		{
		period: scene.seconds_to_frames(1 / 5)
			enemy: 1
			level: 2
		}
	}
	Q.Component
	{
		id: shield_canceler_factory
		ShieldCanceler
		{
			range_bullet: 40
			level: 0
			period: scene.seconds_to_frames(6)
		}
	}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-2/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-2/"+mat+"/c"])
			}
		}
		return fill_tiles
	}

	function fill_2(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y, mat])
			}
		}
		return fill_tiles
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}

	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("%1,%2").arg(ship.position.x).arg(ship.position.y)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}

	scene: Scene
	{
		running: false

		/*Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}*/

		Body
		{
			id: nuit
			position: scene_view.center
			scale: 40
			property bool alive: true
			property real fuzzy_value: fuzzy()

			function fuzzy()
			{
				var res = (Math.cos(Math.PI * 2 * root.ti) + 1) / 2
				return res
			}

			onAliveChanged: if(!alive) destroy()

			Image
			{
				material: "chapter-5/alarm"
				z: 0.01
				mask: Qt.rgba(1,1,1,root.evacuation ? 0.3 * nuit.fuzzy_value : 0)
			}
		}
		Animation
		{
			id: timer
			time: 7
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		Tiles
		{
			id: fond
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[77,51,'n','metal'],
				[77,59,'e','metal'],
				[87,59,'n','metal'],
				[87,65,'e','metal'],
				[77,65,'s','metal'],
				[77,71,'e','metal'],
				[61,71,'s','metal'],
				[61,81,'e','metal'],
				[55,81,'s','metal'],
				[55,71,'w','metal'],
				[39,71,'s','metal'],
				[39,65,'w','metal'],
				[29,65,'s','metal'],
				[29,59,'w','metal'],
				[39,59,'n','metal'],
				[39,51,'w','metal']
				]).concat(
				fill(40,52,76,70,'metal'),
				fill(30,60,39,64,'metal'),
				fill(56,71,60,80,'metal'),
				fill(77,60,86,64,'metal')
				)
		}
		Ship
		{
			id: ship
			position.x: 58 * 4
			position.y: 75 * 4

			Q.Component.onDestruction:
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}
		Body
		{
			id: all_wall
			position.y: 0
			position.x: 0
			scale: chapter_scale*2

			type: Body.STATIC

			function point(x,y)
			{
				return Qt.point(x - 0.19 , y + 0.19)
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(39,71), all_wall.point(55,71), all_wall.point(55,81), all_wall.point(39,81)]
				group: groups.wall
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(29,65), all_wall.point(39,65), all_wall.point(39,81), all_wall.point(29,81)]
				group: groups.wall
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(29,43), all_wall.point(39,43), all_wall.point(39,59 - 2*0.19), all_wall.point(29,59 - 2*0.19)]
				group: groups.wall
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(39,43), all_wall.point(77 + 2*0.19,43), all_wall.point(77 + 2*0.19 ,51 - 2*0.19), all_wall.point(39,51 - 2*0.19)]
				group: groups.wall
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(77 + 2*0.19,43), all_wall.point(87,43), all_wall.point(87,59 - 2*0.19), all_wall.point(77 + 2*0.19 ,59 - 2*0.19)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(77 + 2*0.19 ,65), all_wall.point(87,65), all_wall.point(87,81), all_wall.point(77 + 2*0.19,81)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(61 + 2*0.19 ,71), all_wall.point(77 + 2*0.19,71), all_wall.point(77 + 2*0.19,81), all_wall.point(61 + 2*0.19,81)]
				group: groups.wall
			}
		}
		Sensor
		{
			id: enter_1
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_ville()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(55 ,72), Qt.point(61,72), Qt.point(61,81), Qt.point(55,81)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(55 ,72), Qt.point(61,72), Qt.point(61,81), Qt.point(55,81)]
				group: groups.no_blink
				sensor: true
			}
		}
		Sensor
		{
			id: enter_2
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_interdite_1()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(29 ,59), Qt.point(38,59), Qt.point(38,65), Qt.point(29,65)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(29 ,59), Qt.point(38,59), Qt.point(38,65), Qt.point(29,65)]
				group: groups.no_blink
				sensor: true
			}
		}
		Sensor
		{
			id: enter_3
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_interdite_2()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(78 ,59), Qt.point(87,59), Qt.point(87,65), Qt.point(78,65)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(78 ,59), Qt.point(87,59), Qt.point(87,65), Qt.point(78,65)]
				group: groups.no_blink
				sensor: true
			}
		}

		//batiment mafia
		Sensor
		{
			id: mafia_1
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(58 * 2 * chapter_scale,62 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.mafia_visited_1()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/mafia"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}

		Sensor
		{
			id: mafia_2
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(42.5 * 2 * chapter_scale,68.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.mafia_visited_2()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/mafia"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: mafia_3
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(42.5 * 2 * chapter_scale,53.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.mafia_visited_3()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/mafia"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: mafia_4
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(73.5 * 2 * chapter_scale,53.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.mafia_visited_4()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/mafia"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: mafia_5
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(73.5 * 2 * chapter_scale,68.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.mafia_visited_5()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/mafia"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
	}

	function occur_value(tab, value)
	{
		var res = 0
		for(var i = 0; i < tab.length ; i++)
		{
			if(tab[i] == value)
			{
				res++
			}
		}
		return res
	}

	function compare_tab(tab, tab_comp)
	{
		var res = true
		if(tab.length != tab_comp.length)
		{
			res = false
		}
		for(var i = 0; i < tab.length && res; i++)
		{
			var v1 = occur_value(tab, tab[i])
			var v2 = occur_value(tab_comp, tab[i])
			if(v1 != v2)
			{
				res = false
			}
		}
		return res
	}

	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			id: drone
			max_speed: 10
			max_angular_speed: 20
			icon: 2
			scale: 1.2
			icon_color: "blue"
			property int drone_type: config ? random_index(2) : random_index(tab_im.length)
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)}
			property int indice: 0
			property bool on: false
			property bool config: false
			property bool attack: root.aggression
			//property color mask: config ? "blue" : "white"
			property real distance: 0
			property bool shoot: attack
			property var tab_off: root.tab_weapon_general
			property var tab_im: ["folks/mafia/1","folks/mafia/3","folks/course/0","folks/course/1","folks/heter/6"]
			property var tab_slot: [
			[drone.coords(92,256),drone.coords(420,256),drone.coords(256,256)],
			[drone.coords(123,405),drone.coords(389,405),drone.coords(256,270)],
			[drone.coords(148,256),drone.coords(364,256),drone.coords(256,271),drone.coords(256,154),drone.coords(256, 71)],
			[drone.coords(124,306),drone.coords(388,306),drone.coords(256,267),drone.coords(216,155),drone.coords(296, 155)],
			[Qt.point(-0.70 , -0.37),Qt.point(-0.35 , 0),Qt.point(0 , 0.5),Qt.point(0.35 , 0),Qt.point(0.70 , -0.37)]
			]
			property var tab_weapon: []
			property var tab_ind_weapon: prepare_ind()
			property real ti_local: root.ti

			onTi_localChanged:
			{
				if(drone.position.x > 87 * 4 || drone.position.x < 29 * 4)
				{
					destroy()
				}
			}

			function prepare_ind()
			{
				var res = []
				if(config)
				{
					res = root.tab_ind_config
				}
				else
				{
					for(var i = 0; i < tab_slot[drone_type].length; i++ )
					{
						res.push(random_index(drone.tab_off.length))
					}

					while(compare_tab(res,root.tab_ind_config))
					{
						res.pop()
						res.push(random_index(tab_off.length))
					}
				}
				return res
			}

			onShootChanged:
			{
				for(var i = 0 ; i < drone.tab_weapon.length ; i++)
				{
					if(drone.tab_weapon[i])
					{
						if(drone.tab_ind_weapon[i] != 0)
						{
							drone.tab_weapon[i].shooting = shoot
						}
					}
				}
			}
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						scale: 2
						sound_scale: 10
					}
				}
			}

			Q.Component.onCompleted:
			{
				root.nb_ship++
			}
			Q.Component.onDestruction:
			{
				root.nb_ship--
				if(config)
				{
					root.police_killer()
				}
				else
				{
					root.alert()
				}
			}
			Image
			{
				material: tab_im[drone_type]
				z: altitudes.boss + 0.00000001
				//mask: drone.mask
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
			}
			Repeater
			{
				count: tab_slot[drone_type].length
				Q.Component
				{
					EquipmentSlot
					{
						position: tab_slot[drone_type][index]
						scale: 0.35
						angle: 0
						Q.Component.onCompleted:
						{
							equipment = drone.tab_off[drone.tab_ind_weapon[index]].createObject(null, {slot: this})
							drone.tab_weapon.push(equipment)
						}
					}
				}
			}
			Behaviour_attack_missile
			{
				enabled: !drone.attack
				target_groups: [groups.enemy_checkpoint - drone.indice]
				range: 10000000
			}
			Behaviour_attack_circle
			{
				enabled: drone.attack
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 10
				//max_distance: 20
			}
			CircleCollider
			{
				group: !root.level_begin ? groups.enemy_hull : groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: checkpoint_factory
		Vehicle
		{
			id: check
			property int indice: 0
			property int tab_indice: 0
			property int tab_indice_evacuation: evac_ind()
			property bool on: false
			property var drone
			property var tab: root.tab_checkpoint_enemy
			property var escape_pos: [[40,62],[-100,62],[76,62],[200,62]]
			property real x: root.evacuation ? escape_pos[tab_indice_evacuation][0] * root.chapter_scale * 2 : tab[tab_indice][0] * root.chapter_scale * 2
			property real y: root.evacuation ? escape_pos[tab_indice_evacuation][1] * root.chapter_scale * 2 : tab[tab_indice][1] * root.chapter_scale * 2
			property real ti_local: root.ti

			onTi_localChanged:
			{
				check.position.x = check.x
				check.position.y = check.y
			}

			function evac_ind()
			{
				var res = 0
				if(drone && drone.position.x > 58 * 4)
				{
					res = 2
				}
				return res
			}

			onOnChanged:
			{
				if(on)
				{
					if(!evacuation)
					{
						tab_indice = (tab_indice + 1)%tab.length
					}
					else
					{
						tab_indice_evacuation = (tab_indice_evacuation + 1)%escape_pos.length
					}
					on = false
				}
			}

			Q.Component.onCompleted:
			{
				check.position = Qt.point(x,y)
			}

			CircleCollider
			{
				group: groups.enemy_checkpoint - check.indice
				sensor: true
			}
		}
	}
	Q.Component
	{
		id: move_ship_to
		Animation
		{
			id: timer

			//ship_angle entre -pi et pi
			function calcul_ship_angle()
			{
				var result = ship.angle
				while(result < -Math.PI)
				{
					result += 2 * Math.PI
				}
				while(result > Math.PI)
				{
					result -= 2 * Math.PI
				}
				return result
			}

			property real epsilon: 0.5
			property real x
			property real long_x
			property real y
			property real long_y
			property real angle
			property real long_angle
			property real time_to_move: 3
			property bool first: true
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(time < 6.9)
				{
					if(first)
					{
						ship.angle = calcul_ship_angle()
						long_x = ship.position.x - timer.x
						long_y = ship.position.y - timer.y
						long_angle = ship.angle - timer.angle
						first = false
					}
					else
					{
						if(ship)
						{
							if(root.scene.seconds_to_frames(6.9) - root.scene.seconds_to_frames(time) < root.scene.seconds_to_frames(time_to_move) + 2)
							{
								ship.position.x -= long_x / root.scene.seconds_to_frames(time_to_move)
								ship.position.y -= long_y / root.scene.seconds_to_frames(time_to_move)
								ship.angle -= long_angle / root.scene.seconds_to_frames(time_to_move)
							}
							else
							{
								root.ship_ok()
								alive = false
							}
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: barriere_gauche_factory
		Vehicle
		{
			type: Body.STATIC
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: 26
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (38.625)*2*chapter_scale
						position.y: (65.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, (root.aggression || root.evacuation) ? 1 : 0)
					}
				}
			}
			Repeater
			{
				count: 26
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: 38.375*2*chapter_scale
						position.y: (65.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, (root.aggression || root.evacuation) ? 1 : 0)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [coords(38, 65.25 - 27/4), coords(38.75, 65.25 - 27/4), coords(38.75, 65.25), coords(38, 65.25)]
				group: (root.aggression || root.evacuation) ? groups.enemy_invincible : 0
			}
		}
	}
	Q.Component
	{
		id: barriere_droite_factory
		Vehicle
		{
			type: Body.STATIC
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: 26
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (77.625)*2*chapter_scale
						position.y: (65.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, (root.aggression || root.evacuation) ? 1 : 0)
					}
				}
			}
			Repeater
			{
				count: 26
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: 77.375*2*chapter_scale
						position.y: (65.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, (root.aggression || root.evacuation) ? 1 : 0)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [coords(77.25, 65.25 - 27/4), coords(78, 65.25 - 27/4), coords(78, 65.25), coords(77.25, 65.25)]
				group: (root.aggression || root.evacuation) ? groups.enemy_invincible : 0
			}
		}
	}
	Q.Component
	{
		id: barriere_bas_factory
		Vehicle
		{
			type: Body.STATIC
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: 26
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (61.125 - index / 4)*2*chapter_scale
						position.y: 71.625*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.aggression ? 1 : 0)
					}
				}
			}
			Repeater
			{
				count: 26
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (61.125 - index / 4)*2*chapter_scale
						position.y: 71.375*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, root.aggression ? 1 : 0)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [coords(61.25 - 27 / 4, 71.25), coords(61.25, 71.25), coords(61.25, 72), coords(61.25 - 27 / 4, 72)]
				group: root.aggression ? groups.enemy_invincible : 0
			}
		}
	}

	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_story_0 //state_test_1 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					camera_position= undefined
					/*for(var i = 0; i< tab_ind_config.length; i++)
					{
						console.log(occur_value(tab_ind_config,tab_ind_config[i]))
						console.log(tab_config[tab_ind_config[i]])
					}*/
					//root.aggression = true
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				onTriggered:
				{
					camera_position= undefined
					music.objectName = "chapter-5/mafia"
					barriere_gauche_factory.createObject(null, {scene: scene})
					barriere_droite_factory.createObject(null, {scene: scene})
					barriere_bas_factory.createObject(null, {scene: scene})
					for(var i = 0; i < 10; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(41 * 4, 70 * 4 + i * (52 - 70)/10 * 4), angle: 0, indice : root.indice_global})
						//checkpoint
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 1})
						root.indice_global++
					}
					for(var i = 0; i < 10; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(75 * 4, 52 * 4 + i * (70 - 52)/10 * 4), angle: Math.PI, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 7})
						root.indice_global++
					}
					for(var i = 0; i < 2; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(44 * 4 + i * (41 - 44) / 2 * 4, 70 * 4 ), angle: - Math.PI / 2, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1})
						root.indice_global++
					}
					for(var i = 0; i < 2; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(75 * 4 + i * (72 - 75) / 2 * 4, 70 * 4 ), angle: - Math.PI / 2, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 8})
						root.indice_global++
					}
					for(var i = 0; i < 2; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(41 * 4 + i * (44 - 41) / 2 * 4, 52 * 4 ), angle: Math.PI / 2, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 2})
						root.indice_global++
					}
					for(var i = 0; i < 2; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(72 * 4 + i * (75 - 72) / 2 * 4, 52 * 4 ), angle: Math.PI / 2, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 6})
						root.indice_global++
					}

					//diag
					for(var i = 0; i < 10; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(44 * 4 + i * (56-44) / 10 * 4, 52 * 4 + i * (64 - 52)/10 * 4), angle: Math.PI * 3 / 4, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 3})
						root.indice_global++
					}
					for(var i = 0; i < 10; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(60 * 4 + i * (72-60) / 10 * 4, 64 * 4 + i * (52 - 64)/10 * 4), angle: Math.PI / 4, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 5})
						root.indice_global++
					}
					for(var i = 0; i < 10; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(72 * 4 + i * (60 - 72) / 10 * 4, 70 * 4 + i * (60 - 70) / 10 * 4), angle: - Math.PI / 4, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 9})
						root.indice_global++
					}
					for(var i = 0; i < 10; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(56 * 4 + i * (44 - 56) / 10 * 4, 60 * 4 + i * (70 - 60)/10 * 4), angle: - Math.PI * 3 / 4, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 11})
						root.indice_global++
					}

					//milieu
					for(var i = 0; i < 2; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(56 * 4 + i * (60 - 56) / 2 * 4, 64 * 4), angle: Math.PI / 2, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 4})
						root.indice_global++
					}
					for(var i = 0; i < 2; i++)
					{
						var d1 = drone_factory.createObject(null, {config : root.indice_global == root.indice_traitre, scene: scene, position: Qt.point(60 * 4 + i * (56 - 60) / 2 * 4, 60 * 4), angle: - Math.PI / 2, indice : root.indice_global})
						checkpoint_factory.createObject(null, {scene: scene, indice: indice_global, drone: d1, tab_indice: 10})
						root.indice_global++
					}
					move_ship_to.createObject(null, {parent: scene, x: 58 * chapter_scale * 2, y: 71 * chapter_scale * 2, angle: 0, time_to_move: 3})
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: ship_ok
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("enter 1"))
					messages.add("lycop/normal", qsTr("enter 2"))
					root.level_begin = true
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_2; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}

			SignalTransition
			{
				targetState: state_alter_1
				signal: alert
				onTriggered:
				{
					root.aggression = true
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: police_killer
				onTriggered:
				{
					root.evacuation = true
					music.objectName = "chapter-2/fuite"
					messages.add("mafia/normal", qsTr("trap 1"))
					messages.add("lycop/angry", qsTr("trap 2"))
					messages.add("mafia/normal", qsTr("trap 3"))
					messages.add("lycop/normal", qsTr("trap 4"))
					messages.add("mafia/normal", qsTr("trap 5"))
					messages.add("lycop/angry", qsTr("trap 6"))
					messages.add("mafia/normal", qsTr("trap 7"))
					messages.add("nectaire/normal", qsTr("trap 8"))
				}
			}
			//bat mafia
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_1
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("explication 1"))
					if(!root.mafia_visited)
					{
						root.mafia_visited = true
					}
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_2
				guard: root.mafia_visited && occur_value(tab_ind_config, 0) > 0
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("indice 0 %1 %2").arg(occur_value(tab_ind_config, 0)).arg(tab_config[0]))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_2
				guard: root.mafia_visited && occur_value(tab_ind_config, 0) == 0
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("pas d'info"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_2
				guard: !root.mafia_visited
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("indice pas ok"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_3
				guard: root.mafia_visited && occur_value(tab_ind_config, 1) > 0
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("indice 1 %1 %2").arg(occur_value(tab_ind_config, 1)).arg(tab_config[1]))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_3
				guard: root.mafia_visited && occur_value(tab_ind_config, 1) == 0
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("pas d'info"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_3
				guard: !root.mafia_visited
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("indice pas ok"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_4
				guard: root.mafia_visited && occur_value(tab_ind_config, 2) > 0
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("indice 2 %1 %2").arg(occur_value(tab_ind_config, 2)).arg(tab_config[2]))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_4
				guard: root.mafia_visited && occur_value(tab_ind_config, 2) == 0
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("pas d'info"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_4
				guard: !root.mafia_visited
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("indice pas ok"))
				}
			}

			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_5
				guard: root.mafia_visited && occur_value(tab_ind_config, 3) > 0
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("indice 3 %1 %2").arg(occur_value(tab_ind_config, 3)).arg(tab_config[3]))
				}
			}

			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_5
				guard: root.mafia_visited && occur_value(tab_ind_config, 3) == 0
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("pas d'info"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: mafia_visited_5
				guard: !root.mafia_visited
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("indice pas ok"))
				}
			}

			//sortie
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: sortie_interdite_1
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("sortie interdite"))
					move_ship_to.createObject(null, {parent: scene, x: 39 * chapter_scale * 2, y: 62 * chapter_scale * 2, angle: Math.PI, time_to_move: 1})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: sortie_interdite_2
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("sortie interdite"))
					move_ship_to.createObject(null, {parent: scene, x: 77 * chapter_scale * 2, y: 62 * chapter_scale * 2, angle: 0, time_to_move: 1})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: sortie_ville
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("sortie interdite"))
					move_ship_to.createObject(null, {parent: scene, x: 58 * chapter_scale * 2, y: 71 * chapter_scale * 2, angle: 0, time_to_move: 1})
				}
			}
		}
		State
		{
			id: state_dialogue_2
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_transition
				signal: state_dialogue_2.propertiesAssigned
				guard: !messages.has_unread
			}
		}

		State
		{
			id: state_transition
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_story_2
				signal: ship_ok
			}
		}

		State
		{
			id: state_alter_1
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: nb_shipChanged
				guard: nb_ship == 0
				onTriggered:
				{
					aggression = false
					evacuation = true
					music.objectName = "chapter-2/fuite"
					messages.add("mafia/normal", qsTr("mafia alter 1"))
					messages.add("lycop/normal", qsTr("mafia alter 2"))
					messages.add("mafia/normal", qsTr("mafia alter 3"))
					messages.add("lycop/normal", qsTr("mafia alter 4"))
					messages.add("mafia/normal", qsTr("mafia alter 5"))
					messages.add("nectaire/normal", qsTr("mafia alter 6"))
				}
			}
		}
		State {id: state_dialogue_1; SignalTransition {targetState: state_run; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_run
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_end
				signal: sortie_ville
				onTriggered:
				{
					saved_game.add_science(1)
					saved_game.set("file", "game/chapter-5/chapter-5.qml")
					saved_game.save()
				}
			}
		}

		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}

	Q.Component.onCompleted:
	{
		jobs.run(music, "preload", ["chapter-5/mafia"])
		jobs.run(music, "preload", ["chapter-2/fuite"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		jobs.run(scene_view, "preload", ["folks/mafia/1"])
		jobs.run(scene_view, "preload", ["folks/mafia/3"])
		jobs.run(scene_view, "preload", ["folks/course/0"])
		jobs.run(scene_view, "preload", ["folks/course/1"])
		jobs.run(scene_view, "preload", ["folks/heter/6"])
		jobs.run(scene_view, "preload", ["chapter-5/building/mafia"])
		jobs.run(scene_view, "preload", ["chapter-5/alarm"])
		jobs.run(scene_view, "preload", ["chapter-2/barrier-wall"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		groups.init(scene)
		root.finalize = function() {if(info_print) info_print.destroy()}
	}
}
