import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-5/apis-zone.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal biosphere_destroyed
	signal camera_finish
	signal animation_finish
	signal dial_apis
	signal new_spawn
	property var camera_position: Qt.point(0, 0)
	property var last_ship_position: Qt.point(0, 0)
	property var info_print
	property int wave: 0
	property int wave_max: 3
	property real t0: 0
	property bool intro: true
	property bool wait: true
	property bool dial_active: true
	property int drone_alive: 0
	onWaitChanged:
	{
		if(drone_alive == 0 && wave < wave_max && !wait)
		{
			new_spawn()
			wait = true
			wave++
		}
	}
	onDrone_aliveChanged:
	{
		if(drone_alive == 0 && wave < wave_max && !wait)
		{
			new_spawn()
			wait = true
			wave++
		}
	}

	property var tab_drone: []

	property var tab_biosphere: [
	[0,-30],
	[-20,-50],
	[-20,-70],
	[0,-90],
	[20,-70],
	[20,-50]
	]

	function random_sign() {return Math.random() < 0.5 ? -1 : 1}

	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false

		Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}
		Ship
		{
			id: ship
			position.y: 10 * (1 - t0)

			Q.Component.onDestruction:
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}
		Repeater
		{
			count: tab_biosphere.length
			Q.Component
			{
				Vehicle
				{
					id: biosphere
					position.x: tab_biosphere[index][0]
					position.y: tab_biosphere[index][1]
					icon: 3
					scale: 4
					icon_color: "blue"
					property bool suffer: false
					property bool alive: true
					onAliveChanged: if(!alive) deleteLater()
					property var tab_slot:[[-1,-1],[-1,1],[1,1],[1,-1]]
					property var tab_ind_occuped:[]
					property bool actualise: false

					Q.Component.onDestruction:
					{
						root.biosphere_destroyed()
					}

					loot_factory: Repeater
					{
						Q.Component
						{
							Explosion
							{
								scale: 4
								sound_scale: 10
							}
						}
					}
					function isoccuped(a)
					{
						var res = false
						for(var i=0;i<tab_ind_occuped.length;++i)
						{
							if(tab_ind_occuped[i] == a)
							{
								res = true
							}
						}
						return res
					}

					function ind(a)
					{
						var res = tab_ind_occuped.length
						for(var i=0;i<tab_ind_occuped.length;++i)
						{
							if(tab_ind_occuped[i] == a)
							{
								res = i
							}
						}
						return res
					}

					function find_next_slot(x,y)
					{
						var res = tab_slot.length
						var i = 0
						var find = false
						while(i<tab_slot.length && !find && i < tab_slot.length)
						{
							var x_tot = biosphere.position.x + tab_slot[i][0] * biosphere.scale - x
							var y_tot = biosphere.position.y + tab_slot[i][1] * biosphere.scale - y

							if(x_tot * x_tot + y_tot * y_tot < 2 && !isoccuped(i))
							{
								find = true
								res = i
							}
							else
							{
								++i
							}
						}
						return res
					}

					Image
					{
						material: "chapter-5/building/biosphere"
						z: altitudes.boss
						mask:
						{
							var x = 0
							if(weapon)
							{
								x = weapon.health / weapon.max_health
							}
							return Qt.rgba(1, x, x, 1)
						}
					}

					EquipmentSlot
					{
						position.y: 0.3
						scale: 0.5
						equipment: Weapon
						{
							id: weapon
							shooting: false
							health: max_health
							max_health: 1000
						}
					}

					CircleCollider
					{
						position: Qt.point(tab_slot[0][0],tab_slot[0][1])
						group: groups.transportable
						sensor: true
						radius: 0.2/ biosphere.scale
						property bool calc_group: biosphere.actualise
						onCalc_groupChanged:
						{
							if(isoccuped(0))
							{
								group = 0
							}
							else
							{
								group = groups.transportable
							}
						}
					}

					CircleCollider
					{
						position: Qt.point(tab_slot[1][0],tab_slot[1][1])
						group: groups.transportable
						sensor: true
						radius: 0.2/ biosphere.scale
						property bool calc_group: biosphere.actualise
						onCalc_groupChanged:
						{
							if(isoccuped(1))
							{
								group = 0
							}
							else
							{
								group = groups.transportable
							}
						}
					}
					CircleCollider
					{
						position: Qt.point(tab_slot[2][0],tab_slot[2][1])
						group: groups.transportable
						sensor: true
						radius: 0.2/ biosphere.scale
						property bool calc_group: biosphere.actualise
						onCalc_groupChanged:
						{
							if(isoccuped(2))
							{
								group = 0
							}
							else
							{
								group = groups.transportable
							}
						}
					}
					CircleCollider
					{
						position: Qt.point(tab_slot[3][0],tab_slot[3][1])
						group: groups.transportable
						sensor: true
						radius: 0.2/ biosphere.scale
						property bool calc_group: biosphere.actualise
						onCalc_groupChanged:
						{
							if(isoccuped(3))
							{
								group = 0
							}
							else
							{
								group = groups.transportable
							}
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: space_door_factory
		Body
		{
			id: space_door
			scale: 15
			type: Body.STATIC
			property int indice: 0
			property color mask: "white"
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real z: 0
			Image
			{
				id: image
				material: "chapter-3/door/" + indice
				z: space_door.z
				mask: space_door.mask
			}
		}
	}
	Q.Component
	{
		id: tp_enemy_factory
		Animation
		{
			id: timer
			property var enemy
			property bool new_enemy: true
			property var door
			property real value: 0
			property real enemy_count: 6
			property var position_enemy
			property var position_central
			property bool reapp_ini: true
			property bool reapp_beg: false
			property bool reapp_end: false
			property real last_spawn: 7
			property real time_spawn: 1

			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged:
			{
				if(!alive)
				{
					destroy()
				}
			}

			onTimeChanged:
			{
				if(time < 6.9) //si je n'attends pas tous les elements de la classe ne sont pas initialisé correctement
				{
					//initialisation
					if(reapp_ini)
					{
						reapp_ini = false
						door = space_door_factory.createObject(null, {scene: ship.scene, position: position_enemy, z: altitudes.boss - 0.000001, angle : 0, scale: 4 })
						reapp_beg=true
					}

					//ouverture de la porte
					if(reapp_beg)
					{
						door.indice++
						if(door.indice == 10)
						{
							reapp_beg = false
						}
					}

					//apparition
					if(!reapp_beg && !reapp_end && !reapp_ini)
					{
						if(new_enemy)
						{
							enemy = drone_factory.createObject(null, {scene: scene, position: Qt.point(door.position.x, door.position.y), angle: Math.atan2(position_central.y - door.position.y, position_central.x - door.position.x )})
							new_enemy = false
							last_spawn = time
							enemy_count--
						}
						else
						{
							if(enemy_count == 0)
							{
								reapp_end = true
							}
							else
							{
								if(last_spawn - time > time_spawn)
								{
									new_enemy = true
								}
							}
						}
					}

					//fermeture porte
					if(reapp_end)
					{
						door.indice--
						if(door.indice == 0)
						{
							root.wait = false
							reapp_end = false
							door.alive = false
							timer.alive = false
						}
					}
				}
			}
		}
	}
	Q.Component
	{
		id: drone_townies_factory
		Vehicle
		{
			id: drone
			property int folk: random_index(10)
			max_speed: 10
			max_angular_speed: 6
			property bool alive: root.intro
			onAliveChanged: if(!alive) destroy()
			icon: 2
			icon_color: "blue"
			Image
			{
				material: "folks/townies/" + drone.folk
			}
		}
	}
	Q.Component
	{
		id: drone_apis_factory
		Vehicle
		{
			id: drone
			max_speed: 10
			max_angular_speed: 6
			property bool alive: root.intro
			onAliveChanged: if(!alive) destroy()
			icon: 2
			icon_color: "blue"
			Image
			{
				material: "folks/apis/4"
			}
		}
	}
	Q.Component
	{
		id: drone_apis_chief_factory
		Vehicle
		{
			id: drone
			max_speed: 10
			max_angular_speed: 6
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var zone
			icon: 2
			icon_color: "blue"

			function pos()
			{
				return drone ? drone.position : Qt.point(0,0)
			}

			Q.Component.onCompleted:
			{
				zone = zone_apis_factory.createObject(null, {scene: scene, position: drone.position})
				zone.position = Qt.binding(pos)
			}
			Q.Component.onDestruction:
			{
				zone.alive = false
			}
			Image
			{
				material: "folks/apis/4"
			}
		}
	}

	Q.Component
	{
		id: zone_apis_factory

		Sensor
		{
			id: detector
			property bool active: root.dial_active
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.dial_apis()
				}
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-4,-4),Qt.point(4,-4),Qt.point(4,4),Qt.point(-4,4)]
				group: detector.active ? groups.sensor : 0
				sensor: true
			}
		}
	}

	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			id: drone
			max_speed: 10
			max_angular_speed: 10
			icon: 2
			icon_color: "red"
			property var biosphere
			property bool behaviour: true
			property int num_slot: 99
			property bool alive: true
			onAliveChanged: if(!alive) deleteLater()

			Q.Component.onCompleted:
			{
				root.drone_alive++
			}

			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component
				{
					Explosion
					{
						scale: 2
						sound_scale: 10
					}
				}
			}
			function pos_slot()
			{
				return drone.biosphere ? Qt.point(drone.biosphere.position.x + drone.biosphere.scale * drone.biosphere.tab_slot[drone.num_slot][0],drone.biosphere.position.y + drone.biosphere.scale * drone.biosphere.tab_slot[drone.num_slot][1]) : Qt.point(0,0)
			}

			function begin(a)
			{
				var pos = a.find_next_slot(drone.position.x,drone.position.y)
				if(pos < a.tab_slot.length)
				{
					drone.biosphere = a
					drone.num_slot = pos
					drone.max_speed = 0
					a.tab_ind_occuped.push(pos)
					drone.behaviour = false
					drone.type = Body.KINEMATIC
					drone.position = Qt.binding(pos_slot)
					if(drone.biosphere.tab_ind_occuped.length == drone.biosphere.tab_slot.length)
					{
						drone.biosphere.add_dot(1)
						drone.biosphere.suffer = true
					}
					drone.biosphere.actualise = !drone.biosphere.actualise
				}
			}

			Q.Component.onDestruction:
			{
				root.drone_alive--
				if(drone.biosphere)
				{
					var i = drone.biosphere.ind(num_slot)
					if(i != drone.biosphere.tab_ind_occuped.length - 1)
					{
						drone.biosphere.tab_ind_occuped[i] = drone.biosphere.tab_ind_occuped.pop()
					}
					else
					{
						drone.biosphere.tab_ind_occuped.pop()
					}
					if(drone.biosphere.suffer)
					{
						drone.biosphere.add_dot(-1)
						drone.biosphere.suffer = false
					}
					drone.biosphere.actualise = !drone.biosphere.actualise
				}
			}

			Image
			{
				material: "folks/heter/1"
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
			}
			EquipmentSlot
			{
				position.y: 0.3
				scale: 0.5
				equipment: Shield
				{
					level: 2
				}
			}
			Behaviour_attack_missile
			{
				enabled: drone.behaviour
				target_groups: [groups.transportable]
				range: 10000000
			}
			CircleCollider
			{
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
			}
			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				radius: 1.1
			}
		}
	}
	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("Vagues restantes: %1").arg(root.wave_max - root.wave + 1)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}

	Q.Component
	{
		id: move_camera_to_position
		Animation
		{
			id: timer
			property var end_position
			property real chemin_x: 0
			property real chemin_y: 0
			property real time_to: root.scene.seconds_to_frames(2)
			time: 7
			speed: -1

			Q.Component.onCompleted:
			{
				chemin_x = end_position.x - root.camera_position.x
				chemin_y = end_position.y - root.camera_position.y
			}

			onTimeChanged:
			{
				root.camera_position = Qt.point( root.camera_position.x + chemin_x / time_to, root.camera_position.y + chemin_y / time_to)

				if(root.scene.seconds_to_frames(7) - root.scene.seconds_to_frames(time) > time_to)
				{
					root.camera_finish()
					timer.deleteLater()
				}
			}
		}
	}

	Q.Component
	{
		id: animation_drone_factory
		Animation
		{
			id: timer
			property var tab_drone_local: root.tab_drone
			property bool demi_tour: true
			time: 7
			speed: -1

			onTimeChanged:
			{
				if(demi_tour)
				{
					tab_drone_local[0].angle += 0.06
					if(tab_drone_local[0].angle > Math.PI / 2)
					{
						tab_drone_local[0].angle = Math.PI / 2
						demi_tour = false
					}
				}
				else
				{
					for(var i = 0 ; i < tab_drone.length ; ++i)
					{
						tab_drone_local[i].position.x += 0.5
					}

					if(tab_drone_local[0].position.x - ship.position.x > 50)
					{
						root.animation_finish()
						root.intro = false
						timer.deleteLater()
					}
				}
			}
		}
	}

	StateMachine
	{
		begin: state_story_0 // state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_story_0 //state_test_1 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					/*camera_position= undefined
					for(var i = 0 ; i< 8; ++i)
					{
						drone_factory.createObject(null, {scene: scene, position: Qt.point(i, 30), angle: - Math.PI / 2 })
					}*/
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 2000}
				onTriggered: music.objectName = "chapter-5/apis"
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					ship.position.y = 0
					camera_position = undefined
					messages.add("lycop/normal", qsTr("begin 1"))
					messages.add("nectaire/normal", qsTr("begin 2"))
					messages.add("lycop/normal", qsTr("begin 3"))
					messages.add("nectaire/normal", qsTr("begin 4"))
					messages.add("lycop/normal", qsTr("begin 5"))
					messages.add("nectaire/normal", qsTr("begin 6"))
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_2; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_story_2
			SignalTransition
			{
				targetState: state_animation_camera_0
				signal: state_story_2.propertiesAssigned
				onTriggered:
				{
					camera_position = Qt.point(ship.position.x,ship.position.y)
					// creation des drones manifestants
					var tab_pos_drone = [
					[3,-60],
					[0,-61.5],
					[0,-58.5],
					[-3,-63],
					[-3,-60],
					[-3,-57],
					[-6,-64.5],
					[-6,-61.5],
					[-6,-58.5],
					[-6,-55.5]
					]
					drone_apis_chief_factory.createObject(null, {scene: scene, position: Qt.point(0, -75), angle: Math.PI})
					var z = drone_apis_factory.createObject(null, {scene: scene, position: Qt.point(8, -60), angle: - Math.PI / 2 })
					tab_drone.push(z)
					for(var i = 0 ; i < tab_pos_drone.length ; ++i)
					{
						z = drone_townies_factory.createObject(null, {scene: scene, position: Qt.point(tab_pos_drone[i][0], tab_pos_drone[i][1]), angle: Math.PI / 2 })
						tab_drone.push(z)
					}
					move_camera_to_position.createObject(null, {parent: scene, end_position: Qt.point(0,-60)})
				}
			}
		}
		State
		{
			id: state_animation_camera_0
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: camera_finish
				onTriggered:
				{
					messages.add("apis/normal", qsTr("manif 1"))
					messages.add("apis/normal", qsTr("manif 2"))
					messages.add("townies/normal", qsTr("manif 3"))
				}
			}
		}
		State
		{
			id: state_dialogue_1
			SignalTransition
			{
				targetState: state_animation_drone
				signal: state_dialogue_1.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					//animation des drones
					animation_drone_factory.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_animation_drone
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_animation_camera_1
				signal: animation_finish
				onTriggered:
				{
					//animation de la camera
					move_camera_to_position.createObject(null, {parent: scene, end_position: Qt.point(ship.position.x, ship.position.y)})
				}
			}
		}
		State
		{
			id: state_animation_camera_1
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_1_2
				signal: camera_finish
				onTriggered:
				{
					messages.add("lycop/happy", qsTr("Après manif 1"))
					messages.add("nectaire/normal", qsTr("Après manif 2"))
					messages.add("lycop/normal", qsTr("Après manif 3"))
					camera_position = undefined
				}
			}
		}

		State {id: state_dialogue_1_2; SignalTransition {targetState: state_story_3; signal: state_dialogue_1_2.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_3
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: dial_apis
				onTriggered:
				{
					root.dial_active = false
					messages.add("apis/normal", qsTr("apis_before_invasion 1"))
					messages.add("nectaire/normal", qsTr("apis_before_invasion 2"))
					messages.add("apis/normal", qsTr("apis_before_invasion 3"))
					messages.add("lycop/surprised", qsTr("apis_before_invasion 4"))
					messages.add("apis/normal", qsTr("apis_before_invasion 5"))
					messages.add("lycop/angry", qsTr("apis_before_invasion 6"))
					messages.add("apis/normal", qsTr("apis_before_invasion 7"))
					messages.add("lycop/normal", qsTr("apis_before_invasion 8"))
					var tab_pos = [
					[-40,-70],
					[-40,-50],
					[40,-70],
					[40,-50],
					[0,-110],
					[0,-10],
					]
					for(var i = 0 ; i < tab_pos.length ; ++i)
					{
						tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(tab_pos[i][0], tab_pos[i][1]), position_central: Qt.point(0,-60)})
					}
					root.info_print = screen_info_factory.createObject(screen_hud)
					root.wave++
				}
			}
		}
		State {id: state_dialogue_2; SignalTransition {targetState: state_story_4; signal: state_dialogue_2.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_4
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_story_4
				signal: new_spawn
				onTriggered:
				{
					//animation de spawn
					var tab_pos = [
					[-40,-70],
					[-40,-50],
					[40,-70],
					[40,-50],
					[0,-110],
					[0,-10],
					]
					for(var i = 0 ; i < tab_pos.length ; ++i)
					{
						tp_enemy_factory.createObject(null, {parent: scene, position_enemy: Qt.point(tab_pos[i][0], tab_pos[i][1]), position_central: Qt.point(0,-60)})
					}
				}
			}
			SignalTransition
			{
				targetState: state_story_4
				signal: biosphere_destroyed
				onTriggered:
				{
					game_over = true
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: root.drone_aliveChanged
				guard: root.drone_alive == 0 && wave == wave_max && !wait
				onTriggered:
				{
					if(info_print)
					{
						info_print.destroy()
					}
					messages.add("lycop/normal", qsTr("after_invasion 1"))
					messages.add("nectaire/normal", qsTr("after_invasion 2"))
					messages.add("lycop/normal", qsTr("after_invasion 3"))
					root.dial_active = true
				}
			}
		}
		State {id: state_dialogue_3; SignalTransition {targetState: state_story_5; signal: state_dialogue_3.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_5
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: dial_apis
				onTriggered:
				{
					root.dial_active = false
					messages.add("apis/normal", qsTr("apis_after_invasion 1"))
					messages.add("nectaire/normal", qsTr("apis_after_invasion 2"))
					messages.add("lycop/normal", qsTr("apis_after_invasion 3"))
					messages.add("apis/normal", qsTr("apis_after_invasion 4"))
					messages.add("apis/normal", qsTr("apis_after_invasion 5"))
					messages.add("lycop/normal", qsTr("apis_after_invasion 6"))
					messages.add("apis/normal", qsTr("apis_after_invasion 7"))
					messages.add("nectaire/normal", qsTr("apis_after_invasion 8"))
					messages.add("apis/normal", qsTr("apis_after_invasion 9"))
					messages.add("nectaire/normal", qsTr("apis_after_invasion 10"))
					messages.add("apis/normal", qsTr("apis_after_invasion 11"))
					messages.add("nectaire/normal", qsTr("apis_after_invasion 12"))
					messages.add("apis/normal", qsTr("apis_after_invasion 13"))
					messages.add("lycop/normal", qsTr("apis_after_invasion 14"))
				}
			}
		}
		State
		{
			id: state_dialogue_4
			SignalTransition
			{
				targetState: state_end
				signal: state_dialogue_4.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					saved_game.add_science(1)
					saved_game.set("file", "game/chapter-5/chapter-5.qml")
					saved_game.save()
				}
			}
		}

		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}

	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["folks/townies/" + i])
		for(var i = 0; i < 25; ++i) jobs.run(scene_view, "preload", ["chapter-3/door/" + i])
		jobs.run(scene_view, "preload", ["chapter-5/building/biosphere"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		jobs.run(music, "preload", ["chapter-5/apis"])
		jobs.run(scene_view, "preload", ["folks/apis/4"])
		jobs.run(scene_view, "preload", ["folks/heter/1"])
		groups.init(scene)
		root.finalize = function() {if(info_print) info_print.destroy()}
	}
}
