import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import "qrc:///controls"
import "qrc:///controls/styles" as S

Item
{
	property real cost: 0
	property real matter: 0
	signal accept()
	signal reject()
	ColumnLayout
	{
		anchors.left: parent.left
		anchors.right: parent.right
		anchors.top: parent.top
		anchors.margins: spacing
		Item
		{
			Layout.fillWidth: true
			implicitHeight: layout.implicitHeight + layout.spacing * 2
			BorderImage
			{
				anchors.fill: parent
				source: "///widgets/MessageLog.png"
				verticalTileMode: BorderImage.Repeat
			}
			RowLayout
			{
				id: layout
				anchors.left: parent.left
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: spacing
				Item
				{
					width: 100
					height: 100
					Layout.alignment: Qt.AlignTop
					Image
					{
						source: "///data/game/folks/townies/normal.png"
						anchors.fill: parent
						smooth: false
					}
				}
				Label
				{
					text: [qsTr("text 1 %1"), qsTr("text 2 %1"), qsTr("text 3 %1")][random_index(3)].arg(cost)
					font.weight: Font.Normal
					font.capitalization: Font.MixedCase
					font.pointSize: S.Private.font.pointSize * 1.2
					color: "black"
					wrapMode: Text.WordWrap
					Layout.fillWidth: true
					Layout.fillHeight: true
				}
			}
		}
		Buttons
		{
			model: [action_accept, action_reject]
		}
	}
	Action
	{
		id: action_reject
		text: qsTr("&No")
		onTriggered: reject()
	}
	Action
	{
		id: action_accept
		text: qsTr("&Yes")
		enabled: matter >= cost
		onTriggered: accept()
	}
}
