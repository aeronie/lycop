import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import QtQuick.Controls 1.0 as Q
import QtQuick.Layouts 1.0 as Q
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q
import "qrc:///controls/styles" as S

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-5/chapter-5.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	readonly property bool secret_tomate: saved_game.self.get("secret_tomate", false)
	readonly property int etat_du_niveau: saved_game.self.get("niveau_5", 0) //0 = debut , 1 = retour apis, 2 = retour course, 3 = retour mafia
	property alias ship: ship
	signal one_boss_destroyed
	signal dmg_done
	signal apis_parle
	signal apis_finish
	signal ship_ok
	signal malfrat
	signal citoyen
	signal ready_enter_bot
	signal enter_auto_1
	signal enter_auto_2
	signal enter_auto_3
	signal sortie_auto_1
	signal sortie_auto_2
	signal sortie_auto_3
	signal sortie_interdite
	signal sortie_ok
	signal sortie_apis
	signal sortie_course
	signal sortie_mafia
	signal scan_finish
	signal enter_finish
	signal adm_visited_1
	signal adm_visited_2
	signal adm_visited_3
	signal adm_visited_4
	signal adm_visited_5
	signal adm_visited_6
	signal adm_visited_7
	signal adm_visited_8
	signal apis_visited
	signal course_visited
	signal mafia_visited
	property var camera_position: Qt.point(0, 0)
	property var last_ship_position: Qt.point(0, 0)
	property var info_print
	property var image_accident
	property bool level_begin: false
	property real t0: 0
	property real ti: 0
	property real delit_value: 0
	property real police: saved_game.self.get("niveau_alerte", 0) //indice de police
	property bool police_active: police == 5 //indice de police
	property bool hardcore: police == 5 //si hardcore pas de pop de clampin seulement des mechants pas bô
	property real dette: 0 //indice de dette
	property int ind_general_drone: 0 // nécéssaire pour les checkpoints
	property real chapter_scale: 2
	property int administration_visited: 0
	property int choice: 0
	property var drone_scan_1
	property var drone_scan_2
	property var barriere
	property bool course_open: false
	property bool mafia_open: false
	property bool escape_ship: false

	property var tab_checkpoint_enemy: [
	[45, 34],
	[45, 33],
	[44, 32],
	[43, 32],
	[42, 33],
	[42, 34],
	[45, 45],
	[45, 46],
	[42, 59],
	[42, 60],
	[43, 61],
	[44, 61],
	[45, 60],
	[45, 59],
	[42, 46],
	[42 ,45]
	]

	property var tab_checkpoint_enemy_2: [
	[71, 34],
	[71, 33],
	[70, 32],
	[69, 32],
	[68, 33],
	[68, 34],
	[71, 45],
	[71, 46],
	[68, 59],
	[68, 60],
	[69, 61],
	[70, 61],
	[71, 60],
	[71, 59],
	[68, 46],
	[68 ,45]
	]

	property var tab_checkpoint_enemy_3: [
	[26, 20],
	[90, 20],
	[90, 23],
	[26, 23]
	]

	property var tab_pos_police: [
	[27.5, 21.5],
	[88.5, 21.5],
	[43.5, 33.5],
	[69.5, 33.5],
	[43.5, 45.5],
	[69.5, 45.5],
	[43.5, 59.5],
	[69.5, 59.5]
	]

	property var tab_weapons: [coil_gun_factory,laser_gun_factory,acid_gun_factory,qui_poutre_factory,vortex_factory]
	Q.Component
	{
		id: laser_gun_factory
		LaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: acid_gun_factory
		AcidGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: qui_poutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: vortex_factory
		VortexGun
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: coil_gun_factory
		CoilGun
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			level: 2
		}
	}
	function random_sign() {return Math.random() < 0.5 ? -1 : 1}
	function do_dmg(x)
	{
		root.delit_value = Math.round(x.value * (1.05 - .1 * Math.random()) / 10) * 10
		dmg_done()
	}
	function create_tiles(data, log)
	{
		var size = data.length
		var previous = data[size-1]
		var current
		var chapter_tiles = []
		for (var i = 0; i < size; i++)
		{
			current = data[i]
			if (chapter_tiles.length>0 && current[2] != previous[2] && previous[3] != '')
			{
				var a = current[2]
				var b = previous[2]
				chapter_tiles[chapter_tiles.length-1][2]=sort_and_combine(a,b)+in_or_out(a,b)
			}
			if (current[3] != '')
			{
				if (current[0] == previous[0])
				{
					//console.log("dep-y")
					var min = 0
					var max = 0
					var increment = 0
					if (current[1] > previous[1])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[1]+increment
					max = current[1]
					for (var y = min; y != max+increment; y=y+increment)
					{
						//console.log(current[0]+"-"+y)
						chapter_tiles.push([current[0], y, current[2], current[3]])
					}
				}
				else
				{
					//console.log("dep-x")
					var min = 0
					var max = 0
					var increment = 0
					if (current[0] > previous[0])
					{
						increment = 1
					}
					else
					{
						increment = -1
					}
					min = previous[0]+increment
					max = current[0]
					for (var x = min; x != max+increment; x=x+increment)
					{
						//console.log(x+"-"+current[1])
						chapter_tiles.push([x, current[1], current[2], current[3]])
					}
				}
			}
			//console.log(chapter_tiles.length)
			previous = current
		}
		//console.log(chapter_tiles.length)
		if (data[0][3] != '' || data[data.length-1][3] != '')
			chapter_tiles[chapter_tiles.length-1][2] = 'nw'
		for (var tile=0; tile < chapter_tiles.length; tile++)
		{
			chapter_tiles[tile][2]="chapter-2/"+chapter_tiles[tile][3]+"/"+chapter_tiles[tile][2]
			if (log)
				console.log(chapter_tiles[tile][2])
		}
		return chapter_tiles
	}
	function fill(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y,"chapter-2/"+mat+"/c"])
			}
		}
		return fill_tiles
	}

	function fill_2(x1,y1,x2,y2,mat)
	{
		var fill_tiles = []
		for(var x = x1; x <= x2; ++x)
		{
			for(var y = y1; y <= y2; ++y)
			{
				fill_tiles.push([x,y, mat])
			}
		}
		return fill_tiles
	}
	function sort_and_combine (a, b)
	{
		if (a =='n' || a =='s')
			return a+b
		else
			return b+a
	}
	function index (a)
	{
		var return_value = 0
		switch (a)
		{
			case "n":
				return_value = 1
				break
			case "e":
				return_value = 2
				break
			case "s":
				return_value = 3
				break
			case "o":
				return_value = 4
				break
			default:
				break
		}
		return return_value
	}
	function in_or_out (previous,current)
	{
		var diff = index(previous) - index(current)
		if( diff == -1 || diff == 3 )
			return "i"
		else
			return ""
	}
	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")

	property var tab_auto:[
	[84,162]
	]

	scene: Scene
	{
		running: false

		Background
		{
			x: 10
			y: 2
			position.y: 32.5 * 4
			z: altitudes.background_near-0.000000001
			scale: 10
			material: "chapter-2/rectangle_noir"
		}
		Background
		{
			y: 2
			x: 10
			position.y: 47 * 4
			z: altitudes.background_near-0.000000001
			scale: 7
			material: "chapter-2/rectangle_noir"
		}
		Background
		{
			y: 2
			x: 10
			position.y: 60 * 4
			z: altitudes.background_near-0.000000001
			scale: 7
			material: "chapter-2/rectangle_noir"
		}
		Background
		{
			y: 2
			x: 10
			position.y: 7 * 4
			z: altitudes.background_near-0.000000001
			scale: 55
			material: "chapter-2/rectangle_noir"
		}
		Background
		{
			y: 2
			x: 10
			position.y: 75.5 * 4
			z: altitudes.background_near-0.000000001
			scale: 12
			material: "chapter-2/rectangle_noir"
		}
		Background
		{
			y: 2
			x: 10
			position.y: 85.5 * 4
			z: altitudes.background_near-0.000000001
			scale: 12
			material: "chapter-2/rectangle_noir"
		}

		Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}

		Tiles
		{
			id: fond
			z: altitudes.background_near
			mask: "white"
			scale: root.chapter_scale
			tiles: create_tiles([
				[28,64,'n','metal'],
				[28,57,'w','metal'],
				[18,57,'s','metal'],
				[18,50,'w','metal'],
				[28,50,'n','metal'],
				[28,44,'w','metal'],
				[18,44,'s','metal'],
				[18,37,'w','metal'],
				[28,37,'n','metal'],
				[28,31,'w','metal'],
				[24,31,'s','metal'],
				[24,18,'w','metal'],
				[30,18,'n','metal'],
				[30,0,'w','metal'],
				[36,0,'n','metal'],
				[36,18,'e','metal'],
				[55,18,'n','metal'],
				[55,0,'w','metal'],
				[61,0,'n','metal'],
				[61,18,'e','metal'],
				[81,18,'n','metal'],
				[81,0,'w','metal'],
				[87,0,'n','metal'],
				[87,18,'e','metal'],
				[92,18,'n','metal'],
				[92,31,'e','metal'],
				[87,31,'s','metal'],
				[87,37,'e','metal'],
				[99,37,'n','metal'],
				[99,44,'e','metal'],
				[87,44,'s','metal'],
				[87,50,'e','metal'],
				[99,50,'n','metal'],
				[99,57,'e','metal'],
				[87,57,'s','metal'],
				[87,64,'e','metal'],
				[99,64,'n','metal'],
				[99,70,'e','metal'],
				[61,70,'s','metal'],
				[61,85,'e','metal'],
				[55,85,'s','metal'],
				[55,70,'w','metal'],
				[18,70,'s','metal'],
				[18,64,'w','metal']
				]).concat(
				fill(29,57,86,64,'metal'),
				fill(29,44,86,50,'metal'),
				fill(29,19,86,37,'metal'),
				fill(25,19,28,30,'metal'),
				fill(87,19,91,30,'metal'),
				fill(56,70,60,84,'metal'),
				fill(31,1,35,18,'metal'),
				fill(56,1,60,18,'metal'),
				fill(82,1,86,18,'metal'),
				fill_2(18,38,99,38,'chapter-5/highway/top'),
				fill_2(18,39,99,39,'chapter-5/highway/mid'),
				fill_2(18,40,99,41,'chapter-5/highway/train'),
				fill_2(18,42,99,42,'chapter-5/highway/mid'),
				fill_2(18,43,99,43,'chapter-5/highway/bot'),
				fill_2(18,51,99,51,'chapter-5/highway/top'),
				fill_2(18,52,99,52,'chapter-5/highway/mid'),
				fill_2(18,53,99,54,'chapter-5/highway/train'),
				fill_2(18,55,99,55,'chapter-5/highway/mid'),
				fill_2(18,56,99,56,'chapter-5/highway/bot'),
				fill_2(18,65,99,65,'chapter-5/highway/top'),
				fill_2(18,66,99,66,'chapter-5/highway/mid'),
				fill_2(18,67,99,67,'chapter-5/highway/train'),
				fill_2(18,68,99,68,'chapter-5/highway/mid'),
				fill_2(18,69,99,69,'chapter-5/highway/bot')

				)
		}
		Ship
		{
			id: ship
			position.y: [81.5, 6.5, 6.5, 16.5][root.etat_du_niveau] * root.chapter_scale * 2
			position.x: [58, 84, 33, 58][root.etat_du_niveau] * root.chapter_scale * 2
			angle: [0, Math.PI, Math.PI, Math.PI][root.etat_du_niveau]

			Q.Component.onDestruction:
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}

		Animation
		{
			id: timer
			time: 7
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		Body
		{
			id: pas_de_tir
			position: ship ? ship.position : Qt.point(0,0)
			scale: 30
			type: Body.KINEMATIC
			property bool first: !root.level_begin
			onFirstChanged:
			{
				if(!first)
				{
					first = false
				}
			}
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			CircleCollider
			{
				group: root.level_begin ? 0 : (pas_de_tir.first ? groups.clear_screen : groups.no_blink)
				sensor: true
			}
		}
		Body
		{
			id: all_wall
			position.y: 0
			position.x: 0
			scale: chapter_scale*2

			type: Body.STATIC

			function point(x,y)
			{
				return Qt.point(x - 0.19 , y + 0.19)
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(18,70), all_wall.point(55,70), all_wall.point(55,85), all_wall.point(18,85)]
				group: groups.wall
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(8,18), all_wall.point(18,18), all_wall.point(18,85), all_wall.point(8,85)]
				group: groups.wall
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(18,57), all_wall.point(28,57), all_wall.point(28,64 - 2*0.19), all_wall.point(18,64 - 2*0.19)]
				group: groups.wall
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(18,44), all_wall.point(28,44), all_wall.point(28,50- 2*0.19), all_wall.point(18,50- 2*0.19)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(18,31), all_wall.point(28,31), all_wall.point(28,37- 2*0.19), all_wall.point(18,37- 2*0.19)]
				group: groups.wall
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(8,0), all_wall.point(24,0), all_wall.point(24,31), all_wall.point(8,31)]
				group: groups.wall
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(24,-1), all_wall.point(30,-1), all_wall.point(30,18-2*0.19), all_wall.point(24,18-2*0.19)]
				group: groups.wall
			}

			PolygonCollider
			{
				vertexes: [all_wall.point(8,-3), all_wall.point(109,-3), all_wall.point(109,0-2*0.19), all_wall.point(8,0-2*0.19)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(36 + 2*0.19 ,-1), all_wall.point(55,-1), all_wall.point(55,18-2*0.19), all_wall.point(36+2*0.19,18-2*0.19)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(61 + 2*0.19 ,-1), all_wall.point(81,-1), all_wall.point(81,18-2*0.19), all_wall.point(61+2*0.19,18-2*0.19)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(87 + 2*0.19 ,-1), all_wall.point(109,-1), all_wall.point(109,18-2*0.19), all_wall.point(87+2*0.19,18-2*0.19)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(92 + 2*0.19 ,18-2*0.19), all_wall.point(109,18-2*0.19), all_wall.point(109,31), all_wall.point(92+2*0.19,31)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(87 + 2*0.19 ,31), all_wall.point(109,31), all_wall.point(109,37-2*0.19), all_wall.point(87+2*0.19,37-2*0.19)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(99 + 2*0.19 ,37-2*0.19), all_wall.point(109,37-2*0.19), all_wall.point(109,85), all_wall.point(99+2*0.19,85)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(87 + 2*0.19 ,44), all_wall.point(99+2*0.19,44), all_wall.point(99 + 2*0.19,50 - 2 * 0.19), all_wall.point(87+2*0.19,50-2 * 0.19)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(87 + 2*0.19 ,57), all_wall.point(99+2*0.19,57), all_wall.point(99 + 2*0.19,64 - 2 * 0.19), all_wall.point(87+2*0.19,64- 2*0.19)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(61 + 2*0.19 ,70), all_wall.point(99+2*0.19,70), all_wall.point(99 + 2*0.19,85), all_wall.point(61 + 2 * 0.19,85)]
				group: groups.wall
			}
			PolygonCollider
			{
				vertexes: [all_wall.point(8 ,85), all_wall.point(109,85), all_wall.point(109,100), all_wall.point(8,100)]
				group: groups.wall
			}
		}

		Sensor
		{
			id: enter_1
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_interdite()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(55 ,71), Qt.point(61,71), Qt.point(61,87), Qt.point(55,87)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(55 ,71), Qt.point(61,71), Qt.point(61,87), Qt.point(55,87)]
				group: root.escape_ship ? 0 : groups.no_blink
				sensor: true
			}
		}
		Sensor
		{
			id: sortie
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_ok()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(55 ,80), Qt.point(61,80), Qt.point(61,87), Qt.point(55,87)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: enter_apis
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_apis()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(81 ,17), Qt.point(81,5), Qt.point(87,5), Qt.point(87,17)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(81 ,17), Qt.point(81,5), Qt.point(87,5), Qt.point(87,17)]
				group: groups.no_blink
				sensor: true
			}
		}
		Sensor
		{
			id: enter_course
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_course()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(30 ,17), Qt.point(30,5), Qt.point(36,5), Qt.point(36,17)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(30 ,17), Qt.point(30,5), Qt.point(36,5), Qt.point(36,17)]
				group: groups.no_blink
				sensor: true
			}
		}
		Sensor
		{
			id: enter_mafia
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_mafia()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(55 ,17), Qt.point(55,5), Qt.point(61,5), Qt.point(61,17)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(55 ,17), Qt.point(55,5), Qt.point(61,5), Qt.point(61,17)]
				group: groups.no_blink
				sensor: true
			}
		}
		//entrer/sortie d'autoroute 1
		Sensor
		{
			id: enter_autoroute_1
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.enter_auto_1()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(18 ,64), Qt.point(27,64), Qt.point(27,70), Qt.point(18,70)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(18 ,64), Qt.point(27,64), Qt.point(27,70), Qt.point(18,70)]
				group: groups.no_blink
				sensor: true
			}
		}
		Sensor
		{
			id: sortie_autoroute_1
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_auto_1()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(88 ,64), Qt.point(98,64), Qt.point(98,70), Qt.point(88,70)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(88 ,64), Qt.point(98,64), Qt.point(98,70), Qt.point(88,70)]
				group: groups.no_blink
				sensor: true
			}
		}

		//entrer/sortie d'autoroute 2
		Sensor
		{
			id: enter_autoroute_2
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.enter_auto_2()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(18 ,50), Qt.point(27,50), Qt.point(27,57), Qt.point(18,57)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(18 ,50), Qt.point(27,50), Qt.point(27,57), Qt.point(18,57)]
				group: groups.no_blink
				sensor: true
			}
		}
		Sensor
		{
			id: sortie_autoroute_2
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_auto_2()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(88 ,50), Qt.point(98,50), Qt.point(98,57), Qt.point(88,57)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(88 ,50), Qt.point(98,50), Qt.point(98,57), Qt.point(88,57)]
				group: groups.no_blink
				sensor: true
			}
		}

		//entrer/sortie d'autoroute 3
		Sensor
		{
			id: enter_autoroute_3
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.enter_auto_3()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(18 ,37), Qt.point(27,37), Qt.point(27,44), Qt.point(18,44)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(18 ,37), Qt.point(27,37), Qt.point(27,44), Qt.point(18,44)]
				group: groups.no_blink
				sensor: true
			}
		}
		Sensor
		{
			id: sortie_autoroute_3
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: 30
			scale: chapter_scale*2
			type: Body.STATIC
			onFuzzy_valueChanged:
			{
				if(fuzzy_value == 1)
				{
					root.sortie_auto_3()
				}
			}

			PolygonCollider
			{
				vertexes: [Qt.point(88 ,37), Qt.point(98,37), Qt.point(98,44), Qt.point(88,44)]
				group: root.level_begin ? groups.sensor : 0
				sensor: true
			}
			PolygonCollider
			{
				vertexes: [Qt.point(88 ,37), Qt.point(98,37), Qt.point(98,44), Qt.point(88,44)]
				group: groups.no_blink
				sensor: true
			}
		}

		//batiment administratif
		Sensor
		{
			id: administration_1
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(88.5 * 2 * chapter_scale,21.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.adm_visited_1()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/administration"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: administration_2
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(69.5 * 2 * chapter_scale,45.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.adm_visited_2()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/administration"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: administration_3
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(27.5 * 2 * chapter_scale,21.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.adm_visited_3()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/administration"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: administration_4
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(43.5 * 2 * chapter_scale,45.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.adm_visited_4()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/administration"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: administration_5
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(69.5 * 2 * chapter_scale, 59.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.adm_visited_5()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/administration"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: administration_6
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(43.5 * 2 * chapter_scale,33.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.adm_visited_6()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/administration"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: administration_7
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(43.5 * 2 * chapter_scale,59.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.adm_visited_7()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/administration"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: administration_8
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(69.5 * 2 * chapter_scale,33.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.adm_visited_8()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/administration"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}

		//batiment apis
		Sensor
		{
			id: apis_1
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(79.5 * 2 * chapter_scale,21.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.apis_visited()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/apis"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}

		//batiment course
		Sensor
		{
			id: course_1
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(37.5 * 2 * chapter_scale,21.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.course_visited()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/course"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}

		//batiment mafia
		Sensor
		{
			id: mafia_1
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(53.5 * 2 * chapter_scale,21.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.mafia_visited()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/mafia"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}
		Sensor
		{
			id: mafia_2
			duration_in: root.scene.seconds_to_frames(0.2)
			duration_out: root.scene.seconds_to_frames(6)
			position: Qt.point(62.5 * 2 * chapter_scale,21.5 * 2 * chapter_scale)
			scale: 4
			type: Body.STATIC
			property bool active: true
			onFuzzy_valueChanged:
			{
				if(active && fuzzy_value == 1)
				{
					root.mafia_visited()
					active = false
				}
				if(fuzzy_value == 0)
				{
					active = true
				}
			}
			Image
			{
				material: "chapter-5/building/mafia"
				z: altitudes.boss
			}
			CircleCollider
			{
				group: active ? groups.sensor : 0
				sensor: true
			}
		}

		//police
		Repeater
		{
			count: tab_pos_police.length
			Q.Component
			{
				Vehicle
				{
					id: police
					type: Body.STATIC
					property bool alive: true
					onAliveChanged: if(!alive) deleteLater()
					position: Qt.point(tab_pos_police[index][0] * 2 * chapter_scale,tab_pos_police[index][1] * 2 * chapter_scale)
					scale: 0.7
					Image
					{
						material: "equipments/drone-launcher"
						z: altitudes.boss + 0.00000001
					}
				}
			}
		}
	}

	//drone d'acceuil
	Q.Component
	{
		id: drone_entrer_factory
		Vehicle
		{
			id: drone
			angle: -Math.PI / 2
			property bool behaviour: false
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask: Qt.rgba(1, 0.25, 0.1, 0)
			max_angular_speed: 40
			max_speed: 0
			property int scan_ind: 0
			Image
			{
				material: "folks/insec/1"
			}
			Image
			{
				material: "chapter-5/scan/" + scan_ind
				z: altitudes.boss - 0.000001
				mask: drone.mask
				scale: 5
				position:Qt.point(0,-4.9)
			}

			Behaviour_attack_circle
			{
				enabled: drone.behaviour
				target_groups: groups.enemy_targets
				target_angle: 0
			}
		}
	}
	//animation du tout debut
	Q.Component
	{
		id: ship_begin
		Animation
		{
			id: timer
			time: 25
			speed: -1
			property bool go: false
			property var drone_1
			property var drone_2
			property real angle_init_d1
			property real angle_init_d2
			property bool scan_begin: false
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real rayon: 7
			property real rotation_angle: 0
			property real nb_tour: 0.5
			property bool rotation_drone: false
			property bool scan_off: false
			property real drone_scan_y: 75
			property real fin_y: 70
			property real sens: 1
			property bool altern: true
			onTimeChanged:
			{
				if(ship)
				{
					if(sens * (ship.position.y - drone_scan_y * 4) > 0 )
					{
						ship.position.y -= sens * 2*11.5 * 4 / root.scene.seconds_to_frames(25)
					}
					else
					{
						if(go)
						{
							if(sens * (ship.position.y - fin_y * 4) > 0)
							{
								ship.position.y -= sens * 2*11.5 * 4 / root.scene.seconds_to_frames(25)
							}
							else
							{
								root.enter_finish()
								alive = false
							}
						}
						else
						{
							if(!rotation_drone && (drone_2.position.x - ship.position.x) * (drone_2.position.x - ship.position.x) > rayon * rayon)
							{
								drone_1.position.x -=0.1
								drone_2.position.x +=0.1
							}
							else
							{
								if(!scan_begin)
								{
									root.ready_enter_bot()
									scan_begin = true
									drone_1.mask.a = 1
									drone_2.mask.a = 1
								}
								else
								{
									if(!rotation_drone)
									{
										//animation scan
										if(drone_1.scan_ind != 9)
										{
											if(altern)
											{
												drone_1.scan_ind++
												drone_2.scan_ind++
											}
											altern = !altern
										}
										else
										{
											rotation_drone = true
											altern = true
										}
									}
									else
									{
										if(rotation_angle < nb_tour * 2 * Math.PI)
										{
											rotation_angle += 0.02
											if(rotation_angle > nb_tour * 2 * Math.PI)
											{
												rotation_angle = nb_tour * 2 * Math.PI
											}
											drone_1.position.x = ship.position.x + rayon * Math.cos(rotation_angle)
											drone_1.position.y = ship.position.y + rayon * Math.sin(rotation_angle)
											drone_2.position.x = ship.position.x - rayon * Math.cos(rotation_angle)
											drone_2.position.y = ship.position.y - rayon * Math.sin(rotation_angle)
											drone_1.angle = angle_init_d1 + rotation_angle
											drone_2.angle = angle_init_d2 + rotation_angle
										}
										else
										{
											if(!scan_off)
											{
												//animation scan inverse
												if(drone_1.scan_ind != 0)
												{
													if(altern)
													{
														drone_1.scan_ind--
														drone_2.scan_ind--
													}
													altern= !altern
												}
												else
												{
													drone_1.mask.a = 0
													drone_2.mask.a = 0
													scan_off= true
												}
											}
											else
											{
												if((drone_1.position.x - ship.position.x) * (drone_1.position.x - ship.position.x) < 100)
												{
													if((2 * nb_tour)%2 == 1)
													{
														drone_1.position.x -= 0.1
														drone_2.position.x += 0.1
													}
													else
													{
														drone_1.position.x += 0.1
														drone_2.position.x -= 0.1
													}
												}
												else
												{
													root.scan_finish()
													go = true
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: apis_come
		Animation
		{
			id: timer
			property var apis
			property bool first: true
			property real rotation_angle: 0
			property real distance_ship: 2.5
			property real angle_ini: Math.PI
			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged: if(!alive) destroy()

			Q.Component.onDestruction:
			{
				if(apis)
				{
					apis.deleteLater()
				}
			}

			onTimeChanged:
			{
				if(first)
				{
					apis = drone_apis_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y - 20), angle: angle_ini})
					first = false
				}
				else
				{
					if(rotation_angle == 0 && (ship.position.y - apis.position.y)*(ship.position.y - apis.position.y) > distance_ship * distance_ship)
					{
						apis.position.y += 20 / scene.seconds_to_frames(3.5)
					}
					else
					{
						if(rotation_angle < Math.PI)
						{
							if(rotation_angle == 0)
							{
								root.apis_parle()
							}
							rotation_angle += 0.04
							if(rotation_angle > Math.PI)
							{
								rotation_angle = Math.PI
							}
							apis.angle = angle_ini + rotation_angle
						}
						else
						{
							if((ship.position.y - apis.position.y)*(ship.position.y - apis.position.y) < 400)
							{
								apis.position.y -= 20 / scene.seconds_to_frames(3.5)
							}
							else
							{
								root.apis_finish()
								alive = false
							}
						}
					}
				}
			}
		}
	}
	//drone police
	Q.Component
	{
		id: drone_police_1
		Vehicle
		{
			id: drone
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			icon: 2
			icon_color: "red"
			max_angular_speed: 40
			max_speed: 12
			loot_factory: Repeater
			{
				count: 1
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/1"
			}
			EquipmentSlot
			{
				scale: 0.5
				equipment:LaserGun
				{
					shooting: true
					level: 2
				}
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_police_2
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 128 - 1, y / 128 - 1)} // plink fait des images de drone en 256*256
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 40
			icon: 2
			icon_color: "red"
			scale: 1.5
			max_speed: 12
			loot_factory: Repeater
			{
				count: 1
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/2"
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(74,128)
				equipment: ShieldCanceler
				{
					range_bullet: 30
					level: 0
					shooting: true
					period: root.scene.seconds_to_frames(8)
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(182,128)
				equipment: AcidGun
				{
					shooting: true
					level: 2
				}
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_police_3
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 128 - 1, y / 128 - 1)} // plink fait des images de drone en 256*256
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 40
			icon: 2
			icon_color: "red"
			scale: 1.5
			max_speed: 12
			loot_factory: Repeater
			{
				count: 1
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/3"
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(128,176)
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(74,100)
				Q.Component.onCompleted: equipment = root.tab_weapons[root.hardcore ? 4 : 0].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(182,100)
				Q.Component.onCompleted: equipment = root.tab_weapons[root.hardcore ? 4 : 0].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_police_5
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 40
			icon: 2
			icon_color: "red"
			scale: 2
			max_speed: 12
			loot_factory: Repeater
			{
				count: 2
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "folks/insec/5"
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(406,269)
				Q.Component.onCompleted: equipment = root.tab_weapons[root.hardcore ? 3 : 1].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(106,269)
				Q.Component.onCompleted: equipment = root.tab_weapons[root.hardcore ? 3 : 1].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(200,310)
				Q.Component.onCompleted: equipment = root.tab_weapons[root.hardcore ? 4 : 0].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(312,310)
				Q.Component.onCompleted: equipment = root.tab_weapons[root.hardcore ? 4 : 0].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				scale: 0.5 / drone.scale
				position: drone.coords(256, 411)
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			CircleCollider
			{
				group: groups.enemy_hull_naked
			}
		}
	}

	//générateur police
	Q.Component
	{
		id: police_spawner
		Animation
		{
			id: timer
			property real mult: root.chapter_scale * 2
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real last_time: 7
			property real time_to_respawn: 15
			property int nb_pop: 1
			property var tab_drone_police:[drone_police_1,drone_police_2,drone_police_3,drone_police_5]
			property var tab_unit:[
			[1,0,0,0],
			[0,1,0,0],
			[0,0,1,0],
			[0,0,0,1],
			[0,1,0,1]
			]
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(root.police_active)
				{
					if(last_time - time > time_to_respawn)
					{
						last_time = time
						for(var i=0 ; i< tab_unit[root.police - 1].length; ++i)
						{
							for(var j = 0 ; j<root.tab_pos_police.length; ++j)
							{
								if(tab_unit[root.police - 1][i] > 0)
								{
									tab_drone_police[i].createObject(null, {scene: scene,position: Qt.point(root.tab_pos_police[j][0]*mult, root.tab_pos_police[j][1]*mult), angle: 2 * Math.PI / (i + 1) })
								}
							}
						}
						if(root.police < 5)
						{
							root.police_active = false
						}
					}
				}
			}
		}
	}

	//deplacement vaisseau hors de zone interdite
	Q.Component
	{
		id: move_ship_to
		Animation
		{
			id: timer

			//ship_angle entre -pi et pi
			function calcul_ship_angle()
			{
				var result = ship.angle
				while(result < -Math.PI)
				{
					result += 2 * Math.PI
				}
				while(result > Math.PI)
				{
					result -= 2 * Math.PI
				}
				return result
			}

			property real epsilon: 0.5
			property real x
			property real long_x
			property real y
			property real long_y
			property real angle
			property real long_angle
			property real time_to_move: 1
			property bool first: true
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(time < 6.9)
				{
					if(first)
					{
						ship.angle = calcul_ship_angle()
						long_x = ship.position.x - timer.x
						long_y = ship.position.y - timer.y
						long_angle = ship.angle - timer.angle
						first = false
					}
					else
					{
						if(ship)
						{
							if(root.scene.seconds_to_frames(6.9) - root.scene.seconds_to_frames(time) < root.scene.seconds_to_frames(time_to_move) + 2)
							{
								ship.position.x -= long_x / root.scene.seconds_to_frames(time_to_move)
								ship.position.y -= long_y / root.scene.seconds_to_frames(time_to_move)
								ship.angle -= long_angle / root.scene.seconds_to_frames(time_to_move)
							}
							else
							{
								root.ship_ok()
								alive = false
							}
						}
					}
				}
			}
		}
	}

	//générateur drone administratif haut
	Q.Component
	{
		id: drone_adm_haut
		Animation
		{
			id: timer
			property var tab_pos: [[25,20],[91,23]]
			property real mult: root.chapter_scale * 2
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real last_time: 7
			property real time_to_pop: 1
			property int nb_pop: 0
			property int max_pop: 54
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(last_time - time > time_to_pop)
				{
					last_time = time
					if(nb_pop < max_pop)
					{
						drone_factory.createObject(null, {scene: scene, max_speed: 10 ,position: Qt.point(tab_pos[0][0]*mult, tab_pos[0][1]*mult), angle: Math.PI/2, indice: root.ind_general_drone })
						checkpoint_factory.createObject(null, {scene: scene, indice: root.ind_general_drone, tab_indice: 0, tab: tab_checkpoint_enemy_3 , escape_pos: [[58,21],[58,-10]]})
						nb_pop++
						root.ind_general_drone++
						drone_factory.createObject(null, {scene: scene, max_speed: 10 ,position: Qt.point(tab_pos[1][0]*mult, tab_pos[1][1]*mult), angle: Math.PI/2, indice: root.ind_general_drone })
						checkpoint_factory.createObject(null, {scene: scene, indice: root.ind_general_drone, tab_indice: 2, tab: tab_checkpoint_enemy_3, escape_pos: [[58,21],[58,-10]]})
						nb_pop++
						root.ind_general_drone++
					}
					else
					{
						alive = false
					}
				}
			}
		}
	}
	//générateur drone administratif gauche
	Q.Component
	{
		id: drone_adm_gauche
		Animation
		{
			id: timer
			property var tab_pos: [33,20]
			property real mult: root.chapter_scale * 2
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real last_time: 7
			property real time_to_pop: 1
			property int nb_pop: 0
			property int max_pop: 25
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(last_time - time > time_to_pop)
				{
					last_time = time
					if(nb_pop < max_pop)
					{
						drone_factory.createObject(null, {scene: scene, max_speed: 10 ,position: Qt.point(tab_pos[0]*mult, tab_pos[1]*mult), angle: Math.PI, indice: root.ind_general_drone})
						checkpoint_factory.createObject(null, {scene: scene, indice: root.ind_general_drone, tab_indice: 3, tab: tab_checkpoint_enemy, escape_pos: [[36,40],[0,40]]})
						nb_pop++
						root.ind_general_drone++
					}
					else
					{
						alive = false
					}
				}
			}
		}
	}
	//générateur drone administratif droit
	Q.Component
	{
		id: drone_adm_droit
		Animation
		{
			id: timer
			property var tab_pos: [84,20]
			property real mult: root.chapter_scale * 2
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real last_time: 7
			property real time_to_pop: 1
			property int nb_pop: 0
			property int max_pop: 25
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(last_time - time > time_to_pop)
				{
					last_time = time
					if(nb_pop < max_pop)
					{
						drone_factory.createObject(null, {scene: scene, max_speed: 10 ,position: Qt.point(tab_pos[0]*mult, tab_pos[1]*mult), angle: Math.PI, indice: root.ind_general_drone})
						checkpoint_factory.createObject(null, {scene: scene, indice: root.ind_general_drone, tab_indice: 3, tab: tab_checkpoint_enemy_2, escape_pos: [[80,40],[200,40]]})
						nb_pop++
						root.ind_general_drone++
					}
					else
					{
						alive = false
					}
				}
			}
		}
	}

	//générateur autoroutier en bas
	Q.Component
	{
		id: drone_autoroute_1
		Animation
		{
			id: timer
			property var tab_pos: [[98.5,65],[98.5,66],[19.5,68],[19.5,69]]
			function random_next_pos_angle()
			{
				var a = random_index(timer.tab_pos.length)
				var angle = Math.PI / 2
				var y_min = a
				var y_max = timer.tab_pos.length - y_min - 1
				if(y_max < y_min)
				{
					var temp = y_min
					y_min = y_max
					y_max = temp
				}
				return [Qt.point(tab_pos[y_min][0] * root.chapter_scale * 2,tab_pos[y_min][1]* root.chapter_scale * 2), Qt.point(tab_pos[y_max][0] * root.chapter_scale * 2, tab_pos[y_max][1]* root.chapter_scale * 2) ]
			}
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real last_time: 7
			property real time_to_pop: 0.5
			property int nb_pop: 0
			property int max_pop: 40
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(last_time - time > time_to_pop)
				{
					last_time = time
					if(nb_pop < max_pop)
					{
						var pos_angle = timer.random_next_pos_angle()
						drone_autoroute_factory.createObject(null, {scene: scene, max_speed: 15 ,position: pos_angle[0], angle: - Math.PI / 2, min_x: pos_angle[1].x, max_x: pos_angle[0].x})
						drone_autoroute_factory.createObject(null, {scene: scene, max_speed: 15 ,position: pos_angle[1], angle: Math.PI / 2, min_x: pos_angle[1].x, max_x: pos_angle[0].x})
						nb_pop++
					}
					else
					{
						train_factory.createObject(null, {scene: scene ,position: Qt.point(22.5 * root.chapter_scale * 2, 67 * root.chapter_scale * 2), min_x: 22.5 * root.chapter_scale * 2, max_x: 98.5 * root.chapter_scale * 2, angle: Math.PI / 2})
						alive = false
					}
				}
			}
		}
	}

	//générateur autoroutier en milieu
	Q.Component
	{
		id: drone_autoroute_2
		Animation
		{
			id: timer
			property var tab_pos: [[98.5,51],[98.5,52],[19.5,55],[19.5,56]]
			function random_next_pos_angle()
			{
				var a = random_index(timer.tab_pos.length)
				var angle = Math.PI / 2
				var y_min = a
				var y_max = timer.tab_pos.length - y_min - 1
				if(y_max < y_min)
				{
					var temp = y_min
					y_min = y_max
					y_max = temp
				}
				return [Qt.point(tab_pos[y_min][0] * root.chapter_scale * 2,tab_pos[y_min][1]* root.chapter_scale * 2), Qt.point(tab_pos[y_max][0] * root.chapter_scale * 2, tab_pos[y_max][1]* root.chapter_scale * 2) ]
			}
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real last_time: 7
			property real time_to_pop: 0.5
			property int nb_pop: 0
			property int max_pop: 40
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(last_time - time > time_to_pop)
				{
					last_time = time
					if(nb_pop < max_pop)
					{
						var pos_angle = timer.random_next_pos_angle()
						drone_autoroute_factory.createObject(null, {scene: scene, max_speed: 15 ,position: pos_angle[0], angle: -Math.PI / 2, min_x: pos_angle[1].x, max_x: pos_angle[0].x})
						drone_autoroute_factory.createObject(null, {scene: scene, max_speed: 15 ,position: pos_angle[1], angle: Math.PI / 2, min_x: pos_angle[1].x, max_x: pos_angle[0].x})
						nb_pop++
					}
					else
					{
						train_factory.createObject(null, {scene: scene ,position: Qt.point(22.5 * root.chapter_scale * 2, 53 * root.chapter_scale * 2), min_x: 18.5 * root.chapter_scale * 2, max_x: 95.5 * root.chapter_scale * 2, angle: -Math.PI / 2})
						train_factory.createObject(null, {scene: scene ,position: Qt.point(22.5 * root.chapter_scale * 2, 54 * root.chapter_scale * 2), min_x: 22.5 * root.chapter_scale * 2, max_x: 98.5 * root.chapter_scale * 2, angle: Math.PI / 2})
						alive = false
					}
				}
			}
		}
	}

	//générateur autoroutier en haut
	Q.Component
	{
		id: drone_autoroute_3
		Animation
		{
			id: timer
			property var tab_pos: [[98.5,38],[98.5,39],[19.5,42],[19.5,43]]
			function random_next_pos_angle()
			{
				var a = random_index(timer.tab_pos.length)
				var angle = Math.PI / 2
				var y_min = a
				var y_max = timer.tab_pos.length - y_min - 1
				if(y_max < y_min)
				{
					var temp = y_min
					y_min = y_max
					y_max = temp
				}
				return [Qt.point(tab_pos[y_min][0] * root.chapter_scale * 2,tab_pos[y_min][1]* root.chapter_scale * 2), Qt.point(tab_pos[y_max][0] * root.chapter_scale * 2, tab_pos[y_max][1]* root.chapter_scale * 2) ]
			}
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real last_time: 7
			property real time_to_pop: 0.5
			property int nb_pop: 0
			property int max_pop: 40
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(last_time - time > time_to_pop)
				{
					last_time = time
					if(nb_pop < max_pop)
					{
						var pos_angle = timer.random_next_pos_angle()
						drone_autoroute_factory.createObject(null, {scene: scene, max_speed: 15 ,position: pos_angle[0], angle: - Math.PI / 2, min_x: pos_angle[1].x, max_x: pos_angle[0].x})
						drone_autoroute_factory.createObject(null, {scene: scene, max_speed: 15 ,position: pos_angle[1], angle: Math.PI / 2, min_x: pos_angle[1].x, max_x: pos_angle[0].x})
						nb_pop++
					}
					else
					{
						train_factory.createObject(null, {scene: scene ,position: Qt.point(22.5 * root.chapter_scale * 2, 40 * root.chapter_scale * 2), min_x: 18.5 * root.chapter_scale * 2, max_x: 95.5 * root.chapter_scale * 2, angle: - Math.PI / 2})
						train_factory.createObject(null, {scene: scene ,position: Qt.point(22.5 * root.chapter_scale * 2, 41 * root.chapter_scale * 2), min_x: 22.5 * root.chapter_scale * 2, max_x: 98.5 * root.chapter_scale * 2, angle: Math.PI / 2})
						alive = false
					}
				}
			}
		}
	}

	Q.Component
	{
		id: drone_autoroute_factory
		Vehicle
		{
			id: drone
			property real max_x
			property real min_x
			property real value: 1000
			property int folk: random_index(10)
			property real ti_local: root.ti
			onTi_localChanged:
			{
				if(!root.hardcore)
				{
					if(drone.angle > 0)
					{
						if(drone.position.x > drone.max_x)
						{
							drone.position.x = min_x
						}
					}
					else
					{
						if(drone.position.x < drone.min_x)
						{
							drone.position.x = max_x
						}
					}
				}
			}
			max_speed: 10
			max_angular_speed: 6
			icon: 2
			icon_color: "blue"
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						scale: 2
						sound_scale: 10
					}
				}
			}
			Q.Component.onDestruction:
			{
				do_dmg(drone)
			}
			Image
			{
				material: "folks/townies/" + drone.folk
			}
			Behaviour_attack_missile
			{
				target_groups: [groups.enemy_checkpoint_end - 1]
				range: 10000000
			}
			CircleCollider
			{
				group: !root.level_begin ? groups.enemy_hull : groups.enemy_hull_naked
			}
		}
	}

	Q.Component
	{
		id: explosion_factory
		Explosion
		{
			scale: 2
			sound_scale: 10
		}
	}
	Q.Component
	{
		id: train_factory
		Vehicle
		{
			id: drone
			max_speed: 30
			max_angular_speed: 6
			icon: 2
			scale: 2
			icon_color: "gold"
			property int indice: 0
			property real value: 10000
			property real ti_local: root.ti
			property real max_x
			property real min_x
			onTi_localChanged:
			{
				if(!root.hardcore)
				{
					if(drone.angle > 0)
					{
						if(drone.position.x > drone.max_x)
						{
							drone.position.x = min_x
						}
					}
					else
					{
						if(drone.position.x < drone.min_x)
						{
							drone.position.x = max_x
						}
					}
				}
			}
			Q.Component.onDestruction:
			{
				do_dmg(drone)
				for(var i = 0;i<4;++i)
				{
					explosion_factory.createObject(null, {scene: scene,position: Qt.point(drone.position.x -2 *drone.scale * i * Math.sin(drone.angle), drone.position.y + Math.cos(drone.angle))})
				}
			}
			Image
			{
				material: "chapter-5/train-front"
			}
			Image
			{
				material: "chapter-5/train-car"
				position: Qt.point(0,2)
			}
			Image
			{
				material: "chapter-5/train-car"
				position: Qt.point(0,4)
			}
			Image
			{
				material: "chapter-5/train-car"
				position: Qt.point(0,6)
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
			}
			Behaviour_attack_missile
			{
				target_groups: [groups.enemy_checkpoint_end - 1]
				range: 10000000
			}
			PolygonCollider
			{
				vertexes: [Qt.point(-0.5,-1),Qt.point(0.5,-1),Qt.point(0.5,7),Qt.point(-0.5,7)]
				group: !root.level_begin ? groups.enemy_hull : groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			id: drone
			max_speed: 10
			max_angular_speed: 6
			icon: 2
			icon_color: "blue"
			property int folk: random_index(10)
			property real value: 1000
			property int indice: 0
			property bool on: false
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						scale: 2
						sound_scale: 10
					}
				}
			}
			Q.Component.onDestruction:
			{
				do_dmg(drone)
			}
			Image
			{
				material: "folks/townies/" + drone.folk
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
			}
			Behaviour_attack_missile
			{
				target_groups: [groups.enemy_checkpoint - drone.indice]
				range: 10000000
			}
			CircleCollider
			{
				group: !root.level_begin ? groups.enemy_hull : groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: checkpoint_factory
		Vehicle
		{
			id: check
			property int indice: 0
			property int tab_indice: 0
			property int tab_indice_hardcore: 0
			property bool on: false
			property var tab: root.tab_checkpoint_enemy
			property var escape_pos: [[36,40],[0,40]]
			property real x: root.hardcore ? escape_pos[tab_indice_hardcore][0] * root.chapter_scale * 2 : tab[tab_indice][0] * root.chapter_scale * 2
			property real y: root.hardcore ? escape_pos[tab_indice_hardcore][1] * root.chapter_scale * 2 : tab[tab_indice][1] * root.chapter_scale * 2
			property real ti_local: root.ti

			onTi_localChanged:
			{
				check.position.x = check.x
				check.position.y = check.y
			}

			onOnChanged:
			{
				if(on)
				{
					if(!hardcore)
					{
						tab_indice = (tab_indice + 1)%tab.length
					}
					else
					{
						tab_indice_hardcore = (tab_indice_hardcore + 1)%escape_pos.length
					}
					on = false
				}
			}

			Q.Component.onCompleted:
			{
				check.position = Qt.point(x,y)
			}

			CircleCollider
			{
				group: groups.enemy_checkpoint - check.indice
				sensor: true
			}
		}
	}
	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			function value_calc()
			{
				var res = 1
				if(root.police != 0)
				{
					if((Math.abs(root.ti) * 10) % 4 < 2)
					{
						res = 1 / (root.police + 1)
					}
				}
				return res
			}
			text: qsTr("Niveau d'alerte : %1").arg(root.police)
			Q.Component.onCompleted:
			{
				value = Qt.binding(value_calc)
			}
			property real value: root.police==0 ? 1 / (root.police + 1) : (root.ti%4 < 2 ? 1 / (root.police + 1) : 1)
			color: Qt.rgba(1, value , value, 1)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
			visible: root.police
		}
	}
	Q.Component
	{
		id: decision_factory
		AccidentDialog
		{
			matter: saved_game.matter
			cost: root.delit_value
			onAccept:
			{
				stack.pop()
				messages.add("townies/normal", qsTr("accident good"))
				saved_game.add_matter(- root.delit_value)
			}
			onReject:
			{
				stack.pop()
				messages.add("townies/normal", qsTr("accident bad"))
				if(root.police < 5) ++root.police
				root.police_active = true
			}
		}
	}
	Q.Component
	{
		id: drone_apis_factory
		Vehicle
		{
			id: drone
			max_speed: 10
			max_angular_speed: 6
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			icon: 2
			icon_color: "blue"
			Image
			{
				material: "folks/apis/4"
			}
		}
	}

	Q.Component
	{
		id: barriere_vertical_factory
		Vehicle
		{
			id: barriere
			type: Body.STATIC
			property real x:39
			property real y:64
			property int nb_repe: 26
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere.x - 0.375)*2*chapter_scale
						position.y: (barriere.y + 1.125 - index/4)*2*chapter_scale
						scale: 0.25*chapter_scale
						mask: Qt.rgba(0.58, 0, 0.82, 1)
					}
				}
			}
			Repeater
			{
				count: barriere.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere.x - 0.625)*2*chapter_scale
						position.y: (barriere.y + 1.125 - index / 4)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI
						mask: Qt.rgba(0.58, 0, 0.82, 1)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [coords(barriere.x - 0.75, barriere.y + 1.25 - (barriere.nb_repe+1)/4), coords(barriere.x - 0.25, barriere.y + 1.25 - (barriere.nb_repe+1)/4), coords(barriere.x - 0.25, barriere.y + 1.25), coords(barriere.x - 0.75, barriere.y + 1.25)]
				group: groups.enemy_invincible
			}
		}
	}

	Q.Component
	{
		id: barriere_horizontal_factory
		Vehicle
		{
			id: barriere
			type: Body.STATIC
			property real x:60
			property real y:71
			property int nb_repe: 26
			property bool active: true
			function coords(x,y)
			{
				return Qt.point(x * 2 *chapter_scale, y * 2 *chapter_scale)
			}
			Repeater
			{
				count: barriere.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere.y + 0.625)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82, barriere.active ? 1 : 0)
					}
				}
			}
			Repeater
			{
				count: barriere.nb_repe
				Q.Component
				{
					Image
					{
						material: "chapter-2/barrier-wall"
						position.x: (barriere.x + 1.125 - index / 4)*2*chapter_scale
						position.y: (barriere.y+0.375)*2*chapter_scale
						scale: 0.25*chapter_scale
						angle: -Math.PI / 2
						mask: Qt.rgba(0.58, 0, 0.82,barriere.active ? 1 : 0)
					}
				}
			}
			PolygonCollider
			{
				vertexes: [coords(barriere.x + 1.25 - barriere.nb_repe / 4, barriere.y + 0.25), coords(barriere.x + 1.25, barriere.y + 0.25), coords(barriere.x + 1.25, barriere.y + 0.75), coords(barriere.x + 1.25 - barriere.nb_repe / 4, barriere.y + 0.75)]
				group: barriere.active ? groups.enemy_invincible : 0
			}
		}
	}

	Q.Component
	{
		id: mini_boss_factory
		Vehicle
		{
			function local_coords(x, y) {return Qt.point(x / 540 - 1, y / 540 - 1)} // images en 1080*1080
			function group_of(x) {return x ? shield ? groups.enemy_hull : groups.enemy_hull_naked : 0}
			id: mini_boss
			icon: 5
			icon_color: "red"
			type: Body.STATIC
			scale: 6
			Q.Component.onCompleted: shield = max_shield
			property bool onfire: behaviour.shooting
			max_speed: 0
			max_angular_speed: 0
			property bool shield_1: true
			property bool shield_2: true
			property bool shield_3: true
			property bool quipoutre_1: true
			property bool quipoutre_2: true
			property bool quipoutre_3: true
			readonly property bool alive: shield_1 || shield_2 || shield_3 || quipoutre_1 || quipoutre_2 || quipoutre_3
			onAliveChanged: if(!alive) destroy()
			Q.Component.onDestruction:
			{
				root.escape_ship = true
				barriere.active = false
			}
			loot_factory: Repeater
			{
				count: 10
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "chapter-2/mini-boss-ss"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: Qt.rgba(0, 0.4, 1, mini_boss.shield / mini_boss.max_shield)
				position: local_coords(540, 717)
				scale: 510/540
				z: altitudes.shield
			}
			Behaviour_attack_distance
			{
				id: behaviour
				target_groups: groups.enemy_targets
				max_distance: 40
			}
			CircleCollider
			{
				position: local_coords(540, 717)
				group: shield ? groups.enemy_shield : 0
				radius: 510/540
			}

			CircleCollider
			{
				group: group_of(quipoutre_1)
				radius: 80/540
				position: local_coords(223, 540)
			}
			EquipmentSlot
			{
				position: local_coords(223, 540)
				scale: 0.15*6/mini_boss.scale
				angle: Math.PI * -1/4
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_1 = false
				}
			}

			CircleCollider
			{
				group: group_of(quipoutre_2)
				radius: 80/540
				position: local_coords(540, 540)
			}
			EquipmentSlot
			{
				position: local_coords(540, 540)
				scale: 0.15*6/mini_boss.scale
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_2 = false
				}
			}

			CircleCollider
			{
				group: group_of(quipoutre_3)
				radius: 80/540
				position: local_coords(854, 540)
			}
			EquipmentSlot
			{
				position: local_coords(854, 540)
				scale: 0.15*6/mini_boss.scale
				angle: Math.PI * 1/4
				equipment: Quipoutre
				{
					level: 2
					shooting: mini_boss.onfire
					Q.Component.onDestruction: mini_boss.quipoutre_3 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_1)
				radius: 60/540
				position: local_coords(303, 848)
			}
			EquipmentSlot
			{
				position: local_coords(303, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_1 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_2)
				radius: 60/540
				position: local_coords(540, 848)
			}
			EquipmentSlot
			{
				position: local_coords(540, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_2 = false
				}
			}

			CircleCollider
			{
				group: group_of(shield_3)
				radius: 60/540
				position: local_coords(776, 848)
			}
			EquipmentSlot
			{
				position: local_coords(776, 848)
				scale: 0.15*6/mini_boss.scale
				equipment: SecondaryShield
				{
					level: 2
					Q.Component.onDestruction: mini_boss.shield_3 = false
				}
			}
		}
	}

	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_story_0 //state_test_1 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					camera_position= undefined
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					/*
					root.level_begin = true
					*/
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				guard: etat_du_niveau != 3
				onTriggered:
				{
					camera_position= undefined
					//creation des differents enemy
					drone_autoroute_1.createObject(null, {parent: scene})
					drone_autoroute_2.createObject(null, {parent: scene})
					drone_autoroute_3.createObject(null, {parent: scene})
					drone_adm_droit.createObject(null, {parent: scene})
					drone_adm_gauche.createObject(null, {parent: scene})
					drone_adm_haut.createObject(null, {parent: scene})
					police_spawner.createObject(null, {parent: scene})
					saved_game.set("travel/chapter-5/chapter-5", false)
					saved_game.set("travel/chapter-2/chapter-2c", false)
					if(!secret_tomate)
					{
						saved_game.set("travel/chapter-4/chapter-4b", false)
					}
					music.objectName = "chapter-5/Neolith"
				}
			}
			//penser au signal transition lors de la fuite

			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				guard: etat_du_niveau == 3
				onTriggered:
				{
					camera_position= undefined
					music.objectName = "chapter-2/fuite"
					barriere_vertical_factory.createObject(null, {scene: scene, x:28,y:69})
					barriere_vertical_factory.createObject(null, {scene: scene, x:88,y:69})
					barriere_vertical_factory.createObject(null, {scene: scene, x:28,y:56,nb_repe: 30})
					barriere_vertical_factory.createObject(null, {scene: scene, x:28,y:43,nb_repe: 30})
					barriere_vertical_factory.createObject(null, {scene: scene, x:88,y:56,nb_repe: 30})
					barriere_vertical_factory.createObject(null, {scene: scene, x:88,y:43,nb_repe: 30})
					barriere_horizontal_factory.createObject(null, {scene: scene, x:60,y:15,nb_repe: 26})
					barriere_horizontal_factory.createObject(null, {scene: scene, x:35,y:17,nb_repe: 26})
					barriere_horizontal_factory.createObject(null, {scene: scene, x:86,y:17,nb_repe: 26})
					barriere = barriere_horizontal_factory.createObject(null, {scene: scene, x:60,y:72,nb_repe: 26})
					//creer bourrin à la sortie
					mini_boss_factory.createObject(null, {scene: scene, position: Qt.point(58 * 2 * chapter_scale, 71 * 2 * chapter_scale)})
					police = 5
				}
			}
		}
		State
		{
			id: state_story_1
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: state_story_1.propertiesAssigned
				guard: etat_du_niveau == 0
				onTriggered:
				{
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(60.5 * 2 * chapter_scale, 82.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(55.5 * 2 * chapter_scale, 82.5 * 2 * chapter_scale), angle: Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(60.5 * 2 * chapter_scale, 80 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(55.5 * 2 * chapter_scale, 80 * 2 * chapter_scale), angle: Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(60.5 * 2 * chapter_scale, 77.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(55.5 * 2 * chapter_scale, 77.5 * 2 * chapter_scale), angle: Math.PI / 2 })
					drone_scan_1 = drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(60.5 * 2 * chapter_scale, 75 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_scan_2 = drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(55.5 * 2 * chapter_scale, 75 * 2 * chapter_scale), angle: Math.PI / 2})
					ship_begin.createObject(null, {parent: scene, drone_1 : drone_scan_1, drone_2: drone_scan_2, angle_init_d1: - Math.PI / 2 , angle_init_d2 : Math.PI / 2 })
					messages.add("nectaire/normal", qsTr("avant identification 1"))
					messages.add("lycop/normal", qsTr("avant identification 2"))
					messages.add("nectaire/normal", qsTr("avant identification 3"))
				}
			}

			SignalTransition
			{
				targetState: state_story_4
				signal: state_story_1.propertiesAssigned
				guard: etat_du_niveau == 1
				onTriggered:
				{
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(86.5 * 2 * chapter_scale, 5.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(81.5 * 2 * chapter_scale, 5.5 * 2 * chapter_scale), angle: Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(86.5 * 2 * chapter_scale, 8 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(81.5 * 2 * chapter_scale, 8 * 2 * chapter_scale), angle: Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(86.5 * 2 * chapter_scale, 10.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(81.5 * 2 * chapter_scale, 10.5 * 2 * chapter_scale), angle: Math.PI / 2 })
					drone_scan_1 = drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(86.5 * 2 * chapter_scale, 13 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_scan_2 = drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(81.5 * 2 * chapter_scale, 13 * 2 * chapter_scale), angle: Math.PI / 2 })
					apis_come.createObject(null, {parent: scene})
				}
			}

			SignalTransition
			{
				targetState: state_story_7
				signal: state_story_1.propertiesAssigned
				guard: etat_du_niveau == 2
				onTriggered:
				{
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(35.5 * 2 * chapter_scale, 5.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(30.5 * 2 * chapter_scale, 5.5 * 2 * chapter_scale), angle: Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(35.5 * 2 * chapter_scale, 8 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(30.5 * 2 * chapter_scale, 8 * 2 * chapter_scale), angle: Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(35.5 * 2 * chapter_scale, 10.5 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(30.5 * 2 * chapter_scale, 10.5 * 2 * chapter_scale), angle: Math.PI / 2 })
					drone_scan_1 = drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(35.5 * 2 * chapter_scale, 13 * 2 * chapter_scale), angle: - Math.PI / 2 })
					drone_scan_2 = drone_entrer_factory.createObject(null, {scene: scene, position: Qt.point(30.5 * 2 * chapter_scale, 13 * 2 * chapter_scale), angle: Math.PI / 2 })
					apis_come.createObject(null, {parent: scene})
				}
			}
			SignalTransition
			{
				targetState: state_story_10
				signal: state_story_1.propertiesAssigned
				guard: etat_du_niveau == 3
				onTriggered:
				{
					apis_come.createObject(null, {parent: scene, distance_ship: 7})
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_2; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: ready_enter_bot
				onTriggered:
				{
					messages.add("insec/normal", qsTr("identification 1 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: scan_finish
				onTriggered:
				{
					messages.add("insec/normal", qsTr("identification 1 2"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: enter_finish
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("enter 1 1"))
					messages.add("nectaire/normal", qsTr("enter 1 2"))
					messages.add("lycop/normal", qsTr("enter 1 3"))
					root.level_begin = true
				}
			}
		}
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_2; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}
		State {id: state_dialogue_2; SignalTransition {targetState: state_story_3; signal: state_dialogue_2.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_3
			AssignProperty {target: scene; property: "running"; value: true}

			//en cas d'accident
			SignalTransition
			{
				targetState: state_story_3
				signal: dmg_done
				guard: (root.police < 5 && saved_game.matter >= root.delit_value) || (!root.police_active && root.police < 5)
				onTriggered:
				{
					stack.push(decision_factory)
				}
			}
			SignalTransition
			{
				targetState: state_story_3
				signal: dmg_done
				guard: root.police < 5 && saved_game.matter < root.delit_value && root.police_active
				onTriggered:
				{
					if(root.police < 5)
					{
						root.police++
					}
					root.police_active = true
				}
			}

			//administration général
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_1
				guard: root.administration_visited == 0
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration ok 1 1"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_1
				guard: root.administration_visited != 0
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 1 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_2
				guard: root.administration_visited == 1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration ok 2 1"))
					messages.add("lycop/neutral", qsTr("admistration ok 2 2"))
					messages.add("lycop/neutral", qsTr("admistration ok 2 3"))
					messages.add("insec/normal", qsTr("admistration ok 2 4"))
					messages.add("lycop/surprised", qsTr("admistration ok 2 5"))
					messages.add("nectaire/normal", qsTr("admistration ok 2 6"))
					messages.add("lycop/normal", qsTr("admistration ok 2 7"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_2
				guard: root.administration_visited != 1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 2 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_3
				guard: root.administration_visited == 2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration ok 3 1"))
					messages.add("nectaire/normal", qsTr("admistration ok 3 2"))
					messages.add("lycop/normal", qsTr("admistration ok 3 3"))
					messages.add("insec/normal", qsTr("admistration ok 3 4"))
					messages.add("lycop/normal", qsTr("admistration ok 3 5"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_3
				guard: root.administration_visited != 2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 3 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_4
				guard: root.administration_visited == 3
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration ok 4 1"))
					messages.add("nectaire/panic", qsTr("admistration ok 4 2"))
					messages.add("lycop/normal", qsTr("admistration ok 4 3"))
					messages.add("insec/normal", qsTr("admistration ok 4 4"))
					messages.add("lycop/neutral", qsTr("admistration ok 4 5"))
					messages.add("lycop/neutral", qsTr("admistration ok 4 6"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_4
				guard: root.administration_visited != 3
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 4 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_5
				guard: root.administration_visited == 4
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration ok 5 1"))
					messages.add("nectaire/panic", qsTr("admistration ok 5 2"))
					messages.add("lycop/angry", qsTr("admistration ok 5 3"))
					messages.add("insec/normal", qsTr("admistration ok 5 4"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_5
				guard: root.administration_visited != 4
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 5 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_6
				guard: root.administration_visited == 5
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration ok 6 1"))
					messages.add("lycop/angry", qsTr("admistration ok 6 2"))
					messages.add("insec/normal", qsTr("admistration ok 6 3"))
					messages.add("lycop/neutral", qsTr("admistration ok 6 4"))
					messages.add("insec/normal", qsTr("admistration ok 6 5"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_6
				guard: root.administration_visited != 5
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 6 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_7
				guard: root.administration_visited == 6
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration ok 7 1"))
					messages.add("lycop/normal", qsTr("admistration ok 7 2"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_7
				guard: root.administration_visited != 6
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 7 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_8
				guard: root.administration_visited == 7
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration ok 8 1"))
					messages.add("lycop/normal", qsTr("admistration ok 8 2"))
					messages.add("nectaire/normal", qsTr("admistration ok 8 3"))
					messages.add("insec/normal", qsTr("admistration ok 8 4"))
					messages.add("lycop/normal", qsTr("admistration ok 8 5"))
					messages.add("insec/normal", qsTr("admistration ok 8 6"))
					messages.add("nectaire/normal", qsTr("admistration ok 8 7"))
					messages.add("lycop/normal", qsTr("admistration ok 8 8"))
					messages.add("insec/normal", qsTr("admistration ok 8 9"))
					messages.add("nectaire/normal", qsTr("admistration ok 8 10"))
					messages.add("lycop/normal", qsTr("admistration ok 8 11"))
					messages.add("nectaire/normal", qsTr("admistration ok 8 12"))
					messages.add("lycop/normal", qsTr("admistration ok 8 13"))
					messages.add("nectaire/normal", qsTr("admistration ok 8 14"))
					messages.add("lycop/normal", qsTr("admistration ok 8 15"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: adm_visited_8
				guard: root.administration_visited != 7
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 8 1"))
				}
			}

			//administration des differente zone
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: apis_visited
				onTriggered:
				{
					messages.add("apis/normal", qsTr("apis description 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: course_visited
				onTriggered:
				{
					messages.add("insec/normal", qsTr("course description 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_3
				signal: mafia_visited
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("mafia description 1"))
				}
			}
			//sortie interdite
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_interdite
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("sortie interdite 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 58 * 2 * chapter_scale, y: 70 * 2 * chapter_scale, angle: 0})
				}
			}

			//apis interdit
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_apis
				guard: root.administration_visited != 8
				onTriggered:
				{
					messages.add("apis/normal", qsTr("sortie apis interdite 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 84 * 2 * chapter_scale, y: 18 * 2 * chapter_scale, angle: Math.PI})
				}
			}
			//mafia interdit
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_mafia
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("sortie mafia interdite 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 58 * 2 * chapter_scale, y: 18 * 2 * chapter_scale, angle: Math.PI})
				}
			}
			//course interdit
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_course
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie course interdite 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 33 * 2 * chapter_scale, y: 18 * 2 * chapter_scale, angle: Math.PI})
				}
			}

			//sortie autoroute
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: enter_auto_1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 29.5 * 2 * chapter_scale, y: 64 * 2 * chapter_scale, angle: Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_auto_1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 86.5 * 2 * chapter_scale, y: 64 * 2 * chapter_scale, angle: -Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: enter_auto_2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 29.5 * 2 * chapter_scale, y: 50 * 2 * chapter_scale, angle: Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_auto_2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 86.5 * 2 * chapter_scale, y: 50 * 2 * chapter_scale, angle: -Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: enter_auto_3
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 29.5 * 2 * chapter_scale, y: 37 * 2 * chapter_scale, angle: Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_auto_3
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 86.5 * 2 * chapter_scale, y: 37 * 2 * chapter_scale, angle: -Math.PI / 2})
				}
			}

			//transition vers la sortie apis
			SignalTransition
			{
				targetState: state_end
				signal: sortie_apis
				guard: root.administration_visited == 8
				onTriggered:
				{
					saved_game.add_science(1)
					saved_game.set("niveau_5", 1)
					saved_game.set("niveau_alerte", root.police)
					saved_game.set("file", "game/chapter-5/apis-zone.qml")
					saved_game.save()
				}
			}
		}
		State {id: state_dialogue_3; SignalTransition {targetState: state_story_3; signal: state_dialogue_3.propertiesAssigned; guard: !messages.has_unread}}
		State {id: state_dialogue_4; SignalTransition {targetState: state_transition; signal: state_dialogue_4.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_transition
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_story_3
				signal: ship_ok
				guard: etat_du_niveau == 0
				onTriggered:
				{
					root.level_begin = true
				}
			}
			SignalTransition
			{
				targetState: state_story_6
				signal: ship_ok
				guard: etat_du_niveau == 1
				onTriggered:
				{
					root.level_begin = true
				}
			}
			SignalTransition
			{
				targetState: state_story_9
				signal: ship_ok
				guard: etat_du_niveau == 2
				onTriggered:
				{
					root.level_begin = true
				}
			}
		}

		State
		{
			id: state_story_4
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_5
				signal: apis_parle
				onTriggered:
				{
					messages.add("apis/normal", qsTr("apis parle 1 1"))
					messages.add("lycop/normal", qsTr("apis parle 1 2"))
					messages.add("apis/normal", qsTr("apis parle 1 3"))
					messages.add("nectaire/normal", qsTr("apis parle 1 4"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_6
				signal: apis_finish
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("apis parti 1 1"))
					messages.add("nectaire/normal", qsTr("apis parti 1 2"))
					messages.add("lycop/normal", qsTr("apis parti 1 3"))
					ship_begin.createObject(null, {parent: scene, drone_1 : drone_scan_1, drone_2: drone_scan_2, angle_init_d1: - Math.PI / 2 , angle_init_d2 : Math.PI / 2, drone_scan_y: 13, fin_y: 18, sens: -1, nb_tour: 1 })
				}
			}
		}
		State {id: state_dialogue_5; SignalTransition {targetState: state_story_4; signal: state_dialogue_5.propertiesAssigned; guard: !messages.has_unread}}
		State {id: state_dialogue_6; SignalTransition {targetState: state_story_5; signal: state_dialogue_6.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_story_5
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: enter_finish
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("enter 2 1"))
					root.level_begin = true
				}
			}
		}

		State {id: state_dialogue_7; SignalTransition {targetState: state_story_6; signal: state_dialogue_7.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_story_6
			AssignProperty {target: scene; property: "running"; value: true}
			//en cas d'accident
			SignalTransition
			{
				targetState: state_story_6
				signal: dmg_done
				guard: (root.police < 5 && saved_game.matter >= root.delit_value) || (!root.police_active && root.police < 5)
				onTriggered:
				{
					stack.push(decision_factory)
				}
			}
			SignalTransition
			{
				targetState: state_story_6
				signal: dmg_done
				guard: root.police < 5 && saved_game.matter < root.delit_value && root.police_active
				onTriggered:
				{
					if(root.police < 5)
					{
						root.police++
					}
					root.police_active = true
				}
			}

			//administration général
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: adm_visited_1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 1 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: adm_visited_2
				guard: root.administration_visited != 1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 2 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: adm_visited_2
				guard: root.administration_visited == 1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration ok course 1"))
					messages.add("nectaire/normal", qsTr("admistration ok course 2"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: adm_visited_3
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 3 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: adm_visited_4
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 4 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: adm_visited_5
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 5 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: adm_visited_6
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 6 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: adm_visited_7
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 7 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: adm_visited_8
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 8 1"))
				}
			}

			//administration des differente zone
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: apis_visited
				onTriggered:
				{
					messages.add("apis/normal", qsTr("apis description 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: course_visited
				guard: root.administration_visited != 0 && root.administration_visited != 2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("course description 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: course_visited
				guard: root.administration_visited == 0
				onTriggered:
				{
					messages.add("insec/normal", qsTr("course inscription 1"))
					messages.add("lycop/neutral", qsTr("course inscription 2"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_7
				signal: course_visited
				guard: root.administration_visited == 2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("course validation inscription 1"))
					root.administration_visited++
					root.course_open = true
				}
			}

			SignalTransition
			{
				targetState: state_dialogue_7
				signal: mafia_visited
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("mafia description 1"))
				}
			}
			//sortie interdite
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_interdite
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("sortie interdite 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 58 * 2 * chapter_scale, y: 70 * 2 * chapter_scale, angle: 0})
				}
			}

			//apis interdit
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_apis
				onTriggered:
				{
					messages.add("apis/normal", qsTr("sortie apis interdite 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 84 * 2 * chapter_scale, y: 18 * 2 * chapter_scale, angle: Math.PI})
				}
			}
			//mafia interdit
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_mafia
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("sortie mafia interdite 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 58 * 2 * chapter_scale, y: 18 * 2 * chapter_scale, angle: Math.PI})
				}
			}
			//course interdit
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_course
				guard: !root.course_open
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie course interdite 2"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 33 * 2 * chapter_scale, y: 18 * 2 * chapter_scale, angle: Math.PI})
				}
			}

			//sortie autoroute
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: enter_auto_1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 29.5 * 2 * chapter_scale, y: 64 * 2 * chapter_scale, angle: Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_auto_1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 86.5 * 2 * chapter_scale, y: 64 * 2 * chapter_scale, angle: -Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: enter_auto_2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 29.5 * 2 * chapter_scale, y: 50 * 2 * chapter_scale, angle: Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_auto_2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 86.5 * 2 * chapter_scale, y: 50 * 2 * chapter_scale, angle: -Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: enter_auto_3
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 29.5 * 2 * chapter_scale, y: 37 * 2 * chapter_scale, angle: Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_auto_3
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 86.5 * 2 * chapter_scale, y: 37 * 2 * chapter_scale, angle: -Math.PI / 2})
				}
			}

			//transition vers la sortie course
			SignalTransition
			{
				targetState: state_end
				signal: sortie_course
				guard: root.course_open
				onTriggered:
				{
					saved_game.set("niveau_5", 2)
					saved_game.set("niveau_alerte", root.police)
					saved_game.set("file", "game/chapter-5/course-zone.qml")
					saved_game.save()
				}
			}
		}

		State
		{
			id: state_story_7
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_8
				signal: apis_parle
				onTriggered:
				{
					messages.add("apis/normal", qsTr("apis parle 2 1"))
					messages.add("lycop/normal", qsTr("apis parle 2 2"))
					messages.add("apis/normal", qsTr("apis parle 2 3"))
					messages.add("lycop/normal", qsTr("apis parle 2 4"))
					messages.add("apis/normal", qsTr("apis parle 2 5"))
					messages.add("lycop/normal", qsTr("apis parle 2 6"))
					messages.add("apis/normal", qsTr("apis parle 2 7"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_9
				signal: apis_finish
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("apis parti 2 1"))
					messages.add("lycop/normal", qsTr("apis parti 2 2"))
					if(secret_tomate)
					{
						messages.add("nectaire/normal", qsTr("apis parti 2 3"))
						messages.add("lycop/normal", qsTr("apis parti 2 4"))
						messages.add("nectaire/normal", qsTr("apis parti 2 5"))
						messages.add("lycop/normal", qsTr("apis parti 2 6"))
						messages.add("nectaire/normal", qsTr("apis parti 2 7"))
						messages.add("lycop/normal", qsTr("apis parti 2 8"))
						messages.add("nectaire/normal", qsTr("apis parti 2 9"))
						messages.add("lycop/normal", qsTr("apis parti 2 10"))
					}
					ship_begin.createObject(null, {parent: scene, drone_1 : drone_scan_1, drone_2: drone_scan_2, angle_init_d1: - Math.PI / 2 , angle_init_d2 : Math.PI / 2, drone_scan_y: 13, fin_y: 18, sens: -1, nb_tour: 1 })
				}
			}
		}
		State {id: state_dialogue_8; SignalTransition {targetState: state_story_7; signal: state_dialogue_8.propertiesAssigned; guard: !messages.has_unread}}
		State {id: state_dialogue_9; SignalTransition {targetState: state_story_8; signal: state_dialogue_9.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_8
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: enter_finish
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("enter 3 1"))
					messages.add("nectaire/normal", qsTr("enter 3 2"))
					messages.add("lycop/normal", qsTr("enter 3 3"))
					root.level_begin = true
				}
			}
		}

		State {id: state_dialogue_10; SignalTransition {targetState: state_story_9; signal: state_dialogue_10.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_story_9
			AssignProperty {target: scene; property: "running"; value: true}
			//en cas d'accident
			SignalTransition
			{
				targetState: state_story_9
				signal: dmg_done
				guard: (root.police < 5 && saved_game.matter >= root.delit_value) || (!root.police_active && root.police < 5)
				onTriggered:
				{
					stack.push(decision_factory)
				}
			}
			SignalTransition
			{
				targetState: state_story_9
				signal: dmg_done
				guard: root.police < 5 && saved_game.matter < root.delit_value && root.police_active
				onTriggered:
				{
					if(root.police < 5)
					{
						root.police++
					}
					root.police_active = true
				}
			}

			//administration général
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: adm_visited_1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 1 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: adm_visited_2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 2 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: adm_visited_3
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 3 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: adm_visited_4
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 4 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: adm_visited_5
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 5 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: adm_visited_6
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 6 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: adm_visited_7
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 7 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: adm_visited_8
				guard: root.administration_visited != 1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration pas ok 8 1"))
				}
			}

			SignalTransition
			{
				targetState: state_dialogue_10
				signal: adm_visited_8
				guard: root.administration_visited == 1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("admistration ok mafia 8 1"))
					messages.add("nectaire/normal", qsTr("admistration ok mafia 8 2"))
					messages.add("lycop/normal", qsTr("admistration ok mafia 8 3"))
					root.administration_visited++
				}
			}
			//administration des differente zone
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: apis_visited
				onTriggered:
				{
					messages.add("apis/normal", qsTr("apis description 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: course_visited
				onTriggered:
				{
					messages.add("insec/normal", qsTr("course description 1"))
				}
			}

			SignalTransition
			{
				targetState: state_dialogue_10
				signal: mafia_visited
				guard: root.administration_visited != 0 && root.administration_visited != 2
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("mafia description 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: mafia_visited
				guard: root.administration_visited == 0
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("mafia 1"))
					messages.add("lycop/normal", qsTr("mafia 2"))
					messages.add("mafia/normal", qsTr("mafia 3"))
					messages.add("lycop/normal", qsTr("mafia 4"))
					messages.add("mafia/normal", qsTr("mafia 5"))
					messages.add("lycop/normal", qsTr("mafia 6"))
					root.administration_visited++
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_10
				signal: mafia_visited
				guard: root.administration_visited == 2
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("mafia accessible 1"))
					messages.add("lycop/normal", qsTr("mafia accessible 2"))
					root.administration_visited++
					root.mafia_open = true
				}
			}
			//sortie interdite
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_interdite
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("sortie interdite 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 58 * 2 * chapter_scale, y: 70 * 2 * chapter_scale, angle: 0})
				}
			}

			//apis interdit
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_apis
				onTriggered:
				{
					messages.add("apis/normal", qsTr("sortie apis interdite 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 84 * 2 * chapter_scale, y: 18 * 2 * chapter_scale, angle: Math.PI})
				}
			}
			//mafia interdit
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_mafia
				guard: !root.mafia_open
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("sortie mafia interdite 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 58 * 2 * chapter_scale, y: 18 * 2 * chapter_scale, angle: Math.PI})
				}
			}
			//course interdit
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_course
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie course interdite 3"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 33 * 2 * chapter_scale, y: 18 * 2 * chapter_scale, angle: Math.PI})
				}
			}

			//sortie autoroute
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: enter_auto_1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 29.5 * 2 * chapter_scale, y: 64 * 2 * chapter_scale, angle: Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_auto_1
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 86.5 * 2 * chapter_scale, y: 64 * 2 * chapter_scale, angle: -Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: enter_auto_2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 29.5 * 2 * chapter_scale, y: 50 * 2 * chapter_scale, angle: Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_auto_2
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 86.5 * 2 * chapter_scale, y: 50 * 2 * chapter_scale, angle: -Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: enter_auto_3
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 29.5 * 2 * chapter_scale, y: 37 * 2 * chapter_scale, angle: Math.PI / 2})
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: sortie_auto_3
				onTriggered:
				{
					messages.add("insec/normal", qsTr("sortie autoroute 1"))
					root.level_begin = false
					move_ship_to.createObject(null, {parent: scene, x: 86.5 * 2 * chapter_scale, y: 37 * 2 * chapter_scale, angle: -Math.PI / 2})
				}
			}

			//transition vers la sortie course
			SignalTransition
			{
				targetState: state_end
				signal: sortie_mafia
				guard: root.mafia_open
				onTriggered:
				{
					saved_game.set("niveau_5", 3)
					saved_game.set("niveau_alerte", root.police)
					saved_game.set("file", "game/chapter-5/mafia-zone.qml")
					saved_game.save()
				}
			}
		}

		State
		{
			id: state_story_10
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_11
				signal: apis_parle
				onTriggered:
				{
					messages.add("apis/normal", qsTr("apis parle 3 1"))
					messages.add("lycop/surprised", qsTr("apis parle 3 2"))
					messages.add("apis/normal", qsTr("apis parle 3 3"))
					messages.add("nectaire/normal", qsTr("apis parle 3 4"))
					messages.add("lycop/normal", qsTr("apis parle 3 5"))
					messages.add("apis/normal", qsTr("apis parle 3 6"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_12
				signal: apis_finish
				onTriggered:
				{
					police_spawner.createObject(null, {parent: scene, time: - 4})
					root.police_active = true
					root.level_begin = true
				}
			}
		}

		State {id: state_dialogue_11; SignalTransition {targetState: state_story_10; signal: state_dialogue_11.propertiesAssigned; guard: !messages.has_unread}}
		State {id: state_dialogue_12; SignalTransition {targetState: state_story_11; signal: state_dialogue_12.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_story_11
			AssignProperty {target: scene; property: "running"; value: true}
			
			SignalTransition
			{
				targetState: state_video_end
				signal: sortie_ok
				guard: escape_ship
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-5/fuite-chapitre5.ogg"
					function f ()
					{
						camera_position = Qt.point(58 * 4, 150 * 4)
						ship.position = Qt.point(58 * 4, 150 * 4)
						ship.angle = Math.PI
						saved_game.set("file", "game/chapter-5/boss-zone.qml")
						saved_game.save()
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
				
			}
		}
		State
		{
			id: state_video_end
			SignalTransition
			{
				targetState: state_end
				signal: state_video_end.propertiesAssigned
				guard:!screen_video.video.file
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}

	Q.Component.onCompleted:
	{
		jobs.run(music, "preload", ["chapter-5/Neolith"])
		jobs.run(music, "preload", ["chapter-2/fuite"])
		var a = ["c", "e", "n", "ne", "nei", "nw", "nwi", "s", "se", "sei", "sw", "swi", "w"]; for(var i in a) jobs.run(scene_view, "preload", ["chapter-2/metal/" + a[i]])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["chapter-5/scan/" + i])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["folks/townies/" + i])
		jobs.run(scene_view, "preload", ["chapter-5/building/apis"])
		jobs.run(scene_view, "preload", ["chapter-5/building/course"])
		jobs.run(scene_view, "preload", ["chapter-5/building/mafia"])
		jobs.run(scene_view, "preload", ["chapter-5/building/administration"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		jobs.run(scene_view, "preload", ["chapter-2/rectangle_noir"])
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["equipments/drone-launcher"])
		jobs.run(scene_view, "preload", ["folks/insec/1"])
		jobs.run(scene_view, "preload", ["folks/insec/2"])
		jobs.run(scene_view, "preload", ["folks/insec/3"])
		jobs.run(scene_view, "preload", ["folks/insec/5"])
		jobs.run(scene_view, "preload", ["chapter-5/train-front"])
		jobs.run(scene_view, "preload", ["chapter-5/train-car"])
		jobs.run(scene_view, "preload", ["folks/apis/4"])
		jobs.run(scene_view, "preload", ["chapter-2/barrier-wall"])
		jobs.run(scene_view, "preload", ["chapter-2/mini-boss-ss"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		
		groups.init(scene)
		root.info_print = screen_info_factory.createObject(screen_hud)
		function fin()
		{
			if(info_print)
			{
				info_print.destroy()
			}
			if(image_accident)
			{
				image_accident.destroy()
			}
		}
		root.finalize = fin
	}
}
