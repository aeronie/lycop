import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-5/course-zone.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}

	signal course_perdu
	signal new_spawn
	signal ship_reparer
	signal arbitre_finish
	signal arbitre_parle
	signal arbitre_flee
	signal ship_saboter
	signal ship_ok
	property alias ship: ship
	property var camera_position: Qt.point(40 * chapter_scale, 41 * chapter_scale + 40)
	property var last_ship_position: Qt.point(0, 0)
	property var info_print
	property real ti: 0
	property real t0: 0
	property int checkpoint_done: 0
	property real chapter_scale: 10
	property bool level_begin: false
	property bool start_course: false
	property int nb_participant: 1
	property bool boost: false
	property int classement: 1
	property var tab_concurrent:[]
	property real evolution: 0
	property int nb_checkpoint_to_boom: 7

	Q.Component
	{
		id: laser_gun_factory
		LaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
		}
	}
	Q.Component
	{
		id: secondary_shield_weapon
		SecondaryShield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: mine_launcher_factory
		MineLauncher
		{
			level: 2
		}
	}
	Q.Component
	{
		id: coil_gun_factory
		CoilGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			level: 2
		}
	}

	function near_dead()
	{
		var res = 0
		if(ship)
		{
			for(var i = 0 ; i < tab_concurrent.length ; i++ )
			{
				if(root.tab_concurrent[i] && res != 1)
				{
					if(checkpoint_done <= tab_concurrent[i].checkpoint.tab_indice - nb_checkpoint_to_boom + 1 )
					{
						if(checkpoint_done == tab_concurrent[i].checkpoint.tab_indice - nb_checkpoint_to_boom + 1 )
						{
							var ind1 = tab_concurrent[i].checkpoint.tab_indice
							var ind2 = tab_concurrent[i].checkpoint.tab_indice - 1
							if( tab_concurrent[i].checkpoint.tab_indice - 1 < 0)
							{
								ind2 = root.tab_gate.length - 1
							}
							var dist_check_to_next = (root.tab_gate[ind1][0] * root.chapter_scale - root.tab_gate[ind2][0] * root.chapter_scale) * (root.tab_gate[ind1][0]* root.chapter_scale - root.tab_gate[ind2][0] * root.chapter_scale) + (root.tab_gate[ind1][1]* root.chapter_scale - root.tab_gate[ind2][1] * root.chapter_scale) * (root.tab_gate[ind1][1]* root.chapter_scale -root.tab_gate[ind2][1] * root.chapter_scale )
							dist_check_to_next = Math.sqrt(dist_check_to_next)
							if(res < 1 - tab_concurrent[i].distance / dist_check_to_next)
							{
								res = 1 - tab_concurrent[i].distance / dist_check_to_next
							}
							if(res > 1)
							{
								res = 1
							}
						}
						else
						{
							res = 1
						}
					}
				}
			}
		}
		return res
	}

	onTiChanged:
	{
		classement = classement_calc()
		evolution = near_dead()
	}

	function classement_calc()
	{
		var res = 1
		if(ship)
		{
			for(var i = 0 ; i < tab_concurrent.length ; i++ )
			{
				if(root.tab_concurrent[i])
				{
					if(checkpoint_done < tab_concurrent[i].checkpoint.tab_indice)
					{
						res++
					}
					else
					{
						if(checkpoint_done == tab_concurrent[i].checkpoint.tab_indice)
						{
							if(ship.distance > tab_concurrent[i].distance)
							{
								res++
							}
						}
					}
				}
			}
		}
		else
		{
			res = nb_participant
		}
		return res
	}

	//x,y,angle
	property var tab_gate: [
	[39,41, -1/2],
	[31,38, -1/4],
	[30,33,-1/4],
	[27,32, -1/2],
	[25,30,0],
	[27,29,1/2],
	[30,29,1/4],
	[30,27,-1/4],
	[27,25,0],
	[28,22,0],
	[30,19,1/2],
	[33,20,1/2],
	[36,22,1/2],
	[39,22,1/2],
	[43,22,1/2],
	[47,22,1/2],
	[52,21,1/4],
	[54,19,1/4],
	[58,18,1/2],
	[62,19,1],
	[61,22,-3/4],
	[59,24,-3/4],
	[55,26,-3/4],
	[55,27,3/4],
	[61,28,1],
	[61,30,-3/4],
	[57,34,-3/4],
	[54,38,-3/4],
	[47,41,-1/2]
	]

	function random_sign() {return Math.random() < 0.5 ? -1 : 1}

	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false

		Background
		{
			z: altitudes.background_away
			scale: 10
			material: "vide"
		}

		Ship
		{
			id: ship
			position.x: 40 * chapter_scale //43 * chapter_scale
			position.y: 41 * chapter_scale + 40 + 10 * (1 - t0)//41 * chapter_scale
			property bool on: false
			property real distance: 0
			property int check_done: root.checkpoint_done
			property int indice: -1
			function dist_to_checkpoint(ind,x,y)
			{
				var res = 0
				if(ind < root.tab_gate.length)
				{
					res = (root.tab_gate[ind][0] * root.chapter_scale - x) * (root.tab_gate[ind][0]* root.chapter_scale - x) + (root.tab_gate[ind][1]* root.chapter_scale - y) * (root.tab_gate[ind][1]* root.chapter_scale -y )
					res = Math.sqrt(res)
				}
				return res
			}

			function dist_check()
			{
				return dist_to_checkpoint(check_done,ship.position.x,ship.position.y)
			}

			Q.Component.onCompleted:
			{
				distance = Qt.binding(dist_check)
			}

			Q.Component.onDestruction:
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}

		Vehicle
		{
			id: boom
			position: ship ? ship.position : root.last_ship_position
			property real fuzzy_value: root.evolution
			onFuzzy_valueChanged:
			{
				if(boom.fuzzy_value > 0 && boom.first)
				{
					boom.first = false
				}
			}
			property bool active: boom.fuzzy_value == 1
			readonly property real damages: 10000
			property int ind_img: 0
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			onActiveChanged:
			{
				if(active)
				{
					sound_shooting.play()
				}
			}
			Sound
			{
				id: sound_shooting
				objectName: "chapter-3/aoe_of_death"
				scale: 10
			}
			Image
			{
				material: "chapter-4/boom/viseur"
				z: altitudes.boss - 0.000001
				scale: 4* (1.50 - boom.fuzzy_value)
				mask: Qt.rgba(1, boom.fuzzy_value, boom.fuzzy_value, ship ? boom.fuzzy_value : 0)
			}

			Image
			{
				material: "chapter-4/boom/frappe/" + boom.ind_img
				z: altitudes.boss - 0.000001
				mask: Qt.rgba(1, 1, 1, boom.active ? 1 : 0)
			}
			Image
			{
				material: "chapter-4/boom/anneaux/" + boom.ind_img
				z: altitudes.boss - 0.000001
				mask: Qt.rgba(1, 1, 1, boom.active ? 1 : 0)
			}

			CircleCollider
			{
				group: boom.active ? groups.enemy_dot : 0
				sensor: true
			}
		}

		Body
		{
			id: pas_de_tir
			position: ship ? ship.position : Qt.point(0,0)
			scale: 30
			type: Body.KINEMATIC
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			CircleCollider
			{
				group: root.level_begin ? 0 : groups.clear_screen
				sensor: true
			}
		}
		Animation
		{
			id: timer
			time: 7
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}

		Repeater
		{
			count: tab_gate.length
			Q.Component
			{
				Vehicle
				{
					id: gate
					type: Body.STATIC
					function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)}
					property bool alive: true
					onAliveChanged: if(!alive) deleteLater()
					property bool on: false
					property int indice: index
					property color mask: index != root.checkpoint_done ? "white" : "gold"
					onOnChanged:
					{
						if(on)
						{
							if(index == root.checkpoint_done)
							{
								root.checkpoint_done++
							}
							else
							{
								on = false
							}
						}
					}
					icon: index != root.checkpoint_done ? 0 : 2
					icon_color: index != root.checkpoint_done ? "white" : "gold"
					position: Qt.point(tab_gate[index][0] * chapter_scale,tab_gate[index][1] * chapter_scale)
					scale: 4
					angle: tab_gate[index][2] * Math.PI

					Image
					{
						material: "chapter-5/checkpoint-bot"
						z: altitudes.boss - 0.00000001
						mask: gate.mask
					}
					Image
					{
						material: "chapter-5/checkpoint-top"
						z: altitudes.shield + 0.00000001
						mask: gate.mask
					}
					PolygonCollider
					{
						vertexes: [gate.coords(383,241),gate.coords(506,241),gate.coords(506,273),gate.coords(383,273)]
						group: groups.wall
					}
					PolygonCollider
					{
						vertexes: [gate.coords(6,241),gate.coords(127,241),gate.coords(127,273),gate.coords(6,273)]
						group: groups.wall
					}
					PolygonCollider
					{
						vertexes: [gate.coords(129,241),gate.coords(382,241),gate.coords(382,273),gate.coords(129,273)]
						group: !gate.on ? groups.checkpoint : 0
						sensor: true
					}
				}
			}
		}
	}

	Q.Component
	{
		id: course_factory
		Animation
		{
			id: timer
			time: 7
			speed: -1
			property var tab: root.tab_concurrent
			property bool first: true
			onTimeChanged:
			{
				if(root.start_course)
				{
					if(time > 2)
					{
						if(time < 6 && first)
						{
							root.ship_saboter()
							first = false
						}
						root.boost = true
						root.level_begin = false
					}
					else
					{
						if(ship)
						{
							if(!root.level_begin)
							{
								root.ship_reparer()
								root.level_begin = true
							}
							root.boost = false
							for(var i = 0 ; i < root.tab_concurrent.length ; i++)
							{
								if(root.tab_concurrent[i])
								{
									var dist = (root.tab_concurrent[i].position.x - ship.position.x) * (root.tab_concurrent[i].position.x - ship.position.x) + (root.tab_concurrent[i].position.y - ship.position.y) * (root.tab_concurrent[i].position.y - ship.position.y)
									if(Math.sqrt(dist) < 30)
									{
										root.tab_concurrent[i].shoot = true
									}
									else
									{
										root.tab_concurrent[i].shoot = false
									}
								}
							}
						}
					}
				}
				else
				{
					timer.deleteLater()
				}
			}
		}
	}
	Q.Component
	{
		id: checkpoint_factory
		Vehicle
		{
			id: check
			property int indice: 0
			property int tab_indice: 0
			property bool on: false
			property var tab: root.tab_gate
			position.x: tab[tab_indice][0] * root.chapter_scale
			position.y: tab[tab_indice][1] * root.chapter_scale
			angle: tab[tab_indice][2] * Math.PI
			property int etat: 0
			property real ti_local: root.ti

			onTi_localChanged:
			{
				check.position.x = tab[tab_indice][0] * root.chapter_scale
				check.position.y = tab[tab_indice][1] * root.chapter_scale
				check.angle = tab[tab_indice][2] * Math.PI
			}

			onOnChanged:
			{
				if(on)
				{
					if(etat == 0)
					{
						etat = 1
					}
					else
					{
						tab_indice = (tab_indice + 1)%tab.length
						if(tab_indice == 0)
						{
							root.course_perdu()
						}
						etat = 0
					}
					on = false
				}
			}

			CircleCollider
			{
				position: Qt.point(0, -2)
				group: etat == 1 ? groups.enemy_checkpoint - check.indice : 0
				radius: 0.5
				sensor: true
			}
			CircleCollider
			{
				position: Qt.point(0, 2)
				group: etat == 0 ? groups.enemy_checkpoint - check.indice : 0
				radius: 0.5
				sensor: true
			}
		}
	}

	function ind_in_tab(indice)
	{
		var res = 0
		for(var i = 0 ; i < root.tab_concurrent.length ; i++)
		{
			if(tab_concurrent[i].indice == indice)
			{
				res = i
			}
		}
		return res
	}

	Q.Component
	{
		id: arbitre_factory
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()
			property int indice: 50
			property bool on: true
			max_angular_speed: 40
			icon: 2
			icon_color: "red"
			max_speed: 10
			Image
			{
				material: "folks/insec/5"
			}

			CircleCollider
			{
				group: groups.enemy_checkpoint - drone.indice
			}
		}
	}

	Q.Component
	{
		id: myrmel_factory
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()
			max_angular_speed: 40
			icon: 2
			icon_color: "red"
			max_speed: 10
			Image
			{
				material: "folks/mafia/1"
			}
		}
	}

	Q.Component
	{
		id: myrmel_fighter_factory
		Vehicle
		{
			id: drone
			property bool alive: true
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)} // plink fait des images de drone en 512*512
			onAliveChanged: if(!alive) destroy()
			property bool on: true
			max_angular_speed: 2
			icon: 2
			scale: 1.1
			icon_color: "red"
			property int indice: 50
			max_speed: 0
			EquipmentSlot
			{
				position: drone.coords(123,405)
				scale: 0.5
				equipment: coil_gun_factory.createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position: drone.coords(389,405)
				scale: 0.5
				equipment: coil_gun_factory.createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position: drone.coords(256,270)
				scale: 0.5
				equipment: laser_gun_factory.createObject(null, {slot: this})
			}
			Image
			{
				material: "folks/mafia/3"
				z: altitudes.boss
			}

			Behaviour_attack_missile
			{
				target_groups: [groups.enemy_checkpoint - drone.indice]
				range: 10000000
			}
		}
	}

	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			id: drone
			max_speed: root.start_course ? (root.boost ? 10 + 5 * random_index(6) : 7 + random_index(3)) : 0
			max_angular_speed: root.boost ? 60 : 6
			icon: 2
			scale: 1.1
			icon_color: "blue"
			property int drone_type: random_index(5)
			function coords(x, y) {return Qt.point(x / 256 - 1, y / 256 - 1)}
			property int indice: 0
			property bool on: false
			property real distance: 0
			property var checkpoint
			property bool shoot: false
			property var tab_off: [secondary_shield_weapon,laser_gun_factory,mine_launcher_factory,coil_gun_factory,quipoutre_factory]
			property var tab_im: ["folks/course/0","folks/course/1","folks/heter/6","folks/insec/5","folks/mafia/3"]
			property var tab_slot: [
			[drone.coords(148,256),drone.coords(364,256),drone.coords(256,271),drone.coords(256,154),drone.coords(256, 71)],
			[drone.coords(124,306),drone.coords(388,306),drone.coords(256,267),drone.coords(216,155),drone.coords(296, 155)],
			[Qt.point(-0.70 , -0.37),Qt.point(-0.35 , 0),Qt.point(0 , 0.5),Qt.point(0.35 , 0),Qt.point(0.70 , -0.37)],
			[drone.coords(406,269),drone.coords(106,269),drone.coords(200,310),drone.coords(312,310),drone.coords(256, 411)],
			[drone.coords(123,405),drone.coords(389,405),drone.coords(256,270)]
			]
			property var tab_weapon: []
			property var tab_ind_weapon: []

			onShootChanged:
			{
				for(var i = 0 ; i < tab_weapon.length ; i++)
				{
					if(tab_weapon[i])
					{
						if(tab_ind_weapon[i] != 0)
						{
							tab_weapon[i].shooting = shoot
						}
					}
				}
			}
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						scale: 2
						sound_scale: 10
					}
				}
			}

			function dist_to_checkpoint(ind,x,y)
			{
				var res = 0
				if(ind < root.tab_gate.length)
				{
					res = (root.tab_gate[ind][0] * root.chapter_scale - x) * (root.tab_gate[ind][0]* root.chapter_scale - x) + (root.tab_gate[ind][1]* root.chapter_scale - y) * (root.tab_gate[ind][1]* root.chapter_scale -y )
					res = Math.sqrt(res)
				}
				return res
			}

			function dist_check()
			{
				return dist_to_checkpoint(checkpoint.tab_indice,drone.position.x,drone.position.y)
			}

			Q.Component.onCompleted:
			{
				root.nb_participant++
				root.tab_concurrent.push(drone)
				distance = Qt.binding(dist_check)
			}
			Q.Component.onDestruction:
			{
				root.nb_participant--
				var i = ind_in_tab(drone.indice)
				if(i == tab_concurrent.length - 1)
				{
					tab_concurrent.pop()
				}
				else
				{
					tab_concurrent[i] = tab_concurrent.pop()
				}
			}
			Image
			{
				material: tab_im[drone_type]
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
			}
			Repeater
			{
				count: tab_slot[drone_type].length
				Q.Component
				{
					EquipmentSlot
					{
						position: tab_slot[drone_type][index]
						scale: 0.3
						angle: Math.PI + (Math.random() - 0.5) * Math.PI * 2 / 3
						Q.Component.onCompleted:
						{
							var i = random_index(tab_off.length)
							equipment = tab_off[i].createObject(null, {slot: this})
							drone.tab_weapon.push(equipment)
							drone.tab_ind_weapon.push(i)
						}
					}
				}
			}
			Behaviour_attack_missile
			{
				target_groups: [groups.enemy_checkpoint - drone.indice]
				range: 10000000
			}
			CircleCollider
			{
				group: !root.level_begin ? groups.enemy_hull : groups.enemy_hull_naked
			}
		}
	}
	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("Classement : %1 / %2").arg(classement).arg(nb_participant)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}

	Q.Component
	{
		id: arbitre_come_after_course
		Animation
		{
			id: timer
			property var arbitre
			property bool first: true
			property real rotation_angle: 0
			property real angle_ini: Math.PI
			time: 7
			speed: -1
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var drone1
			property var drone2
			property var chef
			property bool flee: false

			Q.Component.onDestruction:
			{
				if(arbitre)
				{
					arbitre.deleteLater()
				}
			}

			onTimeChanged:
			{
				if(first)
				{
					arbitre = arbitre_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y - 20), angle: angle_ini})
					drone1 = myrmel_fighter_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x - 3, ship.position.y - 20), angle: angle_ini})
					drone2 = myrmel_fighter_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + 3, ship.position.y - 20), angle: angle_ini})
					chef = myrmel_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y + 20), angle: 0})
					first = false
				}
				else
				{
					if(rotation_angle == 0 && (ship.position.y - arbitre.position.y)*(ship.position.y - arbitre.position.y) > 9)
					{
						arbitre.position.y += 20 / scene.seconds_to_frames(2)
					}
					else
					{
						if(rotation_angle < Math.PI)
						{
							if(rotation_angle == 0)
							{
								root.arbitre_parle()
							}
							rotation_angle += 0.04
							if(rotation_angle > Math.PI)
							{
								rotation_angle = Math.PI
							}
							arbitre.angle = angle_ini + rotation_angle
						}
						else
						{
							if(flee)
							{
								if((ship.position.y - arbitre.position.y)*(ship.position.y - arbitre.position.y) < 2000)
								{
									arbitre.position.y -= 0.3
									drone1.max_speed = 15
									drone2.max_speed = 15
								}
								else
								{
									root.arbitre_finish()
									alive = false
								}
							}
							else
							{
								if((drone1.position.y - arbitre.position.y) * (drone1.position.y - arbitre.position.y) + (drone1.position.x - arbitre.position.x) * (drone1.position.x - arbitre.position.x) > 16)
								{
									drone1.position.y += 20 / scene.seconds_to_frames(2)
									drone2.position.y += 20 / scene.seconds_to_frames(2)
									chef.position.y -= 20 / scene.seconds_to_frames(2)
								}
								else
								{
									root.arbitre_flee()
									flee = true
								}
							}
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: arbitre_come
		Animation
		{
			id: timer
			property var arbitre
			property bool first: true
			property real rotation_angle: 0
			property real angle_ini: Math.PI
			time: 3
			speed: -1
			property bool alive: true
			onAliveChanged: if(!alive) destroy()

			Q.Component.onDestruction:
			{
				if(arbitre)
				{
					arbitre.deleteLater()
				}
			}
			onTimeChanged:
			{
				if(first)
				{
					arbitre = arbitre_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y - 20), angle: angle_ini})
					first = false
				}
				else
				{
					if(rotation_angle == 0 && (ship.position.y - arbitre.position.y)*(ship.position.y - arbitre.position.y) > 9)
					{
						arbitre.position.y += 20 / scene.seconds_to_frames(1.5)
					}
					else
					{
						if(rotation_angle < Math.PI)
						{
							if(rotation_angle == 0)
							{
								root.arbitre_parle()
							}
							rotation_angle += 0.08
							if(rotation_angle > Math.PI)
							{
								rotation_angle = Math.PI
							}
							arbitre.angle = angle_ini + rotation_angle
						}
						else
						{
							if((ship.position.y - arbitre.position.y)*(ship.position.y - arbitre.position.y) < 400)
							{
								arbitre.position.y -= 20 / scene.seconds_to_frames(1.5)
							}
							else
							{
								root.arbitre_finish()
								alive = false
							}
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: move_ship_to
		Animation
		{
			id: timer

			//ship_angle entre -pi et pi
			function calcul_ship_angle()
			{
				var result = ship.angle
				while(result < -Math.PI)
				{
					result += 2 * Math.PI
				}
				while(result > Math.PI)
				{
					result -= 2 * Math.PI
				}
				return result
			}

			property real epsilon: 0.5
			property real x
			property real long_x
			property real y
			property real long_y
			property real angle
			property real long_angle
			property real time_to_move: 2
			property bool first: true
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(time < 6.9)
				{
					if(first)
					{
						ship.angle = calcul_ship_angle()
						long_x = ship.position.x - timer.x
						long_y = ship.position.y - timer.y
						long_angle = ship.angle - timer.angle
						first = false
					}
					else
					{
						if(ship)
						{
							if(root.scene.seconds_to_frames(6.9) - root.scene.seconds_to_frames(time) < root.scene.seconds_to_frames(time_to_move) + 2)
							{
								ship.position.x -= long_x / root.scene.seconds_to_frames(time_to_move)
								ship.position.y -= long_y / root.scene.seconds_to_frames(time_to_move)
								ship.angle -= long_angle / root.scene.seconds_to_frames(time_to_move)
							}
							else
							{
								root.ship_ok()
								alive = false
							}
						}
					}
				}
			}
		}
	}
	StateMachine
	{
		begin: state_story_0 // state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_story_0 // state_dialogue_4 //state_test_1 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					//camera_position= undefined
					//animation intro

					//animation camera tour de course
					//animation faux depart == course global

					//course
					//fin de course
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 2000}
				onTriggered:
				{
					music.objectName = "chapter-5/course"
					for(var i = 0 ; i < 30; i++ )
					{
						var check = checkpoint_factory.createObject(null, {scene: scene, indice: i})
						drone_factory.createObject(null, {scene: scene, position: Qt.point(43 * chapter_scale - 3 - 3 * Math.floor(i / 2),41 * chapter_scale - 5 * (i % 2)), checkpoint: check , indice: i, angle: - Math.PI / 2})
					}
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "cutscene"; value: true}
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_story_2
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					ship.position.x = 40 * chapter_scale
					ship.position.y = 41 * chapter_scale + 40
					camera_position = undefined
					arbitre_come.createObject(null, {parent: scene})
					root.level_begin = false
				}
			}
		}

		State
		{
			id: state_story_2
			AssignProperty {target: root; property: "cutscene"; value: true}
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_0
				signal: arbitre_parle
				onTriggered:
				{
					messages.add("insec/normal", qsTr("arbitre 1"))
					messages.add("lycop/normal", qsTr("arbitre 2"))
					messages.add("insec/normal", qsTr("arbitre 3"))
					messages.add("lycop/surprised", qsTr("arbitre 4"))
					messages.add("insec/normal", qsTr("arbitre 5"))
					messages.add("nectaire/normal", qsTr("arbitre 6"))
					messages.add("insec/normal", qsTr("arbitre 7"))
					messages.add("lycop/angry", qsTr("arbitre 8"))
					messages.add("insec/normal", qsTr("arbitre 9"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: arbitre_finish
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("arbitre parti 1"))
					messages.add("lycop/normal", qsTr("arbitre parti 2"))
					messages.add("nectaire/normal", qsTr("arbitre parti 3"))
					messages.add("lycop/normal", qsTr("arbitre parti 4"))
					messages.add("nectaire/normal", qsTr("arbitre parti 5"))
					messages.add("lycop/normal", qsTr("arbitre parti 6"))
					move_ship_to.createObject(null, {parent: scene, x: 43 * chapter_scale, y: 41 * chapter_scale, angle: - Math.PI / 2})
				}
			}
		}
		State {id: state_dialogue_0; SignalTransition {targetState: state_story_2; signal: state_dialogue_0.propertiesAssigned; guard: !messages.has_unread}}
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_3; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_3
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: ship_ok
				onTriggered:
				{
					messages.add("insec/normal", qsTr("presentateur parle 1"))
					messages.add("insec/normal", qsTr("presentateur parle 2"))
					messages.add("insec/normal", qsTr("presentateur parle 3"))
				}
			}
		}
		State
		{
			id: state_dialogue_2
			SignalTransition
			{
				targetState: state_story_before_4
				signal: state_dialogue_2.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					root.info_print = screen_info_factory.createObject(screen_hud)
					root.start_course = true
					course_factory.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_dialogue_2_bis
			SignalTransition
			{
				targetState: state_story_4
				signal: state_dialogue_2_bis.propertiesAssigned
				guard: !messages.has_unread
			}
		}

		State
		{
			id: state_story_before_4
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_3_bis
				signal: ship_saboter
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("sabotage 1"))
					messages.add("lycop/angry", qsTr("sabotage 2"))
					messages.add("nectaire/normal", qsTr("sabotage 3"))
				}
			}

			SignalTransition
			{
				targetState: state_dialogue_2_bis
				signal: ship_reparer
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("reparer 1"))
				}
			}
		}

		State
		{
			id: state_dialogue_3_bis
			SignalTransition
			{
				targetState: state_story_before_4
				signal: state_dialogue_3_bis.propertiesAssigned
				guard: !messages.has_unread
			}
		}

		State
		{
			id: state_story_4
			AssignProperty {target: scene; property: "running"; value: true}

			SignalTransition
			{
				targetState: state_dialogue_3
				signal: course_perdu
				onTriggered:
				{
					messages.add("insec/normal", qsTr("perdu 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_4
				signal: checkpoint_doneChanged
				guard: checkpoint_done == tab_gate.length
				onTriggered:
				{
					messages.add("insec/normal", qsTr("gagner 1"))
					messages.add("lycop/normal", qsTr("gagner 2"))
					messages.add("nectaire/normal", qsTr("gagner 3"))
					messages.add("lycop/happy", qsTr("gagner 4"))
				}
			}
		}
		State
		{
			id: state_dialogue_3
			SignalTransition
			{
				targetState: state_story_4
				signal: state_dialogue_3.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					game_over = true
				}
			}
		}
		State
		{
			id: state_dialogue_4
			SignalTransition
			{
				targetState: state_story_5
				signal: state_dialogue_4.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					move_ship_to.createObject(null, {parent: scene, x: tab_gate[tab_gate.length - 1][0] * chapter_scale - 15, y: tab_gate[tab_gate.length - 1][1] * chapter_scale , angle: - Math.PI / 2, time_to_move: 1})
					root.start_course = false
					if(info_print)
					{
						info_print.destroy()
					}
				}
			}
		}
		State
		{
			id: state_story_5
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_story_6
				signal: ship_ok
				onTriggered:
				{
					arbitre_come_after_course.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_story_6
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_5
				signal: arbitre_parle
				onTriggered:
				{
					messages.add("insec/normal", qsTr("arbitre_before_flee 1"))
					messages.add("nectaire/normal", qsTr("arbitre_before_flee 2"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_5
				signal: arbitre_flee
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("mafia_before_flee 1"))
					messages.add("insec/normal", qsTr("mafia_before_flee 2"))
					messages.add("mafia/normal", qsTr("mafia_before_flee 3"))
					messages.add("insec/normal", qsTr("mafia_before_flee 4"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_6
				signal: arbitre_finish
				onTriggered:
				{
					messages.add("mafia/normal", qsTr("fin 1"))
					messages.add("lycop/normal", qsTr("fin 2"))
					messages.add("mafia/normal", qsTr("fin 3"))
					messages.add("lycop/normal", qsTr("fin 4"))
					messages.add("mafia/normal", qsTr("fin 5"))
					messages.add("lycop/normal", qsTr("fin 6"))
				}
			}
		}
		State {id: state_dialogue_5; SignalTransition {targetState: state_story_6; signal: state_dialogue_5.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_dialogue_6
			SignalTransition
			{
				targetState: state_end
				signal: state_dialogue_6.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					saved_game.add_science(1)
					platform.set_bool("course-fini")
					saved_game.set("file", "game/chapter-5/chapter-5.qml")
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}

	Q.Component.onCompleted:
	{
		jobs.run(music, "preload", ["chapter-5/course"])
		groups.init(scene)
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["chapter-4/boom/viseur"])
		for(var i = 0; i < 3; ++i) jobs.run(scene_view, "preload", ["chapter-4/boom/anneaux/" + i])
		for(var i = 0; i < 3; ++i) jobs.run(scene_view, "preload", ["chapter-4/boom/frappe/" + i])
		jobs.run(scene_view, "preload", ["folks/insec/5"])
		jobs.run(scene_view, "preload", ["folks/mafia/3"])
		jobs.run(scene_view, "preload", ["folks/course/0"])
		jobs.run(scene_view, "preload", ["folks/course/1"])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		jobs.run(scene_view, "preload", ["folks/heter/6"])
		jobs.run(scene_view, "preload", ["chapter-5/checkpoint-bot"])
		jobs.run(scene_view, "preload", ["chapter-5/checkpoint-top"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		root.finalize = function() {if(info_print) info_print.destroy()}
	}
}
