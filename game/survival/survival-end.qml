import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "qrc:///controls"
import "qrc:///controls/styles" as S

Rectangle
{
	id: root
	property int wave: 0
	property int score: 0
	color: "black"
	ColumnLayout
	{
		anchors.fill: parent
		anchors.margins: spacing
		spacing: 20
		Item
		{
			Layout.fillHeight: true
			Layout.preferredHeight: 1
		}
		Label
		{
			Layout.fillWidth: true
			text: wave < 0 ? qsTr("success") : qsTr("failure")
			horizontalAlignment: Text.AlignHCenter
			font.pointSize: S.Private.font.pointSize * 2
		}
		Label
		{
			Layout.fillWidth: true
			text: wave < 0 ? qsTr("success %1").arg(score) : qsTr("failure %1 %2").arg(wave).arg(score)
			wrapMode: Text.Wrap
		}
		TextField
		{
			id: name
			Layout.fillWidth: true
		}
		Buttons
		{
			model: [action_apply, action_cancel]
		}
		Item
		{
			Layout.fillHeight: true
			Layout.preferredHeight: 3
		}
	}
	Action
	{
		id: action_cancel
		text: qsTr("Cancel")
		onTriggered:
		{
			stack.pop(null)
			screen_menu.show(screen_menu.screen_scores)
			stack.push(screen_menu)
			scene_loader.load(undefined, {file: "game/title/title.qml", immediate: true})
		}
	}
	Action
	{
		id: action_apply
		text: qsTr("Apply")
		onTriggered:
		{
			var key = "scores/" + new Date().toISOString() + "/"
			settings.set(key + "name", settings.encode(name.text))
			settings.set(key + "score", score)
			settings.set(key + "wave", wave)
			action_cancel.trigger()
		}
	}
}
