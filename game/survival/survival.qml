import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 0
			source: "///data/game/survival/survival.png"
			fillMode: Q.Image.PreserveAspectFit
			verticalAlignment: Q.Image.AlignBottom
			Q.TextArea
			{
				anchors.top: parent.top
				anchors.left: parent.left
				anchors.right: parent.right
				anchors.margins: 5
				text: qsTr("story")
				height: flickableItem.contentHeight + viewport.anchors.topMargin + viewport.anchors.bottomMargin
				verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff
			}
		}
	}
	property alias ship: ship
	property bool fight: false
	property int wave: 1
	property int enemy_count: 0
	property bool end: false
	property real score: 0
	property real new_score: 0
	property var tab_enemy:[
	[10,0,0,0,0],
	[5,5,0,0,0],
	[0,5,2,0,0],
	[0,0,2,1,0],
	[10,10,2,0,0],
	[0,10,5,0,0],
	[0,10,10,0,0],
	[0,0,12,0,1],
	[10,10,10,0,0],
	[15,10,10,0,0],
	[25,5,10,0,0],
	[5,5,7,1,1],
	[20,0,10,1,0],
	[20,0,10,0,1],
	[20,0,10,1,1],
	[20,0,10,2,1],
	[20,0,10,1,1],
	[25,0,10,1,1],
	[25,0,10,1,2],
	[25,0,10,2,2]
	]

	Q.Component
	{
		id: laser_gun_factory
		LaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: aggressive_shield
		AggressiveShield
		{
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: shield_weapon
		Shield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: secondary_shield_weapon
		SecondaryShield
		{
			level: 2
		}
	}
	Q.Component
	{
		id: heavy_laser_gun_factory
		HeavyLaserGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: acid_gun_factory
		AcidGun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: gun_factory
		Gun
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: missile_launcher_factory
		MissileLauncher
		{
			range_bullet: scene.seconds_to_frames(3)
			level: 2
			shooting: true
		}
	}
	Q.Component
	{
		id: random_spray_factory
		RandomSpray
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			enemy: 1
			level: 2
		}
	}
	Q.Component
	{
		id: coil_gun_factory
		CoilGun
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: triple_gun_factory
		TripleGun
		{
			range_bullet: scene.seconds_to_frames(3)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: vortex_gun_factory
		VortexGun
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	Q.Component
	{
		id: quipoutre_factory
		Quipoutre
		{
			range_bullet: scene.seconds_to_frames(8)
			shooting: true
			level: 2
		}
	}
	scene: Scene
	{
		running: game_running
		Background
		{
			z: -0.5
			scale: 10
			material: "vide"
		}
		Sensor
		{
			id: lapin_timer_1
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(10)
			target_value: state_lapin_1.active
		}
		Sensor
		{
			id: lapin_timer_2
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(10)
			target_value: state_lapin_2.active
		}
		Sensor
		{
			id: timer_respawn
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(10)
			target_value: !fight
		}
		Sensor
		{
			id: bonus_score
			type: Body.STATIC
			duration_in: scene.seconds_to_frames(120)
			target_value: fight
		}
		Ship
		{
			id: ship
			Q.Component.onDestruction:
			{
				platform.set_int("survival-level", root.wave)
				if(game_running)
				{
					stack.push({item: Qt.resolvedUrl("survival-end.qml"), properties: {wave: wave, score: score}})
				}
			}
		}
	}
	Q.Component
	{
		id: drone_factory
		Vehicle
		{
			id: drone
			property var tab_weapons: root.wave < 7 ? [aggressive_shield,laser_gun_factory,coil_gun_factory] : [aggressive_shield,vortex_gun_factory,quipoutre_factory]
			property int indice_weapon: random_index(tab_weapons.length)
			max_speed: indice_weapon ? 5 : 10
			max_angular_speed: 6
			property bool behaviour: root.wave < 8 || !indice_weapon ? true : false
			icon: 2
			icon_color: "red"
			Q.Component.onCompleted: ++root.enemy_count
			Q.Component.onDestruction: --root.enemy_count , root.score += 1
			loot_factory: Repeater
			{
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				material: "folks/heter/1"
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
			}
			CircleCollider
			{
				group: (!indice_weapon && drone.aggressive_shield_active_damages) ? groups.enemy_hull_naked_aggressive_shield : groups.enemy_hull_naked
			}
			CircleCollider
			{
				group: (!indice_weapon && drone.aggressive_shield_active_damages) ? groups.enemy_shield_aggressive : 0
				radius: 1.1
			}
			EquipmentSlot
			{
				position.y: 0.3
				scale: 0.5
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				enabled: behaviour
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: indice_weapon ? 6 : 0
			}
			Behaviour_attack_harass
			{
				enabled: !behaviour
				target_groups: groups.enemy_targets
				relaxation_time: 1
				target_distance: (Math.random()- 0.5) * 40
			}
		}
	}

	Q.Component
	{
		id: dualdrone_factory
		Vehicle
		{
			id: dualdrone
			max_speed: behaviour ? 5 : 7
			max_angular_speed: behaviour ? 4 : 6
			icon: 2
			icon_color: "red"
			property int behaviour: random_index(2)
			property var tab_weapons: [laser_gun_factory,coil_gun_factory,triple_gun_factory,heavy_laser_gun_factory,missile_launcher_factory,shield_weapon]
			property int indice_weapon_1: random_index(tab_weapons.length)
			property int indice_weapon_2: indice_weapon_1==(tab_weapons.length - 1) ? random_index(tab_weapons.length - 1) : random_index(tab_weapons.length) //pas deux boucliers
			Q.Component.onCompleted: ++root.enemy_count, shield = max_shield
			Q.Component.onDestruction: --root.enemy_count , root.score += 2
			loot_factory: Repeater
			{
				count: 2
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				material: "folks/heter/2"
			}

			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
				z: altitudes.shield
			}

			CircleCollider
			{
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
			}

			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				radius: 1.1
			}

			EquipmentSlot
			{
				position.x: 0.3
				position.y: 0.5
				scale: 0.5
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon_1].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: -0.3
				position.y: 0.5
				scale: 0.5
				Q.Component.onCompleted: equipment = tab_weapons[indice_weapon_2].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				enabled: behaviour
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			Behaviour_attack_harass
			{
				enabled: !behaviour
				target_groups: groups.enemy_targets
				relaxation_time: 1
				target_distance: (Math.random()- 0.5) * 40
			}
		}
	}

	Q.Component
	{
		id: starship_factory
		Vehicle
		{
			id: starship
			max_speed: 7
			max_angular_speed: strategie ? 1 : 5
			icon: 3
			weapon_regeneration: strategie ? 2 : 1
			icon_color: "red"
			// 1 = etoile , 2 = tout devant , 3 = gauche droite et un au centre
			property int strategie: random_index(tab_angle.length)
			property int index: tab_weapons.length
			property int shield_count: 0
			property var tab_angle: [
			[0 , 0 , 0 , 0 , 0],
			[-1/4 , -1/2 , 1 , 1/2 , 1/4],
			[-1/2 , -1/2 , 0 , 1/2 , 1/2]]
			property var tab_weapons: root.wave < 7 ? [shield_weapon,coil_gun_factory,heavy_laser_gun_factory,random_spray_factory] :[secondary_shield_weapon,coil_gun_factory,vortex_gun_factory,quipoutre_factory]
			property int indice_weapon_1: random_index(index - 1) + 1
			property int indice_weapon_2: random_index(index - 1) + 1
			property int indice_weapon_3: random_index(index)
			property int indice_weapon_4: 1
			property int indice_weapon_5: 1

			Q.Component.onCompleted: ++root.enemy_count, shield = max_shield
			Q.Component.onDestruction: --root.enemy_count , root.score += 5
			scale: 1.4
			loot_factory: Repeater
			{
				count: 5
				Q.Component {Loot {}}
				Q.Component {Explosion {scale: 2}}
			}
			Image
			{
				material: "folks/heter/6"
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
				z: altitudes.shield
				position.y: -0.2
			}

			CircleCollider
			{
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
				radius: 0.75
			}

			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				position.y: -0.2
			}

			EquipmentSlot
			{
				position.x: -0.70
				position.y: -0.37
				scale: 0.5 / starship.scale
				angle: tab_angle[starship.strategie][0] * Math.PI
				Q.Component.onCompleted: equipment = starship.tab_weapons[indice_weapon_4].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: -0.35
				position.y: 0
				scale: 0.5 / starship.scale
				angle: starship.tab_angle[starship.strategie][1] * Math.PI
				Q.Component.onCompleted: equipment = starship.tab_weapons[indice_weapon_1].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: 0
				position.y: 0.5
				scale: 0.5 / starship.scale
				angle: tab_angle[starship.strategie][2] * Math.PI
				Q.Component.onCompleted: equipment = starship.tab_weapons[indice_weapon_3].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: 0.35
				position.y: 0
				scale: 0.5 / starship.scale
				angle: tab_angle[starship.strategie][3] * Math.PI
				Q.Component.onCompleted: equipment = starship.tab_weapons[indice_weapon_2].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: 0.70
				position.y: -0.37
				scale: 0.5 / starship.scale
				angle: tab_angle[starship.strategie][4] * Math.PI
				Q.Component.onCompleted: equipment = starship.tab_weapons[indice_weapon_5].createObject(null, {slot: this})
			}
			Behaviour_attack_circle
			{
				enabled: starship.strategie == 0
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 6
			}
			Behaviour_attack_rotate
			{
				enabled: starship.strategie > 0
				target_groups: groups.enemy_targets
				target_distance: 10
			}
		}
	}

	Q.Component
	{
		id:	vauban_factory
		Vehicle
		{
			id: vauban
			icon: 4
			icon_color: "red"
			max_speed: 10
			max_angular_speed: 0.4
			weapon_regeneration: 4
			property var tab_weapons: [shield_weapon,shield_canceler_factory,secondary_shield_weapon]
			scale: 8
			Q.Component.onCompleted: ++root.enemy_count, shield = max_shield
			Q.Component.onDestruction: --root.enemy_count , root.score += 20
			Q.Component
			{
				id: shield_canceler_factory
				ShieldCanceler
				{
					range_bullet: 40
					level: 0
					shooting: true
					period: scene.seconds_to_frames(8)
				}
			}
			loot_factory: Repeater
			{
				count: 31
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "boss-2"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				scale: 1.1
				z: altitudes.shield
			}

			CircleCollider
			{
				group: shield ? groups.enemy_hull : groups.enemy_hull_naked
			}

			CircleCollider
			{
				group: shield ? groups.enemy_shield : 0
				radius: 1.1
			}
			Behaviour_attack_rotate
			{
				target_groups: groups.enemy_targets
				target_distance: 10
			}
			/*Behaviour_attack_only
			{
				id: behaviour
				target_groups: groups.enemy_targets
				max_distance: 40
			}*/
			EquipmentSlot
			{
				scale: 0.1
				Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 0 : 1)].createObject(null, {slot: this})
			}
			Repeater
			{
				count: 5
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .2
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .2
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: DroneLauncher
						{
							shooting: true
							period: scene.seconds_to_frames(30)
							level: 3
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index - .25) * 72 * Math.PI / 180) * .1
						position.y: Math.sin((index - .25) * 72 * Math.PI / 180) * .1
						angle: index * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.1
						Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 0 : 2)].createObject(null, {slot: this})
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .8
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .8
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: LaserGun
						{
							shooting: true
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .25) * 72 * Math.PI / 180) * .47
						position.y: Math.sin((index + .25) * 72 * Math.PI / 180) * .47
						angle: (index + .25) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.08
						equipment: MissileLauncher
						{
							shooting: true
							period: scene.seconds_to_frames(2)
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index - .025) * 72 * Math.PI / 180) * .7
						position.y: Math.sin((index - .025) * 72 * Math.PI / 180) * .7
						angle: (index - .01) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Gun
						{
							shooting: true
							level: 2
						}
					}
				}
				Q.Component
				{
					EquipmentSlot
					{
						position.x: Math.cos((index + .525) * 72 * Math.PI / 180) * .7
						position.y: Math.sin((index + .525) * 72 * Math.PI / 180) * .7
						angle: (index + .51) * 72 * Math.PI / 180 + Math.PI / 2
						scale: 0.07
						equipment: Gun
						{
							shooting: true
							level: 2
						}
					}
				}
			}
		}
	}
	Q.Component
	{
		id: lapin_factory
		Vehicle
		{
			id: component
			property int destroyed_weapon_count: 0
			icon: 4
			icon_color: "red"
			max_speed: 10
			max_angular_speed: 2
			scale: 6
			property var tab_weapons: [shield_weapon,secondary_shield_weapon,laser_gun_factory,laser_gun_factory_lapin,triple_gun_factory,random_spray_factory,acid_gun_factory]
			Q.Component.onCompleted: ++root.enemy_count, shield = max_shield
			Q.Component.onDestruction: --root.enemy_count , root.score += 20
			Q.Component
			{
				id: laser_gun_factory_lapin
				LaserGun
				{
					range_bullet: scene.seconds_to_frames(3)
					level: 2
					Q.Component.onDestruction: ++component.destroyed_weapon_count
					shooting: true
				}
			}
			loot_factory: Repeater
			{
				count: 17
				Q.Component {Loot {}}
				Q.Component {Explosion {}}
			}
			Image
			{
				material: "chapter-1/boss"
				z: altitudes.boss
			}
			Image
			{
				material: "equipments/shield"
				mask: mask_shield(parent)
				z: altitudes.shield
				scale: 1.1
			}
			Behaviour_attack_circle
			{
				enabled: state_lapin_1.active
				target_groups: groups.enemy_targets
				target_angle: component.destroyed_weapon_count < 5 ? Math.PI / 2 : -Math.PI / 2
				target_distance: 10
			}
			Behaviour_attack_rotate
			{
				enabled: state_lapin_2.active
				target_groups: groups.enemy_targets
				target_distance: 10
			}
			CircleCollider
			{
				group: component.shield ? groups.enemy_hull : groups.enemy_hull_naked
				position.y: 0.5
				radius: 0.5
			}
			CircleCollider
			{
				group: component.shield ? groups.enemy_hull : groups.enemy_hull_naked
				position.y: -0.5
				radius: 0.5
			}
			CircleCollider
			{
				group: component.shield ? groups.enemy_shield : 0
				radius: 1.1
			}
			EquipmentSlot
			{
				scale: 0.15
				Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 0 : 1)].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.y: 0.9
				scale: 0.15
				angle: Math.PI
				Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 4 : 5)].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.y: -0.85
				scale: 0.15
				Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 4 : 5)].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: 0.25
				position.y: -0.7
				scale: 0.15
				angle: Math.PI / 4
				Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 2 : 5)].createObject(null, {slot: this})
			}
			Repeater
			{
				count: 4
				Q.Component
				{
					EquipmentSlot
					{
						position.x: 0.4
						position.y: index * 0.2 - 0.43
						scale: 0.15
						angle: Math.PI / 2
						Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 3 : 6)].createObject(null, {slot: this})
					}
				}
			}
			EquipmentSlot
			{
				position.x: 0.35
				position.y: 0.57
				scale: 0.15
				angle: Math.PI / 2
				Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 3 : 6)].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: 0.25
				position.y: 0.8
				scale: 0.15
				angle: Math.PI * 3 / 4
				Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 2 : 5)].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: -0.25
				position.y: -0.7
				scale: 0.15
				angle: -Math.PI / 4
				Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 2 : 5)].createObject(null, {slot: this})
			}
			Repeater
			{
				count: 4
				Q.Component
				{
					EquipmentSlot
					{
						position.x: -0.4
						position.y: index * 0.2 - 0.43
						scale: 0.15
						angle: -Math.PI / 2
						Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 2 : 6)].createObject(null, {slot: this})
					}
				}
			}
			EquipmentSlot
			{
				position.x: -0.35
				position.y: 0.57
				scale: 0.15
				angle: -Math.PI / 2
				Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 2 : 6)].createObject(null, {slot: this})
			}
			EquipmentSlot
			{
				position.x: -0.25
				position.y: 0.8
				scale: 0.15
				angle: -Math.PI * 3 / 4
				Q.Component.onCompleted: equipment = tab_weapons[(root.wave < 9 ? 2 : 5)].createObject(null, {slot: this})
			}
		}
	}
	Q.Component
	{
		id: score_factory
		Q.Label
		{
			text: qsTr("score %1").arg(Math.floor(score)) + (state_enemy_generation.active && wave > 1 ? " +" + Math.floor(new_score) : "") + "<br>" + (state_enemy_generation.active ? qsTr("vague %1 %2").arg(wave).arg(Math.ceil(10 * (1 - timer_respawn.fuzzy_value))) : qsTr("vague %1").arg(wave))
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}
	StateMachine
	{
		begin: state_enemy_generation
		active: game_running
		State
		{
			id: state_enemy_generation
			onEntered: fight = false
			SignalTransition
			{
				targetState: state_lapin_1
				signal: timer_respawn.value_changed
				guard: timer_respawn.value && (root.wave <= tab_enemy.length)
				onTriggered:
				{
					for(var i = 0; i < root.tab_enemy[root.wave-1][0]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[wave - 1][0]
						drone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + Math.cos(angle)*40, ship.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
					for(var i = 0; i < root.tab_enemy[root.wave-1][1]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[wave - 1][1] - Math.PI * 1 / 16
						dualdrone_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + Math.cos(angle)*40, ship.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
					for(var i = 0; i < root.tab_enemy[root.wave-1][2]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[wave - 1][2] - Math.PI * 2 / 16
						starship_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + Math.cos(angle)*40, ship.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
					for(var i = 0; i < root.tab_enemy[root.wave-1][3]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[wave - 1][3] - Math.PI * 3 / 16
						lapin_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + Math.cos(angle)*40, ship.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
					for(var i = 0; i < root.tab_enemy[root.wave-1][4]; ++i)
					{
						var angle = 2 * Math.PI * i / tab_enemy[wave - 1][4] - Math.PI * 4 / 16
						vauban_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x + Math.cos(angle)*40, ship.position.y + Math.sin(angle)*40), angle: angle - Math.PI * 1 / 2})
					}
				}
			}
			SignalTransition
			{
				targetState: state_end
				signal: state_enemy_generation.propertiesAssigned
				guard: root.end
				onTriggered:
				{
					platform.set_bool("survival-end")
					stack.push({item: Qt.resolvedUrl("survival-end.qml"), properties: {wave: -1, score: score}})
				}
			}
		}
		State
		{
			onEntered: fight = true
			State
			{
				id: state_lapin_1
				SignalTransition {targetState: state_lapin_2; signal: lapin_timer_1.value_changed; guard: lapin_timer_1.value}
			}
			State
			{
				id: state_lapin_2
				SignalTransition {targetState: state_lapin_1; signal: lapin_timer_2.value_changed; guard: lapin_timer_2.value}
			}

			SignalTransition
			{
				targetState: state_enemy_generation
				signal: root.enemy_countChanged
				guard: !root.enemy_count
				onTriggered:
				{
					new_score = (1 - bonus_score.fuzzy_value) * 50 * wave
					score += new_score
					saved_game.add_science(2)
					saved_game.add_matter(500)
					if(root.wave != 20)
					{
						root.wave++
					}
					else
					{
						root.end = true
					}
				}
			}
		}
		State
		{
			id: state_end
		}
	}
	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["boss-2"])
		jobs.run(scene_view, "preload", ["folks/heter/2"])
		jobs.run(scene_view, "preload", ["folks/heter/6"])
		jobs.run(scene_view, "preload", ["folks/heter/1"])
		jobs.run(scene_view, "preload", ["vide"])
		jobs.run(scene_view, "preload", ["chapter-1/boss"])
		jobs.run(music, "preload", ["survival/main"])
		jobs.run(music, "objectName", ["survival/main"])

		saved_game.add_science(3)
		saved_game.add_matter(1000)
		groups.init(scene)

		var score_print = score_factory.createObject(screen_hud)
		root.finalize = function() {score_print.destroy()}
	}
}
