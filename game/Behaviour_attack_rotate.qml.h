#include "Behaviour_attack.h"
#include "meta.private.h"

namespace aw {
namespace game {
namespace {

struct Behaviour_attack_rotate
: Behaviour_attack
{
	AW_DECLARE_OBJECT_STUB(Behaviour_attack_rotate)
	AW_DECLARE_PROPERTY_STORED(float, target_distance) = 0;

	void attack(b2Vec2 const &target) override
	{
		b2Vec2 direction = target - body()->position();
		float distance = direction.Normalize();

		body()->set_target_direction(b2Vec2_zero);
		body()->set_target_angular_velocity(1);

		body()->set_relative_velocity(b2Vec2_zero);
		body()->set_target_velocity(std::max(-1.f, std::min(1.f, (distance - target_distance()) / body()->max_speed() / scene()->time_step() * .5f)) * direction); // 0.5 = amortissement trouvé empiriquement. Je ne sais pas pourquoi il est nécessaire.
	}
};

AW_DEFINE_OBJECT_STUB(Behaviour_attack_rotate)
AW_DEFINE_PROPERTY_STORED(Behaviour_attack_rotate, target_distance)

}
}
}
