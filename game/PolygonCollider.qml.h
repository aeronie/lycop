#include <Box2D.h>

#include "Collider.h"
#include "meta.private.h"

Q_DECLARE_METATYPE(b2Vec2)
Q_DECLARE_TYPEINFO(b2Vec2, Q_PRIMITIVE_TYPE);

namespace aw {
namespace game {
namespace {

struct PolygonCollider
: Collider
{
	AW_DECLARE_OBJECT_STUB(PolygonCollider)
	AW_DECLARE_PROPERTY_STORED(QVariantList, vertexes);

	void initialize() override
	{
		b2PolygonShape shape;
		update(&shape);
		Collider::initialize(&shape);
	}

	void update(b2Shape *shape)
	{
		Q_ASSERT(vertexes_.count() > 2);
		b2Vec2 vertexes[vertexes_.count()];
		for(int i = 0; i < vertexes_.count(); ++i)
		{
			Q_ASSERT(vertexes_[i].canConvert<b2Vec2>());
			vertexes[i] = transform_point(vertexes_[i].value<b2Vec2>());
		}
		static_cast<b2PolygonShape *>(shape)->Set(vertexes, vertexes_.count());
	}

	void update_transform() override
	{
		if(!fixture_) return;
		update(fixture_->GetShape());
		Collider::update_transform();
	}

protected:

	PolygonCollider()
	{
		set_radius(b2_polygonRadius);
	}
};

void PolygonCollider::set_vertexes(QVariantList const &vertexes)
{
	if(vertexes_ == vertexes) return;
	vertexes_ = vertexes;
	if(fixture_) update(fixture_->GetShape());
	Q_EMIT vertexes_changed(vertexes_);
}

AW_DEFINE_OBJECT_STUB(PolygonCollider)

}
}
}
