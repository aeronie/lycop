import QtQuick 2.0
import aw.game 0.0 as A

Item
{
	property Component splash: null
	property QtObject scene: null
	property bool game_over: false
	property var finalize: undefined
	property bool main_menu: false
	property string title
	property bool cutscene: false
	property bool collider_trash: true
	property var trash
	property real trash_scale
	property int trash_group: -1000
	function add_to_trash(ship, bullet)
	{
		var a = [bullet, bullet.scale]
		bullet.Component.destruction.connect(function() {a[0] = null})
		trash.push(a)
	}
	Component.onCompleted:
	{
		for(var a = 0; a < 2; ++a)
		{
			scene.contact(trash_group, a+groups.bullet).begin.connect(add_to_trash)
			scene.contact(trash_group, a+groups.bullet_only_shield).begin.connect(add_to_trash)
			scene.contact(trash_group, a+groups.bullet_acid).begin.connect(add_to_trash)
			scene.contact(trash_group, a+groups.laser).begin.connect(add_to_trash)
			scene.contact(trash_group, a+groups.roquette).begin.connect(add_to_trash)
			scene.contact(trash_group, a+groups.missile).begin.connect(add_to_trash)
			scene.contact(trash_group, a+groups.laser_beam).begin.connect(add_to_trash)
		}
		scene.contact(trash_group, groups.reflector_1).begin.connect(add_to_trash)
		scene.contact(trash_group, groups.reflector_2).begin.connect(add_to_trash)
		scene.contact(trash_group, groups.reflector_only_player).begin.connect(add_to_trash)
		scene.contact(trash_group, groups.teleporter).begin.connect(add_to_trash)
		scene.contact(trash_group, groups.harpoon).begin.connect(add_to_trash)
		scene.contact(trash_group, groups.mine).begin.connect(add_to_trash)
		scene.contact(trash_group, groups.loot).begin.connect(add_to_trash)
	}
	onCutsceneChanged:
	{
		if(ship)
		{
			if(cutscene)
			{
				inputs.enabled = false
				ship.type = A.Body.STATIC
				if(collider_trash)
				{
					ship.collider.group = trash_group
					ship.collider.radius = 1e9
				}
				controls.speed_boost.enable(false)
				controls.weapons_boost.enable(false)
				controls.shield_boost.enable(false)
				for(var i = 0; i < ship.slots.length; ++i)
				{
					var e = ship.slots[i].equipment
					if(e && e.shooting) e.shooting = false
				}
				trash = []
				animation.restart()
			}
			else
			{
				inputs.enabled = true
				ship.type = A.Body.DYNAMIC
				if(collider_trash)
				{
					ship.collider.group = 0
					ship.collider.radius = 0
				}
				animation.stop()
			}
		}
	}
	NumberAnimation on trash_scale
	{
		id: animation
		from: 1
		to: 0
		onStopped:
		{
			for(var i in trash) if(trash[i][0]) trash[i][0].deleteLater()
			trash = []
		}
	}
	onTrash_scaleChanged:
	{
		for(var i in trash) if(trash[i][0]) trash[i][0].scale = trash[i][1] * trash_scale
	}
}
