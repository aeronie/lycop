import aw.game 0.0

Gyroscope
{
	property int level: 1
	health: max_health
	max_health: 200 +~~body.health_boost
	multiplicateur: [0,1,3][level]
	image: Image
	{
		material: "equipments/gyroscope"
		scale: 0.5
		mask: mask_equipment(parent)
	}
}
