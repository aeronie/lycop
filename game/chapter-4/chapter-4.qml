import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-4/chapter-4.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	property alias ship: ship
	signal one_boss_destroyed
	signal concentration_done
	signal screen_cleared
	property var camera_position: Qt.point(0, 0)
	property var last_ship_position: Qt.point(0, 0)
	property bool save_boss : saved_game.self.get("save_boss_4", false)
	property bool mod_easy : saved_game.self.get("mod_easy", false)
	property real evolution: 0
	property int connection: 0
	property int nb_boom: 8
	property int nb_boom_finish: 0
	property bool maze: true
	property real t0: 0
	property real timer_incoming: 0
	property real t1: 0
	property real ti: 0
	property real base_x: 110
	property real base_y: -680
	property real ship_x: 0
	property real ship_y: 0
	property real ship_angle: 0
	property bool detected: false
	property bool detected_dialogue: true
	property bool cathedrale_dialogue: true
	property bool clear: false
	property var info_print
	property var info_boss
	property var last_boss
	onConnectionChanged:
	{
		if(connection == 0)
		{
			evolution = 0
		}
	}
	property var tab_detector: [
	[0,50],
	[45,40],[-45,40],
	[45,0],[-45,0],
	[45,-40],[-45,-40],
	[45,-80],[-45,-80],
	[45,-120],[-45,-120],
	[45,-160],[-45,-160],
	[45,-200],[-45,-200],
	[45,-240],[-45,-240],
	[45,-280],[-45,-280],
	[45,-320],[-45,-320],
	[45,-360],[-45,-360],
	[-45,-400],[0,-430],
	[45,-445],[85,-430],
	[85,-335],[125,-410],
	[125,-320],[165,-390],
	[165,-300],[205,-370],
	[205,-280],[245,-350],
	[245,-260],[285,-280],
	[325,-340],
	[335,-380],[245,-380],
	[333,-420],[247,-420],
	[331,-460],[249,-460],
	[330,-500],[250,-500],
	[330,-540],[250,-540],
	[330,-580],[250,-620],
	[300,-605],
	[230,-540],[230,-620],
	[210,-540],[210,-620],
	[190,-540],[190,-620],
	[170,-540],[170,-620],
	[150,-540],[150,-620],
	[100,-555],[70,-620]
	]

	property var tab_detector_hard: [
	[-140,-450],[-140,-340],
	[-140,-230],[-140,-120],
	[-140,-10],[-140,100],[-50,100],
	[70,100],[150,100],
	[150,-10],[150,-120],
	[150,-230],[250,-170],
	[300,-200],[400,-300],[420,-200],
	[400,-410],[400,-520],
	[400,-630],[400,-710],
	[290,-710],[180,-710],
	[40,-710],[-70,-710],
	[-70,-600],[-70,-590],
	[-70,-480],[-140,-480],
	[40,-520],[170,-450]
	]

	property var tab_drone_search: [
	[-10,-60,10,-60],
	[-10,-100,10,-100],
	[-10,-220,10,-220],
	[-10,-300,10,-300],
	[-10,-340,10,-340],
	[0,-380,35,-400],
	[155,-344,175,-344],
	[290,-440,290,-402],
	[290,-360,290,-398]
	]

	property var tab_drone_rotate: [
	[0,-140],
	[0,-180],
	[0,-220],
	[0,-260],
	[0,-340],
	[90,-380],
	[117,-366],
	[204,-324],
	[290,-480],
	[290,-520],
	[110,-600],
	[106,-490]
	]
	property var tab_half_rotate: [
	[244,-301]
	]

	property var bgd_shift_x:-13.233
	property var bgd_shift_y:-8.439

	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("%1,%2").arg(ship.position.x).arg(ship.position.y)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}

	Q.Component
	{
		id: screen_incoming_boss
		Q.Label
		{
			text: qsTr("Colonisator : %1 secondes").arg(Math.floor(timer_incoming))
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}

	function random_sign() {return Math.random() < 0.5 ? -1 : 1}

	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false
		Animation
		{
			id: timer
			time: 0
			speed: -1
			onTimeChanged:
			{
				root.ti = timer.time
			}
		}
		Vehicle
		{
			id: clear_screen
			scale: 30
			type: Body.KINEMATIC
			position: ship ? ship.position : Qt.point(0,0)
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			CircleCollider
			{
				group: root.clear ? groups.clear_screen : groups.no_blink
			}
		}
		Body
		{
			id: nuit
			position: scene_view.center
			scale: 30
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			Image
			{
				material: "chapter-2/carre_noir_trou"
				z: 0.01
				mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
			}
		}
		Background
		{
			shift_x:bgd_shift_x
			shift_y:bgd_shift_y
			z: altitudes.background_near
			scale: 20
			material: "chapter-4/background/forest"
		}
		Background
		{
			shift_x:bgd_shift_x
			shift_y:bgd_shift_y
			position.y:-520+bgd_shift_y
			y:1
			z: altitudes.background_near+0.00001
			mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			scale: 20
			material: "chapter-4/background/join"
		}
		Background
		{
			shift_x:bgd_shift_x
			shift_y:bgd_shift_y
			position.y:-180+bgd_shift_y
			y:16
			z: altitudes.background_near+0.00001
			mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			scale: 20
			material: "chapter-4/background/city-1"
		}
		Body
		{
			id:texture_cathedral
			position.x:360+bgd_shift_x
			position.y:-240+bgd_shift_y
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			scale: 20
			Image
			{
				z: altitudes.background_near+0.00002
				material: "chapter-4/background/cathedral"
				mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
			}
		}
		Body
		{
			id: texture_city_2
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			Repeater
			{
				count: 6 * 6
				Q.Component
				{
					Image
					{
					// 2 * 3 * 20 : 2 => largeur d'une tuile; 3 => on a une "tile-2" toutes les 3 tuiles; 20 => scale.
					// Première tuile à être générée en -120 + bgd_shift_x ; -440 + bgd_shift_y
						position.x: 2 * 3 * 20 * (index % 6) - 120 + bgd_shift_x
						position.y: 2 * 3 * 20 * Math.floor(index / 6) - 440 + bgd_shift_y
						scale: 20
						z: altitudes.background_near+0.00002
						mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
						material: "chapter-4/background/city-2"
					}
				}
			}
		}
		Body
		{
			id: texture_puy
			position.x:120+bgd_shift_x
			position.y:-680+bgd_shift_y
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			scale: 20
			Image
			{
				z: altitudes.background_near+0.00001
				position.x: -2
				position.y: 0
				material: "chapter-4/background/puy-nw"
				mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
			}
			Image
			{
				z: altitudes.background_near+0.00001
				position.x: 0
				position.y: 0
				material: "chapter-4/background/puy-n"
				mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
			}
			Image
			{
				z: altitudes.background_near+0.00001
				position.x: 2
				position.y: 0
				material: "chapter-4/background/puy-ne"
				mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
			}
			Image
			{
				z: altitudes.background_near+0.00001
				position.x: -2
				position.y: 2
				material: "chapter-4/background/puy-sw"
				mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
			}
			Image
			{
				z: altitudes.background_near+0.00001
				position.x: 0
				position.y: 2
				material: "chapter-4/background/puy-s"
				mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
			}
			Image
			{
				z: altitudes.background_near+0.00001
				position.x: 2
				position.y: 2
				material: "chapter-4/background/puy-se"
				mask:  Qt.rgba(1,1,1, root.save_boss ? 0 : 1)
			}
		}
		Ship
		{
			id: ship
			position.x: 0
			position.y: save_boss ? 0 : 0 + 6 * (1 - t0)
			scale: save_boss ? 1 : 1 + 4 * (1 - t0)

			function move_ship_to_base() {return Qt.point(t1 * (base_x - ship_x) + ship_x ,t1 * (base_y - ship_y) + ship_y)}

			function face_ship_to_base()
			{
				return (1 - t0) * ship_angle
			}
			function flee()
			{
				return Qt.point(ship_x, ship_y - t0 * 30)
			}

			//ship_angle entre -pi et pi
			function calcul_ship_angle()
			{
				var result = ship.angle
				while(result < -Math.PI)
				{
					result += 2 * Math.PI
				}
				while(result > Math.PI)
				{
					result -= 2 * Math.PI
				}
				return result
			}

			Q.Component.onDestruction:
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}

		Sensor
		{
			id: puy_de_dome
			type: Body.STATIC
			position.x: 110
			position.y: -680
			duration_in: 10
			duration_out: 10
			function coords(x, y) {return Qt.point(x - position.x, y - position.y)}
			icon: 1
			icon_color: "cyan"
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			PolygonCollider
			{
				vertexes:
			[puy_de_dome.coords(110,-645),puy_de_dome.coords(80,-660),puy_de_dome.coords(80,-700),puy_de_dome.coords(140,-700),puy_de_dome.coords(140,-660)]
				group: groups.sensor
				sensor: true
			}
		}

		Sensor
		{
			id: cathedrale
			type: Body.STATIC
			position.x: 323
			position.y: -256
			duration_in: 10
			duration_out: 10
			function coords(x, y) {return Qt.point(x - position.x, y - position.y)}
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			PolygonCollider
			{
				vertexes: [cathedrale.coords(323,-256),cathedrale.coords(343,-273),cathedrale.coords(374,-243),cathedrale.coords(352,-230)]
				group: groups.sensor
				sensor: true
			}
		}

		Vehicle
		{
			id: boom
			position: ship ? ship.position : root.last_ship_position
			property bool first: true
			onFirstChanged:
			{
				if(!root.detected)
				{
					if(!first)
					{
						root.detected = !first
					}
				}
			}
			property real fuzzy_value: root.evolution
			onFuzzy_valueChanged:
			{
				if(boom.fuzzy_value > 0 && boom.first)
				{
					boom.first = false
				}
			}
			property bool active: boom.fuzzy_value == 1
			property bool alive: !game_over
			onAliveChanged: if(!alive) destroy()
			onActiveChanged:
			{
				if(active)
				{
					sound_shooting.play()
					ship.destroy()
				}
			}
			Sound
			{
				id: sound_shooting
				objectName: "chapter-3/aoe_of_death"
				scale: 10
			}
			Image
			{
				material: "chapter-4/boom/viseur"
				z: altitudes.boss - 0.000001
				scale: 4* (1.50 - boom.fuzzy_value)
				mask: Qt.rgba(1, boom.fuzzy_value, boom.fuzzy_value, ship ? boom.fuzzy_value : 0)
			}
		}

		//creation du niveau

		Repeater
		{
			count: tab_detector.length
			Q.Component
			{
				Sensor
				{
					id: detector
					position.x: tab_detector[index][0]
					position.y: tab_detector[index][1]
					type: Body.STATIC
					duration_in: root.scene.seconds_to_frames(1)
					duration_out: 1
					property bool first: true
					property real size: 40
					property bool alive: root.maze
					onAliveChanged: if(!alive) deleteLater()
					onFuzzy_valueChanged:
					{
						if(first)
						{
							first= false
							root.connection++
						}

						if(detector.target_value)
						{
							if(root.connection == 1)
							{
								root.evolution = detector.fuzzy_value
							}
							else
							{
								if(detector.fuzzy_value > root.evolution)
								{
									root.evolution = detector.fuzzy_value
								}
							}
						}
						if(detector.fuzzy_value == 0 && !first)
						{
							first = true
							root.connection--
						}
					}

					Q.Component.onDestruction:
					{
						if(detector.fuzzy_value != 0)
						{
							root.connection--
						}
					}

					Image
					{
						material: "equipments/shield"
						z: altitudes.boss - 0.000001
						scale: detector.size
						mask: Qt.rgba(1, 1, 0.1, root.save_boss ? 0 : 1)
					}
					CircleCollider
					{
						group: groups.sensor
						sensor: true
						radius: detector.size
					}
				}
			}
		}
		Repeater
		{
			count: tab_detector_hard.length
			Q.Component
			{
				Sensor
				{
					id: detector
					position.x: tab_detector_hard[index][0]
					position.y: tab_detector_hard[index][1]
					type: Body.STATIC
					duration_in: root.scene.seconds_to_frames(0.1)
					duration_out: 1
					property bool first: true
					property real size: 60
					property bool alive: root.maze
					onAliveChanged: if(!alive) deleteLater()
					onFuzzy_valueChanged:
					{
						if(first)
						{
							first= false
							root.connection++
						}

						if(detector.target_value)
						{
							if(root.connection == 1)
							{
								root.evolution = detector.fuzzy_value
							}
							else
							{
								if(detector.fuzzy_value > root.evolution)
								{
									root.evolution = detector.fuzzy_value
								}
							}
						}
						if(detector.fuzzy_value == 0 && !first)
						{
							first = true
							root.connection--
						}
					}

					Q.Component.onDestruction:
					{
						if(detector.fuzzy_value != 0)
						{
							root.connection--
						}
					}
					Image
					{
						material: "equipments/shield"
						z: altitudes.boss - 0.000001
						scale: detector.size
						mask: Qt.rgba(1, 0, 0, root.save_boss ? 0 : 1)
					}
					CircleCollider
					{
						group: groups.sensor
						sensor: true
						radius: detector.size
					}
				}
			}
		}

		//les drones
		Repeater
		{
			count: tab_drone_search.length
			Q.Component
			{
				Vehicle
				{
					id: drone
					type: Body.STATIC
					property bool alive: root.maze
					onAliveChanged: if(!alive) deleteLater()
					position: Qt.point(tab_drone_search[index][0],tab_drone_search[index][1])
					property var sensor
					property var behaviour
					property var pos2: Qt.point(tab_drone_search[index][2],tab_drone_search[index][3])
					loot_factory: Explosion
					{
						scale: 2
					}
					function pos()
					{
						return drone.position
					}
					function ang()
					{
						return drone.angle
					}
					Q.Component.onCompleted:
					{
						sensor = detector_drone_factory.createObject(null, {scene: scene, position: drone.position, angle: drone.angle})
						sensor.position = Qt.binding(pos)
						sensor.angle = Qt.binding(ang)
						behaviour= drone_behaviour_search.createObject(null, {parent: scene, drone: drone, pos1: Qt.point(drone.position.x,drone.position.y), pos2: pos2})
					}
					Q.Component.onDestruction:
					{
						sensor.alive= false
						if(!root.detected)
						{
							root.detected = true
						}
						behaviour.alive = false
						//tapis de bomb
						if(root.maze)
						{
							gigaboom_factory.createObject(null, {parent: scene})
						}
					}
					Image
					{
						material: "folks/heter/1"
					}

					CircleCollider
					{
						group: groups.enemy_hull_naked
						sensor: true
					}
				}
			}
		}

		Repeater
		{
			count: tab_drone_rotate.length
			Q.Component
			{
				Vehicle
				{
					id: drone
					type: Body.STATIC
					property bool alive: root.maze
					onAliveChanged: if(!alive) deleteLater()
					position: Qt.point(tab_drone_rotate[index][0],tab_drone_rotate[index][1])
					property var sensor
					property var behaviour
					loot_factory: Explosion
					{
						scale: 2
					}
					function pos()
					{
						return drone.position
					}
					function ang()
					{
						return drone.angle
					}
					Q.Component.onCompleted:
					{
						sensor = detector_drone_factory.createObject(null, {scene: scene, position: drone.position, angle: drone.angle})
						sensor.position = Qt.binding(pos)
						sensor.angle = Qt.binding(ang)
						behaviour = drone_behaviour_rotate.createObject(null, {parent: scene, drone: drone})
					}
					Q.Component.onDestruction:
					{
						sensor.alive= false
						if(!root.detected)
						{
							root.detected = true
						}
						behaviour.alive = false
						//tapis de bomb
						if(root.maze)
						{
							gigaboom_factory.createObject(null, {parent: scene})
						}
					}
					Image
					{
						material: "folks/heter/1"
					}

					CircleCollider
					{
						group: groups.enemy_hull_naked
						sensor: true
					}
				}
			}
		}

		Repeater
		{
			count: tab_half_rotate.length
			Q.Component
			{
				Vehicle
				{
					id: drone
					type: Body.STATIC
					property bool alive: root.maze
					onAliveChanged: if(!alive) deleteLater()
					position: Qt.point(tab_half_rotate[index][0],tab_half_rotate[index][1])
					property var sensor
					property var behaviour
					loot_factory: Explosion
					{
						scale: 2
					}
					function pos()
					{
						return drone.position
					}
					function ang()
					{
						return drone.angle
					}
					Q.Component.onCompleted:
					{
						sensor = detector_drone_factory.createObject(null, {scene: scene, position: drone.position, angle: drone.angle})
						sensor.position = Qt.binding(pos)
						sensor.angle = Qt.binding(ang)
						behaviour = drone_behaviour_half_rotate.createObject(null, {parent: scene, drone: drone})
					}
					Q.Component.onDestruction:
					{
						sensor.alive= false
						if(!root.detected)
						{
							root.detected = true
						}
						behaviour.alive = false
						//tapis de bomb
						if(root.maze)
						{
							gigaboom_factory.createObject(null, {parent: scene})
						}
					}
					Image
					{
						material: "folks/heter/1"
					}

					CircleCollider
					{
						group: groups.enemy_hull_naked
						sensor: true
					}
				}
			}
		}
	}

	// sensor lie au drone
	Q.Component
	{
		id: detector_drone_factory

		Sensor
		{
			id: detector
			type: Body.STATIC
			duration_in: root.scene.seconds_to_frames(0.2)
			function coords(x, y) {return Qt.point(x / 512 - 1, (y - 422) / 512 - 1)} // plink fait des images en 1024*1024
			duration_out: 1
			property bool first: true
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			scale: 10
			onFuzzy_valueChanged:
			{
				if(first)
				{
					first= false
					root.connection++
				}

				if(detector.target_value)
				{
					if(root.connection == 1)
					{
						root.evolution = detector.fuzzy_value
					}
					else
					{
						if(detector.fuzzy_value > root.evolution)
						{
							root.evolution = detector.fuzzy_value
						}
					}
				}
				if(detector.fuzzy_value == 0 && !first)
				{
					first = true
					root.connection--
				}
			}

			Q.Component.onDestruction:
			{
				if(detector.fuzzy_value != 0)
				{
					root.connection--
				}
			}
			Image
			{
				material: "chapter-4/scan"
				z: altitudes.boss - 0.000001
				mask: Qt.rgba(1, 0.25, 0.1, 1)
				position: coords(512,512)
			}
			PolygonCollider
			{
				vertexes: [coords(512,934),coords(340,360),coords(400,220),coords(456,145),coords(512,120),coords(567,145),coords(624,220),coords(684,360)]
				group: groups.sensor
				sensor: true
			}
		}
	}

	//le drone detecteur
	Q.Component
	{
		id: drone_factory

		Vehicle
		{
			id: drone
			type: Body.STATIC
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			property var sensor
			property var behaviour
			property var pos2
			loot_factory: Explosion
			{
				scale: 2
			}
			function pos()
			{
				return drone.position
			}
			function ang()
			{
				return drone.angle
			}
			Q.Component.onCompleted:
			{
				sensor = detector_drone_factory.createObject(null, {scene: scene, position: drone.position, angle: drone.angle})
				sensor.position = Qt.binding(pos)
				sensor.angle = Qt.binding(ang)
				//behaviour= drone_behaviour_search.createObject(null, {parent: scene, drone: drone, pos1: Qt.point(drone.position.x,drone.position.y), pos2: pos2})
				//behaviour = drone_behaviour_rotate.createObject(null, {parent: scene, drone: drone})
				behaviour = drone_behaviour_half_rotate.createObject(null, {parent: scene, drone: drone})
			}
			Q.Component.onDestruction:
			{
				sensor.alive= false
				if(!root.detected)
				{
					root.detected = true
				}
				behaviour.alive = false
				//tapis de bomb
				gigaboom_factory.createObject(null, {parent: scene})
			}
			Image
			{
				material: "folks/heter/1"
			}

			CircleCollider
			{
				group: groups.enemy_hull_naked
				sensor: true
			}
		}
	}

	//comportement des drones

	//se deplace et cherche
	Q.Component
	{
		id: drone_behaviour_search
		Animation
		{
			id: timer
			property var pos1
			property var pos2
			property var drone
			property bool first: true
			property bool go: true
			property bool rotate: false
			property int sens: 1
			property real speed_go: 1/scene.seconds_to_frames(2)
			property int end_rotate: scene.seconds_to_frames(1.5)
			property real speed_rotate: 1/ end_rotate
			property int timer_rotate: 0
			property real last_time: 7
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(first)
				{
					if(pos1 && pos2)
					{
						first=false
						drone.angle = Math.atan2((pos2.y - pos1.y),(pos2.x - pos1.x)) + Math.PI / 2
					}
				}
				else
				{
					if(go && drone)
					{
						drone.position.x += sens * (pos2.x - pos1.x) * speed_go
						drone.position.y += sens * (pos2.y - pos1.y) * speed_go

						if(sens == 1)
						{
							if(Math.sqrt((pos2.x - drone.position.x) * (pos2.x - drone.position.x) + (pos2.y - drone.position.y) * (pos2.y - drone.position.y)) < 0.01)
							{
								rotate = true
								sens = -1
								go = false
							}
						}
						else
						{
							if(Math.sqrt((pos1.x - drone.position.x) * (pos1.x - drone.position.x) + (pos1.y - drone.position.y) * (pos1.y - drone.position.y)) < 0.01)
							{
								rotate = true
								sens = 1
								go = false
							}
						}
					}
					if(rotate)
					{
						drone.angle += (Math.PI) * speed_rotate
						timer_rotate++
						if(timer_rotate >= end_rotate)
						{
							rotate = false
							timer_rotate = 0
							go = true
						}
					}
				}
			}
		}
	}

	//rotation 0->180° 180->0
	Q.Component
	{
		id: drone_behaviour_half_rotate
		Animation
		{
			id: timer
			property var drone
			property bool first: true
			property int sens: 1
			property real speed_rotate: 1/ scene.seconds_to_frames(2)
			property real last_time: 7
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(first)
				{
					if(drone)
					{
						first=false
						drone.angle = - Math.PI / 2
					}
				}
				else
				{
					drone.angle += sens *(Math.PI) * speed_rotate
					if(drone.angle >= Math.PI / 2)
					{
						sens = -1
					}
					if(drone.angle <= - Math.PI / 2)
					{
						sens = 1
					}
				}
			}
		}
	}
	//rotation 360°
	Q.Component
	{
		id: drone_behaviour_rotate
		Animation
		{
			id: timer
			property var drone
			property int sens:1
			property real speed_rotate: 1/ scene.seconds_to_frames(4)
			property real last_time: 7
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				drone.angle += (2 * Math.PI) * speed_rotate
			}
		}
	}

	// bombardement arrive du boss
	Q.Component
	{
		id: boom_boss
		Animation
		{
			id: timer
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property real time_between_boom: 0.8
			property real time_last_boom: 8
			time: 10
			speed: -1
			onTimeChanged:
			{
				if(time > 0)
				{
					root.timer_incoming = time
					if(time_last_boom - time > time_between_boom)
					{
						time_last_boom = time
						var rho = Math.random() * 10
						var theta = Math.random() * Math.PI * 2
						puy_boom_factory.createObject(null, {parent: scene, alive: true, posx : ship.position.x + rho * Math.cos(theta), posy: ship.position.y + rho * Math.sin(theta)})
					}
				}
				else
				{
					if(root.info_boss)
					{
						root.info_boss.destroy()
						//popage du boss
						root.last_boss = colonisator_1_factory.createObject(null, {scene: root.scene, position: Qt.point(ship.position.x, ship.position.y - 50), angle: Math.PI})
					}

					//fin
					timer.alive= false
				}
			}
		}
	}

	// bombardement puy de dome
	//anime une bomb
	Q.Component
	{
		id: puy_boom_factory
		Animation
		{
			id: timer
			property bool first: true
			property bool increment: false
			property bool aoe: false
			property real posx: root.base_x
			property real posy: root.base_y
			property bool finish: false
			property real init_time: 7
			property var boom
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			time: 7.1
			speed: -1
			Q.Component.onDestruction:
			{
				root.nb_boom_finish++
			}
			onTimeChanged:
			{
				if(time < init_time)
				{
					if(first)
					{
						first= false
						boom = boom_factory.createObject(null, {scene: scene, alive: true, position: Qt.point(posx, posy)})
						increment = true
					}
					if(increment)
					{
						boom.fuzzy_value += 1 / root.scene.seconds_to_frames(0.7)
						if(boom.fuzzy_value > 1)
						{
							boom.fuzzy_value = 1
							increment = false
							aoe = true
						}
					}
					if(aoe)
					{
						//animation explosion
						var scale_variation = 0.20
						var scale_finish = 15
						boom.ind_img += 1
						boom.scale += scale_variation
						boom.angle -= 0.1
						if(boom.scale >= scale_finish)
						{
							aoe = false
							finish = true
						}
					}
					if(finish)
					{
						boom.alive = false
						timer.alive = false
					}
				}
			}
		}
	}

	//tapis de bomb
	Q.Component
	{
		id: gigaboom_factory
		Animation
		{
			id: timer
			property var tab_boom:[]
			property bool first: true
			property bool increment: false
			property bool aoe: false
			property bool finish: false
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(first)
				{
					first= false
					for(var i = 0; i < 3 ; ++i)
					{
						for(var j = 0; j < 3 ; ++j)
						{
							tab_boom.push(boom_factory.createObject(null, {scene: scene, position: ship ? Qt.point(ship.position.x + (1-j) * 17, ship.position.y + (1 - i ) * 17) : Qt.point(0,0)}))
						}
					}
					increment = true
				}
				if(increment)
				{
					for(var i = 0; i< tab_boom.length; ++i)
					{
						tab_boom[i].fuzzy_value += 1 / root.scene.seconds_to_frames(1)
						if(tab_boom[i].fuzzy_value > 1)
						{
							tab_boom[i].fuzzy_value = 1
							increment = false
							aoe = true
						}
					}
				}
				if(aoe)
				{
					//animation explosion
					for(var i = 0; i< tab_boom.length; ++i)
					{
						var scale_variation = 0.20
						var scale_finish = 10
						tab_boom[i].ind_img += 1
						tab_boom[i].scale += scale_variation
						tab_boom[i].angle -= 0.1
						//tab_boom[i].opacity -= 1 / (1.5 * scale_finish / scale_variation)
						if(tab_boom[i].scale >= scale_finish)
						{
							aoe = false
							finish = true
						}
					}
				}
				if(finish)
				{
					for(var i = 0; i< tab_boom.length; ++i)
					{
						tab_boom[i].alive = false
					}
					timer.alive = false
				}
			}
		}
	}
	//bomb du tapis de bomb
	Q.Component
	{
		id: boom_factory

		Vehicle
		{
			id: boom
			property bool first: true
			property real fuzzy_value: 0
			property int ind_img: 0
			property bool active: boom.fuzzy_value == 1
			onActiveChanged:
			{
				if(active)
				{
					sound_shooting.play()
				}
			}
			readonly property real damages: 10000
			property bool alive: root.maze
			onAliveChanged: if(!alive) destroy()
			property real opacity: 1

			onScaleChanged:
			{
				collider.update_transform()
			}

			Sound
			{
				id: sound_shooting
				objectName: "chapter-3/aoe_of_death"
				scale: 10
			}
			Image
			{
				material: "chapter-4/boom/viseur"
				z: altitudes.boss - 0.000001
				scale: 4* (1.5 - boom.fuzzy_value)
				mask: Qt.rgba(1, boom.fuzzy_value, boom.fuzzy_value, boom.active ? 0 : boom.fuzzy_value)
			}

			Image
			{
				material: "chapter-4/boom/frappe/" + Math.floor(boom.ind_img / 5) % 3
				z: altitudes.boss - 0.000001
				mask: Qt.rgba(1, 1, 1, boom.active ? boom.opacity : 0)
			}
			Image
			{
				material: "chapter-4/boom/anneaux/" + Math.floor(boom.ind_img / 5) % 3
				z: altitudes.boss - 0.000001
				mask: Qt.rgba(1, 1, 1, boom.active ? boom.opacity : 0)
			}

			CircleCollider
			{
				id: collider
				group: boom.active ? groups.enemy_dot : 0
				sensor: true
			}
		}
	}

	Q.Component
	{
		id: armagedon_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: "transparent"
			scale: 0.01
			property real z: 0.01
			readonly property real damages: 10000
			Q.Component.onCompleted: sound.play()
			onScaleChanged:
			{
				collider.update_transform()
			}
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 5000
			}
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				sensor: true
			}
		}
	}

	Q.Component
	{
		id: explosion_factory
		Explosion
		{
			scale: 4
		}
	}

	//animation mort du boss
	Q.Component
	{
		id: colonisator_animation_end_factory
		Animation
		{
			id: timer
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			function next_boom_position()
			{
				var a = 2 * Math.PI * Math.random()
				var b = Math.random() * boss.scale / 2
				return Qt.point(b * Math.cos(a), b * Math.sin(a))
			}
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var boss: root.last_boss
			property bool swap: true
			property bool swap_light: true
			property bool earthquake: true
			property bool boss_destroyed: true
			property var armagedon

			property real last_boom: 7
			property real time_wait: 14
			time: 7
			speed: -1

			onTimeChanged:
			{
				if(time < 6.9)
				{
					var explosion_step = 0.1
					if(boss)
					{
						if(earthquake)
						{
							var boss_x = timer.boss.position.x
							var boss_y = timer.boss.position.y
							boss_x += swap ? -0.1 : 0.1
							boss_y += swap ? -0.1 : 0.1
							swap= !swap
							boss.position = Qt.point(boss_x, boss_y)
							if(last_boom - time > explosion_step)
							{
								swap_light = !swap_light
								boss.mask_light.a = swap_light ? 1 : 0
								boss.mask_light.a = (boss.mask_light.a == 0) ? 1 : 0
								var boom = timer.next_boom_position()
								last_boom = time
								//vive les matrices de rotation
								explosion_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss_x + boom.x, boss_y + boom.y), scale: boss.scale / 3})
							}
						}
						if(boss.scale > 3)
						{
							boss.scale -= 0.05
							boss.position.x = boss.position.x + 0.06 * Math.sin(boss.angle)
							boss.position.y = boss.position.y - 0.06 * Math.cos(boss.angle)
						}
						else
						{
							boss.alive = false
							time_wait = time
						}
					}
					else
					{
						if(time_wait - time > 1)
						{
							root.one_boss_destroyed()
							time.alive = false
						}
					}
				}
			}
		}
	}
	//boss final qui meurt
	Q.Component
	{
		id: colonisator_animation_final_factory
		Vehicle
		{
			id: boss
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			property bool alive: true
			property var animation
			onAliveChanged: if(!alive) destroy()
			property color mask_light: Qt.rgba(1, 215 / 255, 0 , 1)
			scale: 15
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						scale: 8
						mask: "yellow"
					}
				}
			}
			Q.Component.onCompleted:
			{
				animation = colonisator_animation_end_factory.createObject(null, {parent: root.scene})
			}

			Image
			{
				material: "chapter-4/boss/broken"
				z: altitudes.boss
				mask: Qt.rgba(1,0,0,1)
			}
			Image
			{
				material: "chapter-4/boss/broken/lights-bottom"
				z: altitudes.boss - 0.0000000001
				mask: mask_light
			}
		}
	}

	//comportement phase 3
	Q.Component
	{
		id: colonisator_behaviour_2_factory
		Animation
		{
			id: timer
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024

			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var boss
			property real time_between_boom: 1.5
			property real time_last_boom: 8
			property real time_between_phase: 3
			property real time_last_phase: 8
			property int phase: 0
			time: 7
			speed: -1

			onTimeChanged:
			{
				if(time < 6.9)
				{
					//tire du boss
					if(time_last_boom - time > time_between_boom)
					{
						time_last_boom = time
						var rho = Math.random() * 15
						var theta = Math.random() * Math.PI * 2
						puy_boom_factory.createObject(null, {parent: scene, alive: true, posx : ship.position.x + rho * Math.cos(theta), posy: ship.position.y + rho * Math.sin(theta)})
						boss.number_shoot++
					}

					//changement de phase
					if(time_last_phase - time > time_between_phase)
					{
						if(phase == 0)
						{
							if(boss.megashoot)
							{
								boss.megashoot = false
							}
							if(boss.mask_light.g < 215/255)
							{
								boss.mask_light.g+= 0.01
							}
							else
							{
								boss.mask_light.g = 215 / 255
								time_last_phase = time
								boss.activate_reflection = true
								phase = 1
							}
						}
						else
						{
							if(boss.activate_reflection)
							{
								boss.activate_reflection = false
							}
							if(boss.mask_light.g > 0.02)
							{
								boss.mask_light.g-= 0.01
							}
							else
							{
								boss.mask_light.g = 0
								time_last_phase = time
								phase = 0
								boss.megashoot = true
							}
						}
					}
				}
			}
		}
	}

	//animation transition phase 2 - 3
	Q.Component
	{
		id: colonisator_transition_factory
		Animation
		{
			id: timer
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			function next_boom_position_left()
			{
				var x = Math.random() * (394 - 59) + 59
				var y = Math.random() * (650 - 68) + 68
				return coords(x,y)
			}
			function next_boom_position_right()
			{
				var x = Math.random() * (965 - 630) + 630
				var y = Math.random() * (650 - 68) + 68
				return coords(x,y)
			}
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var boss: root.last_boss
			property bool swap: true
			property bool swap_light: true
			property bool earthquake: true
			property bool wing_fallen: false
			property bool first: true
			property bool first_turn: true
			property bool finish: false
			property bool boss_destroyed: true
			property var armagedon

			property real last_boom: 7
			property real last_turn: 7
			time: 7
			speed: -1

			onTimeChanged:
			{
				var explosion_step = 0.1
				if(earthquake)
				{
					var boss_x = timer.boss.position.x
					var boss_y = timer.boss.position.y
					boss_x += swap ? -0.1 : 0.1
					boss_y += swap ? -0.1 : 0.1
					swap= !swap
					boss.position = Qt.point(boss_x, boss_y)
					if(last_boom - time > explosion_step)
					{
						swap_light = !swap_light
						boss.mask_light.a = swap_light ? 1 : 0
						boss.mask_light.a = (boss.mask_light.a == 0) ? 1 : 0
						var left = timer.next_boom_position_left()
						var right = timer.next_boom_position_right()
						last_boom = time
						//vive les matrices de rotation
						explosion_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss_x + boss.scale * (left.x * Math.cos(boss.angle) - left.y * Math.sin(boss.angle)) , boss_y + boss.scale * (left.y * Math.cos(boss.angle) + left.x * Math.cos(boss.angle))), scale: 8})
						explosion_factory.createObject(null, {scene: boss.scene, position: Qt.point(boss_x + boss.scale * (right.x * Math.cos(boss.angle) - right.y * Math.sin(boss.angle)), boss_y + boss.scale * (right.y * Math.cos(boss.angle) + right.x * Math.sin(boss.angle))), scale: 8})
					}
				}
				if(time < 3)
				{
					if(!finish)
					{
						if(first)
						{
							earthquake = false
							var pos_x = root.last_boss.position.x
							var pos_y = root.last_boss.position.y
							var ang = root.last_boss.angle
							root.last_boss.alive = false
							root.last_boss = colonisator_transition_animation_factory.createObject(null, {scene: root.scene ,position: Qt.point(pos_x, pos_y), angle: ang})
							first = false
						}
						else
						{
							if(!wing_fallen)
							{
								//les ailes tombes
								root.last_boss.wing_left.scale -= 0.1
								root.last_boss.wing_right.scale -= 0.1
								if( root.last_boss.wing_left.scale < 3)
								{
									wing_fallen = true
									root.last_boss.wing_right.deleteLater()
									root.last_boss.wing_left.deleteLater()
									last_turn = time
								}
							}
							else
							{
								if(root.last_boss.ind_turn != 9 && first_turn)
								{
									if(last_turn - time > 0.1)
									{
										last_turn = time
										root.last_boss.ind_turn++
									}
								}
								else
								{
									if(first_turn)
									{
										var pos_x = root.last_boss.position.x
										var pos_y = root.last_boss.position.y
										var ang = root.last_boss.angle
										root.last_boss.alive = false
										root.last_boss = colonisator_final_transition_animation_factory.createObject(null, {scene: root.scene ,position: Qt.point(pos_x, pos_y), angle: ang})
										first_turn = false
									}
									if(root.last_boss.mask_light.b != 0)
									{
										//lights vont se dorer
										if(root.last_boss.mask_light.b > 0.06)
										{
											root.last_boss.mask_light.b -= 0.03
										}
										else
										{
											root.last_boss.mask_light.b = 0
										}
									}
									else
									{
										if(root.last_boss.mask_light.g > 215 / 255)
										{
											root.last_boss.mask_light.g -= 0.01
										}
										else
										{
											root.last_boss.mask_light.g = 215 / 255
											finish = true
										}
									}
								}
							}
						}
					}
					else
					{
						var pos_x = root.last_boss.position.x
						var pos_y = root.last_boss.position.y
						var ang = root.last_boss.angle
						root.last_boss.alive = false
						root.last_boss = colonisator_final_ship_factory.createObject(null, {scene: root.scene ,position: Qt.point(pos_x, pos_y), angle: ang})
						timer.alive = false
					}
				}
			}
		}
	}

	//behaviour boss 1
	Q.Component
	{
		id: colonisator_behaviour_factory
		Animation
		{
			id: timer
			property bool alive: true
			property bool beg_phase_2: true
			property bool anim_phase_2: false
			property bool explosion: true
			property bool armagedon_ini: true
			property var armagedon
			property var boss
			property int phase: 1
			property int sens: 1
			property real last_wall_shoot: 7
			property real last_spawn: 7
			property real vit: 0
			property real vit_max: 0.03
			property real vit_min: -0.03
			onAliveChanged: if(!alive) destroy()
			time: 7
			speed: -1
			onTimeChanged:
			{
				if(time < 6.9)
				{
					if(phase == 1)
					{
						vit += sens * (vit_max - vit_min)/root.scene.seconds_to_frames(8)
						if(vit >= vit_max)
						{
							sens = -1
						}
						if(vit <= vit_min)
						{
							sens = 1
						}
						boss.barrier.angle+= vit
					}
					if(phase == 2)
					{
						if(beg_phase_2)
						{
							beg_phase_2 = false
							anim_phase_2 = true
							boss.max_speed = 0
							boss.max_angular_speed = 0
						}
						if(anim_phase_2)
						{
							if(boss.mask.b > 0)
							{
								if(boss.mask.b > 0.01)
								{
									boss.mask.b -= 0.01
								}
								else
								{
									boss.mask.b = 0
								}

								if(boss.mask.b > 0.58 && boss.ind_crack != 1)
								{
									boss.ind_crack = 1	//craquelure
								}
								if(boss.mask.b > 0.34 && boss.mask.b < 0.58 && boss.ind_crack != 2)
								{
									 boss.ind_crack = 2	//craquelure
								}
								if(boss.mask.b > 0.10 && boss.mask.b < 0.34 && boss.ind_crack != 3)
								{
									 boss.ind_crack = 3	//craquelure
								}
								if(boss.mask.b < 0.10 && boss.ind_crack != 4)
								{
									 boss.ind_crack = 4	//craquelure
								}
							}
							else
							{
								if(boss.mask.r < 1)
								{
									if(1 - boss.mask.r > 0.01)
									{
										boss.mask.r += 0.01
									}
									else
									{
										boss.mask.r = 1
									}
									if(boss.mask.r > 0.70 && boss.ind_crack != 5)
									{
										boss.ind_crack = 5 //craquelure
									}
								}
								else
								{
									//explosion
									if(explosion)
									{
										if(armagedon_ini)
										{
											armagedon = armagedon_factory.createObject(null, {scene: ship.scene, position: Qt.point(boss.position.x, boss.position.y), mask_c: Qt.rgba(1, 0.1 , 0, 1)})
											armagedon_ini = false
										}
										if(ship)
										{
											if(armagedon.scale < 80)
											{
												armagedon.scale += 30 * ship.scene.time_step
												armagedon.angle += 0.001
											}
											else
											{
												var a = armagedon.mask_c.a - ship.scene.time_step
												if(a<=0)
												{
													a=0
													explosion = false
												}
												armagedon.mask_c.a = a
											}
										}
									}

									//fin
									if(!explosion)
									{
										if(armagedon)
										{
											armagedon.alive = false
										}
										anim_phase_2 = false
										boss.mask.a = 0
										boss.max_speed = 8
										boss.max_angular_speed = 3
										last_spawn = time + 3
										boss.activate = true
									}
								}
							}
						}

						if(!beg_phase_2 && !anim_phase_2)
						{
							//effet visuel
							if(boss)
							{
								if(boss.mask_light.r < 0.02 && sens == -1)
								{
									sens = 1
								}
								if(boss.mask_light.r > 0.98 && sens == 1)
								{
									sens = -1
								}
								boss.mask_light.r += sens * 0.01
								boss.mask_light.g += sens * 0.01

								//comportement
								if(last_wall_shoot - time > 0.1 && boss.wall_activate)
								{
									boss.wall_activate = false
								}
								if(last_wall_shoot - time > 6 && !boss.wall_activate)
								{
									boss.wall_activate = true
									last_wall_shoot = time
								}

								if(last_spawn - time > 2 && boss.launcher_activate)
								{
									boss.launcher_activate = false
								}
								if(last_spawn - time > 6 && !boss.launcher_activate)
								{
									boss.launcher_activate = true
									last_spawn = time
								}
							}
						}
					}
				}
			}
		}
	}

	Q.Component
	{
		id: explosion_laser_factory
		Body
		{
			id: component
			type: Body.STATIC
			property bool megaboom: false
			scale: scale_deb
			property color mask: "orange"
			property var material: "chapter-4/explosion-bullet"
			property real ti_local: root.ti
			property real damages: 5000 * scene.time_step
			property real deb: root.ti
			property real scale_finish : megaboom ? 36 : 12
			property real scale_deb: 0.1
			onTi_localChanged:
			{
				if(scale_finish > 0.1)
				{
					scale += (scale_finish - scale_deb)/scene.seconds_to_frames(0.5)
					if(scale > scale_finish)
					{
						scale = scale_finish
					}
					image.mask.a = (scale_finish - scale)/scale_finish
					if(scale == scale_finish)
					{	
						component.deleteLater()
					}
				}
			}
			Q.Component.onCompleted:
			{
				
				deb = root.ti
				sound.play()
			}
			onScaleChanged:
			{
				angle += 0.0001
				collider.update_transform()
			}
			Image
			{
				id: image
				material: component.material
				mask: component.mask
			}
			Sound
			{
				id: sound
				objectName: "chapter-4/bullet_boom"
				scale: 5000
			}
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				sensor: true
			}
		}
	}

	Q.Component
	{
		id: colonisator_canon_factory

		Vehicle
		{
			id: boss
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			property bool alive: true
			property int period: scene.seconds_to_frames(3)
			property bool megashoot: false
			property bool shoot: true
			onAliveChanged: if(!alive) destroy()
			scale: 15
			EquipmentSlot
			{
				id: eq_slot
				position: boss.coords(512,67)
				scale: 0.15/3
				equipment: Weapon
				{
					id: weapon
					shooting: boss.shoot
					property bool mega: boss.megashoot
					property int bullet_group: groups.enemy_dot
					property real bullet_velocity: 5 //utile pour cheater des enemy
					property real damages_aoe: 3000* scene.time_step
					period: boss.period
					range_bullet: scene.seconds_to_frames(2)
					health: max_health
					max_health: 400 +~~body.health_boost
					readonly property Sound sound_shooting: Sound
					{
						objectName: "equipments/qui-poutre/shooting"
						scale: weapon.slot.sound_scale
					}
					bullet_factory: Bullet
					{
						id: bullet
						readonly property int max_range: weapon.range_bullet
						property bool mega: false
						property real scale_temp: mega ? 4 : 2
						scale: scale_temp * Math.min(1, (max_range - range) / scene.seconds_to_frames(0.5))
						onScaleChanged: collider.update_transform()
						range: max_range
						damages: 3000
						angular_velocity: 1
						Image
						{
							material: "chapter-4/boss/gigabullet/" + (Math.floor(scene.time / 5) % 8)
							mask: Qt.rgba(1, range / max_range, 0, 1)
							z: altitudes.bullet
						}
						CircleCollider
						{
							id: collider
							group: weapon.bullet_group
							radius: 0.5
							sensor: true
						}
						Q.Component.onCompleted:
						{
							if(weapon.mega)
							{
								mega = true
							}
							position = weapon.slot.point_to_scene(Qt.point(0, 0))
							angle = weapon.slot.angle_to_scene(0)
							weapon.sound_shooting.play()
							weapon.set_bullet_velocity(this, weapon.bullet_velocity)
						}
						Q.Component.onDestruction:
						{
							explosion_laser_factory.createObject(null, {scene: root.scene ,position: Qt.point(bullet.position.x,bullet.position.y), mask: Qt.rgba(1,0,0,1), megaboom: mega})
						}
					}
				}
			}
		}
	}
	//boss final
	Q.Component
	{
		id: colonisator_final_ship_factory

		Vehicle
		{
			id: boss
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			property bool alive: true
			property bool shoot_canon: true
			property bool megashoot: true
			type: Body.KINEMATIC
			property var canon
			property var comportement
			onAliveChanged: if(!alive) destroy()
			property bool activate_reflection: true
			property color mask_light: Qt.rgba(1, 215 / 255, 0 , 1)
			property int number_shoot: 0
			scale: 15
			icon: 4
			icon_color: "red"

			function boss_pos()
			{
				return boss.position
			}
			function boss_ang()
			{
				return boss.angle
			}
			function boss_mega()
			{
				return boss.megashoot
			}

			max_speed: 5
			max_angular_speed: 1
			Q.Component.onCompleted:
			{
				comportement = colonisator_behaviour_2_factory.createObject(null, {parent: root.scene, boss: boss})
				canon = colonisator_canon_factory.createObject(null, {scene: root.scene ,position: boss.position, angle: boss.angle})
				canon.position = Qt.binding(boss_pos)
				canon.angle = Qt.binding(boss_ang)
				canon.megashoot = Qt.binding(boss_mega)
			}

			Q.Component.onDestruction:
			{
				comportement.alive= false
				canon.alive = false
				root.last_boss = colonisator_animation_final_factory.createObject(null, {scene: root.scene ,position: boss.position, angle: boss.angle, mask_light: boss.mask_light})
			}

			Image
			{
				material: "chapter-4/boss/broken"
				z: altitudes.boss
				mask:
				{
					var x = 0
					if(weapon)
					{
						x = weapon.health / weapon.max_health
					}
					return Qt.rgba(1, x, x, 1)
				}
			}
			Image
			{
				material: "chapter-4/boss/broken/lights-bottom"
				z: altitudes.boss - 0.0000000001
				mask: mask_light
			}

			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 25
			}
			function begin(other)
			{
				var v = direction_from_scene(other.velocity)
				other.velocity = direction_to_scene(Qt.point(v.x, -v.y))
				var a = 2 * angle - other.angle + Math.PI
				var e = scene.time_changed
				function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
				e.connect(f)
				for(var i in other.children)
				{
					var child = other.children[i]
					if(child.group) child.group ^= 1
					if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
				}
			}

			EquipmentSlot
			{
				id: eq_slot
				position: boss.number_shoot % 3 == 0 ? boss.coords(497,673) : (boss.number_shoot % 3 == 1 ? boss.coords(511,662) : boss.coords(526,673))
				scale: 0.15/3
				equipment: Weapon
				{
					id: weapon
					health: max_health
					max_health: 50000
					shooting: boss.shoot_canon
					period: scene.seconds_to_frames(1.5)
					bullet_factory: Body
					{
						id: component
						type: Body.STATIC
						scale: 1
						property color mask: Qt.rgba(0.3,0.3,0.3,1)
						property var material: "explosion"
						Image
						{
							id: image
							material: component.material
							mask: component.mask
						}
						Sound
						{
							id: sound
							objectName: "chapter-4/boss_canon"
							scale: 10
						}
						Q.NumberAnimation
						{
							target: image
							property: "scale"
							from: 0
							to: 1
							duration: 2000
							running: true
							easing.type: Q.Easing.OutCubic
							onStarted:
							{
								sound.play()
								image.scale_changed.connect(update)
							}
						}
						function update()
						{
							image.mask.a = 1 - image.scale
							if(!image.mask.a) component.deleteLater()
						}
						Q.Component.onCompleted:
						{
							position = weapon.slot.point_to_scene(Qt.point(0, 0))
						}
					}
				}
				onEquipmentChanged: boss.alive = false
			}

			PolygonCollider
			{
				vertexes: [boss.coords(401,652),boss.coords(644,652),boss.coords(734,806),boss.coords(649,866),boss.coords(520,908),boss.coords(398,883),boss.coords(298,806)]
				group: boss.activate_reflection ? groups.reflector_only_player : groups.item_destructible
			}
			PolygonCollider
			{
				vertexes: [boss.coords(443,656),boss.coords(483,66),boss.coords(540,66),boss.coords(580,656)]
				group: boss.activate_reflection ? groups.reflector_only_player : groups.item_destructible
			}
		}
	}

	//boss transition
	Q.Component
	{
		id: colonisator_transition_ship_factory

		Vehicle
		{
			id: boss
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			property bool alive: true
			scale: 15
			onAliveChanged: if(!alive) destroy()
			property color mask_light: "white"
			Image
			{
				material: "chapter-4/boss"
				z: altitudes.boss
			}
			Image
			{
				material: "chapter-4/boss/lights"
				z: altitudes.boss - 0.0000000001
				mask: boss.mask_light
			}
		}
	}

	//boss animation transition
	Q.Component
	{
		id: colonisator_final_transition_animation_factory

		Vehicle
		{
			id: boss
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			property bool alive: true
			scale: 15
			onAliveChanged: if(!alive) destroy()
			property color mask_light: "white"
			Image
			{
				material: "chapter-4/boss/broken"
				z: altitudes.boss
			}
			Image
			{
				material: "chapter-4/boss/broken/lights-bottom"
				z: altitudes.boss - 0.0000000001
				mask: mask_light
			}
		}
	}
	//boss animation transition
	Q.Component
	{
		id: colonisator_transition_animation_factory

		Vehicle
		{
			id: boss
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			property bool alive: true
			scale: 15
			onAliveChanged: if(!alive) destroy()
			property var wing_left
			property var wing_right
			property int ind_turn: 0
			Q.Component.onCompleted:
			{
				wing_left = wing_left_factory.createObject(null, {scene: root.scene ,position: boss.position, angle: boss.angle})
				wing_right = wing_right_factory.createObject(null, {scene: root.scene ,position: boss.position, angle: boss.angle})
			}
			Image
			{
				material: "chapter-4/boss/broken/" + ind_turn
				z: altitudes.boss
			}
		}
	}
	Q.Component
	{
		id: wing_left_factory

		Vehicle
		{
			id: boss
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			property bool alive: true
			scale: 15
			onAliveChanged: if(!alive) destroy()
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						scale: 3
						position: Qt.point(root.last_boss.position.x - 15 * Math.cos(root.last_boss.angle),root.last_boss.position.y - 15 * Math.sin(root.last_boss.angle))
					}
				}
			}
			Image
			{
				material: "chapter-4/boss/broken/wing-left"
				z: altitudes.boss
				position: boss.scale > 7.5 ? Qt.point( -(15 - boss.scale) / 15 * 4, 0) : Qt.point( -15 / boss.scale, 0)
			}
		}
	}
	Q.Component
	{
		id: wing_right_factory

		Vehicle
		{
			id: boss
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			property bool alive: true
			scale: 15
			onAliveChanged: if(!alive) destroy()
			loot_factory: Repeater
			{
				Q.Component
				{
					Explosion
					{
						scale: 3
						position: Qt.point(root.last_boss.position.x + 15 * Math.cos(root.last_boss.angle),root.last_boss.position.y + 15 * Math.sin(root.last_boss.angle))
					}
				}
			}
			Image
			{
				material: "chapter-4/boss/broken/wing-right"
				z: altitudes.boss
				position: boss.scale > 7.5 ? Qt.point( (15 - boss.scale) / 15 * 4, 0) : Qt.point( 15 / boss.scale, 0)
			}
		}
	}

	//boss
	Q.Component
	{
		id: colonisator_1_factory

		Vehicle
		{
			id: boss
			function coords(x, y) {return Qt.point(x / 512 - 1, y / 512 - 1)} // plink fait des images de boss en 1024*1024
			property bool wall_activate: false
			property bool launcher_activate: false
			property bool activate: false
			type: Body.KINEMATIC
			property var barrier
			property var canon
			property var comportement
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property bool barrier_on: true
			property color mask: Qt.rgba(0.58, 0, 0.82, 1)
			property color mask_light: "white"
			property int ind_crack: 0
			onBarrier_onChanged:
			{
				if(!barrier_on)
				{
					comportement.phase++
				}
			}
			scale: 15
			max_speed: 8
			max_angular_speed: 3
			icon: 4
			icon_color: "red"

			function barrier_pos()
			{
				return boss.position
			}

			function boss_ang()
			{
				return boss.angle
			}
			function boss_canon_shoot()
			{
				return boss.activate
			}

			Q.Component.onCompleted:
			{
				barrier = barrier_factory.createObject(null, {scene: root.scene ,position: boss.position, boss: boss})
				barrier.position = Qt.binding(barrier_pos)
				canon = colonisator_canon_factory.createObject(null, {scene: root.scene ,position: boss.position, angle: boss.angle, shoot : false})
				canon.position = Qt.binding(barrier_pos)
				canon.angle = Qt.binding(boss_ang)
				canon.shoot = Qt.binding(boss_canon_shoot)
				comportement=colonisator_behaviour_factory.createObject(null, {parent: root.scene, boss: boss})
			}

			Q.Component.onDestruction:
			{
				comportement.alive= false
				canon.alive = false
				root.last_boss = colonisator_transition_ship_factory.createObject(null, {scene: root.scene ,position: boss.position, angle: boss.angle, mask_light: boss.mask_light})
				colonisator_transition_factory.createObject(null, {parent: root.scene})
			}

			Image
			{
				material: "chapter-4/boss"
				z: altitudes.boss
			}
			Image
			{
				material: "chapter-4/boss/lights"
				z: altitudes.boss - 0.0000000001
				mask: boss.mask_light
			}

			Image
			{
				material: "equipments/shield"
				mask: boss.mask
				z: altitudes.shield
			}

			Image
			{
				material: "chapter-4/shield-cracks/" + boss.ind_crack
				mask: boss.mask
				z: altitudes.shield + 0.000000001
			}

			CircleCollider
			{
				group: comportement.explosion ? groups.enemy_invincible : groups.enemy_hull_naked
				radius: comportement.explosion ? 1 : 0.9
			}

			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 25
			}

			//wall generator
			EquipmentSlot
			{
				position: coords(243, 194)
				scale: 0.15/3
				angle: Math.PI * -1/8
				equipment: WallGenerator
				{
					shooting: wall_activate
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(781, 194)
				scale: 0.15/3
				angle: Math.PI * 1/8
				equipment: WallGenerator
				{
					shooting: wall_activate
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(271, 393)
				scale: 0.15/3
				angle: Math.PI * -1/4
				equipment: WallGenerator
				{
					shooting: wall_activate
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(753, 393)
				scale: 0.15/3
				angle: Math.PI * 1/4
				equipment: WallGenerator
				{
					shooting: wall_activate
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(305, 105)
				scale: 0.15/3
				angle: 0
				equipment: WallGenerator
				{
					shooting: wall_activate
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(719, 105)
				scale: 0.15/3
				angle: 0
				equipment: WallGenerator
				{
					shooting: wall_activate
					level: 2
				}
			}

			//vortex
			EquipmentSlot
			{
				position: coords(128, 241)
				scale: 0.15/3
				angle: Math.PI * -1/8
				equipment: VortexGun
				{
					range_bullet: scene.seconds_to_frames(8)
					period: scene.seconds_to_frames(1)
					shooting: boss.activate
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(896, 241)
				scale: 0.15/3
				angle: Math.PI * 1/8
				equipment:VortexGun
				{
					range_bullet: scene.seconds_to_frames(8)
					period: scene.seconds_to_frames(1)
					shooting: boss.activate
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(285, 552)
				scale: 0.15/3
				angle: Math.PI * -1/4
				equipment: VortexGun
				{
					range_bullet: scene.seconds_to_frames(8)
					period: scene.seconds_to_frames(1)
					shooting: boss.activate
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(739, 552)
				scale: 0.15/3
				angle: Math.PI * 1/4
				equipment:VortexGun
				{
					range_bullet: scene.seconds_to_frames(8)
					period: scene.seconds_to_frames(1.5)
					shooting: boss.activate
					level: 2
				}
			}

			//bouclier secondaire
			EquipmentSlot
			{
				position: coords(409, 855)
				scale: 0.15/3
				angle: 0
				equipment: SecondaryShield
				{
					level: 2
				}
			}
			EquipmentSlot
			{
				position: coords(615, 855)
				scale: 0.15/3
				angle: 0
				equipment:SecondaryShield
				{
					level: 2
				}
			}

			//drone spawner cheater
			EquipmentSlot
			{
				position: coords(420, 772)
				scale: 0.15/3
				angle: Math.PI * -1/2
				equipment: Weapon
				{
					id: drone_launcher
					property int drone_count: 0
					shooting: boss.launcher_activate
					signal drone_destroyed()
					property var target_groups: groups.enemy_targets
					onDrone_destroyed: --drone_count
					period: scene.seconds_to_frames(0.1)
					health: max_health
					max_health: 2000
					consumable: Consumable {available: drone_launcher.drone_count < 15}
					image: Image
					{
						material: "equipments/drone-launcher"
						scale: 0.5
						mask: mask_equipment(parent)
					}
					bullet_factory: Vehicle
					{
						Q.Component.onCompleted:
						{
							++weapon.drone_count
							Q.Component.destruction.connect(weapon.drone_destroyed)
							position = weapon.slot.point_to_scene(Qt.point(0, 0))
							angle = weapon.slot.angle_to_scene(0)
							weapon.set_bullet_velocity(this, max_speed)
						}
						scale: 0.3
						max_speed: 20
						icon: 2
						icon_color: "#F00"
						Image
						{
							material: "folks/heter/1"
						}
						Image
						{
							material: "equipments/shield"
							mask: mask_shield(parent)
							scale: 1.1
						}

						CircleCollider
						{
							group: groups.enemy_hull_naked_aggressive_shield
						}
						CircleCollider
						{
							group: groups.enemy_shield_aggressive
							radius: 1.1
						}
						EquipmentSlot
						{
							position.y: 0.5
							scale: 0.8
							equipment: AggressiveShield
							{
								shooting: true
								max_health: 20
								level: 2
							}
						}
						Behaviour_attack_missile
						{
							target_groups: weapon.target_groups
							range: 1000000
						}
					}
				}
			}

			EquipmentSlot
			{
				position: coords(604, 772)
				scale: 0.15/3
				angle: Math.PI * 1/2
				equipment: Weapon
				{
					id: drone_launcher2
					property int drone_count: 0
					shooting: boss.launcher_activate
					signal drone_destroyed()
					property var target_groups: groups.enemy_targets
					onDrone_destroyed: --drone_count
					period: scene.seconds_to_frames(0.1)
					health: max_health
					max_health: 2000
					consumable: Consumable {available: drone_launcher2.drone_count < 15}
					image: Image
					{
						material: "equipments/drone-launcher"
						scale: 0.5
						mask: mask_equipment(parent)
					}
					bullet_factory: Vehicle
					{
						Q.Component.onCompleted:
						{
							++weapon.drone_count
							Q.Component.destruction.connect(weapon.drone_destroyed)
							position = weapon.slot.point_to_scene(Qt.point(0, 0))
							angle = weapon.slot.angle_to_scene(0)
							weapon.set_bullet_velocity(this, max_speed)
						}
						scale: 0.3
						max_speed: 20
						max_angular_speed: 2
						icon: 2
						icon_color: "#F00"
						Image
						{
							material: "folks/heter/1"
						}
						Image
						{
							material: "equipments/shield"
							mask: mask_shield(parent)
							scale: 1.1
						}

						CircleCollider
						{
							group: groups.enemy_hull_naked_aggressive_shield
						}
						CircleCollider
						{
							group: groups.enemy_shield_aggressive
							radius: 1.1
						}
						EquipmentSlot
						{
							position.y: 0.5
							scale: 0.8
							equipment: AggressiveShield
							{
								shooting: true
								max_health: 20
								level: 2
							}
						}
						Behaviour_attack_missile
						{
							target_groups: weapon.target_groups
							range: 1000000
						}
					}
				}
			}
		}
	}

	//asteroid_chaine
	Q.Component
	{
		id: barrier_factory

		Vehicle
		{
			id: barrier
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property int nb_asteroid: 16
			property var boss
			max_speed: 0

			onNb_asteroidChanged:
			{
				if(nb_asteroid == 0)
				{
					alive = false
				}
			}

			Q.Component.onCompleted:
			{
				for(var i = 0; i < nb_asteroid; ++i)
				{
					asteroide_factory.createObject(null, {scene: root.scene ,position: barrier.position, barrier: barrier, num_asteroid: i})
				}
			}

			Q.Component.onDestruction:
			{
				boss.barrier_on = false
			}
		}
	}
	Q.Component
	{
		id: asteroide_factory

		Vehicle
		{
			id: asteroid
			property var barrier
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property int num_asteroid

			max_speed: 0
			max_angular_speed: 3
			scale: 3

			loot_factory: Repeater
			{
				Q.Component {Explosion {scale: 4}}
			}

			
			function begin(other, point)
			{
				var v = other.velocity
				var p = point
				var vect_radian = Qt.point(point.x - asteroid.position.x, point.y - asteroid.position.y)
				var tempx = vect_radian.x
				var tempy = vect_radian.y
				vect_radian.x = tempx / Math.sqrt(tempx * tempx + tempy * tempy)
				vect_radian.y = tempy / Math.sqrt(tempx * tempx + tempy * tempy)
				var point_dep = Qt.point(p.x - v.x, p.y - v.y)
				var vect_dep = Qt.point(point_dep.x - p.x, point_dep.y - p.y)
				
				// AH = <AC.v>/norm(v) * v
				var prod_proj = (vect_dep.x * vect_radian.x + vect_dep.y * vect_radian.y) / Math.sqrt(vect_radian.x * vect_radian.x + vect_radian.y * vect_radian.y)
				
				var vect_proj = Qt.point(prod_proj * vect_radian.x, prod_proj * vect_radian.y)
				
				var point_proj = Qt.point(p.x + vect_proj.x, p.y + vect_proj.y)
				var vect_ortho = Qt.point(point_proj.x - point_dep.x, point_proj.y - point_dep.y)
				
				var point_final = Qt.point(vect_ortho.x + point_proj.x, vect_ortho.y + point_proj.y)
				var vect_velocity =  Qt.point(point_final.x - p.x, point_final.y - p.y)
				
				other.velocity = Qt.point(vect_velocity.x, vect_velocity.y)
				var a = Math.atan2(vect_velocity.y, vect_velocity.x) + Math.PI / 2
				
				var e = scene.time_changed
				function f() {other.angle = a; e.disconnect(f)} // on ne peut pas changer l'angle pendant b2World::Step()
				e.connect(f)
				for(var i in other.children)
				{
					var child = other.children[i]
					if(child.group) child.group ^= 1
					if(child.target_groups) for(var j in child.target_groups) child.target_groups[j] ^= 1
				}
			}

			function barrier_pos()
			{
				return Qt.point(barrier.position.x + 20 * Math.cos(barrier.angle + Math.PI/8 * num_asteroid) ,barrier.position.y + 20 * Math.sin(barrier.angle + Math.PI/8 * num_asteroid))
			}

			Q.Component.onCompleted:
			{
				position = Qt.binding(barrier_pos)
			}

			Q.Component.onDestruction:
			{
				barrier.nb_asteroid--
			}
			Image
			{
				material: "asteroid/" + num_asteroid % 3
				z: altitudes.boss - 0.0000000001
				angle: Math.random() * 2 * Math.PI
			}
			CircleCollider
			{
				group: groups.reflector_only_player
			}
			Behaviour_attack_circle
			{
				target_groups: groups.enemy_targets
				target_angle: 0
				target_distance: 25
				max_distance: 50
			}

			EquipmentSlot
			{
				scale: 1/4
				equipment: HeavyLaserGun
				{
					range_bullet: asteroid.scene.seconds_to_frames(3)
					level: 2
					max_health: 10
					shooting: true
					period: asteroid.scene.seconds_to_frames(1/3.5)
				}
			}
		}
	}

	Q.Component
	{
		id: detector_factory

		Sensor
		{
			id: detector
			type: Body.STATIC
			duration_in: root.scene.seconds_to_frames(1)
			duration_out: 1
			property bool first: true
			property real size: 40
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			onFuzzy_valueChanged:
			{
				if(first)
				{
					first= false
					root.connection++
				}

				if(detector.target_value)
				{
					if(root.connection == 1)
					{
						root.evolution = detector.fuzzy_value
					}
					else
					{
						if(detector.fuzzy_value > root.evolution)
						{
							root.evolution = detector.fuzzy_value
						}
					}
				}
				if(detector.fuzzy_value == 0 && !first)
				{
					first = true
					root.connection--
				}
			}

			Q.Component.onDestruction:
			{
				if(detector.fuzzy_value != 0)
				{
					root.connection--
				}
			}
			Image
			{
				material: "folks/heter/1"
			}
			Image
			{
				material: "equipments/shield"
				z: altitudes.boss - 0.000001
				scale: detector.size
				mask: Qt.rgba(1, 1, 0.1, 1)
			}
			CircleCollider
			{
				group: groups.sensor
				sensor: true
				radius: detector.size
			}
		}
	}

	Q.Component
	{
		id: clear_screen_factory
		Vehicle
		{
			scale: 50
			type: Body.KINEMATIC
			CircleCollider
			{
				group: groups.clear_screen
			}
		}
	}
	Q.Component
	{
		id: clear_screen_animation
		Animation
		{
			id: timer
			property bool first_time: true
			property var clear_screen
			time: 0.1
			speed: -1

			onTimeChanged:
			{
				if(first_time)
				{
					clear_screen= clear_screen_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x,ship.position.x)})
					first_time= false
				}
				if(time<0)
				{
					clear_screen.deleteLater()
					screen_cleared()
					timer.deleteLater()
				}
			}
		}
	}
	StateMachine
	{
		begin: state_story_0 //state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_story_0 // state_story_0 //state_colonisator//state_test_1
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
					//drone_factory.createObject(null, {scene: scene, position: Qt.point(10, -10), angle: 0, pos2: Qt.point(-10, -10)})

					/*root.maze = false
					ship.position = Qt.point(0,0)
					camera_position = undefined
					ship.scale = 1*/
					//root.last_boss = colonisator_final_ship_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y - 30), mask_light: Qt.rgba(1,215/255,0,1)})
					//root.last_boss = colonisator_1_factory.createObject(null, {scene: scene, position: Qt.point(ship.position.x, ship.position.y - 30)})
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				guard: !root.save_boss
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 1000}
				onTriggered:
				{
					music.objectName = "chapter-4/just_among_us"
					saved_game.set("travel/chapter-4/chapter-4", false)
					saved_game.set("travel/chapter-2/chapter-2c", false)
				}
			}
			SignalTransition
			{
				targetState: state_after_video_opening
				signal: state_story_0.propertiesAssigned
				guard: root.save_boss 
				onTriggered:
				{
					music.objectName = "chapter-1/boss"
					root.maze = false
					ship.position = Qt.point(0,0)
					camera_position = undefined
					ship.scale = 1
				}
			}
			
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					ship.scale = 1
					ship.position = Qt.point(0,0)
					camera_position = undefined
					messages.add("nectaire/normal", qsTr("story begin 1"))
					messages.add("lycop/normal", qsTr("story begin 2"))
					messages.add("nectaire/normal", qsTr("story begin 3"))
					messages.add("lycop/normal", qsTr("story begin 4"))
				}
			}
		}
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_2; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}

			SignalTransition
			{
				targetState: state_dialogue_2
				signal: root.detectedChanged
				guard: root.detected && root.detected_dialogue
				onTriggered:
				{
					root.detected_dialogue = false
					messages.add("nectaire/normal", qsTr("detected"))
				}
			}

			SignalTransition
			{
				targetState: state_dialogue_2
				signal: cathedrale ? cathedrale.value_changed : state_story_2.propertiesAssigned
				guard: root.cathedrale_dialogue
				onTriggered:
				{
					saved_game.set("secret_cathedrale", true)
					platform.set_bool("secret-cathedrale")
					root.cathedrale_dialogue = false
					saved_game.add_science(2)
					messages.add("lycop/normal", qsTr("cathedrale 1"))
					messages.add("nectaire/normal", qsTr("cathedrale 2"))
					messages.add("lycop/normal", qsTr("cathedrale 3"))
					messages.add("nectaire/neutral", qsTr("cathedrale 4"))
					messages.add("nectaire/normal", qsTr("cathedrale 5"))
				}
			}

			SignalTransition
			{
				targetState: state_clear
				signal: puy_de_dome ? puy_de_dome.value_changed : state_story_2.propertiesAssigned
				onTriggered:
				{
					clear_screen_animation.createObject(null, {parent: scene})
					root.clear = true
				}
			}
		}

		State
		{
			id: state_clear
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_story_3
				signal: screen_cleared
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 500}
				PropertyAnimation {targetObject: root; propertyName: "t1"; duration: 2000}
				onTriggered:
				{
					ship.angle = ship.calcul_ship_angle()
					ship_angle = ship.angle
					ship.angle = Qt.binding(ship.face_ship_to_base)
					ship_x = ship.position.x
					ship_y = ship.position.y
					ship.position = Qt.binding(ship.move_ship_to_base)
				}
			}
		}

		State {id: state_dialogue_2; SignalTransition {targetState: state_story_2; signal: state_dialogue_2.propertiesAssigned; guard: !messages.has_unread}}

		State
		{
			id: state_story_3
			AssignProperty {target: root; property: "t0"; value: 1}
			AssignProperty {target: root; property: "t1"; value: 1}

			SignalTransition
			{
				targetState: state_dialogue_3
				signal: state_story_3.propertiesAssigned
				onTriggered:
				{
					ship.angle = 0
					ship_x = base_x
					ship_y = base_y
					ship.position.x = base_x
					ship.position.y = base_y
					messages.add("nectaire/normal", qsTr("puy 1"))
					messages.add("lycop/normal", qsTr("puy 2"))
					messages.add("nectaire/normal", qsTr("puy 3"))
					messages.add("lycop/normal", qsTr("puy 4"))
					messages.add("cnes/normal", qsTr("puy 5"))
					messages.add("cnes/normal", qsTr("puy 6"))
					messages.add("cnes/sad", qsTr("puy 7"))
					messages.add("cnes/sad", qsTr("puy 8"))
					messages.add("cnes/sad", qsTr("puy 9"))
					messages.add("nectaire/normal", qsTr("puy 10"))
					saved_game.add_science(1)
				}
			}
		}
		State
		{
			id: state_dialogue_3
			SignalTransition
			{
				targetState: state_story_4
				signal: state_dialogue_3.propertiesAssigned
				guard: !messages.has_unread
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 1000}
				onTriggered:
				{
					ship.position = Qt.binding(ship.flee)
					camera_position = Qt.point(ship_x,ship_y)
				}
			}
		}
		State
		{
			id: state_story_4
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_story_5
				signal: state_story_4.propertiesAssigned
				onTriggered:
				{
					ship.position = Qt.point(ship_x, ship_y - 30)
					//animation boom boom
					for(var i = 0; i < root.nb_boom; ++i)
					{
						puy_boom_factory.createObject(null, {parent: scene, init_time: 7 - i * 0.8 })
					}
				}
			}
		}
		State
		{
			id: state_story_5
			AssignProperty {target: scene; property: "running"; value: true}
			AssignProperty {target: root; property: "cutscene"; value: true}
			//stop quand boom boom fini
			SignalTransition
			{
				targetState: state_after_video_opening
				signal: root.nb_boom_finishChanged
				guard: root.nb_boom -1 == root.nb_boom_finish
				onTriggered:
				{
					screen_video.video.file = ":/data/game/chapter-4/descente_colonisator.ogg"
					function f ()
					{
						music.objectName = "chapter-1/boss"
						root.maze = false
						ship.position = Qt.point(0,0)
						camera_position = undefined
						if(!root.save_boss && mod_easy)
						{
							saved_game.set("save_boss_4", true)
							scene_loader.save()
						}
						screen_video.video.file_changed.disconnect(f)
					}
					screen_video.video.file_changed.connect(f)
				}
			}
		}
		State
		{
			id: state_after_video_opening
			SignalTransition
			{
				targetState: state_colonisator
				signal: state_after_video_opening.propertiesAssigned
				guard:!screen_video.video.file
				onTriggered:
				{
					info_boss = screen_incoming_boss.createObject(screen_hud)
					boom_boss.createObject(null, {parent: scene})
				}
			}
		}
		State
		{
			id: state_colonisator
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_after_colonisator_dialogue
				signal: one_boss_destroyed
				onTriggered:
				{
					messages.add("nectaire/normal", qsTr("end 1"))
					messages.add("lycop/normal", qsTr("end 2"))
					messages.add("nectaire/normal", qsTr("end 3"))
					messages.add("lycop/neutral", qsTr("end 4"))
					messages.add("nectaire/normal", qsTr("end 5"))
					messages.add("lycop/angry", qsTr("end 6"))
					messages.add("nectaire/normal", qsTr("end 7"))
					messages.add("lycop/normal", qsTr("end 8"))
					messages.add("nectaire/normal", qsTr("end 9"))
					messages.add("lycop/normal", qsTr("end 10"))
					messages.add("nectaire/normal", qsTr("end 11"))
				}
			}
		}
		State
		{
			id: state_after_colonisator_dialogue
			SignalTransition
			{
				targetState: state_end
				signal: state_after_colonisator_dialogue.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					saved_game.add_science(2)
					saved_game.add_matter(4000)
					saved_game.set("travel/chapter-5/chapter-5", true)
					saved_game.set("travel/chapter-2/chapter-2c", true)
					saved_game.set("travel/chapter-4/chapter-4b", true)
					saved_game.set("niv_cour", 5)
					saved_game.set("file", "game/chapter-transition/chapter-transition.qml")
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}

	Q.Component.onCompleted:
	{
	
		jobs.run(scene_view, "preload", ["chapter-4/explosion-bullet"])
		jobs.run(scene_view, "preload", ["chapter-4/boom/viseur"])
		jobs.run(scene_view, "preload", ["chapter-4/boss"])
		jobs.run(scene_view, "preload", ["chapter-4/boss/broken"])
		jobs.run(scene_view, "preload", ["chapter-4/boss/broken/lights-bottom"])
		jobs.run(scene_view, "preload", ["chapter-4/boss/broken/wing-left"])
		jobs.run(scene_view, "preload", ["chapter-4/boss/broken/wing-right"])
		jobs.run(scene_view, "preload", ["chapter-4/boss/lights"])
		jobs.run(scene_view, "preload", ["chapter-4/scan"])
		jobs.run(scene_view, "preload", ["explosion"])
		jobs.run(scene_view, "preload", ["chapter-4/background/cathedral"])
		jobs.run(scene_view, "preload", ["chapter-4/background/city-1"])
		jobs.run(scene_view, "preload", ["chapter-4/background/city-2"])
		jobs.run(scene_view, "preload", ["chapter-4/background/forest"])
		jobs.run(scene_view, "preload", ["chapter-4/background/join"])
		jobs.run(scene_view, "preload", ["chapter-4/background/puy-n"])
		jobs.run(scene_view, "preload", ["chapter-4/background/puy-nw"])
		jobs.run(scene_view, "preload", ["chapter-4/background/puy-ne"])
		jobs.run(scene_view, "preload", ["chapter-4/background/puy-s"])
		jobs.run(scene_view, "preload", ["chapter-4/background/puy-sw"])
		jobs.run(scene_view, "preload", ["chapter-4/background/puy-se"])
		jobs.run(scene_view, "preload", ["chapter-2/carre_noir_trou"])
		jobs.run(scene_view, "preload", ["folks/heter/1"])
		jobs.run(scene_view, "preload", ["equipments/drone-launcher"])
		for(var i = 0; i < 3; ++i) jobs.run(scene_view, "preload", ["asteroid/" + i])
		for(var i = 0; i < 3; ++i) jobs.run(scene_view, "preload", ["chapter-4/boom/anneaux/" + i])
		for(var i = 0; i < 3; ++i) jobs.run(scene_view, "preload", ["chapter-4/boom/frappe/" + i])
		for(var i = 0; i < 6; ++i) jobs.run(scene_view, "preload", ["chapter-4/shield-cracks/" + i])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["chapter-4/boss/broken/" + i])
		for(var i = 0; i < 8; ++i) jobs.run(scene_view, "preload", ["chapter-4/boss/gigabullet/" + i])
		jobs.run(music, "preload", ["chapter-3/aoe_of_death"])
		jobs.run(music, "preload", ["chapter-4/just_among_us"])
		jobs.run(music, "preload", ["chapter-1/boss"])
		jobs.run(music, "preload", ["chapter-4/boss_canon"])
		jobs.run(music, "preload", ["chapter-4/bullet_boom"])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/qui-poutre/" + i])
		for(var i = 0; i < 5; ++i) jobs.run(scene_view, "preload", ["equipments/vortex-gun/" + i])
		jobs.run(scene_view, "preload", ["equipments/shield"])
		groups.init(scene)
		//root.info_print = screen_info_factory.createObject(screen_hud)
		root.finalize = function() {if(info_boss) info_boss.destroy()}
	}
}
