import QtQuick 2.0 as Q
import QtQml.StateMachine 1.0
import aw.game 0.0
import aw.statemachine 0.0
import "qrc:///game"
import "qrc:///state-machine"
import "qrc:///controls" as Q

Chapter
{
	id: root
	splash: Splash
	{
		Q.Image
		{
			z: 5000
			source: "///data/game/chapter-4/chapter-4b.png"
			fillMode: Q.Image.PreserveAspectFit
			Q.Label
			{
				anchors.right: parent.right
				anchors.top: parent.top
				anchors.margins: 20
				text: root.title
				font.pointSize: 20
			}
		}
	}
	readonly property int niv_cour: saved_game.self.get("niv_cour", 5)
	property alias ship: ship
	signal one_boss_destroyed
	signal visited
	property var camera_position: Qt.point(0, 0)
	property var last_ship_position: Qt.point(0, 0)
	property var animation
	property var info_print
	property var info_time
	property int sensor: tab_sensor.length
	property int time: 60
	property real t0: 0

	property var tab_sensor: [
	[14,149],
	[50,109],
	[213.5,205],
	[-128,108],
	[-74,-118],
	[-25.5,-221],
	[3.5,-3.5]]

	function random_sign() {return Math.random() < 0.5 ? -1 : 1}

	Q.Binding {target: scene_view; property: "center"; value: camera_position; when: parent && camera_position !== undefined}
	title: qsTr("title")
	scene: Scene
	{
		running: false

		Background
		{
			z: altitudes.background_near
			scale: 20
			material: "chapter-4/background/city-1"
		}
		Tiles
		{
			id: background_special
			z: altitudes.background_near + 0.000000001
			scale: 20
			mask: "white"
			tiles: [[0,0,'chapter-4/background/city-2'],
					[0,4,'chapter-4/background/city-2'],
					[2,3,'chapter-4/background/city-2'],
					[-1,-5,'chapter-4/background/city-2'],
					[-2,-3,'chapter-4/background/city-2'],
					[-3,3,'chapter-4/background/city-2'],
					[5,5,'chapter-4/background/city-2']]
		}
		Ship
		{
			id: ship
			position.y: 10 * (1 - t0)
			position.x: 10 * (1 - t0)
			angle: - Math.PI / 4

			Q.Component.onDestruction:
			{
				game_over = true
				root.last_ship_position= Qt.point(ship.position.x, ship.position.y)
			}
		}

		//creation du niveau

		Repeater
		{
			count: tab_sensor.length
			Q.Component
			{
				Sensor
				{
					id: building
					property var barre
					property real angle_drill: 0
					function coords(x, y) {return Qt.point((x / 128 - 1), (y / 128 - 1))}
					icon: 2
					position.x: tab_sensor[index][0]
					position.y: tab_sensor[index][1]
					icon_color: "yellow"
					duration_in: root.scene.seconds_to_frames(2)
					duration_out: 30
					scale: 0.5
					onFuzzy_valueChanged:
					{
						barre.fuzzy_value = building.fuzzy_value
						if(fuzzy_value == 1)
						{
							deleteLater()
						}
					}

					Q.Component.onCompleted:
					{
						barre = barre_factory.createObject(null, {scene: scene, position: Qt.point(building.position.x, building.position.y - building.scale - 0.2)})
					}
					Q.Component.onDestruction:
					{
						barre.alive = false
						root.sensor--
						if(root.animation)
						{
							root.animation.time-= 300
							if(root.animation.time < 0)
							{
								root.animation.time = 0
								root.time = 0
							}
						}
						root.visited()
					}
					CircleCollider
					{
						group: groups.sensor
						sensor: true
					}
				}
			}
		}
	}
	Q.Component
	{
		id: boom_timer
		Animation
		{
			id: timer
			property bool first: true
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property var boom
			time: 1570
			speed: -1
			onTimeChanged:
			{
				if(timer.time > 0)
				{
					root.time = Math.floor(timer.time)
				}
				if(timer.time < 0)
				{
					//gros boom en 0,0
					if(first)
					{
						boom=armagedon_factory.createObject(null, {scene: ship.scene, position: Qt.point(0, 0), mask_c: Qt.rgba(0.1, 0.1 , 205/255, 1)})
						sound_factory.createObject(null, {scene: ship.scene, position: Qt.point(ship.position.x, ship.position.y)})
						first = false
					}
					else
					{
						boom.scale += 6
						boom.angle += 0.001
					}
				}
			}
		}
	}
	Q.Component
	{
		id: sound_factory
		Body
		{
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			Q.Component.onCompleted: sound.play()

			Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
			}
		}
	}
	Q.Component
	{
		id: armagedon_factory
		Body
		{
			id: armagedon
			property bool alive: true
			onAliveChanged: if(!alive) destroy()
			property color mask_c: "transparent"
			scale: 0.01
			property real z: 0.01
			readonly property real damages: 10000
			Q.Component.onCompleted:
			{
				boom_s.play()
			}
			property Sound boom_s: Sound
			{
				id: sound
				objectName: "explosion/sound_plus_gros"
				scale: 50000
			}
			onScaleChanged:
			{
				collider.update_transform()
			}
			Image
			{
				id: image
				material: "explosion"
				mask: armagedon.mask_c
				z: armagedon.z
			}
			CircleCollider
			{
				id: collider
				group: groups.enemy_dot
				sensor: true
			}
		}
	}
	Q.Component
	{
		id: screen_info_factory
		Q.Label
		{
			text: qsTr("%1,%2").arg(ship.position.x).arg(ship.position.y)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}

	Q.Component
	{
		id: screen_time
		Q.Label
		{
			text: qsTr("Gerboise Bleue : %1 secondes").arg(Math.floor(root.time))
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.margins: 5
		}
	}
	Q.Component
	{
		id: barre_factory
		Body
		{
			property real fuzzy_value: 0
			scale: 2
			property bool alive: true
			onAliveChanged: if(!alive) destroy()

			Image
			{
				material: "progress-bar/" + Math.floor(fuzzy_value * 9)
				mask: fuzzy_value ? Qt.rgba(1, 1, 1, 1) : Qt.rgba(1, 1, 1, 0)
				z: altitudes.boss
				scale: 1
			}
		}
	}
	StateMachine
	{
		begin: state_story_0 // state_test //
		active: game_running
		State
		{
			id: state_test
			SignalTransition
			{
				targetState: state_story_0 //state_test_1 //
				signal: state_test.propertiesAssigned
				onTriggered:
				{
					saved_game.add_science(30)
					saved_game.add_matter(5000)
				}
			}
		}
		State
		{
			id: state_test_1
			AssignProperty {target: scene; property: "running"; value: true}
		}
		State
		{
			id: state_story_0
			SignalTransition
			{
				targetState: state_story_1
				signal: state_story_0.propertiesAssigned
				PropertyAnimation {targetObject: root; propertyName: "t0"; duration: 1000}
				onTriggered:
				{
					music.objectName = "chapter-4/just_among_us"
					saved_game.set("travel/chapter-" + root.niv_cour + "/chapter-" + root.niv_cour, false)
					saved_game.set("travel/chapter-4/chapter-4b", false)
					saved_game.set("travel/chapter-2/chapter-2c", false)
				}
			}
		}
		State
		{
			id: state_story_1
			AssignProperty {target: root; property: "t0"; value: 1}
			SignalTransition
			{
				targetState: state_dialogue_1
				signal: state_story_1.propertiesAssigned
				onTriggered:
				{
					ship.position = Qt.point(0,0)
					camera_position = undefined
					messages.add("nectaire/normal", qsTr("story begin 1"))
					messages.add("lycop/normal", qsTr("story begin 2"))
					messages.add("nectaire/normal", qsTr("story begin 3"))
					messages.add("lycop/happy", qsTr("story begin 4"))
				}
			}
		}
		State {id: state_dialogue_1; SignalTransition {targetState: state_story_2; signal: state_dialogue_1.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_story_2
			AssignProperty {target: scene; property: "running"; value: true}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: visited
				guard: sensor == 6
				onTriggered:
				{
					root.info_time = screen_time.createObject(screen_hud)
					root.animation = boom_timer.createObject(null, {parent: scene})
					messages.add("lycop/normal", qsTr("story 1 1"))
					messages.add("nectaire/normal", qsTr("story 1 2"))
					messages.add("lycop/normal", qsTr("story 1 3"))
					messages.add("nectaire/normal", qsTr("story 1 4"))
					messages.add("lycop/happy", qsTr("story 1 5"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: visited
				guard: sensor == 5
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("story 2 1"))
					messages.add("nectaire/normal", qsTr("story 2 2"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: visited
				guard: sensor == 4
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("story 3 1"))
					messages.add("nectaire/normal", qsTr("story 3 2"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: visited
				guard: sensor == 3
				onTriggered:
				{
					messages.add("lycop/neutral", qsTr("story 4 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: visited
				guard: sensor == 2
				onTriggered:
				{
					messages.add("lycop/neutral", qsTr("story 5 1"))
				}
			}
			SignalTransition
			{
				targetState: state_dialogue_2
				signal: visited
				guard: sensor == 1
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("story 6 1"))
					messages.add("nectaire/normal", qsTr("story 6 2"))
					messages.add("lycop/angry", qsTr("story 6 3"))
				}
			}
			SignalTransition
			{
				targetState: state_end_dialogue
				signal: visited
				guard: sensor == 0
				onTriggered:
				{
					messages.add("lycop/normal", qsTr("story 7 1"))
					messages.add("nectaire/normal", qsTr("story 7 2"))
					messages.add("lycop/normal", qsTr("story 7 3"))
					messages.add("nectaire/normal", qsTr("story 7 4"))
					messages.add("lycop/normal", qsTr("story 7 5"))
					messages.add("nectaire/normal", qsTr("story 7 6"))
					messages.add("lycop/normal", qsTr("story 7 7"))
					messages.add("nectaire/normal", qsTr("story 7 8"))
					messages.add("lycop/neutral", qsTr("story 7 9"))
					messages.add("lycop/normal", qsTr("story 7 10"))
				}
			}
		}
		State {id: state_dialogue_2; SignalTransition {targetState: state_story_2; signal: state_dialogue_2.propertiesAssigned; guard: !messages.has_unread}}
		State
		{
			id: state_end_dialogue
			SignalTransition
			{
				targetState: state_end
				signal: state_end_dialogue.propertiesAssigned
				guard: !messages.has_unread
				onTriggered:
				{
					saved_game.set("travel/chapter-" + root.niv_cour + "/chapter-" + root.niv_cour, true)
					saved_game.set("travel/chapter-2/chapter-2c", true)
					saved_game.set("secret_tomate", true)
					platform.set_bool("secret-tomate")
					saved_game.set("file", "game/chapter-transition/chapter-transition.qml")
					saved_game.save()
				}
			}
		}
		State
		{
			id: state_end
			SignalTransition
			{
				signal: state_end.propertiesAssigned
				guard: !messages.has_unread
				onTriggered: scene_loader.load(settings.get_last_saved_game())
			}
		}
	}

	Q.Component.onCompleted:
	{
		jobs.run(scene_view, "preload", ["explosion"])
		jobs.run(scene_view, "preload", ["chapter-4/background/city-1"])
		jobs.run(scene_view, "preload", ["chapter-4/background/city-2"])
		for(var i = 0; i < 10; ++i) jobs.run(scene_view, "preload", ["progress-bar/" + i])
		jobs.run(music, "preload", ["chapter-4/just_among_us"])
		groups.init(scene)
		//root.info_print = screen_info_factory.createObject(screen_hud)
		root.finalize = function() {if(info_time) info_time.destroy()}
	}
}
