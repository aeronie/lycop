#pragma once

#include "Observer.h"

#define AW_DECLARE_OBJECT_STUB(_class) \
	Q_OBJECT \
	private: \

#define AW_DECLARE_PROPERTY(_type, _name) \
	Q_PROPERTY(_type _name READ _name WRITE set_##_name NOTIFY _name##_changed DESIGNABLE true STORED false) \
	AW_DECLARE_PROPERTY_write_(_type, _name) \
	AW_DECLARE_PROPERTY_read_(_type, _name) \

#define AW_DECLARE_PROPERTY_READONLY(_type, _name) \
	Q_PROPERTY(_type _name READ _name NOTIFY _name##_changed DESIGNABLE false STORED false) \
	AW_DECLARE_PROPERTY_read_(_type, _name) \

#define AW_DECLARE_PROPERTY_STORED(_type, _name) \
	Q_PROPERTY(_type _name READ _name WRITE set_##_name NOTIFY _name##_changed DESIGNABLE true STORED true) \
	AW_DECLARE_PROPERTY_write_(_type, _name) \
	AW_DECLARE_PROPERTY_read_stored_(_type, _name)

#define AW_DECLARE_PROPERTY_STORED_READONLY(_type, _name) \
	Q_PROPERTY(_type _name READ _name NOTIFY _name##_changed DESIGNABLE false STORED true) \
	AW_DECLARE_PROPERTY_read_stored_(_type, _name)

#define AW_DECLARE_PROPERTY_write_(_type, _name) \
	public: \
	Q_SLOT void set_##_name(_type const &); \
	private: \

#define AW_DECLARE_PROPERTY_read_(_type, _name) \
	public: \
	_type _name() const; \
	protected: \
	Q_SIGNAL void _name##_changed(_type const &); \
	private: \

#define AW_DECLARE_PROPERTY_read_stored_(_type, _name) \
	public: \
	_type const &_name() const { return _name##_; } \
	protected: \
	Q_SIGNAL void _name##_changed(_type const &); \
	private: \
	_type _name##_

#define AW_DECLARE_SIGNAL(_name) \
	public: \
	ObserverList *signal_##_name() { return &_name##_; } \
	private: \
	ObserverList _name##_; \

namespace aw {

/*

Évite le warning « defined but not used ».
GCC ne veut pas d'un int inutilisé, mais une apparemment structure est acceptable.

*/
struct register_type
{
	register_type(int) {}
};

template<class T>
T *static_qobject_cast(QObject *x)
{
	return x && (x->metaObject() == &T::staticMetaObject || x->metaObject()->superClass() == &T::staticMetaObject) ? static_cast<T *>(x) : 0;
}

}
