#if !defined CONFIG_RELEASE

#include <QtQuick>
#include <Box2D.h>

#include "NodePool.h"
#include "game/Scene.h"
#include "meta.private.h"

namespace aw {
namespace {

struct Mesh
: QSGGeometryNode
{
	QSGGeometry geometry_ {QSGGeometry::defaultAttributes_Point2D(), 0};
	QSGFlatColorMaterial material_;

	Mesh()
	{
		setGeometry(&geometry_);
		setMaterial(&material_);
	}

	void set(size_t index, const b2Vec2 &vertex)
	{
		geometry_.vertexDataAsPoint2D()[index].set(vertex.x, vertex.y);
	}
};

struct Node
: QSGTransformNode
, b2Draw
{
	NodePool<Mesh> pool_;
	float &zoom_;

	Node(float *zoom)
	: zoom_(*zoom)
	{
		SetFlags(e_shapeBit | e_jointBit | e_aabbBit | e_pairBit | e_centerOfMassBit);
	}

	Mesh *new_child(int vertex_count, const b2Color &color, GLenum mode, float width)
	{
		Mesh *mesh = pool_.acquire();
		Q_ASSERT(mesh);
		mesh->geometry_.allocate(vertex_count);
		mesh->geometry_.setDrawingMode(mode);
		mesh->geometry_.setLineWidth(width);
		mesh->material_.setColor(QColor::fromRgbF(color.r, color.g, color.b));
		appendChildNode(mesh);
		return mesh;
	}

	void DrawPolygon(const b2Vec2 *vertexes, int32 vertex_count, const b2Color &color) override
	{
		Q_ASSERT(vertexes);
		Q_ASSERT(vertex_count > 2);
		Mesh *mesh = new_child(vertex_count, color, GL_LINE_LOOP, 1);

		for(int32 i = 0; i < vertex_count; ++i)
		{
			mesh->set(i, vertexes[i]);
		}
	}

	void DrawSolidPolygon(const b2Vec2 *vertexes, int32 vertex_count, const b2Color &color) override
	{
		Q_ASSERT(vertexes);
		Q_ASSERT(vertex_count > 2);
		Mesh *mesh = new_child(vertex_count, color, GL_TRIANGLE_STRIP, 0);
		mesh->set(vertex_count - 1, vertexes[vertex_count / 2]);

		for(int32 i = 0; i < vertex_count / 2; ++i)
		{
			mesh->set(i * 2, vertexes[i]);
			mesh->set(i * 2 + 1, vertexes[vertex_count - i - 1]);
		}
	}

	void DrawCircle(const b2Vec2 &center, float32 radius, const b2Color &color) override
	{
		Mesh *mesh = new_child(12, color, GL_LINE_LOOP, 1);

		for(int32 i = 0; i < 12; ++i)
		{
			mesh->set(i, center + radius * b2Rot(float(i * M_PI / 6)).GetXAxis());
		}
	}

	void DrawSolidCircle(const b2Vec2 &center, float32 radius, const b2Vec2 &, const b2Color &color) override
	{
		Mesh *mesh = new_child(12, color, GL_TRIANGLE_STRIP, 0);

		for(int32 i = 0; i < 6; ++i)
		{
			mesh->set(i * 2, center + radius * b2Rot(float(-i * M_PI / 6)).GetXAxis());
			mesh->set(i * 2 + 1, center + radius * b2Rot(float((i + 1) * M_PI / 6)).GetXAxis());
		}
	}

	void DrawSegment(const b2Vec2 &begin, const b2Vec2 &end, const b2Color &color) override
	{
		Mesh *mesh = new_child(2, color, GL_LINES, 1);
		mesh->set(0, begin);
		mesh->set(1, end);
	}

	void DrawTransform(const b2Transform &transform) override
	{
		Mesh *mesh = new_child(3, b2Color(1, 1, 1), GL_LINE_STRIP, 2);
		mesh->set(0, b2Mul(transform, b2Vec2(48 / zoom_, 0)));
		mesh->set(1, b2Mul(transform, b2Vec2(0, 0)));
		mesh->set(2, b2Mul(transform, b2Vec2(0, 32 / zoom_)));
	}
};

struct DebugView
: QQuickItem
{
	AW_DECLARE_OBJECT_STUB(DebugView)
	AW_DECLARE_PROPERTY_STORED(aw::game::Scene *, scene) = 0;
	AW_DECLARE_PROPERTY_STORED(float, zoom) = 100; ///< px m⁻¹
	AW_DECLARE_PROPERTY_STORED(b2Vec2, center) = b2Vec2_zero; ///< m

	QSGNode *updatePaintNode(QSGNode *old_node, UpdatePaintNodeData *) override
	{
		if(!isVisible())
		{
			return old_node;
		}
		if(!old_node)
		{
			old_node = new Node(&zoom_);
		}

		Q_ASSERT(dynamic_cast<Node *>(old_node));
		Node *node = static_cast<Node *>(old_node);

		while(QSGNode *child = node->firstChild())
		{
			node->pool_.release(child);
		}
		if(scene())
		{
			QMatrix4x4 transform;
			transform.translate(float(width()) / 2, float(height()) / 2);
			transform.scale(zoom(), zoom(), 1);
			transform.translate(-center().x, -center().y);
			node->setMatrix(transform);

			Q_ASSERT(bool(scene()->world_));
			scene()->world_->SetDebugDraw(node);
			scene()->world_->DrawDebugData();
		}

		return node;
	}

protected:

	DebugView()
	{
		setFlags(ItemHasContents);
	}
};

void DebugView::set_scene(game::Scene *const &scene)
{
	if(scene_ == scene) return;
	scene_ = scene;
	Q_EMIT scene_changed(scene_);
	update();
}

void DebugView::set_zoom(float const &zoom)
{
	if(zoom_ == zoom) return;
	zoom_ = zoom;
	Q_EMIT zoom_changed(zoom_);
	update();
}

void DebugView::set_center(b2Vec2 const &center)
{
	if(center_ == center) return;
	center_ = center;
	Q_EMIT center_changed(center_);
	update();
}

AW_DEFINE_OBJECT_STUB(DebugView)

}
}

#endif
