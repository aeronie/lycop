#include <QtQml>

#include "Settings.h"
#include "meta.private.h"

namespace aw {
namespace {

static QString read(QIODevice *input)
{
	QString output;
	for(;;)
	{
		uint32_t buffer = 0;
		char *begin = reinterpret_cast<char *>(&buffer);
		char const *end = begin + 4;
		while(begin < end)
		{
			int64_t count = input->read(begin, end - begin);
			Q_ASSERT(count > 0);
			begin += count;
		}
		for(int i = 0; i < 5; ++i)
		{
			output.append(((buffer >> (i * 6)) & 63) + 32);
		}
		if(buffer >> 31)
		{
			return output.trimmed().toLower();
		}
	}
}

static bool read(QIODevice &input, QSettings::SettingsMap &output)
{
	while(!input.atEnd())
	{
		QString key = read(&input);
		QString value = read(&input);
		output.insert(key, value);
	}
	return true;
}

static void write(QIODevice *output, QString const &input)
{
	Q_ASSERT(!input.isEmpty());
	for(int i = 0; i < input.count(); i += 5)
	{
		uint32_t buffer = 0;
		char const *begin = reinterpret_cast<char const *>(&buffer);
		char const *end = begin + 4;
		for(int j = 0; j < 5 && i + j < input.count(); ++j)
		{
			buffer |= ((input[i + j].toUpper().cell() - 32) & 63) << (j * 6);
		}
		if(i + 5 >= input.count())
		{
			buffer |= 1 << 31;
		}
		while(begin < end)
		{
			int64_t count = output->write(begin, end - begin);
			Q_ASSERT(count > 0);
			begin += count;
		}
	}
}

static bool write(QIODevice &output, QSettings::SettingsMap const &input)
{
	QString value;
	for(QSettings::SettingsMap::const_iterator p = input.begin(); p != input.end(); ++p)
	{
		if(p.key().contains("/*/")) continue;
		if(p.value().type() == QVariant::Bool) value = p.value().toBool() ? "1" : "";
		else value = p.value().toString().trimmed();
		if(value.isEmpty()) value = " ";
		write(&output, p.key());
		write(&output, value);
	}
	return true;
}

static const QSettings::Format format_ = QSettings::registerFormat("bin", read, write); // j'aimerais préciser Qt::CaseInsensitive, mais ça déclenche ASSERT: "caseSensitivity == Qt::CaseSensitive" in file src/qtbase/src/corelib/io/qsettings.cpp, line 3459

struct Settings
: QSettings
{
	AW_DECLARE_OBJECT_STUB(Settings)
	AW_DECLARE_PROPERTY_READONLY(aw::Settings *, self)

	/*

	QSettings se synchronise trop fréquemment avec le fichier, et ça fait gratter le disque dur.
	À défaut de pouvoir le faire à une fréquence plus raisonnable, on désactive carrément ce comportement.
	Le fichier est quand même lu/écrit dans le constructeur/destructeur.

	*/
	bool event(QEvent *event) override
	{
		if(event->type() == QEvent::UpdateRequest) return QObject::event(event);
		return QSettings::event(event);
	}

public:

	Settings()
	: QSettings("settings.bin", format_)
	{
		Q_ASSERT(format_ != InvalidFormat);
	}

protected:

	Q_SLOT static QStringList children_of(QString const &key)
	{
		Settings s;
		s.beginGroup(key);
		QStringList children = s.childGroups();
		auto function = [&] (QString const &child)
		{
			return child == "*" || ( key.count('/') < 1 && children_of(key + '/' + child).isEmpty() );
		};
		children.erase(std::remove_if(children.begin(), children.end(), function), children.end());
		sort(children.begin(), children.end(), std::greater<QString>());
		return children;
	}

	Q_SLOT QVariant get(QString const &key, QVariant const &value = {}) const
	{
		return key.isEmpty() ? value : QSettings::value(key, value);
	}

	Q_SLOT void set(QString const &key, QVariant const &value)
	{
		if(value == QSettings::value(key)) return;
		QSettings::setValue(key, value);
		Q_EMIT self_changed(this);
	}

	Q_SLOT void remove(QString const &key)
	{
		QSettings::remove(key);
		Q_EMIT self_changed(this);
	}

	Q_SLOT void load(QString const &source)
	{
		while(!group().isEmpty())
		{
			endGroup();
		}
		if(source.isEmpty())
		{
			beginGroup("games/" + QDateTime::currentDateTimeUtc().toString(Qt::ISODate) + "/*");
		}
		else
		{
			beginGroup(source.section('/', 0, -2) + "/*");
			remove("");
			Settings s;
			s.beginGroup(source);
			for(QString const &key: s.allKeys()) set(key, s.get(key));
		}

		Q_EMIT self_changed(this);
	}

	Q_SLOT void save()
	{
		Settings s;
		s.beginGroup(group().section('/', 0, -2) + '/' + QDateTime::currentDateTimeUtc().toString(Qt::ISODate));
		for(QString const &key: allKeys()) s.set(key, get(key));
		s.sync(); // avant que le jeu plante
		self_changed(this);
	}

	Q_SLOT static QString get_last_saved_game()
	{
		QString best_key;
		QDateTime best_date;
		Settings s;
		s.beginGroup("games");
		for(QString const &x: s.childGroups())
		{
			s.beginGroup(x);
			for(QString const &x: s.childGroups())
			{
				QDateTime date = QDateTime::fromString(x, Qt::ISODate);
				if(date > best_date)
				{
					best_date = date;
					best_key = s.group() + "/" + x;
				}
			}
			s.endGroup();
		}
		return best_key;
	}
};

Settings *Settings::self() const
{
	return const_cast<Settings *>(this);
}

AW_DEFINE_OBJECT_STUB(Settings)

}

QSettings &settings()
{
	static Settings s;
	return s;
}

}
