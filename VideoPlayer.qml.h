#include <QtQuick>
#include <fstream>
#include <theora/theoradec.h>
#include <vorbis/codec.h>

#include "CheatPtr.h"
#include "meta.private.h"
#include "libs/alc.h"
#include "libs/al.h"

#define AW_CHECK_NULL(_expression) AW_CHECK_(::aw::check_null, _expression)
#define AW_CHECK_NOT_NULL(_expression) AW_CHECK_(::aw::check_not_null, _expression)

namespace aw {
namespace {

template<class T>
T check_null(T &&x, void (*throw_)(char const *))
{
	if(x) throw_(0);
	return x;
}

template<class T>
T check_not_null(T &&x, void (*throw_)(char const *))
{
	if(!x) throw_(0);
	return x;
}

class OggStream
{
	struct Stream: ogg_stream_state
	{
		explicit Stream(int id) { AW_CHECK_NULL(ogg_stream_init(this, id)); }
		Stream(Stream &&other): ogg_stream_state(other) { new(&other) ogg_stream_state(); }
		~Stream() { AW_CHECK_NULL(ogg_stream_clear(this)); }
	};

	struct Packet: ogg_packet
	{
		Packet(): ogg_packet() {}
		Packet(Packet &&other): ogg_packet(other) { new(&other) ogg_packet(); }
		~Packet() { /*ogg_packet_clear(this);*/ }
	};

public:
	Stream stream_;
	Packet packet_;

	explicit OggStream(int id)
	: stream_(id)
	{
	}

	bool read()
	{
		switch(ogg_stream_packetout(&stream_, &packet_))
		{
			case 0: return false;
			case 1: return true;
			default: abort();
		}
	}
};

class VorbisStream
: public OggStream
{
	struct Info: vorbis_info
	{
		Info() { vorbis_info_init(this); }
		Info(Info &&other): vorbis_info(other) { new(&other) vorbis_info(); }
		~Info() { vorbis_info_clear(this); }
	};

	struct Comment: vorbis_comment
	{
		Comment() { vorbis_comment_init(this); }
		Comment(Comment &&other): vorbis_comment(other) { new(&other) vorbis_comment(); }
		~Comment() { vorbis_comment_clear(this); }
	};

	struct Decoder: vorbis_dsp_state
	{
		Decoder(): vorbis_dsp_state() {}
		Decoder(Decoder &&other): vorbis_dsp_state(other) { new(&other) vorbis_dsp_state(); }
		~Decoder() { vorbis_dsp_clear(this); }
	};

	struct Block: vorbis_block
	{
		Block(): vorbis_block() {}
		Block(Block &&other): vorbis_block(other) { new(&other) vorbis_block(); }
		~Block() { vorbis_block_clear(this); }
	};

	Comment comment_;
	Decoder decoder_;
	Block block_;
	int expected_header_count_ = 2;
public:
	Info info_;
	int buffer_size_;
	float **buffer_;

	static VorbisStream create(OggStream *stream)
	{
		if(!vorbis_synthesis_idheader(&stream->packet_)) throw stream;
		return {std::move(*stream)};
	}

	bool read()
	{
		while(OggStream::read())
		{
			if(expected_header_count_)
			{
				AW_CHECK_NULL(vorbis_synthesis_headerin(&info_, &comment_, &packet_));
				if(--expected_header_count_) continue;
				AW_CHECK_NULL(vorbis_synthesis_init(&decoder_, &info_));
				AW_CHECK_NULL(vorbis_block_init(&decoder_, &block_));
			}
			else return true;
		}
		return false;
	}

	void decode()
	{
		AW_CHECK_NULL(vorbis_synthesis(&block_, &packet_));
		AW_CHECK_NULL(vorbis_synthesis_blockin(&decoder_, &block_));
		buffer_size_ = vorbis_synthesis_pcmout(&decoder_, &buffer_);
		AW_CHECK_NULL(vorbis_synthesis_read(&decoder_, buffer_size_));
	}

private:

	VorbisStream(OggStream &&stream)
	: OggStream(std::move(stream))
	{
		AW_CHECK_NULL(vorbis_synthesis_headerin(&info_, &comment_, &packet_));
	}
};

class TheoraStream
: public OggStream
{
	struct Info: th_info
	{
		Info() { th_info_init(this); }
		Info(Info &&other): th_info(other) { new(&other) th_info(); }
		~Info() { th_info_clear(this); }
	};

	struct Comment: th_comment
	{
		Comment() { th_comment_init(this); }
		Comment(Comment &&other): th_comment(other) { new(&other) th_comment(); }
		~Comment() { th_comment_clear(this); }
	};

	struct Deleter
	{
		void operator()(th_setup_info *p) const { th_setup_free(p); }
		void operator()(th_dec_ctx *p) const { th_decode_free(p); }
	};

	using Setup = std::unique_ptr<th_setup_info, Deleter>;
	using Decoder = std::unique_ptr<th_dec_ctx, Deleter>;
	Comment comment_;
	Setup setup_;
	Decoder decoder_;
public:
	Info info_;
	th_ycbcr_buffer buffer_;

	static TheoraStream create(OggStream *stream)
	{
		Info info;
		Comment comment;
		Setup setup;
		if(th_decode_headerin(&info, &comment, cheat(&setup), &stream->packet_) < 0) throw stream;
		return {std::move(*stream), std::move(info), std::move(comment), std::move(setup)};
	}

	bool read()
	{
		while(OggStream::read())
		{
			if(!decoder_)
			{
				int status = th_decode_headerin(&info_, &comment_, cheat(&setup_), &packet_);
				if(status < 0) abort();
				if(status) continue;
				decoder_.reset(AW_CHECK_NOT_NULL(th_decode_alloc(&info_, setup_.get())));
				setup_.reset();
			}
			return true;
		}
		return false;
	}

	void decode()
	{
		AW_CHECK_NULL(th_decode_packetin(decoder_.get(), &packet_, 0));
		AW_CHECK_NULL(th_decode_ycbcr_out(decoder_.get(), buffer_));
	}

private:

	TheoraStream(OggStream &&stream, Info &&info, Comment &&comment, Setup &&setup)
	: OggStream(std::move(stream))
	, comment_(std::move(comment))
	, setup_(std::move(setup))
	, info_(std::move(info))
	{
	}
};

class OggFile
{
	struct Sync: ogg_sync_state
	{
		Sync() { AW_CHECK_NULL(ogg_sync_init(this)); }
		Sync(Sync &&other): ogg_sync_state(other) { new(&other) ogg_sync_state(); }
		~Sync() { ogg_sync_clear(this); }
	};

	template<class _list_type>
	static bool push(_list_type *list, ogg_page *page)
	{
		for(auto &x: *list) if(x.stream_.serialno == ogg_page_serialno(page))
		{
			AW_CHECK_NULL(ogg_stream_pagein(&x.stream_, page));
			return true;
		}
		return false;
	}

	QFile file_;
	Sync sync_;
public:
	std::vector<TheoraStream> theora_;
	std::vector<VorbisStream> vorbis_;

	OggFile(QString const &file)
	: file_(file)
	{
		file_.open(QIODevice::ReadOnly);
		Q_ASSERT(file_.isOpen());
		read();
	}

	void read()
	{
		ogg_page page;

		for(;;)
		{
			for(;;)
			{
				switch(ogg_sync_pageout(&sync_, &page))
				{
					case 0: goto pageout_failure;
					case 1: goto pageout_success;
					default: abort();
				}

				pageout_failure:
				AW_CHECK_NULL(ogg_sync_wrote(&sync_, (long int)(file_.read(AW_CHECK_NOT_NULL(ogg_sync_buffer(&sync_, 4096)), 4096))));
				Q_ASSERT(file_.error() == QFileDevice::NoError);
			}

			pageout_success:
			if(ogg_page_bos(&page))
			{
				OggStream stream {ogg_page_serialno(&page)};
				AW_CHECK_NULL(ogg_stream_pagein(&stream.stream_, &page));
				AW_CHECK_NOT_NULL(stream.read());
				using Function = void (OggFile *, OggStream *);
				static const Function *functions[]
				{
					[] (OggFile *file, OggStream *stream) { file->theora_.push_back(TheoraStream::create(stream)); },
					[] (OggFile *file, OggStream *stream) { file->vorbis_.push_back(VorbisStream::create(stream)); },
					[] (OggFile *, OggStream *) { abort(); }
				};
				for(Function *function: functions)
				{
					try
					{
						function(this, &stream);
						break;
					}
					catch(OggStream *)
					{
					}
				}
			}
			else
			{
				push(&theora_, &page) || push(&vorbis_, &page) || [] () -> bool { abort(); } ();
				return;
			}
		}
	}

	bool empty() const
	{
		return file_.atEnd();
	}
};

class ALSource
{
	static ALenum format(int channel_count, int sample_size)
	{
		if(channel_count == 1 && sample_size == 1) return AL_FORMAT_MONO8;
		if(channel_count == 1 && sample_size == 2) return AL_FORMAT_MONO16;
		if(channel_count == 2 && sample_size == 1) return AL_FORMAT_STEREO8;
		if(channel_count == 2 && sample_size == 2) return AL_FORMAT_STEREO16;
		abort();
	}

	class Buffer_
	{
		friend struct ALSource;
		ALuint id_ = 0;
		Buffer_() { AW_CHECK_AL(alGenBuffers(1, &id_)); }
		~Buffer_() { AW_CHECK_AL(alDeleteBuffers(1, &id_)); }
		Buffer_(Buffer_ &&other): id_(other.id_) { other.id_ = 0; }
	};

	struct Buffer: Buffer_
	{
		Buffer(int rate, int sample_count, int channel_count, int sample_size, void const *buffer) { AW_CHECK_AL(alBufferData(id_, format(channel_count, sample_size), buffer, sample_count * channel_count * sample_size, rate)); }
	};

	ALuint id_ = 0;
	int offset_ = 0;
public:
	ALSource() { AW_CHECK_AL(alGenSources(1, &id_)); }
	~ALSource() { AW_CHECK_AL(alDeleteSources(1, &id_)); }
	ALSource(ALSource &&other): id_(other.id_) { other.id_ = 0; }

	bool playing() const
	{
		int x;
		AW_CHECK_AL(alGetSourcei(id_, AL_SOURCE_STATE, &x));
		return x == AL_PLAYING;
	}

	void playing(bool x)
	{
		AW_CHECK_AL(alSourcePause(id_));
		if(x) AW_CHECK_AL(alSourcePlay(id_));
	}

	void push(Buffer &&buffer)
	{
		AW_CHECK_AL(alSourceQueueBuffers(id_, 1, &buffer.id_));
		buffer.id_ = 0;
	}

	int pop()
	{
		int count, offset;
		AW_CHECK_AL(alGetSourcei(id_, AL_BUFFERS_PROCESSED, &count));
		if(count)
		{
			ALuint buffers[count];
			AW_CHECK_AL(alSourceUnqueueBuffers(id_, count, buffers));
			for(ALuint buffer: buffers)
			{
				int size, bits, channels;
				AW_CHECK_AL(alGetBufferi(buffer, AL_SIZE, &size));
				AW_CHECK_AL(alGetBufferi(buffer, AL_BITS, &bits));
				AW_CHECK_AL(alGetBufferi(buffer, AL_CHANNELS, &channels));
				offset_ += size * 8 / bits / channels;
			}
			AW_CHECK_AL(alDeleteBuffers(count, buffers));
		}
		AW_CHECK_AL(alGetSourcei(id_, AL_SAMPLE_OFFSET, &offset));
		return offset_ + offset;
	}

	void gain(float x)
	{
		AW_CHECK_AL(alSourcef(id_, AL_GAIN, x));
	}

	int size() const
	{
		int count;
		AW_CHECK_AL(alGetSourcei(id_, AL_BUFFERS_QUEUED, &count));
		return count;
	}

	bool empty() const
	{
		return !size();
	}
};

struct GLFunctions: QOpenGLFunctions
{
	GLFunctions() { initializeOpenGLFunctions(); }
};

struct Attributes
{
	float vertex_in_scene_[2];
	float vertex_in_texture_[2];

	static const QSGGeometry::AttributeSet &definition()
	{
		static const QSGGeometry::Attribute definitions[]
		{
			QSGGeometry::Attribute::create(0, 2, GL_FLOAT, true),
			QSGGeometry::Attribute::create(1, 2, GL_FLOAT),
		};

		static const QSGGeometry::AttributeSet definition {2, sizeof(Attributes), definitions};
		return definition;
	}
};

struct Texture_
: GLFunctions
{
	Texture_() { glGenTextures(1, &id_); }
	~Texture_() { glDeleteTextures(1, &id_); }
	Texture_(Texture_ &&other): id_(other.id_) { other.id_ = 0; }
	GLuint id_;
};

struct Texture
: Texture_
{
	Texture(GLsizei row_count, GLsizei column_count)
	{
		glBindTexture(GL_TEXTURE_2D, id_);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, column_count, row_count, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, 0);
	}

	void load(GLsizei row_count, GLsizei column_count, const void *data)
	{
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, column_count, row_count, GL_LUMINANCE, GL_UNSIGNED_BYTE, data);
	}
};

struct Uniforms
{
	std::unique_ptr<Texture> textures_[3];
};

struct Shader
: QSGSimpleMaterialShader<Uniforms>
, GLFunctions
{
	QSG_DECLARE_SIMPLE_SHADER(Shader, Uniforms)
	int textures_[3];

	Shader()
	{
		setShaderSourceFile(QOpenGLShader::Vertex, ":/VideoPlayer.vsh");
		setShaderSourceFile(QOpenGLShader::Fragment, ":/VideoPlayer.fsh");
	}

	QList<QByteArray> attributes() const override
	{
		return {"vertex_in_scene", "vertex_in_texture"};
	}

	void resolveUniforms() override
	{
		textures_[0] = program()->uniformLocation("texture_Y");
		textures_[1] = program()->uniformLocation("texture_Cb");
		textures_[2] = program()->uniformLocation("texture_Cr");
	}

	void updateState(const Uniforms *uniforms, const Uniforms *) override
	{
		int active_texture;
		glGetIntegerv(GL_ACTIVE_TEXTURE, &active_texture);

		for(int i = 0; i < 3; ++i)
		{
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, uniforms->textures_[i]->id_);
			program()->setUniformValue(textures_[i], i);
		}

		glActiveTexture(active_texture);
	}
};

struct Node
: QSGGeometryNode
, GLFunctions
{
	QSGGeometry geometry_ {Attributes::definition(), 4};
	std::unique_ptr<QSGSimpleMaterial<Uniforms>> material_ {Shader::createMaterial()};
	float aspect_ = 0;

	Node()
	{
		setFlags(OwnedByParent);
		setGeometry(&geometry_);
		setMaterial(material_.get());
	}

	void update(TheoraStream const &video, float width, float height)
	{
		static const float vertices[][2] {{0, 1}, {1, 1}, {0, 0}, {1, 0}};
		auto *attributes = static_cast<Attributes *>(geometry_.vertexData());
		auto *textures = material_->state()->textures_;
		int active_texture;
		glGetIntegerv(GL_ACTIVE_TEXTURE, &active_texture);

		if(!aspect_)
		{
			aspect_ = (video.info_.aspect_numerator && video.info_.aspect_denominator ? float(video.info_.aspect_numerator) / float(video.info_.aspect_denominator) : 1) * float(video.info_.pic_width) / float(video.info_.pic_height);
			for(int i = 0; i < 3; ++i)
			{
				textures[i].reset(new Texture(video.buffer_[i].height, video.buffer_[i].stride));
			}
			for(int i = 0; i < 4; ++i)
			{
				attributes[i].vertex_in_texture_[0] = (vertices[i][0] * float(video.info_.pic_width) + float(video.info_.pic_x)) / float(video.buffer_[0].stride);
				attributes[i].vertex_in_texture_[1] = (vertices[i][1] * float(video.info_.pic_height) + float(video.info_.pic_y)) / float(video.buffer_[0].height);
			}
		}
		if(width / height > aspect_) for(int i = 0; i < 4; ++i)
		{
			attributes[i].vertex_in_scene_[0] = vertices[i][0] * height * aspect_ + (width - height * aspect_) / 2;
			attributes[i].vertex_in_scene_[1] = vertices[i][1] * height;
		}
		else for(int i = 0; i < 4; ++i)
		{
			attributes[i].vertex_in_scene_[0] = vertices[i][0] * width;
			attributes[i].vertex_in_scene_[1] = vertices[i][1] * width / aspect_ + (height - width / aspect_) / 2;
		}
		for(int i = 0; i < 3; ++i)
		{
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, textures[i]->id_);
			textures[i]->load(video.buffer_[i].height, video.buffer_[i].stride, video.buffer_[i].data);
		}

		glActiveTexture(active_texture);
	}
};

struct Data
{
	OggFile file;
	TheoraStream &video;
	VorbisStream &audio;
	ALSource source;
	bool has_video_packet = false;

	Data(QString const &file)
	: file(file)
	, video(this->file.theora_.at(0))
	, audio(this->file.vorbis_.at(0))
	{
	}
};

struct VideoPlayer
: QQuickItem
{
	AW_DECLARE_OBJECT_STUB(VideoPlayer)
	AW_DECLARE_PROPERTY_STORED(QString, file);
	AW_DECLARE_PROPERTY_STORED(bool, playing) = false;
	AW_DECLARE_PROPERTY_STORED(float, gain) = 1;
	std::unique_ptr<Data> this_;
	std::vector<int16_t> audio_buffer_;

	QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *) override
	{
		if(this_ && this_->has_video_packet)
		{
			if(!node) node = new Node;
			Q_ASSERT(dynamic_cast<Node *>(node));
			static_cast<Node *>(node)->update(this_->video, float(width()), float(height()));
			this_->has_video_packet = false;
		}
		if(!this_ && node)
		{
			delete node;
			node = 0;
		}

		return node;
	}

	void set_file(QString const &file, UseAL const &)
	{
		file_ = file;
		if(file.isEmpty()) this_.reset();
		else this_.reset(new Data(file));
		QQuickItem::update();
		Q_EMIT file_changed(file);
	}

protected:

	VideoPlayer()
	{
		setFlags(ItemHasContents);
	}

	~VideoPlayer()
	{
		UseAL use_al;
		this_.reset();
	}

	Q_SLOT void update()
	{
		if(!this_) return;
		UseAL use_al;
		this_->source.gain(gain_);
		int now = this_->source.pop();
		while(this_->video.packet_.packetno * this_->audio.info_.rate * this_->video.info_.fps_denominator < now * this_->video.info_.fps_numerator && this_->video.read())
		{
			this_->video.decode();
			this_->has_video_packet = true;
			QQuickItem::update();
		}
		while(this_->source.size() < 50 && !this_->file.empty())
		{
			this_->file.read();
			while(this_->audio.read())
			{
				this_->audio.decode();
				audio_buffer_.resize(this_->audio.buffer_size_ * this_->audio.info_.channels);
				for(int i = 0; i < this_->audio.info_.channels; ++i) for(int j = 0; j < this_->audio.buffer_size_; ++j) audio_buffer_[i + j * this_->audio.info_.channels] = int16_t(this_->audio.buffer_[i][j] * 32767);
				this_->source.push({int(this_->audio.info_.rate), this_->audio.buffer_size_, this_->audio.info_.channels, sizeof(int16_t), audio_buffer_.data()});
			}
		}
		if(this_->file.empty() && this_->source.empty())
		{
			set_file(QString(), use_al);
			return;
		}
		for(auto &stream: this_->file.theora_) if(&stream != &this_->video) while(stream.read());
		for(auto &stream: this_->file.vorbis_) if(&stream != &this_->audio) while(stream.read());
		this_->source.playing(playing_);
	}
};

void VideoPlayer::set_file(QString const &file)
{
	if(file_ == file) return;
	set_file(file, UseAL());
}

AW_DEFINE_OBJECT_STUB(VideoPlayer)
AW_DEFINE_PROPERTY_STORED(VideoPlayer, playing)
AW_DEFINE_PROPERTY_STORED(VideoPlayer, gain)

}
}
