attribute vec3 vertex_in_scene;
attribute vec2 vertex_in_color_map;
attribute vec2 vertex_in_normal_map;
attribute vec2 direction;
attribute vec4 mask;
uniform mat4 qt_Matrix;
varying vec2 fragment_in_color_map_;
varying vec2 fragment_in_normal_map_;
varying vec2 direction_;
varying vec4 mask_;

void main()
{
	fragment_in_color_map_ = vertex_in_color_map;
	fragment_in_normal_map_ = vertex_in_normal_map;
	direction_ = direction;
	mask_ = mask;
	mask_.rgb *= mask.a;
	gl_Position = qt_Matrix * vec4(vertex_in_scene.x, vertex_in_scene.y, -0.5, 1);
}
