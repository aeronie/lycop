#include <cmath>

#include "ControlInfo.h"
#include "meta.private.h"

namespace aw {
namespace {

struct Info
: QObject
{
	Q_OBJECT
public:
	static QList<ControlInfo> const &info()
	{
		static const QList<ControlInfo> info
		{
			{tr("up_left_down_right"), "up_left_down_right", 2},
			{tr("up_right_down_left"), "up_right_down_left", 2},
			{tr("down_left_up_right"), "down_left_up_right", 2},
			{tr("down_right_up_left"), "down_right_up_left", 2},
			{tr("up"), "up", 0},
			{tr("down"), "down", 0},
			{tr("left"), "left", 0},
			{tr("right"), "right", 0},
			{tr("up_down"), "up_down", 1},
			{tr("down_up"), "down_up", 1},
			{tr("left_right"), "left_right", 1},
			{tr("right_left"), "right_left", 1},
		};
		return info;
	}
};

struct DirectionControl
: QObject
{
	AW_DECLARE_OBJECT_STUB(DirectionControl)
	AW_DECLARE_PROPERTY_STORED(QString, text);
	AW_DECLARE_PROPERTY_READONLY(QPointF, value)
	qreal left_ = 0, right_ = 0, up_ = 0, down_ = 0;

public Q_SLOTS:

	QObject *info() const
	{
		return ControlInfo::model(Info::info(), objectName());
	}

	void up_left_down_right(QPointF xy)
	{
		left_ = 0;
		right_ = xy.x();
		up_ = 0;
		down_ = xy.y();
		Q_EMIT value_changed(value());
	}

	void up_right_down_left(QPointF xy)
	{
		up_left_down_right(QPointF(-xy.x(), xy.y()));
	}

	void down_left_up_right(QPointF xy)
	{
		up_right_down_left(-xy);
	}

	void down_right_up_left(QPointF xy)
	{
		up_left_down_right(-xy);
	}

	void left_right(qreal x)
	{
		left_ = 0;
		right_ = x;
		Q_EMIT value_changed(value());
	}

	void up_down(qreal y)
	{
		up_ = 0;
		down_ = y;
		Q_EMIT value_changed(value());
	}

	void right_left(qreal x)
	{
		left_right(-x);
	}

	void down_up(qreal y)
	{
		up_down(-y);
	}

	void left(bool x)
	{
		left_ = x;
		Q_EMIT value_changed(value());
	}

	void right(bool x)
	{
		right_ = x;
		Q_EMIT value_changed(value());
	}

	void up(bool y)
	{
		up_ = y;
		Q_EMIT value_changed(value());
	}

	void down(bool y)
	{
		down_ = y;
		Q_EMIT value_changed(value());
	}
};

QPointF DirectionControl::value() const
{
	const QPointF v(right_ - left_, down_ - up_);
	const qreal length = v.x() * v.x() + v.y() * v.y();
	return length > 1 ? v / std::sqrt(length) : v;
}

AW_DEFINE_OBJECT_STUB(DirectionControl)
AW_DEFINE_PROPERTY_STORED(DirectionControl, text)

}
}
