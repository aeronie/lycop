#include "ControlInfo.h"
#include "Settings.h"

namespace aw {
namespace {

struct Info
{
	QString text_;
	QString name_;
	int type_;
	QString key_;
};

struct ControlInfoModel
: QAbstractListModel
{
	Q_OBJECT
	QList<Info> data_;

	static QHash<int, QByteArray> const &roles()
	{
		static const QHash<int, QByteArray> roles
		{
			{Qt::DisplayRole, "text"},
			{Qt::UserRole + 0, "name"},
			{Qt::UserRole + 1, "type"},
			{Qt::UserRole + 2, "key"},
		};
		return roles;
	}

	QHash<int, QByteArray> roleNames() const override
	{
		return roles();
	}

	int rowCount(QModelIndex const &index) const override
	{
		return index.isValid() ? 0 : data_.count();
	}

	QVariant data(QModelIndex const &index, int role) const override
	{
		switch(role)
		{
			case Qt::DisplayRole: return data_.at(index.row()).text_;
			case Qt::UserRole + 0: return data_.at(index.row()).name_;
			case Qt::UserRole + 1: return data_.at(index.row()).type_;
			case Qt::UserRole + 2: return data_.at(index.row()).key_;
			default: abort();
		}
	}

public:

	ControlInfoModel(QList<ControlInfo> const &info, QString const &name)
	{
		QSettings &s = settings();
		s.beginGroup("controls");
		s.beginGroup(name);

		for(auto &o: info)
		{
			s.beginGroup(o.name_);
			QStringList keys = s.childKeys();
			QString key;

			for(QString const &key: keys)
			{
				data_.append({o.text_, o.name_, o.type_, s.group() + "/" + key});
			}
			for(int i = 0;; ++i)
			{
				key.setNum(i);
				if(!keys.contains(key)) break;
			}

			data_.append({o.text_, o.name_, o.type_, s.group() + "/" + key});
			s.endGroup();
		}

		s.endGroup();
		s.endGroup();
	}

	Q_SLOT int count() const
	{
		return data_.count();
	}

	Q_SLOT QVariant get(int row, QByteArray const &role) const
	{
		for(auto p = roles().begin(), end = roles().end(); p != end; ++p)
		{
			if(p.value() == role)
			{
				return data(index(row, 0), p.key());
			}
		}
		abort();
	}
};

}

QObject *ControlInfo::model(QList<ControlInfo> const &info, QString const &name)
{
	return new ControlInfoModel {info, name};
}

}
