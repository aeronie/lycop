#include <QtQml>

#include "meta.private.h"

namespace aw {
namespace statemachine {
namespace {

struct AssignProperty
: QObject
, QQmlParserStatus
{
	AW_DECLARE_OBJECT_STUB(AssignProperty)
	Q_INTERFACES(QQmlParserStatus)
	AW_DECLARE_PROPERTY_STORED(QObject *, target) = 0;
	AW_DECLARE_PROPERTY_STORED(QString, property);
	AW_DECLARE_PROPERTY_STORED(QVariant, value);

	void classBegin() override
	{
	}

	void componentComplete() override
	{
		Q_ASSERT(dynamic_cast<QState *>(parent()));
		Q_ASSERT(target_);
		int type = target_->property(property_.toUtf8()).userType();
		if(type != QVariant::Invalid)
		{
			Q_ASSERT(value_.canConvert(type));
			value_.convert(type);
		}
		static_cast<QState *>(parent())->assignProperty(target_, property_.toUtf8(), value_);
		deleteLater();
	}
};

AW_DEFINE_OBJECT_STUB(AssignProperty)
AW_DEFINE_PROPERTY_STORED(AssignProperty, target)
AW_DEFINE_PROPERTY_STORED(AssignProperty, property)
AW_DEFINE_PROPERTY_STORED(AssignProperty, value)

}
}
}
