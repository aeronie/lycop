#include <QtCore>

#include "meta.private.h"

namespace aw {
namespace statemachine {
namespace {

struct PropertyAnimation
: QPropertyAnimation
{
	AW_DECLARE_OBJECT_STUB(PropertyAnimation)
};

AW_DEFINE_OBJECT_STUB(PropertyAnimation)

}
}
}
