import QtQml.StateMachine 1.0
import aw.statemachine 0.0

StateMachine
{
	id: component
	initialState: active ? _begin : _pause
	running: true
	globalRestorePolicy: StateMachine.RestoreProperties
	default property alias children: _begin.children
	property alias begin: _begin.initialState
	property alias end: _end
	property bool active: true
	signal quit()

	State
	{
		id: _begin
		HistoryState
		{
			id: _history
			defaultState: _begin.initialState
			historyType: HistoryState.DeepHistory
		}
		SignalTransition {targetState: _end; signal: quit}
		SignalTransition {targetState: _pause; signal: component.activeChanged; guard: !component.active}
	}
	State
	{
		id: _pause
		SignalTransition {targetState: _end; signal: quit}
		SignalTransition {targetState: _history; signal: component.activeChanged; guard: component.active}
	}
	FinalState
	{
		id: _end
	}
}
