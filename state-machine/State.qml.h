#include <QtCore>

#include "meta.private.h"

namespace aw {
namespace statemachine {
namespace {

static int qml_children_count(QQmlListProperty<QObject> *)
{
	Q_UNREACHABLE();
}

static QObject *qml_children_at(QQmlListProperty<QObject> *, int)
{
	Q_UNREACHABLE();
}

static void qml_children_append(QQmlListProperty<QObject> *list, QObject *object)
{
	if(QAbstractTransition *transition = dynamic_cast<QAbstractTransition *>(object))
	{
		static_cast<QState *>(list->object)->addTransition(transition);
	}
	else
	{
		object->setParent(list->object);
	}
}

struct State
: QState
{
	AW_DECLARE_OBJECT_STUB(State)
	Q_PROPERTY(QQmlListProperty<QObject> children READ qml_children)
	Q_CLASSINFO("DefaultProperty", "children")

	QQmlListProperty<QObject> qml_children()
	{
		return QQmlListProperty<QObject>(this, 0, qml_children_append, qml_children_count, qml_children_at, 0);
	}
};

AW_DEFINE_OBJECT_STUB(State)

}
}
}
