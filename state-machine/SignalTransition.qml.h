#include <QtQml>

#include <private/qv4qobjectwrapper_p.h>
#include <private/qv8engine_p.h>
#include <private/qjsvalue_p.h>
#include <private/qv4scopedvalue_p.h>
#include <private/qstate_p.h>
#include <private/qstatemachine_p.h>

#include "meta.private.h"

namespace aw {
namespace statemachine {
namespace base { // copié depuis qtdeclarative/src/imports/statemachine/signaltransition.cpp

class SignalTransition : public QSignalTransition
{
	Q_OBJECT
	Q_PROPERTY(QJSValue signal READ signal WRITE setSignal NOTIFY qmlSignalChanged)
	Q_PROPERTY(QQmlScriptString guard READ guard WRITE setGuard NOTIFY guardChanged)
public:
	explicit SignalTransition(QState *parent = Q_NULLPTR);
	QQmlScriptString guard() const;
	void setGuard(const QQmlScriptString &guard);
	bool eventTest(QEvent *event);
	const QJSValue &signal();
	void setSignal(const QJSValue &signal);
	Q_INVOKABLE void invoke();
	Q_SIGNALS:
	void guardChanged();
	void invokeYourself();
	void qmlSignalChanged();
private:
	QByteArray m_data;
	QJSValue m_signal;
	QQmlScriptString m_guard;
};

SignalTransition::SignalTransition(QState *parent)
: QSignalTransition(this, SIGNAL(invokeYourself()), parent)
{
	connect(this, SIGNAL(signalChanged()), SIGNAL(qmlSignalChanged()));
}

bool SignalTransition::eventTest(QEvent *event)
{
	Q_ASSERT(event);
	if (!QSignalTransition::eventTest(event))
		return false;
	if (m_guard.isEmpty())
		return true;
	QQmlContext context(QQmlEngine::contextForObject(this));
	QStateMachine::SignalEvent *e = static_cast<QStateMachine::SignalEvent*>(event);
	int count = e->arguments().count();
	QMetaMethod metaMethod = e->sender()->metaObject()->method(e->signalIndex());
	for (int i = 0; i < count; i++)
		context.setContextProperty(metaMethod.parameterNames()[i], QVariant::fromValue(e->arguments().at(i)));
	QQmlExpression expr(m_guard, &context, this);
	QVariant result = expr.evaluate();
	return result.toBool();
}

const QJSValue& SignalTransition::signal()
{
	return m_signal;
}

void SignalTransition::setSignal(const QJSValue &signal)
{
	if (m_signal.strictlyEquals(signal))
		return;
	m_signal = signal;
	QV4::ExecutionEngine *jsEngine = QV8Engine::getV4(QQmlEngine::contextForObject(this)->engine());
	QV4::Scope scope(jsEngine);
	QV4::Scoped<QV4::QObjectMethod> qobjectSignal(scope, QJSValuePrivate::get(m_signal)->getValue(jsEngine));
	Q_ASSERT(qobjectSignal);
	QObject *sender = qobjectSignal->object();
	Q_ASSERT(sender);
	QMetaMethod metaMethod = sender->metaObject()->method(qobjectSignal->methodIndex());
	QSignalTransition::setSenderObject(sender);
	QSignalTransition::setSignal(metaMethod.methodSignature());
}

QQmlScriptString SignalTransition::guard() const
{
	return m_guard;
}

void SignalTransition::setGuard(const QQmlScriptString &guard)
{
	if (m_guard == guard)
		return;
	m_guard = guard;
	Q_EMIT guardChanged();
}

void SignalTransition::invoke()
{
	Q_EMIT invokeYourself();
}

}

namespace {

static void children_append(QQmlListProperty<QObject> *list, QObject *object)
{
	Q_ASSERT(dynamic_cast<QAbstractTransition *>(list->object));
	Q_ASSERT(dynamic_cast<QAbstractAnimation *>(object));
	static_cast<QAbstractTransition *>(list->object)->addAnimation(static_cast<QAbstractAnimation *>(object));
	object->setParent(list->object);
}

static int children_count(QQmlListProperty<QObject> *list)
{
	Q_ASSERT(dynamic_cast<QAbstractTransition *>(list->object));
	return static_cast<QAbstractTransition *>(list->object)->animations().count();
}

static QObject *children_at(QQmlListProperty<QObject> *list, int index)
{
	Q_ASSERT(dynamic_cast<QAbstractTransition *>(list->object));
	return static_cast<QAbstractTransition *>(list->object)->animations().at(index);
}

template<class _transition_type>
struct Transition
: _transition_type
{
	QQmlListProperty<QObject> qml_children()
	{
		return QQmlListProperty<QObject>(this, 0, children_append, children_count, children_at, 0);
	}
};

struct SignalTransition
: Transition<base::SignalTransition>
{
	AW_DECLARE_OBJECT_STUB(SignalTransition)
	Q_PROPERTY(QQmlListProperty<QObject> children READ qml_children)
	Q_CLASSINFO("DefaultProperty", "children")
};

AW_DEFINE_OBJECT_STUB(SignalTransition)

}
}
}
