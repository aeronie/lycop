#if !defined CONFIG_RELEASE

#include <QtQuick>

#include "meta.private.h"

namespace aw {
namespace {

static void print(const QOpenGLDebugMessage &message)
{
	qDebug() << message;
}

struct Node
: QSGNode
, QOpenGLDebugLogger
{
	Node()
	{
		QSurfaceFormat format;
		format.setOption(QSurfaceFormat::DebugContext);
		QOpenGLContext::currentContext()->setFormat(format);

		if(!initialize()) Q_UNREACHABLE();
		QObject::connect(this, &QOpenGLDebugLogger::messageLogged, print); // pour plus de précision : Qt::DirectConnection
		startLogging(); // pour plus de précision : QOpenGLDebugLogger::SynchronousLogging
		for(const QOpenGLDebugMessage &message: loggedMessages()) logMessage(message);
	}
};

struct Debugger
: QQuickItem
{
	AW_DECLARE_OBJECT_STUB(Debugger)

	QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *) override
	{
		if(!node) node = new Node;
		return node;
	}

protected:

	Debugger()
	{
		setFlags(ItemHasContents);
	}
};

AW_DEFINE_OBJECT_STUB(Debugger)

}
}

#endif
