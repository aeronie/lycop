#pragma once

#include <QMutex>
#include <mutex>

namespace aw {

struct Mutex: QMutex
{
	bool try_lock()
	{
		return tryLock();
	}
};

}
