#pragma once

#if defined CONFIG_RELEASE

#define AW_TIME(...)

#else

#include <chrono>

#define AW_TIME(_category, _id, _name) \
	struct \
	{ \
		struct Data \
		{ \
			Data(QString const &name) \
			{ \
				if(category().isDebugEnabled()) \
				{ \
					name_ = name; \
					begin_ = std::chrono::steady_clock::now(); \
				} \
			} \
			~Data() \
			{ \
				qCDebug(category) << name_ << std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::steady_clock::now() - begin_).count() << "s"; \
			} \
			QString name_; \
			std::chrono::steady_clock::time_point begin_; \
		} const data_; \
		static Q_LOGGING_CATEGORY(category, "aw.time." _category, QtWarningMsg) \
	} const _id {_name};

#endif
