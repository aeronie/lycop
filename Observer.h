#pragma once

namespace aw {

struct ObserverList;

struct Observer_
{
	inline void connect(ObserverList *);
	virtual void update() = 0;
	Observer_ **previous_ = 0;
	Observer_ *next_ = 0;

	~Observer_()
	{
		connect(0);
	}
};

template<int>
struct Observer
: Observer_
{
	struct Tag {};
	virtual void update(const Tag &) {}

	void update() override
	{
		update(Tag());
	}
};

struct ObserverList
{
	Observer_ *begin_ = 0;

	~ObserverList()
	{
		if(begin_) begin_->previous_ = 0;
	}

	void notify()
	{
		for(Observer_ *observer = begin_; observer; observer = observer->next_)
		{
			observer->update();
		}
	}
};

void Observer_::connect(ObserverList *list)
{
	if(previous_) *previous_ = next_;
	if(next_) next_->previous_ = previous_;
	previous_ = 0;
	next_ = 0;

	if(list)
	{
		previous_ = &list->begin_;
		next_ = list->begin_;
		*previous_ = this;
		if(next_) next_->previous_ = &next_;
		update();
	}
}

}
