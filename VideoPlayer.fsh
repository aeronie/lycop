uniform sampler2D texture_Y;
uniform sampler2D texture_Cb;
uniform sampler2D texture_Cr;
uniform float qt_Opacity;
varying vec2 fragment_in_texture_;

/*

https://en.wikipedia.org/wiki/YCbCr#YCbCr

Ŷ	= r*R+(1-r-b)*G+b*B
Pb	= (1/2)*(1/(1-b))*(B-Ŷ)
Pr	= (1/2)*(1/(1-r))*(R-Ŷ)

Y	= α Ŷ + γ	= α*(r*R+(1-r-b)*G+b*B)+γ				= α Kr, α (1 - Kb - Kr), α Kb, γ
Cb	= β Pb + δ	= β*((1/2)*(1/(1-b))*(B-(r*R+(1-r-b)*G+b*B)))+δ	= β/2 Kr / (Kb - 1), β/2 (1 - Kb - Kr) / (Kb - 1), β/2, δ
Cr	= β Pr + δ	= β*((1/2)*(1/(1-r))*(R-(r*R+(1-r-b)*G+b*B)))+δ		= β/2, β/2 (1 - Kb - Kr) / (Kr - 1), β/2 Kb / (Kr - 1), δ

β ← β/2

{{α*r,α*(1-b-r),α*b,γ},{β*r/(b-1),β*(1-b-r)/(b-1),β,δ},{β,β*(1-b-r)/(r-1),β*b/(r-1),δ},{0,0,0,1}}^-1

| 1/α	| 0				| 1/β ⋅ (1 - Kr)			| δ/β ⋅ (Kr - 1) - γ/α				|
| 1/α	| 1/β ⋅ (Kb - Kb²) / (Kb + Kr - 1)	| 1/β ⋅ (Kr - Kr²) / (Kb + Kr - 1)	| δ/β ⋅ (Kb² - Kb + Kr² - Kr) / (Kb + Kr - 1) - γ/α	|
| 1/α	| 1/β ⋅ (1 - Kb)			| 0				| δ/β ⋅ (Kb - 1) - γ/α				|
| 0	| 0				| 0				| 1						|

*/

const float Kb = .114;
const float Kr = .299;
const float Ay = 1. / (219. / 255.);
const float Ac = 1. / ((224. / 2.) / 255.);
const float By = -Ay * (16. / 255.);
const float Bc = -Ac * (128. / 255.);
const mat4 YCbCr_to_RGB = mat4 (
	Ay,			Ay,							Ay,			0,
	0,			Ac * (1. - Kb) * Kb / (Kb + Kr - 1.),			Ac * (1. - Kb),		0,
	Ac * (1. - Kr),		Ac * (1. - Kr) * Kr / (Kb + Kr - 1.),				0,			0,
	Bc * (1. - Kr) + By,	Bc * ((1. - Kb) * Kb + (1. - Kr) * Kr) / (Kb + Kr - 1.) + By,	Bc * (1. - Kb) + By,	1);

void main()
{
	gl_FragColor = YCbCr_to_RGB * vec4 (
		texture2D(texture_Y, fragment_in_texture_).r,
		texture2D(texture_Cb, fragment_in_texture_).r,
		texture2D(texture_Cr, fragment_in_texture_).r,
		qt_Opacity);
}
