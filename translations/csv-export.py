#!/usr/bin/python
texts = {}
langs = ('fr', 'en')

import xml.etree.ElementTree as ET
for lang in langs:
	texts[lang] = []
	root = ET.parse(lang + '.ts')
	for context in root.findall('.//context'):
		for message in context.findall('.//message'):
			texts[lang].append((context.find('name').text, message.find('source').text, message.find('translation').text, message.find('translation').get('type', '')))

import csv
out = csv.writer(open('ts.csv', 'w', encoding='utf-8'), quoting=csv.QUOTE_ALL)
for message in texts[langs[0]]:
	row = list(message[:2])
	for lang in langs:
		row += next(x[2:] for x in texts[lang] if x[:2] == message[:2])
	out.writerow([(x or '') for x in row])
