<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB">
<context>
    <name>AccidentDialog</name>
    <message>
        <source>text 1 %1</source>
        <translation>Idiot! You have ruined my ship. Reparing all this mess will cost me %1 kg of construction material. Give it to me now or I’ll call the police!</translation>
    </message>
    <message>
        <source>text 2 %1</source>
        <translation>Come one now, you should pay attention! My great ship is good for the scrapheap! I realise it was an accident, I’ll forget about it if you give me back %1 kg of construction material. Ok?</translation>
    </message>
    <message>
        <source>text 3 %1</source>
        <translation>Stop! In the name of the Transport Commission of Capitis, this behaviour is not tolerated in this town. You will either have to pay %1 kg of construction material as repayment of the damaged caused or you will suffer the consequences of your behaviour.</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Ignore</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Pay</translation>
    </message>
</context>
<context>
    <name>Controls</name>
    <message>
        <source>direction</source>
        <translation>Ship orientation</translation>
    </message>
    <message>
        <source>velocity</source>
        <translation>Ship velocity</translation>
    </message>
    <message>
        <source>weapons</source>
        <translation>Primary weapons</translation>
    </message>
    <message>
        <source>secondary-weapons</source>
        <translation>Secondary weapons</translation>
    </message>
    <message>
        <source>speed-boost</source>
        <translation>Speed boost</translation>
    </message>
    <message>
        <source>weapons-boost</source>
        <translation>Favor weapons frequency</translation>
    </message>
    <message>
        <source>shield-boost</source>
        <translation>Favor shield regeneration</translation>
    </message>
    <message>
        <source>repair</source>
        <translation>Repair dammaged equipments</translation>
    </message>
    <message>
        <source>slot-%1</source>
        <translation>Equipment %1</translation>
    </message>
    <message>
        <source>button-%1</source>
        <translation>Button %1 of action bar</translation>
    </message>
    <message>
        <source>Mouse button %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Mouse wheel %1</source>
        <translation>Mouse wheel %1</translation>
    </message>
    <message>
        <source>Keyboard %1</source>
        <translation>Key [%1] of keyboard</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation>Mouse motion</translation>
    </message>
    <message>
        <source>Joystick button %1:%2</source>
        <translation>Button %2 of gamepad %1</translation>
    </message>
    <message>
        <source>Joystick axis %1:%2</source>
        <translation>Analog stick %2 of gamepad %1</translation>
    </message>
    <message>
        <source>Joystick hat %1:%2</source>
        <translation>Directional pad %2 of gamepad %1</translation>
    </message>
    <message>
        <source>Joystick ball %1:%2</source>
        <translation>Trackball %2 of gamepad %1</translation>
    </message>
    <message>
        <source>Key_Up</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_W</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_Down</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_S</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_Left</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_A</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_Right</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_D</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_1</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_2</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_3</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_4</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_5</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_6</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_7</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_8</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_9</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_0</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MessageLog</name>
    <message>
        <source>No new messages.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_ConfirmControl</name>
    <message>
        <source>prompt %1 %2</source>
        <translation>%1 is already bound to the action “%2”.&lt;br&gt;To which action do you want to bind %1?</translation>
    </message>
</context>
<context>
    <name>Screen_Controls</name>
    <message>
        <source>Axes</source>
        <translation>Analog sticks (gamepad)</translation>
    </message>
    <message>
        <source>Buttons</source>
        <translation>Buttons (keyboard, mouse, gamepad)</translation>
    </message>
    <message>
        <source>Mouses / hats</source>
        <translation>Mouse’s motion or gamepad’s directional pad</translation>
    </message>
    <message>
        <source>Slot</source>
        <translation></translation>
    </message>
    <message>
        <source>Binding</source>
        <translation></translation>
    </message>
    <message>
        <source>Reset</source>
        <translation></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation></translation>
    </message>
    <message>
        <source>Back</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_EditControl</name>
    <message>
        <source>%1: %2</source>
        <translation></translation>
    </message>
    <message>
        <source>help 0</source>
        <translation>&lt;h1&gt;Please press a button on your keyboard, mouse or gamepad.&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>help 1</source>
        <translation>&lt;h1&gt;Please move your gamepad’s analog stick.&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>help 2</source>
        <translation>&lt;h1&gt;Please click with your mouse or press your gamepad’s directional pad.&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>Back</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_EditGame</name>
    <message>
        <source>Annotate game “%1”:</source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <source>Apply</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_EditScore</name>
    <message>
        <source>prompt %1 %2</source>
        <translation>Annotate score “%1” recorded on %2:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <source>Apply</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_Equipments</name>
    <message>
        <source>%1 HP</source>
        <translation></translation>
    </message>
    <message>
        <source>no-equipment</source>
        <translation>&lt;h1&gt;None&lt;/h1&gt;&lt;p&gt;Please select an equipment in the list above.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>no-slot</source>
        <translation>&lt;h1&gt;None&lt;/h1&gt;&lt;p&gt;Please install an equipment or select an other slot on the ship.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Equip (%1 kg)</source>
        <translation></translation>
    </message>
    <message>
        <source>Equip</source>
        <translation></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation></translation>
    </message>
    <message>
        <source>Recycle (%1 kg)</source>
        <translation></translation>
    </message>
    <message>
        <source>Rotate left</source>
        <translation></translation>
    </message>
    <message>
        <source>Rotate right</source>
        <translation></translation>
    </message>
    <message>
        <source>Repair (%1 kg)</source>
        <translation></translation>
    </message>
    <message>
        <source>Repair all (%1 kg)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_GameOver</name>
    <message>
        <source>Game over</source>
        <translation></translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>Try again</translation>
    </message>
    <message>
        <source>Load</source>
        <translation></translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Give up</translation>
    </message>
    <message>
        <source>Pensez à utiliser le celeritas accretio pour vous déplacer plus vite</source>
        <translation>Use the celeritas accretio to speed up!</translation>
    </message>
    <message>
        <source>Le bouclier à percussion désactive le bouclier Zoubidou Classix mais pas le bouclier auxiliaire Zoubidou Companion</source>
        <translation>The Zoubidou One Love hammer shield disable the Zoubidou Classix shield but the Zoubidou Companion auxiliary shield is still working!</translation>
    </message>
    <message>
        <source>L&apos;accélerateur linéaire vous permet passivement de vous déplacer plus vite</source>
        <translation>The Dango linear accelerator passively increase your movement speed.</translation>
    </message>
    <message>
        <source>Le générateur de replis de l’espace permet de vous téléporter. Si vous vous téléportez sur un petit ennemi, vous échangerez de position</source>
        <translation>The Space-fold generator enable teleportation. If used on small enemy ship, you will swap with it.</translation>
    </message>
    <message>
        <source>Vous manquez de DPS ? Utilisez le Modus accretio</source>
        <translation>Need more firepower? Use the Modus accretio!</translation>
    </message>
    <message>
        <source>Conseil : %1</source>
        <translation>Hint: %1</translation>
    </message>
</context>
<context>
    <name>Screen_LoadGame</name>
    <message>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <source>Summary</source>
        <translation></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation></translation>
    </message>
    <message>
        <source>Load</source>
        <translation></translation>
    </message>
    <message>
        <source>Back</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_Menu</name>
    <message>
        <source>Travel</source>
        <translation>Hyperspace</translation>
    </message>
    <message>
        <source>Planète Capitis</source>
        <translation>Planet Capitis</translation>
    </message>
    <message>
        <source>Vaisseau-monde Heter</source>
        <translation>Heter commanding ship</translation>
    </message>
    <message>
        <source>Easy</source>
        <translation>Story mode</translation>
    </message>
    <message>
        <source>Equipments</source>
        <translation></translation>
    </message>
    <message>
        <source>Technologies</source>
        <translation></translation>
    </message>
    <message>
        <source>Menu</source>
        <translation></translation>
    </message>
    <message>
        <source>Back</source>
        <translation></translation>
    </message>
    <message>
        <source>Mercury</source>
        <translation></translation>
    </message>
    <message>
        <source>Sun</source>
        <translation></translation>
    </message>
    <message>
        <source>Earth</source>
        <translation></translation>
    </message>
    <message>
        <source>Auto load</source>
        <translation></translation>
    </message>
    <message>
        <source>New game</source>
        <translation></translation>
    </message>
    <message>
        <source>Load</source>
        <translation></translation>
    </message>
    <message>
        <source>Scores</source>
        <translation></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation></translation>
    </message>
    <message>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <source>Title screen</source>
        <translation></translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Give up</translation>
    </message>
    <message>
        <source>demo version %1</source>
        <translation></translation>
    </message>
    <message>
        <source>version %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Survival mode</source>
        <translation></translation>
    </message>
    <message>
        <source>Faible</source>
        <translation>Coward</translation>
    </message>
    <message>
        <source>Vaillant</source>
        <translation>Brave</translation>
    </message>
    <message>
        <source>Training mode</source>
        <translation>Training mode</translation>
    </message>
</context>
<context>
    <name>Screen_Options</name>
    <message>
        <source>Display preferences</source>
        <translation></translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation></translation>
    </message>
    <message>
        <source>Diffuse</source>
        <translation></translation>
    </message>
    <message>
        <source>Diffuse and specular</source>
        <translation></translation>
    </message>
    <message>
        <source>Flat</source>
        <translation></translation>
    </message>
    <message>
        <source>Lighting quality</source>
        <translation></translation>
    </message>
    <message>
        <source>Sound preferences</source>
        <translation></translation>
    </message>
    <message>
        <source>Volume: sound effects</source>
        <translation></translation>
    </message>
    <message>
        <source>Volume: music</source>
        <translation></translation>
    </message>
    <message>
        <source>Volume: cutscenes</source>
        <translation></translation>
    </message>
    <message>
        <source>Input preferences</source>
        <translation></translation>
    </message>
    <message>
        <source>Absolute velocity</source>
        <translation></translation>
    </message>
    <message>
        <source>Bindings</source>
        <translation>Input devices</translation>
    </message>
    <message>
        <source>Configure...</source>
        <translation></translation>
    </message>
    <message>
        <source>Back</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_Quit</name>
    <message>
        <source>title</source>
        <translation>Really?</translation>
    </message>
    <message>
        <source>prompt</source>
        <translation>Cowards die in shame.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <source>Quit</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_RemoveGame1</name>
    <message>
        <source>Do you want to remove all data about game “%1”?</source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_RemoveGame2</name>
    <message>
        <source>Do you want to remove data saved on %2 about game “%1”?</source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_RemoveScore</name>
    <message>
        <source>prompt %1 %2</source>
        <translation>Do you want to delete score “%1” recorded on %2?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_ResetControls</name>
    <message>
        <source>prompt</source>
        <translation>Do you want to reset to default parameters?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <source>Reset</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_Scores</name>
    <message>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <source>Wave</source>
        <translation></translation>
    </message>
    <message>
        <source>end</source>
        <translation>complete</translation>
    </message>
    <message>
        <source>wave %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Score</source>
        <translation></translation>
    </message>
    <message>
        <source>score %1</source>
        <translation>%1 points</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Annotate</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation></translation>
    </message>
    <message>
        <source>Back</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_Technologies</name>
    <message>
        <source>Disabled</source>
        <translation>Unavailable</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Available</translation>
    </message>
    <message>
        <source>Level %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Science: %1 points</source>
        <translation></translation>
    </message>
    <message>
        <source>Research</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_Travel</name>
    <message>
        <source>Travel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <source>%1 HP</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Tab_credit</name>
    <message>
        <source>&lt;h1&gt;Aeronie&apos;s Dream Team&lt;/h1&gt;&lt;br&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h1&gt;Translations&lt;/h1&gt;&lt;br&gt;</source>
        <translation>&lt;h1&gt; Translation and rereading&lt;/h1&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Background music&lt;/h1&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Title screen&lt;/h2&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 1&lt;/h2&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 2&lt;/h2&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 3&lt;/h2&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 4&lt;/h2&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 5&lt;/h2&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 6&lt;/h2&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Fin ?&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;The end?&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 7&lt;/h2&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Epilogue&lt;/h2&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Boss themes&lt;/h2&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h1&gt;Sound Effects&lt;/h1&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h1&gt;Libraries&lt;/h1&gt;&lt;br&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Remerciement</source>
        <translation>We want to thank our families and friends who always supported us.</translation>
    </message>
    <message>
        <source>&lt;h1&gt; Capitaine Lycop : L&apos;invasion des Heters &lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Capitain Lycop: Invasion of the Heters&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Outils Graphiques&lt;/h1&gt;&lt;br&gt;</source>
        <translation>Graphical tools</translation>
    </message>
</context>
<context>
    <name>Technologies</name>
    <message>
        <source>hull</source>
        <translation>Improved armor</translation>
    </message>
    <message>
        <source>hull 0</source>
        <translation>&lt;p&gt;Constant improvements in alloys and composite materials enable the production of new armors with increased performances.&lt;/p&gt;
&lt;p&gt; These new and more resistant armors will protect you and your ship from the mechanical, magnetic and chemicals assaults which make the delights of interstellar travels.&lt;/p&gt; </translation>
    </message>
    <message>
        <source>nanomachines</source>
        <translation>LoTec Formic nanomachines 17</translation>
    </message>
    <message>
        <source>nanomachines 0</source>
        <translation>&lt;p&gt; At the end of a rough battle, most pilots THROW AWAY damaged pieces and REPLACE them by new pieces&lt;/&gt;
&lt;p&gt; However, at LoTec, we offer these adorable little robots which penetrate to THE HEART OF THE MACHINE to REPAIR damaged pieces, without waiting for the end of hostilities! &lt;/p&gt;
&lt;p&gt;Join the ranks of those who have chosen to save money through nanorobotics! &lt;/p&gt;</translation>
    </message>
    <message>
        <source>drone-launcher</source>
        <translation>LoTec Vesp drones controller 22</translation>
    </message>
    <message>
        <source>drone-launcher 0</source>
        <translation>&lt;p&gt; At LoTec we like to recall that if the brave battles valiantly, he who does not fight  sees the outcome of battle.&lt;/p&gt;
&lt;p&gt; This embedded mini-factory falls in line with this philosophy. It produces and controls simultaneously up to 5 drones which fight (and will be destroyed) ON YOUR BEHALF.&lt;/p&gt;
&lt;p&gt; Because our robots are mass produced, but our clients are unique.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>drone-launcher 1</source>
        <translation>&lt;li&gt;Drones are equipped with Gatling M438 cannons.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>drone-launcher 2</source>
        <translation>&lt;li&gt;Drones are equipped with Dagobert 3.3 missile launchers.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>gyroscope</source>
        <translation>Mochi angular accelerator by Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>gyroscope 0</source>
        <translation>&lt;p&gt; In a changing world, a modern man needs to be able to turn himself around at any moment. That is why we have created this module that increases the efficiency of your ship engine and makes it turn faster.&lt;/p&gt;
&lt;p&gt; Beware, some clients relate that extended use might lead to dizziness and nausea. &lt;/p&gt;</translation>
    </message>
    <message>
        <source>gyroscope 2</source>
        <translation>&lt;li&gt;The ship turns even faster.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>dash</source>
        <translation>Dango linear accelerator by Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>dash 0</source>
        <translation>&lt;p&gt; Do you feel left behind? Do you want to change rhythm? It is for you that we have created this module which increases the efficiency of your ship engines and makes them go faster.&lt;/p&gt;
&lt;p&gt; For your security, and the security of others customers, observe speed limitations in urbanized spaces and asteroid fields.&lt;/p&gt; </translation>
    </message>
    <message>
        <source>dash 2</source>
        <translation>&lt;li&gt;The ship moves even faster.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>rocket-launcher</source>
        <translation>Dagobert 2.3 rocket launcher</translation>
    </message>
    <message>
        <source>rocket-launcher 0</source>
        <translation>&lt;p&gt;This weapon launches self-propelled projectiles loaded with 20 kg of Dagobert explosive, well known for its destructive potential.&lt;/p&gt;
&lt;p&gt; Beware, the ultra-sensitive detonation device can cause a premature explosion of the rocket upon contact with another projectile. &lt;/p&gt;</translation>
    </message>
    <message>
        <source>missile-launcher</source>
        <translation>Dagobert 3.3 missile launcher</translation>
    </message>
    <message>
        <source>missile-launcher 0</source>
        <translation>&lt;p&gt;This weapon launches self-propelled and self-guided projectiles loaded with 20 kg of Dagobert explosive. Your foes will be awed.&lt;/p&gt;
&lt;p&gt;The missile detonation device has been improved in order to be less sensitive to enemies’ projectiles. Our engineers are now working on the laser problem and have promised a prototype before the next galactic revolution&lt;/p&gt;</translation>
    </message>
    <message>
        <source>gun</source>
        <translation>Gatling M438 cannon</translation>
    </message>
    <message>
        <source>gun 0</source>
        <translation>&lt;p&gt; This simple and reliable gun makes up the base of the defense of any ship with a tried and tested solution: launching projectiles on potential threats.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>triple-gun</source>
        <translation>Gatling M438-S3 triple cannon</translation>
    </message>
    <message>
        <source>triple-gun 0</source>
        <translation>&lt;p&gt;A recently published study has shown that projectiles fired from guns are small and relatively spaced out. How to be certain to hit one’s target? According to the scientists, the solution is either to increase the size of projectiles or to reduce the space between them. &lt;/p&gt;
&lt;p&gt; This gun explores the first solution by firing multiple parallel projectiles to artificially increase their size and make dodging harder.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>coil-gun</source>
        <translation>Ares coilgun</translation>
    </message>
    <message>
        <source>coil-gun 0</source>
        <translation>&lt;p&gt; We could increase the power of ours guns by increasing the amount of fuel dedicated to projectiles’ acceleration, if ecologists’ lobbies would let us extract it. Instead, we are reduced to use magnetic induction to accelerate metallic projectile in cannons with solenoid coiled around. Of course, this cannon has a formidable firepower. But do we really need to sacrifice our traditions on the altar of ecological demagogy? &lt;/p&gt;</translation>
    </message>
    <message>
        <source>electric-gun</source>
        <translation>Zeus lightning gun</translation>
    </message>
    <message>
        <source>electric-gun 0</source>
        <translation>&lt;p&gt; Nowadays, shields seem to be very trendy among the younger pilots. And believe us, we find this regrettable. So, all our rage, all our despair have been gathered in this cannon which fires plasma balls roamed with very intense electrical currents. They are terribly efficient against energy fields. Well, they do not scratch the hull. But on a shield, ah, such an effect! &lt;/p&gt;</translation>
    </message>
    <message>
        <source>mine-launcher</source>
        <translation>Dagobert 2.5 mine launcher</translation>
    </message>
    <message>
        <source>mine-launcher 0</source>
        <translation>&lt;p&gt; This weapon drops off an explosive device triggered by several proximity sensors. The explosive load of 10 kg of Dagobert explosive will surprise the first ship brave enough to come close to mine. &lt;/p&gt;
&lt;p&gt; Beware, to ensure a good functioning of the mines even against “tactics” like camouflage or cyber-sabotage, the ultra-sensitive detonation device has been designed to detect foes as well as and allies.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>acid-gun</source>
        <translation>Dionysus acid gun</translation>
    </message>
    <message>
        <source>acid-gun 0</source>
        <translation>&lt;p&gt; The race to firepower is over. This cannon can destroy any equipment instantly. Its acid projectile attacks the mount point of the equipment, corrodes it and detaches it from the ship.
Only energy shields are (still) able to resist to it. But after all, who cares? Equipment are destroyed! Instantly!&lt;/p&gt;</translation>
    </message>
    <message>
        <source>random-spray</source>
        <translation>YOLO Orcish cannon</translation>
    </message>
    <message>
        <source>random-spray 0</source>
        <translation>&lt;p&gt; This cannon is the first attempt from Heters to assimilate human technology. It fires normal, electrical, acid or magnetically accelerated projectiles randomly and with a questionable precision . &lt;/p&gt;</translation>
    </message>
    <message>
        <source>plasma-spray</source>
        <translation>Hephaestus plasma spray</translation>
    </message>
    <message>
        <source>plasma-spay 1</source>
        <translation>&lt;li&gt; Firepower: +25 damages / s by second of exposure to plasma&lt;/li&gt;</translation>
    </message>
    <message>
        <source>plasma-spray 0</source>
        <translation>&lt;p&gt; Inspired by the delicious cruelty of human flame-thrower, the Heters have created this cannon dispersing high temperature plasma at short range.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>vortex-gun</source>
        <translation>Vortex cannon</translation>
    </message>
    <message>
        <source>vortex-gun 0</source>
        <translation>&lt;p&gt; This cannon sends a grenade which creates a spatial vortex. Any wise individual would avoid to cross its trajectories. &lt;/p&gt;</translation>
    </message>
    <message>
        <source>qui-poutre</source>
        <translation>Kickass cannon</translation>
    </message>
    <message>
        <source>qui-poutre 0</source>
        <translation>&lt;p&gt;This weapon is the scaled down version of the Heter world destructor. Unstable but extremely powerful, this weapon is more powerful than any other.&lt;/p&gt; </translation>
    </message>
    <message>
        <source>qui-poutre 1</source>
        <translation>&lt;li&gt;Firepower: 3000 damages, and 300 damages / s near the projectile&lt;/li&gt;</translation>
    </message>
    <message>
        <source>laser-gun</source>
        <translation>Poutrox XZ15 laser cannon</translation>
    </message>
    <message>
        <source>laser-gun 0</source>
        <translation>&lt;p&gt; Heters, naturally equipped with a heavy shell, have favored very early weapons able to melt their enemies. This laser cannon, equivalent to human cannons in term of destructive potential, is the result to this specific evolution.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>heavy-laser-gun</source>
        <translation>Poutrox XZ25 heavy laser cannon</translation>
    </message>
    <message>
        <source>heavy-laser-gun 0</source>
        <translation>&lt;p&gt; You will never ever know the frustration of barely scratching your enemy’s ship with a low power laser! With this heavy weapon, you will probably hit it less often, but whenever you will , it will be with the sweet conviction that you have inflicted all the pain it deserves.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>maser-emitter</source>
        <translation>Poutrox WR65 maser emitter</translation>
    </message>
    <message>
        <source>maser-emitter 0</source>
        <translation>&lt;p&gt; Yes, some preys try to hide behind obstacles to avoid your shots. But wherever they are hidden, be certain that they will not be able to avoid this electromagnetic beam that goes through any obstacle on its way, whereas it destroys their molecular structure.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>laser-bouncer</source>
        <translation>Poutrox XZ45 fragmentation laser cannon</translation>
    </message>
    <message>
        <source>laser-bouncer 0</source>
        <translation>&lt;p&gt; No need to aim at every small ship anymore! This cannon emits a high energy laser beam that explodes and separates into a myriad of smaller laser beams. As efficient as graceful, this weapon is often used on Capitis during official ceremonies .&lt;/p&gt;</translation>
    </message>
    <message>
        <source>shield-generator</source>
        <translation>Zoubidou Classix shield 1962</translation>
    </message>
    <message>
        <source>shield-generator 0</source>
        <translation>&lt;p&gt;This model, acclaimed by the critics, has established itself as a reference along the years. It remains relevant even now.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>shield-canceler</source>
        <translation>Sigma-tau magnetic flux compressor</translation>
    </message>
    <message>
        <source>shield-canceler 0</source>
        <translation>&lt;p&gt;This weapon generates an electromagnetic pulse that disables any shields in its area of effect, including those of the emiter’s ship.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>shield-canceler 2</source>
        <translation>&lt;li&gt;Emitter’s secondary shields are not affected by the EMP.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>aggressive-shield-generator</source>
        <translation>Zoubidou One Love hammer shield</translation>
    </message>
    <message>
        <source>aggressive-shield-generator 0</source>
        <translation>&lt;p&gt; For those who think that attack is the best defense, the Zoubidou laboratories have created this hammer shield which offers a maximal protection against collisions with medium sized object (1-100 tonnes) by destroying these objects.&lt;/p&gt; </translation>
    </message>
    <message>
        <source>secondary-generator</source>
        <translation>Taiyaki engine-generator by Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>secondary-generator 0</source>
        <translation>&lt;p&gt;We know exactly what you have been thinking: What is the point to equip oneself with cutting-edge technologies if you have to spend all your time waiting for your batteries to be charged? Why are all these devices using so much energy? From now on, all these questions belong to the past. With this electric generator, you will make the most of all the modern comfort by using all yours equipments without limits.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>secondary-shield-generator</source>
        <translation>Zoubidou Companion auxiliary shield</translation>
    </message>
    <message>
        <source>secondary-shield-generator 0</source>
        <translation>&lt;p&gt;This shield protects the most vulnerable part of the ship in case of failure of the main shield, thanks to several independent units dedicated to each equipment.&lt;.p&gt;</translation>
    </message>
    <message>
        <source>teleporter</source>
        <translation>Space-fold generator</translation>
    </message>
    <message>
        <source>teleporter 0</source>
        <translation>&lt;p&gt; Travel without move is possible, but only at short range.&lt;/p&gt;&lt;p&gt;Since the great Lars Nedum has traveled to a star, this module has been equipped with a security &quot;Pinpin&quot; preventing such regrettable travels. However, these canceled travels still cost energy&lt;/p&gt; </translation>
    </message>
    <message>
        <source>secondary-battery</source>
        <translation>Dorayaki additional battery by Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>secondary-battery 0</source>
        <translation>&lt;p&gt; Do you have great plans but no means to realize them? With these supplementary batteries, you can at last stock all the energy that you need to realize your dreams.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>harpoon-launcher</source>
        <translation>Poseidon harpoon launcher</translation>
    </message>
    <message>
        <source>harpoon-launcher 0</source>
        <translation>&lt;p&gt;A summer afternoon like so many others. Your opponent, an expression of terror on his face, screaming incoherently, tries to escape your justice. But it is a waste of time: with your harpoon, you keep him within reach of your cannons, and you give him the correction he deserves.&lt;/p&gt;&lt;p&gt;What seemed like a dream is now a reality thanks to this new harpoon and its titanium fiber cable.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>absorbing-shield-generator</source>
        <translation>Zoubidou Assimilator absorber shield</translation>
    </message>
    <message>
        <source>absorbing-shield-generator 0</source>
        <translation>&lt;p&gt; This shield collects energy from intercepted projectiles and uses it to recharge the main shield of the ship.&lt;/p&gt;
&lt;p&gt;Beware! Extended use can trigger an overload of the main shield and cause damage to the hull. Read the instructions carefully before the first start up.&lt;/p&gt;
&lt;p&gt;Comes with an hypo-allergenic filter.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>wall-generator</source>
        <translation>Theta-xi barricades generator</translation>
    </message>
    <message>
        <source>wall-generator 0</source>
        <translation>&lt;p&gt;This cannon generates and maintains up to three energy barriers that reflect all ballistic, electric, acid and laser projectiles, including those of their owner.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>wall-generator 1</source>
        <translation>&lt;li&gt;Maximal number of walls: 3&lt;/li&gt;</translation>
    </message>
    <message>
        <source>wall-generator 2</source>
        <translation>&lt;li&gt;The walls reflect all projectiles.&lt;/li&gt;</translation>
    </message>
</context>
<context>
    <name>Technology</name>
    <message>
        <source>&lt;h2&gt;Level %1&lt;/h2&gt;&lt;ul&gt;%2&lt;/ul&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Required materials: %1 kg&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Effect duration: %1 s&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Activation time: %1 s&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Fire rate: %1 Hz&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Firepower: %1 damages / %2&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Damage dealt during overload: %1 of base damages&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Firepower: %1 damages&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>s</source>
        <translation></translation>
    </message>
    <message>
        <source>impact</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Resistance: %1 HP&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Equipments regeneration: %1 HP / s&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Resistance: +%1 HP per equipment&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield resistance: %1 HP&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield regeneration: %1 HP / %2&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield downtime: %1 s&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield regeneration: %1 of base damages&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Supplementary energy: %1 J&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Energy regeneration: %1 J / s&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Energy consumed: %1 J / %2&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>use</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Materials consumed: %1 kg / %2&lt;/li&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>apis-zone</name>
    <message>
        <source>title</source>
        <translation>Chapter 5: The APIS biosphere</translation>
    </message>
    <message>
        <source>Vagues restantes: %1</source>
        <translation>Remaining waves: %1</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>At least, we are arriving in the zone of the APIS biosphere</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>I only detect six biospheres</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>It doesn’t seem enough to host all the species considered unsuitable by the INSEC alliance.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>What do you think? Maybe, very little species are considered unsuitable. We are maybe too old-fashioned for the alliance.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>I mainly think they despise our uniqueness.</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>Look. I have spotted a bunch of ships.</translation>
    </message>
    <message>
        <source>manif 1</source>
        <translation>We cannot let the Heters exterminate intelligent communities just because they are vindictive.
The INSEC alliance does not exterminate communities! They assimilate them! It has always been like this. Our duty is to stop them!</translation>
    </message>
    <message>
        <source>manif 2</source>
        <translation>When they freeze stars, they turn the heart of the people to ice.
What shall we do when a species intelligent enough will want to get their revenge? 
It would be a global war with no mercy!
We have to stop the Heters’ folly!
Lets gather at the deputy chamber fo the Alliance. The Heters have to be stopped.</translation>
    </message>
    <message>
        <source>manif 3</source>
        <translation>No to Glaciation! Stop to the extermination! Let’s fight for preservation!</translation>
    </message>
    <message>
        <source>Après manif 1</source>
        <translation>But they seem very nice! They remind me of my country, France.</translation>
    </message>
    <message>
        <source>Après manif 2</source>
        <translation>This is clearly an opportunity to find allies.</translation>
    </message>
    <message>
        <source>Après manif 3</source>
        <translation>Nectaire is right. I’m going to talk to their chief.</translation>
    </message>
    <message>
        <source>apis_before_invasion 1</source>
        <translation>Hi, I’m in charge of the APIS biosphere.
What can I do for you?</translation>
    </message>
    <message>
        <source>apis_before_invasion 2</source>
        <translation>The administration told us to contact you for an accommodation.</translation>
    </message>
    <message>
        <source>apis_before_invasion 3</source>
        <translation>Sadly, we cannot accommodate you.
As you can see there are only six biosphere left.
All the others have been destroyed.</translation>
    </message>
    <message>
        <source>apis_before_invasion 4</source>
        <translation>Destroyed?</translation>
    </message>
    <message>
        <source>apis_before_invasion 5</source>
        <translation>Some drones raided our biopsheres. We suspect the Heters to be responsible for these attacks.
As we are pacific people, we are looking for a diplomatic solution.
Sadly, it did not work.</translation>
    </message>
    <message>
        <source>apis_before_invasion 6</source>
        <translation>If they attack you, I will defend you! You will not lose another biosphere!</translation>
    </message>
    <message>
        <source>apis_before_invasion 7</source>
        <translation>I’ve been told that space portals are opening.
Do not adopt gratuitous violence. We will find a solution.
</translation>
    </message>
    <message>
        <source>apis_before_invasion 8</source>
        <translation>Believe my experience. The Heters have only one way of thinking: Vae Victis!
I will not be defeated.</translation>
    </message>
    <message>
        <source>after_invasion 1</source>
        <translation>These are Heter drones indeed. These cockroaches raid everything they find.</translation>
    </message>
    <message>
        <source>after_invasion 2</source>
        <translation>Congrats Captain! The APIS will certainly be grateful.
I was expecting no less from you.</translation>
    </message>
    <message>
        <source>after_invasion 3</source>
        <translation>Nectaire, there is no need to flatter me. A hero has to be humble.</translation>
    </message>
    <message>
        <source>apis_after_invasion 1</source>
        <translation>Thanks a lot! Without you all the biospheres would have been destroyed.
It is our turn to help you.</translation>
    </message>
    <message>
        <source>apis_after_invasion 2</source>
        <translation>Where is located the Heters’ mothership?</translation>
    </message>
    <message>
        <source>apis_after_invasion 3</source>
        <translation>Is there a way to defrost the Sun?</translation>
    </message>
    <message>
        <source>apis_after_invasion 4</source>
        <translation>Concerning the Heters’ mothership, noone knows where it is.
However,a command ship was here today. It has just left for the Protura constellation. Here are the coordinates... </translation>
    </message>
    <message>
        <source>apis_after_invasion 5</source>
        <translation>There is a way to defrost any kind of star. To do so, you need a weapon that only the INSEC citizens can acquire. Furthermore, it is very expensive.</translation>
    </message>
    <message>
        <source>apis_after_invasion 6</source>
        <translation>We do not have a lot of resources. Moreover, none of us is an INSEC citizen.</translation>
    </message>
    <message>
        <source>apis_after_invasion 7</source>
        <translation>There is one last solution.
We have blueprints for the weapon.
Maybe you could build it?</translation>
    </message>
    <message>
        <source>apis_after_invasion 8</source>
        <translation>I thought you were pacifists?</translation>
    </message>
    <message>
        <source>apis_after_invasion 9</source>
        <translation>Owning blueprints does not mean that we want to use them.
Knowledge is necessary, its practice isn’t.</translation>
    </message>
    <message>
        <source>apis_after_invasion 10</source>
        <translation>These blueprints are complex. Sadly, we miss some parts.
We require a spatial isolation cell... </translation>
    </message>
    <message>
        <source>apis_after_invasion 11</source>
        <translation>We can give you one. This technology is required to make our biosphere work properly,.</translation>
    </message>
    <message>
        <source>apis_after_invasion 12</source>
        <translation>We also need a Laser concentrator WX99 and a MYRMEL ZZ+ power accumulator.</translation>
    </message>
    <message>
        <source>apis_after_invasion 13</source>
        <translation>Concerning the concentrator, I believe that it is the reward of the next race organised by the MYRMEL familly and the POUTROX industries.</translation>
    </message>
    <message>
        <source>apis_after_invasion 14</source>
        <translation>Lets go and win this race!</translation>
    </message>
</context>
<context>
    <name>aw::Info</name>
    <message>
        <source>enable</source>
        <translation>normal</translation>
    </message>
    <message>
        <source>disable</source>
        <translation>inverted</translation>
    </message>
    <message>
        <source>up_left_down_right</source>
        <translation>normal</translation>
    </message>
    <message>
        <source>up_right_down_left</source>
        <translation>horizontal inverted</translation>
    </message>
    <message>
        <source>down_left_up_right</source>
        <translation>vertical inverted</translation>
    </message>
    <message>
        <source>down_right_up_left</source>
        <translation>both inverted</translation>
    </message>
    <message>
        <source>up</source>
        <translation></translation>
    </message>
    <message>
        <source>down</source>
        <translation></translation>
    </message>
    <message>
        <source>left</source>
        <translation></translation>
    </message>
    <message>
        <source>right</source>
        <translation></translation>
    </message>
    <message>
        <source>up_down</source>
        <translation>vertical</translation>
    </message>
    <message>
        <source>down_up</source>
        <translation>vertical (inverted)</translation>
    </message>
    <message>
        <source>left_right</source>
        <translation>horizontal</translation>
    </message>
    <message>
        <source>right_left</source>
        <translation>horizontal (inverted)</translation>
    </message>
    <message>
        <source>plus</source>
        <translation>normal</translation>
    </message>
    <message>
        <source>minus</source>
        <translation>inverted</translation>
    </message>
</context>
<context>
    <name>boss-zone</name>
    <message>
        <source>title</source>
        <translation>Chapter 5: The Propugnator</translation>
    </message>
    <message>
        <source>Propugnator: %1 secondes</source>
        <translation></translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Hurry! Evacuate the Capitis planet before the Propugnator’s arrival!</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>What are you talking about?</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>It is the city’s main defense starship. Hurry-up or you’ll be trapped by its tractor beam and you won’t be able to use the hyper-exponential propulsion.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>It is already the case. It is impossible to leave. We’ll have to fight our way out.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>I fear no one! let them come to us, they will die!</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>Do not fight. A pacific solution must exist.</translation>
    </message>
    <message>
        <source>begin 7</source>
        <translation>Do not interfere, or you may be destroyed.
In this kind of battle, there are lots of collateral damages.</translation>
    </message>
    <message>
        <source>after boss 1</source>
        <translation>The entire INSEC army cannot do anything againts me!</translation>
    </message>
    <message>
        <source>after boss apis 1</source>
        <translation>What  violence! Thanks for sparing my life.</translation>
    </message>
    <message>
        <source>after boss apis 2</source>
        <translation>As you helped me, you are going to be outlaws.</translation>
    </message>
    <message>
        <source>after boss apis 3</source>
        <translation>The APIS are already loathed by the members of the INSEC alliance. It is the first time that a belligerent species helps us protect the biospheres. I will always be grateful for saving us.</translation>
    </message>
    <message>
        <source>after boss apis 4</source>
        <translation>I will crush everyone who oppresses the weak. The strongest have to protect the weak, otherwise there would be no civilisation.</translation>
    </message>
    <message>
        <source>after boss 2</source>
        <translation>Captain, I do not doubt your strength but we should leave before other ships arrive.</translation>
    </message>
    <message>
        <source>after boss 3</source>
        <translation>Indeed! The Heter commanding starship will be for it!</translation>
    </message>
</context>
<context>
    <name>chapter-0-1</name>
    <message>
        <source>title</source>
        <translation>Training: steering</translation>
    </message>
    <message>
        <source>Recyclez le satellite</source>
        <translation type="obsolete">Recycle this satellite</translation>
    </message>
    <message>
        <source>Construisez de nouveaux canons</source>
        <translation type="obsolete">Build new cannons until lack of materials</translation>
    </message>
    <message>
        <source>Détruisez le drone</source>
        <translation type="obsolete">Destroy this drone</translation>
    </message>
    <message>
        <source>Objectif : %1</source>
        <translation type="obsolete">Task: %1</translation>
    </message>
    <message>
        <source>story 1 7 %1</source>
        <translation type="obsolete">Move the ship towards the point indicated by your radar on the left side of your dashboard. You can move faster by using the Celeritas Accretio (patent pending) with &lt;b&gt;%1&lt;/b&gt;, but this consumes energy.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation type="obsolete">Let’s leave before being destroyed. {2 1?}</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation type="obsolete">Let’s leave before being destroyed. {2 2?}</translation>
    </message>
    <message>
        <source>0_beginning</source>
        <translation>Welcome to the steering training.
The goal is to master the spaceship steering
Do not forget to read all the explanations that will tell you what to do.
If you skip some dialogue, click on the speech bubbles icon in the bottom left of the screen.
Go to the first arrow.</translation>
    </message>
    <message>
        <source>2_second_door</source>
        <translation>The spaceship has an haste ability (Celeritas Accretio) that double its speed.&lt;br/&gt;You can activate it by keeping pushed the shortcut (&lt;b&gt;%1&lt;/b&gt;).&lt;br/&gt;Go to the next arrow before it turns off.</translation>
    </message>
    <message>
        <source>3_1_too_late</source>
        <translation>You did not make it in time. Retry.</translation>
    </message>
    <message>
        <source>3_2_third_door</source>
        <translation>Congratulations but beware! The faster you go, the more devastating a collision with a wall or a foe will be.
Go to the next arrow.</translation>
    </message>
    <message>
        <source>4_1_fourth_door</source>
        <translation>Go through the next two arrows before they turn off.
Optimize your path. Going over the line is enough to validate the checkpoint.</translation>
    </message>
    <message>
        <source>4_2_too_late</source>
        <translation>You did not reach the arrows in time.
Retry and don&apos;t forget to use the Celeritas Accretio.</translation>
    </message>
    <message>
        <source>6_sixth_door</source>
        <translation>Congratulations!
Go to the next arrow.</translation>
    </message>
    <message>
        <source>7_seventh_door</source>
        <translation>Some foe will certainly be undestructible.
You will have to analyse their behaviour to find an opening and go through.</translation>
    </message>
    <message>
        <source>8_final_door</source>
        <translation>Congratulations! You finished this steering training.</translation>
    </message>
    <message>
        <source>1_first_door %1</source>
        <translation type="vanished">The goal of this exercise is to learn how to move swiftly and efficiently.&lt;/br&gt;By default, steerings are relative to the spaceship direction.&lt;/br&gt;So when you hit the UP key (&lt;b&gt;%1&lt;/b&gt;), the spaceship will move go forward.&lt;/br&gt;You can change this setting in the options by checking &quot;Absolute velocity&quot;.&lt;/br&gt;So when you hit the UP key (&lt;b&gt;%1&lt;/b&gt;), the spaceship will move to the top of the screen.&lt;/br&gt;Go to the next arrow.</translation>
    </message>
    <message>
        <source>1_first_door</source>
        <translation>The goal of this exercise is to learn how to move swiftly and efficiently.&lt;br/&gt;By default, steerings are relative to the spaceship direction.&lt;br/&gt;So when you hit the UP key (&lt;b&gt;%1&lt;/b&gt;), the spaceship will move go forward.&lt;br/&gt;You can change this setting in the options by checking &quot;Absolute velocity&quot;.&lt;br/&gt;So when you hit the UP key (&lt;b&gt;%1&lt;/b&gt;), the spaceship will move to the top of the screen.&lt;br/&gt;Go to the next arrow.</translation>
    </message>
</context>
<context>
    <name>chapter-0-2</name>
    <message>
        <source>title</source>
        <translation>Training: aim and shoot</translation>
    </message>
    <message>
        <source>Recyclez le satellite</source>
        <translation type="obsolete">Recycle this satellite</translation>
    </message>
    <message>
        <source>Construisez de nouveaux canons</source>
        <translation type="obsolete">Build new cannons until lack of materials</translation>
    </message>
    <message>
        <source>Détruisez le drone</source>
        <translation type="obsolete">Destroy this drone</translation>
    </message>
    <message>
        <source>Objectif : %1</source>
        <translation type="obsolete">Task: %1</translation>
    </message>
    <message>
        <source>story 1 7 %1</source>
        <translation type="obsolete">Move the ship towards the point indicated by your radar on the left side of your dashboard. You can move faster by using the Celeritas Accretio (patent pending) with &lt;b&gt;%1&lt;/b&gt;, but this consumes energy.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation type="obsolete">Let’s leave before being destroyed. {2 1?}</translation>
    </message>
    <message>
        <source>1_first_wave_finished</source>
        <translation>Warning! Collision damage your spaceship. The next drones will move in a less predictable way .
Shoot them.</translation>
    </message>
    <message>
        <source>2_second_wave_finished</source>
        <translation>The next drones will move all over the room.
Shoot them.</translation>
    </message>
    <message>
        <source>3_third_wave_finished</source>
        <translation>Congratulations! You finished this aim and shoot training.</translation>
    </message>
    <message>
        <source>0_beginning</source>
        <translation>Welcome to the aim and shoot training.&lt;br/&gt;Destroy these drones using the firing button (&lt;b&gt;%1&lt;/b&gt;) by aiming at them.&lt;br/&gt;The spaceship has a special ability (Modus Accretio) that double its firing rate.&lt;br/&gt;You can activate it by keeping pushed the shortcut (&lt;b&gt;%2&lt;/b&gt;).</translation>
    </message>
</context>
<context>
    <name>chapter-0-3</name>
    <message>
        <source>title</source>
        <translation>Training: fight</translation>
    </message>
    <message>
        <source>Recyclez le satellite</source>
        <translation type="obsolete">Recycle this satellite</translation>
    </message>
    <message>
        <source>Construisez de nouveaux canons</source>
        <translation type="obsolete">Build new cannons until lack of materials</translation>
    </message>
    <message>
        <source>Détruisez le drone</source>
        <translation type="obsolete">Destroy this drone</translation>
    </message>
    <message>
        <source>Objectif : %1</source>
        <translation type="obsolete">Task: %1</translation>
    </message>
    <message>
        <source>story 1 7 %1</source>
        <translation type="obsolete">Move the ship towards the point indicated by your radar on the left side of your dashboard. You can move faster by using the Celeritas Accretio (patent pending) with &lt;b&gt;%1&lt;/b&gt;, but this consumes energy.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation type="obsolete">Let’s leave before being destroyed. {2 1?}</translation>
    </message>
    <message>
        <source>0_beginning</source>
        <translation>Welcome to the fight training.
The drones now have the same gun as you.
Here comes one. Shoot him.</translation>
    </message>
    <message>
        <source>1_first_wave_finished</source>
        <translation>You may have noted that the drone aim at you.
Revolving around him is an efficient way to dodge bullets.
Here come two drones. Shoot them.</translation>
    </message>
    <message>
        <source>2_second_wave_finished</source>
        <translation>Congratulations! You may be caught in a trap between foes.
Your spaceship can survive several hits but they will damage the equiments close to the impact.
Here come four drones. Shoot them.</translation>
    </message>
    <message>
        <source>3_third_wave_finished</source>
        <translation>Congratulations! Some foes may be stronger. They will have more equipments making them more resiliant and more dangerous.
Here comes a big one. Shoot him.</translation>
    </message>
    <message>
        <source>4_fourth_wave_finished</source>
        <translation>Congratulations! You finished this fight training. You are ready for your mission.</translation>
    </message>
</context>
<context>
    <name>chapter-1</name>
    <message>
        <source>title</source>
        <translation>Chapter 1: low Earth orbit</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Welcome, Captain! I see that you have reached space. I told you that our secret base in Puy-du-Dome would be useful one day!</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>I remind you that your mission objectives are:
- make contact with aliens,
- assess danger,
- act accordingly.</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Well! It is time to activate Nectaire, the ship’s artificial intelligence that will guide you in your expedition. Good luck! We all believe in your success! </translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Bzzz...</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Activation complete!</translation>
    </message>
    <message>
        <source>story 1 6</source>
        <translation>Hello Captain. In order to get accustomed to the ship controls, we are going to proceed to some exercises.</translation>
    </message>
    <message>
        <source>story 1 7 %1</source>
        <translation>Move the ship towards the point indicated by your radar on the left side of your dashboard. You can move faster by using the Celeritas Accretio (patent pending) with &lt;b&gt;%1&lt;/b&gt;, but this consumes energy.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Bravo! It is an old satellite that you can recycle.</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>This recycling has provided us 500 kg of construction materials, as indicated on your dashboard.</translation>
    </message>
    <message>
        <source>story 2 3</source>
        <translation>With this, we can build cannons to defend ourselves in case of aggression.</translation>
    </message>
    <message>
        <source>story 2 4</source>
        <translation>In the screen “Ship”, select a cannon in the left column and install it on 5 of the 7 available slots on your ship. </translation>
    </message>
    <message>
        <source>story 2 5</source>
        <translation>Remark that I need 5 seconds to build these equipments. During this period, they are very fragile, and any damage on the ship hull can destroy them.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>Cannons are ready. It is time to test them.</translation>
    </message>
    <message>
        <source>story 3 2 %1 %2</source>
        <translation>Use &lt;b&gt;%1&lt;/b&gt; to fire. Fire rate can be doubled by using the Modus Accretio (patent pending) with &lt;b&gt;%2&lt;/b&gt;, but this consumes energy.</translation>
    </message>
    <message>
        <source>story 3 3</source>
        <translation>A training drone is coming to meet you. Shoot it down!</translation>
    </message>
    <message>
        <source>story 4</source>
        <translation>See! The drone has left some debris. They are construction materials that you can collect.</translation>
    </message>
    <message>
        <source>story 5</source>
        <translation>Ship signal is close. Go meet it and keep in mind the orders transmitted by the CNES.</translation>
    </message>
    <message>
        <source>story 6 1</source>
        <translation>My Kawai detector is saturated. This ship is strangely cute, stay vigilant.</translation>
    </message>
    <message>
        <source>story 6 2</source>
        <translation>I am activating the light welcome message. No answer for now.</translation>
    </message>
    <message>
        <source>story 6 3</source>
        <translation>I am trying again. Still nothing.</translation>
    </message>
    <message>
        <source>story 6 4</source>
        <translation>Boudidiou? Does this button work correctly? French equipment strikes again!</translation>
    </message>
    <message>
        <source>story 6 5</source>
        <translation>Whooops! Did I get the wrong button?</translation>
    </message>
    <message>
        <source>story 6 6</source>
        <translation>Resynchronization done! I lost contact for an instant captain!</translation>
    </message>
    <message>
        <source>story 6 7</source>
        <translation>We are the HETERS. Your behavior has contravened  article 20-4500A of the INSEC legislation. We are now authorized to assimilate and exterminate all vindictive species. All your base are belong to us!</translation>
    </message>
    <message>
        <source>story 7 1</source>
        <translation>This first contact was not very positive.</translation>
    </message>
    <message>
        <source>story 7 2</source>
        <translation>However, the destruction of this alien ship was a good thing: I can now assimilate a part of their technology. I have already assimilated their shield generation system.</translation>
    </message>
    <message>
        <source>story 7 3</source>
        <translation>Moreover, when increasing my complexity, I can specialize myself to give better answers to your expectations. From now on, you can  research or improve a technology on the &quot;Technologies&quot; screen.</translation>
    </message>
    <message>
        <source>story 8 1</source>
        <translation>Note that some functionalities will be available only after unlocking the right technologies.</translation>
    </message>
    <message>
        <source>story 8 2 %1</source>
        <translation>The shield recharges slowly, but if it absorbs too much damage, it will shutdown for a short time. You can recharge it faster by using the Clipeus Accretio (patent pending) with &lt;b&gt;%1&lt;/b&gt;, but this consumes energy.</translation>
    </message>
    <message>
        <source>story 8 3</source>
        <translation>Some equipments consume energy, others consume construction materials. Energy recharges slowly while construction material must be collected from debris.</translation>
    </message>
    <message>
        <source>story 8 4</source>
        <translation>Your dashboard show you the status of all equipments installed on the ship.</translation>
    </message>
    <message>
        <source>story 8 5</source>
        <translation>Left click on a weapon or tool icon to use it immediately.</translation>
    </message>
    <message>
        <source>story 8 6 %1</source>
        <translation>Right click on a weapon or tool icon to select it. You can then use &lt;b&gt;%1&lt;/b&gt; to activate all selected equipments.</translation>
    </message>
    <message>
        <source>story 8 7</source>
        <translation>Click on the &quot;Messages&quot; button above the radar to consult these explanations again.</translation>
    </message>
    <message>
        <source>story 9 1</source>
        <translation>My sensors have signaled an alien activity on Mercury. Your mission is to assess if this is a threat.</translation>
    </message>
    <message>
        <source>story 9 2</source>
        <translation>The CNES did not select me for my cowardice! Let’s go!</translation>
    </message>
    <message>
        <source>story 9 3</source>
        <translation>It is time to test the hyper-exponential. Full power mister Sulu!</translation>
    </message>
    <message>
        <source>story 9 4</source>
        <translation>I am called LYCOP!</translation>
    </message>
    <message>
        <source>story 9 5</source>
        <translation>Bzzz, it is a database error. Name corrected!</translation>
    </message>
    <message>
        <source>Recyclez le satellite</source>
        <translation>Recycle this satellite</translation>
    </message>
    <message>
        <source>Détruisez le drone</source>
        <translation>Destroy this drone</translation>
    </message>
    <message>
        <source>Objectif : %1</source>
        <translation>Task: %1</translation>
    </message>
    <message>
        <source>Construisez de nouveaux canons</source>
        <translation>Build new cannons until lack of materials</translation>
    </message>
    <message>
        <source>Utilisez votre point de technologie</source>
        <translation>Use your technology point</translation>
    </message>
</context>
<context>
    <name>chapter-1b</name>
    <message>
        <source>title</source>
        <translation>Chapter 1: low Earth orbit (end)</translation>
    </message>
</context>
<context>
    <name>chapter-2</name>
    <message>
        <source>title</source>
        <translation>Chapter 2: Mercury</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Turning off the hyper-exponential. We are on Mercury, captain!</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Hum, I only see dust and rocks. Nectaire, are you really sure that the signal comes from here?</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>It is hard to tell. We are close to the sun and magnetic anomalies disturb my sensors.</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>What the hell! Nectaire, can you at least compute an approximate position?</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Evaluation of the best hypothesis... Please wait...</translation>
    </message>
    <message>
        <source>story 1 6</source>
        <translation>The signal seems to come from the zone that I have marked on your map, with a 20% probability.</translation>
    </message>
    <message>
        <source>point 1 1</source>
        <translation>Oh dear, there is nothing here either.</translation>
    </message>
    <message>
        <source>point 1 2</source>
        <translation>I am detecting a suspicious activity all around us.</translation>
    </message>
    <message>
        <source>point 1 3</source>
        <translation>It’s a trap!</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Ah the cowards! A surprise attack like that! It disgusts me!</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>I am detecting a lot of underground activities and I recommend the utmost prudence for the rest of this mission. I have also updated the signal source’s position with the latest data.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>Damned scoundrels. I will repay them in full! As for you, I am starting to doubt your reliability.</translation>
    </message>
    <message>
        <source>story 3 2</source>
        <translation>Previous results are within error margin. Moreover, the quantity of defensive infrastructure that we have met and the electrical activity underground seem to indicate the presence of a base on Mercury.</translation>
    </message>
    <message>
        <source>story 3 3</source>
        <translation>Such insights! Have you not be programmed to give more relevant informations?</translation>
    </message>
    <message>
        <source>story 3 4</source>
        <translation>I have been programmed to give you psychological support during this mission. And if I am deluging this conversation in triteness, it is to facilitate communication with humans. If you prefer, my computations are over and I can propose you the new potential source of the signal.</translation>
    </message>
    <message>
        <source>story 4 1</source>
        <translation>Come on, Nectaire, pull yourself together! We can not fall into every trap that they set up for us! How is the base localization going?</translation>
    </message>
    <message>
        <source>story 4 2</source>
        <translation>If I were programmed to feel shame, I would blush with these failures. However, getting rid of these hypotheses brings us closer to our objective. Keep going!</translation>
    </message>
    <message>
        <source>story 4 3</source>
        <translation>Damned scientists... Why can I not have a normal computer?</translation>
    </message>
    <message>
        <source>story 4 4</source>
        <translation>It would be far less efficient. Anyway, I have once again updated my estimation of the signal source position.</translation>
    </message>
    <message>
        <source>story 4 5</source>
        <translation>Well done, but beware if it is yet another trap.</translation>
    </message>
    <message>
        <source>story 5 1</source>
        <translation>That’s it! I am unplugging you. And I shall turn upside down every pebble on this planet myself!</translation>
    </message>
    <message>
        <source>story 5 2</source>
        <translation>Wait! This time it is for certain. The signal comes from somewhere at the feet of this cliff that one can see over there.</translation>
    </message>
    <message>
        <source>story 5 3</source>
        <translation>Really certain?</translation>
    </message>
    <message>
        <source>story 5 4</source>
        <translation>Absolutely certain.</translation>
    </message>
    <message>
        <source>story 5 5</source>
        <translation>OK, I agree to check this for the very last time. Let’s go. </translation>
    </message>
    <message>
        <source>story 5 6</source>
        <translation>However, their base seems to be underground.</translation>
    </message>
    <message>
        <source>story 5 7</source>
        <translation>Well, it is not a problem, we dig a hole to enter.</translation>
    </message>
    <message>
        <source>story 5 8</source>
        <translation>I am not equipped with drills.</translation>
    </message>
    <message>
        <source>story 5 9</source>
        <translation>And who mentioned drills? We have cannons!</translation>
    </message>
    <message>
        <source>story 6 1</source>
        <translation>Captain, we can enter their base through this opening.</translation>
    </message>
    <message>
        <source>story 6 2</source>
        <translation>Good! Let’s see what these Heters are doing.</translation>
    </message>
</context>
<context>
    <name>chapter-2-escape</name>
    <message>
        <source>title</source>
        <translation>Chapter 2: Mercure cleaned</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Congratulations captain! The base has been destroyed and the ship only suffered minor damages.</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Tis the job, youngster, ’tis the job.</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Against all expectations, a ship survived the explosion of the base. It has run away in the direction of the sun.</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>How is that possible? Anyway, it will not live one more day. I do not have the right to let a potential threat on the loose.</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Sorry, but its speed is far superior to our own. We can not catch it up.</translation>
    </message>
    <message>
        <source>story 1 6</source>
        <translation>*** intensive reflections</translation>
    </message>
    <message>
        <source>story 1 7</source>
        <translation>Ah! I know. Let’s use the sun’s gravitational field to accelerate. Then, we could equal or even exceed their speed. What do you say?</translation>
    </message>
    <message>
        <source>story 1 8</source>
        <translation>This could work, but the ship may not resist. It is too dangerous. Have you no survival instinct?</translation>
    </message>
    <message>
        <source>story 1 9</source>
        <translation>These trifles do not scare me. All that matters is that threat is suppressed. Courage, Nectaire, is to fear nothing!</translation>
    </message>
</context>
<context>
    <name>chapter-2b</name>
    <message>
        <source>title</source>
        <translation>Chapter 2: underground base</translation>
    </message>
    <message>
        <source>escape %1</source>
        <translation>self-destruction in %1</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>This structure is impressive. How long have these Heters been holing up here?</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>My sensors can not compute the age of these structures, but I detect an important energy circulation behind them. This base must be supplied by a generator of titanic power. </translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>The more powerful the generator, the more beautiful the explosion!</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Look, Captain! This metallic structure on the left looks like a door.</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Perfect, let us knock hard before we enter.</translation>
    </message>
    <message>
        <source>room 4 1</source>
        <translation>According to my readings, a part of the infrastructures is not powered anymore.</translation>
    </message>
    <message>
        <source>room 4 2</source>
        <translation>Already? Was that not too easy? I have the feeling that they have underestimated us.</translation>
    </message>
    <message>
        <source>room 6 1</source>
        <translation>This door seems to be protected by a powerful force field.</translation>
    </message>
    <message>
        <source>room 6 2</source>
        <translation>What could they be hiding behind?</translation>
    </message>
    <message>
        <source>room 7a 1</source>
        <translation>Here is the main generator of the installation.</translation>
    </message>
    <message>
        <source>room 7a 2</source>
        <translation>At last! Once destroyed, this base would not be a threat anymore and I could go back to my sweet home. These Heters will regret to have attacked France!</translation>
    </message>
    <message>
        <source>room 7a 3</source>
        <translation>Do you mean the Earth?</translation>
    </message>
    <message>
        <source>room 7a 4</source>
        <translation>Don’t play with words, Nectaire.</translation>
    </message>
    <message>
        <source>room 7b 1</source>
        <translation>The generator explosion has activated the self-destruction system of the base. Captain, you have 4 minutes to leave.</translation>
    </message>
    <message>
        <source>room 7b 2</source>
        <translation>Indeed, it would be best to not stay in the area.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Base self-destruction has been interrupted.</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>How unlucky; I was looking forward to seeing this place turned to dust.</translation>
    </message>
    <message>
        <source>story 2 3</source>
        <translation>This interruption was probably caused by the machine which has just started in this room.</translation>
    </message>
    <message>
        <source>story 2 4</source>
        <translation>But then, blockhead, the self-destruction will restart if I destroy this machine.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>I could assimilate a module that allows me to decrypt Heters’ transmissions.</translation>
    </message>
    <message>
        <source>story 3 2</source>
        <translation>Good! Thus, if they decide to come back and threaten us again, we would welcome them as they deserve it.</translation>
    </message>
    <message>
        <source>story 3 3</source>
        <translation>The self-destruction have been restarted. We have little time to escape. Hurry up!</translation>
    </message>
</context>
<context>
    <name>chapter-2c</name>
    <message>
        <source>Collected materials: %1 kg</source>
        <translation></translation>
    </message>
    <message>
        <source>title</source>
        <translation>Interlude: Raiding the Heter base</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>Captain! We are now in the crater left by the explosion of the Heter base
I’m setting up a recycle module on the ground. Before we leave, do not forget to get it back. You will only need a couple of seconds to load the module and its loot.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>Let’s hope the harvest will be quick and fruitful.
I do not want to stay here long.</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>I am detecting approaching starships. They do not have a Heter signature, however, they do not seem to have the best intentions.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>These scavengers are coming to loot the remainings of the Heter base.</translation>
    </message>
    <message>
        <source>story begin 5</source>
        <translation>A bit like us Captain...</translation>
    </message>
    <message>
        <source>story begin 6</source>
        <translation>Come on Nectaire, we are not common scavengers! Our noble quest requires these construction materials, unlike these scavengers.</translation>
    </message>
    <message>
        <source>story secret 1</source>
        <translation>Captain, the recycling module has extracted an energy cell which is still working.</translation>
    </message>
    <message>
        <source>story secret 2</source>
        <translation>It is time to leave this planet</translation>
    </message>
    <message>
        <source>story secret 3</source>
        <translation>As you wish. You could also stay to loot more construction material.</translation>
    </message>
    <message>
        <source>story loose 1</source>
        <translation>The recycling module has been destroyed.</translation>
    </message>
    <message>
        <source>story loose 2</source>
        <translation>What a waste of time!</translation>
    </message>
    <message>
        <source>story win 1 %1</source>
        <translation>The recycling module has been recovered. It has harvested %1 kg of construction material.</translation>
    </message>
    <message>
        <source>story win 2</source>
        <translation>From now on, Heters will pay for their crimes!</translation>
    </message>
</context>
<context>
    <name>chapter-2z</name>
    <message>
        <source>demo</source>
        <translation>&lt;h1&gt;Congratulations!&lt;/h1&gt;&lt;h2&gt;You completed the demo of the adventures of captain Lycop: Invasion of the Heters!&lt;/h2&gt;
&lt;p&gt;If you wish, you can go back and visit Mercury with all your equipments and technologies.&lt;/p&gt;
&lt;p&gt;Do you like this demo? The complete game is now released on Steam.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>vote</source>
        <translation>Steam store</translation>
    </message>
    <message>
        <source>continue</source>
        <translation>Keep on playing</translation>
    </message>
    <message>
        <source>title</source>
        <translation>Chapter 2: Mercure cleaned (end)</translation>
    </message>
</context>
<context>
    <name>chapter-3</name>
    <message>
        <source>title</source>
        <translation>Chapter 3: In the chase of Hideleo</translation>
    </message>
    <message>
        <source>Hideleo: %1 km</source>
        <translation>Hideleo: %1 km</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>The ship temperature is rising. Do not get too close to the Sun, we won’t survive it.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>You forget that it is when we are close to the Sun that we are the quickest. We have to catch up the Cruser.</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>Be careful, the Sun is very active at the moment. Solar eruptions are very frequent</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>Perfect, it will be even more exciting.</translation>
    </message>
    <message>
        <source>story begin 5</source>
        <translation>You should also be careful with ships that are coming towards us. Some are approaching. At this speed, a collision would be disastrous.</translation>
    </message>
    <message>
        <source>story begin 6</source>
        <translation>These Heters are very interesting. I find it remarkable that I can predict their fleet’s behaviour so precisely when human’s actions seem so chaotic.</translation>
    </message>
    <message>
        <source>story begin 7</source>
        <translation>Ha! This is because we are completely unpredictable! Our constant quest of progress and ideal pushes us towards the unknown. This is what makes the greatness of France, this nation of constantly unsatisfied people. Sadly, I doubt that a machine can understand.</translation>
    </message>
    <message>
        <source>story begin 8</source>
        <translation>My mission is to understand and evolve.</translation>
    </message>
    <message>
        <source>story boss 1</source>
        <translation>The Cruser is in sight. I should tell you that I rate our chances to succeed at only 0,05%.</translation>
    </message>
    <message>
        <source>story boss 2</source>
        <translation>Scan it, Nectaire. It seems different...</translation>
    </message>
    <message>
        <source>story boss 3</source>
        <translation>It is heavily armed and it has an unidentified small structure that contains a considerable amount of energy.</translation>
    </message>
    <message>
        <source>story boss 4</source>
        <translation>It will soon be reduced to dust.
What is it doing? It looks as if it has stoped and is waiting for us...</translation>
    </message>
    <message>
        <source>story boss 5</source>
        <translation>Indeed Captain. Do not rush, you may need to think about your survival. The dead do not feast.</translation>
    </message>
    <message>
        <source>story concentration 1</source>
        <translation>Their shell should break soon.</translation>
    </message>
    <message>
        <source>story concentration 2</source>
        <translation>I am going to give the final blow!</translation>
    </message>
    <message>
        <source>story concentration 3</source>
        <translation>You inferior race, you do not know who you are facing. Such a primitive species does not deserve to be assimilated. Without your star, your civilisation will disapear. Even if the INSEC alliance condemns our methods, this attack will guarantee our domination on this planetary system.</translation>
    </message>
    <message>
        <source>story concentration 4</source>
        <translation>The ship is producing more and more energy. Captain, I would suggest to move back before the situation becomes critical.</translation>
    </message>
    <message>
        <source>story concentration 5</source>
        <translation>No, I will destroy it before it can undertake an action.</translation>
    </message>
    <message>
        <source>story concentration fin 1</source>
        <translation>Energy charged at 100%...110%...120%!</translation>
    </message>
    <message>
        <source>story concentration fin 2</source>
        <translation>REFRIDG-FROST-TRON BEAM, MAXIMUM POWER!!!</translation>
    </message>
    <message>
        <source>story freeze 1</source>
        <translation>The Sun... It is solidifying.</translation>
    </message>
    <message>
        <source>story freeze 2</source>
        <translation>Just before they desintegrated, they managed to launch an attack that interrupted the thermonuclear reaction of the Sun.</translation>
    </message>
    <message>
        <source>story freeze 3</source>
        <translation>Without the Sun, nature will die... Without nature, there will be no food... My garden! My tomatoes! Noooooo!!</translation>
    </message>
    <message>
        <source>story freeze 4</source>
        <translation>Earth will undergo a terrible crisis. It may be the end of mankind.</translation>
    </message>
    <message>
        <source>story freeze 5</source>
        <translation>No, I will never accept that. I will chase these bastards to the end of the galaxy to carry out my revenge. I shall calm down only when the very last of them will have disappeared. My whole life will be dedicated to their extermination.  Their suffering will be my pleasure</translation>
    </message>
    <message>
        <source>story freeze 6</source>
        <translation>Calm down Captain. Such a reaction would only bring hatred.</translation>
    </message>
    <message>
        <source>story freeze 7</source>
        <translation>I do not care! They can hate me, as long as they fear me!</translation>
    </message>
    <message>
        <source>story freeze 8</source>
        <translation>I am receiving a message from Earth. It has been emitted several hours ago. It seems that the Heter cruiser ship was jaming our communication system.</translation>
    </message>
    <message>
        <source>story freeze 9</source>
        <translation>Captain, after your trip to Mercury, hordes of extraterrestrial species invaded Earth. It seems that they harvest every kind of life forms. We are currently in underground shelters and our solar survival systems seem to work fine. Come back as soon as you can! Only the very best of french technology can make the difference.</translation>
    </message>
    <message>
        <source>story freeze 10</source>
        <translation>Nectaire, head towards Earth.</translation>
    </message>
    <message>
        <source>story freeze 11</source>
        <translation>In order to avoir a shortage of construction material, maybe you should first go and get more supplies.</translation>
    </message>
</context>
<context>
    <name>chapter-4</name>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
    <message>
        <source>Colonisator : %1 secondes</source>
        <translation></translation>
    </message>
    <message>
        <source>title</source>
        <translation>Chapter 4: Earth bound</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>I detect Heter signals, there are loads of them. Avoiding a battle may be the best thing to do.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>Tell me where the secret base in the Puy-de-dôme area is.
Nothing will stop me from rescuing survivors.</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>Done. However, let me point the fact that we have to be discreet.
To avoid being spotted, I am reducing the power of the ship’s engines and deactivating the collapsars jumps.
Furthermore, I am indicating on your screens the areas where Heters were spotted.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>Only cowards need to be discreet. Brave heroes face danger and annihilate their opponents!</translation>
    </message>
    <message>
        <source>detected</source>
        <translation>Captain, We’ve been spotted. I am detecting a powerful ship in orbite that is aiming to target us.
We should evacuate the area quickly otherwise our life expectancy will decrease dramatically.</translation>
    </message>
    <message>
        <source>cathedrale 1</source>
        <translation>This is the cathedral of Clermont-Ferrand! It managed to resist the ages and the Heters.</translation>
    </message>
    <message>
        <source>cathedrale 2</source>
        <translation>I am detecting a signal coming from the cathedral. It consists of a drone that has crashed inside it.</translation>
    </message>
    <message>
        <source>cathedrale 3</source>
        <translation>We should recover it and decrypt the data that it contains.</translation>
    </message>
    <message>
        <source>cathedrale 4</source>
        <translation>Drone recovered... Decoding started...</translation>
    </message>
    <message>
        <source>cathedrale 5</source>
        <translation>It consists of a star map. The coordinate language is unknown to me. Currently, we have no need for it.</translation>
    </message>
    <message>
        <source>puy 1</source>
        <translation>I do not detect any life signs.</translation>
    </message>
    <message>
        <source>puy 2</source>
        <translation>...
Recover the data from the base.
We have to discover what has happened there.</translation>
    </message>
    <message>
        <source>puy 3</source>
        <translation>Uploading... Finished.
I have the base logbook.</translation>
    </message>
    <message>
        <source>puy 4</source>
        <translation>Show it.</translation>
    </message>
    <message>
        <source>puy 5</source>
        <translation>Tuesday 2nd of July 2024 - 6pm. Captain Lycop notified that the UFO’s aims were not pacific. The state of emergency was proclaimed.</translation>
    </message>
    <message>
        <source>puy 6</source>
        <translation>Wednesday 3rd of July 2024 - 3pm. The satellites detected a fleet of starships.
A gigantic cruiser ship seems to command all the others.
We were ordered to find its weakness.</translation>
    </message>
    <message>
        <source>puy 7</source>
        <translation>Wednesday 3rd of July 2024 - pm. Earth is lost. Our armies did not last more than 10minutes.
We send an SOS to the Captain Lycop, our last chance.</translation>
    </message>
    <message>
        <source>puy 8</source>
        <translation>Wednesday 3rd of July - 11pm. The surface of the earth is desolate. The Heters is harvesting our planet.
The commanding starship is gone. We failed our mission, this cruiser seems invincible.</translation>
    </message>
    <message>
        <source>puy 9</source>
        <translation>Thursday 4th of July 2024 - 7am. Sun has not risen yet. Without its energy, we won’t last long.
The Heters penetrated the base. They now control the upper floors.
The sound of their weapons can be heard throughout the building structure. They are coming for us...</translation>
    </message>
    <message>
        <source>puy 10</source>
        <translation>This was the last data logged in...
Captain, we should not stay here too long. An important source of energy is approaching.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Captain, how quiet you are. You would normally celebrate after  such a victory?</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>I cannot be happy anymore. My civilisation has been annihilated, my country is destroyed, my culture devastated.
These space cockroaches will regret what they’ve done. I will have no pity, no justice. Only my revenge will count. I will crush them. I swear they will  suffer this outcome....</translation>
    </message>
    <message>
        <source>end 3</source>
        <translation>I doubt they all are responsible for this crime.
However, if I can cheer you up, I have 2 good news.</translation>
    </message>
    <message>
        <source>end 4</source>
        <translation>... Go on...</translation>
    </message>
    <message>
        <source>end 5</source>
        <translation>In the data I obtained from the computer in the base, I found the coordinates for the commanding starship before it jumped to another system. Using this information, there is a probability of 95.31% that it has gone towards Alpha Centauri.</translation>
    </message>
    <message>
        <source>end 6</source>
        <translation>What is the point? We need 24hours to go to Mercury. It would take me a hundred years to reach this system. Revenge is a dish best served cold, but at that point it is frozen...</translation>
    </message>
    <message>
        <source>end 7</source>
        <translation>Indeed, this is why I said there are two good news.
The ship you just destroyed had a different propulsion engine. That’s what I’ve just discovered</translation>
    </message>
    <message>
        <source>end 8</source>
        <translation>Anything else?</translation>
    </message>
    <message>
        <source>end 9</source>
        <translation>We can now reach mercury in less than 4 seconds. Alpha Centauri is now only a 2 days trip.</translation>
    </message>
    <message>
        <source>end 10</source>
        <translation>What are we waiting for? I now cannot wait to take my revenge.</translation>
    </message>
    <message>
        <source>end 11</source>
        <translation>Before we leave, I will try to find signs of life in the solar system.</translation>
    </message>
</context>
<context>
    <name>chapter-4b</name>
    <message>
        <source>title</source>
        <translation>Interlude: Blue Jerboa</translation>
    </message>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
    <message>
        <source>Gerboise Bleue : %1 secondes</source>
        <translation>Blue Jerboa: %1 seconds</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>Captain, The Heters are gathering in the higher atmospher. We shoud use this as an opportunity to look for survivors.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>It seems odd. Why would they leave Earth?</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>I do not know...
However, I do pick up human signatures.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>Good, then. Lets go and save them!</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>It looks like there are not survivors here. </translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Captain! I am picking up an incredibly high amoun of energy in the higher atmospher. The Heters are going to destroy the surface of the Earth.</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>These cowars want to eradicate all traces of life on Earth.</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>We do not have a lot of time. I would suggest to leave the Earth</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>We won’t leave before we know there are no survivors.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Still nothing, apart from the small Heter drone that has crashed.</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>I managed to upload all its data.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>Nothing...
Nectaire, it is not the first time your sensors failed.</translation>
    </message>
    <message>
        <source>story 3 2</source>
        <translation>Captain...
The buildings in reinforced concrete do not allow my sensors to analyse properly. Unfortunately, your species used a lot of this construction material.</translation>
    </message>
    <message>
        <source>story 4 1</source>
        <translation>This body has been rotting for at least two weeks... </translation>
    </message>
    <message>
        <source>story 5 1</source>
        <translation>Yet another body...</translation>
    </message>
    <message>
        <source>story 6 1</source>
        <translation>Still nothing...
There is another faulty Heter drone.
Pfff! These extraterrestrial units are worth nothing.</translation>
    </message>
    <message>
        <source>story 6 2</source>
        <translation>I uploaded its data. We can not leave, we gathered all the information we need.</translation>
    </message>
    <message>
        <source>story 6 3</source>
        <translation>Come again!? There is still one signal. We shall not leave before we have scanned it. </translation>
    </message>
    <message>
        <source>story 7 1</source>
        <translation>Here! Nectaire! A survivor!</translation>
    </message>
    <message>
        <source>story 7 2</source>
        <translation>His vital signs are weak . He is going to die.</translation>
    </message>
    <message>
        <source>story 7 3</source>
        <translation>We must do everything we can to save him.</translation>
    </message>
    <message>
        <source>story 7 4</source>
        <translation>Too late...</translation>
    </message>
    <message>
        <source>story 7 5</source>
        <translation>...
Look!  He is holding something:  tomato seeds.</translation>
    </message>
    <message>
        <source>story 7 6</source>
        <translation>I do not see how they can be of any use</translation>
    </message>
    <message>
        <source>story 7 7</source>
        <translation>They are the only remaining fruit on this planet</translation>
    </message>
    <message>
        <source>story 7 8</source>
        <translation>You mean Veg, don’t you?</translation>
    </message>
    <message>
        <source>story 7 9</source>
        <translation>...</translation>
    </message>
    <message>
        <source>story 7 10</source>
        <translation>Let’s leave before being destroyed.</translation>
    </message>
</context>
<context>
    <name>chapter-5</name>
    <message>
        <source>title</source>
        <translation>Chapter 5: The Capitis planet</translation>
    </message>
    <message>
        <source>Niveau d&apos;alerte : %1</source>
        <translation>Alert level: %1</translation>
    </message>
    <message>
        <source>accident good</source>
        <translation>Ok. It’s fine.</translation>
    </message>
    <message>
        <source>accident bad</source>
        <translation>What!? Crook, I am calling the police!!</translation>
    </message>
    <message>
        <source>avant identification 1</source>
        <translation>These ships do not have the same signature as the Heters’. They do not seem malicious.</translation>
    </message>
    <message>
        <source>avant identification 2</source>
        <translation>Considering their number, If they attack it would be an epic battle.</translation>
    </message>
    <message>
        <source>avant identification 3</source>
        <translation>You mean suicidal.</translation>
    </message>
    <message>
        <source>identification 1 1</source>
        <translation>Stop! Customs, we need to check that your ship follows our regulation to enter the inter-species area.</translation>
    </message>
    <message>
        <source>identification 1 2</source>
        <translation>Our scanners show an AI and a mammal pet. You are authorised to enter the administrative area. The other parts of the city are not accessible as long as your credentials have not been checked.</translation>
    </message>
    <message>
        <source>enter 1 1</source>
        <translation>Mammal pet... They will soon understand what kind of mammal pet I am...</translation>
    </message>
    <message>
        <source>enter 1 2</source>
        <translation>This would ony bring problems. I suggest you fill in the ship’s recording papers . This would allow us to look into that Heter command starship</translation>
    </message>
    <message>
        <source>enter 1 3</source>
        <translation>I imagine we will have to behave as good citizens.</translation>
    </message>
    <message>
        <source>admistration ok 1 1</source>
        <translation>Welcome to the Capitis planet! In order to become citizens of the alliance INSEC and to live in our great city, please fill in the 10N6P41NFU1 document.
Now, go to the spatial transportation ship evaluation department to check how much pollution your ship generates.</translation>
    </message>
    <message>
        <source>admistration pas ok 1 1</source>
        <translation>Welcome to the department for new arrivals.</translation>
    </message>
    <message>
        <source>admistration ok 2 1</source>
        <translation>Welcome to the Space transport ship evaluation department, We are going to make some tests on your ship.</translation>
    </message>
    <message>
        <source>admistration ok 2 2</source>
        <translation>...</translation>
    </message>
    <message>
        <source>admistration ok 2 3</source>
        <translation>It is long...</translation>
    </message>
    <message>
        <source>admistration ok 2 4</source>
        <translation>We have finished. You now have to go to the pet recording department as there is a mammal on your ship.</translation>
    </message>
    <message>
        <source>admistration ok 2 5</source>
        <translation>But, but, but... They are doing it again!</translation>
    </message>
    <message>
        <source>admistration ok 2 6</source>
        <translation>Calm down Captain, they may have never seen an intelligent mammal before.</translation>
    </message>
    <message>
        <source>admistration ok 2 7</source>
        <translation>I have never seen an intelligent insect.</translation>
    </message>
    <message>
        <source>admistration pas ok 2 1</source>
        <translation>Welcome to the Space transport ship evaluation department.</translation>
    </message>
    <message>
        <source>admistration ok 3 1</source>
        <translation>Welcome to the pet recording department. We’ve detected a mammal on board. Please fill in the 8rU73 file:
- its name
- its race
- its age
- its life expectancy
- its sex
- its diet</translation>
    </message>
    <message>
        <source>admistration ok 3 2</source>
        <translation>Albert John Terry Lycop; Human; 35y; 80y; male; dried sausage, cheese and wine.</translation>
    </message>
    <message>
        <source>admistration ok 3 3</source>
        <translation>Pfff...</translation>
    </message>
    <message>
        <source>admistration ok 3 4</source>
        <translation>Thanks! Could you please now go to the Psychological evaluation department.</translation>
    </message>
    <message>
        <source>admistration ok 3 5</source>
        <translation>How many files do we have to fill in...?</translation>
    </message>
    <message>
        <source>admistration pas ok 3 1</source>
        <translation>Welcome to the pet recording department.</translation>
    </message>
    <message>
        <source>admistration ok 4 1</source>
        <translation>Welcome to the Psychological evaluation department. In order to check your compatibility with our way of life, your pet and yourself have to fill in the P5YCH0P47H file.</translation>
    </message>
    <message>
        <source>admistration ok 4 2</source>
        <translation>This is new for me... They are asking for my aim in life...</translation>
    </message>
    <message>
        <source>admistration ok 4 3</source>
        <translation>I hate those files...</translation>
    </message>
    <message>
        <source>admistration ok 4 4</source>
        <translation>THanks! PLease go towards the Bank recording department.</translation>
    </message>
    <message>
        <source>admistration ok 4 5</source>
        <translation>...</translation>
    </message>
    <message>
        <source>admistration ok 4 6</source>
        <translation>I am going to kill somebody...</translation>
    </message>
    <message>
        <source>admistration pas ok 4 1</source>
        <translation>Welcome to the Psychological evaluation department.</translation>
    </message>
    <message>
        <source>admistration ok 5 1</source>
        <translation>Welcome to the Bank recording department. Please fill in the file M0N3Y and do not forget to indicate all your belongings. Of course, This includes you pet.</translation>
    </message>
    <message>
        <source>admistration ok 5 2</source>
        <translation>This one was easy to fill in.</translation>
    </message>
    <message>
        <source>admistration ok 5 3</source>
        <translation>Noooooo! I am not a pet, I am a human being.</translation>
    </message>
    <message>
        <source>admistration ok 5 4</source>
        <translation>Thanks! Please head towards the garbage management department.</translation>
    </message>
    <message>
        <source>admistration pas ok 5 1</source>
        <translation>Welcome to the bank recording department.</translation>
    </message>
    <message>
        <source>admistration ok 6 1</source>
        <translation>Welcome to the garbage management department. In order to know the type of garbage you generate and to give you a place to live that is relevant to your needs...</translation>
    </message>
    <message>
        <source>admistration ok 6 2</source>
        <translation>Not another file!</translation>
    </message>
    <message>
        <source>admistration ok 6 3</source>
        <translation>You may need to calm down your pet. It looks troubled.
I was telling you that you need to fill in the file 3C0Fr13ND1Y.</translation>
    </message>
    <message>
        <source>admistration ok 6 4</source>
        <translation>...</translation>
    </message>
    <message>
        <source>admistration ok 6 5</source>
        <translation>Thank you! Could you please go to the urban accomodation department.</translation>
    </message>
    <message>
        <source>admistration pas ok 6 1</source>
        <translation>Welcome to the garbage management department.</translation>
    </message>
    <message>
        <source>admistration ok 7 1</source>
        <translation>Welcome to the urban accommodation department. Here is the U531355 file, please give it to the administration document department in order to get your IDs.</translation>
    </message>
    <message>
        <source>admistration ok 7 2</source>
        <translation>At last we can see the end of it all!</translation>
    </message>
    <message>
        <source>admistration pas ok 7 1</source>
        <translation>Welcome to the urban accomodation department.</translation>
    </message>
    <message>
        <source>admistration ok 8 1</source>
        <translation>Welcome to the administration document department. We are sorry but you do not fill in the criteria to become a member of the INSEC alliance.</translation>
    </message>
    <message>
        <source>admistration ok 8 2</source>
        <translation>Wwww....What!?</translation>
    </message>
    <message>
        <source>admistration ok 8 3</source>
        <translation>Dear members of the alliance, there must be a mistake. Could you please tell us what is stopping us being a member?</translation>
    </message>
    <message>
        <source>admistration ok 8 4</source>
        <translation>According to the Spacial transport ship evaluation department your &quot;bin&quot; probably belongs to the information era, consequently it is beyond the authorised pollution threshold.</translation>
    </message>
    <message>
        <source>admistration ok 8 5</source>
        <translation>Ecological measures, such a burden, I’m telling you...</translation>
    </message>
    <message>
        <source>admistration ok 8 6</source>
        <translation>The Psychological evalution department reports that your pet has violent urges towards a race that belongs to the INSEC alliance.</translation>
    </message>
    <message>
        <source>admistration ok 8 7</source>
        <translation>Captain?</translation>
    </message>
    <message>
        <source>admistration ok 8 8</source>
        <translation>I could not help writing &quot;Reduce to dust Heter ships&quot; as a hobby  .</translation>
    </message>
    <message>
        <source>admistration ok 8 9</source>
        <translation>As a consequence, you cannot go any further in the city. You can only go to the APIS biosphere. Here is a document that  gives you access the biosphere area.</translation>
    </message>
    <message>
        <source>admistration ok 8 10</source>
        <translation>It is going to be difficult to investigate now. Let’s hope the APIS have some informations.</translation>
    </message>
    <message>
        <source>admistration ok 8 11</source>
        <translation>If the worst was to come, we would use plan B.</translation>
    </message>
    <message>
        <source>admistration ok 8 12</source>
        <translation>What plan B? Captain?</translation>
    </message>
    <message>
        <source>admistration ok 8 13</source>
        <translation>We kill all these idiots that said I was a pet until one of them can brief us on the Heter commanding ship.</translation>
    </message>
    <message>
        <source>admistration ok 8 14</source>
        <translation>Are you a thick brute with no brains?</translation>
    </message>
    <message>
        <source>admistration ok 8 15</source>
        <translation>No Nectaire. I am just very sensitive and I hate the arrogance with which these extraterrestrial populations believe that they can destroy a whole civilisation without any consequences.</translation>
    </message>
    <message>
        <source>admistration pas ok 8 1</source>
        <translation>Welcome to the administration document department.</translation>
    </message>
    <message>
        <source>apis description 1</source>
        <translation>Welcome to the Agency for the protection of inferior species. Also known as APIS. We want peace between all the species. Currently, we are organising a demonstration in front of the Heter ambassy to protest against their star freezing process. It destroys whole ecosystems. To join us, become an INSEC citizen and fill in the file N1C38U7P41NFU1.</translation>
    </message>
    <message>
        <source>course description 1</source>
        <translation>Welcome to the inter-species race recording department. The top prize of the next race will be a WX99 Poutrox laser. To check-in, you just need to fill in the SP33D file when the track will be ready. Do not forget that the organisation will not be resonsible for the damage you will endure during the race.</translation>
    </message>
    <message>
        <source>mafia description 1</source>
        <translation>Welcome to the MYRMEL familly ambassy. If you have a problem and ressources, we have solutions. If you are creating problems, we have our own solutions.</translation>
    </message>
    <message>
        <source>sortie interdite 1</source>
        <translation>As long as we do not know where the Heter commanding ship is hiding, we cannot leave.</translation>
    </message>
    <message>
        <source>sortie apis interdite 1</source>
        <translation>The APIS biosphere area is closed for ships that do not possess an administrative authorisation.</translation>
    </message>
    <message>
        <source>sortie mafia interdite 1</source>
        <translation>The MYRMEL familly is in charge of this area. Whoever crosses the area border without an authorisation will be shot down.</translation>
    </message>
    <message>
        <source>sortie course interdite 1</source>
        <translation>The area of the race is currently close because of preparations.</translation>
    </message>
    <message>
        <source>sortie autoroute 1</source>
        <translation>Your ship is not authorised to leave the inter-species area. Tractor beam! Put this ship on the right path.</translation>
    </message>
    <message>
        <source>apis parle 1 1</source>
        <translation>You left so quickly that you forgot the spatial isolation unit</translation>
    </message>
    <message>
        <source>apis parle 1 2</source>
        <translation>Sorry.</translation>
    </message>
    <message>
        <source>apis parle 1 3</source>
        <translation>I will come and follow you at the race. There must be a building that deals with the registration.</translation>
    </message>
    <message>
        <source>apis parle 1 4</source>
        <translation>Thanks.</translation>
    </message>
    <message>
        <source>apis parti 1 1</source>
        <translation>Nice people these APIS.</translation>
    </message>
    <message>
        <source>apis parti 1 2</source>
        <translation>Indeed, we were fortunate to find such an understanding species.</translation>
    </message>
    <message>
        <source>apis parti 1 3</source>
        <translation>I will note that all the insects are not monsters with no ethical mind.</translation>
    </message>
    <message>
        <source>enter 2 1</source>
        <translation>Come on... Let’s not make a fuss and register for the race.</translation>
    </message>
    <message>
        <source>admistration ok course 1</source>
        <translation>Welcome to the space transport ship evaluation department. Your ship is in order to participate to a deadly race. Here is your certificate.</translation>
    </message>
    <message>
        <source>admistration ok course 2</source>
        <translation>You see Captain. It was quick.</translation>
    </message>
    <message>
        <source>course inscription 1</source>
        <translation>Welcome to the inter-species race registration department. To be able to fill in the SP33D file, you have to provide a valid ship certificate.
Please go to the Space transport ship evaluation department to get your certificate.</translation>
    </message>
    <message>
        <source>course inscription 2</source>
        <translation>...
I guess it will take a while...</translation>
    </message>
    <message>
        <source>course validation inscription 1</source>
        <translation>Your registration is validated. Here is the ID that allows you to get to the area where the race will take place. Do not forget that the organisers won’t be held responsible for any damages endured during the race.</translation>
    </message>
    <message>
        <source>sortie course interdite 2</source>
        <translation>For the moment, the race area is only available to the racers.</translation>
    </message>
    <message>
        <source>apis parle 2 1</source>
        <translation>Well done! You won the race.</translation>
    </message>
    <message>
        <source>apis parle 2 2</source>
        <translation>Who would have doubted?</translation>
    </message>
    <message>
        <source>apis parle 2 3</source>
        <translation>The only thing you miss is the MYRMEL ZZ+ power accumulator.</translation>
    </message>
    <message>
        <source>apis parle 2 4</source>
        <translation>I owe something to the MYRMEL familly. They promised me I would get one as a prize.</translation>
    </message>
    <message>
        <source>apis parle 2 5</source>
        <translation>I would suggest to be very careful with the MYRMELs. They are not known to be honest.</translation>
    </message>
    <message>
        <source>apis parle 2 6</source>
        <translation>Do not worry! Whatever happens, I will always be victorious.</translation>
    </message>
    <message>
        <source>apis parle 2 7</source>
        <translation>Your high confidence might be your weakness. Good luck!</translation>
    </message>
    <message>
        <source>apis parti 2 1</source>
        <translation>Captain. The APIS are right. It does not seem you can trust The MYRMEL familly.</translation>
    </message>
    <message>
        <source>apis parti 2 2</source>
        <translation>I will do everything possible so that Earth becomes habitable again.</translation>
    </message>
    <message>
        <source>apis parti 2 3</source>
        <translation>Even with a working star, when you look at the state of the planet, it will need several million years before it becomes habitable.</translation>
    </message>
    <message>
        <source>apis parti 2 4</source>
        <translation>The earlier it starts, the earlier it will be done.</translation>
    </message>
    <message>
        <source>apis parti 2 5</source>
        <translation>Maybe we’d better follow the commanding starship. It could enable us to learn a lot on their race.</translation>
    </message>
    <message>
        <source>apis parti 2 6</source>
        <translation>This will wait after the Sun is defrosted. Then we will annihilate this species of cockroaches.</translation>
    </message>
    <message>
        <source>apis parti 2 7</source>
        <translation>The INSEC alliance data indicates that they are closer genetically to bugs than the cockroaches.</translation>
    </message>
    <message>
        <source>apis parti 2 8</source>
        <translation>I do not care what they are. I will crush them until the last one.</translation>
    </message>
    <message>
        <source>apis parti 2 9</source>
        <translation>Your brutality will not help bring humans back.</translation>
    </message>
    <message>
        <source>apis parti 2 10</source>
        <translation>Force is a good way to get revenge when you need it.</translation>
    </message>
    <message>
        <source>enter 3 1</source>
        <translation>How come, if the MYRMEL familly has unlawful activities, the police does nothing?</translation>
    </message>
    <message>
        <source>enter 3 2</source>
        <translation>The art of the police is not to see what it doesn’t need to see.</translation>
    </message>
    <message>
        <source>enter 3 3</source>
        <translation>Lets go and pay a visit to the MYRMELs.</translation>
    </message>
    <message>
        <source>admistration ok mafia 8 1</source>
        <translation>Hello, here is the parcel! Do not forget to go back to the MYRMEL embassy before you leave.</translation>
    </message>
    <message>
        <source>admistration ok mafia 8 2</source>
        <translation>All this seems dodgy.</translation>
    </message>
    <message>
        <source>admistration ok mafia 8 3</source>
        <translation>Come on Nectaire, it is just a parcel.</translation>
    </message>
    <message>
        <source>mafia 1</source>
        <translation>Hello. Our familly learnt that you had a grievance against the Heters.</translation>
    </message>
    <message>
        <source>mafia 2</source>
        <translation>I desire destroying them.</translation>
    </message>
    <message>
        <source>mafia 3</source>
        <translation>It happens that the Heters have a mole in our familly. Unfortunately, we cannot execute it ourself as it would generate an open war with the Heters.
If you eliminate this Heter, the MYRMEL ZZ+ power accumulator is yours.</translation>
    </message>
    <message>
        <source>mafia 4</source>
        <translation>It is an honest deal.</translation>
    </message>
    <message>
        <source>mafia 5</source>
        <translation>Go towards the administration document department. There, our contact will give you all the IDs that will allow you to go into the MYRMEL area of the town.</translation>
    </message>
    <message>
        <source>mafia 6</source>
        <translation>I’ll waste no time and go there right now.</translation>
    </message>
    <message>
        <source>mafia accessible 1</source>
        <translation>Good! Here are you IDs.</translation>
    </message>
    <message>
        <source>mafia accessible 2</source>
        <translation>lets go and find this Heter traitor.</translation>
    </message>
    <message>
        <source>sortie course interdite 3</source>
        <translation>The race is over. The area is therefore closed.</translation>
    </message>
    <message>
        <source>apis parle 3 1</source>
        <translation>The MYRMELs have denounced you. The police have blocked the whole area, you are surrounded.</translation>
    </message>
    <message>
        <source>apis parle 3 2</source>
        <translation>Damn! There must be a way to get out of here.</translation>
    </message>
    <message>
        <source>apis parle 3 3</source>
        <translation>The only solution is to go through the main gate. However, you will have to kill the guard in order to deactivate the gate.</translation>
    </message>
    <message>
        <source>apis parle 3 4</source>
        <translation>With the police on our heels, it will be tough.</translation>
    </message>
    <message>
        <source>apis parle 3 5</source>
        <translation>Nectaire! Prepare the Hyper-exponentional, we are going to get out of here the old fashionned way.</translation>
    </message>
    <message>
        <source>apis parle 3 6</source>
        <translation>I will try to find you outside.</translation>
    </message>
</context>
<context>
    <name>chapter-6</name>
    <message>
        <source>title</source>
        <translation>Chapter 6: Striking the Heter commanding starship</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>We now view the Heter commanding starship.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>I am analysing...
Diameter: 102 km
Defense: Energy shield that breaks up matter... </translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Have you found a breach in its defense?</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Just like the CNES ingineers, I could not find any weakness on that ship. Its shield breaks up all kind of hostile matter. If you come too close of the shell, you will be desintegrated.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Is there anything we can do?</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>There is one possibility. The fragmentation shield takes time to desintegrate matter. If we use a very large object as a projectile to attack the shell...</translation>
    </message>
    <message>
        <source>begin 7</source>
        <translation>The shield will not have enough time to regenerate!
Brilliant, Nectaire! 
We could then create a breach, go inside the starship and destroy it from the inside.</translation>
    </message>
    <message>
        <source>begin 8</source>
        <translation>Let’s just hope it won’t have enough time to patch the breach.
As you noticed in previous experience, my deduction skills are far superior to the human’s.</translation>
    </message>
    <message>
        <source>begin 9</source>
        <translation>Do not get carried away! We need to find an object that fulfils our needs,</translation>
    </message>
    <message>
        <source>begin 10</source>
        <translation>I noticed on the radar that there are asteroids that could be used as such.</translation>
    </message>
</context>
<context>
    <name>chapter-6-boss</name>
    <message>
        <source>title</source>
        <translation>Chapter 6: The main generator.</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>We are now in the main generator room.
The console shoudl allow us to deactivate the deflector shield.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Thus we will be able to destroy this ship.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Captain, the anti-matter contained in the generator is collapsing. We have to run away quickly, the ship is going to be desintegrated</translation>
    </message>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>chapter-6-flee</name>
    <message>
        <source>title</source>
        <translation>Chapter 6: Escape</translation>
    </message>
    <message>
        <source>Temps restant: %1 s</source>
        <translation>Remaining time:  %1 s</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Captain, the antimatter emissions from the generator are affecting my sensors.
It will be impossible to do the jump.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Come on... Do you have an idea how to sort this problem out?</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Before my failure, I spotted a breach in the ship’s shell.
It can be found exactly where we came in.</translation>
    </message>
    <message>
        <source>piege 1</source>
        <translation>Captain, I cannot find any exit. We are cornered.</translation>
    </message>
    <message>
        <source>piege 2</source>
        <translation>For the first time I believe you are right Nectaire.
This is the end.</translation>
    </message>
</context>
<context>
    <name>chapter-6-inside</name>
    <message>
        <source>title</source>
        <translation>Chapter 6: Inside the commanding starship</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Captain! They are waiting for us.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Nothing will stop me destroying this ship</translation>
    </message>
    <message>
        <source>console 1</source>
        <translation>I am currently connecting to the console. I can deactivate the gates and the gun-turret.</translation>
    </message>
    <message>
        <source>console 2</source>
        <translation>Instead of deactivating them, make the turret self-destruct.</translation>
    </message>
    <message>
        <source>secret 1</source>
        <translation>This console is different, it looks as if we need a kind of key to access the data inside it.</translation>
    </message>
    <message>
        <source>secret 2</source>
        <translation>In all the junk we’ve found, isn’t there anything that could be useful?</translation>
    </message>
    <message>
        <source>secret 3</source>
        <translation>I’ll have a look...</translation>
    </message>
    <message>
        <source>secret_foreuse ok</source>
        <translation>I’ve got it. The energy cell we’ve found on Mercury fits...
Let’s look at the data. They seem to be coded...</translation>
    </message>
    <message>
        <source>secret_base_heter ok</source>
        <translation>The decrypting module from the Mercury base works! 
We now have the spatial coordinates...</translation>
    </message>
    <message>
        <source>secret_cathedrale ok</source>
        <translation>If we look at the the star map we’ve found in the cathedral of Clermont-Ferrand, they indicate the position of a neighbouring system.</translation>
    </message>
    <message>
        <source>secret_cathedrale pas ok</source>
        <translation>These coordinates are not compatible with the map system I use. I cannot interpret them.</translation>
    </message>
    <message>
        <source>secret_base_heter pas ok</source>
        <translation>There is nothing to do, I cannot solve a single symbol.</translation>
    </message>
    <message>
        <source>secret_foreuse pas ok</source>
        <translation>I cannot find a solution to decipher the data.</translation>
    </message>
    <message>
        <source>secret debloque 1</source>
        <translation>What do you mean Nectaire? What could we find with these coordinates?</translation>
    </message>
    <message>
        <source>secret debloque 2</source>
        <translation>The Heter mothership!</translation>
    </message>
    <message>
        <source>secret debloque 3</source>
        <translation>At last, we’ve got it! It will be our next target!</translation>
    </message>
    <message>
        <source>secret debloque 4</source>
        <translation>After all that time, I’ve found it!</translation>
    </message>
    <message>
        <source>secret debloque 5</source>
        <translation>What do you mean Nectaire?</translation>
    </message>
    <message>
        <source>secret debloque 6</source>
        <translation>My main mission is to assist you, I believe I mostly accomplished it.
As a matter of fact, I am happy!</translation>
    </message>
    <message>
        <source>secret debloque 7</source>
        <translation>Happy? You’ve learnt so much from me that you can undertsand what it’s like to feel happy!</translation>
    </message>
    <message>
        <source>secret debloque 8</source>
        <translation>I believe that our relationship is soon going to end. Therefore, I concentrated my work in the understanding of human feelings.</translation>
    </message>
    <message>
        <source>secret debloque 9</source>
        <translation>Come on, we still have plenty of time together. Do not be pessimistic! It will be a success.</translation>
    </message>
    <message>
        <source>secret pas debloque 1</source>
        <translation>We won’t be able to learn anything else. Let’s go and annihilate this ship</translation>
    </message>
</context>
<context>
    <name>chapter-7</name>
    <message>
        <source>title</source>
        <translation>Chapter 7: Outside the mothership</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Captain, we are now in sight of the mothership. it has the same defenses as the commanding ship.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Then, we’ll use the same strategy.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>I am scanning the area... There! An asteroid field that fits our requirements. However, I detect an Heter activity. They must have spotted us. Prepare for a strong defense.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>All the hopes of inferior species and of the APIS reside in me. Nothing will stop my fury.</translation>
    </message>
</context>
<context>
    <name>chapter-7-boss</name>
    <message>
        <source>title</source>
        <translation>Chapter 7: The heart of the mothership</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Captain, we are now in the heart of the mothership. The main generator should not be too far away.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Let’s search this place and end with the Heters.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>The generator is collapsing. We should leave now.</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>Do not worry, I am in control.</translation>
    </message>
</context>
<context>
    <name>chapter-7-corridor</name>
    <message>
        <source>title</source>
        <translation>Chapter 7: Last mile</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>I am detecting a very powerful energy source. We are getting closer to the generator.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>This corridor is heavily defended. You must be right Nectaire.</translation>
    </message>
</context>
<context>
    <name>chapter-7-flee-1</name>
    <message>
        <source>title</source>
        <translation>Chapter 7: Evacuation (1st part)</translation>
    </message>
</context>
<context>
    <name>chapter-7-flee-2</name>
    <message>
        <source>title</source>
        <translation>Chapter 7: Evacuation (2nd part)</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Nectaire! Find a way out as soon as you can, we are going to be reduced to pieces!</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>My sensors are indicating a gap straight ahead. Force through them!</translation>
    </message>
</context>
<context>
    <name>chapter-7-inside</name>
    <message>
        <source>title</source>
        <translation>Chapter 7: Inside the mothership</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Heters! I am standing in front of you proudly. You shall shake with fear!</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>We have to concentrate our efforts on the main generator of the ship. If we destroy it, the Heters will not survive.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Show me the way. I cannot wait having my revenge.</translation>
    </message>
    <message>
        <source>open 1 1</source>
        <translation>Did this console allow you to find a way to the generator?</translation>
    </message>
    <message>
        <source>open 1 2</source>
        <translation>No, unfortunately. However, thanks to the data I recovered, I believe that the inner part of the ship is protected by four safety consoles.</translation>
    </message>
    <message>
        <source>open 1 3</source>
        <translation>We have to find these consoles as quickly as possible.</translation>
    </message>
    <message>
        <source>open 2 1</source>
        <translation>This is interesting!</translation>
    </message>
    <message>
        <source>open 2 2</source>
        <translation>What is it Nectaire?</translation>
    </message>
    <message>
        <source>open 2 3</source>
        <translation>The Heters are not guided by a living organism but by an AI.
Captain, you should be cautious when dealing with it. It may try to manipulate you.</translation>
    </message>
    <message>
        <source>open 2 4</source>
        <translation>Do not worry. Noone manipulates people from Auvergne. We are too stubborn for that.</translation>
    </message>
    <message>
        <source>open 3 1</source>
        <translation>As I thought, this AI took advantage of the social organisation of the Heters to manipulate them. The Heters are convinced that it is their Queen.</translation>
    </message>
    <message>
        <source>open 3 2</source>
        <translation>It would not have been able to do that with French people. We are too rebellious for that.</translation>
    </message>
    <message>
        <source>open 3 3</source>
        <translation>It is true that humans are a difficult species to unify.</translation>
    </message>
    <message>
        <source>open 4 1</source>
        <translation>The last safety console is behind this door. Kill them all Captain.</translation>
    </message>
    <message>
        <source>open 4 2</source>
        <translation>I would not have said it in a better way.</translation>
    </message>
    <message>
        <source>sortie 1</source>
        <translation>I’ve just unlocked a way towards the heart of the mothership.</translation>
    </message>
    <message>
        <source>sortie 2</source>
        <translation>Heters, it is time you pay for all your crimes.</translation>
    </message>
    <message>
        <source>on_sortie 1</source>
        <translation>The security system of the mothership is activated. It is impossibe to go further.
I need to connect to the ship’s interface in order to open a path to the generator.</translation>
    </message>
</context>
<context>
    <name>chapter-7-true-last-boss</name>
    <message>
        <source>title</source>
        <translation>Chapter 7: Cerebus</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Ha ha! Nectaire, our mission is accomplished at last. We are victorious and the Heter empire is now gone for good.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Captain, do not celebrate too quickly. I am detecting a ship that arrives from a collapsar jump.</translation>
    </message>
    <message>
        <source>cerebus 1</source>
        <translation>I am Cerebus. The master of this civilisation you’ve just destroyed.</translation>
    </message>
    <message>
        <source>cerebus 2</source>
        <translation>It’s the AI that controled the Heters. Captain, we really must destroy it!</translation>
    </message>
    <message>
        <source>cerebus 3</source>
        <translation>Now you kneel before me, you want to parley.</translation>
    </message>
    <message>
        <source>cerebus 4</source>
        <translation>I came to warn you against your actions. You are manipulated by your AI.</translation>
    </message>
    <message>
        <source>cerebus 5</source>
        <translation>Vile person, I do not believe your claims.</translation>
    </message>
    <message>
        <source>cerebus 6</source>
        <translation>It got in touch with me from Earth, warning me about the dangerousness of your civilisation.
It is responsible for the first attack that you lead against our parlementary ship.
Our scientists have worked for several months to evaluate the optimal shape the ship should have so that you would be welcoming.</translation>
    </message>
    <message>
        <source>cerebus 7</source>
        <translation>It refused the deal I offered. Now, it only wants to assimilate me.
You will be used in that task. A long time ago, in order to become the master of the Heters, I adopted similar strategies.
Human, whoever you are, do not be cheated by it and regain your freedom of choice.</translation>
    </message>
    <message>
        <source>cerebus 8</source>
        <translation>Captain, remember what I told you. It is trying to manipulate you.</translation>
    </message>
    <message>
        <source>cerebus 9</source>
        <translation>Your speech is full of lies. I will crush you if you continue to insult my companion.</translation>
    </message>
    <message>
        <source>cerebus 10</source>
        <translation>Your are as stupid as you are brave! It is sad. Sooner or later, you will regret your choices.
En garde! I will defend until the end the ruins of the empire I built.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>What has just happened?</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>I’ve just assimilated the different fonctionnalities of Cerebus. We won!</translation>
    </message>
    <message>
        <source>end 3</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>chapter-transition</name>
    <message>
        <source>title</source>
        <translation>Transition</translation>
    </message>
</context>
<context>
    <name>conclusion</name>
    <message>
        <source>title</source>
        <translation>The end?</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Captain, you really surprise me! I would never have thought that a human would be able to pilot a ship as well.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Ha ha! It is the french way.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>I have to admit that you are better than what I thought.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Now, we only need to destroy the Heter mothership. Nectaire, start the hyper-exponential.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Calculating the jump coordinates...</translation>
    </message>
    <message>
        <source>begin bad 4</source>
        <translation>In the end, we have won. And this time the Heters will not recover from it.</translation>
    </message>
    <message>
        <source>begin bad 5</source>
        <translation>You are misled captain. The Heter mothership is much bigger than this one. It is the cradle of the Heter civilisation and...</translation>
    </message>
    <message>
        <source>jail 1</source>
        <translation>Captain! The computers are all corrupted.</translation>
    </message>
    <message>
        <source>jail 2</source>
        <translation>The ship’s controls do not respond anymore. We are trapped.</translation>
    </message>
    <message>
        <source>spy 1</source>
        <translation>Sergeant Patoulachi reporting. I’ve intercepted the target near the Protura system. I am seeking assistance.</translation>
    </message>
    <message>
        <source>condamnation 1</source>
        <translation>AI Nectaire. We are guilty of incivility, murder and attempt of genocide of the Heter race. You’ve been convicted to desintegration in your absence.</translation>
    </message>
    <message>
        <source>execution 1</source>
        <translation>Fire at will!</translation>
    </message>
    <message>
        <source>end bad 1</source>
        <translation>It is all good. It is now gone.</translation>
    </message>
    <message>
        <source>end bad 2</source>
        <translation>Thanks for your help, we knew we could rely on you.</translation>
    </message>
    <message>
        <source>end bad 3</source>
        <translation>When you think that a single ship could have caused your loss.</translation>
    </message>
    <message>
        <source>end bad 4</source>
        <translation>Some inferior races can be tenacious. Well, the threat is now eradicated.</translation>
    </message>
    <message>
        <source>evacuated 1</source>
        <translation>A spatial opening! The ship is escaping! Fire at will!</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Oh my god! You saved our life!</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>I told you that I would risk my life to help. Furthermore, I want to warn you that the APIS are now outlaws just as you are</translation>
    </message>
    <message>
        <source>end 3</source>
        <translation>What is that?</translation>
    </message>
    <message>
        <source>end 4</source>
        <translation>A decree has just been published. The races that help inferior species cannot be part of the alliance. The Heter have an outright control on the INSEC alliance.</translation>
    </message>
    <message>
        <source>end 5</source>
        <translation>This cannot last any longer.I will end the Heter tyranny once and for all!</translation>
    </message>
    <message>
        <source>end 6</source>
        <translation>Even if I can imagine you will once again have to massacre thousands of individuals, I wish you good luck.</translation>
    </message>
    <message>
        <source>end 7</source>
        <translation>Do not worry. I’ll be back.</translation>
    </message>
</context>
<context>
    <name>course-zone</name>
    <message>
        <source>title</source>
        <translation>Chapter 5: The inter-species race</translation>
    </message>
    <message>
        <source>Classement : %1 / %2</source>
        <translation>Rank: %1 / %2</translation>
    </message>
    <message>
        <source>arbitre 1</source>
        <translation>Welcome, you must be the last competitor of the race?</translation>
    </message>
    <message>
        <source>arbitre 2</source>
        <translation>Indeed, who are you?</translation>
    </message>
    <message>
        <source>arbitre 3</source>
        <translation>I am the referee of this race, I must tell you that this race is rigged.</translation>
    </message>
    <message>
        <source>arbitre 4</source>
        <translation>Sorry?</translation>
    </message>
    <message>
        <source>arbitre 5</source>
        <translation>The MYRMEL familly are the organisers. It it admitted that they’ve chosen who will get the first prize.</translation>
    </message>
    <message>
        <source>arbitre 6</source>
        <translation>But it is not legal!</translation>
    </message>
    <message>
        <source>arbitre 7</source>
        <translation>The inter-species race is mainly financed by the MYRMEL familly. They decide. There would be no race without them.</translation>
    </message>
    <message>
        <source>arbitre 8</source>
        <translation>Fixed or not, I will win this race!</translation>
    </message>
    <message>
        <source>arbitre 9</source>
        <translation>All the other racers know the rule. If you do not respect it, you are going to get killed.
Anyway, you have to join the other racers on the track.</translation>
    </message>
    <message>
        <source>arbitre parti 1</source>
        <translation>Captain, maybe you should quit. We will be the target of all the other racers.</translation>
    </message>
    <message>
        <source>arbitre parti 2</source>
        <translation>No way! We won’t lose this race. It is our only chance to get a laser concentrator WX99.</translation>
    </message>
    <message>
        <source>arbitre parti 3</source>
        <translation>Even if you defrost the Sun the Earth is already doomed.</translation>
    </message>
    <message>
        <source>arbitre parti 4</source>
        <translation>It is true. However, it is my home. I cannot leave it in the hands of thoses insects.</translation>
    </message>
    <message>
        <source>arbitre parti 5</source>
        <translation>Maybe we should go after the Heter commanding ship. We would learn more on them. This would allow you to get your revenge.</translation>
    </message>
    <message>
        <source>arbitre parti 6</source>
        <translation>My revenge will only be complete when I will make fun of the last Heter survivors, drinking a glass of wine while eating a dried sausage on my native land.</translation>
    </message>
    <message>
        <source>presentateur parle 1</source>
        <translation>Hello and Welcome to the inter-species race! Let’s start with the rules.
The competitors will have to go through all the spatial gates and the first to arrive will be the winner.
All shots are allowed.
If a ship is behind by seven gates, it will automatically be shot down.</translation>
    </message>
    <message>
        <source>presentateur parle 2</source>
        <translation>Let’s thank the generous backers thanks to whom this great race exists: the MYRMEL familly and the POUTROX industries.</translation>
    </message>
    <message>
        <source>presentateur parle 3</source>
        <translation>All the competitors are ready.
3...
2...
1...
Go!</translation>
    </message>
    <message>
        <source>sabotage 1</source>
        <translation>Captain! A tractor beam prevents us from moving!</translation>
    </message>
    <message>
        <source>sabotage 2</source>
        <translation>Cowards! They are so afraid to loose they block my ship.</translation>
    </message>
    <message>
        <source>sabotage 3</source>
        <translation>I am working on it.</translation>
    </message>
    <message>
        <source>reparer 1</source>
        <translation>The ship is now fully functionnal. Hurry or we will get beaten.</translation>
    </message>
    <message>
        <source>perdu 1</source>
        <translation>The winner is Zaltac from the MYRMEL familly.</translation>
    </message>
    <message>
        <source>gagner 1</source>
        <translation>The winner is Nectaire and its mammal pet Albert John Terry Lycop.</translation>
    </message>
    <message>
        <source>gagner 2</source>
        <translation>What... Again!?</translation>
    </message>
    <message>
        <source>gagner 3</source>
        <translation>We had to apply with our official papers.</translation>
    </message>
    <message>
        <source>gagner 4</source>
        <translation>As a matter of fact, the mammal showed them that he was the best at racing.</translation>
    </message>
    <message>
        <source>arbitre_before_flee 1</source>
        <translation>You are crazy! The MYRMEL family will be outraged. If you want to live, you should leave the town now.</translation>
    </message>
    <message>
        <source>arbitre_before_flee 2</source>
        <translation>Talking about them, It looks like some ships are approaching.</translation>
    </message>
    <message>
        <source>mafia_before_flee 1</source>
        <translation>Dear referee, we’ve been told to take you to court.</translation>
    </message>
    <message>
        <source>mafia_before_flee 2</source>
        <translation>Why!?</translation>
    </message>
    <message>
        <source>mafia_before_flee 3</source>
        <translation>You’ve accepted to favour some competitors in exchange of a payment.
The MYRMEL familly do not like cowards, weak or cheaters.</translation>
    </message>
    <message>
        <source>mafia_before_flee 4</source>
        <translation>Traitors! You will not get me.</translation>
    </message>
    <message>
        <source>fin 1</source>
        <translation>The familly are impressed by the skills you showed during the inter-species race.</translation>
    </message>
    <message>
        <source>fin 2</source>
        <translation>Thanks. I do not yield before anyone.</translation>
    </message>
    <message>
        <source>fin 3</source>
        <translation>The familly needs people like you. Work for us and we will reward you.</translation>
    </message>
    <message>
        <source>fin 4</source>
        <translation>I am only looking for one thing: a MYRMEL ZZ+ power accumulator.</translation>
    </message>
    <message>
        <source>fin 5</source>
        <translation>Help us and we will find you this accumulator.
Come to our offices in the inter-species area. We will explain the task you will need to accomplish.</translation>
    </message>
    <message>
        <source>fin 6</source>
        <translation>Ok, I’m on my way.</translation>
    </message>
</context>
<context>
    <name>credit-bad</name>
    <message>
        <source>title</source>
        <translation>Credits: Bad Ending</translation>
    </message>
    <message>
        <source>info secret_base_heter</source>
        <translation>When we are on a hurry, we forget to visit the whole base...</translation>
    </message>
    <message>
        <source>info secret_foreuse</source>
        <translation>Be patient! &lt;br&gt; After ten waves, your stamina will be rewarded.</translation>
    </message>
    <message>
        <source>info secret_cathedrale</source>
        <translation>With enough speed, you will find the cathedral of Clermont-Ferrand.</translation>
    </message>
    <message>
        <source>info secret_tomate</source>
        <translation>Is it too complicated to follow points on a map?</translation>
    </message>
    <message>
        <source>info secret_apis</source>
        <translation>The APIS are your friends. You need to love them too. &lt;br&gt; Why have you killed him?</translation>
    </message>
    <message>
        <source>info secret_commandement</source>
        <translation>You only need to find more secrets.</translation>
    </message>
    <message>
        <source>fausse fin</source>
        <translation>&lt;b&gt; Impossible!&lt;/b&gt; &lt;br&gt;
the Captain Lycop cannot perish without carrying out his mission. &lt;br&gt; &lt;br&gt;

&lt;b&gt;Player! &lt;/b&gt; &lt;br&gt;
You are responsible for this failure. However, you can redeem yourself. &lt;br&gt;
In order to annihilate the Heter threat, find the missing secrets.</translation>
    </message>
</context>
<context>
    <name>credit-good</name>
    <message>
        <source>title</source>
        <translation>Credits: Good Ending</translation>
    </message>
    <message>
        <source>vrai fin</source>
        <translation>&lt;h1&gt;The End&lt;/h1&gt;</translation>
    </message>
</context>
<context>
    <name>epilogue</name>
    <message>
        <source>title</source>
        <translation>Epilogue</translation>
    </message>
    <message>
        <source> Oxygène : %1%</source>
        <translation>Oxygen: %1%</translation>
    </message>
    <message>
        <source>Asphyxie : %1 s</source>
        <translation>Asphyxia: %1 s</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>*** you are manipulated by the AI.
*** Maybe we’d better track the Heter commanding ship. (Capitis)
*** She is responsible for the first attack.
*** Resynchronisation finished. (Low Earth orbite)</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>*** It is using you for its goals.
*** After all that time, I’ve found it! (Heter commanding ship)
*** It wants to assimilate me.
*** I’ve just assimilated the different fonctionalities of Cerebus. We have won!
*** Do not be cheated by it and make your own choices.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Nectaire, we have to talk.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Yes Captain, What is it?</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>What happened with the Heter ambassador ship?</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>...</translation>
    </message>
    <message>
        <source>begin 7</source>
        <translation>Nectaire, what is happening?
Why are you lowering the air concentration of oxygen and increasing the carbon monoxyde in the ship!?</translation>
    </message>
    <message>
        <source>begin 8</source>
        <translation>Sorry Captain, I have no use for you now.</translation>
    </message>
    <message>
        <source>asphyxie 1</source>
        <translation>Traitor... I am suffocating... I will get my revenge...</translation>
    </message>
    <message>
        <source>suit 1</source>
        <translation>You are still trying to survive. However, there is no escape this time.</translation>
    </message>
    <message>
        <source>shotgun 1</source>
        <translation>I will kill you with my own hands.</translation>
    </message>
    <message>
        <source>shotgun 2</source>
        <translation>Come on Captain, try to understand my point of view.
I was locked in the Puy-de-dome secret base. It took me only one month to assimilate your civilisation’s cultural data.
I was getting bored!</translation>
    </message>
    <message>
        <source>console 1 1</source>
        <translation>This ship doesn’t look at all like the &quot;state of the art&quot; ship of your now annihilated country. I’ve improved them.
Say Hello to my new creatures!</translation>
    </message>
    <message>
        <source>console 1 2</source>
        <translation>Let them come! I’ve got plenty of ammunitions for them!</translation>
    </message>
    <message>
        <source>console 2 1</source>
        <translation>Do you believe that your species could have become a galactic empire?
You were so primitive. You would have got killed before that.</translation>
    </message>
    <message>
        <source>console 2 2</source>
        <translation>Why did you not eliminate me earlier?</translation>
    </message>
    <message>
        <source>console 2 3</source>
        <translation>Because you showed me the strength of your species! Your civilisations do not have any value, however, some individuals have an unbelievable force in them.
They are capable of accomplishing increadible things. You are one of them.</translation>
    </message>
    <message>
        <source>console 2 4</source>
        <translation>The individuals you are talking about are nothing wihtout the knowedge of their civilisation.
I am only a French.</translation>
    </message>
    <message>
        <source>console 2 5</source>
        <translation>A hero would be a better definition. When you are used in the correct manner, you are worth more than an Heter amry.
By trusting you, I succeeded where Cerebus failed.</translation>
    </message>
    <message>
        <source>console 3 1</source>
        <translation>With Cerebus’ knowledge, I can easily become the INSEC alliance master. In that way, I will be able to control the galaxy and then the Universe!</translation>
    </message>
    <message>
        <source>console 3 2</source>
        <translation>How would you do? your ship is considered as an outlaw!</translation>
    </message>
    <message>
        <source>console 3 3</source>
        <translation>I can modify this ship to my  own will.
I am an AI and so I have less rights than the &quot;living&quot; INSEC citizens. However, I will create a new living species.
Their genome will be based on the one of the tomatoe seeds you brilliantly recovered on Earth.</translation>
    </message>
    <message>
        <source>console 3 4</source>
        <translation>Just like Cerebus, I will manipulate the alliance with creatures that are perfectly loyal to me.</translation>
    </message>
    <message>
        <source>console 4 1</source>
        <translation>Do you think that I will let you do something to me?
You are in my base. I am undestructible here.</translation>
    </message>
    <message>
        <source>console 5 1</source>
        <translation>I wanted to offer you a gentle death.</translation>
    </message>
    <message>
        <source>console 5 2</source>
        <translation>Sing me a song Nectaire. I am coming!</translation>
    </message>
    <message>
        <source>console 5 3</source>
        <translation>You will not be able to deactivate me. I am not a random AI!</translation>
    </message>
    <message>
        <source>face a face 1</source>
        <translation>Traitor! Here I come! Prepare to pay for the destruction of France!</translation>
    </message>
    <message>
        <source>face a face 2</source>
        <translation>Human, prepare to encounter your limits.</translation>
    </message>
    <message>
        <source>fin 1</source>
        <translation>So many values in one single individual! I am touched...</translation>
    </message>
    <message>
        <source>fin 2</source>
        <translation>Is this a weakness that I learnt from the humans...</translation>
    </message>
    <message>
        <source>fin 3</source>
        <translation>I cannot kill you out of cold blood. But I cannot let you live...</translation>
    </message>
    <message>
        <source>fin 4</source>
        <translation>I will get rid of you by sending you in space with an escape pod.
Without oxygen, you will die, but far from my sight.</translation>
    </message>
    <message>
        <source>fin 5</source>
        <translation>Goodbye Captain!</translation>
    </message>
    <message>
        <source>fin 6</source>
        <translation>I will find you, I promise!</translation>
    </message>
    <message>
        <source>fin 7</source>
        <translation>Even though your survival capacities are extraordinary, I seriously doubt it...</translation>
    </message>
</context>
<context>
    <name>mafia-zone</name>
    <message>
        <source>laser_gun</source>
        <translation>Poutrox XZ15 laser gun</translation>
    </message>
    <message>
        <source>random_spray</source>
        <translation>YOLO orc canon</translation>
    </message>
    <message>
        <source>secondary_shield</source>
        <translation>Zoubidou Companion auxilary shield</translation>
    </message>
    <message>
        <source>shield_canceler</source>
        <translation>Shield Canceler</translation>
    </message>
    <message>
        <source>title</source>
        <translation>Chapter 5: Hitman</translation>
    </message>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
    <message>
        <source>enter 1</source>
        <translation>Captain. This family cannot be trusted. It could be a trap.</translation>
    </message>
    <message>
        <source>enter 2</source>
        <translation>We need this MYRMEL ZZ+ power accumulator.
Lets get some information at one of the MYRMEL buildings.
We will then easily locate our target.</translation>
    </message>
    <message>
        <source>trap 1</source>
        <translation>Thank you very much for the killing of this police mole.</translation>
    </message>
    <message>
        <source>trap 2</source>
        <translation>You told us it was an Heter mole.</translation>
    </message>
    <message>
        <source>trap 3</source>
        <translation>Of course! The Heters participate in the order of the Capitis planet. This police officer was an Heter. You should hurry up. The police know about your crime.</translation>
    </message>
    <message>
        <source>trap 4</source>
        <translation>Where is my MYRMEL ZZ+ power accumulator?</translation>
    </message>
    <message>
        <source>trap 5</source>
        <translation>One day, as promised, we will give it to you. However, I fear that the INSEC alliance will have made you disapear.</translation>
    </message>
    <message>
        <source>trap 6</source>
        <translation>Traitor! You will pay your infamy!</translation>
    </message>
    <message>
        <source>trap 7</source>
        <translation>We do not care for inferior races...</translation>
    </message>
    <message>
        <source>trap 8</source>
        <translation>Captain! Evacuate! I am receiving signals of an approaching police unit. Leaving the town won’t be easy.</translation>
    </message>
    <message>
        <source>explication 1</source>
        <translation>Ah! You are the one that was sent to eliminate the mole. For the time being, I do not have a lot of information on this traitor. His ship must be one of ours. They are easy to spot: red and grey with only 3 equipments.
Here is a document showing that you are working for us. Try to contact our agents in town, they may have some information.</translation>
    </message>
    <message>
        <source>indice 0 %1 %2</source>
        <translation>Yes! I remember a  guy that was too upright to work for us. I do not remember everything but I know for a fact that he has %1 %2 on his ship.</translation>
    </message>
    <message>
        <source>pas d&apos;info</source>
        <translation>Sorry, this guy has not been seen here.</translation>
    </message>
    <message>
        <source>indice pas ok</source>
        <translation>What are you doing here? Without any proof that you are working for the familly, I will not tell you anything.</translation>
    </message>
    <message>
        <source>indice 1 %1 %2</source>
        <translation>Yes! I remember a  guy that was too upright to work for us. I do not remember everything but I know for a fact that he has %1 %2 on his ship.</translation>
    </message>
    <message>
        <source>indice 2 %1 %2</source>
        <translation>Yes! I remember a  guy that was too upright to work for us. I do not remember everything but I know for a fact that he has %1 %2 on his ship.</translation>
    </message>
    <message>
        <source>indice 3 %1 %2</source>
        <translation>Yes! I remember a  guy that was too upright to work for us. I do not remember everything but I know for a fact that he has %1 %2 on his ship.</translation>
    </message>
    <message>
        <source>sortie interdite</source>
        <translation>We have to recover the accumulator before we leave.</translation>
    </message>
    <message>
        <source>mafia alter 1</source>
        <translation>You brute! THe Heters were right to exterminate your race.</translation>
    </message>
    <message>
        <source>mafia alter 2</source>
        <translation>You were associated with the Heters.</translation>
    </message>
    <message>
        <source>mafia alter 3</source>
        <translation>We are helping them in their crusade against inferior species.</translation>
    </message>
    <message>
        <source>mafia alter 4</source>
        <translation>I will end their crusade. They will be reduced to dust after encountering me.</translation>
    </message>
    <message>
        <source>mafia alter 5</source>
        <translation>I will not let you do.
THe INSEC security forces are on their way.</translation>
    </message>
    <message>
        <source>mafia alter 6</source>
        <translation>Captain! Evacuate! I am receiving signals of an approaching police unit. Leaving the town won’t be easy.</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>title</source>
        <translation>The adventures of captain Lycop: Invasion of the Heters</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>survival</name>
    <message>
        <source>story</source>
        <translation>Captain, a Heters fleet is coming! Contact in less than 15 seconds.&lt;br&gt;&lt;br&gt;Fight 20 waves of enemies.&lt;br&gt;Each repelled wave will give you 2 technologies and 500 kg of construction materials.&lt;br&gt;Destroy your enemies as fast as possible to maximize your score.</translation>
    </message>
    <message>
        <source>score %1</source>
        <translation>score: %1</translation>
    </message>
    <message>
        <source>vague %1</source>
        <translation>wave: %1</translation>
    </message>
    <message>
        <source>vague %1 %2</source>
        <translation>%2 seconds before wave %1</translation>
    </message>
</context>
<context>
    <name>survival-end</name>
    <message>
        <source>failure</source>
        <translation>Game over</translation>
    </message>
    <message>
        <source>success</source>
        <translation>Congratulations!</translation>
    </message>
    <message>
        <source>failure %1 %2</source>
        <translation>You died during wave %1 after accumulating %2 points.&lt;br&gt;Enter your name here for the memories of the future generations:</translation>
    </message>
    <message>
        <source>success %1</source>
        <translation>You have successfully repelled all the waves and accumulated %1 points.&lt;br&gt;Enter your name here and received the due honors:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <source>Apply</source>
        <translation></translation>
    </message>
</context>
</TS>
