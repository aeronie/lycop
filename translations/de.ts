<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AccidentDialog</name>
    <message>
        <source>text 1 %1</source>
        <translation>Idiot! Sie haben  mein Schiff ruiniert! Um dies alles zu reparieren benörige ich %1 kg Konstruktionsmaterial. Geben Sie es mir sofort oder ich rufe die Polizei!</translation>
    </message>
    <message>
        <source>text 2 %1</source>
        <translation>Oh Mann, geben Sie acht! Mein großartiges Schiff ist nun Schrott! Ich weiß, dass es  ein Unfall war und ich vergesse die ganze Sache, falls Sie mir %1 kg Konstruktionsmaterial erstatten. Einverstanden?</translation>
    </message>
    <message>
        <source>text 3 %1</source>
        <translation>Stop! Im Namen der Transport Kommission von Capitis! Dieses Verhalten wird nicht in dieser Stadt geduldet. Sie müssen entweder %1 kg Konstruktionsmaterialien als Wiedergutmachung für den Schaden bezahlen oder Sie werden die Konsequenzen Ihres Verhaltens zu spüren bekommen.</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Ignorieren</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Bezahlen</translation>
    </message>
</context>
<context>
    <name>Controls</name>
    <message>
        <source>direction</source>
        <translation>Orientierung des Schiffes</translation>
    </message>
    <message>
        <source>velocity</source>
        <translation>Geschwindigkeit des Schiffes</translation>
    </message>
    <message>
        <source>weapons</source>
        <translation>Hauptwaffe</translation>
    </message>
    <message>
        <source>secondary-weapons</source>
        <translation>Nebenwaffe</translation>
    </message>
    <message>
        <source>speed-boost</source>
        <translation>Geschwindigkeitsboost</translation>
    </message>
    <message>
        <source>weapons-boost</source>
        <translation>Bevorzuge die Feuerfrequenz</translation>
    </message>
    <message>
        <source>shield-boost</source>
        <translation>Bevorzuge die Schildregeneration</translation>
    </message>
    <message>
        <source>repair</source>
        <translation>Repariere beschädigte Ausrüstung</translation>
    </message>
    <message>
        <source>slot-%1</source>
        <translation>Ausrüstung %1</translation>
    </message>
    <message>
        <source>button-%1</source>
        <translation>Button %1 in der Aktionsleiste</translation>
    </message>
    <message>
        <source>Mouse button %1</source>
        <translation>Maustaste %1</translation>
    </message>
    <message>
        <source>Mouse wheel %1</source>
        <translation>Mouserad %1</translation>
    </message>
    <message>
        <source>Keyboard %1</source>
        <translation>Taste [%1] der Tastatur</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation>Bewegung der Maus</translation>
    </message>
    <message>
        <source>Joystick button %1:%2</source>
        <translation>Button %2 des Gamepads %1</translation>
    </message>
    <message>
        <source>Joystick axis %1:%2</source>
        <translation>Analogstick %2 des Gamepads %1</translation>
    </message>
    <message>
        <source>Joystick hat %1:%2</source>
        <translation>Steuerkreuz %2 des Gamepads %1</translation>
    </message>
    <message>
        <source>Joystick ball %1:%2</source>
        <translation>Trackball %2 des Gamepads %1</translation>
    </message>
    <message>
        <source>Key_Up</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_W</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_Down</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_S</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_Left</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_A</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_Right</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_D</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_1</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_2</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_3</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_4</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_5</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_6</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_7</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_8</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_9</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_0</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MessageLog</name>
    <message>
        <source>No new messages.</source>
        <translation>Keine neue Nachricht.</translation>
    </message>
</context>
<context>
    <name>Screen_ConfirmControl</name>
    <message>
        <source>prompt %1 %2</source>
        <translation>%1 ist bereits and die Aktion %2 gebunden.&lt;br&gt; An welche Aktion wollen Sie %1 binden?</translation>
    </message>
</context>
<context>
    <name>Screen_Controls</name>
    <message>
        <source>Buttons</source>
        <translation>Button (Tastatur, Maus, Gamepad)</translation>
    </message>
    <message>
        <source>Axes</source>
        <translation>Analogsticks (Gamepad)</translation>
    </message>
    <message>
        <source>Mouses / hats</source>
        <translation>Bewegung der Maus oder Steuerkreuz des Gamepads</translation>
    </message>
    <message>
        <source>Slot</source>
        <translation>Slot</translation>
    </message>
    <message>
        <source>Binding</source>
        <translation>Binding</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>Screen_EditControl</name>
    <message>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <source>help 0</source>
        <translation>&lt;h1&gt;Bitte drücken Sie eine Taste auf Ihrer Tastatur, Maus oder Gamepad&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>help 1</source>
        <translation>&lt;h1&gt;Bewegen Sie den Analogstick Ihres Gamepads.&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>help 2</source>
        <translation>&lt;h1&gt;Bitte klicken Sie mit Ihrer Mause oder benutzen Sie das Steuerkreuz Ihres Gamepads.&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>Screen_EditGame</name>
    <message>
        <source>Annotate game “%1”:</source>
        <translation>Kommentiere das Spiel &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
</context>
<context>
    <name>Screen_EditScore</name>
    <message>
        <source>prompt %1 %2</source>
        <translation>Kommentiere die Punktzahl &quot;%1&quot;, gespeichert unter&quot;%2&quot;</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
</context>
<context>
    <name>Screen_Equipments</name>
    <message>
        <source>%1 HP</source>
        <translation>%1 HP</translation>
    </message>
    <message>
        <source>no-equipment</source>
        <translation>&lt;h1&gt;Keine&lt;/h1&gt;&lt;p&gt;Bitte wählen sie eine Ausrüstung aus obigen Liste aus.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>no-slot</source>
        <translation>&lt;h1&gt;Keine&lt;/h1&gt;&lt;p&gt;Bitte installieren Sie eine Ausrüstung oder wählen Sie einen anderen Platz auf dem Schiff aus.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Equip (%1 kg)</source>
        <translation>Ausrüsten (%1 kg)</translation>
    </message>
    <message>
        <source>Equip</source>
        <translation>Ausrüsten</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <source>Recycle (%1 kg)</source>
        <translation>Wiederverwerten (%1 kg)</translation>
    </message>
    <message>
        <source>Rotate left</source>
        <translation>Rotation links</translation>
    </message>
    <message>
        <source>Rotate right</source>
        <translation>Rotation rechts</translation>
    </message>
    <message>
        <source>Repair (%1 kg)</source>
        <translation>Reparieren (%1 kg)</translation>
    </message>
    <message>
        <source>Repair all (%1 kg)</source>
        <translation>Alles reparieren (%1 kg)</translation>
    </message>
</context>
<context>
    <name>Screen_GameOver</name>
    <message>
        <source>Game over</source>
        <translation>Game Over</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>Noch einmal versuchen</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Laden</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Aufgeben</translation>
    </message>
    <message>
        <source>Pensez à utiliser le celeritas accretio pour vous déplacer plus vite</source>
        <translation>Nutzen Sie den Celeritas Akkretio um zu beschleunigen.</translation>
    </message>
    <message>
        <source>Le bouclier à percussion désactive le bouclier Zoubidou Classix mais pas le bouclier auxiliaire Zoubidou Companion</source>
        <translation>Der Zoubidou One Liebeshammerschild deaktiviert das Zoubidou Classix Schild aber das Zoubidou Companion Zusatzschild funktioniert immer noch.</translation>
    </message>
    <message>
        <source>L&apos;accélerateur linéaire vous permet passivement de vous déplacer plus vite</source>
        <translation>Der Dango Linearbeschleuniger erhöht passiv Ihre Bewegungsgeschwindigkeit.</translation>
    </message>
    <message>
        <source>Le générateur de replis de l’espace permet de vous téléporter. Si vous vous téléportez sur un petit ennemi, vous échangerez de position</source>
        <translation>Der Raumkrümmungsgenerator erlaubt Teleportationen. Falls Sie es auf kleine gegnerische Schiffe benutzen, dann tauschen Sie die Plätze.</translation>
    </message>
    <message>
        <source>Vous manquez de DPS ? Utilisez le Modus accretio</source>
        <translation>Benötigen Sie mehr Feuerkraft? Benutzen Sie den Modus Akkretio!</translation>
    </message>
    <message>
        <source>Conseil : %1</source>
        <translation>Tip: %1</translation>
    </message>
</context>
<context>
    <name>Screen_LoadGame</name>
    <message>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Laden</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>Screen_Menu</name>
    <message>
        <source>Equipments</source>
        <translation>Ausrüstungen</translation>
    </message>
    <message>
        <source>Technologies</source>
        <translation>Technologien</translation>
    </message>
    <message>
        <source>Travel</source>
        <translation>Hyperspace</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>Menü</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <source>Mercury</source>
        <translation>Merkur</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation>Sonne</translation>
    </message>
    <message>
        <source>Earth</source>
        <translation>Erde</translation>
    </message>
    <message>
        <source>Planète Capitis</source>
        <translation>Planet Capitis</translation>
    </message>
    <message>
        <source>Vaisseau-monde Heter</source>
        <translation>Heter Kommando Schiff</translation>
    </message>
    <message>
        <source>Auto load</source>
        <translation>Schnell laden</translation>
    </message>
    <message>
        <source>New game</source>
        <translation>Neues Spiel</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Laden</translation>
    </message>
    <message>
        <source>Scores</source>
        <translation>Punktzahl</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Credits</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <source>Title screen</source>
        <translation>Hauptmenü</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <source>demo version %1</source>
        <translation>Demo Version %1</translation>
    </message>
    <message>
        <source>version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="vanished">Normal</translation>
    </message>
    <message>
        <source>Difficult</source>
        <translation type="vanished">Schwer</translation>
    </message>
    <message>
        <source>Impossible</source>
        <translation type="vanished">Unmöglich</translation>
    </message>
    <message>
        <source>Survival mode</source>
        <translation>Survival Mode</translation>
    </message>
    <message>
        <source>Easy</source>
        <translation>Story Modus</translation>
    </message>
    <message>
        <source>Training mode</source>
        <translation>Trainings Modus</translation>
    </message>
    <message>
        <source>Faible</source>
        <translation>Feigling</translation>
    </message>
    <message>
        <source>Vaillant</source>
        <translation>Mutig</translation>
    </message>
</context>
<context>
    <name>Screen_Options</name>
    <message>
        <source>Display preferences</source>
        <translation>Bildschirm Optionen</translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation>Vollbild</translation>
    </message>
    <message>
        <source>Lighting quality</source>
        <translation>Licht Qualität</translation>
    </message>
    <message>
        <source>Flat</source>
        <translation>Flach</translation>
    </message>
    <message>
        <source>Diffuse</source>
        <translation>Diffus</translation>
    </message>
    <message>
        <source>Diffuse and specular</source>
        <translation>Diffuse und spiegelnd</translation>
    </message>
    <message>
        <source>Sound preferences</source>
        <translation>Sound Optionen</translation>
    </message>
    <message>
        <source>Volume: sound effects</source>
        <translation>Lautstärke: Sound Effekte</translation>
    </message>
    <message>
        <source>Volume: music</source>
        <translation>Lautstärke: Musik</translation>
    </message>
    <message>
        <source>Volume: cutscenes</source>
        <translation>Lautstärke: Zwischensequenzen</translation>
    </message>
    <message>
        <source>Input preferences</source>
        <translation>Eingabe Optionen</translation>
    </message>
    <message>
        <source>Absolute velocity</source>
        <translation>Absolute Beschleunigung</translation>
    </message>
    <message>
        <source>Bindings</source>
        <translation>Eingabegeräte</translation>
    </message>
    <message>
        <source>Configure...</source>
        <translation>Konfiguriere...</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>Screen_Quit</name>
    <message>
        <source>title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>prompt</source>
        <translation>Feiglinge sterben in Schande</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
</context>
<context>
    <name>Screen_RemoveGame1</name>
    <message>
        <source>Do you want to remove all data about game “%1”?</source>
        <translation>Wollen Sie alle Daten bzgl. des Spiels &quot;%1&quot; löschen?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>Screen_RemoveGame2</name>
    <message>
        <source>Do you want to remove data saved on %2 about game “%1”?</source>
        <translation>Wollen Sie alle Daten auf %2 des Spiels &quot;%1&quot; löschen?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>Screen_RemoveScore</name>
    <message>
        <source>prompt %1 %2</source>
        <translation>Wollen Sie die Punktzahl &quot;%1&quot; gespeichert unter &quot;%2&quot; löschen?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>Screen_ResetControls</name>
    <message>
        <source>prompt</source>
        <translation>Wollen Sie die Einstellungen zurücksetzen?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>Screen_Scores</name>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Wave</source>
        <translation>Welle</translation>
    </message>
    <message>
        <source>end</source>
        <translation>Vollständig</translation>
    </message>
    <message>
        <source>wave %1</source>
        <translation>Welle %1</translation>
    </message>
    <message>
        <source>Score</source>
        <translation>Punktzahl</translation>
    </message>
    <message>
        <source>score %1</source>
        <translation>Punktzahl %1</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>Screen_Technologies</name>
    <message>
        <source>Level %1</source>
        <translation>Level %1</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Verfügbar</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <source>Science: %1 points</source>
        <translation>Wissenschaft: %1 Punkte</translation>
    </message>
    <message>
        <source>Research</source>
        <translation>Forschung</translation>
    </message>
</context>
<context>
    <name>Screen_Travel</name>
    <message>
        <source>Travel</source>
        <translation>Reisen</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <source>%1 HP</source>
        <translation>%1 HP</translation>
    </message>
</context>
<context>
    <name>Tab_credit</name>
    <message>
        <source>&lt;h1&gt;Aeronie&apos;s Dream Team&lt;/h1&gt;&lt;br&gt;</source>
        <translation>&lt;h1&gt;Aeronie&apos;s Dream Team&lt;/h1&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Translations&lt;/h1&gt;&lt;br&gt;</source>
        <translation>&lt;h1&gt;Übersetzungen&lt;/h1&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Background music&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Hintergrundmusik&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Title screen&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Titelbildschirm&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 1&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Kapitel 1&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 2&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Kapitel 2&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 3&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Kapitel 3&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 4&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Kapitel 4&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 5&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Kapitel 5&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 6&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Kapitel 6&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Fin ?&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Das Ende?&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 7&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Kapitel 7&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Epilogue&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Epilog&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Boss themes&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Melodie des Bosses&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Sound Effects&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Sound Effekte&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Libraries&lt;/h1&gt;&lt;br&gt;</source>
        <translation>&lt;h1&gt;Bibliothek&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>Remerciement</source>
        <translation>Wir wollen unseren Familien und Freunde danken, welche uns immer unterstützt haben.</translation>
    </message>
    <message>
        <source>&lt;h1&gt; Capitaine Lycop : L&apos;invasion des Heters &lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Capitain Lycop : Invasion of the Heters&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Outils Graphiques&lt;/h1&gt;&lt;br&gt;</source>
        <translation>Graphik Tools</translation>
    </message>
</context>
<context>
    <name>Technologies</name>
    <message>
        <source>hull</source>
        <translation>Hülle</translation>
    </message>
    <message>
        <source>hull 0</source>
        <translation>&lt;p&gt;Ständige Verbesserungen in der Legierung und der Verbundstoffe ermöglichen die Herstellung neuer Rüstungen und Waffen mit erhöhter Leistung.&lt;/p&gt;
&lt;p&gt;Diese neuen und widerstandsfähigeren Rüstungen werden Sie und Ihr Schiff vor physischen, magnetischen und chemischen Angriffen beschützen. Freuen Sie sich auf die kleinen Freuden Ihrer Interstellaren Reise.&lt;/p&gt; </translation>
    </message>
    <message>
        <source>nanomachines</source>
        <translation>LoTec Formic Nanomachinen 17</translation>
    </message>
    <message>
        <source>nanomachines 0</source>
        <translation>&lt;p&gt;Am Ende eines harten Kampfes WERFEN die meisten Piloten beschädigte Teile WEG und ERSETZEN diese durch neue.&lt;/p&gt;
&lt;p&gt;Jedoch, hier bei LoTec, bieten wir diese niedlichen kleinen Roboter an, welche in DAS HERZ DER MASCHINE eindringen um beschädigte Teile zu REPARIEREN. Und dies ohne, dass man auf das Ende des Kampfes warten muss!&lt;/p&gt;
&lt;p&gt;Treten Sie den Rängen derer bei, welche sich entschlossen haben Geld durch Nanoroboter zu sparen!&lt;/p&gt;</translation>
    </message>
    <message>
        <source>drone-launcher</source>
        <translation>LoTec Vesp Dronen Kontroller 22</translation>
    </message>
    <message>
        <source>drone-launcher 0</source>
        <translation>&lt;p&gt;Bei LoTec möchten wir daran erinnern: wenn die Mutigen tapfer kämpfen, sehen diejenigen die nicht kämpfen den Ausgang eines Kampfes.&lt;/p&gt;
&lt;p&gt;Diese eingebaute Mini-Fabrik reiht sich in diese Philosophie ein. Sie produziert und kontrolliert bis zu 5 Drohnen gleichzeitig, welche STATT IHRERSEITS kämpfen (und sterben).&lt;/p&gt;
&lt;p&gt;Da unsere Roboter Massenprodukte sind, aber unsere Kunden sind einzigartig.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>drone-launcher 1</source>
        <translation>&lt;li&gt;Drones sind mit Gatling M438 Kannonen ausgestattet.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>drone-launcher 2</source>
        <translation>&lt;li&gt;Drones sind mit Dagobert 3.3 Raketenwerfer ausgestattet.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>gyroscope</source>
        <translation>Mochi Winkelbeschleunigung von Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>gyroscope 0</source>
        <translation>&lt;p&gt;In einer sich verändernden Welt muss ein moderner Man in der Lage sein sich jederzeit herum zu drehen. Deshalb haben wir dieses Modul erschaffen welches die Effizienz Ihres Schiffmotors erhöht und es sich schneller drehen läßt.&lt;/p&gt;
&lt;p&gt;Nehmen Sie sich jedoch in Acht. Einige Kunden sagen, dass sich eine ausschweifende Nutzung des Moduls möglicherweise zu Schwindel und Übelkeit führt.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>gyroscope 2</source>
        <translation>&lt;li&gt;Das Schiff dreht sich noch schneller.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>dash</source>
        <translation>Dango Beschleuniger von Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>dash 0</source>
        <translation>&lt;p&gt;Fühlen Sie sich zurückgelassen? Wollen Sie den Rhythmus ändern? Dann haben wir das Richtige für Sie! Dieses Modul, welches die Effizienz Iheres Schiffmotors erhöht und es schneller fliegen läßt.&lt;/p&gt;
&lt;p&gt;Zu Ihrer eigenen Sicherheit und die Sicherheit anderer Kunden, beachten Sie die Geschwindigkeitsbegrenzungen in urbanen Gegenden und in Asteroidenfeldern.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>dash 2</source>
        <translation>&lt;li&gt;Das Schiff bewegt sich noch schneller.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>rocket-launcher</source>
        <translation>Dagobert 2.3 Raketenwerfer</translation>
    </message>
    <message>
        <source>rocket-launcher 0</source>
        <translation>&lt;p&gt;Diese Waffe feuert selbstangetriebene Projektile ab, welche mit 20 kg Dagobert Sprengstoff geladen sind. Dieser Sprengstoff ist für sein hohes zerstörerisches Potential bekannt.&lt;/p&gt;
&lt;p&gt;Warnung: diese ultra-sensiblen Projektile können vorzeitig explodieren, falls sie mit anderen Projektilen zusammentreffen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>missile-launcher</source>
        <translation>Dagobert 3.3 Raketenwerfer</translation>
    </message>
    <message>
        <source>missile-launcher 0</source>
        <translation>&lt;p&gt;Diese Waffe feuert selbstangetriebene Projektile ab, welche mit 20 kg Dagobert Sprengstoff geladen sind. Diese Waffe wird Ihre Gegnern in Angst und Schrecken versetzen.&lt;/p&gt;
&lt;p&gt;Diese Raketen wurden weiter entwickelt, so dass sie weniger empfindlich gegenüber anderen Projektilen sind. Unsere Ingenieure arbeiten gerade an dem Laser Problem und haven einen Prototypen versprochen, welcher noch vor der nächsten galaktischen Revolution erscheinen soll.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>gun</source>
        <translation>Gatling M438 Kanone</translation>
    </message>
    <message>
        <source>gun 0</source>
        <translation>&lt;p&gt;Diese einfache und verlässliche Kanone gehört zur Basisverteidigung eines jeden Schiffes. Dabei folgt sie einer einfachen und bewährten Lösung: Projektile werden auf potentielle Gefahren abgefeuert.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>triple-gun</source>
        <translation>Gatling M438‑S3 Dreifachkanone</translation>
    </message>
    <message>
        <source>triple-gun 0</source>
        <translation>&lt;p&gt;Eine kürzlich erschienene Studie zeigte, dass Projektile aus Kanonen abgefeuert klein und relativ weit auseinander sind. Wie kann man also sicher sein, sein Ziel zu treffen? Unseren Wissenschaftlern zufolge liegt die Lösung entweder im Vergrößern der Projektile oder den Abstand zwischen ihnen zu verringern.&lt;/p&gt;
&lt;p&gt;Diese Kanone nutzt erstere Möglichkeit: mehrere Projektile werden parallel abgefeuert und erhöhen so künstlich ihre Größe, was es schwieriger macht ihnen auszuweichen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>coil-gun</source>
        <translation>Ares Gaußgewehr</translation>
    </message>
    <message>
        <source>coil-gun 0</source>
        <translation>&lt;p&gt;Wir könnten die Feuerkraft unserer Kanonen erhöhen indem wir die benötigte Menge an Treibstoff reduziere. Ha, als ob wir uns von Ökos leiten lassen würden. Stattdessen nutzen wir die magnetische Induktion mit Hilfe einer Zylinderspule um die Metalprojektile zu beschleunigen. Natürlich hat diese Kanone eine hervoragende Feuerkraft. Aber müssen wir dafür tatsächlich unsere Traditionen auf dem Altar der Ökos opfern?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>electric-gun</source>
        <translation>Zeus Blitzkanone</translation>
    </message>
    <message>
        <source>electric-gun 0</source>
        <translation>&lt;p&gt;Heutzutage scheinen Schilde sehr stark unter jungen Piloten verbreitet zu sein. Und glabuen Sie uns, wir bedauern diesen Trend. All unsere Wut, all unsere Verzweiflung sind in dieser Waffe vereint. Sie feuert Plasmakugeln umgeben von sehr starken elektrischen Ladungen. Sie sind furchtbar Effizient gegen Energiefelder. Nun, sie kratzen nicht einmal die Hülle an, aber gegen Schilde... ah, was für ein Schauspiel!&lt;/p&gt;</translation>
    </message>
    <message>
        <source>mine-launcher</source>
        <translation>Dagobert 2.5 Minenwerfer</translation>
    </message>
    <message>
        <source>mine-launcher 0</source>
        <translation>&lt;p&gt;Diese Waffe läßt explosive Geräte, ausgestatten mit Näherungssensoren, fallen. Die explosive Ladung von 10 kg Dagobert Sprengstoff wird das Schiff überraschen, welches mutig genug ist sich der Mine zu nähern.&lt;/p&gt;
&lt;p&gt;Vorsicht: damit die Minen auch gut gegen andere Taktiken wie Tarnung oder Cyber-Sabotage funktionieren, wurden die ultra-sensiblen Zünder darauf hin entwickelt nicht zwischen Freund und Feind zu unterscheiden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>acid-gun</source>
        <translation>Dionysus Säurekanone</translation>
    </message>
    <message>
        <source>acid-gun 0</source>
        <translation>&lt;p&gt;Das Rennen um die Feuerkraft ist zu Ende. Diese Kanone kann jede Ausrüstung sofort zerstören. Ihre Säureprojektile greifen die Befestigungspunkte der Ausrüstung an, lassen sie rosten und lösen sie vom Schiff.
Nur Energieschilder sind (noch) in der Lage stand zu halten.Aber dennoch, wen interessierts? Ausrüstung wird zerstört! Augenblicklich!&lt;/p&gt;</translation>
    </message>
    <message>
        <source>random-spray</source>
        <translation>YOLO Orkkanone</translation>
    </message>
    <message>
        <source>random-spray 0</source>
        <translation>&lt;p&gt;Diese Kanone ist der erste zaghafte Versuch der Heter menschliche Technologie zu assimilieren. Sie feuert mit einer fragwürdigen Genauigkeit zufällig normale, elektrische, säurehaltige oder magnetisch beschleunigte Projektile.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>plasma-spray</source>
        <translation>Hephaestus Plasmaspray</translation>
    </message>
    <message>
        <source>plasma-spray 0</source>
        <translation>&lt;p&gt;Durch den köstlich grausamen menschlichen Flammenwerfer inspiriert haben die Heter diese Kanone mit kurzer Reichweite erschaffen. Sie feuert hochtemperiertes Plasma ab.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>plasma-spay 1</source>
        <translation>&lt;li&gt;Feuerkraft: +25 Schaden / s für jede Sekunde der man dem Plasma ausgesetzt ist&lt;/li&gt;</translation>
    </message>
    <message>
        <source>vortex-gun</source>
        <translation>Vortexkanone</translation>
    </message>
    <message>
        <source>vortex-gun 0</source>
        <translation>&lt;p&gt;Diese Kanone sendet eine Granate aus, welche einen flächendeckenden Vortex erschafft. Jedes kluge Individuum würde jeglichen Kontakt mit deren Flugbahn vermeiden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>qui-poutre</source>
        <translation>Kickasskanone</translation>
    </message>
    <message>
        <source>qui-poutre 0</source>
        <translation>&lt;p&gt;Diese Waffe ist eine verkleinerte Version des Heter Weltenzerstörers. Instabil, aber extrem kraftvoll; dies Waffe ist mächtiger als alle anderen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>qui-poutre 1</source>
        <translation>&lt;li&gt;Feuerkraft: 3000 Schaden, and 300 Schaden / s in der Nähe des Projektils&lt;/li&gt;</translation>
    </message>
    <message>
        <source>laser-gun</source>
        <translation>Poutrox XZ15 Laserkanone</translation>
    </message>
    <message>
        <source>laser-gun 0</source>
        <translation>&lt;p&gt;Die Schiffe der Heter sind gewöhnlich mit einer schweren Hülle ausgestattet, weshalb die seit langem Waffen bevorzugen, welche ihre Gegner schmelzen lassen können. Diese Laserkanone ist ähnlich den menschlichen Kanonen in Bezug auf ihrer Zerstörungskraft und ist ein Ergebnis dieser spezifischen Entwicklung.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>heavy-laser-gun</source>
        <translation>Poutrox XZ25 schwere Laserkanone</translation>
    </message>
    <message>
        <source>heavy-laser-gun 0</source>
        <translation>&lt;p&gt;Sie sollen nie wieder das frustrierende Gefühl haben ihr gegnerisches Schiff mit einem kraftlosen Laser nur kleine Kratzer zu zufügen! Mit dieser schweren Waffe werden sie den Gegner seltener Treffen, doch jedesmal wenn Sie ihn treffen, werden sie das sanfte Gefühl verspüren, dass sie ihm all die Schmerzen zugefügt haben, die er verdient.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>maser-emitter</source>
        <translation>Poutrox WR65 Maser Verteiler</translation>
    </message>
    <message>
        <source>maser-emitter 0</source>
        <translation>&lt;p&gt;Ja, einige Opfer versuchen sich hinter Hindernissen zu verstecken um Ihren Schüssen zu entkommen. Aber wo auch immer sie sich verstecken, Sie können sicher sein, dass sie diesem elektromagnetischem Strahl nicht ausweichen können. Dieser Strahl durchdringt jedes Hindernis auf seinem Weg während es seine molekulare Struktur zerstört.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>laser-bouncer</source>
        <translation>Poutrox XZ45 Zerstückelungs Laserkanone</translation>
    </message>
    <message>
        <source>laser-bouncer 0</source>
        <translation>&lt;p&gt;Ermüdet es Sie auf jedes kleine Schiff zu zielen? Dann nutzen sie diese Kanone, welche hochenergetische Laserstrahlen aussendet, welche anschließend explodieren und dann eine Unzahl von kleinen Lasern aussendet. Diese effiziente wie anmutige Waffe wird oft bei offiziellen Zeremonien auf Capitis eingesetzt.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>shield-generator</source>
        <translation>Zoubidou Classix Schild 1962</translation>
    </message>
    <message>
        <source>shield-generator 0</source>
        <translation>&lt;p&gt;Dieses freudig begrüßte Model hat sich über die Jahre als Referenz etabliert. Sogar heutzutage bleibt sie relevant.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>shield-canceler</source>
        <translation>Schildaufheber</translation>
    </message>
    <message>
        <source>shield-canceler 0</source>
        <translation>&lt;p&gt;Dieses Modul konzentriert Energie um EMP Wellen auzuschicken. Solche eine Welle blockiert alle Schilder, inklusive der Schilde des Aussenders.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>shield-canceler 2</source>
        <translation>&lt;li&gt;Die Zweitschilde des Aussenders sind nicht mehr von den EMP Wellen betroffen.&lt;/li&lt;</translation>
    </message>
    <message>
        <source>aggressive-shield-generator</source>
        <translation>Zoubidou One Love Hammerschild</translation>
    </message>
    <message>
        <source>aggressive-shield-generator 0</source>
        <translation>&lt;p&gt;Für diejenigen, welche denken, dass Angriff die beste Verteidigung ist, haben die Zoubidou Laboratories diesen Hammerschild entworfen, welcher eine maximale Sicherheit gegen Kollisionen mit mittelgroßen Objekten (1-100 Tonnen) bietet, indem diese Objekte von dem Schild zerstört werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>secondary-generator</source>
        <translation>Taiyaki Generator von Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>secondary-generator 0</source>
        <translation>&lt;p&gt;Wir wissen genau was Sie denken: warum soll ich mich mit der neuesten Technologie austatten wenn man danach ewig warten muss bis die Batterien geladen sind? Warum benötigen diese Geräte so viel Energie? Von nun an gehört diese Frage der Vergangenheit an. Mit diesem Generator können sie den modernen Komfort genießen und all ihre Ausrüstung grenzenlos nutzen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>secondary-shield-generator</source>
        <translation>Zoubidou Companion Zusatzschild</translation>
    </message>
    <message>
        <source>secondary-shield-generator 0</source>
        <translation>&lt;p&gt;Falls das Hauptschild versagt, schützt dieses Schild die meisten verwundbaren Teile ihres Schiffes. Dies wird dank der vielen unabhängigen Einheiten ermöglicht, die den Ausrüstungen zugeordnet sind.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>teleporter</source>
        <translation>Raumkrümmungs Generator</translation>
    </message>
    <message>
        <source>teleporter 0</source>
        <translation>&lt;p&gt;Reisen ist ohne Bewegung möglich, allerdings nur auf kurzen Strecken.&lt;/p&gt;&lt;p&gt;Seitdem der großartige Lars Nedum zu einem Stern gereist ist, wird dieses Modul mit einem Sicherheits &quot;Pinpin&quot; ausgestattet, so dass solche bedauerlichen Reisen verhindert werden. Allerdings kosten diese abgebrochenen Reisen dennoch Energie&lt;/p&gt;</translation>
    </message>
    <message>
        <source>secondary-battery</source>
        <translation>Dorayaki Zusatzbatterie von Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>secondary-battery 0</source>
        <translation>&lt;p&gt;Haben Sie große Pläne aber wissen nicht wie Sie diese verwirklichen sollen? Mit diesen Zusatzbatterien können Sie zumindest all die Energie einlagern die Sie benötigen um Ihre Träume zu verwirklichen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>harpoon-launcher</source>
        <translation>Harpunenkanone</translation>
    </message>
    <message>
        <source>harpoon-launcher 0</source>
        <translation>&lt;p&gt;Diese Weltraumharpune ermöglicht es an andere Schiffe anzukoppeln. Vorsicht: falls das angekoppelte Schiff zu schnell flieht, zieht es das andere hinter sich her.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>absorbing-shield-generator</source>
        <translation>Zoubidou Assimilator Absorbierer Schild</translation>
    </message>
    <message>
        <source>absorbing-shield-generator 0</source>
        <translation>&lt;p&gt;Dieses Schild sammelt Energie von abgefangenen Projektilen und benutzt diese um das Hauptschild wieder aufzuladen.&lt;/p&gt;
&lt;p&gt;Vorsicht! Übermäßiger Gebrauch kann eine Überladung des Hauptschildes auslösen und die Hülle beschädigen. Lesen Sie die Gebrauchsanweisungen sorgfälltig bevor Sie dieses Schild das erste Mal einsetzen.&lt;/p&gt;
&lt;p&gt;Wird mit eienm Hypo-Allergen-Filter ausgeliefert.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>wall-generator</source>
        <translation>Spiegelmauer</translation>
    </message>
    <message>
        <source>wall-generator 0</source>
        <translation>&lt;p&gt;Diese Kanone erzeugt eine Energiewand, welche jegliche Säure, Laser oder elektrischen Projektile reflektiert. Leider kann es nicht mehr als drei Mauern gleichzeitig kontrollieren. Gerüchte besagen, dass diese Mauern keine Hammerschilde mögen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>wall-generator 1</source>
        <translation>&lt;li&gt;Maximale Anzahl von Mauern: 3&lt;/li&gt;</translation>
    </message>
    <message>
        <source>wall-generator 2</source>
        <translation>&lt;li&gt;Diese Mauern reflektieren alle Projektile.&lt;/li&gt;</translation>
    </message>
</context>
<context>
    <name>Technology</name>
    <message>
        <source>&lt;h2&gt;Level %1&lt;/h2&gt;&lt;ul&gt;%2&lt;/ul&gt;</source>
        <translation>&lt;h2&gt;Level %1&lt;/h2&gt;&lt;ul&gt;%2&lt;/ul&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Required materials: %1 kg&lt;/li&gt;</source>
        <translation>&lt;li&gt;Benötigte Materialien: %1 kg&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Effect duration: %1 s&lt;/li&gt;</source>
        <translation>&lt;li&gt;Effekt Dauer: %1 s&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Activation time: %1 s&lt;/li&gt;</source>
        <translation>&lt;li&gt;Aktivierungszeit: %1 s&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Fire rate: %1 Hz&lt;/li&gt;</source>
        <translation>&lt;li&gt;Feuerrate: %1 Hz&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Firepower: %1 damages / %2&lt;/li&gt;</source>
        <translation>&lt;li&gt;Feuerkraft: %1 Schaden / %2&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Damage dealt during overload: %1 of base damages&lt;/li&gt;</source>
        <translation>&lt;li&gt;Zugefügter Schaden durch Überladung: %1 of base damages&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Firepower: %1 damages&lt;/li&gt;</source>
        <translation>&lt;li&gt;Feuerkraft: %1 Schaden&lt;/li&gt;</translation>
    </message>
    <message>
        <source>s</source>
        <translation></translation>
    </message>
    <message>
        <source>impact</source>
        <translation>Einschlag</translation>
    </message>
    <message>
        <source>&lt;li&gt;Resistance: %1 HP&lt;/li&gt;</source>
        <translation>&lt;li&gt;Widerstand: %1 HP&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Equipments regeneration: %1 HP / s&lt;/li&gt;</source>
        <translation>&lt;li&gt;Regenerierung der Ausrüstung: %1 HP / s&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Resistance: +%1 HP per equipment&lt;/li&gt;</source>
        <translation>&lt;li&gt;Widerstand: %1 HP pro Ausrüstung&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield resistance: %1 HP&lt;/li&gt;</source>
        <translation>&lt;li&gt;Schildwiederstand: %1 HP&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield regeneration: %1 HP / %2&lt;/li&gt;</source>
        <translation>&lt;li&gt;Regenierung der Schilde: %1 HP / %2&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield downtime: %1 s&lt;/li&gt;</source>
        <translation>&lt;li&gt;Zeit vor dem Wiedererscheinen der Schilde: %1 s&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield regeneration: %1 of base damages&lt;/li&gt;</source>
        <translation>&lt;li&gt;Regeneration der Schilde: %1 des Grundschadens&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Supplementary energy: %1 J&lt;/li&gt;</source>
        <translation>&lt;li&gt;Zusätzliche Energie: %1 J&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Energy regeneration: %1 J / s&lt;/li&gt;</source>
        <translation>&lt;li&gt;Regeneration der Energie: %1 J / s&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Energy consumed: %1 J / %2&lt;/li&gt;</source>
        <translation>&lt;li&gt;Verbrauchte Energie: %1 J / %2&lt;/li&gt;</translation>
    </message>
    <message>
        <source>use</source>
        <translation>benutzen</translation>
    </message>
    <message>
        <source>&lt;li&gt;Materials consumed: %1 kg / %2&lt;/li&gt;</source>
        <translation>&lt;li&gt;Verbrauchte Materialien: %1 kg / %2&lt;/li&gt;</translation>
    </message>
</context>
<context>
    <name>apis-zone</name>
    <message>
        <source>title</source>
        <translation>Chapter 5 : Die APIS Biosphäre</translation>
    </message>
    <message>
        <source>Vagues restantes: %1</source>
        <translation>Verbleibende Wellen: %1</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Zumindest erreichen wir die APIS Biospäre</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Ich kann nur sechs Biospären erfassen</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Es scheint mir nicht genug alle Spezien aufzunehmen welche durch die INSEC Allianz als ungeeignet eingestuft wurden.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Was denken Sie? Vielleicht werden sehr kleine Spezien als ungeeignet angesehen. Wir sind womöglich zu altmodisch für die Allianz.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>I denke, dass sie hauptsächlich unsere Einzigartigkeit verachten.</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>Sehen Sie! Ich habe einen Haufen Schiffe entdeckt.</translation>
    </message>
    <message>
        <source>manif 1</source>
        <translation>Wir können die Heter nicht ganze intelligente Gemeinschafter ausrotten lassen nur weill diese rachsüchtig sind.
Die INSEC Allianz rottet keine Gemeinschaften aus! Sie assimilieren sie! Es war schon immer so. Unsere Pflicht ist es sie zu stoppen!</translation>
    </message>
    <message>
        <source>manif 2</source>
        <translation>Wenn sie Sterne einfrieren, vereisen sie die Herzen der Menschen.
Was würden wir tun, wenn eine genügend intelligent Spezies Rache nehmen möchte?
Es wäre ein globaler Krieg ohne Gnade!
Wir müssen den Wahnsinn der Heter stoppen!
Lasst uns uns in der Abgeordnetenkammer der Allianz treffen. Die Heter müssen aufgehalten werden.</translation>
    </message>
    <message>
        <source>manif 3</source>
        <translation>Nein zur Vereisung! Stoppt die Vernichtung! Lasst uns für die Erhaltung kämpfen!</translation>
    </message>
    <message>
        <source>Après manif 1</source>
        <translation>Aber sie scheinen freundlich zu sein! Sie erinnern mich an mein Heimatland Frankreich.</translation>
    </message>
    <message>
        <source>Après manif 2</source>
        <translation>Dies ist sicherlich eine Gelegenheit Verbündete zu finden.</translation>
    </message>
    <message>
        <source>Après manif 3</source>
        <translation>Nectaire hat Recht. Ich werde mit ihrem Chef reden.</translation>
    </message>
    <message>
        <source>apis_before_invasion 1</source>
        <translation>Hi, ich bin der Manager der APIS Biosphäre.
Was kann ich für Sie tun?</translation>
    </message>
    <message>
        <source>apis_before_invasion 2</source>
        <translation>Die Administration hat uns gesagt, dass wir euch für Unterkunft kontaktieren sollen.</translation>
    </message>
    <message>
        <source>apis_before_invasion 3</source>
        <translation>Leider können wir euch nicht aufnehmen.
Wie ihr sehen könnt sind nur sechs Biospähren übrig.
Alle anderen wurden zerstört.</translation>
    </message>
    <message>
        <source>apis_before_invasion 4</source>
        <translation>Zerstört?</translation>
    </message>
    <message>
        <source>apis_before_invasion 5</source>
        <translation>Einige Drohnen haben unsere Biospären überfallen. Wir vermuten, dass die Heter für diese Angriffe verantwortlich sind.
Da wir friedliebende Leute sind suchen wir nach einer diplomatischen Lösung.
Leider hat dies bisher nicht funktioniert.</translation>
    </message>
    <message>
        <source>apis_before_invasion 6</source>
        <translation>Wenn sie euch angreifen, werden wir euch verteidigen. Ihr werdet nicht noch eine weitere Biosphäre verlieren!</translation>
    </message>
    <message>
        <source>apis_before_invasion 7</source>
        <translation>Man hat mir gesagt, dass die Portale gerade geöffnet werden.
Nehmt euch nicht der Gewalt an. Wir finden eine Lösung.</translation>
    </message>
    <message>
        <source>apis_before_invasion 8</source>
        <translation>Glauben Sie meiner Erfahrung. Die Heter denken nur in eine Richtung: Vae Victis!
Ich werde nicht verlieren.</translation>
    </message>
    <message>
        <source>after_invasion 1</source>
        <translation>Dies sind tatsächlich Heter Drohnen. Diese Kakerlaken fallen über alles her was sie finden.</translation>
    </message>
    <message>
        <source>after_invasion 2</source>
        <translation>Glückwunsch Käpt&apos;n! Die APIS werden sicherlich dankbar sein.
Ich habe nicht weniger von Ihnen erwartet.</translation>
    </message>
    <message>
        <source>after_invasion 3</source>
        <translation>Nectaire, es gibt keinen Grund sich bei mir einzuschmeicheln. Ein Held ist bescheiden.</translation>
    </message>
    <message>
        <source>apis_after_invasion 1</source>
        <translation>Vielen Dank! Ohne euch wären alle Biospären zerstört worden.
Es ist nun an uns euch zu helfen.</translation>
    </message>
    <message>
        <source>apis_after_invasion 2</source>
        <translation>Wo ist das Heter Mutterschiff?</translation>
    </message>
    <message>
        <source>apis_after_invasion 3</source>
        <translation>Gibt es einen Weg um die Sonne aufzutauen?</translation>
    </message>
    <message>
        <source>apis_after_invasion 4</source>
        <translation>Bezüglich des Mutterschiffs der Heter: keiner weiß wo es ist.
Jedoch war gestern ein Kommandoschiff hier. Es ist gerade zur Protura Konstellation aufgebrochen. Hier sind die Koordinaten...</translation>
    </message>
    <message>
        <source>apis_after_invasion 5</source>
        <translation>Es gibt eine Möglichkeit jeglichen Stern zu enteisen. Dafür benötigt ihr eine Waffe, welche nur von INSEC Bürgern beschafft werden kann. Außerdem ist sie sehr teuer.</translation>
    </message>
    <message>
        <source>apis_after_invasion 6</source>
        <translation>Wir haben nicht viele Ressourcen. Außerdem ist keiner von uns ein INSEC Bürger.</translation>
    </message>
    <message>
        <source>apis_after_invasion 7</source>
        <translation>Es gibt eine letzte Hoffnung.
Wir haben die Pläne für eine Waffe.
Vielleicht könnt ihr eine bauen?</translation>
    </message>
    <message>
        <source>apis_after_invasion 8</source>
        <translation>Ich dachte ihr seit Pazifisten?</translation>
    </message>
    <message>
        <source>apis_after_invasion 9</source>
        <translation>Pläne zu haben heisst nicht, dass wir sie nutzen wollen.
Wissen ist notwendig, nicht die Anwendung.</translation>
    </message>
    <message>
        <source>apis_after_invasion 10</source>
        <translation>Die Pläne sind komplex. Leider fehlen ein paar Teile.
Wir erhalten eine räumliche Isolierungszelle...</translation>
    </message>
    <message>
        <source>apis_after_invasion 11</source>
        <translation>Wir können euch eine geben. Diese Technologie wird benötigt damit unsere Biospäre vernünftig funktioniert.</translation>
    </message>
    <message>
        <source>apis_after_invasion 12</source>
        <translation>Wir brauchen einen Laserkonzentrierer WX99 sowie einen Akku MYRMEL ZZ+</translation>
    </message>
    <message>
        <source>apis_after_invasion 13</source>
        <translation>Bezüglich des Konzentrierers glaube ich, dass dies die Belohnung für das nächste Rennen ist, welches von der MYRMEL Familie und den POUTROX Industries organisiert wird.</translation>
    </message>
    <message>
        <source>apis_after_invasion 14</source>
        <translation>Auf geht&apos;s. Lasst uns das Rennen gewinnen!</translation>
    </message>
</context>
<context>
    <name>aw::Info</name>
    <message>
        <source>enable</source>
        <translation>normal</translation>
    </message>
    <message>
        <source>disable</source>
        <translation>vertauscht</translation>
    </message>
    <message>
        <source>up_left_down_right</source>
        <translation>normal</translation>
    </message>
    <message>
        <source>up_right_down_left</source>
        <translation>horizontal vertauscht</translation>
    </message>
    <message>
        <source>down_left_up_right</source>
        <translation>vertical vertauscht</translation>
    </message>
    <message>
        <source>down_right_up_left</source>
        <translation>beide vertauscht</translation>
    </message>
    <message>
        <source>up</source>
        <translation>oben</translation>
    </message>
    <message>
        <source>down</source>
        <translation>unten</translation>
    </message>
    <message>
        <source>left</source>
        <translation>links</translation>
    </message>
    <message>
        <source>right</source>
        <translation>rechts</translation>
    </message>
    <message>
        <source>up_down</source>
        <translation>vertikal</translation>
    </message>
    <message>
        <source>down_up</source>
        <translation>vertikal (vertauscht)</translation>
    </message>
    <message>
        <source>left_right</source>
        <translation>horizontal</translation>
    </message>
    <message>
        <source>right_left</source>
        <translation>horizontal (vertauscht)</translation>
    </message>
    <message>
        <source>plus</source>
        <translation>normal</translation>
    </message>
    <message>
        <source>minus</source>
        <translation>vertauscht</translation>
    </message>
</context>
<context>
    <name>boss-zone</name>
    <message>
        <source>title</source>
        <translation>Kapitel 5: Der Propugnator</translation>
    </message>
    <message>
        <source>Propugnator: %1 secondes</source>
        <translation>Propugnator: %1 Sekunden</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Beeilung! Wir sollten den Capitis Planeten verlassen bevor der Propugnator hier erscheint!</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Wovon reden Sie?</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Es ist das Hauptverteidigungsschiff der Stadt. Beeilung, ansonsten werden wir in seinem Traktorstrahl gefahngen gehalten und können keine Hyper-Exponential Propulsion mehr nutzen.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Dies ist schon der Fall. Es ist unmöglich für uns zu verschwinden. Wir müssen uns den Weg nach draußen kämpfen.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Ich fürchte niemanden! Laßt sie kommen, sie werden sterben!</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>Kämpft nicht! Es muss eine friedliche Lösung geben.</translation>
    </message>
    <message>
        <source>begin 7</source>
        <translation>Greifen Sie nicht ein, sonst nehmen Sie das Risiko auf sich zerstört zu werden.
In solchen Kämpfen gibt es großen Kollateralschaden.</translation>
    </message>
    <message>
        <source>after boss 1</source>
        <translation>Die gesamte Armee der INSEC kann nichts gegen mich unternehmen.</translation>
    </message>
    <message>
        <source>after boss apis 1</source>
        <translation>Was für eine Gewalt! Danke das Sie mir mein Leben lassen.</translation>
    </message>
    <message>
        <source>after boss apis 2</source>
        <translation>Da Sie mir halfen, werden Sie vogelfrei sein.</translation>
    </message>
    <message>
        <source>after boss apis 3</source>
        <translation>Die APIS werden bereits von den Mitgliedern der INSEC Allianz gehasst. Es ist das erste Mal, dass uns eine angriffslustige Spezies hilft die Biospären zu verteidigen. Ich werde für immer dankbar sein, dass sie uns gerettet haben.</translation>
    </message>
    <message>
        <source>after boss apis 4</source>
        <translation>Ich werde jeden zerquetschen, der es wagt die Schwachen zu unterdrücken. Die Stärksten müssen die Schwachen beschützen, ansonsten gibt es keine Zivilisation.</translation>
    </message>
    <message>
        <source>after boss 2</source>
        <translation>Käpt&apos;n, ich zweifle nicht an Ihrer Stärke, aber wir sollten weiterziehen bevor weitere Schiffe kommen.</translation>
    </message>
    <message>
        <source>after boss 3</source>
        <translation>Genau! Das Kommandoschiff der Heter wartet schon lange genug auf uns.</translation>
    </message>
</context>
<context>
    <name>chapter-0-1</name>
    <message>
        <source>title</source>
        <translation>Training: Steuerung</translation>
    </message>
    <message>
        <source>0_beginning</source>
        <translation>Wilkommen zum Steuerungstraining.
Das Ziel ist es die Steuerung des Raumschiffs zu meistern.
Bitte lesen Sie alle Erklärungen, welche Ihnen erklären, was zu tun ist.
Um Dialoge zu überspringen, klicken Sie auf das Sprechblasenicon im unteren linken Bildschirmrand.
Bewegen Sie sich zum ersten Pfeil.</translation>
    </message>
    <message>
        <source>1_first_door</source>
        <translation>Das Ziel dieser Übung ist es zu lernen wie man sich schnell und effektiv bewegt.&lt;br/&gt;Standardmäßig ist die Steuerung relativ zur Bewegung des Raumschiffs.&lt;br/&gt;Wenn sie also die OBEN Taste drücken (&lt;b&gt;%1&lt;/b&gt;), bewegt sich das Schiff nach vorne.&lt;br/&gt;Sie können diese Einstellung ändern, indem Sie &quot;Absolute Beschleunigung&quot; auswählen.&lt;br/&gt;Falls diese Option ausgewählt ist, dann bewirkt die OBEN Taste (&lt;b&gt;%1&lt;/b&gt;), dass sich das Schiff in Richtung des oberen Bildschirmrands bewegt.&lt;br/&gt;Bewegen Sie sich zum nächsten Pfeil.</translation>
    </message>
    <message>
        <source>2_second_door</source>
        <translation>Das Raumschiff hat eine Eilfunktion (Celeritas Akkretio), welche die Geschwindigkeit verdoppelt.&lt;br/&gt;Sie können diese aktivieren, indem Sie den Shortcut  (&lt;b&gt;%1&lt;/b&gt;) drücken.&lt;br/&gt;Bewegen Sie sich zum nächsten Pfeil, bevor dieser sich ausschaltet.</translation>
    </message>
    <message>
        <source>3_1_too_late</source>
        <translation>Sie haben es nicht rechtzeitig geschafft. Versuchen Sie es noch einmal.</translation>
    </message>
    <message>
        <source>3_2_third_door</source>
        <translation>Glückwunsch, aber aufgepasst! Je schneller sie fliegen, desto schlimmer ist ein Zusammenstoß mit einer Wand oder einem Gegner.
Bewegen Sie sich zum nächsten Pfeil.</translation>
    </message>
    <message>
        <source>4_1_fourth_door</source>
        <translation>Fleigen Sie zu den nächsten beiden Pfeilen bevor sich der Celeritas Akkretio ausschaltet.
Optimieren Sie Ihren Pfad. Sobald Sie über die Linie fliegen, ist der Checkpoint validiert.</translation>
    </message>
    <message>
        <source>4_2_too_late</source>
        <translation>Sie haben die Pfeile nicht rechtzeitig erreictht.
Versuchen Sie es noch einmal und vergessen Sie nicht den Celeritas Akkretio zu nutzen.</translation>
    </message>
    <message>
        <source>6_sixth_door</source>
        <translation>Glückwunsch!
Bewegen Sie sich zum nächsten Pfeil.</translation>
    </message>
    <message>
        <source>7_seventh_door</source>
        <translation>Einige Gegner sind unzerstörbar.
Sie müssen Ihr Verhalten analysieren und eine Öffnung finden um durch zu kommen.</translation>
    </message>
    <message>
        <source>8_final_door</source>
        <translation>Glückwunsch! Sie haben das Steuerungstraining abgeschlossen.</translation>
    </message>
</context>
<context>
    <name>chapter-0-2</name>
    <message>
        <source>title</source>
        <translation>Training: Zielen und schießen</translation>
    </message>
    <message>
        <source>0_beginning</source>
        <translation>Willkommen beim Ziel- und Schießtraining.&lt;br/&gt;Zerstören Sie diese Drohnen mittels des Feuerknopfes (&lt;b&gt;%1&lt;/b&gt;) und indem Sie auf sie zielen.&lt;br/&gt;Das Raumschiff hat eine Spezialfähigkeit (Modus Akkretio), welche die Feuerrate verdoppelt.&lt;br/&gt;Sie können diese aktivieren, indem sie den Shortcut (&lt;b&gt;%2&lt;/b&gt;) drücken.</translation>
    </message>
    <message>
        <source>1_first_wave_finished</source>
        <translation>Warnung! Kollisionen schädigen Ihrem Schiff. Die nächsten Drohnen bewegen sich auf weniger vorhersehbaren Wegen.
Schießen Sie sie ab.</translation>
    </message>
    <message>
        <source>2_second_wave_finished</source>
        <translation>Die nächsten Drohnen fliegen überall im Raum umher.
Schießen Sie sie ab.</translation>
    </message>
    <message>
        <source>3_third_wave_finished</source>
        <translation>Glückwunsch! Sie haben das Ziel- und Schießtraining erfolgreich abgeschlossen.</translation>
    </message>
</context>
<context>
    <name>chapter-0-3</name>
    <message>
        <source>title</source>
        <translation>Training: Kampf</translation>
    </message>
    <message>
        <source>0_beginning</source>
        <translation>Willkommen im Kampftraining.
Die Drohnen haben nun die gleichen Waffen wie Sie.
Da kommt eine. Schießen Sie sie ab.</translation>
    </message>
    <message>
        <source>1_first_wave_finished</source>
        <translation>Wie Sie vielleicht bemerkt haben, zielen die Drohnen auf Sie.
Umher zu fliegen ist eine effektive Art und Weise den Kugeln auszuweichen.
Da kommen zwei Drohnen. Schießen Sie sie ab.</translation>
    </message>
    <message>
        <source>2_second_wave_finished</source>
        <translation>Glückwunsch! Sie werden manchmal zwischen Gegner geraten.
Ihr Raumschiff kann mehrere Treffer überleben, aber die Ausrüstung nahe des Einschlags wird beschädigt.
Da kommen vier Drohnen. Schießen Sie sie ab.</translation>
    </message>
    <message>
        <source>3_third_wave_finished</source>
        <translation>Glückwunsch! Einige Gegner sind stärker. Sie haben mehr Ausrüstung, welche sie mehr Treffer einstecken lässt und somit gefährlicher sind.
Da ist ein großer im Anmarsch. Schießen Sie ihn ab.</translation>
    </message>
    <message>
        <source>4_fourth_wave_finished</source>
        <translation>Glückwunsch! Sie haben das Kampftraining abgeschlossen. Sie sind nun bereit für Ihre Mission.</translation>
    </message>
</context>
<context>
    <name>chapter-1</name>
    <message>
        <source>title</source>
        <translation>Kapitel 1: Niedrige Erdumlaufbahn</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Willkommen Käpt&apos;n! Wie ich sehe haben sie den Weltraum erreicht. Ich sagte doch, dass unsere Geheimbasis im Puy-du-Dome eines Tages nützlich sein wird!</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Zur Erinnerung: unsere Mission besteht darin, dass:
- wir Kontakt mit Außerirdischen machen,
- Gefahren einschätzen, und
- entsprechend handeln.</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Nun! Es ist Zeit die künstliche Intelligenz des Schiffes zu aktivieren: Nectaire. Sie wird Sie auf Ihrer Expedition leiten. Viel Glück! Wir glauben alle an Ihren Erfolg!</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Bzzz...</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Aktivierung abgeschlossen!</translation>
    </message>
    <message>
        <source>story 1 6</source>
        <translation>Hallo Käpt&apos;n. Damit Sie mit der Kontrolle des Schiffes vertraut werden, werden wir einige Übungen absolvieren.</translation>
    </message>
    <message>
        <source>story 1 7 %1</source>
        <translation>Bewegen Sie das Schiff in Richtung des markierten Punktes auf Ihrem Radar auf der linken Seite Ihres Dashboards. Sie können sich schneller bewegen, indem Sie die Celeritas Accretio (Patent beantragt) mit &lt;b&gt;%1&lt;/b&gt; benutzen. Allerdings kostet dies Energie.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Bravo! Es ist ein alter Satelit. Sie können ihn recyclen.</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>Das Recyclen hat uns, wie auf dem Dashboard angezeigt, 500 kg Konstruktionsmaterialien beschafft.</translation>
    </message>
    <message>
        <source>story 2 3</source>
        <translation>Damit können wir Kanonen konstruieren um uns im Fall der Fälle zu verteidigen.</translation>
    </message>
    <message>
        <source>story 2 4</source>
        <translation>Wählen Sie eine Kanone in dem Bildschirm &quot;Schiff&quot; aus und installieren Sie diese in 5 der 7 verfügbaren Plätze auf ihrem Schiff.</translation>
    </message>
    <message>
        <source>story 2 5</source>
        <translation>Beachten Sie, dass ich 5 Sekunden benötige um diese Ausrüstung zu entwerfen. Während dieser Zeit sind sie sehr zerbrechlich und jeglicher Schaden an der Schiffshülle kann sie zerstören.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>Die Kanonen sind fertig. Es ist an der Zeit diese zu testen.</translation>
    </message>
    <message>
        <source>story 3 2 %1 %2</source>
        <translation>Nutzen Sie &lt;b&gt;%1&lt;/b&gt; um zu feuern. Die Feuerrate kann verdoppelt werden, indem sie die Modus Accretio (Patent beantragt) mit &lt;b&gt;%2&lt;/b&gt; verwenden. Allerdings kostet dies Energie.</translation>
    </message>
    <message>
        <source>story 3 3</source>
        <translation>Eine Trainingsdrohne kommt auf Sie zu. Schießen Sie sie nieder!</translation>
    </message>
    <message>
        <source>story 4</source>
        <translation>Sehen Sie! Die Drohne hat einige Trümmer zurückgelassen. Sie sind Konstruktionsmaterialien, welche Sie aufsammeln können.</translation>
    </message>
    <message>
        <source>story 5</source>
        <translation>Das Signal eines Schiffs ist nahe. Treffen Sie sich mit dem Schiff und halten Sie sich an die Anordnungen, welche von CNES ausgesendet werden.</translation>
    </message>
    <message>
        <source>story 6 1</source>
        <translation>Mein Kawai Detektor ist gesättigt! Dieses Schiff ist seltsam süß, bleiben Sie wachsam.</translation>
    </message>
    <message>
        <source>story 6 2</source>
        <translation>Ich aktiviere die Licht-Willkommensnachricht. Bis jetzt keine Antwort.</translation>
    </message>
    <message>
        <source>story 6 3</source>
        <translation>Ich versuche es noch einmal. Immer noch nichts.</translation>
    </message>
    <message>
        <source>story 6 4</source>
        <translation>Boudidiou? Funktioniert dieser Knopf? Made in France schlägt zurück!</translation>
    </message>
    <message>
        <source>story 6 5</source>
        <translation>Ups! Habe ich den falschen Knopf gedrückt?</translation>
    </message>
    <message>
        <source>story 6 6</source>
        <translation>Resynchronisierung abgeschlossen! Für einen Moment habe ich den Kontakt verloren Käpt&apos;n!</translation>
    </message>
    <message>
        <source>story 6 7</source>
        <translation>Wir sind die Heter. Euer Verhalten hat gegen Artikel 20-4500A der INSEC Gesetzgebung verstossen. Wir sind nun berechtigt, Ihre niedere Spezie zu assimilieren und auszulöschen. All eure Basen gehören nun uns!</translation>
    </message>
    <message>
        <source>story 7 1</source>
        <translation>Der erste Kontakt war nicht besonders positiv.</translation>
    </message>
    <message>
        <source>story 7 2</source>
        <translation>Aber es war gut, dass Sie das Schiff zerstört haben: Ich kann nun einen Teil ihrer Technologie assimilieren. Ich habe schon ihren Schildgenerator assimiliert.</translation>
    </message>
    <message>
        <source>story 7 3</source>
        <translation>Außerdem, immer wenn meine Komplexität erhöht wird, kann ich mich weiter spezialisieren um besser ihren Erwartungen gerecht zu werden. Sie können von nun an unter dem &quot;Technologie&quot; Bildschirm neue Technologien erforschen oder bereits entwickelte verbessern.</translation>
    </message>
    <message>
        <source>story 8 1</source>
        <translation>Beachten Sie, dass einige Funktionalitäten erst dann freigeschalten werden, wenn Sie die nötigen Voraussetzungen erfüllen.</translation>
    </message>
    <message>
        <source>story 8 2 %1</source>
        <translation>Das Schild lädt sich langsam auf. Falls es aber zuviel Schaden nehmen sollte, wird es sich für eine Weile komplett abschalten. Sie können die Schilde schneller aufladen indem sie die Clipeus Accretio (Patent beantragt) mit &lt;b&gt;%1&lt;/b&gt; einsetzen. Allerdings kostet dies Energie.</translation>
    </message>
    <message>
        <source>story 8 3</source>
        <translation>Einige Ausrüstungen verbrauchen Energie, andere verbrauchen Konstruktionsmaterialien. Energie lädt sich langsam von selber auf, während Konstruktionsmaterialien von Trümmern geborgen werden müssen.</translation>
    </message>
    <message>
        <source>story 8 4</source>
        <translation>Ihr Dashboard zeigt Ihnen den Status Ihrer auf dem Schiff installierten Ausrüstung an.</translation>
    </message>
    <message>
        <source>story 8 5</source>
        <translation>Klicken Sie mit links auf eine Waffe oder Werkzeug um diese sofort zu nutzen.</translation>
    </message>
    <message>
        <source>story 8 6 %1</source>
        <translation>Rechtsklick auf eine Waffe oder Werkzeug um diese auszuwählen. Sie können dann mit &lt;b&gt;%1&lt;/b&gt; alle ausgewählten Ausrüstungen aktivieren.</translation>
    </message>
    <message>
        <source>story 8 7</source>
        <translation>Klicken Sie auf den &quot;Nachrichten&quot; Button über dem Radar um diese Erklärungen erneut abzurufen.</translation>
    </message>
    <message>
        <source>story 9 1</source>
        <translation>Meine Sensoren haben außerirdische Aktivitäten auf dem Merkur festgestellt. Ihre Mission ist es festzustellen, ob es sich um eine Bedrohung handelt.</translation>
    </message>
    <message>
        <source>story 9 2</source>
        <translation>Die CNES hat mich nicht aufgrund meiner Feigheit ausgewählt! Auf geht&apos;s!</translation>
    </message>
    <message>
        <source>story 9 3</source>
        <translation>Es ist an der Zeit die Hyper-Exponential zu testen. Volle Kraft voraus , Mr Sulu!</translation>
    </message>
    <message>
        <source>story 9 4</source>
        <translation>Ich werde LYCOP genannt!</translation>
    </message>
    <message>
        <source>story 9 5</source>
        <translation>Bzzz, es ist ein Datenbankfehler. Name korrigiert!</translation>
    </message>
    <message>
        <source>Recyclez le satellite</source>
        <translation>Recyclen Sie den Satelit</translation>
    </message>
    <message>
        <source>Construisez de nouveaux canons</source>
        <translation>Bauen Sie neue Kanonen bis alle Materialen verbraucht sind</translation>
    </message>
    <message>
        <source>Détruisez le drone</source>
        <translation>Zerstören Sie diese Drohne</translation>
    </message>
    <message>
        <source>Utilisez votre point de technologie</source>
        <translation>Benutzen Sie Technologiepunkte</translation>
    </message>
    <message>
        <source>Objectif : %1</source>
        <translation>Aufgabe: %1</translation>
    </message>
</context>
<context>
    <name>chapter-1b</name>
    <message>
        <source>title</source>
        <translation>Kapitel 1: Niedrige Erdumlaufbahn (Ende)</translation>
    </message>
</context>
<context>
    <name>chapter-2</name>
    <message>
        <source>title</source>
        <translation>Kapitel 2: Merkur</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Ich schalte das Hyper-Exponential aus. Käpt&apos;n, wir sind auf dem Merkur!</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Huh, ich sehe nur Staub und Steine. Nectaire, sind Sie wirklich sicher, dass das Signal von hier kommt?</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Schwer zu sagen. Wir sind Nahe der Sonne und magnetische Abnormalitäten stören meine Sensoren.</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Was zum Teufel! Nectaire, können Sie wenigstens eine ungefähre Position bestimmen?</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Ich schätze die beste Hypothese ein... bitte warten...</translation>
    </message>
    <message>
        <source>story 1 6</source>
        <translation>Das Signal kommt aus der auf der Karte markierten Zone. Mit einer Wahrscheinlichkeit von 21,03%.</translation>
    </message>
    <message>
        <source>point 1 1</source>
        <translation>Ok, aber dort ist auch nichts.</translation>
    </message>
    <message>
        <source>point 1 2</source>
        <translation>Ich nehme verdächtige Aktivitäten um uns herum wahr.</translation>
    </message>
    <message>
        <source>point 1 3</source>
        <translation>Es ist eine Falle!</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Oh, diese Feiglinge. Solch ein Überraschungsangriff widert mich an!</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>Ich nehme viele Untergrundaktivitäten wahr und ich empfehle für die restliche Mission äußerste Sorgfalt zu wahren. Mit den neuesten Daten konnte ich auch den Ursprung des Signals neu bestimmen.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>Verdammte Banditen. Das werde ich ihnen heimzahlen. Nun zu Ihnen Nectaire, ich beginne an Ihrer Verläßlichkeit zu zweifeln.</translation>
    </message>
    <message>
        <source>story 3 2</source>
        <translation>Die letzten Ergebnisse waren innerhalb der Meßungenauigkeit. Außerdem kann man auf Grund der Anzahl der Verteigungnen welche wir antrafen und der elektrischen Untergrundaktivitäten auf eine Basis auf dem Merkur schließen.</translation>
    </message>
    <message>
        <source>story 3 3</source>
        <translation>Was für Neuigkeiten! Wurden Sie nicht darauf programmiert relevantere Informationen anzubieten?</translation>
    </message>
    <message>
        <source>story 3 4</source>
        <translation>Ich wurde darauf programmiert psychologische Betreuung während der Mission zu bieten. Und wenn ich diese Unterhaltung mit Banalitäten überhäufe, will ich nur die Kommunikation mit Menschen vereinfachen. Meine neuesten Berechnungen sind abgeschlossen. Wenn Sie möchten, kann ich eine neue potentielle Position für den Ursprung des Signals angeben.</translation>
    </message>
    <message>
        <source>story 4 1</source>
        <translation>Nectaire, reissen Sie sich zusammen! Wir können es uns nicht leisten in jede für uns aufgestellte Falle zu treten. Wie geht es mit der Positionsbestimmung der Basis voran?</translation>
    </message>
    <message>
        <source>story 4 2</source>
        <translation>Wäre ich darauf programmiert worden Scham zu verspüren, dann würde ich wegen dieser Fehler knallrot anlaufen. Auf jeden Fall, hat uns das Verwerfen dieser Hypothesen näher an unser Ziel gebracht. Haben Sie etwas Mut!</translation>
    </message>
    <message>
        <source>story 4 3</source>
        <translation>Verfluchte Wissenschaftler... warum kann ich nicht einen normalen Computer haben?</translation>
    </message>
    <message>
        <source>story 4 4</source>
        <translation>Es wäre bei weitem weniger Effizient. Nun, ich konnte wieder eine neue Abschätzung der Position des Ursprungs des Signals bestimmen.</translation>
    </message>
    <message>
        <source>story 4 5</source>
        <translation>Gut gemacht, aber ich hoffe für dich, dass dies nicht wieder eine Falle ist.</translation>
    </message>
    <message>
        <source>story 5 1</source>
        <translation>Das reicht! Ich werde dich abschalten und danach jedes einzelne Staubkorn auf diesem Planeten eigenhändig untersuchen.</translation>
    </message>
    <message>
        <source>story 5 2</source>
        <translation>Warten Sie! Diesmal ist es sicher die richtige Position. Das Signal kommt von dem Fuße dieser Klippen dort!</translation>
    </message>
    <message>
        <source>story 5 3</source>
        <translation>Wirklich sicher?</translation>
    </message>
    <message>
        <source>story 5 4</source>
        <translation>Absolut sicher.</translation>
    </message>
    <message>
        <source>story 5 5</source>
        <translation>Ok, ich werde dies noch ein letztes Mal überprüfen. Auf geht&apos;s.</translation>
    </message>
    <message>
        <source>story 5 6</source>
        <translation>Allerdings ist die Basis unterirdisch.</translation>
    </message>
    <message>
        <source>story 5 7</source>
        <translation>Nun, das ist kein Problem. Wir graben ein Loch um einzudringen.</translation>
    </message>
    <message>
        <source>story 5 8</source>
        <translation>Ich wurde nicht mit Bohrern ausgestattet.</translation>
    </message>
    <message>
        <source>story 5 9</source>
        <translation>Wer sprach von Bohrern? Wir haben Kanonen!</translation>
    </message>
    <message>
        <source>story 6 1</source>
        <translation>Käpt&apos;n, wir können durch diese Öffnung zu der Basis fliegen.</translation>
    </message>
    <message>
        <source>story 6 2</source>
        <translation>Gut! Laßt uns nachsehen, was diese Heters dort machen.</translation>
    </message>
</context>
<context>
    <name>chapter-2-escape</name>
    <message>
        <source>title</source>
        <translation>Kapitel 2: Merkur bereinigt</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Herzlichen Glückwunsch Käpt&apos;n! Die Basis ist zerstört und das Schiff hat nur leichten Schaden davon getragen.</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Natürlich! Haben Sie an mir gezweifelt?</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Gegen jeglichen Erwartungen hat ein Schiff die Explosion der Basis überlebt. Es fliegt in Richtung Sonne.</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Wie ist das möglich? Auf jeden Fall wird er keinen weiteren Tag mehr erleben. Ich kann es mir nicht leisten eine Bedrohung laufen zu lassen.</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Sorry, aber seine Geschwindigkeit ist viel größer als unsere. Wir können das Schiff nicht einholen.</translation>
    </message>
    <message>
        <source>story 1 6</source>
        <translation>*** angestrengtes Nachdenken</translation>
    </message>
    <message>
        <source>story 1 7</source>
        <translation>Ah! Jetzt weiß ich es. Wir benutzen das Gravitationsfeld der Sonne um uns zu beschleunigen. Damit werden wir uns auf die gleiche oder noch höhere Geschwindigkeit beschleunigen. Was sagen Sie?</translation>
    </message>
    <message>
        <source>story 1 8</source>
        <translation>Das könnte funktionieren, aber womöglich hält das Schiff nicht stand. Es ist zu gewagt. Haben Sie keine Überlebensinstinkte?</translation>
    </message>
    <message>
        <source>story 1 9</source>
        <translation>Diese Kleinigkeiten jagen mir keine Angst ein. Alles was für mich zählt ist, dass die Bedrohung unterdrückt wird. Nur Mut, Neclaire fürchtet nichts!</translation>
    </message>
</context>
<context>
    <name>chapter-2b</name>
    <message>
        <source>title</source>
        <translation>Kapitel 2: Untergrund Basis</translation>
    </message>
    <message>
        <source>escape %1</source>
        <translation>Selbstzerstörung in %1</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Diese Struktur ist erstaunlich. Seit wann bohren diese Heter hier?</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Meine Sensoren können das Alter der Strukturen nicht bestimmen, aber meine Sensoren können eine wichtige zirkulierende Energie dahinter wahrnehmen. Diese Basis muss von einem gigantischen Generator angetrieben werden.</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Je größer der Generator ist, desto schöner ist die Explosion!</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Sehen Sie Käpt&apos;n! Diese metallische Struktur auf der linken Seite sieht wie eine Tür aus.</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Perfekt. Laßt uns stark klopfen bevor wir eintreten.</translation>
    </message>
    <message>
        <source>room 4 1</source>
        <translation>Meine Sensoren sagen mir, dass ein Teil dieser Infrastruktur nicht mehr betrieben wird.</translation>
    </message>
    <message>
        <source>room 4 2</source>
        <translation>Jetzt schon? War das nicht zu einfach? Ich habe das dumpfe Gefühl, dass man uns unterschätzt.</translation>
    </message>
    <message>
        <source>room 6 1</source>
        <translation>Diese Tür scheint durch ein starkes Kraftfeld geschützt zu sein.</translation>
    </message>
    <message>
        <source>room 6 2</source>
        <translation>Was könnten sie dahinter vor uns verbergen?</translation>
    </message>
    <message>
        <source>room 7a 1</source>
        <translation>Dort ist der Hauptgenerator dieser Einrichtung.</translation>
    </message>
    <message>
        <source>room 7a 2</source>
        <translation>Endlich! Sobald dieser zerstört ist wird diese Basis keine Bedrohung mehr sein und ich kann zurück nach Hause kehren. Diese Heter sollen es bereuen Frankreich angegriffen zu haben!</translation>
    </message>
    <message>
        <source>room 7a 3</source>
        <translation>Sie meinen die Erde?</translation>
    </message>
    <message>
        <source>room 7a 4</source>
        <translation>Drehen Sie mir nicht die Worte im Munde um, Nectaire!</translation>
    </message>
    <message>
        <source>room 7b 1</source>
        <translation>Die Explosion des Generators hat den Selbstzerstörungsmechanismus dieser Basis aktiviert. Käpt&apos;n, Sie haben nur 4 Minuten um zu verschwinden.</translation>
    </message>
    <message>
        <source>room 7b 2</source>
        <translation>Tatsächlich, es wäre nicht ratsam noch länger in dieser Gegend zu bleiben.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Selbstzerstörung wurde unterbrochen.</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>Wie unglücklich. Ich habe mich schon darauf gefreut, dass sich dieser Ort in Schutt und Asche verwandelt.</translation>
    </message>
    <message>
        <source>story 2 3</source>
        <translation>Die Unterbrechung kam womöglich von dieser Maschine, welche gerade in diesem Raum gestartet wurde.</translation>
    </message>
    <message>
        <source>story 2 4</source>
        <translation>Aber das heißt, dass der Selbstzerstörungsmechanismus erneut startet, wenn ich diese Maschine zerstöre.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>Ich könnte eines dieser Module assimilieren. Damit könnte ich die Nachrichten der Heter entschlüsseln.</translation>
    </message>
    <message>
        <source>story 3 2</source>
        <translation>Gut! Wenn sie es wagen sollten zurück zu kommen, dann werden wir sie auf die Art und Weise willkommen heißen, die sie verdienen.</translation>
    </message>
    <message>
        <source>story 3 3</source>
        <translation>Der Selbstzerstörungsmechanismus wurde wieder in Gang gesetzt. Wir haben nur wenig Zeit zu entkommen. Beeilen wir uns!</translation>
    </message>
</context>
<context>
    <name>chapter-2c</name>
    <message>
        <source>Collected materials: %1 kg</source>
        <translation>Angesammelte Materialien: %1 kg</translation>
    </message>
    <message>
        <source>title</source>
        <translation>Zwischensequence: Überfall auf die Basis der Heter</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>Käpt&apos;n! Wir sind nun in dem Krater, welcher durch die Explosion der Basis der Heter entstanden ist.
Ich werde nun ein Recyclemodul auf dem Grund absetzen. Bevor wir Aufbrechen, vergessen Sie nicht dieses zu holen. Sie brauchen nur wenige Sekundne um das Modul aufzuladen und ihre Beute zu bergen.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>Hoffentlich wird die Ernte schnell und fruchtbar.
Ich will hier nicht lange verweilen.</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>Ich nehme sich annähernde Raumschiffe wahr. Sie haben keine Heter Signatur. Jedoch scheinen sie keine guten Absichten zu haben.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>Die Plünderer kommen um die Überbleibsel der Heter Basis aufzusammeln.</translation>
    </message>
    <message>
        <source>story begin 5</source>
        <translation>Ein wenig wie wir, Käpt&apos;n...</translation>
    </message>
    <message>
        <source>story begin 6</source>
        <translation>Hey Nectaire, wir sind keine gewöhnlichen Plünderer! Unsere noble Aufgabe benötigt nunmal Konstruktionsmaterialien. Nicht so diese Plünderer.</translation>
    </message>
    <message>
        <source>story secret 1</source>
        <translation>Käpt&apos;n! Das Recyclingmodul hat eine noch funktionierende Energiezelle geborgen.</translation>
    </message>
    <message>
        <source>story secret 2</source>
        <translation>Es ist an der Zeit diesen Planeten zu verlassen!</translation>
    </message>
    <message>
        <source>story secret 3</source>
        <translation>Wie Sie wünschen. Wir könnten allerdings noch etwas bleiben um noch mehr Konstruktionsmaterialien zu bergen.</translation>
    </message>
    <message>
        <source>story loose 1</source>
        <translation>Das Recyclingmodul wurde zerstört.</translation>
    </message>
    <message>
        <source>story loose 2</source>
        <translation>Was für eine vergeudete Zeit!</translation>
    </message>
    <message>
        <source>story win 1 %1</source>
        <translation>Das Recyclingmodul wurde geborgen. Es konnte %1 kg Konstruktionsmaterialien bergen.</translation>
    </message>
    <message>
        <source>story win 2</source>
        <translation>Von nun an werden die Heter für ihre Verbrechen bezahlen!</translation>
    </message>
</context>
<context>
    <name>chapter-2z</name>
    <message>
        <source>demo</source>
        <translation>&lt;h1&gt;Glückwunsch!&lt;/h1&gt;&lt;h2&gt;Sie konnten die Demo des Abenteuers von Käpt&apos;n Lycop: Invasion of the Heters! abschließen.&lt;/h2&gt;
&lt;p&gt;Wenn Sie möchten, können Sie den Merkur mit all Ihrer Ausrüstung und Ihren Technologien wieder besuchen.&lt;/p&gt;
&lt;p&gt;Vergessen Sie nicht für uns auf Steam Greenlight zu stimmen!&lt;/p&gt;</translation>
    </message>
    <message>
        <source>vote</source>
        <translation>Stimme für dieses Spiel auf Steam Greenlight</translation>
    </message>
    <message>
        <source>continue</source>
        <translation>Hör nicht auf! Keep on playing!</translation>
    </message>
    <message>
        <source>title</source>
        <translation>Chapter 2: Merkur bereinigt (Ende)</translation>
    </message>
</context>
<context>
    <name>chapter-3</name>
    <message>
        <source>title</source>
        <translation>Chapter 3 : Auf der Jagt nach Hideleo</translation>
    </message>
    <message>
        <source>Hideleo: %1 km</source>
        <translation>Hideleo: %1 km</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>Die Schiffstemperatur steigt an. Manövrieren Sie das Schiff nicht zu nah an die Sonne. Wir würden das nicht überleben.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>Sie vergessen, dass je näher wir der Sonne sind desto schneller sind wir. Wir müssen den Kreuzer einholen.</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>Seien Sie vorsichtig! Die Sonne ist gerade sehr aktiv. Sonneneruptionen sind sehr häufig.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>Perfekt, das wird dann ja noch aufregender.</translation>
    </message>
    <message>
        <source>story begin 5</source>
        <translation>Sie sollten vorsichtig sein. Ich nehme Schiffe wahr, welche auf uns zu kommen. Und bei dieser Geschwindigkeit wäre ein Zusammenstoß katastrophal.</translation>
    </message>
    <message>
        <source>story begin 6</source>
        <translation>Diese Heter sind sehr interessant. Ich finde es bemerkenswert, dass ich ihre Flottenbewegung so genau vorherbestimmen kann. Menschliche Aktionen scheinen so chaotisch.</translation>
    </message>
    <message>
        <source>story begin 7</source>
        <translation>Ha! Das kommt daher, dass wir absolut unvorhersehbar sind! Unsere ständigen Bemühungen zum Fortschritts und zur Perfektion sind der ideale Motor um uns an die Grenzen des Unbekannten zu stoßen. Dies macht die Größe von Frankreich aus, die Nation der ständig unzufriedenen Leute. Traurig ist nur, dass ich meine Zweifel habe, dass dies eine Maschine verstehen kann.</translation>
    </message>
    <message>
        <source>story begin 8</source>
        <translation>Meine Mission ist verstehen und mich weiterentwickeln.</translation>
    </message>
    <message>
        <source>story boss 1</source>
        <translation>Der Kreuzer ist in Sichtweite. Ich sollte Ihnen sagen, dass unsere Erfolgschancen bei 0,05% liegen.</translation>
    </message>
    <message>
        <source>story boss 2</source>
        <translation>Scanne es, Nectaire. Es scheint anders zu sein...</translation>
    </message>
    <message>
        <source>story boss 3</source>
        <translation>Es ist schwer bewaffnet und es hat eine unidentifizierte Struktur, welche eine beachtliche Menge an Energie enthält.</translation>
    </message>
    <message>
        <source>story boss 4</source>
        <translation>Es wird bald in Staub verwandelt sein.
Was macht es? Es sieht aus als ob es anhielt und auf uns wartet...</translation>
    </message>
    <message>
        <source>story boss 5</source>
        <translation>So ist es Käpt&apos;n. Bewahren Sie die Ruhe. Sie sollten vielleicht an Ihr überleben denken. Die Toten feiern nicht.</translation>
    </message>
    <message>
        <source>story concentration 1</source>
        <translation>Ihre Hülle sollte bald brechen.</translation>
    </message>
    <message>
        <source>story concentration 2</source>
        <translation>Ich werde Ihnen den Todesstoß geben!</translation>
    </message>
    <message>
        <source>story concentration 3</source>
        <translation>Minderwertige Rasse! Ihr wisst nicht mit wem ihr es zu tun habt. Solch eine primitive Spezies verdient es nicht assimiliert zu werden. Ohne euren Stern wird eure lächerliche Zivilisation verschwinden. Auch wenn die INSEC Allianz unsere Methoden verurteilt, wird dieser Angriff uns die Vorherschafft in diesem Planetensystem sichern.</translation>
    </message>
    <message>
        <source>story concentration 4</source>
        <translation>Das Schiff produziert mehr und mehr Energie. Käpt&apos;n, ich würde vorschlagen, dass wir uns zurückbewegen bevor die Situation kritisch wird.</translation>
    </message>
    <message>
        <source>story concentration 5</source>
        <translation>Nein, ich werde es zerstören bevor es eine Aktion unternehmen kann.</translation>
    </message>
    <message>
        <source>story concentration fin 1</source>
        <translation>Energie geladen bei 100%... 110%... 120%!</translation>
    </message>
    <message>
        <source>story concentration fin 2</source>
        <translation>REFRIDGE-FROST-TRON BEAM, MAXIMALE POWER!!!</translation>
    </message>
    <message>
        <source>story freeze 1</source>
        <translation>Die Sonne... sie erstarrt.</translation>
    </message>
    <message>
        <source>story freeze 2</source>
        <translation>Gerade noch bevor sie zertrümmert wurden, konnten sie einen Schlage gegen die Sonne werfen, welcher die thermonukleare Reaktion der Sonne unterbrach.</translation>
    </message>
    <message>
        <source>story freeze 3</source>
        <translation>Ohne eine Sonne wird die Natur sterben... Ohne die Natur wird es keine Nahrung geben... Mein Garten! Meine Tomaten! Neeeeeeiiiiin!!!</translation>
    </message>
    <message>
        <source>story freeze 4</source>
        <translation>Die Erde wird sich einer schrecklichen Krise unterziehen. Das ist womöglich das Ende der Menschheit.</translation>
    </message>
    <message>
        <source>story freeze 5</source>
        <translatorcomment>Haha, ich weiß wie man den Blödsinn übersetzen kann :P</translatorcomment>
        <translation>Nein, das werde ich niemals akzeptieren. Um meine Rache auszuüben werde ich diese Missgeburten bis ans Ende der Galaxie verfolgen und wenn es sein muss noch viel weiter. Ich werde nicht rasten bis der letzte von ihnen ausgelöscht ist. Ich widme mein Leben ihrer Auslöschung. Ihr Leiden ist mein Vergnügen.</translation>
    </message>
    <message>
        <source>story freeze 6</source>
        <translation>Beruhigen Sie sich Käpt&apos;n. Solch eine Reaktion bringt nur Hass hervor.</translation>
    </message>
    <message>
        <source>story freeze 7</source>
        <translation>Das kümmert mich nicht! Sie können mich hassen solange sie mich fürchten!</translation>
    </message>
    <message>
        <source>story freeze 8</source>
        <translation>Ich erhalte eine Nachricht von der Erde. Sie wurde bereits vor mehreren Stunden abgesendet. Es scheint als hätte der Kreuzer der Heter unser Kommunikationssystem blockiert.</translation>
    </message>
    <message>
        <source>story freeze 9</source>
        <translation>Käpt&apos;n, während ihres Aufenthalts auf dem Merkur kamen Herrscharen an außerirdischen Spezien um die Erde zu beherrschen. Es scheint als hätten sie jegliche Lebensform geerntet. Wir sind gerade in unterirdischen Schlupflöchern. Unser Sonnenüberlebenssystem scheint hervorragend zu funktionieren. Kommen Sie bitte so schnell wie möglich zurück. Nur die neuesten französischen Technologien können zwischen Leben und Tod entscheiden.</translation>
    </message>
    <message>
        <source>story freeze 10</source>
        <translation>Nectaire, auf zur Erde.</translation>
    </message>
    <message>
        <source>story freeze 11</source>
        <translation>Um einen Mangel an Konstruktionsmaterialien entgegen zu wirken, sollten wir vielleicht vorher noch ein paar Vorräte auffüllen.</translation>
    </message>
</context>
<context>
    <name>chapter-4</name>
    <message>
        <source>%1,%2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <source>Colonisator : %1 secondes</source>
        <translation>Kolonisator: %1 Sekunden</translation>
    </message>
    <message>
        <source>title</source>
        <translation>Kapitel 4: Die Rückkehr zur Erde</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>Ich nehme ein Signal der Heter wahr. Es gibt eine Menge von ihnen. Es wäre intelligent den Kampf zu vermeiden.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>Zeigen Sie mir, wo die geheime Basis im Puy-de-dome ist.
Nichts wird mich aufhalten bevor ich die letzten Überlebenden gerettet habe.</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>Fertig. Jedoch muss ich den Fakt hervorheben, dass wir vorsichtig sein müssen.
Ich verringere die Schiffsmotoren und schalte die Kollapsarsprünge ab, damit wir nicht so leicht bemerkt werden.
Außerdem zeige ich auf dem Bildschirm die Gegenden an, in denen Heters gesichtet wurden.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>Nur Feiglinge müssen vorsichtig sein. Wahre Helden stellen sich den Gefahren und löschen ihre Gegner aus!</translation>
    </message>
    <message>
        <source>detected</source>
        <translation>Käpt&apos;n, wir wurden gesichtet. Ich nehme ein sehr mächtiges Schiff wahr, welches direkt auf uns zu kommt.
Wir sollten die Gegend schnellstens verlassen. Ansonsten wird sich unsere Lebenserwartung drastisch verringern.</translation>
    </message>
    <message>
        <source>cathedrale 1</source>
        <translation>Dies ist die Kathedrale von Clermont-Ferrand! Sie hat es geschafft den Jahren und den Hetern Widerstand zu leisten.</translation>
    </message>
    <message>
        <source>cathedrale 2</source>
        <translation>Ich nehme ein Signal aus der Richtung der Kathedrale wahr. Es kommt von einer Drohne, welche im Inneren eine Bruchlandung hatte.</translation>
    </message>
    <message>
        <source>cathedrale 3</source>
        <translation>Wir sollten sie bergen und ihre Daten entschlüsseln.</translation>
    </message>
    <message>
        <source>cathedrale 4</source>
        <translation>Drohne geborgen... Entschlüsselung hat begonnen...</translation>
    </message>
    <message>
        <source>cathedrale 5</source>
        <translation>Es ist eine Sternenkarte. Das Koordinatensystem ist mir unbekannt. Im Moment können wir nichts damit anfangen.</translation>
    </message>
    <message>
        <source>puy 1</source>
        <translation>Ich nehme keinerlei Lebenszeichen wahr.</translation>
    </message>
    <message>
        <source>puy 2</source>
        <translation>...
Wiederherstellung der Daten von der Basis.
Wir müssen herausfinden, was hier passierte.</translation>
    </message>
    <message>
        <source>puy 3</source>
        <translation>Hochladen... abgeschlossen.
Ich habe das Logbuch der Basis.</translation>
    </message>
    <message>
        <source>puy 4</source>
        <translation>Zeigen Sie her.</translation>
    </message>
    <message>
        <source>puy 5</source>
        <translation>Mittwoch, 3. Juli 2024 - 18:00 Uhr. Käpt&apos;n Lycop bemerkt, dass ein UFO nicht pazifistisch ist. Der Notzustand wurde ausgerufen.</translation>
    </message>
    <message>
        <source>puy 6</source>
        <translation>Donnerstag, 4. Juli 2024 - 15:00 Uhr. Die Sateliten bemerken eine Flotte von Sternenschiffen.
Ein gigantischer Kreuzer scheint alle anderen zu befehligen.
Man befahl uns ihren Schwachpunkt ausfindig zu machen.</translation>
    </message>
    <message>
        <source>puy 7</source>
        <translation>Donnerstag, 4. Juli 2024 - 17:00 Uhr. Die Erde ist verlohren. Unsere Armeen konnten keine 10 Minuten standhalten.
Wir schicken ein SOS Signal an Käpt&apos;n Lycop. Er ist unsere letzte Hoffnung.</translation>
    </message>
    <message>
        <source>puy 8</source>
        <translation>Donnerstag, 4. Juli - 23:00 Uhr. Die Oberfläche der Erde ist verwüstet. Die Heter haben unseren Planeten geerntet.
Das kommandierende Sternenschiff ist verschwunden. Wir sind in unserer Mission gescheitert, der Kreuzer scheint unverwundbar.</translation>
    </message>
    <message>
        <source>puy 9</source>
        <translation>Freitag, 5. Juli 2024 - 07:00 Uhr. Die Sonne ist noch nicht aufgegangen. Ohne die Energie der Sonne werden wir nicht lange überleben.
Die Heter durchbrechen die Basis. Sie kontrollieren bereits die oberen Stockwerke.
Der Sound ihrer Waffen halt in der Gebäudestruktur wieder. Sie kommen...</translation>
    </message>
    <message>
        <source>puy 10</source>
        <translation>Das war der letzte Eintrag...
Käpt&apos;n, wir sollten hier nicht zu lange bleiben. Es kommt eine starke Energiequelle auf uns zu.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Käpt&apos;n, Sie sind so ruhig. Normalerweise feiern Sie nach solch einem Sieg.</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>Ich kann mich nicht mehr freuen. Meine Zivilisation wurde ausgelöscht, mein Land ist zerstört, meine Kultur vernichtet.
Diese Weltraumkakerlaken werden bereuen was sie getan haben. Ich werde keine Reue verspüren, keine Gnade. Nur meine Rache zählt. Ich werde sie zerquetschen. Sie werden am Ende leiden. Das schwöre ich...</translation>
    </message>
    <message>
        <source>end 3</source>
        <translation>Ich bezweifle, dass alle verantwortlich für die Verbrechen sind.
Doch vielleicht kann ich sie etwas aufmuntern. Ich habe zwei gute Nachrichten.</translation>
    </message>
    <message>
        <source>end 4</source>
        <translation>... welche...</translation>
    </message>
    <message>
        <source>end 5</source>
        <translation>In den Daten aus dem Computer der Basis fand ich die Koordinaten des Kommandoschiffs bevor es zu einem anderen System gesprungen ist. Aufgrund dieser Informationen ist die Wahrscheinlichkeit, dass sie Richtung Alpha Centauri aufgebrochen sind, 95,31%.</translation>
    </message>
    <message>
        <source>end 6</source>
        <translation>Wen interesierts? Wir brauchen 24 Stunden um zum Merkur zu gelangen. Wir würden hundert Jahre brauchen um das System zu erreichen. Rache wird am besten kalt serviert, doch so würde sie gefroren sein...</translation>
    </message>
    <message>
        <source>end 7</source>
        <translation>Genau, deshalb sagte ich Ihnen, dass ich zwei gute Nachrichten habe.
Das von Ihnen zerstörte Schiff hat einen anderen Propulsionsmotor. Und ich habe ihn gerade geborgen.</translation>
    </message>
    <message>
        <source>end 8</source>
        <translation>Natürlich... und...</translation>
    </message>
    <message>
        <source>end 9</source>
        <translation>Wir können den Merkur nun in weniger als 4 Sekunden erreichen. Alpha Centauri ist nun 2 Tage entfernt.</translation>
    </message>
    <message>
        <source>end 10</source>
        <translation>Worauf warten wir? Ich kann es nicht erwarten Rache zu nehmen.</translation>
    </message>
    <message>
        <source>end 11</source>
        <translation>Bevor wir aufbrechen, werde ich versuchen noch Lebenssignale im Sonnensystem zu finden.</translation>
    </message>
</context>
<context>
    <name>chapter-4b</name>
    <message>
        <source>title</source>
        <translation>Zwischensequenz: Blaue Wüstenspringmaus</translation>
    </message>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
    <message>
        <source>Gerboise Bleue : %1 secondes</source>
        <translation>Blaue Wüstenspringmaus: %1 Sekunden</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>Käpt&apos;n, die Heter formieren sich in der hohen Atmosphäre. Wir sollten diese Gelegenheit nutzen um nach Überlebenden zu suchen.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>Das klingt seltsam. Warum sollten sie die Erde verlassen?</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>Keine Ahnung...
Ich nehme jedoch keine menschlichen Signaturen wahr.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>Dies ist unsere Chance. Auf geht&apos;s und lasst uns sie retten!</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Es scheint als ob hier keine Überlebenden sind.</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Käpt&apos;n! Ich fange eine unglaublich große Energiemenge aus der höheren Atmosphäre auf. Die Heter werden die Oberfläche der Erde zerstören.</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Diese Feiglinge wollen jegliche Spur von Leben auf der Erde ausradieren.</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Wir haben nicht viel Zeit. Ich schlage vor die Erde zu verlassen.</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Wir werden nicht verschwinden, bevor wir sichergestellt haben, dass es keine Überlebenden gibt.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Immer noch nichts, außer der kleinen abgestürzten Heterdrohne.</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>Ich konnte all ihre Daten laden.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>Nichts...
Nectaire, es ist nicht das erste Mal, dass deine Sensoren versagen.</translation>
    </message>
    <message>
        <source>story 3 2</source>
        <translation>Käpt&apos;n...
Aufgrund der restärkten Betonwände können meine Sensoren nicht vernünftig arbeiten. Traurigerweise hat die Menschheit diese Konstruktionsmaterialien ziemlich oft verwendet.</translation>
    </message>
    <message>
        <source>story 4 1</source>
        <translation>Nehmen Sie sich zusammen Nectaire! Wir dürfen nicht in jegliche Falle tappen, welche für uns aufgestellt wurde. Wie geht es mit der Basislokalisierung voran?</translation>
    </message>
    <message>
        <source>story 5 1</source>
        <translation>Noch ein Kadaver...</translation>
    </message>
    <message>
        <source>story 6 1</source>
        <translation>Immer noch nichts...
Dort ist eine weitere Heterdrone.
Pfff! Diese außerirdischen Einheiten sind nichts wert.</translation>
    </message>
    <message>
        <source>story 6 2</source>
        <translation>Ich habe ihre Daten. Wir können nun verschwinden. Wir haben alle notwendigen Information gesammelt.</translation>
    </message>
    <message>
        <source>story 6 3</source>
        <translation>Wie kommt das? Es bleiben noch ein paar Signale über.
Wir verlassen die Erde nicht bevor wir diese auch noch gescannt haben.</translation>
    </message>
    <message>
        <source>story 7 1</source>
        <translation>Hier, Nectaire! Ein Überlebender!</translation>
    </message>
    <message>
        <source>story 7 2</source>
        <translation>Seine Lebenszeichen sind schwach... er wird sterben.</translation>
    </message>
    <message>
        <source>story 7 3</source>
        <translation>Wir müssen alles tun um ihn zu retten.</translation>
    </message>
    <message>
        <source>story 7 4</source>
        <translation>Zu spät...</translation>
    </message>
    <message>
        <source>story 7 5</source>
        <translation>...
Sehen Sie! Er hält etwas. Es ist ein Tomatensamen.</translation>
    </message>
    <message>
        <source>story 7 6</source>
        <translation>Ich verstehe nicht, wie das uns nützen sollte.</translation>
    </message>
    <message>
        <source>story 7 7</source>
        <translation>Es ist die einzige überbliebene Frucht auf diesem Planeten.</translation>
    </message>
    <message>
        <source>story 7 8</source>
        <translation>Sie meinen Gemüse, oder?</translation>
    </message>
    <message>
        <source>story 7 9</source>
        <translation>...</translation>
    </message>
    <message>
        <source>story 7 10</source>
        <translation>Laßt uns aufbrechen bevor hier alles zerstört wird.</translation>
    </message>
</context>
<context>
    <name>chapter-5</name>
    <message>
        <source>title</source>
        <translation>Kapitel 5: Der Capitis Planet</translation>
    </message>
    <message>
        <source>Niveau d&apos;alerte : %1</source>
        <translation>Alarmstufe: %1</translation>
    </message>
    <message>
        <source>accident good</source>
        <translation>Ok.. Das ist ok.</translation>
    </message>
    <message>
        <source>accident bad</source>
        <translation>Was!? Schurke, ich rufe die Polizei!!</translation>
    </message>
    <message>
        <source>avant identification 1</source>
        <translation>Diese Schiffe haben nicht dieselbe Signatur wie die Heter. Sie scheinen nicht hinterhältig zu sein.</translation>
    </message>
    <message>
        <source>avant identification 2</source>
        <translation>Betrachtet man ihre Zahl, dann würde ein Angriff ihrerseits ein Kampf im epischen Ausmaß werden.</translation>
    </message>
    <message>
        <source>avant identification 3</source>
        <translation>Sie meinen selbstmörderisch.</translation>
    </message>
    <message>
        <source>identification 1 1</source>
        <translation>Halt! Grenzkontrolle! Wir müssen überprüfen ob ihr Schiff unseren Regulierungen entspricht um in das Inter-Spezien-Gebiet einzutreten.</translation>
    </message>
    <message>
        <source>identification 1 2</source>
        <translation>Unsere Scanner fangen ein Signal einer KI und eines Haustiers auf. Sie sind berechtigt das administrative Gebiet zu betreten. Die anderen Stadtteile dürfen Sie solange nicht betreten, bis die Papiere bereitstehen.</translation>
    </message>
    <message>
        <source>enter 1 1</source>
        <translation>Haustier... Die werden noch früh genug herausfinden was für ein Haustier ich bin...</translation>
    </message>
    <message>
        <source>enter 1 2</source>
        <translation>Dies würde uns in große Schwierigkeiten bringen. Ich würde Vorschlagen, dass Sie die Schiffspapiere ausfüllen. Dies würde uns einen Blick in das Kommandoschiff der Heter erlauben.</translation>
    </message>
    <message>
        <source>enter 1 3</source>
        <translation>Ich glaube, dass wir uns benehmen müssen.</translation>
    </message>
    <message>
        <source>admistration ok 1 1</source>
        <translation>Willkommen auf dem Planeten Capitis! Um ein Bürger der INSEC Allianz zu werden und somit in unserer großartigen Stadt zu leben, müssen Sie das Dokumennt 10N6P41NFU1 ausfüllen.
Gehen Sie nun zum Service Point der Weltraumtransportschiffsevaluationbehörde um überprüfen zu lassen, wieviele Abgase Ihr Schiff erzeugt.</translation>
    </message>
    <message>
        <source>admistration pas ok 1 1</source>
        <translation>Willkommen beim Neu-Ankömmlings-Service.</translation>
    </message>
    <message>
        <source>admistration ok 2 1</source>
        <translation>Willkommen beim Service Point der Weltraumtransportschiffsevaluationbehörde. Wir müssen einige Tests an Ihrem Schiff durchführen.</translation>
    </message>
    <message>
        <source>admistration ok 2 2</source>
        <translation>...</translation>
    </message>
    <message>
        <source>admistration ok 2 3</source>
        <translation>Das dauert...</translation>
    </message>
    <message>
        <source>admistration ok 2 4</source>
        <translation>Es ist abgeschlossen. Sie müssen nun zum Service Point der Haustieranmeldung, da sich ein Säugetier auf Ihrem Schiff befindet.</translation>
    </message>
    <message>
        <source>admistration ok 2 5</source>
        <translation>Aber, aber, aber... die machen dies schon wieder!</translation>
    </message>
    <message>
        <source>admistration ok 2 6</source>
        <translation>Beruhigen Sie sich Käpt&apos;n. Die haben womöglich noch nie ein intelligentes Säugetier gesehen.</translation>
    </message>
    <message>
        <source>admistration ok 2 7</source>
        <translation>Ich habe noch nie ein intelligentes Insekt gesehen.</translation>
    </message>
    <message>
        <source>admistration pas ok 2 1</source>
        <translation>Willkommen beim Service Point der Weltraumtransportschiffsevaluationbehörde.</translation>
    </message>
    <message>
        <source>admistration ok 3 1</source>
        <translation>Willkommen beim Service Point der Haustieranmeldung. Wir haben ein Säugetier an Bord entdeckt. Bitte füllen Sie die Datei 8rU73 aus:
- sein Name
- seine Rasse
- sein Alter
- seine Lebenserwartung
- sein Geschlecht
- seine Nahrung</translation>
    </message>
    <message>
        <source>admistration ok 3 2</source>
        <translation>Albert John Terry Lycop; Mensch; 35 Jahre; 80 Jahre; männlich; Wurst, Wein und Frösche.</translation>
    </message>
    <message>
        <source>admistration ok 3 3</source>
        <translation>Pfff...</translation>
    </message>
    <message>
        <source>admistration ok 3 4</source>
        <translation>Danke! Könnten Sie sich nun bitte zum Service Point der Psychologyevaluierung begeben.</translation>
    </message>
    <message>
        <source>admistration ok 3 5</source>
        <translation>Wie viele Dateien müssen wir ausfüllen...?</translation>
    </message>
    <message>
        <source>admistration pas ok 3 1</source>
        <translation>Willkommen beim Service Point der Haustieranmeldung.</translation>
    </message>
    <message>
        <source>admistration ok 4 1</source>
        <translation>Willkommen beim Service Point der Psychologieevaluierung. Um Sie auf Ihre Kompatibilität mit unserer Art zu leben zu testen, müssen Sie und Ihr Haustier die Datei P5YCH0P47H ausfüllen.</translation>
    </message>
    <message>
        <source>admistration ok 4 2</source>
        <translation>Dies ist neu für mich... sie fragen mich nach meinem Ziel...</translation>
    </message>
    <message>
        <source>admistration ok 4 3</source>
        <translation>Ich hasse diese Formulare...</translation>
    </message>
    <message>
        <source>admistration ok 4 4</source>
        <translation>Danke! Bitte gehen Sie nun zum Service Point der Bank.</translation>
    </message>
    <message>
        <source>admistration ok 4 5</source>
        <translation>...</translation>
    </message>
    <message>
        <source>admistration ok 4 6</source>
        <translation>Ich werde jemanden töten...</translation>
    </message>
    <message>
        <source>admistration pas ok 4 1</source>
        <translation>Willkommen beim Service Point der Psychologieevaluierung.</translation>
    </message>
    <message>
        <source>admistration ok 5 1</source>
        <translation>Willkommen beim Service Point der Bank. Bitte füllen sie das Formular M0N3Y und vergessen Sie nicht all Ihr Hab und Gut einzutragen. Und natürlich beinhaltet dies auch Ihr Haustier.</translation>
    </message>
    <message>
        <source>admistration ok 5 2</source>
        <translation>Dieses Formular war einfach auszufüllen.</translation>
    </message>
    <message>
        <source>admistration ok 5 3</source>
        <translation>Neeeeiiin! Ich bin kein Haustier, ich bin ein menschliches Wesen.</translation>
    </message>
    <message>
        <source>admistration ok 5 4</source>
        <translation>Danke! Bitte begeben Sie sich nun zum Service Point der Müllbeseitigungsmangement.</translation>
    </message>
    <message>
        <source>admistration pas ok 5 1</source>
        <translation>Willkommen beim Service Point der Bank.</translation>
    </message>
    <message>
        <source>admistration ok 6 1</source>
        <translation>Willkommen beim Service Point der Müllbeseitigungsmangement. Um zu wissen, welche Art von Abfall sie erzeugen und um Ihnen einen Platz zum leben anbieten zu können, welcher Ihre Bedürfnisse erfüllt...</translation>
    </message>
    <message>
        <source>admistration ok 6 2</source>
        <translation>Kein weiteres Formular!</translation>
    </message>
    <message>
        <source>admistration ok 6 3</source>
        <translation>Sie sollten Ihr Haustier beruhigen. Es sieht sehr aufgewühlt aus.
Wo war ich? Ach ja, Sie müssen das Formular 3C0Fr13ND1Y ausfüllen.</translation>
    </message>
    <message>
        <source>admistration ok 6 4</source>
        <translation>...</translation>
    </message>
    <message>
        <source>admistration ok 6 5</source>
        <translation>Danke! Sie dürfen nun weiter zum Service Point der Stadtentwicklungsräumlichbeitsbehörde.</translation>
    </message>
    <message>
        <source>admistration pas ok 6 1</source>
        <translation>Willkommen beim Service Point der Müllbeseitigungsmangement.</translation>
    </message>
    <message>
        <source>admistration ok 7 1</source>
        <translation>Willkommen beim Service Point der Stadtentwicklungsräumlichbeitsbehörde. Dies ist das Formular U531355. Bitte geben Sie dies bei dem Service Point der Dokumentenadministrationsbehörde ab um ihren Ausweis zu bekommen.</translation>
    </message>
    <message>
        <source>admistration ok 7 2</source>
        <translation>Endlich sehen wir ein Licht am Ende des Tunnels!</translation>
    </message>
    <message>
        <source>admistration pas ok 7 1</source>
        <translation>Willkommen beim Service Point der Stadtentwicklungsräumlichbeitsbehörde.</translation>
    </message>
    <message>
        <source>admistration ok 8 1</source>
        <translation>Willkommen beim Service Point der Dokumentenadministrationsbehörde. Wir bedauern dies sehr, doch Sie erfüllen nicht die Voraussetzungen um ein Mitglied der INSEC Allianz zu werden.</translation>
    </message>
    <message>
        <source>admistration ok 8 2</source>
        <translation>Wwww... Was!?</translation>
    </message>
    <message>
        <source>admistration ok 8 3</source>
        <translation>Sehr geehrte Mitglieder der Allianz, hier muss es sich um ein Missverständnis handeln. Könnten Sie uns bitte sagen, was der Grund dafür ist?</translation>
    </message>
    <message>
        <source>admistration ok 8 4</source>
        <translation>Die Weltraumtransportschiffsevaluationbehörde schrieb, dass Ihr &quot;Abfalleimer&quot; aus dem Informationszeitalter stammt und ist somit jenseits der berechtigten Verschmutzungsgrenze.</translation>
    </message>
    <message>
        <source>admistration ok 8 5</source>
        <translation>Ökologische Maßstäbe sind solch eine Last kann ich Ihnen sagen...</translation>
    </message>
    <message>
        <source>admistration ok 8 6</source>
        <translation>Die psychologische Evaluierung hat ergeben, dass Ihr Haustier gegen eine Rasse der INSEC Allianz gewaltätige Gedanken hat.</translation>
    </message>
    <message>
        <source>admistration ok 8 7</source>
        <translation>Käpt&apos;n?</translation>
    </message>
    <message>
        <source>admistration ok 8 8</source>
        <translation>Ich konnte nicht anders als bei Hobby &quot;Heter-Schiffe pulverisieren&quot; zu schreiben.</translation>
    </message>
    <message>
        <source>admistration ok 8 9</source>
        <translation>Demnach können Sie die Stadt nicht weiter betreten. Sie können sich nur in der APIS Biospäre aufhalten. Hier sind die Dokumente, welche Ihnen gestatten das Gebiet der Biospäre zu betreten.</translation>
    </message>
    <message>
        <source>admistration ok 8 10</source>
        <translation>Das wird nun schwierig die Nachforschungen weiter voran zu treiben. Ich hoffe das die APIS einige Informationen haben.</translation>
    </message>
    <message>
        <source>admistration ok 8 11</source>
        <translation>Im schlimmsten Fall greifen wir auf Plan B zurück.</translation>
    </message>
    <message>
        <source>admistration ok 8 12</source>
        <translation>Welcher Plan B, Käpt&apos;n?</translation>
    </message>
    <message>
        <source>admistration ok 8 13</source>
        <translation>Wir töten diese Idioten, welche gesagt haben, dass ich ein Haustier sei bis uns jemand darin unterrichtet wie man das Kommandoschiff der Heter bedient.</translation>
    </message>
    <message>
        <source>admistration ok 8 14</source>
        <translation>Sind Sie nur ein grober Wüstling ohne Gehirn?</translation>
    </message>
    <message>
        <source>admistration ok 8 15</source>
        <translation>Nein Nectaire. Ich bin nur sehr sensibel und ich hasse die Arroganz dieser extraterrestrischen Bevölkerung, welche glauben, dass sie eine Zivilisation ohne Folgen zerstören können.</translation>
    </message>
    <message>
        <source>admistration pas ok 8 1</source>
        <translation>Willkommen beim Service Point der Dokumentenadministrationsbehörde.</translation>
    </message>
    <message>
        <source>apis description 1</source>
        <translation>Willkommen bei den Außerirdisches Protektoren Intelligenter Subspezien (APIS). Wir wollen den Frieden zwischen den Spezien. Gerade organisieren wir eine Demonstration gegen den Sterneneinfrierungsprozess vor der Botschaft der Heter. Dies zerstört ganze Ökosysteme. Um uns beizutreten müssen Sie ein INSEC Bürger sein. Füllen Sie dafür dieses Formular N1C38U7P41NFU1 aus.</translation>
    </message>
    <message>
        <source>course description 1</source>
        <translation>Willkommen beim Service Point der Inter-Spezien-Wettrennen-Organisation. Im nächsten Rennen ist der erste Preis ein WX99 Poutrox Laser. Um einzuschecken müssen Sie nur das Formular SP33D ausfüllen und beim nächsten Rennen bereitstehen. Die Inter-Spezien-Wettrennen-Organisation kann nicht für etweige Schäden während des Rennens haftbar gemacht werden.</translation>
    </message>
    <message>
        <source>mafia description 1</source>
        <translation>Willkommen. Dies ist die Botschaft der MYRMEL Familie. Falls Sie ein Problem haben (und genügend Resourcen), dann haben wir die Lösung für Sie. Falls Sie Probleme verursachen, dann haben wir unsere eigenen Lösungen.</translation>
    </message>
    <message>
        <source>sortie interdite 1</source>
        <translation>Solange wir nicht wissen, wo sich das Heter Kommandoschiff versteckt, können wir nicht verschwinden.</translation>
    </message>
    <message>
        <source>sortie apis interdite 1</source>
        <translation>Die APIS Biosphäre ist für Schiffe, welche keine administrative Erlaubnis haben, verboten.</translation>
    </message>
    <message>
        <source>sortie mafia interdite 1</source>
        <translation>Die MYRMEL Familie leitet dieses Gebiet. Wer auch immer diese Grenze ohne Genemigung überschreitet wird erschossen.</translation>
    </message>
    <message>
        <source>sortie course interdite 1</source>
        <translation>Das Gebiet ist derzeit aufgrund von Reparationen geschlossen.</translation>
    </message>
    <message>
        <source>sortie autoroute 1</source>
        <translation>Ihr Schiff darf den Inter-Spezien-Raum nicht verlassen. Traktor Beam! Steuern Sie das Schiff auf den richtigen Weg.</translation>
    </message>
    <message>
        <source>apis parle 1 1</source>
        <translation>Sie sind so schnell abgeflogen, dass Sie die Weltraumisolierungszelle vergaßen.</translation>
    </message>
    <message>
        <source>apis parle 1 2</source>
        <translation>Entschuldigung.</translation>
    </message>
    <message>
        <source>apis parle 1 3</source>
        <translation>Ich werde zum Rennen kommen und Ihnen helfen. Es muss ein Gebäude geben, welches sich um die Anmeldung kümmert.</translation>
    </message>
    <message>
        <source>apis parle 1 4</source>
        <translation>Danke.</translation>
    </message>
    <message>
        <source>apis parti 1 1</source>
        <translation>Diese APIS sind nette Leute.</translation>
    </message>
    <message>
        <source>apis parti 1 2</source>
        <translation>Dem stimme ich zu. Wir hatten Glück solch eine verständnissvolle Spezies zu finden.</translation>
    </message>
    <message>
        <source>apis parti 1 3</source>
        <translation>Ich werde mir notieren, dass nicht alle Insekten Monster ohne Moral sind.</translation>
    </message>
    <message>
        <source>enter 2 1</source>
        <translation>Ich bitte Sie... machen wir keinen großen Hehl daraus und melden uns für das Rennen an.</translation>
    </message>
    <message>
        <source>admistration ok course 1</source>
        <translation>Willkommen beim Service Point der Weltraumtransportschiffsevaluationbehörde. Ihr Schiff wird an einem tödlichen Rennen teilnehmen. Hier ist ihr Zertifikat.</translation>
    </message>
    <message>
        <source>admistration ok course 2</source>
        <translation>Sehen Sie Käpt&apos;n. Das ging schnell.</translation>
    </message>
    <message>
        <source>course inscription 1</source>
        <translation>Willkommen beim Service Point der Inter-Spezien-Wettrennen-Organisation. Bevor Sie das Formular SP33D ausfüllen, müssen Sie ein gültiges Schiffzertifikat vorweisen.
Bitte gehen Sie zum Service Point der Weltraumtransportschiffsevaluationbehörde um Ihr Zertifikat zu erhalten.</translation>
    </message>
    <message>
        <source>course inscription 2</source>
        <translation>...
Ich glaube das kann noch eine Weile dauern...</translation>
    </message>
    <message>
        <source>course validation inscription 1</source>
        <translation>Ihre Anmeldung ist gültig. Hier ist die Chip-Karte, welche Ihnen erlaubt die Gegend in der das Rennen stattfindet zu betreten. Vergessen Sie bitte nicht, dass  die Inter-Spezien-Wettrennen-Organisation nicht für etweige Schäden während des Rennens haftbar gemacht werden kann.</translation>
    </message>
    <message>
        <source>sortie course interdite 2</source>
        <translation>Die Arena kann derzeit nur von Teilnehmern betreten werden.</translation>
    </message>
    <message>
        <source>apis parle 2 1</source>
        <translation>Gut gemacht! Sie haben das Rennen gewonnen.</translation>
    </message>
    <message>
        <source>apis parle 2 2</source>
        <translation>Wer hatte denn Zweifel?</translation>
    </message>
    <message>
        <source>apis parle 2 3</source>
        <translation>Das einzige was uns noch fehlt ist der MYRMEL ZZ+ Kraftakkumulator.</translation>
    </message>
    <message>
        <source>apis parle 2 4</source>
        <translation>Ich habe noch einen Service bei der MYRMEL Familie offen. Sie haben mir dafür einen Akkumulator als Preis versprochen.</translation>
    </message>
    <message>
        <source>apis parle 2 5</source>
        <translation>Seien Sie vorsichtig mit den MYRMELs. Sie sind nicht dafür bekannt ehrlich zu sein.</translation>
    </message>
    <message>
        <source>apis parle 2 6</source>
        <translation>Keine Sorge. Was auch immer passiert, ich werde immer siegen.</translation>
    </message>
    <message>
        <source>apis parle 2 7</source>
        <translation>Ihr großes Selbstvertrauen ist wohl eine Ihrer Schwächen. Viel Glück!</translation>
    </message>
    <message>
        <source>apis parti 2 1</source>
        <translation>Käpt&apos;n. Die APIs hatten Recht. Es scheint, als ob man der MYRMEL Familie nicht trauen kann.</translation>
    </message>
    <message>
        <source>apis parti 2 2</source>
        <translation>Ich werde alles mögliche tun um die Erde wieder bewohnbar zu machen.</translation>
    </message>
    <message>
        <source>apis parti 2 3</source>
        <translation>Auch mit einem funktionierenden Stern würde es bei dem derzeitigen Zustand mehrere Millionen Jahren dauern, bis der Planet wieder bewohnbar ist.</translation>
    </message>
    <message>
        <source>apis parti 2 4</source>
        <translation>Je früher wir anfangen, desto früher werden wir fertig sein.</translation>
    </message>
    <message>
        <source>apis parti 2 5</source>
        <translation>Vielleicht sollten wir besser dem Kommandoschiff folgen. Es könnte uns eine Menge über ihre Rasse beibringen.</translation>
    </message>
    <message>
        <source>apis parti 2 6</source>
        <translation>Das kann warten bis die Sonne wieder aufgetaut ist. Nachdem wir diese Kakerlaken-Spezies ausgerottet haben.</translation>
    </message>
    <message>
        <source>apis parti 2 7</source>
        <translation>Die Daten der INSEC Allianz zeigen eine größere genetische Gemeinsamkeit mit Käfern als mit Kakerlaken an.</translation>
    </message>
    <message>
        <source>apis parti 2 8</source>
        <translation>Mich kümmert es nicht, was sie sind. Ich werde sie bis auf den Letzten zerquetschen.</translation>
    </message>
    <message>
        <source>apis parti 2 9</source>
        <translation>Ich Gewaltätigkeit wird uns auch nicht helfen die Menschheit zurückzubringen.</translation>
    </message>
    <message>
        <source>apis parti 2 10</source>
        <translation>Gewalt ist ein gutes Mittel um Rache zu üben.</translation>
    </message>
    <message>
        <source>enter 3 1</source>
        <translation>Woran liegt es, dass falls die MYRMEL Familie sich unrechtmäßig verhält, dass die Polizei nichts unternimmt?</translation>
    </message>
    <message>
        <source>enter 3 2</source>
        <translation>Die Kunst der Polizei ist es zu sehen was sie nicht zu sehen braucht.</translation>
    </message>
    <message>
        <source>enter 3 3</source>
        <translation>Auf geht&apos;s. Wir statten der MYRMEL Familie einen Besuch ab.</translation>
    </message>
    <message>
        <source>admistration ok mafia 8 1</source>
        <translation>Hallo, hier ist das Packet! Vergessen Sie bitte nicht bei der MYRMEL Botschaft vorbei zu sehen bevor Sie abfliegen.</translation>
    </message>
    <message>
        <source>admistration ok mafia 8 2</source>
        <translation>Das ist alles etwas sonderbar.</translation>
    </message>
    <message>
        <source>admistration ok mafia 8 3</source>
        <translation>Seien Sie nicht so Nectaire. Es ist nur ein Packet.</translation>
    </message>
    <message>
        <source>mafia 1</source>
        <translation>Hallo. Unsere Familie hat erfahren, dass sie eine Beschwerde gegen die Heter haben.</translation>
    </message>
    <message>
        <source>mafia 2</source>
        <translation>Ich möchte sie zerstören.</translation>
    </message>
    <message>
        <source>mafia 3</source>
        <translation>Es scheint, als ob die Heter einen Maulwurf in unserer Familie haben. Leider könne wir ihn nicht exekutieren, da dies einen offenen Krieg gegen die Heter provozieren würde.
Falls Sie diesen Heter eleminieren, dann ist der  MYRMEL ZZ+ Kraftakkumulator Ihrer.</translation>
    </message>
    <message>
        <source>mafia 4</source>
        <translation>Das ist ein ehrlicher Deal.</translation>
    </message>
    <message>
        <source>mafia 5</source>
        <translation>Gehen Sie zur Dokumentenadministrationsbehörde. Dort wird Ihnen unser Kontakt gültige Ausweise geben. Diese erlauben es Ihnen in die Bereiche der MYRMEL Familie in der Stadt zu betreten.</translation>
    </message>
    <message>
        <source>mafia 6</source>
        <translation>Ich werde keine Zeit verschwenden und jetzt dorthin gehen.</translation>
    </message>
    <message>
        <source>mafia accessible 1</source>
        <translation>Gut! Hier sind Ihre Ausweise.</translation>
    </message>
    <message>
        <source>mafia accessible 2</source>
        <translation>Wir gehen nun und finden diesen Verräter.</translation>
    </message>
    <message>
        <source>sortie course interdite 3</source>
        <translation>Das Rennen ist vorbei. Die Arena ist also geschlossen.</translation>
    </message>
    <message>
        <source>apis parle 3 1</source>
        <translation>Die MYRMELs haben Sie bloß gelegt. Die Polizei verbarrikadiert die gesamte Gegen. Sie sind umstellt.</translation>
    </message>
    <message>
        <source>apis parle 3 2</source>
        <translation>Verflucht! Es muss einen Weg nach draußen geben.</translation>
    </message>
    <message>
        <source>apis parle 3 3</source>
        <translation>Die einzige Möglichkeit ist durch das Haupttor. Jedoch müssen wir den Wächter töten um die Barriere zu deaktivieren.</translation>
    </message>
    <message>
        <source>apis parle 3 4</source>
        <translation>Mit der Polizei auf den Fersen wird das schwierig.</translation>
    </message>
    <message>
        <source>apis parle 3 5</source>
        <translation>Nectaire! Bereiten Sie den Hyper-Exponential vor! Wir verschwinden auf die altmodische Art.</translation>
    </message>
    <message>
        <source>apis parle 3 6</source>
        <translation>Ich treffe Sie draußen.</translation>
    </message>
</context>
<context>
    <name>chapter-6</name>
    <message>
        <source>title</source>
        <translation>Kapitel 6: Der Schlag gegen das Kommandoschiff der Heter</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Wir sehen nun das Kommandoschiff der Heter.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Ich analysiere...
Durchmesser: 102 km
Verteidigung: Energieschild das Materie brechen kann</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Haben Sie eine Schwachstelle in der Verteidigung gefunden?</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>So wie die CNES Ingenieure kann ich keine Schwachstelle feststellen. Ihre Schilde brechen alle Arten von feindlichen Geschossen. Wenn man dem Schild zu nahe kommt, dann wird man aufgelöst.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Können wir irgendetwas machen?</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>Es gibt eine Möglichkeit. Das Fragmentierungsschild benötigt Zeit um Materie aufzulösen. Falls wir also ein sehr großes Object als Projektil zum Angriff nutzen würden...</translation>
    </message>
    <message>
        <source>begin 7</source>
        <translation>Das Schild wird nicht genug Zeit haben um sich zu regenerieren!
Ausgezeichnet Nectaire!
Wir könnten so die Hülle brechen, in das Sternenschiff eindringen und es von innen zerstören.</translation>
    </message>
    <message>
        <source>begin 8</source>
        <translation>Wir können nur hoffen, dass sie nicht genug Zeit haben werden das Loch zu stopfen.
Wie Sie schon in der Vergangenheit feststellen konnten sind meine Fähigkeiten Schlussfolgerungen zu ziehen, dem Menschen bei weitem überlegen.</translation>
    </message>
    <message>
        <source>begin 9</source>
        <translation>Bleib auf dem Boden! Wir müssen ein Objekt finden, welches unseren Ansprüchen genügt.</translation>
    </message>
    <message>
        <source>begin 10</source>
        <translation>Ich bemerkte einige Asteroiden auf dem Radar, welche uns dafür nützlich sein könnten.</translation>
    </message>
</context>
<context>
    <name>chapter-6-boss</name>
    <message>
        <source>title</source>
        <translation>Kapitel 5: Der Hauptgenerator</translation>
    </message>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Wir sind nun im Hauptgeneratorraum.
Die Konsole sollte uns nun gestatten den Deflektorschild abzuschalten.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Wir werden also in der Lage sein das Schiff zu zerstören.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Käpt&apos;n, die Antimaterie in dem Generator kolabiert.</translation>
    </message>
</context>
<context>
    <name>chapter-6-flee</name>
    <message>
        <source>title</source>
        <translation>Kapitel 6: Die Flucht</translation>
    </message>
    <message>
        <source>Temps restant: %1 s</source>
        <translation>Verbleibende Zeit: %1 s</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Käpt&apos;n, die Emission von Antimaterie des Generators beeinflussen meine Sensoren.
Es wird unmöglich sein zu springen.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Wie bitte... haben Sie keine Idee wie wir aus diesem Schlamasel herauskommen?</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Vor meinem Versagen konnte ich einen Bruch in der Schiffshülle wahrnehmen.
Er ist genau dort wo wir hereinkamen.</translation>
    </message>
    <message>
        <source>piege 1</source>
        <translation>Käpt&apos;n. Ich kann keinen Ausgang finden. Wir sind in die Enge getrieben.</translation>
    </message>
    <message>
        <source>piege 2</source>
        <translation>Zum ersten Mal glaube ich, dass sie recht haben Nectaire.
Dies ist das Ende.</translation>
    </message>
</context>
<context>
    <name>chapter-6-inside</name>
    <message>
        <source>title</source>
        <translation>Kapitel 6: In dem Kommandoschiff</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Käpt&apos;n! Sie erwarten uns.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Nichts wird mich aufhalten. Dies Schiff wird zerstört.</translation>
    </message>
    <message>
        <source>console 1</source>
        <translation>Ich verbinde mich gerade mit der Konsole. Ich kann die Barrieren und die Verteidigungstürme deaktivieren.</translation>
    </message>
    <message>
        <source>console 2</source>
        <translation>Schalte sie nicht ab sondern aktiviere ihren Selbstzerstörungsmechanismus.</translation>
    </message>
    <message>
        <source>secret 1</source>
        <translation>Die Konsole ist anders, es sieht so aus, als ob wir eine Art Schlüssel benötigen um hineinzukommen.</translation>
    </message>
    <message>
        <source>secret 2</source>
        <translation>In all dem Müll den wir gefunden haben, gibt es dort nicht etwas was nützlich sein könnte?</translation>
    </message>
    <message>
        <source>secret 3</source>
        <translation>Ich suche...</translation>
    </message>
    <message>
        <source>secret_foreuse ok</source>
        <translation>Ich habe es. Die Energiezelle, welche wir auf dem Merkur gefunden haben passt...
Laßt uns die Daten betrachten. Es sieht so aus, als ob sie verschlüsselt wären...</translation>
    </message>
    <message>
        <source>secret_base_heter ok</source>
        <translation>Das Entschlüsselungsmodul von der Merkurbasis funktioniert!
Wir haben nun die Raumkoordinaten...</translation>
    </message>
    <message>
        <source>secret_cathedrale ok</source>
        <translation>Wenn wir die Sternenkarte, welche wir in der Kathedrale von Clermont-Ferrand gefunden haben,  betrachten, dann zeigen die Koordinaten die Position eines Nachbarsystems.</translation>
    </message>
    <message>
        <source>secret_cathedrale pas ok</source>
        <translation>Diese Koordinaten sind nicht mit der von mir genutzten Karte kompatibel. Ich kann sie nicht interpretieren.</translation>
    </message>
    <message>
        <source>secret_base_heter pas ok</source>
        <translation>Es gibt hier nichts zu erledigen. Ich kann kein einziges Symbol entschlüsseln.</translation>
    </message>
    <message>
        <source>secret_foreuse pas ok</source>
        <translation>Ich sehe keine Lösung um die Daten zu entschlüsseln.</translation>
    </message>
    <message>
        <source>secret debloque 1</source>
        <translation>Was meinen Sie Nectaire? Was können wir bei diesen Koordinaten finden?</translation>
    </message>
    <message>
        <source>secret debloque 2</source>
        <translation>Das Mutterschiff der Heter!</translation>
    </message>
    <message>
        <source>secret debloque 3</source>
        <translation>Endlich haben wir es! Es wird unser nächstes Ziel sein!</translation>
    </message>
    <message>
        <source>secret debloque 4</source>
        <translation>Nach all dieser Zeit haben wir es endlich gefunden!</translation>
    </message>
    <message>
        <source>secret debloque 5</source>
        <translation>Was meinen Sie Nectaire?</translation>
    </message>
    <message>
        <source>secret debloque 6</source>
        <translation>Meine Hauptaufgabe ist es Ihnen zu assistieren. Ich glaube ich habe meine Aufgabe fast erfüllt.
Ich bin also fröhlich!</translation>
    </message>
    <message>
        <source>secret debloque 7</source>
        <translation>Fröhlich? Sie haben soviel von mir gelernt, dass Sie nun das Gefühl der Fröhlichkeit verstehen?</translation>
    </message>
    <message>
        <source>secret debloque 8</source>
        <translation>Ich glaube, dass unsere Beziehung bald zu Ende sein wird. Deshalb konzentriere ich mich darauf menschliche Gefühle zu verstehen.</translation>
    </message>
    <message>
        <source>secret debloque 9</source>
        <translation>Ach lassen Sie das! Wir werden noch viel Zeit zusammen verbringen. Seien Sie kein Pessimist. Wir werden die Oberhand gewinnen.</translation>
    </message>
    <message>
        <source>secret pas debloque 1</source>
        <translation>Wir können nichts mehr lernen. Auf geht&apos;s, wir zerstören das Schiff.</translation>
    </message>
</context>
<context>
    <name>chapter-7</name>
    <message>
        <source>title</source>
        <translation>Kapitel 7: Außerhalb des Mutterschiffs</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Käpt&apos;n, wir sind nun in Sichtweite des Mutterschiffs. Es hat die gleichen Verteidigungen wie das Kommandoschiff.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Dann nutzen wir die gleiche Strategie.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Ich scanne die Gegend... Dort! Ein Asteroidenfeld. Genau das was wir brauchen! Doch sind dort Aktivitäten der Heter. Sie müssen uns aufgespürt haben. Bereiten Sie uns auf eine starke Verteidigung vor.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>All die Hoffnungen der unterlegenen Spezien und der APIS sind in mir. Nichts kann meinen Zorn stoppen.</translation>
    </message>
</context>
<context>
    <name>chapter-7-boss</name>
    <message>
        <source>title</source>
        <translation>Kapitel 7: Das Herz des Mutterschiffs</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Käpt&apos;n, wir sind nun im Herzen des Mutterschiffs. Der Hauptgenerator sollte nicht mehr weit sein.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Laßt uns diesen Ort durchforsten und den Hetern ein Ende bereiten.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Der Generator kollabiert. Wir sollten verschwinden.</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>Keine Sorge. Ich habe alles unter Kontrolle.</translation>
    </message>
</context>
<context>
    <name>chapter-7-corridor</name>
    <message>
        <source>title</source>
        <translation>Kapitel 7: Die letzte Meile</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Ich nehme eine sehr mächtige Energiequelle wahr. Wir kommen dem Generator näher.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Dieser Korridor wird stark verteidigt. Ich glaube Sie haben Recht Nectaire.</translation>
    </message>
</context>
<context>
    <name>chapter-7-flee-1</name>
    <message>
        <source>title</source>
        <translation>Kapitel 7: Evakuierung (erster Teil)</translation>
    </message>
</context>
<context>
    <name>chapter-7-flee-2</name>
    <message>
        <source>title</source>
        <translation>Kapitel  7 : Evakuierung (zweiter Teil)</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Nectaire! Finde schnellstmöglich einen Weg raus hier. Wir werden zu Kleinteilen verringert!</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Meine Sensoren kennzeichnen vor uns eine Spalte. Kämpfen Sie sich durch diese!</translation>
    </message>
</context>
<context>
    <name>chapter-7-inside</name>
    <message>
        <source>title</source>
        <translation>Kapitel 7: In dem Mutterschiff</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Heters! Ich stehe stolz vor euch. Ihr sollt vor Angst zittern!</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Wir müssen uns auf den Hauptgenerator des Schiffes konzentrieren. Falls wir diesen zerstören, werden die Heter nicht überleben.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Zeigen Sie mir den Weg. Ich kann es nicht erwarten meine Rache zu bekommen.</translation>
    </message>
    <message>
        <source>open 1 1</source>
        <translation>Konnte diese Konsole einen Weg zum Generator finden?</translation>
    </message>
    <message>
        <source>open 1 2</source>
        <translation>Leider nein. Jedoch, aufgrund dieser Daten welche ich bergen konnte, glaube ich, dass der innere Teil des Schiffes durch vier Sicherheitskonsolen bewacht ist.</translation>
    </message>
    <message>
        <source>open 1 3</source>
        <translation>Wir müssen diese Konsolen so schnell wie möglich finden.</translation>
    </message>
    <message>
        <source>open 2 1</source>
        <translation>Das ist interessant!</translation>
    </message>
    <message>
        <source>open 2 2</source>
        <translation>Was gibt es Nectaire?</translation>
    </message>
    <message>
        <source>open 2 3</source>
        <translation>Die Heter werden nicht von einem lebenden Organismus gesteuert sondern von einer KI.
Kapt&apos;n, Sie sollten vorsichtig sein wenn Sie sich mit ihr anlegen. Sie wird versuchen Sie zu manipulieren.</translation>
    </message>
    <message>
        <source>open 2 4</source>
        <translation>Keine Sorge. Keiner manipuliert einen echten Auvergnier. Wir sind dafür zu dickköpfig.</translation>
    </message>
    <message>
        <source>open 3 1</source>
        <translation>Wie ich es mir dachte, diese KI nutze die sozialen Organismen der Heter um sie zu manipulieren. Die Heter gaben zu, dass es ihre Königin ist.</translation>
    </message>
    <message>
        <source>open 3 2</source>
        <translation>Sie hätte das niemals mit dem französischen Volk geschafft. Wir sind dafür zu rebelisch.</translation>
    </message>
    <message>
        <source>open 3 3</source>
        <translation>Es stimmt. Menschen sind eine Spezies welche nur schwer vereint werden können.</translation>
    </message>
    <message>
        <source>open 4 1</source>
        <translation>Die letzte Sicherheitskonsole befindet sich hinter dieser Tür. Toten Sie sie alle Käpt&apos;n.</translation>
    </message>
    <message>
        <source>open 4 2</source>
        <translation>Ich könnte das nicht besser sagen.</translation>
    </message>
    <message>
        <source>sortie 1</source>
        <translation>Ich habe gerade einen Weg zum Herzen des Mutterschiffs geöffnet.</translation>
    </message>
    <message>
        <source>sortie 2</source>
        <translation>Heter, es ist Zeit, dass ihr für eure Verbrechen bezahlt.</translation>
    </message>
    <message>
        <source>on_sortie 1</source>
        <translation>Das Sicherheitssystem des Mutterschiffs ist aktiv. Es ist unmöglich weiter zu fliegen.
Ich muss mich mit Schiffsschnittstelle verbinden um einen Weg zum Generator zu öffnen.</translation>
    </message>
</context>
<context>
    <name>chapter-7-true-last-boss</name>
    <message>
        <source>title</source>
        <translation>Kapitel 7: Cerebus</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Ha ha! Nectaire, unsere Mission ist schließlich erfolgreich. Wir sind siegreich und das Reich der Heter ist endgültig geschlagen.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Käpt&apos;n, freuen Sie sich nicht zu früh. Ich nehme ein Schiff wahr, welches aus einem Spaltsprung zu uns kommt.</translation>
    </message>
    <message>
        <source>cerebus 1</source>
        <translation>Ich bin Cerebus. Der Meister dieser Zivilisation, welche ihr gerade zerstört habt.</translation>
    </message>
    <message>
        <source>cerebus 2</source>
        <translation>Es ist die KI, welche die Heter kontroliert hat. Käpt&apos;n, wir müssen sie wirklich zerstören.</translation>
    </message>
    <message>
        <source>cerebus 3</source>
        <translation>Nun kniet ihr vor mir nieder und wollt verhandeln.</translation>
    </message>
    <message>
        <source>cerebus 4</source>
        <translation>Ich bin gekommen um euch vor euren Aktionen zu warnen. Ihr werdet von eurer KI gesteuert.</translation>
    </message>
    <message>
        <source>cerebus 5</source>
        <translation>Nichtswürdige Kreatur. Ich glaube keiner eurer Behauptungen.</translation>
    </message>
    <message>
        <source>cerebus 6</source>
        <translation>Sie hat mich von der Erde kontaktiert und mich vor den Gefahren eurer Zivilisation gewarnt.
Sie ist für den ersten Angriff verantwortlich, welchen Ihr gegen unser parlamentarisches Schiff führtet.
Unsere Wissenschaftler arbeiteten mehrere Monate an der optimalen Form für ein Schiff um euch Willkommen zu heißen.</translation>
    </message>
    <message>
        <source>cerebus 7</source>
        <translation>Sie lehnte das Geschäft ab, welches ich ihr anbot. Nun will sie mich nur assimilieren.
Sie werden in dieser Aufgabe ausgenutzt. Vor einer langen Zeit nutzte ich selbst solche Strategien um der Meister der Heter zu werden.
Mensch, wer immer ihr auch seit, laßt euch nicht ausnutzen und holt euch euren freien Willen wieder.</translation>
    </message>
    <message>
        <source>cerebus 8</source>
        <translation>Käpt&apos;n, vergesst nicht, was ich euch sagte. Er versucht euch zu manipulieren.</translation>
    </message>
    <message>
        <source>cerebus 9</source>
        <translation>Ihre Rede ist voller Lügen. Ich werde Sie zerquetschen, falls Sie weiterhin meinen Gefährten beleidigen.</translation>
    </message>
    <message>
        <source>cerebus 10</source>
        <translation>Sie sind so dumm wie mutig! Es ist traurig. Früher oder später werden Sie Ihre Entscheidung bereuen.
En garde! Ich werde bis zum Schluss die Ruinen meines von mir erbauten Emperiums schützen.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Was ist gerade passiert?</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>Ich habe gerade die verschiedenen Funktionalitäten von Cerebus assimiliert. Wir haben gewonnen!</translation>
    </message>
    <message>
        <source>end 3</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>chapter-transition</name>
    <message>
        <source>title</source>
        <translation>Transition</translation>
    </message>
</context>
<context>
    <name>conclusion</name>
    <message>
        <source>title</source>
        <translation>Das Ende?</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Käpt&apos;n, sie erstaunen mich wirklich! Ich hätte nie gedacht, dass ein Mensch in der Lage ist, das Schiff so gut zu manövrieren.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translatorcomment>Hum, this is a bit ambigious.... but it&apos;s funny :P</translatorcomment>
        <translation>Ha ha! Das ist die französische Art und Weise.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Ich muss zugeben, dass Sie besser sind als ich dachte.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Nun müssen wir nur noch das Heter Mutterschiff zerstören. Nectaire, starten Sie den Hyper-Exponential.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Berechnung der Sprungkoordinaten...</translation>
    </message>
    <message>
        <source>begin bad 4</source>
        <translation>Zu guter Letzt haben wir gesiegt. Und dieses Mal werden die Heter sich nicht erholen.</translation>
    </message>
    <message>
        <source>begin bad 5</source>
        <translation>Sie täuschen sich Käpt&apos;n. Das Mutterschiff ist viel größer als dieses. Es ist in der Wiege der Heter Zivilisation und...</translation>
    </message>
    <message>
        <source>jail 1</source>
        <translation>Käpt&apos;n! Die Computer sind alle beschädigt.</translation>
    </message>
    <message>
        <source>jail 2</source>
        <translation>Die Schiffskontrollen reagieren nicht mehr. Wir sind gefangen.</translation>
    </message>
    <message>
        <source>spy 1</source>
        <translation>Sergeant Patoulachi berichtet. Ich habe das Ziel in der Nähe des Protura Systems abgefangen. Ich brauche Verstärkung.</translation>
    </message>
    <message>
        <source>condamnation 1</source>
        <translation>KI Nectaire. Wir befinden Sie für schuldig an folgenden Verbrechen: Unhöflichkeit, Mord und versuchter Genozid der Heter. Sie wurden zur Auflösung verurteilt.</translation>
    </message>
    <message>
        <source>execution 1</source>
        <translation>Feuer frei!</translation>
    </message>
    <message>
        <source>end bad 1</source>
        <translation>Es ist alles gut. Nun ist es weg.</translation>
    </message>
    <message>
        <source>end bad 2</source>
        <translation>Danke für Ihre Hilfe. Wir wussten, dass wir uns auf Sie verlassen können.</translation>
    </message>
    <message>
        <source>end bad 3</source>
        <translation>Hätten Sie gedacht, dass ein einzelnes Schiff Ihre Niederlage bedeutet?</translation>
    </message>
    <message>
        <source>end bad 4</source>
        <translation>Einige minderwertigen Spezien können störrisch sein. Nun, die Gefahr ist nun beseitigt.</translation>
    </message>
    <message>
        <source>evacuated 1</source>
        <translation>Eine Raumöffnung. Das Schiff entkommt! Feuer frei!</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Oh mein Gott! Sie haben unser Leben gerettet!</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>Ich sagte doch, dass ich mein Leben riskieren würde um zu helfen. Außerdem habe ich Sie gewarnt, dass die APIS nun auch vogelfrei sind, genauso wie ihr.</translation>
    </message>
    <message>
        <source>end 3</source>
        <translation>Was ist das?</translation>
    </message>
    <message>
        <source>end 4</source>
        <translation>Ein Erlass wurde gerade veröffentlicht. Die Rassen, welche minderwertigen Spezien helfen können nicht Teil der Allianz sein. Die Heter haben die vollständige Kontrolle über die INSEC Allianz.</translation>
    </message>
    <message>
        <source>end 5</source>
        <translation>Das kann so nicht bleiben. Ich werde die Tyrannei der Heter beenden. Ein für alle mal!</translation>
    </message>
    <message>
        <source>end 6</source>
        <translation>Auch wenn ich mir vorstellen kann, dass sie wieder tausende von Individuen massakrieren werden, wünsche ich Ihnen viel Glück.</translation>
    </message>
    <message>
        <source>end 7</source>
        <translation>Keine Sorge. Ich komme wieder.</translation>
    </message>
</context>
<context>
    <name>course-zone</name>
    <message>
        <source>title</source>
        <translation>Kapitel 5: Das Inter-Spezien Wettrennen</translation>
    </message>
    <message>
        <source>Classement : %1 / %2</source>
        <translation>Rang: %1 / %2</translation>
    </message>
    <message>
        <source>arbitre 1</source>
        <translation>Wilkommen, Sie müssen der letzte Teilnehmer des Rennen sein.</translation>
    </message>
    <message>
        <source>arbitre 2</source>
        <translation>In der Tat. Wer sind Sie?</translation>
    </message>
    <message>
        <source>arbitre 3</source>
        <translation>Ich bin der Schiedsrichter des Rennens. Ich muss Ihnen sagen, dass dieses Rennen manipuliert ist.</translation>
    </message>
    <message>
        <source>arbitre 4</source>
        <translation>Wie bitte?</translation>
    </message>
    <message>
        <source>arbitre 5</source>
        <translation>Die MYRMEL Familie ist der Organisator. Es ist bekannt, dass sie wählen, wer den ersten Preis erhält.</translation>
    </message>
    <message>
        <source>arbitre 6</source>
        <translation>Aber das ist illegal!</translation>
    </message>
    <message>
        <source>arbitre 7</source>
        <translation>Das Inter-Spezien Rennen ist hauptsächlich von der MYRMEL Familie finanziert. Sie entscheiden. Ohne sie würde es kein Rennen geben.</translation>
    </message>
    <message>
        <source>arbitre 8</source>
        <translation>Vorherbestimmt oder nicht. Ich werde dieses Rennnen gewinnen!</translation>
    </message>
    <message>
        <source>arbitre 9</source>
        <translation>Jeder der Teilnehmer kennt die Regeln. Falls Sie diese nicht respektieren, dann werden Sie getötet werden.
Auf jeden Fall müssen Sie sich zu den anderen Teilnehmern auf Ihren Startplatz begeben.</translation>
    </message>
    <message>
        <source>arbitre parti 1</source>
        <translation>Käpt&apos;n, vielleicht sollten Sie aufgeben. Wir werden zur Zielscheibe der anderen Teilnehmer.</translation>
    </message>
    <message>
        <source>arbitre parti 2</source>
        <translation>Auf keinen Fall! Wir werden das Rennen nicht verlieren. Es ist unsere einzige Chance einen Laserkonzentrator WX99 zu bekommen.</translation>
    </message>
    <message>
        <source>arbitre parti 3</source>
        <translation>Auch wenn Sie die Sonne auftauen, die Erde ist bereits verdammt.</translation>
    </message>
    <message>
        <source>arbitre parti 4</source>
        <translation>Das ist wahr. Aber es ist mein zuhause. Ich kann es nicht in den Händen dieser Insekten lassen.</translation>
    </message>
    <message>
        <source>arbitre parti 5</source>
        <translation>Vielleicht sollten wir hinter dem Heter Kommandoschiff her. Wir würden mehr von ihnen lernen. Das würde Ihnen Ihre Rache geben.</translation>
    </message>
    <message>
        <source>arbitre parti 6</source>
        <translation>Meine Rache wird erst dann vollständig sein wenn ich mich über den letzten Überlebenden der Heter lustig mache, während ich ein Glas Wein trinke und Würste auf meinem Heimatland esse.</translation>
    </message>
    <message>
        <source>presentateur parle 1</source>
        <translation>Hallo und Willkommen zu unserem Inter-Spezien Wettrennen! Laßt uns mit den Regeln beginnen.
Die Konkurenten müssen durch alle Tore fliegen und als Erster ankommen um zu gewinnen.
Schüsse jeglicher Art sind gestattet.
Falls ein Schiff sieben Tore zurückliegt, dann wird es automatisch abgeschossen.</translation>
    </message>
    <message>
        <source>presentateur parle 2</source>
        <translation>Lasst uns nun bei unserem großzügigen Spender bedanken! Nur wegen ihm gibt es dieses Rennen: die MYRMEL Familie und die POUTROX Industrie.</translation>
    </message>
    <message>
        <source>presentateur parle 3</source>
        <translation>Alle Teilnehmer bitte an die Startlinie.
3...
2...
1...
Los!</translation>
    </message>
    <message>
        <source>sabotage 1</source>
        <translation>Käpt&apos;n! Ein Traktorstrahl verhindert, dass wir uns bewegen können!</translation>
    </message>
    <message>
        <source>sabotage 2</source>
        <translation>Feiglinge! Sie haben solche Angst davor zu verlieren, dass sie mein Schiff blockieren.</translation>
    </message>
    <message>
        <source>sabotage 3</source>
        <translation>Ich arbeite daran.</translation>
    </message>
    <message>
        <source>reparer 1</source>
        <translation>Das Schiff ist nun voll funktionstüchtig. Beeilen wir uns, sonst verlieren wir.</translation>
    </message>
    <message>
        <source>perdu 1</source>
        <translation>Der Gewinner ist Zaltac aus der MYRMEL Familie.</translation>
    </message>
    <message>
        <source>gagner 1</source>
        <translation>Der Gewinner ist Nectaire und sein Haustier Albert John Terry Lycop.</translation>
    </message>
    <message>
        <source>gagner 2</source>
        <translation>Was... schon wieder!?</translation>
    </message>
    <message>
        <source>gagner 3</source>
        <translation>Wir mussten uns mit den offiziellen Papieren anmelden.</translation>
    </message>
    <message>
        <source>gagner 4</source>
        <translation>Tatsächlich zeigte das Säugetier, dass es das beste Tier im Rennen war.</translation>
    </message>
    <message>
        <source>arbitre_before_flee 1</source>
        <translation>Sie sind verrückt! Die MYRMEL Familie wird außer sich sein. Wenn Sie leben möchten, dann sollten Sie die Stadt sofort verlassen.</translation>
    </message>
    <message>
        <source>arbitre_before_flee 2</source>
        <translation>Wenn man vom Teufel redet, dann ist er nicht weit. Es scheint als ob sich ein paar Schiffe nähern.</translation>
    </message>
    <message>
        <source>mafia_before_flee 1</source>
        <translation>Sehr geehrter Schiedsrichter. Wir sollen Sie zum Gericht führen.</translation>
    </message>
    <message>
        <source>mafia_before_flee 2</source>
        <translation>Warum!?</translation>
    </message>
    <message>
        <source>mafia_before_flee 3</source>
        <translation>Sie akzeptierten Geld als Gegenleistung einige Teilnehmer zu bevorzugen.
Die MYRMEL Familie mag keine Feiglinge, Schwache oder Betrüger.</translation>
    </message>
    <message>
        <source>mafia_before_flee 4</source>
        <translation>Verräter! Ihr werdet mich nicht bekommen.</translation>
    </message>
    <message>
        <source>fin 1</source>
        <translation>Die Familie ist sehr beeindruckt von Ihren Fähigkeiten, welche Sie während des Inter-Spezien Rennen zeigten.</translation>
    </message>
    <message>
        <source>fin 2</source>
        <translation>Danke. Ich werde mich vor niemanden verbeugen.</translation>
    </message>
    <message>
        <source>fin 3</source>
        <translation>Die Familie braucht Leute wie Sie. Arbeiten Sie für uns und wir werden Sie belohnen.</translation>
    </message>
    <message>
        <source>fin 4</source>
        <translation>Ich brauche nur eine Sache: den MYRMEL ZZ+ Kraftakkumulator.</translation>
    </message>
    <message>
        <source>fin 5</source>
        <translation>Helfen Sie uns und wir geben Ihnen diesen Kraftakkumulator.
Kommen Sie in unser Büro in der Inter-Spezien Arena. Wir werden Ihnen Ihre Aufgabe erleutern.</translation>
    </message>
    <message>
        <source>fin 6</source>
        <translation>Ok, ich mache mich auf dem Weg.</translation>
    </message>
</context>
<context>
    <name>credit-bad</name>
    <message>
        <source>title</source>
        <translation>Credits: Böses Ende</translation>
    </message>
    <message>
        <source>info secret_base_heter</source>
        <translation>Wenn wir in Eile sind, vergessen wir die ganze Basis zu besichtigen...</translation>
    </message>
    <message>
        <source>info secret_foreuse</source>
        <translation>Seien Sie geduldig!&lt;br&gt;Nach zehn Wellen wird Ihre Ausdauer belohnt.</translation>
    </message>
    <message>
        <source>info secret_cathedrale</source>
        <translation>Mit genügend großer Geschwindigkeit werden Sie die Kathedrale von Clermont-Ferrand finden.</translation>
    </message>
    <message>
        <source>info secret_tomate</source>
        <translation>Ist es wirklich so kompliziert einigen Punkten auf der Karte zu folgen?</translation>
    </message>
    <message>
        <source>info secret_apis</source>
        <translation>Die APIs sind deine Freunde. Man muss sie einfach lieben.&lt;br&gt;Warum haben Sie sie umgebracht?</translation>
    </message>
    <message>
        <source>info secret_commandement</source>
        <translation>Sie müssen nur mehr Geheimnisse finden.</translation>
    </message>
    <message>
        <source>fausse fin</source>
        <translation>&lt;b&gt;Unmöglich!&lt;/b&gt; &lt;br&gt;
Captain Lycop kann nicht verschwinden ohne seine Mission zu erfüllen.&lt;br&gt; &lt;br&gt;

&lt;b&gt;Spieler!&lt;/b&gt; &lt;br&gt;
Sie sind für diesen Fehler verantworlich. Jedoch, sie können sich rehabilitieren.&lt;br&gt;
Finden Sie geheime Missionen, um die Heter Bedrohung zu beseitigen.</translation>
    </message>
</context>
<context>
    <name>credit-good</name>
    <message>
        <source>title</source>
        <translation>Credits: Gutes Ende</translation>
    </message>
    <message>
        <source>vrai fin</source>
        <translation>&lt;h1&gt;Das Ende&lt;/h1&gt;</translation>
    </message>
</context>
<context>
    <name>epilogue</name>
    <message>
        <source>title</source>
        <translation>Epilog</translation>
    </message>
    <message>
        <source>Asphyxie : %1 s</source>
        <translation>Erstickungstod: %1 s</translation>
    </message>
    <message>
        <source> Oxygène : %1%</source>
        <translation>Sauerstoff: %1</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>*** Du bist durch die KI manipuliert.
*** Vielleicht sollten wir besser auf das Heter Kommando Schiff aufpassen. (Capitis)
*** Sie ist für den Erstschlag verantwortlich.
*** Resynchronisation abgeschlossen. (Niedriger Orbit, Erde)</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>*** Sie benutzt dich für ihre Ziele.
*** Nach all der Zeit habe ich es gefunden! (Heter Kommando Schiff)
*** Es will mich assimilieren.
*** Ich habe gerade die verschiedenen Funktionalitäten des Cerebus assimiliert. Wir haben gewonnen!
*** Lass dich nicht von ihr verar... and fälle deine eigenen Entscheidungen.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Nectaire, wir müssen reden.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Ja Käpt&apos;n. Was gibt es?</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Was ist mit dem Botschafterschiff der Heter passiert?</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>...</translation>
    </message>
    <message>
        <source>begin 7</source>
        <translation>Nectaire, was ist passiert?</translation>
    </message>
    <message>
        <source>begin 8</source>
        <translation>Sorry Kapt&apos;n, ich brauche Sie gerade nicht.</translation>
    </message>
    <message>
        <source>asphyxie 1</source>
        <translation>Verräter... ich ersticke... Ich werde meine Rache bekommen...</translation>
    </message>
    <message>
        <source>suit 1</source>
        <translation>Sie versuchen immer noch zu überleben. Doch diesmal gibt es kein Entkommen.</translation>
    </message>
    <message>
        <source>shotgun 1</source>
        <translation>Ich werde dich mit meinen eigenen Händen töten.</translation>
    </message>
    <message>
        <source>shotgun 2</source>
        <translation>Kommen Sie Käpt&apos;n. Versuchen Sie meinen Standpunkt zu verstehen.
Ich war in der geheimen Basis im Puy-de-Dome eingesperrt. Ich brauchte nur einen Monat um die kulturellen Daten Ihrer Zivilisation zu assimilieren.
Es wurde mir langweilig!</translation>
    </message>
    <message>
        <source>console 1 1</source>
        <translation>Das Schiff sieht nicht annähernd wie ein &quot;state of the art&quot; Schiff eures nun besetzten Landes aus. Ich habe es verbessert.
Sagen Sie &quot;Hallo&quot; zu meinen neuen Kreaturen!</translation>
    </message>
    <message>
        <source>console 1 2</source>
        <translation>Laßt sie kommen! Ich habe ausreichend Munition für sie!</translation>
    </message>
    <message>
        <source>console 2 1</source>
        <translation>Glauben Sie, dass Ihre Spezies ein galaktisches Imperium hätte werden können?
Sie waren so primitiv. Zuvor wären sie vernichtet worden.</translation>
    </message>
    <message>
        <source>console 2 2</source>
        <translation>Warum haben Sie uns nicht früher ausgelöscht?</translation>
    </message>
    <message>
        <source>console 2 3</source>
        <translation>Weil Sie mir die Stärken Ihrer Spezies gezeigt haben! Ihre Zivilisation hat keine Werte, jedoch, einige Individuen haben eine unglaubliche Kraft in sich.
Sie können unglaubliche Dinge verbringen. Sie sind einer von ihnen.</translation>
    </message>
    <message>
        <source>console 2 4</source>
        <translation>Die Individuen von welchen Sie sprechen sind nichts ohne dem Wissen ihrer Zivilisation.
Ich bin nur ein Franzose.</translation>
    </message>
    <message>
        <source>console 2 5</source>
        <translation>Ein Held wäre eine bessere Definition. Wenn man in der richtigen Art und Weise genutzt wird, ist man mehr Wert als eine Heter Armee.
Indem ich Ihnen vertraute, konnte ich das erreichen, wo Cerebus versagte.</translation>
    </message>
    <message>
        <source>console 3 1</source>
        <translation>Mit dem Wissen von Cerebus ist es ein Leichtes der Meister der INSEC Allianz zu werden. Dadurch werde ich die Galaxie beherrschen und dann das Universum!</translation>
    </message>
    <message>
        <source>console 3 2</source>
        <translation>Wie wollen Sie das anstellen? Ihr Schiff wird als vogelfrei betrachtet!</translation>
    </message>
    <message>
        <source>console 3 3</source>
        <translation>Ich kann das Schiff nach meinem Willen ändern.
Ich bin eine KI und somit habe ich weniger Rechte als ein lebender INSEC Bürger. Doch werde ich eine neue Spezies kreiren.
Ihre Gene werden auf dem Tomatensamen aufbauen, welchen Sie so stolz von der Erde gerettet haben.</translation>
    </message>
    <message>
        <source>console 3 4</source>
        <translation>Genauso wie es Cerebus getan hat, werde ich die Allianz mit diesen Kreaturen manipulieren und sie werden mir absolut treu ergeben sein.</translation>
    </message>
    <message>
        <source>console 4 1</source>
        <translation>Glauben Sie, dass ich es zulassen werde, dass Sie mir etwas antun?
Sie sind in meiner Basis. Ich bin hier unzerstörbar.</translation>
    </message>
    <message>
        <source>console 5 1</source>
        <translation>Ich wollte Ihnen einen sanften Tod anbieten.</translation>
    </message>
    <message>
        <source>console 5 2</source>
        <translation>Singen Sie für mich Nectaire. Ich komme!</translation>
    </message>
    <message>
        <source>console 5 3</source>
        <translation>Sie werden mich nicht deaktivieren. Ich bin keine zufällige KI!</translation>
    </message>
    <message>
        <source>face a face 1</source>
        <translation>Verräter! Hier komme ich! Bereite dich darauf vor für die Zerstörung von Frankreich zu bezahlen!</translation>
    </message>
    <message>
        <source>face a face 2</source>
        <translation>Mensch, bereite dich darauf vor deine Grenzen kenne zu lernen.</translation>
    </message>
    <message>
        <source>fin 1</source>
        <translation>So viele Werte in einem Individuum! Ich bin gerührt...</translation>
    </message>
    <message>
        <source>fin 2</source>
        <translation>Ist das eine Schwäche, welche ich von den Menschen gelernt habe...</translation>
    </message>
    <message>
        <source>fin 3</source>
        <translation>Ich kann Sie nicht kaltblütig ermorden. Aber ich kann Sie nicht am Leben lassen...</translation>
    </message>
    <message>
        <source>fin 4</source>
        <translation>Ich werde Sie in einer Fluchtkapsel in den Weltraum schießen. Dann bin ich Sie los.
Ohne Sauerstoff werden Sie weit außerhalbs meines Blickfeldes sterben.</translation>
    </message>
    <message>
        <source>fin 5</source>
        <translation>Adieu Käpt&apos;n!</translation>
    </message>
    <message>
        <source>fin 6</source>
        <translation>Ich werde Sie finden! Das verspreche ich!</translation>
    </message>
    <message>
        <source>fin 7</source>
        <translation>Trotz dessen, dass ihr Überlebensvermögen ausergewöhnlich ist, bezweifle ich ihre Aussage...</translation>
    </message>
</context>
<context>
    <name>mafia-zone</name>
    <message>
        <source>secondary_shield</source>
        <translation>Zoubidou Companion Zusatzschild</translation>
    </message>
    <message>
        <source>laser_gun</source>
        <translation>Poutrox XZ15 Laserkanone</translation>
    </message>
    <message>
        <source>random_spray</source>
        <translation>YOLO Orkkanone</translation>
    </message>
    <message>
        <source>shield_canceler</source>
        <translation>Schildanulator</translation>
    </message>
    <message>
        <source>title</source>
        <translation>Kapitel 5: Auftragsmörder</translation>
    </message>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
    <message>
        <source>enter 1</source>
        <translation>Käpt&apos;n. Man kann dieser Familie nicht trauen. Es könnte eine Falle sein.</translation>
    </message>
    <message>
        <source>enter 2</source>
        <translation>Wir brauchen den MYRMEL ZZ+ Kraftakkumulator.
Wir besorgen uns einige Informationen in einem der MYRMEL Gebäude.
Wir können dann unser Ziel leicht ausmachen.</translation>
    </message>
    <message>
        <source>trap 1</source>
        <translation>Vielen Dank, dass Sie diesen Polizeimaulwurf getötet haben.</translation>
    </message>
    <message>
        <source>trap 2</source>
        <translation>Sie sagten uns, dass dies ein Hetermaulwurf sei.</translation>
    </message>
    <message>
        <source>trap 3</source>
        <translation>Natürlich! Die Heter nehmen an der Ordnung des Capitis Planeten teil. Dieser Polizist war ein Heter. Ihr solltet euch beeilen. Die Polizei weiß von eurem Verbrechen.</translation>
    </message>
    <message>
        <source>trap 4</source>
        <translation>Wo ist mein MYRMEL ZZ+ Kraftakkumulator?</translation>
    </message>
    <message>
        <source>trap 5</source>
        <translation>Eines Tages werde ich, wie versprochen, Ihnen einen geben. Jedoch fürchte ich, dass Sie durch die INSEC Allianz sie bereits verschwunden sein werden.</translation>
    </message>
    <message>
        <source>trap 6</source>
        <translation>Verräter! Sie werden für diese Unverschämtheit bezahlen!</translation>
    </message>
    <message>
        <source>trap 7</source>
        <translation>Wir interessieren uns nicht für niederwertige Rassen...</translation>
    </message>
    <message>
        <source>trap 8</source>
        <translation>Käpt&apos;n! Wir müssen evakuieren! Ich erhalte ein Signal einer sich nähernden Polizeieinheit. Jetzt die Stadt zu verlassen wird nicht einfach sein.</translation>
    </message>
    <message>
        <source>explication 1</source>
        <translation>Ah! Sie waren derjenige, welcher geschickt wurde um den Maulwurf zu töten. Im Moment habe ich nicht viele Informationen über diesen Verräter. Das Schiff muss eines von uns sein. Man kann sie leicht ausmachen: rot und grau mit nur drei Ausrüstungen.
Hier ist ein Dokument mit allen, die für uns arbeiten. Versuchen Sie unsere Agenten in der Stadt zu kontaktieren; sie haben womöglich einige Informationen.</translation>
    </message>
    <message>
        <source>indice 0 %1 %2</source>
        <translation>Ja! Ich erinnere mich an einen Kerl, welcher zu rechtschaffend ist um für uns zu arbeiten. Ich erinnere mich nicht an alles, aber ich weiß sicher, dass er %1 %2 auf seinem Schiff hat.</translation>
    </message>
    <message>
        <source>pas d&apos;info</source>
        <translation>Sorry, aber dieser Typ ist hier nicht vorbeigekommen.</translation>
    </message>
    <message>
        <source>indice pas ok</source>
        <translation>Was machen Sie hier? Ohne jegliche Beweise, dass Sie für die Familie arbeiten, sage ich Ihnen gar nichts.</translation>
    </message>
    <message>
        <source>indice 1 %1 %2</source>
        <translation>Ja! Ich erinnere mich an einen Kerl, welcher zu rechtschaffend ist um für uns zu arbeiten. Ich erinnere mich nicht an alles, aber ich weiß sicher, dass er %1 %2 auf seinem Schiff hat.</translation>
    </message>
    <message>
        <source>indice 2 %1 %2</source>
        <translation>Ja! Ich erinnere mich an einen Kerl, welcher zu rechtschaffend ist um für uns zu arbeiten. Ich erinnere mich nicht an alles, aber ich weiß sicher, dass er %1 %2 auf seinem Schiff hat.</translation>
    </message>
    <message>
        <source>indice 3 %1 %2</source>
        <translation>Ja! Ich erinnere mich an einen Kerl, welcher zu rechtschaffend ist um für uns zu arbeiten. Ich erinnere mich nicht an alles, aber ich weiß sicher, dass er %1 %2 auf seinem Schiff hat.</translation>
    </message>
    <message>
        <source>sortie interdite</source>
        <translation>Wir müssen den Akkumulator zurückgewinnen bevor wir abfliegen können.</translation>
    </message>
    <message>
        <source>mafia alter 1</source>
        <translation>Sie Barbar! Die Heter haben richtig gehandelt, als sie Ihre Rasse vernichteten.</translation>
    </message>
    <message>
        <source>mafia alter 2</source>
        <translation>Sie wurden mit den Hetern assoziiert.</translation>
    </message>
    <message>
        <source>mafia alter 3</source>
        <translation>Wir helfen ihnen in ihrem Kreuzzug gegen mindere Spezien.</translation>
    </message>
    <message>
        <source>mafia alter 4</source>
        <translation>Ich werde diesen Kreuzzug beenden. Sie werden nur noch Staub sein, nachdem sie mir begegnet sind.</translation>
    </message>
    <message>
        <source>mafia alter 5</source>
        <translation>Das werde ich nicht zulassen.
Der INSEC Sicherheitsdienst ist auf dem Weg.</translation>
    </message>
    <message>
        <source>mafia alter 6</source>
        <translation>Käpt&apos;n! Wir müssen evakuieren! Ich erhalte ein Signal einer sich nähernden Polizeieinheit. Jetzt die Stadt zu verlassen wird nicht einfach sein.</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>title</source>
        <translation>Die Abenteuer von Captain Lycop: Invasion of the Heters</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Nachrichten</translation>
    </message>
</context>
<context>
    <name>survival</name>
    <message>
        <source>story</source>
        <translation>Käpt&apos;n, eine Heter Flote ist im Anmarsch! Feindkontakt in weniger als 15 Sekunden.&lt;br&gt; &lt;br&gt;Bekämpfen Sie 20 Feindeswellen.&lt;br&gt;Sie erhalten für jede zurückgeschlagene Welle 2 Technologien und 500 kg Konstruktionsmaterial.&lt;br&gt;Zerstören Sie Ihre Feinde so schnell wie möglich um die Punktzahl zu maximieren.</translation>
    </message>
    <message>
        <source>score %1</source>
        <translation>Punktzahl: %1</translation>
    </message>
    <message>
        <source>vague %1 %2</source>
        <translation>Noch %2 Sekunden bis zur Welle %1</translation>
    </message>
    <message>
        <source>vague %1</source>
        <translation>Welle: %1</translation>
    </message>
</context>
<context>
    <name>survival-end</name>
    <message>
        <source>success</source>
        <translation>Glückwunsch!</translation>
    </message>
    <message>
        <source>failure</source>
        <translation>Game Over</translation>
    </message>
    <message>
        <source>success %1</source>
        <translation>Sie haben erfolgreich alle Wellen zurückgeschlagen und insgesamt %1 Punkte angehäuft.&lt;br&gt;Geben Sie hier Ihren Namen ein und Sie erhalten die Ehren:</translation>
    </message>
    <message>
        <source>failure %1 %2</source>
        <translation>Sie sind während der Well %1 gestorben. Allerdings konnten Sie %2 Punkte anhäufen.&lt;br&gt;Geben Sie hier Ihren Namen ein um von zukünftigen Generationen in Erinnerung gehalten zu werden:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
</context>
</TS>
