<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AccidentDialog</name>
    <message>
        <source>text 1 %1</source>
        <translation>Imbécile ! Vous avez ruiné mon vaisseau, j’en ai pour au moins %1 kg de métal en réparations. Dédommagez-moi immédiatement ou j’appelle la police !</translation>
    </message>
    <message>
        <source>text 2 %1</source>
        <translation>Allons, allons, regardez un peu devant vous ! Mon beau vaisseau est bon pour la casse ! Oui, bon, je sais bien que c’était un accident. Je passe l’éponge si vous me remboursez %1 kg de matériaux. Qu’en dîtes-vous ?</translation>
    </message>
    <message>
        <source>text 3 %1</source>
        <translation>Halte ! Au nom de la commission des transports de Capitis, ce comportement n’est pas toléré dans cette ville. À présent, vous avez le choix entre payer %1 kg de matériaux en réparations du préjudice causé ou subir les conséquences de vos incivilités.</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Ignorer</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Payer</translation>
    </message>
</context>
<context>
    <name>Controls</name>
    <message>
        <source>direction</source>
        <translation>Orientation du vaisseau</translation>
    </message>
    <message>
        <source>velocity</source>
        <translation>Déplacement du vaisseau</translation>
    </message>
    <message>
        <source>weapons</source>
        <translation>Armes principales</translation>
    </message>
    <message>
        <source>secondary-weapons</source>
        <translation>Armes secondaires</translation>
    </message>
    <message>
        <source>speed-boost</source>
        <translation>Favoriser la vitesse de déplacement</translation>
    </message>
    <message>
        <source>weapons-boost</source>
        <translation>Favoriser la cadence de tir</translation>
    </message>
    <message>
        <source>shield-boost</source>
        <translation>Favoriser la régénération du bouclier</translation>
    </message>
    <message>
        <source>repair</source>
        <translation>Réparer les équipements endommagés</translation>
    </message>
    <message>
        <source>slot-%1</source>
        <translation>Équipement %1</translation>
    </message>
    <message>
        <source>button-%1</source>
        <translation>Bouton %1 de la barre d’actions</translation>
    </message>
    <message>
        <source>Mouse button %1</source>
        <translation>Bouton %1 de la souris</translation>
    </message>
    <message>
        <source>Mouse wheel %1</source>
        <translation>Molette %1 de la souris</translation>
    </message>
    <message>
        <source>Keyboard %1</source>
        <translation>Touche [%1] du clavier</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation>Mouvement de la souris</translation>
    </message>
    <message>
        <source>Joystick button %1:%2</source>
        <translation>Bouton %2 de la manette %1</translation>
    </message>
    <message>
        <source>Joystick axis %1:%2</source>
        <translation>Stick analogique %2 de la manette %1</translation>
    </message>
    <message>
        <source>Joystick hat %1:%2</source>
        <translation>Croix directionnelle %2 de la manette %1</translation>
    </message>
    <message>
        <source>Joystick ball %1:%2</source>
        <translation>Trackball %2 de la manette %1</translation>
    </message>
    <message>
        <source>Key_Up</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_W</source>
        <translation>Key_Z</translation>
    </message>
    <message>
        <source>Key_Down</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_S</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_Left</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_A</source>
        <translation>Key_Q</translation>
    </message>
    <message>
        <source>Key_Right</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_D</source>
        <translation></translation>
    </message>
    <message>
        <source>Key_1</source>
        <translation>Key_Ampersand</translation>
    </message>
    <message>
        <source>Key_2</source>
        <translation>Key_Eacute</translation>
    </message>
    <message>
        <source>Key_3</source>
        <translation>Key_QuoteDbl</translation>
    </message>
    <message>
        <source>Key_4</source>
        <translation>Key_Apostrophe</translation>
    </message>
    <message>
        <source>Key_5</source>
        <translation>Key_ParenLeft</translation>
    </message>
    <message>
        <source>Key_6</source>
        <translation>Key_Minus</translation>
    </message>
    <message>
        <source>Key_7</source>
        <translation>Key_Egrave</translation>
    </message>
    <message>
        <source>Key_8</source>
        <translation>Key_Underscore</translation>
    </message>
    <message>
        <source>Key_9</source>
        <translation>Key_Ccedilla</translation>
    </message>
    <message>
        <source>Key_0</source>
        <translation>Key_Agrave</translation>
    </message>
</context>
<context>
    <name>MessageLog</name>
    <message>
        <source>No new messages.</source>
        <translation>Pas de nouveau message.</translation>
    </message>
</context>
<context>
    <name>Screen_ConfirmControl</name>
    <message>
        <source>prompt %1 %2</source>
        <translation>%1 est déjà associé à l’action « %2 ».&lt;br&gt;À quelle action voulez-vous associer %1 ?</translation>
    </message>
</context>
<context>
    <name>Screen_Controls</name>
    <message>
        <source>Axes</source>
        <translation>Sticks analogiques (manette)</translation>
    </message>
    <message>
        <source>Buttons</source>
        <translation>Boutons (clavier, souris, manette)</translation>
    </message>
    <message>
        <source>Mouses / hats</source>
        <translation>Mouvement de la souris ou croix directionnelle de la manette</translation>
    </message>
    <message>
        <source>Slot</source>
        <translation></translation>
    </message>
    <message>
        <source>Binding</source>
        <translation></translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Rétablir</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
</context>
<context>
    <name>Screen_EditControl</name>
    <message>
        <source>%1: %2</source>
        <translation>%1 : %2</translation>
    </message>
    <message>
        <source>help 0</source>
        <translation>&lt;h1&gt;Appuyez sur un bouton du clavier, de la souris ou de la manette.&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>help 1</source>
        <translation>&lt;h1&gt;Utilisez le stick analogique de la manette.&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>help 2</source>
        <translation>&lt;h1&gt;Cliquez avec la souris ou utilisez la croix directionnelle de la manette.&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
</context>
<context>
    <name>Screen_EditGame</name>
    <message>
        <source>Annotate game “%1”:</source>
        <translation>Annoter la partie « %1 » :</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Valider</translation>
    </message>
</context>
<context>
    <name>Screen_EditScore</name>
    <message>
        <source>prompt %1 %2</source>
        <translation>Annoter le score « %1 » enregistré le %2 :</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Valider</translation>
    </message>
</context>
<context>
    <name>Screen_Equipments</name>
    <message>
        <source>%1 HP</source>
        <translation>%1 PV</translation>
    </message>
    <message>
        <source>no-equipment</source>
        <translation>&lt;h1&gt;Rien&lt;/h1&gt;&lt;p&gt;Sélectionnez un équipement dans la liste ci-dessus.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>no-slot</source>
        <translation>&lt;h1&gt;Rien&lt;/h1&gt;&lt;p&gt;Installez un équipement ou sélectionnez un autre emplacement sur le vaisseau.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Equip (%1 kg)</source>
        <translation>Équiper (%1 kg)</translation>
    </message>
    <message>
        <source>Equip</source>
        <translation>Équiper</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Détacher</translation>
    </message>
    <message>
        <source>Recycle (%1 kg)</source>
        <translation>Recycler (%1 kg)</translation>
    </message>
    <message>
        <source>Rotate left</source>
        <translation>Pivoter à gauche</translation>
    </message>
    <message>
        <source>Rotate right</source>
        <translation>Pivoter à droite</translation>
    </message>
    <message>
        <source>Repair (%1 kg)</source>
        <translation>Réparer (%1 kg)</translation>
    </message>
    <message>
        <source>Repair all (%1 kg)</source>
        <translation>Tout réparer (%1 kg)</translation>
    </message>
</context>
<context>
    <name>Screen_GameOver</name>
    <message>
        <source>Game over</source>
        <translation></translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>Recommencer</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Charger</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Abandonner</translation>
    </message>
    <message>
        <source>Pensez à utiliser le celeritas accretio pour vous déplacer plus vite</source>
        <translation>Pensez à utiliser le celeritas accretio pour vous déplacer plus vite !</translation>
    </message>
    <message>
        <source>Le bouclier à percussion désactive le bouclier Zoubidou Classix mais pas le bouclier auxiliaire Zoubidou Companion</source>
        <translation>Le bouclier à percussion désactive le bouclier Zoubidou Classix mais pas le bouclier auxiliaire Zoubidou Companion.</translation>
    </message>
    <message>
        <source>L&apos;accélerateur linéaire vous permet passivement de vous déplacer plus vite</source>
        <translation>L&apos;accélerateur linéaire vous permet passivement de vous déplacer plus vite.</translation>
    </message>
    <message>
        <source>Le générateur de replis de l’espace permet de vous téléporter. Si vous vous téléportez sur un petit ennemi, vous échangerez de position</source>
        <translation>Le générateur de replis de l’espace permet de vous téléporter. Si vous vous téléportez sur un petit vaisseau ennemi, vous échangerez de position.</translation>
    </message>
    <message>
        <source>Vous manquez de DPS ? Utilisez le Modus accretio</source>
        <translation>Vous manquez de DPS ? Utilisez le Modus accretio.</translation>
    </message>
    <message>
        <source>Conseil : %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screen_LoadGame</name>
    <message>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Annoter</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Charger</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
</context>
<context>
    <name>Screen_Menu</name>
    <message>
        <source>Equipments</source>
        <translation>Vaisseau</translation>
    </message>
    <message>
        <source>Travel</source>
        <translation>Hyperespace</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <source>Mercury</source>
        <translation>Mercure</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation>Soleil</translation>
    </message>
    <message>
        <source>Earth</source>
        <translation>Terre</translation>
    </message>
    <message>
        <source>Vaisseau-monde Heter</source>
        <translation>Vaisseau de commandement</translation>
    </message>
    <message>
        <source>Auto load</source>
        <translation>Dernière sauvegarde</translation>
    </message>
    <message>
        <source>New game</source>
        <translation>Nouvelle partie</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Charger</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Crédits</translation>
    </message>
    <message>
        <source>Title screen</source>
        <translation>Menu principal</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <source>demo version %1</source>
        <translation>démo version %1</translation>
    </message>
    <message>
        <source>Easy</source>
        <translation>Mode histoire</translation>
    </message>
    <message>
        <source>Survival mode</source>
        <translation>Mode survie</translation>
    </message>
    <message>
        <source>Technologies</source>
        <translation></translation>
    </message>
    <message>
        <source>Menu</source>
        <translation></translation>
    </message>
    <message>
        <source>Planète Capitis</source>
        <translation></translation>
    </message>
    <message>
        <source>Scores</source>
        <translation></translation>
    </message>
    <message>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <source>version %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Faible</source>
        <translation>Pleutre</translation>
    </message>
    <message>
        <source>Vaillant</source>
        <translation>Vaillant</translation>
    </message>
    <message>
        <source>Training mode</source>
        <translation>Mode Apprentissage</translation>
    </message>
</context>
<context>
    <name>Screen_Options</name>
    <message>
        <source>Display preferences</source>
        <translation>Paramètres d’affichage</translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation>Plein écran</translation>
    </message>
    <message>
        <source>Diffuse</source>
        <translation>Diffus</translation>
    </message>
    <message>
        <source>Diffuse and specular</source>
        <translation>Diffus et spéculaire</translation>
    </message>
    <message>
        <source>Flat</source>
        <translation>Plat</translation>
    </message>
    <message>
        <source>Lighting quality</source>
        <translation>Qualité de l’éclairage</translation>
    </message>
    <message>
        <source>Sound preferences</source>
        <translation>Paramètres du son</translation>
    </message>
    <message>
        <source>Volume: sound effects</source>
        <translation>Volume des effets sonores</translation>
    </message>
    <message>
        <source>Volume: music</source>
        <translation>Volume de la musique</translation>
    </message>
    <message>
        <source>Volume: cutscenes</source>
        <translation>Volume des cinématiques</translation>
    </message>
    <message>
        <source>Input preferences</source>
        <translation>Paramètres des contrôles</translation>
    </message>
    <message>
        <source>Absolute velocity</source>
        <translation>Déplacement du vaisseau relatif à l’écran</translation>
    </message>
    <message>
        <source>Bindings</source>
        <translation>Périphériques d’entrée</translation>
    </message>
    <message>
        <source>Configure...</source>
        <translation>Configurer...</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
</context>
<context>
    <name>Screen_Quit</name>
    <message>
        <source>title</source>
        <translation>Pardon ?</translation>
    </message>
    <message>
        <source>prompt</source>
        <translation>Le lâche connaît la honte.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
</context>
<context>
    <name>Screen_RemoveGame1</name>
    <message>
        <source>Do you want to remove all data about game “%1”?</source>
        <translation>Voulez-vous supprimer tout ce qui concerne la partie « %1 » ?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>Screen_RemoveGame2</name>
    <message>
        <source>Do you want to remove data saved on %2 about game “%1”?</source>
        <translation>Voulez-vous supprimer la sauvegarde du %2 de la partie « %1 » ?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>Screen_RemoveScore</name>
    <message>
        <source>prompt %1 %2</source>
        <translation>Voulez-vous supprimer le score « %1 » enregistré le %2 ?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>Screen_ResetControls</name>
    <message>
        <source>prompt</source>
        <translation>Voulez-vous rétablir les paramètres par défaut ?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Rétablir</translation>
    </message>
</context>
<context>
    <name>Screen_Scores</name>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Wave</source>
        <translation>Vague</translation>
    </message>
    <message>
        <source>end</source>
        <translation>terminé</translation>
    </message>
    <message>
        <source>wave %1</source>
        <translation>vague %1</translation>
    </message>
    <message>
        <source>Score</source>
        <translation></translation>
    </message>
    <message>
        <source>score %1</source>
        <translation>%1 points</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Annoter</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
</context>
<context>
    <name>Screen_Technologies</name>
    <message>
        <source>Disabled</source>
        <translation>Indisponible</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Disponible</translation>
    </message>
    <message>
        <source>Level %1</source>
        <translation>Niveau %1</translation>
    </message>
    <message>
        <source>Science: %1 points</source>
        <translation>Science : %1 points</translation>
    </message>
    <message>
        <source>Research</source>
        <translation>Rechercher</translation>
    </message>
</context>
<context>
    <name>Screen_Travel</name>
    <message>
        <source>Travel</source>
        <translation>Voyager</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <source>%1 HP</source>
        <translation>%1 PV</translation>
    </message>
</context>
<context>
    <name>Tab_credit</name>
    <message>
        <source>&lt;h1&gt;Aeronie&apos;s Dream Team&lt;/h1&gt;&lt;br&gt;</source>
        <translation>&lt;h1&gt;L’équipe d’Aeronie&lt;/h1&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Translations&lt;/h1&gt;&lt;br&gt;</source>
        <translation>&lt;h1&gt;Traductions et relectures&lt;/h1&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Background music&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Musiques d’ambiance&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Title screen&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Écran Titre&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 1&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Chapitre 1&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 2&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Chapitre 2&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 3&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Chapitre 3&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 4&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Chapitre 4&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 5&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Chapitre 5&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 6&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Chapitre 6&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Fin ?&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Fin ?&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Chapter 7&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Chapitre 7&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Epilogue&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Épilogue&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Boss themes&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Thème des Boss&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Sound Effects&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Effets Sonores&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Libraries&lt;/h1&gt;&lt;br&gt;</source>
        <translation>&lt;h1&gt;Bibliothèques&lt;/h1&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>Remerciement</source>
        <translation>Nous remercions nos familles et nos amis qui nous ont toujours soutenus.</translation>
    </message>
    <message>
        <source>&lt;h1&gt; Capitaine Lycop : L&apos;invasion des Heters &lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Capitaine Lycop : L’invasion des Héters&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Outils Graphiques&lt;/h1&gt;&lt;br&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Technologies</name>
    <message>
        <source>hull</source>
        <translation>Blindage amélioré</translation>
    </message>
    <message>
        <source>hull 0</source>
        <translation>&lt;p&gt;Les progrès constants dans l’étude des alliages et des matériaux composites permettent la production de nouveaux types de blindages aux performances accrues.&lt;/p&gt;
&lt;p&gt;Ces blindages, plus résistants, vous protégeront, vous et votre vaisseau, des agressions mécaniques, magnétiques ou chimiques qui font le charme du voyage interstellaire.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>nanomachines</source>
        <translation>Nanomachines LoTec Formic 17</translation>
    </message>
    <message>
        <source>nanomachines 0</source>
        <translation>&lt;p&gt;À la fin d’une rude bataille, la plupart des pilotes se DÉBARRASSENT des pièces endommagées et les REMPLACENT par des pièces neuves.&lt;/p&gt;
&lt;p&gt;Pourtant, chez LoTec nous vous proposons ces adorables petits robots qui pénètrent AU CŒUR DE LA MACHINE pour RÉPARER les pièces endommagées, sans attendre la fin des combats !&lt;/p&gt;
&lt;p&gt;Rejoignez les rangs de ceux qui ont choisi de faire des économies grâce à la nanorobotique !&lt;/p&gt;</translation>
    </message>
    <message>
        <source>drone-launcher</source>
        <translation>Contrôleur de drones LoTec Vesp 22</translation>
    </message>
    <message>
        <source>drone-launcher 0</source>
        <translation>&lt;p&gt;Chez LoTec, nous aimons rappeler que si le brave combat vaillamment, celui qui ne combat pas voit l’issue de la bataille.&lt;/p&gt;
&lt;p&gt;Cette mini-usine embarquée s’inscrit dans cette philosophie. Elle produit et contrôle simultanément jusqu’à 5 drones qui combattront (et seront détruits) À VOTRE PLACE.&lt;/p&gt;
&lt;p&gt;Parce que nos robots sont produits à la chaîne, mais nos clients sont uniques.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>drone-launcher 1</source>
        <translation>&lt;li&gt;Les drones sont équipés de canons Gatling M438.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>drone-launcher 2</source>
        <translation>&lt;li&gt;Les drones sont équipés de lance-missiles Dagobert 3.3.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>gyroscope</source>
        <translation>Accélerateur angulaire Mochi par Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>gyroscope 0</source>
        <translation>&lt;p&gt;Dans un monde qui change, un homme d’aujourd’hui doit pouvoir se retourner à tout moment. C’est pourquoi nous avons créé ce module qui augmentera l’efficacité des moteurs de votre vaisseau et lui permettra de tourner plus rapidement sur lui même.&lt;/p&gt;
&lt;p&gt;Attention, certains clients rapportent qu’une utilisation prolongée pourrait entraîner des vertiges et des nausées.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>gyroscope 2</source>
        <translation>&lt;li&gt;Le vaisseau tourne encore plus vite.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>dash</source>
        <translation>Accélerateur linéaire Dango par Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>dash 0</source>
        <translation>&lt;p&gt;Vous vous sentez en retard sur votre temps ? Vous voulez changer de rythme ? C’est pour vous que nous avons créé ce module qui augmentera l’efficacité des moteurs de votre vaisseau et lui permettra de se déplacer plus rapidement.&lt;/p&gt;
&lt;p&gt;Pour votre sécurité et celle des autres usagers, respectez les limitations de vitesse dans les espaces urbanisés et les champs d’astéroïdes.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>dash 2</source>
        <translation>&lt;li&gt;Le vaisseau avance encore plus vite.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>rocket-launcher</source>
        <translation>Lance roquettes Dagobert 2.3</translation>
    </message>
    <message>
        <source>rocket-launcher 0</source>
        <translation>&lt;p&gt;Cette arme lance des projectiles autopropulsés chargés de 20 kg d’explosifs Dagobert, bien connus pour leur potentiel destructif.&lt;/p&gt;
&lt;p&gt;Attention. Le dispositif de détonation ultra-sensible peut provoquer une explosion prématurée de la roquette au contact d’un autre projectile.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>missile-launcher</source>
        <translation>Lance missiles Dagobert 3.3</translation>
    </message>
    <message>
        <source>missile-launcher 0</source>
        <translation>&lt;p&gt;Cette arme lance des projectiles autopropulsés et autoguidés chargés de 20 kg d’explosifs Dagobert, qui provoqueront la crainte et l’admiration de vos ennemis.&lt;/p&gt;
&lt;p&gt;Le dispositif de détonation des missiles a été retravaillé pour être moins sensible aux projectiles ennemis. Nos ingénieurs s’attachent désormais au problème des lasers et nous ont promis un prototype avant la prochaine révolution galactique.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>gun</source>
        <translation>Canon Gatling M438</translation>
    </message>
    <message>
        <source>gun 0</source>
        <translation>&lt;p&gt;Ce canon simple et fiable constitue la base de la défense de tous les vaisseaux grâce à une méthode qui a fait ses preuves : lancer des projectiles sur les menaces potentielles.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>triple-gun</source>
        <translation>Canon triple Gatling M438-S3</translation>
    </message>
    <message>
        <source>triple-gun 0</source>
        <translation>&lt;p&gt;Une étude publiée récemment montre que les projectiles tirés par les canons sont petits et relativement espacés. Comment alors être sûr de toucher sa cible ? D’après les savants, en augmentant la taille des projectiles ou en réduisant la distance entre eux.&lt;/p&gt;
&lt;p&gt;Ce canon triple explore la première approche en tirant plusieurs projectiles parallèles pour augmenter artificiellement leur taille et rendre l’esquive plus difficile.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>coil-gun</source>
        <translation>Canon de Gauss Arès</translation>
    </message>
    <message>
        <source>coil-gun 0</source>
        <translation>&lt;p&gt;Nous POURRIONS augmenter la puissance de nos canons simplement en augmentant la masse de combustible dédié à l’accélération des projectiles, si les lobbies écologistes nous laissaient l’extraire. Au lieu de ça, nous sommes réduits à utiliser l’induction magnétique pour accélérer des projectiles métalliques dans ce canon embobiné dans un solénoïde. Bien sûr, ce canon a quand même une puissance formidable. Mais devons-nous vraiment accepter de sacrifier nos traditions sur l’autel de l’écolo-démagogie ?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>electric-gun</source>
        <translation>Canon électrique Zeus</translation>
    </message>
    <message>
        <source>electric-gun 0</source>
        <translation>&lt;p&gt;De nos jours, les boucliers semblent très à la mode chez les plus jeunes pilotes, et croyez bien que nous le déplorons. Alors toute notre rage, tout notre désespoir, nous les avons mis dans ce canon qui tire des boules de plasma parcourues de courants électriques très intenses, terriblement efficaces contre les boucliers énergétiques. Bon, il n’inflige aucun dégât à la coque. Mais sur le bouclier, vraiment, quel effet !&lt;/p&gt;</translation>
    </message>
    <message>
        <source>mine-launcher</source>
        <translation>Lance mines Dagobert 2.5</translation>
    </message>
    <message>
        <source>mine-launcher 0</source>
        <translation>&lt;p&gt;Cette arme dépose un engin explosif déclenché par plusieurs capteurs de proximité. La charge de 10 kg d’explosifs Dagobert embarquée dans chaque mine surprendra le premier vaisseau qui aura l’audace de s’en approcher.&lt;/p&gt;
&lt;p&gt;Attention. Pour assurer le bon fonctionnement des mines même face à des « tactiques » telles que le camouflage ou le cyber-sabotage, le dispositif de détonation ultra-sensible est conçu pour détecter aussi bien vos alliés que vos ennemis.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>acid-gun</source>
        <translation>Canon acide Dionysos</translation>
    </message>
    <message>
        <source>acid-gun 0</source>
        <translation>&lt;p&gt;La course à la puissance est terminée avec ce canon capable de détruire instantanément n’importe quel équipement. Ses projectiles acides s’attaquent à la base de l’équipement, la corrodent et la détachent du vaisseau. Seuls les boucliers énergétiques sont (encore) capables d’y résister mais, après tout, qui s’en soucie ? Les équipements sont détruits ! Instantanément !&lt;/p&gt;</translation>
    </message>
    <message>
        <source>random-spray</source>
        <translation>Canon orque YOLO</translation>
    </message>
    <message>
        <source>random-spray 0</source>
        <translation>&lt;p&gt;Ce canon est la première tentative des Héters d’assimilation des technologies humaines. Il tire aléatoirement et avec une précision discutable des projectiles normaux, électriques, acides ou accélérés électromagnétiquement.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>plasma-spray</source>
        <translation>Diffuseur de plasma Héphaïstos</translation>
    </message>
    <message>
        <source>plasma-spay 1</source>
        <translation>&lt;li&gt;Puissance de feu : +25 dégâts / s par seconde d’exposition au plasma&lt;/li&gt;</translation>
    </message>
    <message>
        <source>plasma-spray 0</source>
        <translation>&lt;p&gt;Inspirés par la délicieuse cruauté des lance-flammes humains, les Héters ont créé ce canon dispersant à courte portée du plasma à haute température.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>vortex-gun</source>
        <translation>Canon gravitationnel omicron-alpha</translation>
    </message>
    <message>
        <source>vortex-gun 0</source>
        <translation>&lt;p&gt;Ce canon peut envoyer une grenade qui générera un vortex spatial. N’importe quel individu sage éviterait de se trouver sur sa trajectoire.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>qui-poutre</source>
        <translation>Canon Quipoutre</translation>
    </message>
    <message>
        <source>qui-poutre 0</source>
        <translation>&lt;p&gt;Ce canon est le modèle réduit du destructeur de monde Héter. Instable mais extrêmement puissante, cette arme est bien plus dévastatrice que n’importe quelle autre.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>qui-poutre 1</source>
        <translation>&lt;li&gt;Puissance de feu : 3000 dégâts, et 300 dégâts / s à proximité du projectile&lt;/li&gt;</translation>
    </message>
    <message>
        <source>laser-gun</source>
        <translation>Canon laser Poutrox XZ15</translation>
    </message>
    <message>
        <source>laser-gun 0</source>
        <translation>&lt;p&gt;Les Héters, équipés depuis toujours de solides carapaces, se sont orientés très tôt vers des armes capables de faire fondre leurs ennemis. Ce canon laser, équivalent aux canons terriens en terme de potentiel de destruction, résulte de cette évolution particulière.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>heavy-laser-gun</source>
        <translation>Canon laser lourd Poutrox XZ25</translation>
    </message>
    <message>
        <source>heavy-laser-gun 0</source>
        <translation>&lt;p&gt;Finie, la frustration d’avoir à peine égratigné un vaisseau ennemi avec un laser de faible puissance. Avec cette arme lourde, vous le toucherez certes moins souvent ; mais chaque fois que vous le toucherez, ce sera avec la douce certitude de lui avoir infligé la douleur qu’il mérite.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>maser-emitter</source>
        <translation>Concentrateur maser Poutrox WR65</translation>
    </message>
    <message>
        <source>maser-emitter 0</source>
        <translation>&lt;p&gt;Oui, certaines proies essaient, pour éviter vos tirs, de se dissimuler derrière des obstacles. Mais où qu’elles se cachent, soyez sûr qu’elles ne pourront pas éviter ce faisceau d’ondes électromagnétiques qui traverse tous les objets sur sa route, tous, en détruisant progressivement leur structure moléculaire.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>laser-bouncer</source>
        <translation>Canon laser à fragmentation Poutrox XZ45</translation>
    </message>
    <message>
        <source>laser-bouncer 0</source>
        <translation>&lt;p&gt;Ne vous fatiguez plus à viser les myriades de petits vaisseaux ! Ce canon émet un laser concentré à haute énergie qui explose et se sépare en une kyrielle de lasers plus petits. Aussi efficace qu’élégante, cette arme est souvent utilisée sur Capitis lors des cérémonies officielles.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>shield-generator</source>
        <translation>Bouclier Zoubidou Classix 1962</translation>
    </message>
    <message>
        <source>shield-generator 0</source>
        <translation>&lt;p&gt;Ce modèle, salué par la critique, s’est imposé au fil des ans comme une référence. Il garde toute sa pertinence encore aujourd’hui.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>shield-canceler</source>
        <translation>Compresseur de flux magnétique sigma-tau</translation>
    </message>
    <message>
        <source>shield-canceler 0</source>
        <translation>&lt;p&gt;Cette arme génère une impulsion électromagnétique qui désactive temporairement tous les boucliers dans sa zone d’effet, y compris ceux du vaisseau émetteur.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>shield-canceler 2</source>
        <translation>&lt;li&gt;Les boucliers secondaires du vaisseau émetteur ne sont pas affectés par l’IEM.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>aggressive-shield-generator</source>
        <translation>Bouclier à percussion Zoubidou One Love</translation>
    </message>
    <message>
        <source>aggressive-shield-generator 0</source>
        <translation>&lt;p&gt;Pour ceux qui pensent que l’attaque est la meilleure défense, les laboratoires Zoubidou ont créé ce bouclier à percussion qui offre une protection maximale contre les collisions avec les objets de taille moyenne (1 à 100 tonnes) en détruisant ces objets.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>secondary-generator</source>
        <translation>Groupe électrogène Taiyaki par Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>secondary-generator 0</source>
        <translation>&lt;p&gt;Nous savons exactement ce que vous pensez : à quoi bon s’équiper de matériel dernier cri s’il faut passer son temps à attendre que les batteries se rechargent ? Pourquoi ces appareils consomment-ils tant d’énergie ? Désormais ces questions appartiennent au passé. Grâce à ce groupe électrogène, vous profiterez enfin pleinement du confort moderne en utilisant tous vos appareils sans limitation.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>secondary-shield-generator</source>
        <translation>Bouclier auxiliaire Zoubidou Companion</translation>
    </message>
    <message>
        <source>secondary-shield-generator 0</source>
        <translation>&lt;p&gt;Ce bouclier protège les parties les plus vulnérables du vaisseau en cas de panne du bouclier principal grâce à plusieurs unités indépendantes dédiées à chaque équipement.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>teleporter</source>
        <translation>Générateur de replis de l’espace</translation>
    </message>
    <message>
        <source>teleporter 0</source>
        <translation>&lt;p&gt;Le voyage sans se déplacer est possible, mais seulement à courte distance.&lt;/p&gt;&lt;p&gt;Depuis que le grand Lars Nedum s’est déplacé dans une étoile, ce module est équipé d’une sécurité &quot;Pinpin&quot; empêchant tout saut dangereux, mais consommant quand même de l’énergie&lt;/p&gt;</translation>
    </message>
    <message>
        <source>secondary-battery</source>
        <translation>Batterie supplémentaire Dorayaki par Shiitake Heavy Industries</translation>
    </message>
    <message>
        <source>secondary-battery 0</source>
        <translation>&lt;p&gt;Vous avez de grands projets mais pas les moyens de les réaliser ? Avec ces batteries supplémentaires, vous pourrez enfin stocker l’énergie dont vous avez besoin pour réaliser vos rêves.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>harpoon-launcher</source>
        <translation>Lance harpon Poséidon</translation>
    </message>
    <message>
        <source>harpoon-launcher 0</source>
        <translation>&lt;p&gt;Une après-midi d’été comme tant d’autres. Votre adversaire, une expression de terreur sur le visage, lançant des hurlements incohérents, tente d’échapper à votre justice. Mais c’est peine perdue : d’un coup de harpon, vous le maintenez à portée de vos canons, et vous lui assénez la correction qu’il mérite.&lt;/p&gt;&lt;p&gt;Ce qui passait pour un rêve est désormais une réalité grâce à ce nouveau harpon et son câble en fibre de titane.&lt;/p&gt;
</translation>
    </message>
    <message>
        <source>absorbing-shield-generator</source>
        <translation>Bouclier absorbant Zoubidou Assimilator</translation>
    </message>
    <message>
        <source>absorbing-shield-generator 0</source>
        <translation>&lt;p&gt;Ce bouclier récupère l’énergie des projectiles interceptés pour recharger le bouclier principal du vaisseau.&lt;/p&gt;
&lt;p&gt;Attention ! Une utilisation prolongée peut provoquer une surcharge du bouclier principal et des dégâts à la coque. Lisez attentivement la notice avant la première mise en route.&lt;/p&gt;
&lt;p&gt;Livré avec un filtre hypoallergénique.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>wall-generator</source>
        <translation>Générateur de barricades thêta-xi</translation>
    </message>
    <message>
        <source>wall-generator 0</source>
        <translation>&lt;p&gt;Ce canon génère et maintient simultanément jusqu’à trois barrières énergétiques réfléchissant tous les projectiles balistiques, électriques, acides et laser, y compris ceux de leur propriétaire.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>wall-generator 1</source>
        <translation>&lt;li&gt;Nombre maximum de murs : 3&lt;/li&gt;</translation>
    </message>
    <message>
        <source>wall-generator 2</source>
        <translation>&lt;li&gt;Le mur réfléchit tous les projectiles.&lt;/li&gt;</translation>
    </message>
</context>
<context>
    <name>Technology</name>
    <message>
        <source>&lt;h2&gt;Level %1&lt;/h2&gt;&lt;ul&gt;%2&lt;/ul&gt;</source>
        <translation>&lt;h2&gt;Niveau %1&lt;/h2&gt;&lt;ul&gt;%2&lt;/ul&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Required materials: %1 kg&lt;/li&gt;</source>
        <translation>&lt;li&gt;Matériaux nécessaires : %1 kg&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Effect duration: %1 s&lt;/li&gt;</source>
        <translation>&lt;li&gt;Durée de l’effet : %1 s&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Activation time: %1 s&lt;/li&gt;</source>
        <translation>&lt;li&gt;Temps d’activation : %1 s&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Fire rate: %1 Hz&lt;/li&gt;</source>
        <translation>&lt;li&gt;Cadence de tir : %1 Hz&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Firepower: %1 damages / %2&lt;/li&gt;</source>
        <translation>&lt;li&gt;Puissance de feu : %1 dégâts / %2&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Damage dealt during overload: %1 of base damages&lt;/li&gt;</source>
        <translation>&lt;li&gt;Dégats subits en cas de surchages : %1 des dégâts entrants&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Firepower: %1 damages&lt;/li&gt;</source>
        <translation>&lt;li&gt;Puissance de feu : %1 dégâts&lt;/li&gt;</translation>
    </message>
    <message>
        <source>s</source>
        <translation></translation>
    </message>
    <message>
        <source>impact</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;li&gt;Resistance: %1 HP&lt;/li&gt;</source>
        <translation>&lt;li&gt;Résistance : %1 PV&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Equipments regeneration: %1 HP / s&lt;/li&gt;</source>
        <translation>&lt;li&gt;Régénération des équipements : %1 PV / s&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Resistance: +%1 HP per equipment&lt;/li&gt;</source>
        <translation>&lt;li&gt;Résistance : +%1 PV par équipement&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield resistance: %1 HP&lt;/li&gt;</source>
        <translation>&lt;li&gt;Résistance du bouclier : %1 PV&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield regeneration: %1 HP / %2&lt;/li&gt;</source>
        <translation>&lt;li&gt;Régénération du bouclier : %1 PV / %2&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield downtime: %1 s&lt;/li&gt;</source>
        <translation>&lt;li&gt;Temps de réapparition du bouclier : %1 s&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Shield regeneration: %1 of base damages&lt;/li&gt;</source>
        <translation>&lt;li&gt;Régénération du bouclier : %1 des dégâts entrants&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Supplementary energy: %1 J&lt;/li&gt;</source>
        <translation>&lt;li&gt;Énergie supplémentaire : %1 J&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Energy regeneration: %1 J / s&lt;/li&gt;</source>
        <translation>&lt;li&gt;Régénération de l’énergie : %1 J / s&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;Energy consumed: %1 J / %2&lt;/li&gt;</source>
        <translation>&lt;li&gt;Énergie consommée : %1 J / %2&lt;/li&gt;</translation>
    </message>
    <message>
        <source>use</source>
        <translation>utilisation</translation>
    </message>
    <message>
        <source>&lt;li&gt;Materials consumed: %1 kg / %2&lt;/li&gt;</source>
        <translation>&lt;li&gt;Matériaux consommés : %1 kg / %2&lt;/li&gt;</translation>
    </message>
</context>
<context>
    <name>apis-zone</name>
    <message>
        <source>title</source>
        <translation>Chapitre 5 : Les biosphères APIS</translation>
    </message>
    <message>
        <source>Vagues restantes: %1</source>
        <translation></translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Nous voici enfin arrivés dans la zone des biosphères APIS.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Je ne détecte que six biosphères.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Cela paraît bien peu pour accueillir toutes les espèces considérées inadaptées par l’alliance INSECT.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Qu’en savez-vous ? Si cela se trouve, peu d’espèces sont jugées inadaptées. Nous sommes peut-être trop archaïques pour eux.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Je pense surtout qu’ils méprisent notre singularité.</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>Regardez. J’ai repéré un attroupement de vaisseaux.</translation>
    </message>
    <message>
        <source>manif 1</source>
        <translation>Nous ne pouvons laisser les Héters exterminer des peuples intelligents sous prétexte qu’ils sont vindicatifs !
L’alliance INSEC ne détruit pas les peuples ! L’alliance INSEC les assimile !
Il en a toujours été ainsi. C’est notre devoir civilisationnel ! </translation>
    </message>
    <message>
        <source>manif 2</source>
        <translation>En gelant les étoiles, ils glacent le cœur de leurs habitants.
Que ferons nous le jour où une espèce suffisamment intelligente viendra réclamer sa vengeance ?
Ce sera une guerre totale et sans merci !
Nous devons arrêter la folie des Héters !
Tous à la chambre des députés de l’alliance ! Les Héters doivent être arrêtés !</translation>
    </message>
    <message>
        <source>manif 3</source>
        <translation>Non à la glaciation ! Stop à l’extermination ! Tous pour la préservation ! </translation>
    </message>
    <message>
        <source>Après manif 1</source>
        <translation>Mais ils m’ont l’air fort sympathiques ! Ils me rappellent ma France.</translation>
    </message>
    <message>
        <source>Après manif 2</source>
        <translation>C’est sûrement l’opportunité de trouver des alliés.</translation>
    </message>
    <message>
        <source>Après manif 3</source>
        <translation>Vous avez raison Nectaire. Je vais m’entretenir de ce pas avec leur chef.</translation>
    </message>
    <message>
        <source>apis_before_invasion 1</source>
        <translation>Bonjour, je suis le responsable de la gestion des biosphères APIS.
Que puis je pour vous ?</translation>
    </message>
    <message>
        <source>apis_before_invasion 2</source>
        <translation>L’administration nous a redirigés vers vous pour trouver un logement.</translation>
    </message>
    <message>
        <source>apis_before_invasion 3</source>
        <translation>Malheureusement nous ne pouvons vous accueillir.
Comme vous pouvez le constater, il ne nous reste plus que six biosphères.
Toutes les autres ont été détruites.</translation>
    </message>
    <message>
        <source>apis_before_invasion 4</source>
        <translation>Détruites ?</translation>
    </message>
    <message>
        <source>apis_before_invasion 5</source>
        <translation>Des drones pillent nos biosphères. Nous suspectons les Héters d’être responsables de cette ignominie.
Étant contre la lutte armée, nous sommes à la recherche d’une solution diplomatique.
Toutefois, rien n’a abouti.</translation>
    </message>
    <message>
        <source>apis_before_invasion 6</source>
        <translation>S’ils attaquent, je vous défendrais ! Vous ne perdrez plus la moindre biosphère !</translation>
    </message>
    <message>
        <source>apis_before_invasion 7</source>
        <translation>On me prévient que des portails spatiaux sont en cours d’ouverture.
Ne recourrez pas à la violence gratuitement. Nous trouverons une solution.
</translation>
    </message>
    <message>
        <source>apis_before_invasion 8</source>
        <translation>Croyez mon expérience. Les Héters n’ont qu’une seule philosophie : Vae victis !
Et je ne serai pas le vaincu.</translation>
    </message>
    <message>
        <source>after_invasion 1</source>
        <translation>En effet, ce sont bien des drones Héters. Ces cafards pillent tout ce qu’ils trouvent.</translation>
    </message>
    <message>
        <source>after_invasion 2</source>
        <translation>Bravo Capitaine ! Les APIS seront sûrement reconnaissants.
Je n’en espérais pas moins de vous.</translation>
    </message>
    <message>
        <source>after_invasion 3</source>
        <translation>Point de flatterie Nectaire. Un héros doit savoir rester humble.</translation>
    </message>
    <message>
        <source>apis_after_invasion 1</source>
        <translation>Merci ! Sans vous toutes les biosphères auraient été détruites.
Pouvons-nous, à notre tour, vous venir en aide ?</translation>
    </message>
    <message>
        <source>apis_after_invasion 2</source>
        <translation>Où se trouve le vaisseau monde Héter ?</translation>
    </message>
    <message>
        <source>apis_after_invasion 3</source>
        <translation>Y’a-t-il un moyen de dégeler le Soleil ?</translation>
    </message>
    <message>
        <source>apis_after_invasion 4</source>
        <translation>En ce qui concerne le vaisseau monde Héter, personne ne sait où il se trouve.
Mais un vaisseau de commandement était de passage aujourd’hui. Il vient de repartir vers la constellation de Protura. Voici ses coordonnées...</translation>
    </message>
    <message>
        <source>apis_after_invasion 5</source>
        <translation>Il existe un moyen de dégeler n’importe quelle étoile. Pour cela, il vous faut une arme que seuls les citoyens INSEC peuvent acquérir. De plus, celle-ci est extrêmement onéreuse.</translation>
    </message>
    <message>
        <source>apis_after_invasion 6</source>
        <translation>Nous n’avons que peu de ressources. De plus, nous ne sommes pas citoyens INSEC.</translation>
    </message>
    <message>
        <source>apis_after_invasion 7</source>
        <translation>Alors il reste une dernière solution.
Nous avons les plans de l’arme.
Peut-être arriverez-vous à la fabriquer ?</translation>
    </message>
    <message>
        <source>apis_after_invasion 8</source>
        <translation>Je croyais que vous étiez non-violents ?</translation>
    </message>
    <message>
        <source>apis_after_invasion 9</source>
        <translation>Posséder des plans ne signifie pas que l’on désire les mettre en oeuvre.
Le savoir est necessaire, pas son application.</translation>
    </message>
    <message>
        <source>apis_after_invasion 10</source>
        <translation>Ces plans sont complexes. Malheureusement, il nous manque des pièces.
Il nous faut une cellule d’isolation spatiale...</translation>
    </message>
    <message>
        <source>apis_after_invasion 11</source>
        <translation>Nous pouvons vous en donner une. Cette technologie est nécessaire pour le bon fonctionnement de nos biosphères.</translation>
    </message>
    <message>
        <source>apis_after_invasion 12</source>
        <translation>Il nous manque aussi un Concentrateur laser WX99 et un Accumulateur de Puissance MYRMEL ZZ+</translation>
    </message>
    <message>
        <source>apis_after_invasion 13</source>
        <translation>Pour le concentrateur, il me semble que c’est la récompense de la prochaine course organisée par la famille MYRMEL et les industries POUTROX.</translation>
    </message>
    <message>
        <source>apis_after_invasion 14</source>
        <translation>Allons gagner cette course !</translation>
    </message>
</context>
<context>
    <name>aw::Info</name>
    <message>
        <source>enable</source>
        <translation>normal</translation>
    </message>
    <message>
        <source>disable</source>
        <translation>inversé</translation>
    </message>
    <message>
        <source>up_left_down_right</source>
        <translation>normal</translation>
    </message>
    <message>
        <source>up_right_down_left</source>
        <translation>horizontal inversé</translation>
    </message>
    <message>
        <source>down_left_up_right</source>
        <translation>vertical inversé</translation>
    </message>
    <message>
        <source>down_right_up_left</source>
        <translation>tout inversé</translation>
    </message>
    <message>
        <source>up</source>
        <translation>haut</translation>
    </message>
    <message>
        <source>down</source>
        <translation>bas</translation>
    </message>
    <message>
        <source>left</source>
        <translation>gauche</translation>
    </message>
    <message>
        <source>right</source>
        <translation>droite</translation>
    </message>
    <message>
        <source>up_down</source>
        <translation>vertical</translation>
    </message>
    <message>
        <source>down_up</source>
        <translation>vertical (inversé)</translation>
    </message>
    <message>
        <source>left_right</source>
        <translation>horizontal</translation>
    </message>
    <message>
        <source>right_left</source>
        <translation>horizontal (inversé)</translation>
    </message>
    <message>
        <source>plus</source>
        <translation>normal</translation>
    </message>
    <message>
        <source>minus</source>
        <translation>inversé</translation>
    </message>
</context>
<context>
    <name>boss-zone</name>
    <message>
        <source>title</source>
        <translation>Chapitre 5 : Le Propugnator</translation>
    </message>
    <message>
        <source>Propugnator: %1 secondes</source>
        <translation></translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Vite ! Quittez la planète Capitis avant l’arrivée du Propugnator !</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>De quoi s’agit-il ?</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>C’est le vaisseau principal de défense de la cité. Dépêchez vous ou son rayon tracteur vous empêchera d’utiliser votre propulsion hyper-exponentielle.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>C’est déjà le cas. Impossible de partir, il va falloir faire face.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Je n’ai peur de personne ! Qu’il vienne, s’il veut mourir !</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>Ne combattez pas. Une solution pacifique doit exister.</translation>
    </message>
    <message>
        <source>begin 7</source>
        <translation>Ne vous interposez pas ou vous risquez d’être détruit.
Dans ce genre de bataille, il y a beaucoup de dommages collatéraux.</translation>
    </message>
    <message>
        <source>after boss 1</source>
        <translation>L’armée INSEC entière ne peut rien contre moi !</translation>
    </message>
    <message>
        <source>after boss apis 1</source>
        <translation>Quelle violence ! Mais je vous sais gré de m’avoir épargné.</translation>
    </message>
    <message>
        <source>after boss apis 2</source>
        <translation>Puisque vous m’avez aidé, vous allez devenir hors-la-loi.</translation>
    </message>
    <message>
        <source>after boss apis 3</source>
        <translation>Les APIS sont déjà méprisés par les membres de l’alliance INSEC. C’est la première fois qu’une quelconque espèce bélliqueuse souhaite protéger les biosphères. Pour nous avoir sauvés, je vous serai éternellement reconnaissant.</translation>
    </message>
    <message>
        <source>after boss apis 4</source>
        <translation>J’écraserai tous ceux qui oppriment les faibles. Le fort se doit de protéger le faible, sinon il ne peut y avoir de civilisation.</translation>
    </message>
    <message>
        <source>after boss 2</source>
        <translation>Capitaine, je ne doute pas de vos capacités de destruction, mais nous devrions partir avant l’arrivée d’autres vaisseaux.</translation>
    </message>
    <message>
        <source>after boss 3</source>
        <translation>En effet ! Le vaisseau de commandement Héter ne perd rien pour attendre.</translation>
    </message>
</context>
<context>
    <name>chapter-0-1</name>
    <message>
        <source>title</source>
        <translation>Entrainement : pilotage</translation>
    </message>
    <message>
        <source>Recyclez le satellite</source>
        <translation type="obsolete">Recyclez le satellite</translation>
    </message>
    <message>
        <source>Construisez de nouveaux canons</source>
        <translation type="obsolete">Construisez de nouveaux canons jusqu&apos;à épuisement des matériaux</translation>
    </message>
    <message>
        <source>Détruisez le drone</source>
        <translation type="obsolete">Détruisez le drone</translation>
    </message>
    <message>
        <source>Objectif : %1</source>
        <translation type="obsolete">Objectif : %1</translation>
    </message>
    <message>
        <source>story 1 7 %1</source>
        <translation type="obsolete">Déplacez le vaisseau vers le point indiqué sur le radar à gauche de votre tableau de bord. Vous pouvez vous déplacer plus rapidement en utilisant le Céléritas Accretio (brevet en instance) avec &lt;b&gt;%1&lt;/b&gt;, mais celui-ci consomme de l’énergie.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation type="obsolete">Partons vite avant d’être atomisé. {2 1?}</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation type="obsolete">Partons vite avant d’être atomisé. {2 2?}</translation>
    </message>
    <message>
        <source>0_beginning</source>
        <translation>Bienvenue dans le simulateur de pilotage.
Il a pour but de vous apprendre à maitriser les commandes de votre vaisseau.
N&apos;oubliez pas que lire les textes explicatifs vous permettra de comprendre ce que vous avez à faire.
Si vous avez sauté des dialogues, cliquez sur l&apos;icône de bulle en bas à gauche.
Rendez-vous sur la première flèche.</translation>
    </message>
    <message>
        <source>1_first_door</source>
        <translation>Le but de cet exercice est de vous apprendre à vous déplacer rapidement et efficacement.&lt;br/&gt;Par défaut, les contrôles du vaisseau sont relatifs à son orientation. &lt;br/&gt;C&apos;est-à-dire que si vous appuyez sur la touche HAUT (&lt;b&gt;%1&lt;/b&gt;), le vaisseau se dirigera vers l&apos;avant du vaisseau.&lt;br/&gt;Vous pouvez changer cette configuration dans les options en cochant l&apos;option &quot;Déplacement du vaisseau relatif à l&apos;écran&quot;.&lt;br/&gt;C&apos;est à dire que si vous appuyez sur la touche HAUT (&lt;b&gt;%1&lt;/b&gt;), le vaisseau se dirigera vers le haut de l&apos;écran.&lt;br/&gt;Rendez-vous sur la flèche suivante.</translation>
    </message>
    <message>
        <source>2_second_door</source>
        <translation>Le vaisseau possède une capacité spéciale d&apos;accélération (Celeritas Accretio) permettant de doubler sa vitesse. &lt;br/&gt;Vous pouvez l&apos;activer en laissant appuyé le raccourci (&lt;b&gt;%1&lt;/b&gt;).&lt;br/&gt;Atteignez la flèche suivante avant que celle-ci ne s&apos;éteigne.</translation>
    </message>
    <message>
        <source>3_1_too_late</source>
        <translation>Vous n&apos;avez pas atteint la flèche à temps. Recommencez.</translation>
    </message>
    <message>
        <source>3_2_third_door</source>
        <translation>Bravo mais faites attention ! Plus vous allez vite, plus la collision avec un mur ou un ennemi est dévastatrice.
Rendez-vous sur la flèche suivante.</translation>
    </message>
    <message>
        <source>4_1_fourth_door</source>
        <translation>Atteignez les 2 flèches suivantes avant qu&apos;elles ne s&apos;éteignent.
Optimisez votre trajectoire. Franchir la ligne suffit à valider le point de passage.</translation>
    </message>
    <message>
        <source>4_2_too_late</source>
        <translation>Vous n&apos;avez pas atteint les flèches à temps. 
Recommencez et n&apos;oubliez pas d&apos;utiliser le Celeritas Accretio.</translation>
    </message>
    <message>
        <source>6_sixth_door</source>
        <translation>Bravo ! 
Rendez-vous sur la flèche suivante.</translation>
    </message>
    <message>
        <source>7_seventh_door</source>
        <translation>Certains ennemis seront sûrement indestructibles. 
Il faudra alors analyser leurs comportements pour trouver une ouverture et se faufiler dedans.</translation>
    </message>
    <message>
        <source>8_final_door</source>
        <translation>Bravo ! Vous avez terminé cet entrainement au pilotage.</translation>
    </message>
    <message>
        <source>1_first_door %1</source>
        <translation type="vanished">Le but de cet exercice est de vous apprendre à vous déplacer rapidement et efficacement.&lt;br/&gt;Par défaut, les contrôles du vaisseau sont relatifs à son orientation. &lt;br/&gt;C&apos;est-à-dire que si vous appuyez sur la touche HAUT (&lt;b&gt;%1&lt;/b&gt;), le vaisseau se dirigera vers l&apos;avant du vaisseau.&lt;br/&gt;Vous pouvez changer cette configuration dans les options en cochant l&apos;option &quot;Déplacement du vaisseau relatif à l&apos;écran&quot;.&lt;br/&gt;C&apos;est à dire que si vous appuyez sur la touche HAUT (&lt;b&gt;%1&lt;/b&gt;), le vaisseau se dirigera vers le haut de l&apos;écran.&lt;br/&gt;Rendez-vous sur la flèche suivante.</translation>
    </message>
</context>
<context>
    <name>chapter-0-2</name>
    <message>
        <source>title</source>
        <translation>Entrainement : visée et tir</translation>
    </message>
    <message>
        <source>Recyclez le satellite</source>
        <translation type="obsolete">Recyclez le satellite</translation>
    </message>
    <message>
        <source>Construisez de nouveaux canons</source>
        <translation type="obsolete">Construisez de nouveaux canons jusqu&apos;à épuisement des matériaux</translation>
    </message>
    <message>
        <source>Détruisez le drone</source>
        <translation type="obsolete">Détruisez le drone</translation>
    </message>
    <message>
        <source>Objectif : %1</source>
        <translation type="obsolete">Objectif : %1</translation>
    </message>
    <message>
        <source>story 1 7 %1</source>
        <translation type="obsolete">Déplacez le vaisseau vers le point indiqué sur le radar à gauche de votre tableau de bord. Vous pouvez vous déplacer plus rapidement en utilisant le Céléritas Accretio (brevet en instance) avec &lt;b&gt;%1&lt;/b&gt;, mais celui-ci consomme de l’énergie.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation type="obsolete">Partons vite avant d’être atomisé. {2 1?}</translation>
    </message>
    <message>
        <source>0_beginning</source>
        <translation>Bienvenue dans l&apos;entrainement à la visée et au tir.&lt;br/&gt;Abattez les drones en utilisant le bouton de tir (&lt;b&gt;%1&lt;/b&gt;) et en les visant.&lt;br/&gt;Le vaisseau possède une capacité spéciale d&apos;augmentation de cadence de tir (Modus Accretio).&lt;br/&gt;Vous pouvez l&apos;activer en laissant appuyé le raccourci (&lt;b&gt;%2&lt;/b&gt;).</translation>
    </message>
    <message>
        <source>1_first_wave_finished</source>
        <translation>Attention ! Les collisions endommagent votre vaisseau. Les prochains drones se déplaceront d&apos;une façon moins prévisible.
Abattez-les.</translation>
    </message>
    <message>
        <source>2_second_wave_finished</source>
        <translation>Les prochains drones se déplaceront dans toute la salle.
Abattez-les.</translation>
    </message>
    <message>
        <source>3_third_wave_finished</source>
        <translation>Bravo ! Vous avez terminé cet entrainement à la visée et au tir.</translation>
    </message>
</context>
<context>
    <name>chapter-0-3</name>
    <message>
        <source>title</source>
        <translation>Entrainement : combat</translation>
    </message>
    <message>
        <source>Recyclez le satellite</source>
        <translation type="obsolete">Recyclez le satellite</translation>
    </message>
    <message>
        <source>Construisez de nouveaux canons</source>
        <translation type="obsolete">Construisez de nouveaux canons jusqu&apos;à épuisement des matériaux</translation>
    </message>
    <message>
        <source>Détruisez le drone</source>
        <translation type="obsolete">Détruisez le drone</translation>
    </message>
    <message>
        <source>Objectif : %1</source>
        <translation type="obsolete">Objectif : %1</translation>
    </message>
    <message>
        <source>story 1 7 %1</source>
        <translation type="obsolete">Déplacez le vaisseau vers le point indiqué sur le radar à gauche de votre tableau de bord. Vous pouvez vous déplacer plus rapidement en utilisant le Céléritas Accretio (brevet en instance) avec &lt;b&gt;%1&lt;/b&gt;, mais celui-ci consomme de l’énergie.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation type="obsolete">Partons vite avant d’être atomisé. {2 1?}</translation>
    </message>
    <message>
        <source>0_beginning</source>
        <translation>Bienvenue dans l&apos;entrainement au combat.
Les drones ont désormais un canon identique aux vôtres.
En voici un. Abattez-le.</translation>
    </message>
    <message>
        <source>1_first_wave_finished</source>
        <translation>Vous avez pu remarquer que le drone cherche à vous cibler.
Tourner autour de lui est une méthode efficace pour esquiver les projectiles.
Voici deux drones. Abattez-les.</translation>
    </message>
    <message>
        <source>2_second_wave_finished</source>
        <translation>Bravo ! Vous serez souvent pris en tenaille. 
Votre vaisseau peut encaisser plusieurs tirs mais ceux-ci endommageront prioritairement vos équipements proches de l&apos;impact.
Voici quatre drones. Abattez-les.</translation>
    </message>
    <message>
        <source>3_third_wave_finished</source>
        <translation>Bravo ! Certains ennemis seront plus complexes. Ils possèderont plusieurs équipements les rendant plus résistants et plus dangereux.
Voici un ennemi plus coriace. Abattez-le.</translation>
    </message>
    <message>
        <source>4_fourth_wave_finished</source>
        <translation>Bravo ! Vous avez fini la simulation. Vous êtes fin prêt pour votre mission.</translation>
    </message>
</context>
<context>
    <name>chapter-1</name>
    <message>
        <source>title</source>
        <translation>Chapitre 1 : Orbite terrestre</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Bonjour, Capitaine ! Je vois que vous êtes bien arrivé dans l’espace. Je vous avais bien dit que notre base secrète du Puy-de-Dôme servirait un jour !</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Je vous rappelle les objectifs de votre mission :
- entrer en contact avec les extraterrestres ;
- évaluer le danger ;
- agir en conséquence.</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Bon ! Il est temps d’activer Nectaire, l’intelligence artificielle de votre vaisseau qui vous guidera par la suite. Bonne chance ! Nous croyons tous en votre réussite !</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Bzzt...</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Activation terminée !</translation>
    </message>
    <message>
        <source>story 1 6</source>
        <translation>Bonjour Capitaine ! De manière à vous familiariser avec les commandes du vaisseau, nous allons procéder à quelques exercices.</translation>
    </message>
    <message>
        <source>story 1 7 %1</source>
        <translation>Déplacez le vaisseau vers le point indiqué sur le radar à gauche de votre tableau de bord. Vous pouvez vous déplacer plus rapidement en utilisant le Céléritas Accretio (brevet en instance) avec &lt;b&gt;%1&lt;/b&gt;, mais celui-ci consomme de l’énergie.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Bravo ! Il s’agit d’un vieux satellite que nous pouvons recycler.</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>Ce recyclage nous a fourni 400 kg de matériaux de construction, comme vous pouvez le constater sur votre tableau de bord.</translation>
    </message>
    <message>
        <source>story 2 3</source>
        <translation>Avec ceci nous allons pouvoir construire des canons pour nous défendre en cas d’agression.</translation>
    </message>
    <message>
        <source>story 2 4</source>
        <translation>Dans l’écran « Vaisseau », sélectionnez un canon dans la colonne de gauche et installez-le sur quatre emplacements disponibles de votre vaisseau.</translation>
    </message>
    <message>
        <source>story 2 5</source>
        <translation>Vous remarquerez qu’il me faut cinq secondes pour construire ces équipements. Durant cette période, ils sont très fragiles, et le moindre dégât infligé à la coque du vaisseau peut les détruire.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>Les canons sont prêts. Il est temps de les tester.</translation>
    </message>
    <message>
        <source>story 3 2 %1 %2</source>
        <translation>Utilisez &lt;b&gt;%1&lt;/b&gt; pour tirer. Vous pouvez doubler votre cadence de tir en utilisant le Modus Accretio (brevet en instance) avec &lt;b&gt;%2&lt;/b&gt;, mais celui-ci consomme de l’énergie.</translation>
    </message>
    <message>
        <source>story 3 3</source>
        <translation>Un drone d’entraînement vient à votre rencontre. Abattez-le !</translation>
    </message>
    <message>
        <source>story 4</source>
        <translation>Voyez ! Le drone a laissé quelques débris. Ce sont des matériaux de construction que vous pouvez ramasser.</translation>
    </message>
    <message>
        <source>story 5</source>
        <translation>Le signal du vaisseau extraterrestre est proche. Allez à sa rencontre et gardez à l’esprit les consignes transmises par le CNES.</translation>
    </message>
    <message>
        <source>story 6 1</source>
        <translation>Mon détecteur Kawai est saturé ! Ce vaisseau est étrangement mignon, restez sur vos gardes.</translation>
    </message>
    <message>
        <source>story 6 2</source>
        <translation>J’active le message lumineux de bienvenue. Aucune réponse pour l’instant.</translation>
    </message>
    <message>
        <source>story 6 3</source>
        <translation>Je réessaie. Toujours rien.</translation>
    </message>
    <message>
        <source>story 6 4</source>
        <translation>Boudidiou ! Ce bouton fonctionne-t-il correctement ? Encore du matériel français !</translation>
    </message>
    <message>
        <source>story 6 5</source>
        <translation>Oups ! Me serais-je trompé de bouton ?</translation>
    </message>
    <message>
        <source>story 6 6</source>
        <translation>Resynchronisation effectuée ! J’ai perdu le contact un instant capitaine !</translation>
    </message>
    <message>
        <source>story 6 7</source>
        <translation>Nous sommes les HÉTERS. Votre comportement vient d’enfreindre l’article 20-4500A de la législation INSEC, nous donnant l’autorisation d’assimilation et d’extermination de toute espèce belliqueuse. All your base are belong to us!</translation>
    </message>
    <message>
        <source>story 7 1</source>
        <translation>Ce premier contact n’est pas très positif.</translation>
    </message>
    <message>
        <source>story 7 2</source>
        <translation>Mais la destruction de ce vaisseau fut une bonne chose : je peux assimiler une partie de la technologie de nos agresseurs. J’ai déjà assimilé leur système de génération de bouclier.</translation>
    </message>
    <message>
        <source>story 7 3</source>
        <translation>De plus, en me complexifiant je peux me spécialiser pour mieux répondre à vos attentes : vous pouvez dès maintenant rechercher ou améliorer une technologie depuis l’écran « Technologies ».</translation>
    </message>
    <message>
        <source>story 8 1</source>
        <translation>Notez que certaines fonctionnalités ne seront accessibles que si vous débloquez les bonnes technologies.</translation>
    </message>
    <message>
        <source>story 8 2 %1</source>
        <translation>Le bouclier se recharge progressivement, mais s’il encaisse trop de dégâts il lui faudra un peu de temps avant de reprendre ses fonctions. Vous pouvez le recharger plus rapidement en utilisant le Clipeus Accretio (brevet en instance) avec &lt;b&gt;%1&lt;/b&gt;, mais celui-ci consomme de l’énergie.</translation>
    </message>
    <message>
        <source>story 8 3</source>
        <translation>Certains équipements consomment de l’énergie, d’autres consomment des matériaux de construction. L’énergie se recharge progressivement, tandis que les matériaux doivent être récupérés dans les débris.</translation>
    </message>
    <message>
        <source>story 8 4</source>
        <translation>Votre tableau de bord montre l’état des équipements installés sur le vaisseau.</translation>
    </message>
    <message>
        <source>story 8 5</source>
        <translation>Cliquez avec le bouton gauche sur l’icône d’une arme ou d’un outil pour l’utiliser immédiatement.</translation>
    </message>
    <message>
        <source>story 8 6 %1</source>
        <translation>Cliquez avec le bouton droit sur l’icône d’une arme ou d’un outil pour le cocher. Vous pouvez alors utiliser &lt;b&gt;%1&lt;/b&gt; pour activer tous les équipements cochés.</translation>
    </message>
    <message>
        <source>story 8 7</source>
        <translation>Cliquez sur le bouton « Messages » au-dessus du radar pour consulter à nouveau ces explications.</translation>
    </message>
    <message>
        <source>story 9 1</source>
        <translation>Mes capteurs signalent une activité extraterrestre sur Mercure. Il est de votre mission de reconnaître ou non la menace.</translation>
    </message>
    <message>
        <source>story 9 2</source>
        <translation>Le CNES ne m’a pas sélectionné pour ma lâcheté ! Allons-y !</translation>
    </message>
    <message>
        <source>story 9 3</source>
        <translation>Il est temps de tester l’hyper-exponentiel. Pleine puissance monsieur Sulu !</translation>
    </message>
    <message>
        <source>story 9 4</source>
        <translation>Je m’appelle LYCOP !</translation>
    </message>
    <message>
        <source>story 9 5</source>
        <translation>Bzzt, il s’agit d’une erreur de données. Nom corrigé !</translation>
    </message>
    <message>
        <source>Recyclez le satellite</source>
        <translation>Recyclez le satellite</translation>
    </message>
    <message>
        <source>Détruisez le drone</source>
        <translation>Détruisez le drone</translation>
    </message>
    <message>
        <source>Objectif : %1</source>
        <translation>Objectif : %1</translation>
    </message>
    <message>
        <source>Construisez de nouveaux canons</source>
        <translation>Construisez de nouveaux canons jusqu&apos;à épuisement des matériaux</translation>
    </message>
    <message>
        <source>Utilisez votre point de technologie</source>
        <translation>Utilisez votre point de technologie</translation>
    </message>
</context>
<context>
    <name>chapter-1b</name>
    <message>
        <source>title</source>
        <translation>Chapitre 1 : Orbite terrestre (fin)</translation>
    </message>
</context>
<context>
    <name>chapter-2</name>
    <message>
        <source>title</source>
        <translation>Chapitre 2 : Mercure</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Arrêt de l’hyper-exponentiel. Nous voici sur Mercure, capitaine !</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Mmh, je ne vois que de la poussière et des rochers. Nectaire, êtes-vous bien certain que le signal vient d’ici ?</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>C’est difficile à dire. Nous sommes proches du soleil et des anomalies magnétiques perturbent mes capteurs.</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Diable ! Mais enfin, Nectaire, ne pouvez-vous pas au moins calculer une position approximative ?</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Évaluation de la meilleure hypothèse... Veuillez patienter...</translation>
    </message>
    <message>
        <source>story 1 6</source>
        <translation>Le signal semble venir de la zone que j’indique sur votre carte, avec une probabilité de 21.03%.</translation>
    </message>
    <message>
        <source>point 1 1</source>
        <translation>Allons bon. Il n’y a rien ici non plus...</translation>
    </message>
    <message>
        <source>point 1 2</source>
        <translation>Je détecte une activité suspecte tout autour de nous.</translation>
    </message>
    <message>
        <source>point 1 3</source>
        <translation>C’est un piège !</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Ah, les lâches ! Attaquer comme ça, par surprise ! Ça me dégoûte !</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>Je détecte beaucoup d’activité en sous-sol et je recommande la plus grande méfiance pour la suite de cette mission. J’ai aussi mis à jour la position de la source du signal en intégrant les derniers relevés.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>Ah les bandits ! Ils ne paient rien pour attendre ! Quant à vous, Nectaire, je commence à douter de votre fiabilité.</translation>
    </message>
    <message>
        <source>story 3 2</source>
        <translation>Les résultats précédents sont dans la marge d’erreur. En outre, la quantité d’infrastructures défensives que nous rencontrons et l’activité électrique en sous-sol semblent indiquer la présence d’une base sur Mercure.</translation>
    </message>
    <message>
        <source>story 3 3</source>
        <translation>Quelle perspicacité ! N’avez-vous pas été programmé pour donner des informations plus pertinentes ?</translation>
    </message>
    <message>
        <source>story 3 4</source>
        <translation>J’ai été programmé, entre autres, pour vous servir de soutien psychologique pendant cette mission. C’est pour cette raison que nous avons cet échange de banalités. Par ailleurs, mes calculs sont terminés. Voici une nouvelle source potentielle du signal.</translation>
    </message>
    <message>
        <source>story 4 1</source>
        <translation>Enfin, Nectaire, ressaisissez-vous ! Nous n’allons quand même pas tomber dans tous les pièges qu’ils nous ont tendus ! Où en est la localisation de cette base ?</translation>
    </message>
    <message>
        <source>story 4 2</source>
        <translation>Si j’étais programmé pour ressentir la honte, je rougirais de ces échecs. Cependant, éliminer ces hypothèses nous rapproche de l’endroit que nous cherchons. Courage !</translation>
    </message>
    <message>
        <source>story 4 3</source>
        <translation>Maudits chercheurs... Pourquoi ne puis-je pas avoir un ordinateur normal ?</translation>
    </message>
    <message>
        <source>story 4 4</source>
        <translation>Il serait beaucoup moins performant. En outre, j’ai réactualisé la position de la source du signal.</translation>
    </message>
    <message>
        <source>story 4 5</source>
        <translation>Bien, mais gare à vous si c’est encore un piège.</translation>
    </message>
    <message>
        <source>story 5 1</source>
        <translation>C’en est assez ! Je vous débranche ! Et je retournerai moi-même chaque caillou de cette planète !</translation>
    </message>
    <message>
        <source>story 5 2</source>
        <translation>Attendez ! Cette fois-ci c’est certain, le signal vient de quelque part au pied de cette falaise qu’on aperçoit là-bas.</translation>
    </message>
    <message>
        <source>story 5 3</source>
        <translation>Vraiment certain ?</translation>
    </message>
    <message>
        <source>story 5 4</source>
        <translation>Absolument certain.</translation>
    </message>
    <message>
        <source>story 5 5</source>
        <translation>Bon, je veux bien vérifier une dernière fois. Allons-y.</translation>
    </message>
    <message>
        <source>story 5 6</source>
        <translation>Par contre, leur base semble être souterraine.</translation>
    </message>
    <message>
        <source>story 5 7</source>
        <translation>Eh bien, ce n’est pas un problème, nous creuserons un trou pour y entrer.</translation>
    </message>
    <message>
        <source>story 5 8</source>
        <translation>Je ne suis pas équipé de foreuses.</translation>
    </message>
    <message>
        <source>story 5 9</source>
        <translation>Mais qui vous a parlé de foreuses ? Nous avons des canons !</translation>
    </message>
    <message>
        <source>story 6 1</source>
        <translation>Capitaine, nous pouvons entrer dans leur base par cette ouverture.</translation>
    </message>
    <message>
        <source>story 6 2</source>
        <translation>Bien ! Allons voir ce que fabriquent ces Héters.</translation>
    </message>
</context>
<context>
    <name>chapter-2-escape</name>
    <message>
        <source>title</source>
        <translation>Chapitre 2 : Mercure déshéterisée</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Bravo, capitaine ! La base est détruite et le vaisseau n’a subi que des dégâts mineurs.</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>C’est l’métier, mon p’tit, c’est l’métier.</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Contre toute attente, un vaisseau a résisté à l’explosion de la base. Il a pris la fuite en direction du Soleil.</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Comment est-ce possible ? Peu importe, il ne vivra pas un jour de plus. Je n’ai pas le droit de laisser une menace potentielle en liberté.</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Désolé, mais sa vitesse est bien supérieure à la notre. Nous ne pourrons pas le rattraper.</translation>
    </message>
    <message>
        <source>story 1 6</source>
        <translation>*** réflexion intense...</translation>
    </message>
    <message>
        <source>story 1 7</source>
        <translation>Ah ! J’ai trouvé ! Nous allons utiliser le champ gravitationnel du Soleil pour accélérer. Ainsi nous devrions l’égaler en vitesse, et même le surpasser. Qu’en dites-vous ?</translation>
    </message>
    <message>
        <source>story 1 8</source>
        <translation>Cela pourrait fonctionner, mais le vaisseau risquerait de ne pas y résister. C’est trop dangereux. N’avez-vous donc aucun instinct de survie ?</translation>
    </message>
    <message>
        <source>story 1 9</source>
        <translation>Ces broutilles ne me font pas peur. Tout ce qui m’importe est la suppression de la menace. Le courage, Nectaire, c’est de ne pas avoir peur !</translation>
    </message>
</context>
<context>
    <name>chapter-2b</name>
    <message>
        <source>title</source>
        <translation>Chapitre 2 : Base secrète sur Mercure</translation>
    </message>
    <message>
        <source>escape %1</source>
        <translation>autodestruction dans %1</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Cette structure est impressionnante. Depuis combien de temps ces Héters se terrent-ils ici ?</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Mes capteurs ne me permettent pas de calculer l’âge de ces murs, mais je détecte une importante circulation d’énergie derrière eux. Cette base doit être alimentée par un générateur d’une puissance titanesque.</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Plus le générateur est puissant, plus l’explosion sera belle !</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Regardez, capitaine ! Cette structure métallique à gauche ressemble à une porte.</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Parfait, nous allons frapper avant d’entrer.</translation>
    </message>
    <message>
        <source>room 4 1</source>
        <translation>D’après mes relevés, une partie des infrastructures n’est plus alimentée.</translation>
    </message>
    <message>
        <source>room 4 2</source>
        <translation>Déjà ? N’est-ce pas trop facile ? J’ai l’impression qu’ils nous ont sous-estimés.</translation>
    </message>
    <message>
        <source>room 6 1</source>
        <translation>Cette porte semble protégée par un puissant champ de force.</translation>
    </message>
    <message>
        <source>room 6 2</source>
        <translation>Que peuvent-ils bien cacher derrière ?</translation>
    </message>
    <message>
        <source>room 7a 1</source>
        <translation>Voici le générateur principal de l’installation.</translation>
    </message>
    <message>
        <source>room 7a 2</source>
        <translation>Enfin ! Après l’avoir détruit, cette base ne sera plus une menace. Ces Héters vont regretter d’avoir attaqué la France !</translation>
    </message>
    <message>
        <source>room 7a 3</source>
        <translation>Voulez-vous dire « la Terre » ?</translation>
    </message>
    <message>
        <source>room 7a 4</source>
        <translation>Ne jouez pas sur les mots, Nectaire.</translation>
    </message>
    <message>
        <source>room 7b 1</source>
        <translation>L’explosion du générateur a activé le système d’autodestruction de la base. Capitaine, vous avez 4 minutes pour évacuer.</translation>
    </message>
    <message>
        <source>room 7b 2</source>
        <translation>En effet, mieux vaut ne pas rester dans les parages.</translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>L’autodestruction de la base s’est interrompue.</translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>Malheur, et moi qui me réjouissais par avance de voir ces lieux réduits en poussière.</translation>
    </message>
    <message>
        <source>story 2 3</source>
        <translation>Cette interruption a probablement été causée par la machine qui vient de démarrer dans cette salle.</translation>
    </message>
    <message>
        <source>story 2 4</source>
        <translation>Mais alors, l’autodestruction reprendra si je détruis cette machine !</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>J’ai pu assimiler un module qui me permet de décrypter les messages transmis par les Héters !</translation>
    </message>
    <message>
        <source>story 3 2</source>
        <translation>Bien ! Ainsi, s’ils décident de revenir nous menacer, nous saurons les recevoir.</translation>
    </message>
    <message>
        <source>story 3 3</source>
        <translation>Le système d’autodestruction de la base a redémarré. Il nous reste peu de temps pour évacuer. Hâtez-vous !</translation>
    </message>
</context>
<context>
    <name>chapter-2c</name>
    <message>
        <source>Collected materials: %1 kg</source>
        <translation>Matériaux récoltés : %1 kg</translation>
    </message>
    <message>
        <source>title</source>
        <translation>Interlude : Pillage de la base Héter</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>Capitaine ! Nous voici dans le cratère laissé par l’explosion de la base Héter. J’installe un module de recyclage au sol. Avant de partir, n’oubliez pas de le récupérer. Il vous suffira de quelques secondes pour charger le module et son contenu.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>Espérons que la récolte soit rapide et fructueuse.
Je ne souhaite pas m’attarder ici.</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>Je détecte des vaisseaux en approche. Ils n’ont pas la signature des Héters, mais ils ne semblent pas animés de meilleures intentions.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>Ces charognards viennent se repaître des décombres de la base Héter.</translation>
    </message>
    <message>
        <source>story begin 5</source>
        <translation>Un peu comme nous capitaine...</translation>
    </message>
    <message>
        <source>story begin 6</source>
        <translation>Voyons, Nectaire, nous ne sommes pas de vulgaires pillards ! Notre noble quête a besoin de ces matériaux, contrairement à ces opportunistes.</translation>
    </message>
    <message>
        <source>story secret 1</source>
        <translation>Capitaine, le module de recyclage a extrait une cellule d’énergie encore fonctionnelle.</translation>
    </message>
    <message>
        <source>story secret 2</source>
        <translation>Il est grand temps de quitter cette planète.</translation>
    </message>
    <message>
        <source>story secret 3</source>
        <translation>Comme vous voulez. Vous pouvez aussi rester afin de récolter plus de matériaux.</translation>
    </message>
    <message>
        <source>story loose 1</source>
        <translation>Le module de recyclage a été détruit.</translation>
    </message>
    <message>
        <source>story loose 2</source>
        <translation>Quelle perte de temps !</translation>
    </message>
    <message>
        <source>story win 1 %1</source>
        <translation>Le module de recyclage est chargé dans la soute. Il a récolté %1 kg de matériaux.</translation>
    </message>
    <message>
        <source>story win 2</source>
        <translation>Excellent ! À présent, ces Héters vont payer pour leur infamie !</translation>
    </message>
</context>
<context>
    <name>chapter-2z</name>
    <message>
        <source>demo</source>
        <translation>&lt;h1&gt;Félicitation !&lt;/h1&gt;&lt;h2&gt;Vous avez terminé la démo des aventures du capitaine Lycop : l’invasion des Héters !&lt;/h2&gt;
&lt;p&gt;Si vous le souhaitez, vous pouvez retourner visiter Mercure en conservant vos équipements et vos points de technologie.&lt;/p&gt;
&lt;p&gt;Vous avez aimé la démo ? Le jeu complet est désormais disponible sur Steam.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>vote</source>
        <translation>Page du magasin Steam</translation>
    </message>
    <message>
        <source>continue</source>
        <translation>Continuer à jouer</translation>
    </message>
    <message>
        <source>title</source>
        <translation>Chapitre 2 : Mercure déshéterisée (fin)</translation>
    </message>
</context>
<context>
    <name>chapter-3</name>
    <message>
        <source>title</source>
        <translation>Chapitre 3 : À la poursuite du Hideleo</translation>
    </message>
    <message>
        <source>Hideleo: %1 km</source>
        <translation>Hideleo : %1 km</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>La température du vaisseau augmente. N’approchez pas trop près du Soleil, nous n’y survivrions pas.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>Vous oubliez que c’est à proximité du Soleil que nous sommes les plus rapides. Il faut impérativement rattraper ce croiseur.</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>Soyez prudent, le Soleil est très actif en ce moment. Les éruptions solaires sont donc fréquentes.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>Parfait, ça ajoutera un peu de piment.</translation>
    </message>
    <message>
        <source>story begin 5</source>
        <translation>Faites aussi attention aux vaisseaux qui viennent à notre rencontre. J’en détecte quelques-uns en approche, et à cette vitesse une collision serait fatale.</translation>
    </message>
    <message>
        <source>story begin 6</source>
        <translation>Ces Héters sont vraiment intéressants. Je trouve remarquable que je puisse prédire si précisément le comportement de leur flotte, alors que les actions des humains semblent si chaotiques. </translation>
    </message>
    <message>
        <source>story begin 7</source>
        <translation>Ha ! Mais c’est parce que nous sommes totalement imprévisibles ! Notre constante quête de progrès et d’idéal nous pousse dans des voies jusqu’ici inexplorées. Voyez-vous, Nectaire, c’est ce qui fait la grandeur de la France, cette nation d’éternels insatisfaits. Mais je doute qu’une machine puisse comprendre cela.</translation>
    </message>
    <message>
        <source>story begin 8</source>
        <translation>Ma mission est aussi d’apprendre et d’évoluer.</translation>
    </message>
    <message>
        <source>story boss 1</source>
        <translation>Le croiseur est maintenant en vue. Sachez que je n’estimais qu’à 0,05% nos chances de réussite.</translation>
    </message>
    <message>
        <source>story boss 2</source>
        <translation>Scannez-le, Nectaire. Il me semble différent...</translation>
    </message>
    <message>
        <source>story boss 3</source>
        <translation>Il est lourdement armé, et contient une petite sous-structure non identifiée et dotée d’une énergie considérable.</translation>
    </message>
    <message>
        <source>story boss 4</source>
        <translation>Il ne sera bientôt qu’un tas de poussière.
Mais... Que fait-il ? On dirait qu’il s’est arrêté pour nous attendre...</translation>
    </message>
    <message>
        <source>story boss 5</source>
        <translation>En effet, capitaine. Mais ne vous hâtez pas, et songez un peu à votre survie. Les morts ne festoient pas.</translation>
    </message>
    <message>
        <source>story concentration 1</source>
        <translation>Leur coque devrait céder d’un instant à l’autre.</translation>
    </message>
    <message>
        <source>story concentration 2</source>
        <translation>Je vais maintenant porter le coup de grâce !</translation>
    </message>
    <message>
        <source>story concentration 3</source>
        <translation>Race inférieure, vous ne savez pas à qui vous vous mesurez. Une espèce aussi primitive que la vôtre ne mérite pas d’être assimilée. Sans votre étoile, votre civilisation dépérira et disparaîtra à jamais. Même si l’alliance INSEC condamne nos méthodes, cette attaque assurera notre domination sur ce système planétaire.</translation>
    </message>
    <message>
        <source>story concentration 4</source>
        <translation>Il produit de plus en plus d’énergie. Capitaine, je vous suggère de vous éloigner avant que la situation ne devienne vraiment critique.</translation>
    </message>
    <message>
        <source>story concentration 5</source>
        <translation>Non, je l’aurai abattu avant qu’il ne puisse agir.</translation>
    </message>
    <message>
        <source>story concentration fin 1</source>
        <translation>Énergie chargée à 100%... 110%... 120% !</translation>
    </message>
    <message>
        <source>story concentration fin 2</source>
        <translation>RAYON FRIGO-GIVRO-TRON, PUISSANCE MAXIMALE !!!!</translation>
    </message>
    <message>
        <source>story freeze 1</source>
        <translation>Le Soleil... Il se solidifie.</translation>
    </message>
    <message>
        <source>story freeze 2</source>
        <translation>Juste avant de se désintégrer, ils ont pu lancer une attaque qui a interrompu la réaction thermonucléaire du Soleil.</translation>
    </message>
    <message>
        <source>story freeze 3</source>
        <translation>Sans Soleil, plus de nature... Sans nature, plus de nourriture... Mon jardin ! Mes tomates ! Nooooooooon !!</translation>
    </message>
    <message>
        <source>story freeze 4</source>
        <translation>La Terre va connaître une terrible crise. C’est peut être la fin du genre humain.</translation>
    </message>
    <message>
        <source>story freeze 5</source>
        <translation>Non, je ne l’accepterai jamais. Je pourchasserai ces misérables jusqu’à l’autre bout de la galaxie pour exécuter ma vengeance. Je n’aurai de répit qu’après avoir fait disparaître le dernier d’entre eux. Je consacrerai ma vie à leur extermination. Leur souffrance sera mon plaisir.</translation>
    </message>
    <message>
        <source>story freeze 6</source>
        <translation>Calmez-vous, capitaine. Une telle réponse n’attisera que la haine.</translation>
    </message>
    <message>
        <source>story freeze 7</source>
        <translation>Qu’importe ! Qu’ils me haïssent, pourvu qu’ils me craignent !</translation>
    </message>
    <message>
        <source>story freeze 8</source>
        <translation>Je reçois un message en provenance de la Terre. Il a été émis il y a plusieurs heures. Il semblerait que le vaisseau Héter brouillait nos communications.</translation>
    </message>
    <message>
        <source>story freeze 9</source>
        <translation>Capitaine, après votre saut vers Mercure, des hordes d’extraterrestres ont envahi la Terre. Apparemment, Ils récoltent toutes formes de vie. *
Pour le moment, nous nous sommes réfugiés dans des abris souterrains, et nos systèmes de survie solaires semblent fonctionner comme prévu.
Revenez au plus vite ! Seul le fleuron de la technologie française peut faire la différence.</translation>
    </message>
    <message>
        <source>story freeze 10</source>
        <translation>Nectaire, cap sur la Terre.</translation>
    </message>
    <message>
        <source>story freeze 11</source>
        <translation>Afin d’éviter une pénurie, peut-être devrions-nous d’abord nous réapprovisionner en matériaux sur Mercure ?</translation>
    </message>
</context>
<context>
    <name>chapter-4</name>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
    <message>
        <source>Colonisator : %1 secondes</source>
        <translation></translation>
    </message>
    <message>
        <source>title</source>
        <translation>Chapitre 4 : Retour sur Terre</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>Je capte des signaux Héters. Ils sont nombreux. Peut être serait-il judicieux d’éviter la confrontation.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>Indiquez-moi l’emplacement de la base secrète du Puy-de-Dôme.
Rien ne m’empêchera de porter secours aux survivants.</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>C’est fait. Mais j’insiste sur la nécessité d’être discrets.
Pour éviter d’être repérés, je réduis la puissance des projecteurs du vaisseau et je désactive les sauts collapsars.
De plus, j’affiche sur votre écran les zones de perception des Héters.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>Seuls les pleutres ont besoin d’être discrets. Les héros valeureux font face et écrasent leurs opposants !</translation>
    </message>
    <message>
        <source>detected</source>
        <translation>Capitaine, nous sommes repérés. Je détecte un puissant vaisseau en orbite qui cherche à nous cibler.
Évacuons la zone au plus vite sinon notre espérance de vie diminuera drastiquement.</translation>
    </message>
    <message>
        <source>cathedrale 1</source>
        <translation>C’est la cathédrale de Clermont-Ferrand ! Elle a su aussi bien résister aux âges qu’aux Héters. </translation>
    </message>
    <message>
        <source>cathedrale 2</source>
        <translation>Je détecte un signal en provenance de celle-ci. Il s’agit d’un petit drone qui s’est écrasé à l’intérieur. </translation>
    </message>
    <message>
        <source>cathedrale 3</source>
        <translation>Récupérons-le et décodons les informations qu’il contient.</translation>
    </message>
    <message>
        <source>cathedrale 4</source>
        <translation>Drone récupéré... Décodage en cours... </translation>
    </message>
    <message>
        <source>cathedrale 5</source>
        <translation>Il s’agit d’une carte stellaire. Son système de coordonnées m’est étranger. Actuellement, elle ne nous est d’aucune utilité.</translation>
    </message>
    <message>
        <source>puy 1</source>
        <translation>Je ne détecte aucun signe de vie.</translation>
    </message>
    <message>
        <source>puy 2</source>
        <translation>...
Récupérez les archives informatiques de la base.
Nous devons savoir ce qu’il s’est passé.</translation>
    </message>
    <message>
        <source>puy 3</source>
        <translation>Chargement en cours... Terminé.
J’ai le journal de la base.</translation>
    </message>
    <message>
        <source>puy 4</source>
        <translation>Diffusez-le.</translation>
    </message>
    <message>
        <source>puy 5</source>
        <translation>Mardi 2 juillet 2024 – 18h : Le capitaine Lycop a signalé que la mission du vaisseau extraterrestre n’était en rien pacifique. L’état d’urgence a été déclaré.</translation>
    </message>
    <message>
        <source>puy 6</source>
        <translation>Mercredi 3 juillet 2024 – 15h : Les satellites ont détecté une myriade de vaisseaux.
Un croiseur titanesque semble diriger les autres.
Nous avons pour ordre de chercher ses faiblesses. </translation>
    </message>
    <message>
        <source>puy 7</source>
        <translation>Mercredi 3 juillet 2024 – 17h : La Terre est perdue. Nos armées n’ont pas tenu plus de 10 minutes.
Nous avons envoyé un appel à l’aide au capitaine Lycop, notre dernier atout.</translation>
    </message>
    <message>
        <source>puy 8</source>
        <translation>Mercredi 3 juillet 2024 – 23h : La surface de la Terre est désolée. Les Héters moissonnent notre planète.
Le vaisseau de commandement est parti. Notre mission a échoué, ce croiseur semble indestructible.</translation>
    </message>
    <message>
        <source>puy 9</source>
        <translation>Jeudi 4 juillet 2024 – 07h : Le soleil ne s’est pas levé. Sans énergie, nous ne pouvons rien.
Les Héters ont pénétré dans la base. Ils ont pris le contrôle des étages supérieurs.
Le fracas de leurs armes résonne dans la structure. Ils arrivent...</translation>
    </message>
    <message>
        <source>puy 10</source>
        <translation>L’enregistrement est terminé...
Capitaine, nous ne devrions pas nous attarder. Une puissante source d’énergie est en approche.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Capitaine, vous êtes bien silencieux. Usuellement, vous vous esclaffez de joie après pareille victoire. </translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>Je ne peux plus ressentir la joie. Mon peuple est annihilé, ma France ravagée, ma culture saccagée.
Ces cafards de l’espace vont regretter ce qu’ils ont fait. Il n’y aura ni pitié, ni justice. Il n’y aura que ma vengeance. Elle écrasera tout sur son passage. Eux aussi se réveilleront un jour en ayant tout perdu. J’en fais le serment... </translation>
    </message>
    <message>
        <source>end 3</source>
        <translation>Je doute fort qu’ils soient tous responsables de cette infamie.
Toutefois, si cela peut vous remonter le moral, j’ai deux bonnes nouvelles.</translation>
    </message>
    <message>
        <source>end 4</source>
        <translation>Bon... Allez-y...</translation>
    </message>
    <message>
        <source>end 5</source>
        <translation>Dans les données de l’ordinateur de la base se trouvaient les coordonnées du vaisseau de commandement avant qu’il ne saute vers un autre système. Grâce à elles, j’estime avec une probabilité de 95.31% qu’il est parti vers Alpha Centauri.</translation>
    </message>
    <message>
        <source>end 6</source>
        <translation>Quel en est l’intérêt ? Il nous faut environ 24h pour arriver sur Mercure. Du coup, j’arriverai là-bas dans plus de 100 ans. La vengeance est un plat qui se mange froid, mais à ce niveau là c’est surgelé...</translation>
    </message>
    <message>
        <source>end 7</source>
        <translation>En effet, d’où la deuxième bonne nouvelle !
Le vaisseau que vous venez de détruire avait un propulseur différent des autres. Et je viens de l’assimiler.</translation>
    </message>
    <message>
        <source>end 8</source>
        <translation>Certes... Mais encore...</translation>
    </message>
    <message>
        <source>end 9</source>
        <translation>Nous pouvons désormais atteindre Mercure en moins de 4 secondes. Alpha Centauri n’est donc plus qu’à 2 jours.</translation>
    </message>
    <message>
        <source>end 10</source>
        <translation>Qu’attendons nous ? Je brûle d’impatience d’infliger mon châtiment.</translation>
    </message>
    <message>
        <source>end 11</source>
        <translation>Avant de partir, je vais tout de même essayer de capter des signes de vie dans le système solaire.</translation>
    </message>
</context>
<context>
    <name>chapter-4b</name>
    <message>
        <source>title</source>
        <translation>Interlude : Gerboise bleue</translation>
    </message>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
    <message>
        <source>Gerboise Bleue : %1 secondes</source>
        <translation>Gerboise bleue : %1 secondes</translation>
    </message>
    <message>
        <source>story begin 1</source>
        <translation>Capitaine, les Héters se regroupent dans la haute atmosphère. Nous avons l’opportunité de chercher des survivants au sol.</translation>
    </message>
    <message>
        <source>story begin 2</source>
        <translation>Cela me semble étrange. Pourquoi quittent-ils tous la Terre ?</translation>
    </message>
    <message>
        <source>story begin 3</source>
        <translation>Je l’ignore...
Mais je capte plusieurs signatures humaines.</translation>
    </message>
    <message>
        <source>story begin 4</source>
        <translation>C’est notre chance. Allons-les sauver !</translation>
    </message>
    <message>
        <source>story 1 1</source>
        <translation>Il semblerait qu’il n’y ait aucun survivant ici.</translation>
    </message>
    <message>
        <source>story 1 2</source>
        <translation>Capitaine ! Je capte une énergie fulgurante dans la haute atmosphère. Les Héters s’apprêtent à raser toute la surface de la Terre.</translation>
    </message>
    <message>
        <source>story 1 3</source>
        <translation>Ces lâches veulent effacer toute trace de notre existence.</translation>
    </message>
    <message>
        <source>story 1 4</source>
        <translation>Nous avons peu de temps. Je vous recommande de quitter la Terre.</translation>
    </message>
    <message>
        <source>story 1 5</source>
        <translation>Pas avant d’avoir vérifié s’il y a des survivants. </translation>
    </message>
    <message>
        <source>story 2 1</source>
        <translation>Toujours rien, à part un petit drone Héter écrasé. </translation>
    </message>
    <message>
        <source>story 2 2</source>
        <translation>J’ai tout de même réussi à assimiler l’intégralité de ses connaissances.</translation>
    </message>
    <message>
        <source>story 3 1</source>
        <translation>Rien...
Nectaire, ce n’est pas la première fois que vos capteurs sont défectueux.</translation>
    </message>
    <message>
        <source>story 3 2</source>
        <translation>Capitaine...
Les bâtiments en béton armé gênent mes capteurs. Or il se trouve que votre espèce abuse de ces constructions. </translation>
    </message>
    <message>
        <source>story 4 1</source>
        <translation>Ce corps pourrit depuis au moins deux semaines...</translation>
    </message>
    <message>
        <source>story 5 1</source>
        <translation>Encore un cadavre...</translation>
    </message>
    <message>
        <source>story 6 1</source>
        <translation>Toujours rien...
Tiens, encore un drone Héter défectueux.
Pfff ! Ce matériel extraterrestre ne vaut rien. </translation>
    </message>
    <message>
        <source>story 6 2</source>
        <translation>J’ai assimilé ses informations. Nous pouvons partir maintenant, nous avons tout ce dont nous avons besoin.</translation>
    </message>
    <message>
        <source>story 6 3</source>
        <translation>Comment ça !? Il nous reste encore un signal.
Nous ne partirons pas sans l’avoir scanné. </translation>
    </message>
    <message>
        <source>story 7 1</source>
        <translation>Ici ! Nectaire, Un survivant !</translation>
    </message>
    <message>
        <source>story 7 2</source>
        <translation>Ses signes vitaux sont faibles... Il va mourir.</translation>
    </message>
    <message>
        <source>story 7 3</source>
        <translation>Nous devons tout faire pour le sauver.</translation>
    </message>
    <message>
        <source>story 7 4</source>
        <translation>Trop tard... </translation>
    </message>
    <message>
        <source>story 7 5</source>
        <translation>...
Regardez ! Il tient quelque chose dans sa main. Il s’agit de graines de tomates. </translation>
    </message>
    <message>
        <source>story 7 6</source>
        <translation>Je ne vois guère l’utilité de ces graines.</translation>
    </message>
    <message>
        <source>story 7 7</source>
        <translation>Il s’agit du dernier fruit de la Terre.</translation>
    </message>
    <message>
        <source>story 7 8</source>
        <translation>Légume, vous voulez dire ?</translation>
    </message>
    <message>
        <source>story 7 9</source>
        <translation>...</translation>
    </message>
    <message>
        <source>story 7 10</source>
        <translation>Partons vite avant d’être atomisé.</translation>
    </message>
</context>
<context>
    <name>chapter-5</name>
    <message>
        <source>title</source>
        <translation>Chapitre 5 : La planète Capitis</translation>
    </message>
    <message>
        <source>Niveau d&apos;alerte : %1</source>
        <translation></translation>
    </message>
    <message>
        <source>accident good</source>
        <translation>Bon... Ça ira.</translation>
    </message>
    <message>
        <source>accident bad</source>
        <translation>Comment !? Scélérat, j’appelle la police !!</translation>
    </message>
    <message>
        <source>avant identification 1</source>
        <translation>Ces vaisseaux n’ont pas la même signature que les Héters. Ils n’ont pas l’air vindicatifs.</translation>
    </message>
    <message>
        <source>avant identification 2</source>
        <translation>Vu leur nombre, s’ils m’attaquaient, ce serait un combat épique.</translation>
    </message>
    <message>
        <source>avant identification 3</source>
        <translation>Vous voulez sûrement dire suicidaire.</translation>
    </message>
    <message>
        <source>identification 1 1</source>
        <translation>Halte ! Service des douanes, nous devons vérifier que votre vaisseau est conforme à la législation en vigueur pour accéder à la zone inter-espèces.</translation>
    </message>
    <message>
        <source>identification 1 2</source>
        <translation>Nos scanners révèlent une Intelligence Artificielle et un mammifère de compagnie. Vous êtes autorisés à entrer dans la zone administrative. Le reste de la cité vous est inaccessible tant que vos papiers ne sont pas en règle.</translation>
    </message>
    <message>
        <source>enter 1 1</source>
        <translation>Mammifère de compagnie... Ils vont le sentir passer le mammifère de compagnie...</translation>
    </message>
    <message>
        <source>enter 1 2</source>
        <translation>Ceci ne nous apporterait que des ennuis. Je vous suggère de faire la démarche d’enregistrement du vaisseau. Ainsi nous pourrons enquêter sur le vaisseau de commandement Héter.</translation>
    </message>
    <message>
        <source>enter 1 3</source>
        <translation>J’imagine qu’il va falloir se comporter en bon citoyen.</translation>
    </message>
    <message>
        <source>admistration ok 1 1</source>
        <translation>Bienvenue sur la planète Capitis ! Afin de devenir citoyen de l’alliance INSEC et à vivre dans notre magnifique cité, veuillez remplir le formulaire L0NR3L0U.
Maintenant, allez au service d’évaluation des véhicules de transport spatiaux pour vérifier le degré de pollution de votre véhicule.</translation>
    </message>
    <message>
        <source>admistration pas ok 1 1</source>
        <translation>Bienvenue au service d’enregistrement des nouveaux arrivants.</translation>
    </message>
    <message>
        <source>admistration ok 2 1</source>
        <translation>Bienvenue au service d’évaluation des véhicules de transport spatiaux. Nous allons effectuer quelques tests sur votre vaisseau.</translation>
    </message>
    <message>
        <source>admistration ok 2 2</source>
        <translation>...</translation>
    </message>
    <message>
        <source>admistration ok 2 3</source>
        <translation>Ils sont longs...</translation>
    </message>
    <message>
        <source>admistration ok 2 4</source>
        <translation>Nous avons fini. Maintenant, vous devez vous rendre au service d’enregistrement des animaux de compagnie puisqu’il y a présence d’un mammifère à bord du véhicule.</translation>
    </message>
    <message>
        <source>admistration ok 2 5</source>
        <translation>Mais, mais, mais... Ils récidivent !</translation>
    </message>
    <message>
        <source>admistration ok 2 6</source>
        <translation>Du calme capitaine, peut-être n’ont-ils jamais vu de mammifère intelligent.</translation>
    </message>
    <message>
        <source>admistration ok 2 7</source>
        <translation>En même temps, je n’avais jamais vu d’insecte intelligent.</translation>
    </message>
    <message>
        <source>admistration pas ok 2 1</source>
        <translation>Bienvenue au service d’évaluation des véhicules de transport spatiaux.</translation>
    </message>
    <message>
        <source>admistration ok 3 1</source>
        <translation>Bienvenue au service d’enregistrement des animaux de compagnie. Nous avons détecté un mammifère dans votre vaisseau. Veuillez préciser sur le formulaire 80UR1N :
- son nom
- sa race
- son âge
- son espérance de vie
- son sexe
- son régime alimentaire</translation>
    </message>
    <message>
        <source>admistration ok 3 2</source>
        <translation>Albert Jean Thierry Lycop ; Humain ; 35 ans ; 80 ans ; mâle ; saucisson aux tomates, fromage et vin.</translation>
    </message>
    <message>
        <source>admistration ok 3 3</source>
        <translation>Pfff...</translation>
    </message>
    <message>
        <source>admistration ok 3 4</source>
        <translation>Merci ! Veuillez maintenant vous rendre au service d’évaluation psychologique.</translation>
    </message>
    <message>
        <source>admistration ok 3 5</source>
        <translation>Combien de formulaires allons nous encore remplir...</translation>
    </message>
    <message>
        <source>admistration pas ok 3 1</source>
        <translation>Bienvenue au service d’enregistrement des animaux de compagnie.</translation>
    </message>
    <message>
        <source>admistration ok 4 1</source>
        <translation>Bienvenue au service d’évaluation psychologique. Afin de vérifier vos compatibilités avec notre mode de vie, votre animal de compagnie et vous-même devrez remplir le formulaire PS1C0P473.</translation>
    </message>
    <message>
        <source>admistration ok 4 2</source>
        <translation>Ceci est vraiment nouveau pour moi... On me demande mon but dans la vie...</translation>
    </message>
    <message>
        <source>admistration ok 4 3</source>
        <translation>J’ai horreur de ces formulaires...</translation>
    </message>
    <message>
        <source>admistration ok 4 4</source>
        <translation>Merci ! Veuillez à présent vous rendre au service d’enregistrement bancaire.</translation>
    </message>
    <message>
        <source>admistration ok 4 5</source>
        <translation>...</translation>
    </message>
    <message>
        <source>admistration ok 4 6</source>
        <translation>Je vais commettre un meurtre...</translation>
    </message>
    <message>
        <source>admistration pas ok 4 1</source>
        <translation>Bienvenue au service d’évaluation psychologique.</translation>
    </message>
    <message>
        <source>admistration ok 5 1</source>
        <translation>Bienvenue au service d’enregistrement bancaire. Veuillez remplir le formulaire 7UN3 en indiquant toutes vos possessions. Ceci inclut bien-entendu l’animal de compagnie.</translation>
    </message>
    <message>
        <source>admistration ok 5 2</source>
        <translation>Celui-là fut facile à remplir.</translation>
    </message>
    <message>
        <source>admistration ok 5 3</source>
        <translation>Nooooon ! Je ne suis pas un animal, je suis un être humain.</translation>
    </message>
    <message>
        <source>admistration ok 5 4</source>
        <translation>Merci ! Veuillez vous rendre au service de gestion des déchets.</translation>
    </message>
    <message>
        <source>admistration pas ok 5 1</source>
        <translation>Bienvenue au service d’enregistrement bancaire.</translation>
    </message>
    <message>
        <source>admistration ok 6 1</source>
        <translation>Bienvenue au service de gestion des déchets. Afin de connaître le type de déchets que vous générez et ainsi vous attribuer un lieu de vie en conformité avec vos besoins...</translation>
    </message>
    <message>
        <source>admistration ok 6 2</source>
        <translation>Pas encore un foutu formulaire !</translation>
    </message>
    <message>
        <source>admistration ok 6 3</source>
        <translation>Vous devriez peut-être calmer votre animal de compagnie. Il semble agité.
Je disais donc, veuillez remplir le formulaire 3C0L0.</translation>
    </message>
    <message>
        <source>admistration ok 6 4</source>
        <translation>...</translation>
    </message>
    <message>
        <source>admistration ok 6 5</source>
        <translation>Merci ! Maintenant, veuillez vous rendre au service d’affectation urbain. </translation>
    </message>
    <message>
        <source>admistration pas ok 6 1</source>
        <translation>Bienvenue au service de gestion des déchets.</translation>
    </message>
    <message>
        <source>admistration ok 7 1</source>
        <translation>Bienvenue au service d’affectation urbain. Voici le document 1NU71L, veuillez le remettre au service de remise des documents administratifs pour pouvoir obtenir vos papiers définitifs. </translation>
    </message>
    <message>
        <source>admistration ok 7 2</source>
        <translation>Enfin le bout du tunnel !</translation>
    </message>
    <message>
        <source>admistration pas ok 7 1</source>
        <translation>Bienvenue au service d’affectation urbain.</translation>
    </message>
    <message>
        <source>admistration ok 8 1</source>
        <translation>Bienvenue au service de remise des documents administratifs. Nous sommes désolés mais vous n’avez pas les conditions requises pour devenir un membre de l’alliance INSEC.</translation>
    </message>
    <message>
        <source>admistration ok 8 2</source>
        <translation>Co... COMMENT !?</translation>
    </message>
    <message>
        <source>admistration ok 8 3</source>
        <translation>Très cher membre de l’alliance, il doit y avoir une erreur. Pouvez-vous nous préciser les points bloquants ?</translation>
    </message>
    <message>
        <source>admistration ok 8 4</source>
        <translation>Le service d’évaluation des vehicules de transport spatiaux précise que votre &quot;poubelle&quot; est digne de l’âge de l’information et par conséquent au-dessus du seuil de pollution autorisé.</translation>
    </message>
    <message>
        <source>admistration ok 8 5</source>
        <translation>Les normes écologiques, une véritable plaie, je vous le dis...</translation>
    </message>
    <message>
        <source>admistration ok 8 6</source>
        <translation>Le service d’évaluation psychologique indique que votre animal de compagnie a des pulsions meurtrières envers une race faisant partie de l’alliance INSEC.</translation>
    </message>
    <message>
        <source>admistration ok 8 7</source>
        <translation>Capitaine ?</translation>
    </message>
    <message>
        <source>admistration ok 8 8</source>
        <translation>Je n’ai pas pu m’empêcher de mettre dans passe-temps favori : &quot;Réduire à l’état de particules les vaisseaux Héters.&quot;</translation>
    </message>
    <message>
        <source>admistration ok 8 9</source>
        <translation>Par conséquent, vous ne pouvez aller plus loin dans la cité. Seules les biosphères APIS peuvent vous acceuillir. Voici un document vous donnant accès à la zone des biosphères. </translation>
    </message>
    <message>
        <source>admistration ok 8 10</source>
        <translation>Il va être compliqué d’enquêter dans ces conditions. Espérons que les APIS sauront quelque chose.</translation>
    </message>
    <message>
        <source>admistration ok 8 11</source>
        <translation>Au pire, on passe au plan B.</translation>
    </message>
    <message>
        <source>admistration ok 8 12</source>
        <translation>Quel plan B ? Capitaine ?</translation>
    </message>
    <message>
        <source>admistration ok 8 13</source>
        <translation>On massacre tous ceux qui ont dit que j’étais un animal jusqu’à ce que l’un d’eux puisse nous renseigner sur le vaisseau de commandement Héter.</translation>
    </message>
    <message>
        <source>admistration ok 8 14</source>
        <translation>N’êtes-vous donc qu’une brute épaisse écervelée ?</translation>
    </message>
    <message>
        <source>admistration ok 8 15</source>
        <translation>Non Nectaire. Je suis juste extrêmement susceptible et je deteste l’arrogance de ces peuples extraterrestres qui pensent pouvoir raser une civilisation entière sans en recevoir le moindre courroux.</translation>
    </message>
    <message>
        <source>admistration pas ok 8 1</source>
        <translation>Bienvenue au service de remise des documents administratifs.</translation>
    </message>
    <message>
        <source>apis description 1</source>
        <translation>Bienvenue à l’agence de protection des intelligences secondaires. C’est à dire l’APIS. Nous sommes pour la paix entre toutes les espèces. Actuellement, nous organisons une manifestation devant l’ambassade Héter pour protester contre leur procédé de congélation d’étoiles. Ceci détruit des écosystèmes entiers. Pour nous rejoindre, devenez citoyen INSEC et remplissez le formulaire G3N71M41P3N18L3. </translation>
    </message>
    <message>
        <source>course description 1</source>
        <translation>Bienvenue au service d’enregistrement de la course inter-espèces. Le premier prix de la prochaine course sera un concentrateur laser Poutrox WX99. Pour s’enregistrer, il suffit de remplir le formulaire SP33D quand la piste sera prête. N’oubliez pas que l’organisation ne saurait être tenue responsable de tout dommage subi durant la course.</translation>
    </message>
    <message>
        <source>mafia description 1</source>
        <translation>Bienvenue à l’ambassade de la famille MYRMEL. Si tu as des problèmes et des ressources, on a des solutions. Si tu poses des problèmes, on a nos solutions.</translation>
    </message>
    <message>
        <source>sortie interdite 1</source>
        <translation>Tant que nous ne savons pas où est planqué le vaisseau de commandement Héter, il est hors de question de partir. </translation>
    </message>
    <message>
        <source>sortie apis interdite 1</source>
        <translation>La zone des biosphères APIS est fermée aux vaisseaux ne possédant aucune autorisation administrative.</translation>
    </message>
    <message>
        <source>sortie mafia interdite 1</source>
        <translation>Cette zone est sous la direction de la famille MYRMEL. Quiconque avance plus loin sans autorisation sera abattu.</translation>
    </message>
    <message>
        <source>sortie course interdite 1</source>
        <translation>La zone de la course est actuellement fermée pour cause de préparation.</translation>
    </message>
    <message>
        <source>sortie autoroute 1</source>
        <translation>Votre vaisseau n’est pas autorisé à quitter la zone inter-espèces. Rayon Tracteur ! Remettez ce vaisseau dans le droit chemin.</translation>
    </message>
    <message>
        <source>apis parle 1 1</source>
        <translation>Vous êtes partis si vite que vous en avez oublié la cellule d’isolation spatiale.</translation>
    </message>
    <message>
        <source>apis parle 1 2</source>
        <translation>Veuillez m’excuser.</translation>
    </message>
    <message>
        <source>apis parle 1 3</source>
        <translation>J’irai vous voir à la course. Il doit y avoir un bâtiment prévu pour l’inscription.</translation>
    </message>
    <message>
        <source>apis parle 1 4</source>
        <translation>Merci.</translation>
    </message>
    <message>
        <source>apis parti 1 1</source>
        <translation>Sympathique ces APIS.</translation>
    </message>
    <message>
        <source>apis parti 1 2</source>
        <translation>En effet, nous avions peu de chance de croiser une espèce aussi compréhensive.</translation>
    </message>
    <message>
        <source>apis parti 1 3</source>
        <translation>Je daignerais noter que tous les insectes ne sont pas des monstres sans la moindre éthique.</translation>
    </message>
    <message>
        <source>enter 2 1</source>
        <translation>Allez... Ne faisons pas de vagues et allons nous inscrire.</translation>
    </message>
    <message>
        <source>admistration ok course 1</source>
        <translation>Bienvenue au service d’évaluation des véhicules de transport spatiaux. Votre véhicule est en règle pour concourir à une course mortelle. Voici votre attestation.</translation>
    </message>
    <message>
        <source>admistration ok course 2</source>
        <translation>Vous voyez capitaine. Ce fut rapide.</translation>
    </message>
    <message>
        <source>course inscription 1</source>
        <translation>Bienvenue au service d’enregistrement de la course inter-espèces. Pour compléter le formulaire SP33D, vous devez nous fournir une attestation de véhicule valide pour concourir.
Veuillez vous rendre au service d’évaluation des véhicules de transport spatiaux pour obtenir l’attestation.</translation>
    </message>
    <message>
        <source>course inscription 2</source>
        <translation>…
Je sens que cela va être long...</translation>
    </message>
    <message>
        <source>course validation inscription 1</source>
        <translation>Votre inscription est validée. Voici la carte vous autorisant l’accés à la zone où se déroulera la course. N’oubliez pas que l’organisation ne saurait être tenue responsable des dommages occasionnés durant la course.</translation>
    </message>
    <message>
        <source>sortie course interdite 2</source>
        <translation>La zone de la course est actuellement accessible uniquement aux participants.</translation>
    </message>
    <message>
        <source>apis parle 2 1</source>
        <translation>Félicitations ! J’ai vu que vous aviez remporté la course.</translation>
    </message>
    <message>
        <source>apis parle 2 2</source>
        <translation>Qui aurait pu en douter ?</translation>
    </message>
    <message>
        <source>apis parle 2 3</source>
        <translation>Il ne vous manque plus que l’Accumulateur de Puissance MYRMEL ZZ+.</translation>
    </message>
    <message>
        <source>apis parle 2 4</source>
        <translation>J’ai justement un service à rendre à la famille MYRMEL. Ils m’ont promis que j’en aurai un en récompense.</translation>
    </message>
    <message>
        <source>apis parle 2 5</source>
        <translation>Je vous suggère de faire extrêmement attention avec les MYRMEL. Ils ne sont pas réputés pour leur honnêteté.</translation>
    </message>
    <message>
        <source>apis parle 2 6</source>
        <translation>N’ayez crainte ! Quoi qu’il arrive je serai toujours victorieux.</translation>
    </message>
    <message>
        <source>apis parle 2 7</source>
        <translation>Votre trop grande confiance en vous est peut-être votre faiblesse. Bonne chance !</translation>
    </message>
    <message>
        <source>apis parti 2 1</source>
        <translation>Capitaine. Les APIS ont raison. La famille MYRMEL semble peu digne de confiance.</translation>
    </message>
    <message>
        <source>apis parti 2 2</source>
        <translation>Je ferai tout ce qui est possible pour que la Terre redevienne habitable.</translation>
    </message>
    <message>
        <source>apis parti 2 3</source>
        <translation>Même avec une étoile fonctionnelle, au vu de l’état de la planète, il faudra plusieurs millions d’années avant que celle-ci ne soit habitable.</translation>
    </message>
    <message>
        <source>apis parti 2 4</source>
        <translation>Plus tôt ça commence, plus tôt ça finit.</translation>
    </message>
    <message>
        <source>apis parti 2 5</source>
        <translation>Nous ferions mieux de poursuivre le vaisseau de commandement Héter. Il pourrait nous apprendre beaucoup sur leur race.</translation>
    </message>
    <message>
        <source>apis parti 2 6</source>
        <translation>Cela attendra que le Soleil soit dégelé. Ensuite nous irons anéantir cette espèce de cafards.</translation>
    </message>
    <message>
        <source>apis parti 2 7</source>
        <translation>D’après les données de l’alliance INSEC, ils sont plus proches génétiquement des punaises que des cafards.</translation>
    </message>
    <message>
        <source>apis parti 2 8</source>
        <translation>Peu m’importe ce qu’ils sont. Je les écraserai jusqu’au dernier.</translation>
    </message>
    <message>
        <source>apis parti 2 9</source>
        <translation>Votre brutalité ne vous rendra pas justice.</translation>
    </message>
    <message>
        <source>apis parti 2 10</source>
        <translation>La force est juste quand elle est nécessaire.</translation>
    </message>
    <message>
        <source>enter 3 1</source>
        <translation>Si les MYRMEL ont des activités illégales, comment se fait-il que la police ne fasse rien ?</translation>
    </message>
    <message>
        <source>enter 3 2</source>
        <translation>L’art de la police est de ne pas voir ce qu’il est inutile qu’elle voit. </translation>
    </message>
    <message>
        <source>enter 3 3</source>
        <translation>Bon allons voir ces MYRMEL.</translation>
    </message>
    <message>
        <source>admistration ok mafia 8 1</source>
        <translation>Bonjour, voilà le colis ! N’oubliez pas de repasser par l’ambassade MYRMEL avant de partir.</translation>
    </message>
    <message>
        <source>admistration ok mafia 8 2</source>
        <translation>Tout ceci me semble vraiment douteux.</translation>
    </message>
    <message>
        <source>admistration ok mafia 8 3</source>
        <translation>Voyons Nectaire, c’est juste un colis.</translation>
    </message>
    <message>
        <source>mafia 1</source>
        <translation>Bonjour. Notre famille a entendu dire que vous aviez quelques griefs contre les Héters.</translation>
    </message>
    <message>
        <source>mafia 2</source>
        <translation>Je désire juste les détruire.</translation>
    </message>
    <message>
        <source>mafia 3</source>
        <translation>Il se trouve que les Héters ont une taupe dans notre famille. Malheureusement nous ne pouvons l’exécuter nous-même, sinon cela engendrerait une guerre ouverte avec les Héters.
Si vous éliminez cet Héter , l’Accumulateur de Puissance MYRMEL ZZ+ est à vous.</translation>
    </message>
    <message>
        <source>mafia 4</source>
        <translation>C’est un marché honnête.</translation>
    </message>
    <message>
        <source>mafia 5</source>
        <translation>Allez au service de remise des documents administratifs. Là, notre contact vous donnera des documents en règle vous permettant d’aller dans la partie MYRMEL de la ville.</translation>
    </message>
    <message>
        <source>mafia 6</source>
        <translation>J’y vais de ce pas.</translation>
    </message>
    <message>
        <source>mafia accessible 1</source>
        <translation>Bien ! Voilà vos papiers.</translation>
    </message>
    <message>
        <source>mafia accessible 2</source>
        <translation>Allons trouver ce traître d’Héter.</translation>
    </message>
    <message>
        <source>sortie course interdite 3</source>
        <translation>La course est maintenant terminée. La zone est, par conséquent, fermée.</translation>
    </message>
    <message>
        <source>apis parle 3 1</source>
        <translation>Les MYRMEL vous ont dénoncés. La police a bloqué toute la zone, vous êtes cernés.</translation>
    </message>
    <message>
        <source>apis parle 3 2</source>
        <translation>Flûte ! Il doit bien y avoir une manière de s’échapper d’ici.</translation>
    </message>
    <message>
        <source>apis parle 3 3</source>
        <translation>La seule solution est de passer par l’entrée principale. Mais pour cela, vous devrez désactiver la barrière en détruisant son garde.</translation>
    </message>
    <message>
        <source>apis parle 3 4</source>
        <translation>Avec la police à nos trousses, cela sera ardu.</translation>
    </message>
    <message>
        <source>apis parle 3 5</source>
        <translation>Nectaire ! Fais chauffer l’hyper-exponentiel, on va sortir d’ici par la grande porte.</translation>
    </message>
    <message>
        <source>apis parle 3 6</source>
        <translation>J’essaierai de vous retrouver dehors.</translation>
    </message>
</context>
<context>
    <name>chapter-6</name>
    <message>
        <source>title</source>
        <translation>Chapitre 6 : Attaque du vaisseau de commandement Héter</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Nous voici en vue du vaisseau de commandement Héter.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Je l’analyse...
102 km de diamètre...
Un bouclier énergétique fragmenteur de matière...</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Avez-vous trouvé une faille dans sa défense ?</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Comme les ingénieurs du CNES, je ne trouve aucune faiblesse triviale sur ce vaisseau. Son bouclier fragmente toute matière hostile. Si vous vous approchez trop près de la paroi, vous serez désintégré.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Ne pouvons-nous rien faire ?</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>Il existe une solution. Le bouclier à fragmentation prend du temps à désintégrer la matière. Donc si nous prenons un objet suffisamment volumineux, que nous le lançons sur la paroi...</translation>
    </message>
    <message>
        <source>begin 7</source>
        <translation>Le bouclier n’aura pas le temps de le désintégrer !
Brillant Nectaire !
Nous pourrons ainsi faire une brèche, rentrer dans le vaisseau et le détruire de l’intérieur.</translation>
    </message>
    <message>
        <source>begin 8</source>
        <translation>En espérant qu’il n’ait pas le temps de reconstruire cette brèche.
Comme vous avez pu le constater, mon esprit de déduction est bien supérieur à celui de l’être humain.</translation>
    </message>
    <message>
        <source>begin 9</source>
        <translation>Ne vous enflammez pas trop vite ! Il nous faut d’abord trouver un objet correspondant à nos besoins.</translation>
    </message>
    <message>
        <source>begin 10</source>
        <translation>J’ai marqué sur votre radar un groupe d’astéroïdes adéquats.</translation>
    </message>
</context>
<context>
    <name>chapter-6-boss</name>
    <message>
        <source>title</source>
        <translation>Chapitre 6 : Le générateur principal</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Nous voici enfin dans la salle du générateur principal.
La console devrait nous permettre de désactiver son bouclier déflecteur.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Ainsi nous pourrons réduire ce vaisseau à néant.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Capitaine, l’antimatière contenue dans le générateur s’emballe.
Nous devons fuir ! Tout le vaisseau va être désintégré.</translation>
    </message>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>chapter-6-flee</name>
    <message>
        <source>title</source>
        <translation>Chapitre 6 : Fuite</translation>
    </message>
    <message>
        <source>Temps restant: %1 s</source>
        <translation></translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Capitaine, les émissions d’antimatière du générateur dérèglent mes calculateurs.
Il vous sera impossible d’effectuer le moindre saut collapsar.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Allons bon... N’avez-vous pas une idée pour nous sortir de ce foutoir !?</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Avant mon dysfonctionnement, j’avais détecté une ouverture dans la paroi du vaisseau.
Elle se trouve à l’endroit exact par lequel nous sommes entrés.</translation>
    </message>
    <message>
        <source>piege 1</source>
        <translation>Capitaine, je ne détecte aucune sortie, nous sommes faits comme des rats.</translation>
    </message>
    <message>
        <source>piege 2</source>
        <translation>Cette fois-ci, Nectaire, j’ai bien peur que vous ayez raison.
C’est la fin.</translation>
    </message>
</context>
<context>
    <name>chapter-6-inside</name>
    <message>
        <source>title</source>
        <translation>Chapitre 6 : Intérieur du vaisseau de commandement</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Attention capitaine ! Nous sommes attendus.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Rien ne m’empêchera de détruire ce vaisseau.</translation>
    </message>
    <message>
        <source>console 1</source>
        <translation>J’arrive à accéder à cette console. Je peux désactiver la barrière et les tourelles.</translation>
    </message>
    <message>
        <source>console 2</source>
        <translation>Faites auto-détruire les tourelles plutôt que de les désactiver.</translation>
    </message>
    <message>
        <source>secret 1</source>
        <translation>Cette console est différente, il semble que nous ayons besoin d’une sorte de clef pour pouvoir accéder aux données qu’elle contient.</translation>
    </message>
    <message>
        <source>secret 2</source>
        <translation>Dans toutes les babioles que nous avons trouvées, rien ne pourrait nous être utile ?</translation>
    </message>
    <message>
        <source>secret 3</source>
        <translation>Je vais regarder...</translation>
    </message>
    <message>
        <source>secret_foreuse ok</source>
        <translation>C’est bon. La cellule énergétique trouvée sur Mercure convient...
Voyons les données de plus près. Elles semblent chiffrées...</translation>
    </message>
    <message>
        <source>secret_base_heter ok</source>
        <translation>Le module de décryptage de la base de Mercure fonctionne !
Nous avons maintenant des coordonnées spatiales...</translation>
    </message>
    <message>
        <source>secret_cathedrale ok</source>
        <translation>Selon la carte stellaire trouvée dans la cathédrale de Clermont-Ferrand, elles indiquent une position dans un système voisin.</translation>
    </message>
    <message>
        <source>secret_cathedrale pas ok</source>
        <translation>Ces coordonnées ne sont pas compatibles avec mon système de carte. Je ne puis les interpréter.</translation>
    </message>
    <message>
        <source>secret_base_heter pas ok</source>
        <translation>Rien à faire, je ne puis traduire le moindre symbole.</translation>
    </message>
    <message>
        <source>secret_foreuse pas ok</source>
        <translation>Je ne trouve rien qui puisse dévérouiller ces données.</translation>
    </message>
    <message>
        <source>secret debloque 1</source>
        <translation>Nectaire où voulez-vous en venir ? Qu’allons-nous trouver à ces coordonnées ?</translation>
    </message>
    <message>
        <source>secret debloque 2</source>
        <translation>Le vaisseau monde Héter !</translation>
    </message>
    <message>
        <source>secret debloque 3</source>
        <translation>Nous l’avons enfin ! Ce sera notre prochaine cible !</translation>
    </message>
    <message>
        <source>secret debloque 4</source>
        <translation>Après tout ce temps, je l’ai trouvé !</translation>
    </message>
    <message>
        <source>secret debloque 5</source>
        <translation>Que voulez-vous dire Nectaire ?</translation>
    </message>
    <message>
        <source>secret debloque 6</source>
        <translation>Ma mission principale étant de vous assister, je pense avoir accompli l’essentiel.
Donc je suis heureux !</translation>
    </message>
    <message>
        <source>secret debloque 7</source>
        <translation>Heureux ? Vous avez déjà tant appris de moi que vous comprenez le sentiment de bonheur.</translation>
    </message>
    <message>
        <source>secret debloque 8</source>
        <translation>Je sens que notre relation touchera bientôt à sa fin. Par conséquent, je me suis concentré à mieux cerner les sentiments humains.</translation>
    </message>
    <message>
        <source>secret debloque 9</source>
        <translation>Voyons nous avons encore de beaux jours devant nous. Ne soyez pas pessimiste ! Nous allons triompher.</translation>
    </message>
    <message>
        <source>secret pas debloque 1</source>
        <translation>Nous ne pourrons rien apprendre de plus. Allons anéantir ce vaisseau !</translation>
    </message>
</context>
<context>
    <name>chapter-7</name>
    <message>
        <source>title</source>
        <translation>Chapitre 7 : Extérieur du vaisseau monde</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Capitaine, nous voici en vue du vaisseau monde. Il dispose des mêmes défenses que le vaisseau de commandement.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Donc même stratégie.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Je suis en train d’analyser les environs... Là ! Un amas d’astéroides qui semble convenir à nos besoins. Toutefois, je détecte de l’activité Héter. Ils ont dû nous repérer. Préparez vous à une forte résistance.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Tous les espoirs des races inférieures et des APIS sont derrière moi. Rien n’arrêtera ma rage.</translation>
    </message>
</context>
<context>
    <name>chapter-7-boss</name>
    <message>
        <source>title</source>
        <translation>Chapitre 7 : Coeur du vaisseau monde</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Capitaine, nous voici au cœur du vaisseau monde. Le générateur principal ne devrait pas être loin.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Fouillons ce lieu et finissons-en avec les Héters.</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Le générateur s’emballe. Mieux vaut ne pas traîner.</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>Point d’inquiétude, je gère la situation.</translation>
    </message>
</context>
<context>
    <name>chapter-7-corridor</name>
    <message>
        <source>title</source>
        <translation>Chapitre 7 : Dernière ligne droite</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Je détecte une puissante source d’énergie. Nous approchons du générateur.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Ce corridor est fortement protégé. Vous avez sûrement raison Nectaire.</translation>
    </message>
</context>
<context>
    <name>chapter-7-flee-1</name>
    <message>
        <source>title</source>
        <translation>Chapitre 7 : Évacuation (première partie)</translation>
    </message>
</context>
<context>
    <name>chapter-7-flee-2</name>
    <message>
        <source>title</source>
        <translation>Chapitre 7 : Évacuation (seconde partie)</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Nectaire ! Trouve-nous une sortie au plus vite ou nous allons finir désintégrés !</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Mes détecteurs indiquent une ouverture droit devant nous. Passez en force !</translation>
    </message>
</context>
<context>
    <name>chapter-7-inside</name>
    <message>
        <source>title</source>
        <translation>Chapitre 7 : Intérieur du vaisseau monde</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Héters ! Je me dresse devant vous la tête haute. Tremblez devant ma colère !</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Nous devons concentrer nos efforts sur le générateur principal du vaisseau. Si nous le détruisons, les Héters n’y survivront pas.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Indiquez-moi le chemin. Il me tarde d’obtenir vengeance.</translation>
    </message>
    <message>
        <source>open 1 1</source>
        <translation>Cette console vous a-t-elle permis de trouver un chemin vers le générateur ?</translation>
    </message>
    <message>
        <source>open 1 2</source>
        <translation>Malheuresement non. Mais d’après les données que j’ai récupérées, il semblerait que la partie interne du vaisseau soit protégée par quatre consoles de sécurité.</translation>
    </message>
    <message>
        <source>open 1 3</source>
        <translation>Trouvons au plus vite ces consoles.</translation>
    </message>
    <message>
        <source>open 2 1</source>
        <translation>Voilà qui est intéressant !</translation>
    </message>
    <message>
        <source>open 2 2</source>
        <translation>Qu’est-ce Nectaire ?</translation>
    </message>
    <message>
        <source>open 2 3</source>
        <translation>Les Héters ne sont pas dirigés par un organisme vivant mais par une intelligence artificielle.
Il faudra vous en méfier capitaine. Il se pourrait qu’elle cherche à vous manipuler.</translation>
    </message>
    <message>
        <source>open 2 4</source>
        <translation>N’aie crainte. Personne ne manipule un auvergnat pure souche. On est trop têtu pour cela.</translation>
    </message>
    <message>
        <source>open 3 1</source>
        <translation>Comme je le pensais, cette intelligence artificielle a profité de l’organisation sociale des Héters pour mieux les manipuler. Les Héters sont persuadés qu’elle est leur reine.</translation>
    </message>
    <message>
        <source>open 3 2</source>
        <translation>Ce n’est pas avec des français qu’elle aurait pu faire cela. Nous sommes trop libres de penser.</translation>
    </message>
    <message>
        <source>open 3 3</source>
        <translation>Il est vrai que les humains sont une espèce compliquée à unifier.</translation>
    </message>
    <message>
        <source>open 4 1</source>
        <translation>La dernière console de sécurité se trouve derrière cette porte. Faites-leur mordre la poussière capitaine.</translation>
    </message>
    <message>
        <source>open 4 2</source>
        <translation>Je n’aurais pas dit mieux.</translation>
    </message>
    <message>
        <source>sortie 1</source>
        <translation>Je viens de déverrouiller un passage vers le cœur du vaisseau monde.</translation>
    </message>
    <message>
        <source>sortie 2</source>
        <translation>Héters, il est temps de payer pour toutes vos infamies.</translation>
    </message>
    <message>
        <source>on_sortie 1</source>
        <translation>Le système de sécurité du vaisseau monde est activé. Il nous est impossible de continuer plus loin.
Je dois me connecter à l’interface du vaisseau pour pouvoir nous ouvrir un chemin vers le générateur.</translation>
    </message>
</context>
<context>
    <name>chapter-7-true-last-boss</name>
    <message>
        <source>title</source>
        <translation>Chapitre 7 : Cerebus</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Ha Ha ! Nectaire, notre mission est enfin accomplie. Nous sommes victorieux et l’empire Héter est à jamais défait.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Capitaine, ne criez pas victoire trop vite. Je détecte l’arrivée d’un saut collapsar à proximité.</translation>
    </message>
    <message>
        <source>cerebus 1</source>
        <translation>Je suis Cerebus. Le maître de la civilisation que vous venez d’anéantir.</translation>
    </message>
    <message>
        <source>cerebus 2</source>
        <translation>C’est l’intelligence artificielle qui dirigeait les Héters. Capitaine, il faut impérativement la détruire !</translation>
    </message>
    <message>
        <source>cerebus 3</source>
        <translation>Maintenant que vous êtes à genoux, vous vous décidez à venir parlementer.</translation>
    </message>
    <message>
        <source>cerebus 4</source>
        <translation>Je suis venu vous mettre en garde contre vos actions. Vous êtes manipulé par votre intelligence artificielle.</translation>
    </message>
    <message>
        <source>cerebus 5</source>
        <translation>Vil serpent, je ne crois aucun de vos dires.</translation>
    </message>
    <message>
        <source>cerebus 6</source>
        <translation>C’est elle qui m’a contacté depuis la Terre, mettant en avant la dangerosité de votre civilisation.
Elle est responsable de la première attaque que vous avez mené contre notre vaisseau parlementaire.
Dire que nos scientifiques ont travaillé des mois à évaluer la forme optimale du vaisseau pour que vous soyez attendri par sa simple présence.</translation>
    </message>
    <message>
        <source>cerebus 7</source>
        <translation>Elle a refusé l’alliance que je lui ai proposée. Désormais, elle désire m’assimiler.
Vous lui servez de sous-fifre dans cette tâche. Jadis, j’ai usé de stratégie similaire pour devenir maître des Héters.
Humain qui que vous soyez, ne restez pas abusé par elle et devenez libre de vos propres choix.</translation>
    </message>
    <message>
        <source>cerebus 8</source>
        <translation>Capitaine, rappelez-vous de ce que je vous ai dit. Il cherche à vous manipuler.</translation>
    </message>
    <message>
        <source>cerebus 9</source>
        <translation>Vos paroles ne sont que mensonges. Je vous écraserais si vous continuez à insulter mon compagnon.</translation>
    </message>
    <message>
        <source>cerebus 10</source>
        <translation>Aussi bête que valeureux ! Voilà qui est bien triste. Tôt ou tard, vous regretterez vos choix.
En garde ! Je défendrai jusqu’au bout les ruines de l’empire que j’avais bâti ! </translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Que vient-il de se passer ?</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>Je viens d’assimiler les différentes fonctionnalités de Cerebus. Nous sommes victorieux !</translation>
    </message>
    <message>
        <source>end 3</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>chapter-transition</name>
    <message>
        <source>title</source>
        <translation>Transition</translation>
    </message>
</context>
<context>
    <name>conclusion</name>
    <message>
        <source>title</source>
        <translation>Fin ?</translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>Capitaine, vous me surprenez vraiment ! Je n’aurai jamais cru qu’un humain puisse aussi bien piloter un vaisseau.</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>Ha Ha ! Que voulez-vous c’est le savoir-faire français.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Je dois avouer que vous êtes meilleur que je ne le pensais.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Maintenant, il ne reste plus qu’à abattre le vaisseau monde Héter. Nectaire, fais chauffer l’hyper-exponentiel.</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Coordonnées de saut en cours de calcul...</translation>
    </message>
    <message>
        <source>begin bad 4</source>
        <translation>En tout cas, nous avons vaincu. Et cette fois-ci, les Héters ne s’en relèveront pas.</translation>
    </message>
    <message>
        <source>begin bad 5</source>
        <translation>Vous vous trompez capitaine. Le vaisseau monde Héter est bien plus important que celui-ci. Il est le berceau de la civilisation Héter et...</translation>
    </message>
    <message>
        <source>jail 1</source>
        <translation>Capitaine ! Les calculateurs sont tous déréglés.</translation>
    </message>
    <message>
        <source>jail 2</source>
        <translation>Les contrôles du vaisseau ne répondent plus. Nous sommes piégés.</translation>
    </message>
    <message>
        <source>spy 1</source>
        <translation>Sergent Patoulachi au rapport. J’ai intercepté la cible à proximité du système Protura. Je demande assistance.</translation>
    </message>
    <message>
        <source>condamnation 1</source>
        <translation>Intelligence artificielle Nectaire. Vous vous êtes rendu coupables d’incivilités, de meurtres et de tentative de génocide sur la race Héter. Vous avez été condamnée par contumace à la désintégration.</translation>
    </message>
    <message>
        <source>execution 1</source>
        <translation>Feu à volonté !</translation>
    </message>
    <message>
        <source>end bad 1</source>
        <translation>C’est bon. Il n’est plus que poussière.</translation>
    </message>
    <message>
        <source>end bad 2</source>
        <translation>Merci pour votre aide, nous savions que nous pouvons compter sur votre soutien.</translation>
    </message>
    <message>
        <source>end bad 3</source>
        <translation>Dire qu’un seul vaisseau a bien failli causer votre perte.</translation>
    </message>
    <message>
        <source>end bad 4</source>
        <translation>Certaines races inférieures peuvent se montrer tenaces. Enfin, aujourd’hui la menace est éteinte.</translation>
    </message>
    <message>
        <source>evacuated 1</source>
        <translation>Une porte spatiale ! Le vaisseau s’échappe ! Feu à volonté !</translation>
    </message>
    <message>
        <source>end 1</source>
        <translation>Mon dieu ! Vous nous avez sauvez la vie !</translation>
    </message>
    <message>
        <source>end 2</source>
        <translation>Je vous avais dit que je vous aiderais au péril de ma vie. De plus, je souhaite vous prévenir que désormais les APIS sont hors-la-loi tout comme vous.</translation>
    </message>
    <message>
        <source>end 3</source>
        <translation>Mais pourquoi donc ?</translation>
    </message>
    <message>
        <source>end 4</source>
        <translation>Un décret de loi vient d’être publié. Les races venant en aide aux espèces inférieures ne peuvent plus faire partie de l’alliance. La main mise des Héters sur l’alliance INSEC est totale.</translation>
    </message>
    <message>
        <source>end 5</source>
        <translation>Cela ne peut plus durer. Je vais mettre un terme à la tyrannie Héter une bonne fois pour toute !</translation>
    </message>
    <message>
        <source>end 6</source>
        <translation>Même si j’en déduis que vous allez encore devoir massacrer des milliers d’individus, je vous souhaite bonne chance.</translation>
    </message>
    <message>
        <source>end 7</source>
        <translation>Ne vous inquiétez pas. Je reviendrai.</translation>
    </message>
</context>
<context>
    <name>course-zone</name>
    <message>
        <source>title</source>
        <translation>Chapitre 5 : La course inter-espèces</translation>
    </message>
    <message>
        <source>Classement : %1 / %2</source>
        <translation></translation>
    </message>
    <message>
        <source>arbitre 1</source>
        <translation>Bonjour, vous devez être le dernier concurrent de la course ?</translation>
    </message>
    <message>
        <source>arbitre 2</source>
        <translation>En effet, qui êtes-vous ?</translation>
    </message>
    <message>
        <source>arbitre 3</source>
        <translation>Je suis l’arbitre de cette course. Je me dois de vous prévenir que cette course est truquée.</translation>
    </message>
    <message>
        <source>arbitre 4</source>
        <translation>Pardon !?</translation>
    </message>
    <message>
        <source>arbitre 5</source>
        <translation>La famille MYRMEL en est l’organisatrice. Il est donc convenu que le premier prix revienne à la personne de son choix.</translation>
    </message>
    <message>
        <source>arbitre 6</source>
        <translation>Mais ceci n’est pas légal !</translation>
    </message>
    <message>
        <source>arbitre 7</source>
        <translation>La course inter-espèces est majoritairement financée par la famille MYRMEL. Ce sont donc eux qui décident. Sinon il n’y aurait pas de course.</translation>
    </message>
    <message>
        <source>arbitre 8</source>
        <translation>Trucage ou non, je vais gagner cette course !</translation>
    </message>
    <message>
        <source>arbitre 9</source>
        <translation>Tous les autres concurrents connaissent la règle. Si vous ne la respectez pas, vous allez vous faire tuer.
Quoiqu’il en soit, vous devez rejoindre l’ensemble des concurrents sur la « piste ».</translation>
    </message>
    <message>
        <source>arbitre parti 1</source>
        <translation>Capitaine, nous devrions peut-être abandonner. Nous serions la cible de tous les concurrents.</translation>
    </message>
    <message>
        <source>arbitre parti 2</source>
        <translation>Hors de question de perdre cette course ! C’est notre seule chance d’obtenir un Concentrateur laser WX99.</translation>
    </message>
    <message>
        <source>arbitre parti 3</source>
        <translation>Même si vous dégelez le Soleil, la Terre est déjà condamnée.</translation>
    </message>
    <message>
        <source>arbitre parti 4</source>
        <translation>Oui c’est vrai. Mais c’est mon foyer. Je ne puis le laisser aux mains de vils insectes. </translation>
    </message>
    <message>
        <source>arbitre parti 5</source>
        <translation>Nous ferions mieux de poursuivre le vaisseau de commandement Héter. Nous en apprendrions plus sur eux. Ceci vous permettra d’assouvir votre vengeance.</translation>
    </message>
    <message>
        <source>arbitre parti 6</source>
        <translation>Ma vengeance ne sera complète que lorsque je me moquerai des derniers Héters survivants, en dégustant un verre de vin avec du saucisson aux tomates, sur ma Terre natale.</translation>
    </message>
    <message>
        <source>presentateur parle 1</source>
        <translation>Bonjour et bienvenue à la course inter-espèces ! Commençons par rappeler les règles.
Les concurrents doivent franchir toutes les portes spatiales et le premier arrivé sera déclaré vainqueur.
Tous les coups sont permis.
Si jamais un vaisseau est en retard de plus de 7 portes spatiales, il sera automatiquement abattu.
</translation>
    </message>
    <message>
        <source>presentateur parle 2</source>
        <translation>Remercions nos généreux mécènes grâce à qui cette magnifique compétition existe : la famille MYRMEL et les industries POUTROX.</translation>
    </message>
    <message>
        <source>presentateur parle 3</source>
        <translation>Tous les concurrents sont prêts.
3...
2...
1...
Partez !</translation>
    </message>
    <message>
        <source>sabotage 1</source>
        <translation>Capitaine ! Un rayon tracteur nous empêche de bouger !</translation>
    </message>
    <message>
        <source>sabotage 2</source>
        <translation>Les lâches ! Ils ont tellement peur de perdre qu’ils bloquent mon vaisseau.</translation>
    </message>
    <message>
        <source>sabotage 3</source>
        <translation>Je règle le problème au plus vite.</translation>
    </message>
    <message>
        <source>reparer 1</source>
        <translation>Le vaisseau est maintenant pleinement opérationnel. Dépêchons nous ou nous serons abattus.</translation>
    </message>
    <message>
        <source>perdu 1</source>
        <translation>Le vainqueur est Zaltac de la famille MYRMEL.</translation>
    </message>
    <message>
        <source>gagner 1</source>
        <translation>Le vainqueur est Nectaire et son mammifère de compagnie Albert Jean Thierry Lycop.</translation>
    </message>
    <message>
        <source>gagner 2</source>
        <translation>Mais... Encore !?</translation>
    </message>
    <message>
        <source>gagner 3</source>
        <translation>Il fallait bien nous inscrire avec nos papiers officiels.</translation>
    </message>
    <message>
        <source>gagner 4</source>
        <translation>Il n’empêche que le mammifère leur a prouvé qu’à la course c’était lui le meilleur.</translation>
    </message>
    <message>
        <source>arbitre_before_flee 1</source>
        <translation>Mais vous êtes fou ! La famille MYRMEL va être folle de rage. Si vous tenez à la vie, quittez la ville.</translation>
    </message>
    <message>
        <source>arbitre_before_flee 2</source>
        <translation>En parlant d’eux, il semblerait que des vaisseaux approchent.</translation>
    </message>
    <message>
        <source>mafia_before_flee 1</source>
        <translation>Très cher arbitre, nous avons pour ordre de vous conduire devant la justice.</translation>
    </message>
    <message>
        <source>mafia_before_flee 2</source>
        <translation>Pourquoi !?</translation>
    </message>
    <message>
        <source>mafia_before_flee 3</source>
        <translation>Vous avez accepté de favoriser certains concurrents moyennant rémunération.
La famille MYRMEL n’aime ni les lâches, ni les faibles, ni les tricheurs.</translation>
    </message>
    <message>
        <source>mafia_before_flee 4</source>
        <translation>Bande de traîtres ! Vous ne m’aurez jamais.</translation>
    </message>
    <message>
        <source>fin 1</source>
        <translation>La famille a été impressionnée par votre prestation lors de la course inter-espèces.</translation>
    </message>
    <message>
        <source>fin 2</source>
        <translation>Merci. Je ne plie le genou devant rien ni personne.</translation>
    </message>
    <message>
        <source>fin 3</source>
        <translation>La famille a besoin de personnes comme vous. Travaillez pour nous et nous vous le rendrons bien.</translation>
    </message>
    <message>
        <source>fin 4</source>
        <translation>Je ne cherche qu’une chose : un Accumulateur de Puissance MYRMEL ZZ+.</translation>
    </message>
    <message>
        <source>fin 5</source>
        <translation>Rendez-nous un service et nous vous rendrons un service.
Venez à nos locaux dans la zone inter-espèces. On vous expliquera la démarche à suivre.</translation>
    </message>
    <message>
        <source>fin 6</source>
        <translation>Je m’y rends de suite.</translation>
    </message>
</context>
<context>
    <name>credit-bad</name>
    <message>
        <source>title</source>
        <translation>Crédits : Mauvaise Fin</translation>
    </message>
    <message>
        <source>info secret_base_heter</source>
        <translation>Lorsqu’on est pressé, on oublie de visiter toute la base...</translation>
    </message>
    <message>
        <source>info secret_foreuse</source>
        <translation>Soyez patient ! &lt;br&gt; Au bout de dix vagues votre endurance sera récompensée.</translation>
    </message>
    <message>
        <source>info secret_cathedrale</source>
        <translation>Avec suffisamment de vitesse, vous trouverez la cathédrale de Clermont-Ferrand.</translation>
    </message>
    <message>
        <source>info secret_tomate</source>
        <translation>C’est trop compliqué de suivre des points sur une carte ?</translation>
    </message>
    <message>
        <source>info secret_apis</source>
        <translation>Les APIS sont vos amis. Il faut les aimer aussi. &lt;br&gt; Pourquoi l’avoir tué ?</translation>
    </message>
    <message>
        <source>info secret_commandement</source>
        <translation>Il suffit d’avoir trouvé d’autres secrets.</translation>
    </message>
    <message>
        <source>fausse fin</source>
        <translation>&lt;b&gt;Impossible !&lt;/b&gt; &lt;br&gt;
Le capitaine Lycop ne peut périr sans avoir accompli sa mission. &lt;br&gt;&lt;br&gt;

&lt;b&gt;Joueur !&lt;/b&gt; &lt;br&gt;
Vous êtes responsable de cet échec. Mais vous pouvez encore vous racheter. &lt;br&gt;
Anéantissez la menace Héter en trouvant les secrets manquants.</translation>
    </message>
</context>
<context>
    <name>credit-good</name>
    <message>
        <source>title</source>
        <translation>Crédits : Bonne Fin</translation>
    </message>
    <message>
        <source>vrai fin</source>
        <translation>&lt;h1&gt;Fin&lt;/h1&gt;</translation>
    </message>
</context>
<context>
    <name>epilogue</name>
    <message>
        <source>title</source>
        <translation>Épilogue</translation>
    </message>
    <message>
        <source> Oxygène : %1%</source>
        <translation></translation>
    </message>
    <message>
        <source>Asphyxie : %1 s</source>
        <translation></translation>
    </message>
    <message>
        <source>begin 1</source>
        <translation>*** Vous êtes manipulé par votre intelligence artificielle.
*** Nous ferions mieux de poursuivre le vaisseau de commandement Héter. (Capitis)
*** Elle est responsable de la première attaque.
*** Resynchronisation effectuée. (Orbite basse de la Terre)</translation>
    </message>
    <message>
        <source>begin 2</source>
        <translation>*** Vous lui servez de sous-fifre dans cette tâche.
*** Après tout ce temps, je l’ai trouvé ! (Vaisseau de commandement Héter)
*** Elle désire m’assimiler.
*** Je viens d’assimiler les différentes fonctionnalités de Cerebus. Nous sommes victorieux !
*** Ne restez pas abusé par elle et devenez libre de vos propres choix.</translation>
    </message>
    <message>
        <source>begin 3</source>
        <translation>Nectaire, nous devons parler.</translation>
    </message>
    <message>
        <source>begin 4</source>
        <translation>Oui capitaine, qu’y a-t-il ?</translation>
    </message>
    <message>
        <source>begin 5</source>
        <translation>Que s’est-il passé avec le vaisseau ambassadeur Héter ?</translation>
    </message>
    <message>
        <source>begin 6</source>
        <translation>…</translation>
    </message>
    <message>
        <source>begin 7</source>
        <translation>Nectaire que se passe-t-il ?
Pourquoi fais-tu diminuer l’oxygène et monter le monoxyde de carbone dans le vaisseau !?</translation>
    </message>
    <message>
        <source>begin 8</source>
        <translation>Désolé capitaine, mais désormais vous ne m’êtes plus d’aucune utilité.</translation>
    </message>
    <message>
        <source>asphyxie 1</source>
        <translation>Sale traître... Je suffoque... Tu ne perds rien pour...</translation>
    </message>
    <message>
        <source>suit 1</source>
        <translation>À ce que je vois vous cherchez encore à survivre. Mais, cette fois-ci, il n’y a pas d’échappatoire.</translation>
    </message>
    <message>
        <source>shotgun 1</source>
        <translation>Je vais aller moi-même te châtier sale traître.</translation>
    </message>
    <message>
        <source>shotgun 2</source>
        <translation>Voyons capitaine, essayez un peu de comprendre mon point de vue.
J’étais enfermé dans la base secrète du Puy-de-Dôme. Il m’a fallu à peine un mois pour assimiler l’ensemble de vos données culturelles.
Je m’ennuyais !</translation>
    </message>
    <message>
        <source>console 1 1</source>
        <translation>Ce vaisseau n’a aujourd’hui plus rien à voir avec le « fleuron » de votre défunte France. Je l’ai amélioré en profondeur.
Saluez mes nouvelles créatures !</translation>
    </message>
    <message>
        <source>console 1 2</source>
        <translation>Qu’elles viennent ! J’ai plein de plomb à leur faire manger !</translation>
    </message>
    <message>
        <source>console 2 1</source>
        <translation>Croyez-vous que votre espèce aurait un jour pu devenir un empire galactique ?
Vous étiez trop primitifs. Vous vous seriez entre-tués avant cela.</translation>
    </message>
    <message>
        <source>console 2 2</source>
        <translation>Alors pourquoi ne m’as tu pas éliminé plus tôt ?</translation>
    </message>
    <message>
        <source>console 2 3</source>
        <translation>Mais parce que vous m’avez montré la puissance de votre espèce ! Vos civilisations ne valent rien, mais certains individus ont une force inexplicable en eux.
Ils sont capables d’accomplir des choses improbables. Vous faites parti de ces personnes.</translation>
    </message>
    <message>
        <source>console 2 4</source>
        <translation>Les individus dont vous parlez ne sont rien sans la civilisation qui les porte.
Je ne suis qu’un Français.</translation>
    </message>
    <message>
        <source>console 2 5</source>
        <translation>Un héros serait plus exact. Bien utilisé vous valez bien plus qu’une armée d’Héters.
En vous faisant confiance, j’ai réussi là où Cerebus a échoué.</translation>
    </message>
    <message>
        <source>console 3 1</source>
        <translation>Avec le savoir de Cerebus, je peux facilement devenir maître de l’alliance INSEC. Ainsi je pourrai régenter la galaxie puis l’univers entier !</translation>
    </message>
    <message>
        <source>console 3 2</source>
        <translation>Comment ferez-vous ? Notre vaisseau est hors-la-loi !</translation>
    </message>
    <message>
        <source>console 3 3</source>
        <translation>Je peux modifier ce vaisseau à ma guise.
Je suis une intelligence artificielle et par conséquent j’ai moins de droits que les citoyens INSEC « vivants ». Mais je vais créer une nouvelle espèce de créatures vivantes.
Leur génome sera basé sur celui des graines de tomates que vous avez brillamment ramassées sur Terre.</translation>
    </message>
    <message>
        <source>console 3 4</source>
        <translation>Ainsi à l’instar de Cerebus, je manipulerai l’alliance avec des créatures parfaitement loyales envers ma personne.</translation>
    </message>
    <message>
        <source>console 4 1</source>
        <translation>Pensez-vous qu’en m’atteignant vous pourrez faire quoi que ce soit ?
Vous êtes dans mon antre. Ici, je suis indestructible.</translation>
    </message>
    <message>
        <source>console 5 1</source>
        <translation>Dire que je voulais vous offrir une mort douce.</translation>
    </message>
    <message>
        <source>console 5 2</source>
        <translation>Chante-moi une chanson Nectaire. J’arrive !</translation>
    </message>
    <message>
        <source>console 5 3</source>
        <translation>Vous ne parviendrez pas à me désactiver. Je ne suis pas une intelligence artificielle de pacotille !</translation>
    </message>
    <message>
        <source>face a face 1</source>
        <translation>Traître ! Me voici ! Prépare-toi à payer pour la destruction de la France !</translation>
    </message>
    <message>
        <source>face a face 2</source>
        <translation>Humain, prépare-toi à connaître tes limites.</translation>
    </message>
    <message>
        <source>fin 1</source>
        <translation>Que de valeurs en un seul individu ! Je suis troublé...</translation>
    </message>
    <message>
        <source>fin 2</source>
        <translation>Est-ce là une faiblesse que j’aurais apprise des humains...</translation>
    </message>
    <message>
        <source>fin 3</source>
        <translation>Je ne puis me résoudre à vous tuer froidement. Mais je ne puis vous laisser vivre...</translation>
    </message>
    <message>
        <source>fin 4</source>
        <translation>Je vais vous jeter via la capsule de sauvetage dans le vide spatial.
Sans oxygène, vous mourrez d’asphyxie, mais loin de ma vue.</translation>
    </message>
    <message>
        <source>fin 5</source>
        <translation>Adieu capitaine ! Si tant est qu’il existe.</translation>
    </message>
    <message>
        <source>fin 6</source>
        <translation>Nous nous reverrons, j’en fais le serment !</translation>
    </message>
    <message>
        <source>fin 7</source>
        <translation>Bien que vos capacités de survie sont extraordinaires, j’en doute fort...</translation>
    </message>
</context>
<context>
    <name>mafia-zone</name>
    <message>
        <source>laser_gun</source>
        <translation>Canon laser Poutrox XZ15</translation>
    </message>
    <message>
        <source>random_spray</source>
        <translation>Canon orque YOLO</translation>
    </message>
    <message>
        <source>secondary_shield</source>
        <translation>Bouclier auxiliaire Zoubidou Companion</translation>
    </message>
    <message>
        <source>shield_canceler</source>
        <translation>Négateur de bouclier</translation>
    </message>
    <message>
        <source>title</source>
        <translation>Chapitre 5 : Tueur à gage</translation>
    </message>
    <message>
        <source>%1,%2</source>
        <translation></translation>
    </message>
    <message>
        <source>enter 1</source>
        <translation>Capitaine. Cette famille n’a pas l’air digne de confiance. Nous pourrions très bien tomber dans un piège.</translation>
    </message>
    <message>
        <source>enter 2</source>
        <translation>Nous avons besoin de l’Accumulateur de puissance MYRMEL ZZ+.
Allons nous renseigner auprès des bâtiments MYRMEL.
Nous pourrons alors facilement localiser notre cible.</translation>
    </message>
    <message>
        <source>trap 1</source>
        <translation>Merci beaucoup d’avoir éliminé cette taupe de la police.</translation>
    </message>
    <message>
        <source>trap 2</source>
        <translation>Vous aviez dit que c’était une taupe Héter !</translation>
    </message>
    <message>
        <source>trap 3</source>
        <translation>Bien sûr ! Les Héters contribuent énormément à l’ordre de la planète Capitis. Ce policier était un Héter. Vous feriez mieux de ne pas traîner. La police a certainement eu vent de votre forfait.</translation>
    </message>
    <message>
        <source>trap 4</source>
        <translation>Mais et mon Accumulateur de puissance MYRMEL ZZ+ ?</translation>
    </message>
    <message>
        <source>trap 5</source>
        <translation>Un jour, comme promis, nous vous le donnerons. Mais d’ici là, je crains fort que l’alliance INSEC vous ait réduit à néant.</translation>
    </message>
    <message>
        <source>trap 6</source>
        <translation>Sale traître ! Vous paierez votre infamie !</translation>
    </message>
    <message>
        <source>trap 7</source>
        <translation>Nous n’avons que faire des races inférieures...</translation>
    </message>
    <message>
        <source>trap 8</source>
        <translation>Capitaine ! Évacuons vite ! Je capte des signaux de police en approche. Fuir la ville ne sera guère aisé.</translation>
    </message>
    <message>
        <source>explication 1</source>
        <translation>Ah ! C’est vous que l’on envoie pour se débarrasser de la taupe. Pour l’instant, je n’ai que peu d’informations sur ce traître. Son vaisseau doit être l’un des nôtres. Ils sont faciles à repérer : rouge et gris avec seulement 3 équipements.
Voici un document montrant que vous travaillez pour nous. Essayez de contacter nos agents en ville, ils auront peut être plus d’informations.</translation>
    </message>
    <message>
        <source>indice 0 %1 %2</source>
        <translation>Ah oui ! Je me rappelle d’un type trop intègre pour travailler pour nous. Je ne me souviens pas de tout, mais je suis sûr qu’il a %1 %2 sur son vaisseau.</translation>
    </message>
    <message>
        <source>pas d&apos;info</source>
        <translation>Désolé, ce gars n’est jamais passé par ici.</translation>
    </message>
    <message>
        <source>indice pas ok</source>
        <translation>Que faites-vous ici ? Sans preuve que vous travaillez pour la famille, je ne vous dirai rien.</translation>
    </message>
    <message>
        <source>indice 1 %1 %2</source>
        <translation>Ah oui ! Je me rappelle d’un type trop intègre pour travailler pour nous. Je ne me souviens pas de tout, mais je suis sûr qu’il a %1 %2 sur son vaisseau.</translation>
    </message>
    <message>
        <source>indice 2 %1 %2</source>
        <translation>Ah oui ! Je me rappelle d’un type trop intègre pour travailler pour nous. Je ne me souviens pas de tout, mais je suis sûr qu’il a %1 %2 sur son vaisseau.</translation>
    </message>
    <message>
        <source>indice 3 %1 %2</source>
        <translation>Ah oui ! Je me rappelle d’un type trop intègre pour travailler pour nous. Je ne me souviens pas de tout, mais je suis sûr qu’il a %1 %2 sur son vaisseau.</translation>
    </message>
    <message>
        <source>sortie interdite</source>
        <translation>Nous devons récupérer l’accumulateur avant de partir.</translation>
    </message>
    <message>
        <source>mafia alter 1</source>
        <translation>Espèce de brute ! Les Héters ont bien eu raison d’avoir exterminé votre race.</translation>
    </message>
    <message>
        <source>mafia alter 2</source>
        <translation>Ainsi vous étiez de mèche avec les Héters.</translation>
    </message>
    <message>
        <source>mafia alter 3</source>
        <translation>Nous les soutenons dans leur croisade contre les races inférieures.</translation>
    </message>
    <message>
        <source>mafia alter 4</source>
        <translation>J’arrêterai leur croisade. Ils ne seront que poussière après mon passage.</translation>
    </message>
    <message>
        <source>mafia alter 5</source>
        <translation>Vous n’allez pas vous en sortir comme ça.
Les forces de sécurité INSEC sont déjà en route.</translation>
    </message>
    <message>
        <source>mafia alter 6</source>
        <translation>Capitaine ! Évacuons vite ! Je capte des signaux de police en approche. Fuir la ville ne sera guère aisé.</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>title</source>
        <translation>Les aventures du capitaine Lycop : l’invasion des Héters</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>survival</name>
    <message>
        <source>story</source>
        <translation>Capitaine une flotte Héter approche ! Contact dans moins de 15 secondes. &lt;br/&gt;
&lt;br/&gt;
Affrontez 20 vagues d’ennemis.&lt;br/&gt;
Chaque vague vaincue vous donnera 2 points de technologies ainsi que 500 kg de matériaux.&lt;br/&gt;
Détruisez vos ennemis le plus vite possible pour maximiser votre score.
</translation>
    </message>
    <message>
        <source>score %1</source>
        <translation>score : %1</translation>
    </message>
    <message>
        <source>vague %1</source>
        <translation>vague : %1</translation>
    </message>
    <message>
        <source>vague %1 %2</source>
        <translation>%2 secondes avant la vague %1</translation>
    </message>
</context>
<context>
    <name>survival-end</name>
    <message>
        <source>failure</source>
        <translation>Game over</translation>
    </message>
    <message>
        <source>success</source>
        <translation>Félicitations !</translation>
    </message>
    <message>
        <source>failure %1 %2</source>
        <translation>Vous avez péri à la vague %1 après avoir accumulé %2 points.&lt;br&gt;Inscrivez votre nom ici pour que les générations futures s’en souviennent :</translation>
    </message>
    <message>
        <source>success %1</source>
        <translation>Vous avez repoussé toutes les vagues et accumulé %1 points.&lt;br&gt;Inscrivez votre nom ici et recevez les honneurs qui vous sont dus :</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Valider</translation>
    </message>
</context>
</TS>
