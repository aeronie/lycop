<!-- Attention, ne fonctionne pas avec xmlpatterns. Utiliser xsltproc. -->
<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform">
	<template match="@* | node()">
		<copy><apply-templates select="@* | node()"/></copy>
	</template>
	<template match="context">
		<copy>
			<apply-templates select="@* | name"/>
			<apply-templates select="message">
				<sort select="location/@filename"/>
				<sort select="location/@line" data-type="number"/>
				<sort select="source"/>
			</apply-templates>
		</copy>
	</template>
</stylesheet>
