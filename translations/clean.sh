#!/bin/sh

export QT_SELECT=5

for ts in *.ts
do
	lupdate -recursive -locations absolute -no-obsolete .. -ts "$ts"
	sorted=$(xsltproc sort.xsl "$ts")
	echo "$sorted" >"$ts"
	lupdate -recursive -locations none .. -ts "$ts"
done
