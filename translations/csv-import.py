#!/usr/bin/python
texts = {}
langs = ('fr_FR', 'en_GB')

import csv
for row in csv.reader(open('ts.csv', 'r')):
	row = [x.decode('utf-8') for x in row]
	if row[0] not in texts: texts[row[0]] = {}
	texts[row[0]][row[1]] = row[2:]

import xml.etree.ElementTree as ET
for i, lang in enumerate(langs):
	root_ = ET.Element('TS', {'version': '2.1', 'language': lang})
	out = ET.ElementTree(root_)
	for context, messages in texts.iteritems():
		context_ = ET.SubElement(root_, 'context')
		ET.SubElement(context_, 'name').text = context
		for source, translations in messages.iteritems():
			message_ = ET.SubElement(context_, 'message')
			ET.SubElement(message_, 'source').text = source
			translation_ = ET.SubElement(message_, 'translation')
			translation_.text = translations[i * 2]
			translation_.set('type', translations[i * 2 + 1])
	out.write(lang + '.xml', 'utf-8')
