import QtQuick 2.0
import aw 0.0

QtObject
{
	readonly property QtObject direction: DirectionControl
	{
		objectName: "direction"
		text: qsTr("direction")
	}
	readonly property QtObject velocity: DirectionControl
	{
		objectName: "velocity"
		text: qsTr("velocity")
	}
	readonly property QtObject weapons: BooleanControl
	{
		objectName: "weapons"
		text: qsTr("weapons")
	}
	readonly property QtObject secondary_weapons: BooleanControl
	{
		objectName: "secondary-weapons"
		text: qsTr("secondary-weapons")
	}
	readonly property QtObject speed_boost: BooleanControl
	{
		objectName: "speed-boost"
		text: qsTr("speed-boost")
	}
	readonly property QtObject weapons_boost: BooleanControl
	{
		objectName: "weapons-boost"
		text: qsTr("weapons-boost")
	}
	readonly property QtObject shield_boost: BooleanControl
	{
		objectName: "shield-boost"
		text: qsTr("shield-boost")
	}
	readonly property QtObject repair: BooleanControl
	{
		objectName: "repair"
		text: qsTr("repair")
	}
	readonly property var slots:
	{
		var a = []
		for(var i = 0; i < 7; ++i) a.push(boolean_control.createObject(null, {objectName: "slot-%1".arg(i), text: qsTr("slot-%1").arg(i + 1)}))
		return a
	}
	readonly property var buttons:
	{
		var a = []
		for(var i = 0; i < 10; ++i) a.push(boolean_control.createObject(null, {objectName: "button-%1".arg(i), text: qsTr("button-%1").arg(i + 1)}))
		return a
	}
	readonly property var all:
	{
		var a = [direction, velocity, weapons, secondary_weapons, speed_boost, weapons_boost, shield_boost, repair]
		for(var i = 0; i < slots.length; ++i) a.push(slots[i])
		for(var i = 0; i < buttons.length; ++i) a.push(buttons[i])
		return a
	}
	function update()
	{
		for(var i = 0; i < connections.length; ++i) connections[i].destroy()
		connections = []
		for(var i1 = 0; i1 < all.length; ++i1)
		{
			var x1 = all[i1]
			var info = x1.info()
			for(var i2 = 0; i2 < info.count(); ++i2)
			{
				var x2 = info.get(i2, "name")
				var s = settings.get(info.get(i2, "key"), "0").split(",")
				function connect(object, signal)
				{
					connections.push(object)
					object[signal].connect(x1[x2])
				}
				switch(s[0]*1)
				{
					case 1: connect(inputs.mouse_button(s[1]), "pressed"); break
					case 3: connect(inputs.key(s[1]), "pressed"); break
					case 4: connect(inputs.mouse(), "moved"); break
					case 5: connect(inputs.joystick_button(s[1], s[2]), "pressed"); break
					case 6: connect(inputs.joystick_axis(s[1], s[2]), "moved"); break
					case 7: connect(inputs.joystick_hat(s[1], s[2]), "moved"); break
					case 8: connect(inputs.joystick_ball(s[1], s[2]), "moved"); break
				}
			}
		}
	}
	function init(ship)
	{
		function control_velocity(x) {(settings.velocity_absolute ? ship.set_target_velocity : ship.set_relative_velocity)(x)}
		function disconnect() {velocity.value_changed.disconnect(control_velocity)}
		ship.Component.destruction.connect(disconnect)
		velocity.value_changed.connect(control_velocity)
		direction.value_changed.connect(ship.set_target_direction)
	}
	readonly property var to_string_:
	[
		function() {return ""},
		function(x) {return qsTr("Mouse button %1").arg(1 + Math.log(x) / Math.log(2))},
		function(x) {return qsTr("Mouse wheel %1").arg(inputs.key_name(x + Qt.Key_Left))},
		function(x) {return qsTr("Keyboard %1").arg(inputs.key_name(x))},
		function(x) {return qsTr("Mouse")},
		function(x, y) {return qsTr("Joystick button %1:%2").arg(x + 1).arg(y + 1)},
		function(x, y) {return qsTr("Joystick axis %1:%2").arg(x + 1).arg(y + 1)},
		function(x, y) {return qsTr("Joystick hat %1:%2").arg(x + 1).arg(y + 1)},
		function(x, y) {return qsTr("Joystick ball %1:%2").arg(x + 1).arg(y + 1)},
	]
	function to_string(key)
	{
		var x = settings.get(key, "").split(",")
		return x[0] ? "<span>" + to_string_[~~x[0]](~~x[1], ~~x[2]) + "</span>" : ""
	}
	function get_bindings(array, control)
	{
		var info = control.info()
		for(var i = 0; i < info.count(); ++i)
		{
			var x = controls.to_string(info.get(i, "key"))
			if(x) array.push(x)
		}
	}
	function reset_all()
	{
		for(var i1 = 0; i1 < all.length; ++i1) settings.remove("controls/"+all[i1].objectName)
		settings.set("controls/direction/up_left_down_right/0", "4,0")
		settings.set("controls/direction/up_left_down_right/1", "7,0,0")
		settings.set("controls/direction/up_down/0", "6,0,1")
		settings.set("controls/direction/left_right/0", "6,0,0")
		settings.set("controls/velocity/up/0", "3,"+Qt[qsTr("Key_Up")])
		settings.set("controls/velocity/up/1", "3,"+Qt[qsTr("Key_W")])
		settings.set("controls/velocity/down/0", "3,"+Qt[qsTr("Key_Down")])
		settings.set("controls/velocity/down/1", "3,"+Qt[qsTr("Key_S")])
		settings.set("controls/velocity/left/0", "3,"+Qt[qsTr("Key_Left")])
		settings.set("controls/velocity/left/1", "3,"+Qt[qsTr("Key_A")])
		settings.set("controls/velocity/right/0", "3,"+Qt[qsTr("Key_Right")])
		settings.set("controls/velocity/right/1", "3,"+Qt[qsTr("Key_D")])
		settings.set("controls/velocity/up_down/0", "6,0,3")
		settings.set("controls/velocity/left_right/0", "6,0,2")
		settings.set("controls/weapons/enable/0", "1,1")
		settings.set("controls/weapons/enable/1", "5,0,0")
		settings.set("controls/secondary-weapons/enable/0", "3,"+Qt.Key_Space)
		settings.set("controls/secondary-weapons/enable/1", "5,0,1")
		settings.set("controls/speed-boost/enable/0", "1,2")
		settings.set("controls/weapons-boost/enable/0", "3,"+Qt.Key_Shift)
		settings.set("controls/weapons-boost/enable/1", "1,8")
		settings.set("controls/shield-boost/enable/0", "3,"+Qt.Key_Control)
		settings.set("controls/shield-boost/enable/1", "1,16")
		settings.set("controls/button-0/enable/0", "3,"+Qt[qsTr("Key_1")])
		settings.set("controls/button-1/enable/0", "3,"+Qt[qsTr("Key_2")])
		settings.set("controls/button-2/enable/0", "3,"+Qt[qsTr("Key_3")])
		settings.set("controls/button-3/enable/0", "3,"+Qt[qsTr("Key_4")])
		settings.set("controls/button-4/enable/0", "3,"+Qt[qsTr("Key_5")])
		settings.set("controls/button-5/enable/0", "3,"+Qt[qsTr("Key_6")])
		settings.set("controls/button-6/enable/0", "3,"+Qt[qsTr("Key_7")])
		settings.set("controls/button-7/enable/0", "3,"+Qt[qsTr("Key_8")])
		settings.set("controls/button-8/enable/0", "3,"+Qt[qsTr("Key_9")])
		settings.set("controls/button-9/enable/0", "3,"+Qt[qsTr("Key_0")])
		settings.set("controls/initialized", true)
		update()
	}
	readonly property Component boolean_control: BooleanControl {}
	property var connections: []
	Component.onCompleted:
	{
		if(!settings.get("controls/initialized", false)) reset_all()
		update()
	}
}
