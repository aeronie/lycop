#include <QtConcurrent>

#include "meta.private.h"

namespace aw {
namespace {

struct ThreadPool
: QObject
{
	AW_DECLARE_OBJECT_STUB(ThreadPool)
	AW_DECLARE_PROPERTY_READONLY(int, count)
	QAtomicInt count_ = 0;

	void begin()
	{
		++count_;
		Q_EMIT count_changed(count_);
	}

	void end()
	{
		--count_;
		Q_EMIT count_changed(count_);
	}

	static void async(ThreadPool *pool, QObject *object, QByteArray const &member, QVariantList args)
	{
		Q_ASSERT(args.count() < 10);
		QMetaObject const *meta = object->metaObject();
		Q_ASSERT(meta);

		for(int method_id = meta->methodCount(); method_id--;)
		{
			QMetaMethod method = meta->method(method_id);
			Q_ASSERT(method.isValid());
			if(method.name() != member) continue;
			QList<QByteArray> types = method.parameterTypes();
			if(types.count() != args.count()) continue;
			QGenericArgument a[10];

			for(int i = 0; i < args.count(); ++i)
			{
				int type = method.parameterType(i);
				if(!args[i].canConvert(type)) goto next;
				args[i].convert(type);
				a[i] = {types[i], const_cast<void *>(args[i].constData())};
			}

			if(!method.invoke(object, Qt::DirectConnection, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9])) Q_UNREACHABLE();
			return pool->end();
			next:;
		}

		Q_ASSERT(args.count() == 1);
		int property_id = meta->indexOfProperty(member);
		Q_ASSERT(property_id >= 0);
		QMetaProperty property = meta->property(property_id);
		Q_ASSERT(property.isWritable());
		Q_ASSERT(args[0].canConvert(property.userType()));
		args[0].convert(property.userType());
		if(!property.write(object, args[0])) Q_UNREACHABLE();
		return pool->end();
	}

protected:

	Q_SLOT void run(QObject *object, QByteArray const &member, QVariantList args)
	{
		begin();
		QtConcurrent::run(async, this, object, member, args);
	}
};

int ThreadPool::count() const
{
	return count_;
}

AW_DEFINE_OBJECT_STUB(ThreadPool)

}
}
